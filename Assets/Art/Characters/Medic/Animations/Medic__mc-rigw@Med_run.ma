//Maya ASCII 2013 scene
//Name: Medic__mc-rigw@Med_run.ma
//Last modified: Tue, Oct 28, 2014 04:02:31 PM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Medic/Medic__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Medic__rig.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Medic/Medic__mc-rigw.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.4";
createNode transform -s -n "persp";
	setAttr ".t" -type "double3" 63.665723362325153 65.315375754404585 91.826755880617284 ;
	setAttr ".r" -type "double3" -23.138352729647064 -327.80000000002849 -9.3966567232447049e-16 ;
	setAttr ".rp" -type "double3" 2.55351295663786e-15 4.4408920985006262e-16 0 ;
	setAttr ".rpt" -type "double3" -7.9036382800926453e-16 -1.6005923041385164e-19 1.8471263371705302e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v";
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 135.22883774920209;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.51186942342126507 2.9166354580199307 -0.034653475996007543 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".t" -type "double3" 101.06737012034692 32.193809579173411 -0.59211508345640151 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 41.283563045993432;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" -3.2345791681016438 6.6731242405084625 500.04116847780688 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".ow" 65.801277620892151;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 17 ".lnk";
	setAttr -s 17 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 1;
	setAttr -s 3 ".dli[1:2]"  1 2;
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 825 ".phl";
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".phl[440]" 0;
	setAttr ".phl[441]" 0;
	setAttr ".phl[442]" 0;
	setAttr ".phl[443]" 0;
	setAttr ".phl[444]" 0;
	setAttr ".phl[445]" 0;
	setAttr ".phl[446]" 0;
	setAttr ".phl[447]" 0;
	setAttr ".phl[448]" 0;
	setAttr ".phl[449]" 0;
	setAttr ".phl[450]" 0;
	setAttr ".phl[451]" 0;
	setAttr ".phl[452]" 0;
	setAttr ".phl[453]" 0;
	setAttr ".phl[454]" 0;
	setAttr ".phl[455]" 0;
	setAttr ".phl[456]" 0;
	setAttr ".phl[457]" 0;
	setAttr ".phl[458]" 0;
	setAttr ".phl[459]" 0;
	setAttr ".phl[460]" 0;
	setAttr ".phl[461]" 0;
	setAttr ".phl[462]" 0;
	setAttr ".phl[463]" 0;
	setAttr ".phl[464]" 0;
	setAttr ".phl[465]" 0;
	setAttr ".phl[466]" 0;
	setAttr ".phl[467]" 0;
	setAttr ".phl[468]" 0;
	setAttr ".phl[469]" 0;
	setAttr ".phl[470]" 0;
	setAttr ".phl[471]" 0;
	setAttr ".phl[472]" 0;
	setAttr ".phl[473]" 0;
	setAttr ".phl[474]" 0;
	setAttr ".phl[475]" 0;
	setAttr ".phl[476]" 0;
	setAttr ".phl[477]" 0;
	setAttr ".phl[478]" 0;
	setAttr ".phl[479]" 0;
	setAttr ".phl[480]" 0;
	setAttr ".phl[481]" 0;
	setAttr ".phl[482]" 0;
	setAttr ".phl[483]" 0;
	setAttr ".phl[484]" 0;
	setAttr ".phl[485]" 0;
	setAttr ".phl[486]" 0;
	setAttr ".phl[487]" 0;
	setAttr ".phl[488]" 0;
	setAttr ".phl[489]" 0;
	setAttr ".phl[490]" 0;
	setAttr ".phl[491]" 0;
	setAttr ".phl[492]" 0;
	setAttr ".phl[493]" 0;
	setAttr ".phl[494]" 0;
	setAttr ".phl[495]" 0;
	setAttr ".phl[496]" 0;
	setAttr ".phl[497]" 0;
	setAttr ".phl[498]" 0;
	setAttr ".phl[499]" 0;
	setAttr ".phl[500]" 0;
	setAttr ".phl[501]" 0;
	setAttr ".phl[502]" 0;
	setAttr ".phl[503]" 0;
	setAttr ".phl[504]" 0;
	setAttr ".phl[505]" 0;
	setAttr ".phl[506]" 0;
	setAttr ".phl[507]" 0;
	setAttr ".phl[508]" 0;
	setAttr ".phl[509]" 0;
	setAttr ".phl[510]" 0;
	setAttr ".phl[511]" 0;
	setAttr ".phl[512]" 0;
	setAttr ".phl[513]" 0;
	setAttr ".phl[514]" 0;
	setAttr ".phl[515]" 0;
	setAttr ".phl[516]" 0;
	setAttr ".phl[517]" 0;
	setAttr ".phl[518]" 0;
	setAttr ".phl[519]" 0;
	setAttr ".phl[520]" 0;
	setAttr ".phl[521]" 0;
	setAttr ".phl[522]" 0;
	setAttr ".phl[523]" 0;
	setAttr ".phl[524]" 0;
	setAttr ".phl[525]" 0;
	setAttr ".phl[526]" 0;
	setAttr ".phl[527]" 0;
	setAttr ".phl[528]" 0;
	setAttr ".phl[529]" 0;
	setAttr ".phl[530]" 0;
	setAttr ".phl[531]" 0;
	setAttr ".phl[532]" 0;
	setAttr ".phl[533]" 0;
	setAttr ".phl[534]" 0;
	setAttr ".phl[535]" 0;
	setAttr ".phl[536]" 0;
	setAttr ".phl[537]" 0;
	setAttr ".phl[538]" 0;
	setAttr ".phl[539]" 0;
	setAttr ".phl[540]" 0;
	setAttr ".phl[541]" 0;
	setAttr ".phl[542]" 0;
	setAttr ".phl[543]" 0;
	setAttr ".phl[544]" 0;
	setAttr ".phl[545]" 0;
	setAttr ".phl[546]" 0;
	setAttr ".phl[547]" 0;
	setAttr ".phl[548]" 0;
	setAttr ".phl[549]" 0;
	setAttr ".phl[550]" 0;
	setAttr ".phl[551]" 0;
	setAttr ".phl[552]" 0;
	setAttr ".phl[553]" 0;
	setAttr ".phl[554]" 0;
	setAttr ".phl[555]" 0;
	setAttr ".phl[556]" 0;
	setAttr ".phl[557]" 0;
	setAttr ".phl[558]" 0;
	setAttr ".phl[559]" 0;
	setAttr ".phl[560]" 0;
	setAttr ".phl[561]" 0;
	setAttr ".phl[562]" 0;
	setAttr ".phl[563]" 0;
	setAttr ".phl[564]" 0;
	setAttr ".phl[565]" 0;
	setAttr ".phl[566]" 0;
	setAttr ".phl[567]" 0;
	setAttr ".phl[568]" 0;
	setAttr ".phl[569]" 0;
	setAttr ".phl[570]" 0;
	setAttr ".phl[571]" 0;
	setAttr ".phl[572]" 0;
	setAttr ".phl[573]" 0;
	setAttr ".phl[574]" 0;
	setAttr ".phl[575]" 0;
	setAttr ".phl[576]" 0;
	setAttr ".phl[577]" 0;
	setAttr ".phl[578]" 0;
	setAttr ".phl[579]" 0;
	setAttr ".phl[580]" 0;
	setAttr ".phl[581]" 0;
	setAttr ".phl[582]" 0;
	setAttr ".phl[583]" 0;
	setAttr ".phl[584]" 0;
	setAttr ".phl[585]" 0;
	setAttr ".phl[586]" 0;
	setAttr ".phl[587]" 0;
	setAttr ".phl[588]" 0;
	setAttr ".phl[589]" 0;
	setAttr ".phl[590]" 0;
	setAttr ".phl[591]" 0;
	setAttr ".phl[592]" 0;
	setAttr ".phl[593]" 0;
	setAttr ".phl[594]" 0;
	setAttr ".phl[595]" 0;
	setAttr ".phl[596]" 0;
	setAttr ".phl[597]" 0;
	setAttr ".phl[598]" 0;
	setAttr ".phl[599]" 0;
	setAttr ".phl[600]" 0;
	setAttr ".phl[601]" 0;
	setAttr ".phl[602]" 0;
	setAttr ".phl[603]" 0;
	setAttr ".phl[604]" 0;
	setAttr ".phl[605]" 0;
	setAttr ".phl[606]" 0;
	setAttr ".phl[607]" 0;
	setAttr ".phl[608]" 0;
	setAttr ".phl[609]" 0;
	setAttr ".phl[610]" 0;
	setAttr ".phl[611]" 0;
	setAttr ".phl[612]" 0;
	setAttr ".phl[613]" 0;
	setAttr ".phl[614]" 0;
	setAttr ".phl[615]" 0;
	setAttr ".phl[616]" 0;
	setAttr ".phl[617]" 0;
	setAttr ".phl[618]" 0;
	setAttr ".phl[619]" 0;
	setAttr ".phl[620]" 0;
	setAttr ".phl[621]" 0;
	setAttr ".phl[622]" 0;
	setAttr ".phl[623]" 0;
	setAttr ".phl[624]" 0;
	setAttr ".phl[625]" 0;
	setAttr ".phl[626]" 0;
	setAttr ".phl[627]" 0;
	setAttr ".phl[628]" 0;
	setAttr ".phl[629]" 0;
	setAttr ".phl[630]" 0;
	setAttr ".phl[631]" 0;
	setAttr ".phl[632]" 0;
	setAttr ".phl[633]" 0;
	setAttr ".phl[634]" 0;
	setAttr ".phl[635]" 0;
	setAttr ".phl[636]" 0;
	setAttr ".phl[637]" 0;
	setAttr ".phl[638]" 0;
	setAttr ".phl[639]" 0;
	setAttr ".phl[640]" 0;
	setAttr ".phl[641]" 0;
	setAttr ".phl[642]" 0;
	setAttr ".phl[643]" 0;
	setAttr ".phl[644]" 0;
	setAttr ".phl[645]" 0;
	setAttr ".phl[646]" 0;
	setAttr ".phl[647]" 0;
	setAttr ".phl[648]" 0;
	setAttr ".phl[649]" 0;
	setAttr ".phl[650]" 0;
	setAttr ".phl[651]" 0;
	setAttr ".phl[652]" 0;
	setAttr ".phl[653]" 0;
	setAttr ".phl[654]" 0;
	setAttr ".phl[655]" 0;
	setAttr ".phl[656]" 0;
	setAttr ".phl[657]" 0;
	setAttr ".phl[658]" 0;
	setAttr ".phl[659]" 0;
	setAttr ".phl[660]" 0;
	setAttr ".phl[661]" 0;
	setAttr ".phl[662]" 0;
	setAttr ".phl[663]" 0;
	setAttr ".phl[664]" 0;
	setAttr ".phl[665]" 0;
	setAttr ".phl[666]" 0;
	setAttr ".phl[667]" 0;
	setAttr ".phl[668]" 0;
	setAttr ".phl[669]" 0;
	setAttr ".phl[670]" 0;
	setAttr ".phl[671]" 0;
	setAttr ".phl[672]" 0;
	setAttr ".phl[673]" 0;
	setAttr ".phl[674]" 0;
	setAttr ".phl[675]" 0;
	setAttr ".phl[676]" 0;
	setAttr ".phl[677]" 0;
	setAttr ".phl[678]" 0;
	setAttr ".phl[679]" 0;
	setAttr ".phl[680]" 0;
	setAttr ".phl[681]" 0;
	setAttr ".phl[682]" 0;
	setAttr ".phl[683]" 0;
	setAttr ".phl[684]" 0;
	setAttr ".phl[685]" 0;
	setAttr ".phl[686]" 0;
	setAttr ".phl[687]" 0;
	setAttr ".phl[688]" 0;
	setAttr ".phl[689]" 0;
	setAttr ".phl[690]" 0;
	setAttr ".phl[691]" 0;
	setAttr ".phl[692]" 0;
	setAttr ".phl[693]" 0;
	setAttr ".phl[694]" 0;
	setAttr ".phl[695]" 0;
	setAttr ".phl[696]" 0;
	setAttr ".phl[697]" 0;
	setAttr ".phl[698]" 0;
	setAttr ".phl[699]" 0;
	setAttr ".phl[700]" 0;
	setAttr ".phl[701]" 0;
	setAttr ".phl[702]" 0;
	setAttr ".phl[703]" 0;
	setAttr ".phl[704]" 0;
	setAttr ".phl[705]" 0;
	setAttr ".phl[706]" 0;
	setAttr ".phl[707]" 0;
	setAttr ".phl[708]" 0;
	setAttr ".phl[709]" 0;
	setAttr ".phl[710]" 0;
	setAttr ".phl[711]" 0;
	setAttr ".phl[712]" 0;
	setAttr ".phl[713]" 0;
	setAttr ".phl[714]" 0;
	setAttr ".phl[715]" 0;
	setAttr ".phl[716]" 0;
	setAttr ".phl[717]" 0;
	setAttr ".phl[718]" 0;
	setAttr ".phl[719]" 0;
	setAttr ".phl[720]" 0;
	setAttr ".phl[721]" 0;
	setAttr ".phl[722]" 0;
	setAttr ".phl[723]" 0;
	setAttr ".phl[724]" 0;
	setAttr ".phl[725]" 0;
	setAttr ".phl[726]" 0;
	setAttr ".phl[727]" 0;
	setAttr ".phl[728]" 0;
	setAttr ".phl[729]" 0;
	setAttr ".phl[730]" 0;
	setAttr ".phl[731]" 0;
	setAttr ".phl[732]" 0;
	setAttr ".phl[733]" 0;
	setAttr ".phl[734]" 0;
	setAttr ".phl[735]" 0;
	setAttr ".phl[736]" 0;
	setAttr ".phl[737]" 0;
	setAttr ".phl[738]" 0;
	setAttr ".phl[739]" 0;
	setAttr ".phl[740]" 0;
	setAttr ".phl[741]" 0;
	setAttr ".phl[742]" 0;
	setAttr ".phl[743]" 0;
	setAttr ".phl[744]" 0;
	setAttr ".phl[745]" 0;
	setAttr ".phl[746]" 0;
	setAttr ".phl[747]" 0;
	setAttr ".phl[748]" 0;
	setAttr ".phl[749]" 0;
	setAttr ".phl[750]" 0;
	setAttr ".phl[751]" 0;
	setAttr ".phl[752]" 0;
	setAttr ".phl[753]" 0;
	setAttr ".phl[754]" 0;
	setAttr ".phl[755]" 0;
	setAttr ".phl[756]" 0;
	setAttr ".phl[757]" 0;
	setAttr ".phl[758]" 0;
	setAttr ".phl[759]" 0;
	setAttr ".phl[760]" 0;
	setAttr ".phl[761]" 0;
	setAttr ".phl[762]" 0;
	setAttr ".phl[763]" 0;
	setAttr ".phl[764]" 0;
	setAttr ".phl[765]" 0;
	setAttr ".phl[766]" 0;
	setAttr ".phl[767]" 0;
	setAttr ".phl[768]" 0;
	setAttr ".phl[769]" 0;
	setAttr ".phl[770]" 0;
	setAttr ".phl[771]" 0;
	setAttr ".phl[772]" 0;
	setAttr ".phl[773]" 0;
	setAttr ".phl[774]" 0;
	setAttr ".phl[775]" 0;
	setAttr ".phl[776]" 0;
	setAttr ".phl[777]" 0;
	setAttr ".phl[778]" 0;
	setAttr ".phl[779]" 0;
	setAttr ".phl[780]" 0;
	setAttr ".phl[781]" 0;
	setAttr ".phl[782]" 0;
	setAttr ".phl[783]" 0;
	setAttr ".phl[784]" 0;
	setAttr ".phl[785]" 0;
	setAttr ".phl[786]" 0;
	setAttr ".phl[787]" 0;
	setAttr ".phl[788]" 0;
	setAttr ".phl[789]" 0;
	setAttr ".phl[790]" 0;
	setAttr ".phl[791]" 0;
	setAttr ".phl[792]" 0;
	setAttr ".phl[793]" 0;
	setAttr ".phl[794]" 0;
	setAttr ".phl[795]" 0;
	setAttr ".phl[796]" 0;
	setAttr ".phl[797]" 0;
	setAttr ".phl[798]" 0;
	setAttr ".phl[799]" 0;
	setAttr ".phl[800]" 0;
	setAttr ".phl[801]" 0;
	setAttr ".phl[802]" 0;
	setAttr ".phl[803]" 0;
	setAttr ".phl[804]" 0;
	setAttr ".phl[805]" 0;
	setAttr ".phl[806]" 0;
	setAttr ".phl[807]" 0;
	setAttr ".phl[808]" 0;
	setAttr ".phl[809]" 0;
	setAttr ".phl[810]" 0;
	setAttr ".phl[811]" 0;
	setAttr ".phl[812]" 0;
	setAttr ".phl[813]" 0;
	setAttr ".phl[814]" 0;
	setAttr ".phl[815]" 0;
	setAttr ".phl[816]" 0;
	setAttr ".phl[817]" 0;
	setAttr ".phl[818]" 0;
	setAttr ".phl[819]" 0;
	setAttr ".phl[820]" 0;
	setAttr ".phl[821]" 0;
	setAttr ".phl[822]" 0;
	setAttr ".phl[823]" 0;
	setAttr ".phl[824]" 0;
	setAttr ".phl[825]" 0;
	setAttr ".phl[826]" 0;
	setAttr ".phl[827]" 0;
	setAttr ".phl[828]" 0;
	setAttr ".phl[829]" 0;
	setAttr ".phl[830]" 0;
	setAttr ".phl[831]" 0;
	setAttr ".phl[832]" 0;
	setAttr ".phl[833]" 0;
	setAttr ".phl[834]" 0;
	setAttr ".phl[835]" 0;
	setAttr ".phl[836]" 0;
	setAttr ".phl[837]" 0;
	setAttr ".phl[838]" 0;
	setAttr ".phl[839]" 0;
	setAttr ".phl[840]" 0;
	setAttr ".phl[841]" 0;
	setAttr ".phl[842]" 0;
	setAttr ".phl[843]" 0;
	setAttr ".phl[844]" 0;
	setAttr ".phl[845]" 0;
	setAttr ".phl[846]" 0;
	setAttr ".phl[847]" 0;
	setAttr ".phl[848]" 0;
	setAttr ".phl[849]" 0;
	setAttr ".phl[850]" 0;
	setAttr ".phl[851]" 0;
	setAttr ".phl[852]" 0;
	setAttr ".phl[853]" 0;
	setAttr ".phl[854]" 0;
	setAttr ".phl[855]" 0;
	setAttr ".phl[856]" 0;
	setAttr ".phl[857]" 0;
	setAttr ".phl[858]" 0;
	setAttr ".phl[859]" 0;
	setAttr ".phl[860]" 0;
	setAttr ".phl[861]" 0;
	setAttr ".phl[862]" 0;
	setAttr ".phl[863]" 0;
	setAttr ".phl[864]" 0;
	setAttr ".phl[865]" 0;
	setAttr ".phl[866]" 0;
	setAttr ".phl[867]" 0;
	setAttr ".phl[868]" 0;
	setAttr ".phl[869]" 0;
	setAttr ".phl[870]" 0;
	setAttr ".phl[871]" 0;
	setAttr ".phl[872]" 0;
	setAttr ".phl[873]" 0;
	setAttr ".phl[874]" 0;
	setAttr ".phl[875]" 0;
	setAttr ".phl[876]" 0;
	setAttr ".phl[877]" 0;
	setAttr ".phl[878]" 0;
	setAttr ".phl[879]" 0;
	setAttr ".phl[880]" 0;
	setAttr ".phl[881]" 0;
	setAttr ".phl[882]" 0;
	setAttr ".phl[883]" 0;
	setAttr ".phl[884]" 0;
	setAttr ".phl[885]" 0;
	setAttr ".phl[886]" 0;
	setAttr ".phl[887]" 0;
	setAttr ".phl[888]" 0;
	setAttr ".phl[889]" 0;
	setAttr ".phl[890]" 0;
	setAttr ".phl[891]" 0;
	setAttr ".phl[892]" 0;
	setAttr ".phl[893]" 0;
	setAttr ".phl[894]" 0;
	setAttr ".phl[895]" 0;
	setAttr ".phl[896]" 0;
	setAttr ".phl[897]" 0;
	setAttr ".phl[898]" 0;
	setAttr ".phl[899]" 0;
	setAttr ".phl[900]" 0;
	setAttr ".phl[901]" 0;
	setAttr ".phl[902]" 0;
	setAttr ".phl[903]" 0;
	setAttr ".phl[904]" 0;
	setAttr ".phl[905]" 0;
	setAttr ".phl[906]" 0;
	setAttr ".phl[907]" 0;
	setAttr ".phl[908]" 0;
	setAttr ".phl[909]" 0;
	setAttr ".phl[910]" 0;
	setAttr ".phl[911]" 0;
	setAttr ".phl[912]" 0;
	setAttr ".phl[913]" 0;
	setAttr ".phl[914]" 0;
	setAttr ".phl[915]" 0;
	setAttr ".phl[916]" 0;
	setAttr ".phl[917]" 0;
	setAttr ".phl[918]" 0;
	setAttr ".phl[919]" 0;
	setAttr ".phl[920]" 0;
	setAttr ".phl[921]" 0;
	setAttr ".phl[922]" 0;
	setAttr ".phl[923]" 0;
	setAttr ".phl[924]" 0;
	setAttr ".phl[925]" 0;
	setAttr ".phl[926]" 0;
	setAttr ".phl[927]" 0;
	setAttr ".phl[928]" 0;
	setAttr ".phl[929]" 0;
	setAttr ".phl[930]" 0;
	setAttr ".phl[931]" 0;
	setAttr ".phl[932]" 0;
	setAttr ".phl[933]" 0;
	setAttr ".phl[934]" 0;
	setAttr ".phl[935]" 0;
	setAttr ".phl[936]" 0;
	setAttr ".phl[937]" 0;
	setAttr ".phl[938]" 0;
	setAttr ".phl[939]" 0;
	setAttr ".phl[940]" 0;
	setAttr ".phl[941]" 0;
	setAttr ".phl[942]" 0;
	setAttr ".phl[943]" 0;
	setAttr ".phl[944]" 0;
	setAttr ".phl[945]" 0;
	setAttr ".phl[946]" 0;
	setAttr ".phl[947]" 0;
	setAttr ".phl[948]" 0;
	setAttr ".phl[949]" 0;
	setAttr ".phl[950]" 0;
	setAttr ".phl[951]" 0;
	setAttr ".phl[952]" 0;
	setAttr ".phl[953]" 0;
	setAttr ".phl[954]" 0;
	setAttr ".phl[955]" 0;
	setAttr ".phl[956]" 0;
	setAttr ".phl[957]" 0;
	setAttr ".phl[958]" 0;
	setAttr ".phl[959]" 0;
	setAttr ".phl[960]" 0;
	setAttr ".phl[961]" 0;
	setAttr ".phl[962]" 0;
	setAttr ".phl[963]" 0;
	setAttr ".phl[964]" 0;
	setAttr ".phl[965]" 0;
	setAttr ".phl[966]" 0;
	setAttr ".phl[967]" 0;
	setAttr ".phl[968]" 0;
	setAttr ".phl[969]" 0;
	setAttr ".phl[970]" 0;
	setAttr ".phl[971]" 0;
	setAttr ".phl[972]" 0;
	setAttr ".phl[973]" 0;
	setAttr ".phl[974]" 0;
	setAttr ".phl[975]" 0;
	setAttr ".phl[976]" 0;
	setAttr ".phl[977]" 0;
	setAttr ".phl[978]" 0;
	setAttr ".phl[979]" 0;
	setAttr ".phl[980]" 0;
	setAttr ".phl[981]" 0;
	setAttr ".phl[982]" 0;
	setAttr ".phl[983]" 0;
	setAttr ".phl[984]" 0;
	setAttr ".phl[985]" 0;
	setAttr ".phl[986]" 0;
	setAttr ".phl[987]" 0;
	setAttr ".phl[988]" 0;
	setAttr ".phl[989]" 0;
	setAttr ".phl[990]" 0;
	setAttr ".phl[991]" 0;
	setAttr ".phl[992]" 0;
	setAttr ".phl[993]" 0;
	setAttr ".phl[994]" 0;
	setAttr ".phl[995]" 0;
	setAttr ".phl[996]" 0;
	setAttr ".phl[997]" 0;
	setAttr ".phl[998]" 0;
	setAttr ".phl[999]" 0;
	setAttr ".phl[1000]" 0;
	setAttr ".phl[1001]" 0;
	setAttr ".phl[1002]" 0;
	setAttr ".phl[1003]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 131
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27" 
		"translate" " -type \"double3\" 1.391941 1.274803 3.265174"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27" 
		"translateX" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27" 
		"translateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28" 
		"translate" " -type \"double3\" 1.381383 1.714877 0.927441"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28" 
		"translateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29" 
		"translate" " -type \"double3\" 1.381383 1.714877 -1.768712"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29" 
		"translateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30" 
		"translate" " -type \"double3\" 1.437952 1.194252 -4.098118"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30" 
		"translateX" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30" 
		"translateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo39" 
		"translate" " -type \"double3\" -1.392227 1.274803 3.265174"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo39" 
		"rotate" " -type \"double3\" 180 0 -90"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo39" 
		"scale" " -type \"double3\" 0.659898 0.573087 -0.659898"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo39" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo39" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo39" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo39" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo40" 
		"translate" " -type \"double3\" -1.381666 1.714877 0.927441"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo40" 
		"scale" " -type \"double3\" 1.112729 0.966348 -1.112729"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo41" 
		"translate" " -type \"double3\" -1.381666 1.714877 -1.768712"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo41" 
		"scale" " -type \"double3\" 1.112729 0.966348 -1.112729"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo42" 
		"translate" " -type \"double3\" -1.438247 1.194252 -4.098118"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo42" 
		"rotate" " -type \"double3\" 180 0 -90"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo42" 
		"scale" " -type \"double3\" 0.659898 0.573087 -0.659898"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo42" 
		"rotatePivot" " -type \"double3\" -0.955401 2.077063 3.278495"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo42" 
		"rotatePivotTranslate" " -type \"double3\" -1.121661 -1.121661 -6.55699"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo42" 
		"scalePivot" " -type \"double3\" -1.447802 3.62434 -4.968187"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo42" 
		"scalePivotTranslate" " -type \"double3\" 0.492401 -1.547278 8.246682"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56" 
		"translate" " -type \"double3\" 0.790515 1.714877 0.927441"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56" 
		"translateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57" 
		"translate" " -type \"double3\" 0.981737 1.275013 3.265174"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57" 
		"translateX" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57" 
		"translateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77" 
		"translate" " -type \"double3\" 0.790515 1.714877 -1.768712"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77" 
		"translateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78" 
		"translate" " -type \"double3\" 1.036394 1.194252 -4.098118"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78" 
		"translateX" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78" 
		"translateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo84" 
		"translate" " -type \"double3\" -0.790677 1.714877 0.927441"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo84" 
		"scale" " -type \"double3\" 0.957172 0.957369 -0.957172"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo85" 
		"translate" " -type \"double3\" -0.981938 1.275013 3.265174"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo85" 
		"rotate" " -type \"double3\" -180 0 -90"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo85" 
		"scale" " -type \"double3\" 0.688329 0.68847 -0.688329"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo85" 
		"rotatePivot" " -type \"double3\" 3.69248e-07 6.56576e-07 0"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo85" 
		"rotatePivotTranslate" " -type \"double3\" -1.02582e-06 -1.02582e-06 0"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo85" 
		"scalePivot" " -type \"double3\" 5.36442e-07 9.53674e-07 0"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo85" 
		"scalePivotTranslate" " -type \"double3\" -1.67193e-07 -2.97098e-07 0"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo105" 
		"translate" " -type \"double3\" -0.790677 1.714877 -1.768712"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo105" 
		"scale" " -type \"double3\" 0.957172 0.957369 -0.957172"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo106" 
		"translate" " -type \"double3\" -1.036607 1.194252 -4.098118"
		2 "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo106" 
		"scale" " -type \"double3\" 0.593562 0.593683 -0.593562"
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27.visibility" 
		"rigwRN.placeHolderList[179]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27.translateX" 
		"rigwRN.placeHolderList[180]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27.translateY" 
		"rigwRN.placeHolderList[181]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27.translateZ" 
		"rigwRN.placeHolderList[182]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27.rotateX" 
		"rigwRN.placeHolderList[183]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27.rotateY" 
		"rigwRN.placeHolderList[184]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27.rotateZ" 
		"rigwRN.placeHolderList[185]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27.scaleX" 
		"rigwRN.placeHolderList[186]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27.scaleY" 
		"rigwRN.placeHolderList[187]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo27.scaleZ" 
		"rigwRN.placeHolderList[188]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28.visibility" 
		"rigwRN.placeHolderList[189]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28.translateX" 
		"rigwRN.placeHolderList[190]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28.translateY" 
		"rigwRN.placeHolderList[191]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28.translateZ" 
		"rigwRN.placeHolderList[192]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28.rotateX" 
		"rigwRN.placeHolderList[193]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28.rotateY" 
		"rigwRN.placeHolderList[194]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28.rotateZ" 
		"rigwRN.placeHolderList[195]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28.scaleX" 
		"rigwRN.placeHolderList[196]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28.scaleY" 
		"rigwRN.placeHolderList[197]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo28.scaleZ" 
		"rigwRN.placeHolderList[198]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29.visibility" 
		"rigwRN.placeHolderList[199]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29.translateX" 
		"rigwRN.placeHolderList[200]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29.translateY" 
		"rigwRN.placeHolderList[201]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29.translateZ" 
		"rigwRN.placeHolderList[202]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29.rotateX" 
		"rigwRN.placeHolderList[203]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29.rotateY" 
		"rigwRN.placeHolderList[204]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29.rotateZ" 
		"rigwRN.placeHolderList[205]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29.scaleX" 
		"rigwRN.placeHolderList[206]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29.scaleY" 
		"rigwRN.placeHolderList[207]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo29.scaleZ" 
		"rigwRN.placeHolderList[208]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30.visibility" 
		"rigwRN.placeHolderList[209]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30.translateX" 
		"rigwRN.placeHolderList[210]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30.translateY" 
		"rigwRN.placeHolderList[211]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30.translateZ" 
		"rigwRN.placeHolderList[212]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30.rotateX" 
		"rigwRN.placeHolderList[213]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30.rotateY" 
		"rigwRN.placeHolderList[214]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30.rotateZ" 
		"rigwRN.placeHolderList[215]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30.scaleX" 
		"rigwRN.placeHolderList[216]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30.scaleY" 
		"rigwRN.placeHolderList[217]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo30.scaleZ" 
		"rigwRN.placeHolderList[218]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56.visibility" 
		"rigwRN.placeHolderList[219]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56.translateX" 
		"rigwRN.placeHolderList[220]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56.translateY" 
		"rigwRN.placeHolderList[221]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56.translateZ" 
		"rigwRN.placeHolderList[222]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56.rotateX" 
		"rigwRN.placeHolderList[223]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56.rotateY" 
		"rigwRN.placeHolderList[224]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56.rotateZ" 
		"rigwRN.placeHolderList[225]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56.scaleX" 
		"rigwRN.placeHolderList[226]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56.scaleY" 
		"rigwRN.placeHolderList[227]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo56.scaleZ" 
		"rigwRN.placeHolderList[228]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57.visibility" 
		"rigwRN.placeHolderList[229]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57.translateX" 
		"rigwRN.placeHolderList[230]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57.translateY" 
		"rigwRN.placeHolderList[231]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57.translateZ" 
		"rigwRN.placeHolderList[232]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57.rotateX" 
		"rigwRN.placeHolderList[233]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57.rotateY" 
		"rigwRN.placeHolderList[234]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57.rotateZ" 
		"rigwRN.placeHolderList[235]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57.scaleX" 
		"rigwRN.placeHolderList[236]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57.scaleY" 
		"rigwRN.placeHolderList[237]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo57.scaleZ" 
		"rigwRN.placeHolderList[238]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77.visibility" 
		"rigwRN.placeHolderList[239]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77.translateX" 
		"rigwRN.placeHolderList[240]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77.translateY" 
		"rigwRN.placeHolderList[241]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77.translateZ" 
		"rigwRN.placeHolderList[242]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77.rotateX" 
		"rigwRN.placeHolderList[243]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77.rotateY" 
		"rigwRN.placeHolderList[244]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77.rotateZ" 
		"rigwRN.placeHolderList[245]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77.scaleX" 
		"rigwRN.placeHolderList[246]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77.scaleY" 
		"rigwRN.placeHolderList[247]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo77.scaleZ" 
		"rigwRN.placeHolderList[248]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78.visibility" 
		"rigwRN.placeHolderList[249]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78.translateX" 
		"rigwRN.placeHolderList[250]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78.translateY" 
		"rigwRN.placeHolderList[251]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78.translateZ" 
		"rigwRN.placeHolderList[252]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78.rotateX" 
		"rigwRN.placeHolderList[253]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78.rotateY" 
		"rigwRN.placeHolderList[254]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78.rotateZ" 
		"rigwRN.placeHolderList[255]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78.scaleX" 
		"rigwRN.placeHolderList[256]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78.scaleY" 
		"rigwRN.placeHolderList[257]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo78.scaleZ" 
		"rigwRN.placeHolderList[258]" ""
		"rigw:rigRN" 0
		"rigwRN" 1193
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translate" " -type \"double3\" 0.740445 0.715486 4.165678"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotate" " -type \"double3\" 112.246345 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translate" " -type \"double3\" 0.740445 1.248661 3.934626"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotate" " -type \"double3\" 87.636758 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translate" " -type \"double3\" 0.740445 2.006366 3.876286"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotate" " -type \"double3\" 27.17363 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translate" " -type \"double3\" 0.740445 2.443066 2.998664"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotate" " -type \"double3\" 27.17363 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translate" " -type \"double3\" 0.740572 2.802991 2.134471"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotate" " -type \"double3\" 28.003616 0.054517 -0.506614"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translate" " -type \"double3\" 0.740445 3.03787 1.259282"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotate" " -type \"double3\" 4.484307 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translate" " -type \"double3\" 0.740445 3.050656 0.33016"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translate" " -type \"double3\" 0.740445 3.050656 -0.642503"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translate" " -type \"double3\" 0.740445 3.050656 -1.615166"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translate" " -type \"double3\" 0.740445 2.829374 -2.525288"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotate" " -type \"double3\" -23.380861 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translate" " -type \"double3\" 0.740445 2.562667 -3.403567"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotate" " -type \"double3\" -27.443689 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translate" " -type \"double3\" 0.740445 2.112302 -4.27079"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotate" " -type \"double3\" -27.443689 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translate" " -type \"double3\" 0.740445 1.661936 -5.138014"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotate" " -type \"double3\" -27.443689 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translate" " -type \"double3\" 0.740445 1.078102 -5.613094"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotate" " -type \"double3\" -101.394566 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translate" " -type \"double3\" 0.740445 0.659831 -4.957155"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotate" " -type \"double3\" 210.809462 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translate" " -type \"double3\" 0.740445 0.360482 -4.137065"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translate" " -type \"double3\" 0.740445 0.360482 -3.159629"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translate" " -type \"double3\" 0.740445 0.360482 -2.182193"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translate" " -type \"double3\" 0.740445 0.360482 -1.204757"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translate" " -type \"double3\" 0.740445 0.360482 -0.22732"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translate" " -type \"double3\" 0.740445 0.360482 0.750116"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translate" " -type \"double3\" 0.740445 0.360482 1.727552"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translate" " -type \"double3\" 0.740445 0.360482 2.704989"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translate" " -type \"double3\" 0.740445 0.360482 3.682425"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translate" " -type \"double3\" -0.740596 2.156221 3.374191"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotate" " -type \"double3\" 152.82637 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translate" " -type \"double3\" -0.740596 1.662469 4.221696"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotate" " -type \"double3\" 148.612692 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translate" " -type \"double3\" -0.740596 0.974319 4.426617"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotate" " -type \"double3\" 74.561257 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translate" " -type \"double3\" -0.740596 0.360482 3.904841"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translate" " -type \"double3\" -0.740596 0.360482 2.927405"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translate" " -type \"double3\" -0.740596 0.360482 1.949969"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translate" " -type \"double3\" -0.740596 0.360482 0.972532"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translate" " -type \"double3\" -0.740596 0.360482 -0.00490393"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translate" " -type \"double3\" -0.740596 0.360482 -0.98234"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translate" " -type \"double3\" -0.740596 0.360482 -1.959776"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translate" " -type \"double3\" -0.740596 0.360482 -2.937213"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translate" " -type \"double3\" -0.740596 0.360482 -3.914649"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translate" " -type \"double3\" -0.740596 0.360482 -4.892085"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translate" " -type \"double3\" -0.740596 0.664251 -5.529688"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotate" " -type \"double3\" -78.605434 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translate" " -type \"double3\" -0.740596 1.474073 -5.499763"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotate" " -type \"double3\" 27.443689 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translate" " -type \"double3\" -0.740596 1.924439 -4.632539"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotate" " -type \"double3\" 27.443689 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translate" " -type \"double3\" -0.740596 2.374804 -3.765316"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotate" " -type \"double3\" 27.443689 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translate" " -type \"double3\" -0.740596 2.818561 -2.910817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotate" " -type \"double3\" 27.443689 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translate" " -type \"double3\" -0.740596 3.050656 -2.12289"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translate" " -type \"double3\" -0.740596 3.050656 -1.150227"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translate" " -type \"double3\" -0.740596 3.050656 -0.177564"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translate" " -type \"double3\" -0.740596 3.050656 0.795099"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translate" " -type \"double3\" -0.741585 2.953546 1.639622"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotate" " -type \"double3\" 161.793326 0.0545059 -179.493489"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translate" " -type \"double3\" -0.740596 2.603864 2.502186"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotate" " -type \"double3\" 152.82637 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scaleZ" " -av"
		
		2 "rigw:_mesh" "displayType" " 2"
		2 "rigw:_mesh" "displayOrder" " 2"
		5 4 "rigwRN" "|rigw:robo85.visibility" "rigwRN.placeHolderList[437]" 
		""
		5 4 "rigwRN" "|rigw:robo84.visibility" "rigwRN.placeHolderList[438]" 
		""
		5 4 "rigwRN" "|rigw:robo105.visibility" "rigwRN.placeHolderList[439]" 
		""
		5 4 "rigwRN" "|rigw:robo106.visibility" "rigwRN.placeHolderList[440]" 
		""
		5 4 "rigwRN" "|rigw:robo57.visibility" "rigwRN.placeHolderList[441]" 
		""
		5 4 "rigwRN" "|rigw:robo58.visibility" "rigwRN.placeHolderList[442]" 
		""
		5 4 "rigwRN" "|rigw:robo77.visibility" "rigwRN.placeHolderList[443]" 
		""
		5 4 "rigwRN" "|rigw:robo78.visibility" "rigwRN.placeHolderList[444]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:floaters|rigw:mesh:polySurface16.visibility" 
		"rigwRN.placeHolderList[445]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:floaters|rigw:mesh:polySurface12.visibility" 
		"rigwRN.placeHolderList[446]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:floaters|rigw:mesh:polySurface10.visibility" 
		"rigwRN.placeHolderList[447]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo4.visibility" 
		"rigwRN.placeHolderList[448]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo3.visibility" 
		"rigwRN.placeHolderList[449]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo2.visibility" 
		"rigwRN.placeHolderList[450]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo5.visibility" 
		"rigwRN.placeHolderList[451]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo11.visibility" 
		"rigwRN.placeHolderList[452]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo11.translateX" 
		"rigwRN.placeHolderList[453]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo11.translateY" 
		"rigwRN.placeHolderList[454]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo11.translateZ" 
		"rigwRN.placeHolderList[455]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo11.rotateX" 
		"rigwRN.placeHolderList[456]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo11.rotateY" 
		"rigwRN.placeHolderList[457]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo11.rotateZ" 
		"rigwRN.placeHolderList[458]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo11.scaleX" 
		"rigwRN.placeHolderList[459]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo11.scaleY" 
		"rigwRN.placeHolderList[460]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo11.scaleZ" 
		"rigwRN.placeHolderList[461]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo12.visibility" 
		"rigwRN.placeHolderList[462]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo12.translateX" 
		"rigwRN.placeHolderList[463]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo12.translateY" 
		"rigwRN.placeHolderList[464]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo12.translateZ" 
		"rigwRN.placeHolderList[465]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo12.rotateX" 
		"rigwRN.placeHolderList[466]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo12.rotateY" 
		"rigwRN.placeHolderList[467]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo12.rotateZ" 
		"rigwRN.placeHolderList[468]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo12.scaleX" 
		"rigwRN.placeHolderList[469]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo12.scaleY" 
		"rigwRN.placeHolderList[470]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo12.scaleZ" 
		"rigwRN.placeHolderList[471]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo13.visibility" 
		"rigwRN.placeHolderList[472]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo14.visibility" 
		"rigwRN.placeHolderList[473]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo15.visibility" 
		"rigwRN.placeHolderList[474]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo16.visibility" 
		"rigwRN.placeHolderList[475]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo17.visibility" 
		"rigwRN.placeHolderList[476]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo18.visibility" 
		"rigwRN.placeHolderList[477]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo19.visibility" 
		"rigwRN.placeHolderList[478]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo20.visibility" 
		"rigwRN.placeHolderList[479]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo21.visibility" 
		"rigwRN.placeHolderList[480]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo22.visibility" 
		"rigwRN.placeHolderList[481]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo32.visibility" 
		"rigwRN.placeHolderList[482]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo33.visibility" 
		"rigwRN.placeHolderList[483]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo34.visibility" 
		"rigwRN.placeHolderList[484]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo35.visibility" 
		"rigwRN.placeHolderList[485]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo36.visibility" 
		"rigwRN.placeHolderList[486]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo37.visibility" 
		"rigwRN.placeHolderList[487]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo38.visibility" 
		"rigwRN.placeHolderList[488]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo43.visibility" 
		"rigwRN.placeHolderList[489]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo44.visibility" 
		"rigwRN.placeHolderList[490]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo45.visibility" 
		"rigwRN.placeHolderList[491]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo46.visibility" 
		"rigwRN.placeHolderList[492]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo47.visibility" 
		"rigwRN.placeHolderList[493]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo48.visibility" 
		"rigwRN.placeHolderList[494]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo49.visibility" 
		"rigwRN.placeHolderList[495]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo50.visibility" 
		"rigwRN.placeHolderList[496]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo51.visibility" 
		"rigwRN.placeHolderList[497]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo52.visibility" 
		"rigwRN.placeHolderList[498]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo53.visibility" 
		"rigwRN.placeHolderList[499]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo54.visibility" 
		"rigwRN.placeHolderList[500]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo55.visibility" 
		"rigwRN.placeHolderList[501]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo112.visibility" 
		"rigwRN.placeHolderList[502]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo113.visibility" 
		"rigwRN.placeHolderList[503]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo114.visibility" 
		"rigwRN.placeHolderList[504]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo115.visibility" 
		"rigwRN.placeHolderList[505]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo116.visibility" 
		"rigwRN.placeHolderList[506]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo117.visibility" 
		"rigwRN.placeHolderList[507]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo118.visibility" 
		"rigwRN.placeHolderList[508]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo119.visibility" 
		"rigwRN.placeHolderList[509]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo120.visibility" 
		"rigwRN.placeHolderList[510]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo121.visibility" 
		"rigwRN.placeHolderList[511]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo122.visibility" 
		"rigwRN.placeHolderList[512]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo123.visibility" 
		"rigwRN.placeHolderList[513]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo124.visibility" 
		"rigwRN.placeHolderList[514]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo125.visibility" 
		"rigwRN.placeHolderList[515]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:polySurface1.visibility" 
		"rigwRN.placeHolderList[516]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:polySurface17.visibility" 
		"rigwRN.placeHolderList[517]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:polySurface13.visibility" 
		"rigwRN.placeHolderList[518]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:polySurface14.visibility" 
		"rigwRN.placeHolderList[519]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:polySurface15.visibility" 
		"rigwRN.placeHolderList[520]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo126.visibility" 
		"rigwRN.placeHolderList[521]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo127.visibility" 
		"rigwRN.placeHolderList[522]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:model|rigw:mesh:Medic|rigw:mesh:robo128.visibility" 
		"rigwRN.placeHolderList[523]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.visibility" 
		"rigwRN.placeHolderList[524]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateX" 
		"rigwRN.placeHolderList[525]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateY" 
		"rigwRN.placeHolderList[526]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateZ" 
		"rigwRN.placeHolderList[527]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateX" 
		"rigwRN.placeHolderList[528]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateY" 
		"rigwRN.placeHolderList[529]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateZ" 
		"rigwRN.placeHolderList[530]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleX" 
		"rigwRN.placeHolderList[531]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleY" 
		"rigwRN.placeHolderList[532]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleZ" 
		"rigwRN.placeHolderList[533]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.visibility" 
		"rigwRN.placeHolderList[534]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateX" 
		"rigwRN.placeHolderList[535]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateY" 
		"rigwRN.placeHolderList[536]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateZ" 
		"rigwRN.placeHolderList[537]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateX" 
		"rigwRN.placeHolderList[538]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateY" 
		"rigwRN.placeHolderList[539]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateZ" 
		"rigwRN.placeHolderList[540]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleX" 
		"rigwRN.placeHolderList[541]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleY" 
		"rigwRN.placeHolderList[542]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleZ" 
		"rigwRN.placeHolderList[543]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.visibility" 
		"rigwRN.placeHolderList[544]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateX" 
		"rigwRN.placeHolderList[545]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateY" 
		"rigwRN.placeHolderList[546]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateZ" 
		"rigwRN.placeHolderList[547]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateX" 
		"rigwRN.placeHolderList[548]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateY" 
		"rigwRN.placeHolderList[549]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateZ" 
		"rigwRN.placeHolderList[550]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleX" 
		"rigwRN.placeHolderList[551]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleY" 
		"rigwRN.placeHolderList[552]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleZ" 
		"rigwRN.placeHolderList[553]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.visibility" 
		"rigwRN.placeHolderList[554]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateX" 
		"rigwRN.placeHolderList[555]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateY" 
		"rigwRN.placeHolderList[556]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateZ" 
		"rigwRN.placeHolderList[557]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateX" 
		"rigwRN.placeHolderList[558]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateY" 
		"rigwRN.placeHolderList[559]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateZ" 
		"rigwRN.placeHolderList[560]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleX" 
		"rigwRN.placeHolderList[561]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleY" 
		"rigwRN.placeHolderList[562]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleZ" 
		"rigwRN.placeHolderList[563]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.visibility" 
		"rigwRN.placeHolderList[564]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateX" 
		"rigwRN.placeHolderList[565]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateY" 
		"rigwRN.placeHolderList[566]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateZ" 
		"rigwRN.placeHolderList[567]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateX" 
		"rigwRN.placeHolderList[568]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateY" 
		"rigwRN.placeHolderList[569]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateZ" 
		"rigwRN.placeHolderList[570]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleX" 
		"rigwRN.placeHolderList[571]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleY" 
		"rigwRN.placeHolderList[572]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleZ" 
		"rigwRN.placeHolderList[573]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.visibility" 
		"rigwRN.placeHolderList[574]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateX" 
		"rigwRN.placeHolderList[575]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateY" 
		"rigwRN.placeHolderList[576]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateZ" 
		"rigwRN.placeHolderList[577]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateX" 
		"rigwRN.placeHolderList[578]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateY" 
		"rigwRN.placeHolderList[579]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateZ" 
		"rigwRN.placeHolderList[580]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleX" 
		"rigwRN.placeHolderList[581]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleY" 
		"rigwRN.placeHolderList[582]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleZ" 
		"rigwRN.placeHolderList[583]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.visibility" 
		"rigwRN.placeHolderList[584]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateX" 
		"rigwRN.placeHolderList[585]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateY" 
		"rigwRN.placeHolderList[586]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateZ" 
		"rigwRN.placeHolderList[587]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateX" 
		"rigwRN.placeHolderList[588]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateY" 
		"rigwRN.placeHolderList[589]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateZ" 
		"rigwRN.placeHolderList[590]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleX" 
		"rigwRN.placeHolderList[591]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleY" 
		"rigwRN.placeHolderList[592]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleZ" 
		"rigwRN.placeHolderList[593]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.visibility" 
		"rigwRN.placeHolderList[594]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateX" 
		"rigwRN.placeHolderList[595]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateY" 
		"rigwRN.placeHolderList[596]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateZ" 
		"rigwRN.placeHolderList[597]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateX" 
		"rigwRN.placeHolderList[598]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateY" 
		"rigwRN.placeHolderList[599]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateZ" 
		"rigwRN.placeHolderList[600]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleX" 
		"rigwRN.placeHolderList[601]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleY" 
		"rigwRN.placeHolderList[602]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleZ" 
		"rigwRN.placeHolderList[603]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.visibility" 
		"rigwRN.placeHolderList[604]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateX" 
		"rigwRN.placeHolderList[605]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateY" 
		"rigwRN.placeHolderList[606]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateZ" 
		"rigwRN.placeHolderList[607]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateX" 
		"rigwRN.placeHolderList[608]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateY" 
		"rigwRN.placeHolderList[609]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateZ" 
		"rigwRN.placeHolderList[610]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleX" 
		"rigwRN.placeHolderList[611]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleY" 
		"rigwRN.placeHolderList[612]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleZ" 
		"rigwRN.placeHolderList[613]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.visibility" 
		"rigwRN.placeHolderList[614]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateX" 
		"rigwRN.placeHolderList[615]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateY" 
		"rigwRN.placeHolderList[616]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateZ" 
		"rigwRN.placeHolderList[617]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateX" 
		"rigwRN.placeHolderList[618]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateY" 
		"rigwRN.placeHolderList[619]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateZ" 
		"rigwRN.placeHolderList[620]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleX" 
		"rigwRN.placeHolderList[621]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleY" 
		"rigwRN.placeHolderList[622]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleZ" 
		"rigwRN.placeHolderList[623]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.visibility" 
		"rigwRN.placeHolderList[624]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateX" 
		"rigwRN.placeHolderList[625]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateY" 
		"rigwRN.placeHolderList[626]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateZ" 
		"rigwRN.placeHolderList[627]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateX" 
		"rigwRN.placeHolderList[628]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateY" 
		"rigwRN.placeHolderList[629]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateZ" 
		"rigwRN.placeHolderList[630]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleX" 
		"rigwRN.placeHolderList[631]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleY" 
		"rigwRN.placeHolderList[632]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleZ" 
		"rigwRN.placeHolderList[633]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.visibility" 
		"rigwRN.placeHolderList[634]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateX" 
		"rigwRN.placeHolderList[635]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateY" 
		"rigwRN.placeHolderList[636]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateZ" 
		"rigwRN.placeHolderList[637]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateX" 
		"rigwRN.placeHolderList[638]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateY" 
		"rigwRN.placeHolderList[639]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateZ" 
		"rigwRN.placeHolderList[640]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleX" 
		"rigwRN.placeHolderList[641]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleY" 
		"rigwRN.placeHolderList[642]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleZ" 
		"rigwRN.placeHolderList[643]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.visibility" 
		"rigwRN.placeHolderList[644]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateX" 
		"rigwRN.placeHolderList[645]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateY" 
		"rigwRN.placeHolderList[646]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateZ" 
		"rigwRN.placeHolderList[647]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateX" 
		"rigwRN.placeHolderList[648]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateY" 
		"rigwRN.placeHolderList[649]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateZ" 
		"rigwRN.placeHolderList[650]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleX" 
		"rigwRN.placeHolderList[651]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleY" 
		"rigwRN.placeHolderList[652]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleZ" 
		"rigwRN.placeHolderList[653]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.visibility" 
		"rigwRN.placeHolderList[654]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateX" 
		"rigwRN.placeHolderList[655]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateY" 
		"rigwRN.placeHolderList[656]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateZ" 
		"rigwRN.placeHolderList[657]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateX" 
		"rigwRN.placeHolderList[658]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateY" 
		"rigwRN.placeHolderList[659]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateZ" 
		"rigwRN.placeHolderList[660]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleX" 
		"rigwRN.placeHolderList[661]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleY" 
		"rigwRN.placeHolderList[662]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleZ" 
		"rigwRN.placeHolderList[663]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.visibility" 
		"rigwRN.placeHolderList[664]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateX" 
		"rigwRN.placeHolderList[665]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateY" 
		"rigwRN.placeHolderList[666]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateZ" 
		"rigwRN.placeHolderList[667]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateX" 
		"rigwRN.placeHolderList[668]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateY" 
		"rigwRN.placeHolderList[669]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateZ" 
		"rigwRN.placeHolderList[670]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleX" 
		"rigwRN.placeHolderList[671]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleY" 
		"rigwRN.placeHolderList[672]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleZ" 
		"rigwRN.placeHolderList[673]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.visibility" 
		"rigwRN.placeHolderList[674]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateX" 
		"rigwRN.placeHolderList[675]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateY" 
		"rigwRN.placeHolderList[676]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateZ" 
		"rigwRN.placeHolderList[677]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateX" 
		"rigwRN.placeHolderList[678]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateY" 
		"rigwRN.placeHolderList[679]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateZ" 
		"rigwRN.placeHolderList[680]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleX" 
		"rigwRN.placeHolderList[681]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleY" 
		"rigwRN.placeHolderList[682]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleZ" 
		"rigwRN.placeHolderList[683]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.visibility" 
		"rigwRN.placeHolderList[684]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateX" 
		"rigwRN.placeHolderList[685]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateY" 
		"rigwRN.placeHolderList[686]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateZ" 
		"rigwRN.placeHolderList[687]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateX" 
		"rigwRN.placeHolderList[688]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateY" 
		"rigwRN.placeHolderList[689]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateZ" 
		"rigwRN.placeHolderList[690]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleX" 
		"rigwRN.placeHolderList[691]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleY" 
		"rigwRN.placeHolderList[692]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleZ" 
		"rigwRN.placeHolderList[693]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.visibility" 
		"rigwRN.placeHolderList[694]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateX" 
		"rigwRN.placeHolderList[695]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateY" 
		"rigwRN.placeHolderList[696]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateZ" 
		"rigwRN.placeHolderList[697]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateX" 
		"rigwRN.placeHolderList[698]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateY" 
		"rigwRN.placeHolderList[699]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateZ" 
		"rigwRN.placeHolderList[700]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleX" 
		"rigwRN.placeHolderList[701]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleY" 
		"rigwRN.placeHolderList[702]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleZ" 
		"rigwRN.placeHolderList[703]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.visibility" 
		"rigwRN.placeHolderList[704]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateX" 
		"rigwRN.placeHolderList[705]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateY" 
		"rigwRN.placeHolderList[706]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateZ" 
		"rigwRN.placeHolderList[707]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateX" 
		"rigwRN.placeHolderList[708]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateY" 
		"rigwRN.placeHolderList[709]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateZ" 
		"rigwRN.placeHolderList[710]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleX" 
		"rigwRN.placeHolderList[711]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleY" 
		"rigwRN.placeHolderList[712]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleZ" 
		"rigwRN.placeHolderList[713]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.visibility" 
		"rigwRN.placeHolderList[714]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateX" 
		"rigwRN.placeHolderList[715]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateY" 
		"rigwRN.placeHolderList[716]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateZ" 
		"rigwRN.placeHolderList[717]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateX" 
		"rigwRN.placeHolderList[718]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateY" 
		"rigwRN.placeHolderList[719]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateZ" 
		"rigwRN.placeHolderList[720]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleX" 
		"rigwRN.placeHolderList[721]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleY" 
		"rigwRN.placeHolderList[722]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleZ" 
		"rigwRN.placeHolderList[723]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.visibility" 
		"rigwRN.placeHolderList[724]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateX" 
		"rigwRN.placeHolderList[725]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateY" 
		"rigwRN.placeHolderList[726]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateZ" 
		"rigwRN.placeHolderList[727]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateX" 
		"rigwRN.placeHolderList[728]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateY" 
		"rigwRN.placeHolderList[729]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateZ" 
		"rigwRN.placeHolderList[730]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleX" 
		"rigwRN.placeHolderList[731]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleY" 
		"rigwRN.placeHolderList[732]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleZ" 
		"rigwRN.placeHolderList[733]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.visibility" 
		"rigwRN.placeHolderList[734]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateX" 
		"rigwRN.placeHolderList[735]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateY" 
		"rigwRN.placeHolderList[736]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateZ" 
		"rigwRN.placeHolderList[737]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateX" 
		"rigwRN.placeHolderList[738]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateY" 
		"rigwRN.placeHolderList[739]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateZ" 
		"rigwRN.placeHolderList[740]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleX" 
		"rigwRN.placeHolderList[741]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleY" 
		"rigwRN.placeHolderList[742]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleZ" 
		"rigwRN.placeHolderList[743]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.visibility" 
		"rigwRN.placeHolderList[744]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateX" 
		"rigwRN.placeHolderList[745]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateY" 
		"rigwRN.placeHolderList[746]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateZ" 
		"rigwRN.placeHolderList[747]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateX" 
		"rigwRN.placeHolderList[748]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateY" 
		"rigwRN.placeHolderList[749]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateZ" 
		"rigwRN.placeHolderList[750]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleX" 
		"rigwRN.placeHolderList[751]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleY" 
		"rigwRN.placeHolderList[752]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleZ" 
		"rigwRN.placeHolderList[753]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.visibility" 
		"rigwRN.placeHolderList[754]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateX" 
		"rigwRN.placeHolderList[755]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateY" 
		"rigwRN.placeHolderList[756]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateZ" 
		"rigwRN.placeHolderList[757]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateX" 
		"rigwRN.placeHolderList[758]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateY" 
		"rigwRN.placeHolderList[759]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateZ" 
		"rigwRN.placeHolderList[760]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleX" 
		"rigwRN.placeHolderList[761]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleY" 
		"rigwRN.placeHolderList[762]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleZ" 
		"rigwRN.placeHolderList[763]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.visibility" 
		"rigwRN.placeHolderList[764]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateX" 
		"rigwRN.placeHolderList[765]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateY" 
		"rigwRN.placeHolderList[766]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateZ" 
		"rigwRN.placeHolderList[767]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateX" 
		"rigwRN.placeHolderList[768]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateY" 
		"rigwRN.placeHolderList[769]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateZ" 
		"rigwRN.placeHolderList[770]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleX" 
		"rigwRN.placeHolderList[771]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleY" 
		"rigwRN.placeHolderList[772]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleZ" 
		"rigwRN.placeHolderList[773]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.visibility" 
		"rigwRN.placeHolderList[774]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateX" 
		"rigwRN.placeHolderList[775]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateY" 
		"rigwRN.placeHolderList[776]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateZ" 
		"rigwRN.placeHolderList[777]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateX" 
		"rigwRN.placeHolderList[778]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateY" 
		"rigwRN.placeHolderList[779]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateZ" 
		"rigwRN.placeHolderList[780]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleX" 
		"rigwRN.placeHolderList[781]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleY" 
		"rigwRN.placeHolderList[782]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleZ" 
		"rigwRN.placeHolderList[783]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.visibility" 
		"rigwRN.placeHolderList[784]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateX" 
		"rigwRN.placeHolderList[785]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateY" 
		"rigwRN.placeHolderList[786]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateZ" 
		"rigwRN.placeHolderList[787]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateX" 
		"rigwRN.placeHolderList[788]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateY" 
		"rigwRN.placeHolderList[789]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateZ" 
		"rigwRN.placeHolderList[790]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleX" 
		"rigwRN.placeHolderList[791]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleY" 
		"rigwRN.placeHolderList[792]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleZ" 
		"rigwRN.placeHolderList[793]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.visibility" 
		"rigwRN.placeHolderList[794]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateX" 
		"rigwRN.placeHolderList[795]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateY" 
		"rigwRN.placeHolderList[796]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateZ" 
		"rigwRN.placeHolderList[797]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateX" 
		"rigwRN.placeHolderList[798]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateY" 
		"rigwRN.placeHolderList[799]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateZ" 
		"rigwRN.placeHolderList[800]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleX" 
		"rigwRN.placeHolderList[801]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleY" 
		"rigwRN.placeHolderList[802]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleZ" 
		"rigwRN.placeHolderList[803]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.visibility" 
		"rigwRN.placeHolderList[804]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateX" 
		"rigwRN.placeHolderList[805]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateY" 
		"rigwRN.placeHolderList[806]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateZ" 
		"rigwRN.placeHolderList[807]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateX" 
		"rigwRN.placeHolderList[808]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateY" 
		"rigwRN.placeHolderList[809]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateZ" 
		"rigwRN.placeHolderList[810]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleX" 
		"rigwRN.placeHolderList[811]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleY" 
		"rigwRN.placeHolderList[812]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleZ" 
		"rigwRN.placeHolderList[813]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.visibility" 
		"rigwRN.placeHolderList[814]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateX" 
		"rigwRN.placeHolderList[815]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateY" 
		"rigwRN.placeHolderList[816]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateZ" 
		"rigwRN.placeHolderList[817]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateX" 
		"rigwRN.placeHolderList[818]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateY" 
		"rigwRN.placeHolderList[819]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateZ" 
		"rigwRN.placeHolderList[820]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleX" 
		"rigwRN.placeHolderList[821]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleY" 
		"rigwRN.placeHolderList[822]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleZ" 
		"rigwRN.placeHolderList[823]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.visibility" 
		"rigwRN.placeHolderList[824]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateX" 
		"rigwRN.placeHolderList[825]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateY" 
		"rigwRN.placeHolderList[826]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateZ" 
		"rigwRN.placeHolderList[827]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateX" 
		"rigwRN.placeHolderList[828]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateY" 
		"rigwRN.placeHolderList[829]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateZ" 
		"rigwRN.placeHolderList[830]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleX" 
		"rigwRN.placeHolderList[831]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleY" 
		"rigwRN.placeHolderList[832]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleZ" 
		"rigwRN.placeHolderList[833]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.visibility" 
		"rigwRN.placeHolderList[834]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateX" 
		"rigwRN.placeHolderList[835]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateY" 
		"rigwRN.placeHolderList[836]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateZ" 
		"rigwRN.placeHolderList[837]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateX" 
		"rigwRN.placeHolderList[838]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateY" 
		"rigwRN.placeHolderList[839]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateZ" 
		"rigwRN.placeHolderList[840]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleX" 
		"rigwRN.placeHolderList[841]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleY" 
		"rigwRN.placeHolderList[842]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleZ" 
		"rigwRN.placeHolderList[843]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.visibility" 
		"rigwRN.placeHolderList[844]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateX" 
		"rigwRN.placeHolderList[845]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateY" 
		"rigwRN.placeHolderList[846]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateZ" 
		"rigwRN.placeHolderList[847]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateX" 
		"rigwRN.placeHolderList[848]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateY" 
		"rigwRN.placeHolderList[849]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateZ" 
		"rigwRN.placeHolderList[850]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleX" 
		"rigwRN.placeHolderList[851]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleY" 
		"rigwRN.placeHolderList[852]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleZ" 
		"rigwRN.placeHolderList[853]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.visibility" 
		"rigwRN.placeHolderList[854]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateX" 
		"rigwRN.placeHolderList[855]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateY" 
		"rigwRN.placeHolderList[856]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateZ" 
		"rigwRN.placeHolderList[857]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateX" 
		"rigwRN.placeHolderList[858]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateY" 
		"rigwRN.placeHolderList[859]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateZ" 
		"rigwRN.placeHolderList[860]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleX" 
		"rigwRN.placeHolderList[861]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleY" 
		"rigwRN.placeHolderList[862]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleZ" 
		"rigwRN.placeHolderList[863]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.visibility" 
		"rigwRN.placeHolderList[864]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateX" 
		"rigwRN.placeHolderList[865]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateY" 
		"rigwRN.placeHolderList[866]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateZ" 
		"rigwRN.placeHolderList[867]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateX" 
		"rigwRN.placeHolderList[868]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateY" 
		"rigwRN.placeHolderList[869]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateZ" 
		"rigwRN.placeHolderList[870]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleX" 
		"rigwRN.placeHolderList[871]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleY" 
		"rigwRN.placeHolderList[872]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleZ" 
		"rigwRN.placeHolderList[873]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.visibility" 
		"rigwRN.placeHolderList[874]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateX" 
		"rigwRN.placeHolderList[875]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateY" 
		"rigwRN.placeHolderList[876]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateZ" 
		"rigwRN.placeHolderList[877]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateX" 
		"rigwRN.placeHolderList[878]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateY" 
		"rigwRN.placeHolderList[879]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateZ" 
		"rigwRN.placeHolderList[880]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleX" 
		"rigwRN.placeHolderList[881]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleY" 
		"rigwRN.placeHolderList[882]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleZ" 
		"rigwRN.placeHolderList[883]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.visibility" 
		"rigwRN.placeHolderList[884]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateX" 
		"rigwRN.placeHolderList[885]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateY" 
		"rigwRN.placeHolderList[886]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateZ" 
		"rigwRN.placeHolderList[887]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateX" 
		"rigwRN.placeHolderList[888]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateY" 
		"rigwRN.placeHolderList[889]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateZ" 
		"rigwRN.placeHolderList[890]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleX" 
		"rigwRN.placeHolderList[891]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleY" 
		"rigwRN.placeHolderList[892]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleZ" 
		"rigwRN.placeHolderList[893]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.visibility" 
		"rigwRN.placeHolderList[894]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateX" 
		"rigwRN.placeHolderList[895]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateY" 
		"rigwRN.placeHolderList[896]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateZ" 
		"rigwRN.placeHolderList[897]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateX" 
		"rigwRN.placeHolderList[898]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateY" 
		"rigwRN.placeHolderList[899]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateZ" 
		"rigwRN.placeHolderList[900]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleX" 
		"rigwRN.placeHolderList[901]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleY" 
		"rigwRN.placeHolderList[902]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleZ" 
		"rigwRN.placeHolderList[903]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.visibility" 
		"rigwRN.placeHolderList[904]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateX" 
		"rigwRN.placeHolderList[905]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateY" 
		"rigwRN.placeHolderList[906]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateZ" 
		"rigwRN.placeHolderList[907]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateX" 
		"rigwRN.placeHolderList[908]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateY" 
		"rigwRN.placeHolderList[909]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateZ" 
		"rigwRN.placeHolderList[910]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleX" 
		"rigwRN.placeHolderList[911]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleY" 
		"rigwRN.placeHolderList[912]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleZ" 
		"rigwRN.placeHolderList[913]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.visibility" 
		"rigwRN.placeHolderList[914]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateX" 
		"rigwRN.placeHolderList[915]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateY" 
		"rigwRN.placeHolderList[916]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateZ" 
		"rigwRN.placeHolderList[917]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateX" 
		"rigwRN.placeHolderList[918]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateY" 
		"rigwRN.placeHolderList[919]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateZ" 
		"rigwRN.placeHolderList[920]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleX" 
		"rigwRN.placeHolderList[921]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleY" 
		"rigwRN.placeHolderList[922]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleZ" 
		"rigwRN.placeHolderList[923]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.visibility" 
		"rigwRN.placeHolderList[924]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateX" 
		"rigwRN.placeHolderList[925]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateY" 
		"rigwRN.placeHolderList[926]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateZ" 
		"rigwRN.placeHolderList[927]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateX" 
		"rigwRN.placeHolderList[928]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateY" 
		"rigwRN.placeHolderList[929]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateZ" 
		"rigwRN.placeHolderList[930]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleX" 
		"rigwRN.placeHolderList[931]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleY" 
		"rigwRN.placeHolderList[932]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleZ" 
		"rigwRN.placeHolderList[933]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.visibility" 
		"rigwRN.placeHolderList[934]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateX" 
		"rigwRN.placeHolderList[935]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateY" 
		"rigwRN.placeHolderList[936]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateZ" 
		"rigwRN.placeHolderList[937]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateX" 
		"rigwRN.placeHolderList[938]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateY" 
		"rigwRN.placeHolderList[939]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateZ" 
		"rigwRN.placeHolderList[940]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleX" 
		"rigwRN.placeHolderList[941]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleY" 
		"rigwRN.placeHolderList[942]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleZ" 
		"rigwRN.placeHolderList[943]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.visibility" 
		"rigwRN.placeHolderList[944]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateX" 
		"rigwRN.placeHolderList[945]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateY" 
		"rigwRN.placeHolderList[946]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateZ" 
		"rigwRN.placeHolderList[947]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateX" 
		"rigwRN.placeHolderList[948]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateY" 
		"rigwRN.placeHolderList[949]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateZ" 
		"rigwRN.placeHolderList[950]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleX" 
		"rigwRN.placeHolderList[951]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleY" 
		"rigwRN.placeHolderList[952]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleZ" 
		"rigwRN.placeHolderList[953]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.visibility" 
		"rigwRN.placeHolderList[954]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateX" 
		"rigwRN.placeHolderList[955]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateY" 
		"rigwRN.placeHolderList[956]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateZ" 
		"rigwRN.placeHolderList[957]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateX" 
		"rigwRN.placeHolderList[958]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateY" 
		"rigwRN.placeHolderList[959]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateZ" 
		"rigwRN.placeHolderList[960]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleX" 
		"rigwRN.placeHolderList[961]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleY" 
		"rigwRN.placeHolderList[962]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleZ" 
		"rigwRN.placeHolderList[963]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.visibility" 
		"rigwRN.placeHolderList[964]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateX" 
		"rigwRN.placeHolderList[965]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateY" 
		"rigwRN.placeHolderList[966]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateZ" 
		"rigwRN.placeHolderList[967]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateX" 
		"rigwRN.placeHolderList[968]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateY" 
		"rigwRN.placeHolderList[969]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateZ" 
		"rigwRN.placeHolderList[970]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleX" 
		"rigwRN.placeHolderList[971]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleY" 
		"rigwRN.placeHolderList[972]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleZ" 
		"rigwRN.placeHolderList[973]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.visibility" 
		"rigwRN.placeHolderList[974]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateX" 
		"rigwRN.placeHolderList[975]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateY" 
		"rigwRN.placeHolderList[976]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateZ" 
		"rigwRN.placeHolderList[977]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateX" 
		"rigwRN.placeHolderList[978]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateY" 
		"rigwRN.placeHolderList[979]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateZ" 
		"rigwRN.placeHolderList[980]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleX" 
		"rigwRN.placeHolderList[981]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleY" 
		"rigwRN.placeHolderList[982]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleZ" 
		"rigwRN.placeHolderList[983]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.visibility" 
		"rigwRN.placeHolderList[984]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateX" 
		"rigwRN.placeHolderList[985]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateY" 
		"rigwRN.placeHolderList[986]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateZ" 
		"rigwRN.placeHolderList[987]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateX" 
		"rigwRN.placeHolderList[988]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateY" 
		"rigwRN.placeHolderList[989]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateZ" 
		"rigwRN.placeHolderList[990]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleX" 
		"rigwRN.placeHolderList[991]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleY" 
		"rigwRN.placeHolderList[992]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleZ" 
		"rigwRN.placeHolderList[993]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.visibility" 
		"rigwRN.placeHolderList[994]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateX" 
		"rigwRN.placeHolderList[995]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateY" 
		"rigwRN.placeHolderList[996]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateZ" 
		"rigwRN.placeHolderList[997]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateX" 
		"rigwRN.placeHolderList[998]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateY" 
		"rigwRN.placeHolderList[999]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateZ" 
		"rigwRN.placeHolderList[1000]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleX" 
		"rigwRN.placeHolderList[1001]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleY" 
		"rigwRN.placeHolderList[1002]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleZ" 
		"rigwRN.placeHolderList[1003]" ""
		"rigw:rigRN" 345
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translate" " -type \"double3\" 0 0.0860172 0.131701"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"CenterBtwFeet" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotate" " -type \"double3\" 4.499496 8.917142 1.505573"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotate" " -type \"double3\" 3.690753 -7.793696 1.285723"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" -5.974857 -2.408263 36.806"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"Global" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotate" " -type \"double3\" 10.235198 -10.948626 -4.398679"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"Global" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 -10.631262"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotate" " -type \"double3\" 5.007655 5.11023 11.461924"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotate" " -type \"double3\" 14.422333 7.583613 -7.322673"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotate" " -type \"double3\" 1.236995 6.734636 -17.287047"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotate" " -type \"double3\" 2.002965 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotate" " -type \"double3\" 15.695979 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotate" " -type \"double3\" 12.875868 -17.146869 15.283508"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotate" " -type \"double3\" 15.695979 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotate" " -type \"double3\" 15.695979 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotate" " -type \"double3\" 1.445128 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotate" " -type \"double3\" 1.445128 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotate" " -type \"double3\" 4.569654 3.66747 -24.099252"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotate" " -type \"double3\" -19.225396 21.318876 -5.002626"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotate" " -type \"double3\" -10.204553 -1.602598 8.184013"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translate" " -type \"double3\" 0.031517 0.0902931 1.210447"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translate" " -type \"double3\" 0.031517 0.0902931 1.210447"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotate" " -type \"double3\" -22.028843 -16.267797 -39.607339"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" -0.609696 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0.571318 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length2" " -av -k 1 1"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateX" "rigwRN.placeHolderList[259]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateY" "rigwRN.placeHolderList[260]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateZ" "rigwRN.placeHolderList[261]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.visibility" "rigwRN.placeHolderList[262]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateX" "rigwRN.placeHolderList[263]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateY" "rigwRN.placeHolderList[264]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateZ" "rigwRN.placeHolderList[265]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleX" "rigwRN.placeHolderList[266]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleY" "rigwRN.placeHolderList[267]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleZ" "rigwRN.placeHolderList[268]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateX" 
		"rigwRN.placeHolderList[269]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateY" 
		"rigwRN.placeHolderList[270]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateZ" 
		"rigwRN.placeHolderList[271]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateX" 
		"rigwRN.placeHolderList[272]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateY" 
		"rigwRN.placeHolderList[273]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateZ" 
		"rigwRN.placeHolderList[274]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateX" 
		"rigwRN.placeHolderList[275]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateY" 
		"rigwRN.placeHolderList[276]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateZ" 
		"rigwRN.placeHolderList[277]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateX" 
		"rigwRN.placeHolderList[278]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateY" 
		"rigwRN.placeHolderList[279]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateZ" 
		"rigwRN.placeHolderList[280]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateX" 
		"rigwRN.placeHolderList[281]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateY" 
		"rigwRN.placeHolderList[282]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateZ" 
		"rigwRN.placeHolderList[283]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateX" 
		"rigwRN.placeHolderList[284]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateY" 
		"rigwRN.placeHolderList[285]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateZ" 
		"rigwRN.placeHolderList[286]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateX" 
		"rigwRN.placeHolderList[287]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateY" 
		"rigwRN.placeHolderList[288]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateZ" 
		"rigwRN.placeHolderList[289]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateX" 
		"rigwRN.placeHolderList[290]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateY" 
		"rigwRN.placeHolderList[291]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateZ" 
		"rigwRN.placeHolderList[292]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[293]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[294]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[295]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[296]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[297]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[298]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[299]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateX" 
		"rigwRN.placeHolderList[300]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateY" 
		"rigwRN.placeHolderList[301]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateZ" 
		"rigwRN.placeHolderList[302]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateX" 
		"rigwRN.placeHolderList[303]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateY" 
		"rigwRN.placeHolderList[304]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateZ" 
		"rigwRN.placeHolderList[305]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateX" 
		"rigwRN.placeHolderList[306]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateY" 
		"rigwRN.placeHolderList[307]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateZ" 
		"rigwRN.placeHolderList[308]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateX" 
		"rigwRN.placeHolderList[309]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateY" 
		"rigwRN.placeHolderList[310]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateZ" 
		"rigwRN.placeHolderList[311]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.Global" 
		"rigwRN.placeHolderList[312]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[313]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[314]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[315]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateX" 
		"rigwRN.placeHolderList[316]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateY" 
		"rigwRN.placeHolderList[317]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateZ" 
		"rigwRN.placeHolderList[318]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateX" 
		"rigwRN.placeHolderList[319]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateY" 
		"rigwRN.placeHolderList[320]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateZ" 
		"rigwRN.placeHolderList[321]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateX" 
		"rigwRN.placeHolderList[322]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateY" 
		"rigwRN.placeHolderList[323]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateZ" 
		"rigwRN.placeHolderList[324]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.Global" 
		"rigwRN.placeHolderList[325]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateX" 
		"rigwRN.placeHolderList[326]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateY" 
		"rigwRN.placeHolderList[327]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateZ" 
		"rigwRN.placeHolderList[328]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[329]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[330]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[331]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateX" 
		"rigwRN.placeHolderList[332]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateY" 
		"rigwRN.placeHolderList[333]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateZ" 
		"rigwRN.placeHolderList[334]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateX" 
		"rigwRN.placeHolderList[335]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateY" 
		"rigwRN.placeHolderList[336]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[337]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateX" 
		"rigwRN.placeHolderList[338]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateY" 
		"rigwRN.placeHolderList[339]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[340]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateX" 
		"rigwRN.placeHolderList[341]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateY" 
		"rigwRN.placeHolderList[342]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[343]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateX" 
		"rigwRN.placeHolderList[344]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateY" 
		"rigwRN.placeHolderList[345]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[346]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateX" 
		"rigwRN.placeHolderList[347]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateY" 
		"rigwRN.placeHolderList[348]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[349]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateX" 
		"rigwRN.placeHolderList[350]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateY" 
		"rigwRN.placeHolderList[351]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[352]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateX" 
		"rigwRN.placeHolderList[353]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateY" 
		"rigwRN.placeHolderList[354]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[355]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateX" 
		"rigwRN.placeHolderList[356]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateY" 
		"rigwRN.placeHolderList[357]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[358]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateX" 
		"rigwRN.placeHolderList[359]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateY" 
		"rigwRN.placeHolderList[360]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[361]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateX" 
		"rigwRN.placeHolderList[362]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateY" 
		"rigwRN.placeHolderList[363]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[364]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateX" 
		"rigwRN.placeHolderList[365]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateY" 
		"rigwRN.placeHolderList[366]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[367]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateX" 
		"rigwRN.placeHolderList[368]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateY" 
		"rigwRN.placeHolderList[369]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[370]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateX" 
		"rigwRN.placeHolderList[371]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateY" 
		"rigwRN.placeHolderList[372]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateZ" 
		"rigwRN.placeHolderList[373]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateX" 
		"rigwRN.placeHolderList[374]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateY" 
		"rigwRN.placeHolderList[375]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateZ" 
		"rigwRN.placeHolderList[376]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateX" 
		"rigwRN.placeHolderList[377]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateY" 
		"rigwRN.placeHolderList[378]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateZ" 
		"rigwRN.placeHolderList[379]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateX" 
		"rigwRN.placeHolderList[380]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateY" 
		"rigwRN.placeHolderList[381]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateZ" 
		"rigwRN.placeHolderList[382]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateX" 
		"rigwRN.placeHolderList[383]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateY" 
		"rigwRN.placeHolderList[384]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateZ" 
		"rigwRN.placeHolderList[385]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateX" 
		"rigwRN.placeHolderList[386]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateY" 
		"rigwRN.placeHolderList[387]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateZ" 
		"rigwRN.placeHolderList[388]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[389]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[390]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[391]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[392]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[393]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[394]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[395]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rollAngle" 
		"rigwRN.placeHolderList[396]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.roll" 
		"rigwRN.placeHolderList[397]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[398]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[399]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[400]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[401]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateX" 
		"rigwRN.placeHolderList[402]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateY" 
		"rigwRN.placeHolderList[403]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateZ" 
		"rigwRN.placeHolderList[404]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R.rotateX" 
		"rigwRN.placeHolderList[405]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateX" 
		"rigwRN.placeHolderList[406]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateY" 
		"rigwRN.placeHolderList[407]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateZ" 
		"rigwRN.placeHolderList[408]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.follow" 
		"rigwRN.placeHolderList[409]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[410]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[411]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[412]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[413]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[414]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[415]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[416]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rollAngle" 
		"rigwRN.placeHolderList[417]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.roll" 
		"rigwRN.placeHolderList[418]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[419]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[420]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[421]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[422]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateX" 
		"rigwRN.placeHolderList[423]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateY" 
		"rigwRN.placeHolderList[424]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateZ" 
		"rigwRN.placeHolderList[425]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L.rotateX" 
		"rigwRN.placeHolderList[426]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateX" 
		"rigwRN.placeHolderList[427]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateY" 
		"rigwRN.placeHolderList[428]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateZ" 
		"rigwRN.placeHolderList[429]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.follow" 
		"rigwRN.placeHolderList[430]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKIKBlend" 
		"rigwRN.placeHolderList[431]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.IKVis" 
		"rigwRN.placeHolderList[432]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKVis" 
		"rigwRN.placeHolderList[433]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKIKBlend" 
		"rigwRN.placeHolderList[434]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.IKVis" 
		"rigwRN.placeHolderList[435]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKVis" 
		"rigwRN.placeHolderList[436]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -sn "imrVerbosity" -ln "imrVerbosity" -dv 3 -at "short";
	addAttr -ci true -sn "imrThreads" -ln "imrThreads" -dv 1 -at "short";
	addAttr -ci true -sn "imrThreadsAdjust" -ln "imrThreadsAdjust" -dv 1 -min 0 -max 
		1 -at "bool";
	addAttr -ci true -sn "imrTaskOrder" -ln "imrTaskOrder" -dv 1 -at "short";
	addAttr -ci true -sn "imrTaskSize" -ln "imrTaskSize" -at "short";
	addAttr -ci true -sn "imrTaskAdjust" -ln "imrTaskAdjust" -dv 1 -at "short";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
	setAttr ".imrThreads" 4;
	setAttr ".imrThreadsAdjust" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 6 -ast 1 -aet 35 ";
	setAttr ".st" 6;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 10.235198130810424 2 9.4618312936176263
		 3 7.9150744179949024 4 6.7549777593834293 5 7.9150396157911889 6 9.4617964914139101
		 7 10.235198130810424;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -10.948626393713951 2 -10.739323171740722
		 3 -10.320710448634816 4 -10.006743057261881 5 -10.32070102980145 6 -10.739313752907355
		 7 -10.948626393713951;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.3986790342304323 2 -7.2083300367254317
		 3 -9.2607650953791136 4 -13.354712449161241 5 -9.6642249532766851 6 -6.8392893900556038
		 7 -4.3986790342304323;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -10.631262309169207 2 -8.2740872145172943
		 3 -4.0603079488408778 4 0.40090779067913018 5 -2.3121712537430676 6 -8.2965565548897224
		 7 -10.631262309169207;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKShoulder1_L_Global";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 0 3 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -5.9748572566316245 2 -3.6993059888401616
		 3 -2.756107127607172 4 -2.4790844696534187 5 -3.2837720019095316 6 -4.6293146292705778
		 7 -5.9748572566316245;
	setAttr -s 7 ".kit[0:6]"  3 2 2 2 2 2 2;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.4082633828197806 2 -0.70323569822339427
		 3 0.003459559943319479 4 0.21098860489519697 5 -0.3919421307955781 6 -1.4001027568076794
		 7 -2.4082633828197806;
	setAttr -s 7 ".kit[0:6]"  3 2 2 2 2 2 2;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 36.80599952590147 2 36.825351662136192
		 3 36.828794863291961 4 36.824283596908259 5 36.818584762560633 6 36.812292144231051
		 7 36.80599952590147;
	setAttr -s 7 ".kit[0:6]"  3 2 2 2 2 2 2;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTU -n "rigw:rig:FKShoulder_R_Global";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  3 2 2 2 2 2 2;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.9960036108132044e-16 3 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.7763568394002505e-15 3 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.031517026706455682 2 0.020006103247747636
		 3 0.0084951797890395874 4 0.0027397180596855637 5 0.0056174489243625758 6 0.018567237815409129
		 7 0.031517026706455682;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.090293065743133533 2 0.059583501278588184
		 3 0.028873936814042836 4 0.013519154581770162 5 0.021196545697906501 6 0.055744805720520013
		 7 0.090293065743133533;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.2104467242967614 2 0.76819347409367211
		 3 0.32594022389058314 4 0.10481359878903858 5 0.21537691133981085 6 0.71291181781828605
		 7 1.2104467242967614;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.031517026706455682 2 -0.010110740481975643
		 3 -0.051738507670406969 4 -0.072552391264622623 5 -0.062145449467514803 6 -0.015314211380529555
		 7 0.031517026706455682;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.090293065743133533 2 -0.1654963483129277
		 3 -0.4212857623689889 4 -0.54918046939701948 5 -0.48523311588300422 6 -0.19747002506993533
		 7 0.090293065743133533;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.2104467242967614 2 1.1370564501260956
		 3 1.0636661759554298 4 1.026971038870097 5 1.0453186074127634 6 1.1278826658547625
		 7 1.2104467242967614;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 12.896414942915222 3 25.792829885830443
		 4 32.24103735728805 5 29.01693362155925 6 14.508466810779625 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 7.5804348704969549 3 15.160869740993906
		 4 0.68905733401323932 5 -4.2497230239825221 6 -2.1248615119912611 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 12.50898228355266 3 25.01796456710532
		 4 31.272455708881655 5 28.145210137993487 6 14.072605068996744 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -1.8633598639506559 3 -3.7267197279013118
		 4 -4.6583996598766406 5 -4.1925596938889766 6 -2.0962798469444883 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -18.018900543815541 3 -36.037801087631081
		 4 -34.795407046162438 5 -28.582041191312484 6 -14.291020595656242 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0.15796617356686221 3 0.31593234713372442
		 4 0.3949154339171555 5 0.35542389052543999 6 0.17771194526271999 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 6 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 6 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 6 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.6907530161472706 6 3.6907530161472706
		 7 3.6907530161472706;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -7.7936959331332352 6 -7.7936959331332352
		 7 -7.7936959331332352;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.2857229145701967 6 1.2857229145701967
		 7 1.2857229145701967;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  3 2 2 2 2 2 2;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  3 2 2 2 2 2 2;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  3 2 2 2 2 2 2;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.499495654074547 2 3.3562969221613792
		 3 4.2056056798639503 4 2.8869934259511307 5 4.8473204024628203 6 4.2069588392377906
		 7 4.499495654074547;
	setAttr -s 7 ".kit[0:6]"  3 2 2 2 2 2 2;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 8.9171416070983636 2 8.9171416070983636
		 3 8.9171416070983636 4 8.9171416070983636 5 8.9171416070983636 6 8.9171416070983636
		 7 8.9171416070983636;
	setAttr -s 7 ".kit[0:6]"  3 2 2 2 2 2 2;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.5055734361092721 2 1.5055734361092721
		 3 1.5055734361092721 4 1.5055734361092721 5 1.5055734361092721 6 1.5055734361092721
		 7 1.5055734361092721;
	setAttr -s 7 ".kit[0:6]"  3 2 2 2 2 2 2;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 15.695979058802134 2 14.242148212345722
		 4 11.334442904071349 5 12.788273750527759 6 12.788273750527759 7 15.695979058802134;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.9345935583114624 0.79568833112716675 
		0.54899346828460693;
	setAttr -s 6 ".kiy[3:5]"  0.35571748018264771 0.60570621490478516 
		0.83582663536071777;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 -7.10952650370381 4 -21.328792799039419
		 5 -14.219266295335608 6 -14.219266295335608 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.47328436374664307 0.25943225622177124 
		0.13311949372291565;
	setAttr -s 6 ".kiy[3:5]"  0.88090968132019043 0.96576130390167236 
		0.991100013256073;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 1.4144043340723753 4 4.2432554347714726
		 5 2.8288511006990977 6 2.8288511006990977 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.93777328729629517 0.80361455678939819 
		0.55955028533935547;
	setAttr -s 6 ".kiy[3:5]"  -0.34724819660186768 -0.5951501727104187 
		-0.82879638671875;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.4451280095530803 2 -0.0087028369033416153
		 4 -2.9164081451777331 5 -1.4625772987213108 6 -1.4625772987213108 7 1.4451280095530803;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.9345935583114624 0.79568833112716675 
		0.54899346828460693;
	setAttr -s 6 ".kiy[3:5]"  0.35571748018264771 0.60570621490478516 
		0.83582663536071777;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 -7.109526503703778 4 -21.328792799039324
		 5 -14.219266295335546 6 -14.219266295335546 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.47328436374664307 0.25943225622177124 
		0.13311949372291565;
	setAttr -s 6 ".kiy[3:5]"  0.88090968132019043 0.96576130390167236 
		0.991100013256073;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 1.414404334072368 4 4.2432554347714513
		 5 2.8288511006990835 6 2.8288511006990835 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.93777328729629517 0.80361455678939819 
		0.55955028533935547;
	setAttr -s 6 ".kiy[3:5]"  -0.34724819660186768 -0.5951501727104187 
		-0.82879638671875;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.4451280095530803 2 -0.0087028369033416153
		 4 -2.9164081451777331 5 -1.4625772987213108 6 -1.4625772987213108 7 1.4451280095530803;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.9345935583114624 0.79568833112716675 
		0.54899346828460693;
	setAttr -s 6 ".kiy[3:5]"  0.35571748018264771 0.60570621490478516 
		0.83582663536071777;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 -7.109526503703778 4 -21.328792799039324
		 5 -14.219266295335546 6 -14.219266295335546 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.47328436374664307 0.25943225622177124 
		0.13311949372291565;
	setAttr -s 6 ".kiy[3:5]"  0.88090968132019043 0.96576130390167236 
		0.991100013256073;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 1.414404334072368 4 4.2432554347714513
		 5 2.8288511006990835 6 2.8288511006990835 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.93777328729629517 0.80361455678939819 
		0.55955028533935547;
	setAttr -s 6 ".kiy[3:5]"  -0.34724819660186768 -0.5951501727104187 
		-0.82879638671875;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 15.695979058802134 2 14.242148212345722
		 4 11.334442904071349 5 12.788273750527759 6 12.788273750527759 7 15.695979058802134;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.9345935583114624 0.79568833112716675 
		0.54899346828460693;
	setAttr -s 6 ".kiy[3:5]"  0.35571748018264771 0.60570621490478516 
		0.83582663536071777;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 -7.10952650370381 4 -21.328792799039419
		 5 -14.219266295335608 6 -14.219266295335608 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.47328436374664307 0.25943225622177124 
		0.13311949372291565;
	setAttr -s 6 ".kiy[3:5]"  0.88090968132019043 0.96576130390167236 
		0.991100013256073;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 1.4144043340723753 4 4.2432554347714726
		 5 2.8288511006990977 6 2.8288511006990977 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.93777328729629517 0.80361455678939819 
		0.55955028533935547;
	setAttr -s 6 ".kiy[3:5]"  -0.34724819660186768 -0.5951501727104187 
		-0.82879638671875;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 15.695979058802134 2 14.242148212345722
		 4 11.334442904071349 5 12.788273750527759 6 12.788273750527759 7 15.695979058802134;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.9345935583114624 0.79568833112716675 
		0.54899346828460693;
	setAttr -s 6 ".kiy[3:5]"  0.35571748018264771 0.60570621490478516 
		0.83582663536071777;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 -7.10952650370381 4 -21.328792799039419
		 5 -14.219266295335608 6 -14.219266295335608 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.47328436374664307 0.25943225622177124 
		0.13311949372291565;
	setAttr -s 6 ".kiy[3:5]"  0.88090968132019043 0.96576130390167236 
		0.991100013256073;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 1.4144043340723753 4 4.2432554347714726
		 5 2.8288511006990977 6 2.8288511006990977 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.93777328729629517 0.80361455678939819 
		0.55955028533935547;
	setAttr -s 6 ".kiy[3:5]"  -0.34724819660186768 -0.5951501727104187 
		-0.82879638671875;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 14.422333210657342 2 12.130351828368532
		 4 7.5463203036618482 5 9.838301685950654 6 9.838301685950654 7 14.422333210657342;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.85747808218002319 0.64015394449234009 
		0.38458898663520813;
	setAttr -s 6 ".kiy[3:5]"  0.51452052593231201 0.76824665069580078 
		0.9230879545211792;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 7.5836133324732389 2 0.70055298770273033
		 4 -13.065774195713567 5 -6.1827138509430579 6 -6.1827138509430579 7 7.5836133324732389;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.48523485660552979 0.2673669159412384 
		0.13741795718669891;
	setAttr -s 6 ".kiy[3:5]"  0.87438380718231201 0.96359479427337646 
		0.9905131459236145;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -7.3226729329366318 2 -5.7459185574801941
		 4 -2.5923625034630242 5 -4.1691168789194615 6 -4.1691168789194615 7 -7.3226729329366318;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.92434298992156982 0.77114641666412354 
		0.51802659034729004;
	setAttr -s 6 ".kiy[3:5]"  -0.38156256079673767 -0.63665777444839478 
		-0.85536450147628784;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1.2369946191731733 2 -2.2182736455390835
		 4 -9.1289138340481291 5 -5.6736455693358723 6 -5.6736455693358723 7 1.2369946191731733;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.74160027503967285 0.48375210165977478 
		0.26637944579124451;
	setAttr -s 6 ".kiy[3:5]"  0.67084193229675293 0.87520503997802734 
		0.96386826038360596;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 6.7346357678697979 2 0.35004674358142918
		 4 -12.419322844581432 5 -6.034733820293062 6 -6.034733820293062 7 6.7346357678697979;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.51340484619140625 0.2865842878818512 
		0.14792032539844513;
	setAttr -s 6 ".kiy[3:5]"  0.8581465482711792 0.95805507898330688 
		0.98899930715560913;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -17.287047285011258 2 -15.772982282272395
		 4 -12.744806854390362 5 -14.258871857129224 6 -14.258871857129224 7 -17.287047285011258;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.92963218688964844 0.7836230993270874 
		0.53345918655395508;
	setAttr -s 6 ".kiy[3:5]"  -0.36848872900009155 -0.62123644351959229 
		-0.84582573175430298;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 2.0029652386186796 2 0.54913439216226234
		 4 -2.3585709161121189 5 -0.90474006965570208 6 -0.90474006965570208 7 2.0029652386186796;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.9345935583114624 0.79568833112716675 
		0.54899346828460693;
	setAttr -s 6 ".kiy[3:5]"  0.35571748018264771 0.60570621490478516 
		0.83582663536071777;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 -7.1095265037038198 4 -21.328792799039448
		 5 -14.219266295335627 6 -14.219266295335627 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.47328436374664307 0.25943225622177124 
		0.13311949372291565;
	setAttr -s 6 ".kiy[3:5]"  0.88090968132019043 0.96576130390167236 
		0.991100013256073;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 1.4144043340723742 4 4.2432554347714699
		 5 2.828851100699096 6 2.828851100699096 7 0;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.93777328729629517 0.80361455678939819 
		0.55955028533935547;
	setAttr -s 6 ".kiy[3:5]"  -0.34724819660186768 -0.5951501727104187 
		-0.82879638671875;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:mesh:robo100_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667886 2 -0.74059646635667886
		 3 -0.74059646635667886 4 -0.74059646635667886 5 -0.74059646635667886 6 -0.74059646635667886
		 7 -0.74059646635667886;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo100_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.97431852244656514 2 0.74471816022079163
		 3 0.78085739357261597 4 0.97431852244656514 5 0.74471816022079163 6 0.78085739357261597
		 7 0.97431852244656514;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.074607968330383301 0.67799490690231323 
		0.074607968330383301 0.14367356896400452 0.074607968330383301 0.16979789733886719;
	setAttr -s 7 ".kiy[1:6]"  -0.99721294641494751 0.73506659269332886 
		-0.99721294641494751 -0.98962515592575073 -0.99721294641494751 0.98547887802124023;
createNode animCurveTL -n "rigw:mesh:robo100_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.4266167344947958 2 4.0626068428328539
		 3 3.7000118253302889 4 4.4266167344947958 5 4.0626068428328539 6 3.7000118253302889
		 7 4.4266167344947958;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.26147893071174622 0.091543905436992645 
		0.26147893071174622 0.09119105339050293 0.26147893071174622 0.04582725465297699;
	setAttr -s 7 ".kiy[1:6]"  -0.96520918607711792 -0.99580103158950806 
		-0.96520918607711792 -0.99583345651626587 -0.96520918607711792 0.99894934892654419;
createNode animCurveTL -n "rigw:mesh:robo101_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667886 2 -0.74059646635667886
		 3 -0.74059646635667886 4 -0.74059646635667886 5 -0.74059646635667886 6 -0.74059646635667886
		 7 -0.74059646635667886;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo101_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.6624688403975216 2 1.1973508140526119
		 3 0.88461705504037969 4 1.6624688403975216 5 1.1973508140526119 6 0.88461705504037969
		 7 1.6624688403975216;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13715896010398865 0.105986587703228 0.13715896010398865 
		0.071483060717582703 0.13715896010398865 0.042813770473003387;
	setAttr -s 7 ".kiy[1:6]"  -0.99054896831512451 -0.99436759948730469 
		-0.99054896831512451 -0.99744182825088501 -0.99054896831512451 0.99908310174942017;
createNode animCurveTL -n "rigw:mesh:robo101_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.2216964860081436 2 4.2336977692296349
		 3 3.9863185349596777 4 4.2216964860081436 5 4.2336977692296349 6 3.9863185349596777
		 7 4.2216964860081436;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.084179051220417023 0.13353903591632843 
		0.084179051220417023 0.94087588787078857 0.084179051220417023 0.1402171403169632;
	setAttr -s 7 ".kiy[1:6]"  0.99645066261291504 -0.9910435676574707 
		0.99645066261291504 0.33875155448913574 0.99645066261291504 0.99012076854705811;
createNode animCurveTL -n "rigw:mesh:robo102_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667886 2 -0.74059646635667886
		 3 -0.74059646635667886 4 -0.74059646635667886 5 -0.74059646635667886 6 -0.74059646635667886
		 7 -0.74059646635667886;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo102_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.1562213913571369 2 1.9451338734646024
		 3 1.8313925067863721 4 2.1562213913571369 5 1.9451338734646024 6 1.8313925067863721
		 7 2.1562213913571369;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15597957372665405 0.2812342643737793 
		0.15597957372665405 0.15597955882549286 0.15597957372665405 0.10208204388618469;
	setAttr -s 7 ".kiy[1:6]"  -0.98776024580001831 -0.95963919162750244 
		-0.98776024580001831 -0.98776024580001831 -0.98776024580001831 0.99477595090866089;
createNode animCurveTL -n "rigw:mesh:robo102_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.3741913489707507 2 3.7853886333795215
		 3 4.1073596048219603 4 3.3741913489707507 5 3.7853886333795215 6 4.1073596048219603
		 7 3.3741913489707507;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.080799050629138947 0.10297859460115433 
		0.080799050629138947 0.08079904317855835 0.080799050629138947 0.045417863875627518;
	setAttr -s 7 ".kiy[1:6]"  0.99673044681549072 0.99468362331390381 
		0.99673044681549072 0.99673044681549072 0.99673044681549072 -0.99896812438964844;
createNode animCurveTL -n "rigw:mesh:robo103_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667886 2 -0.74059646635667886
		 3 -0.74059646635667886 4 -0.74059646635667886 5 -0.74059646635667886 6 -0.74059646635667886
		 7 -0.74059646635667886;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo103_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.6038638383367565 2 2.3927763204442214
		 3 2.2833981565146386 4 2.6038638383367565 5 2.3927763204442214 6 2.2833981565146386
		 7 2.6038638383367565;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15597957372665405 0.29151636362075806 
		0.15597957372665405 0.15597955882549286 0.15597957372665405 0.10345713794231415;
	setAttr -s 7 ".kiy[1:6]"  -0.98776024580001831 -0.95656585693359375 
		-0.98776024580001831 -0.98776024580001831 -0.98776024580001831 0.99463391304016113;
createNode animCurveTL -n "rigw:mesh:robo103_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.502186407978328 2 2.9133836923870993
		 3 3.2368629377813183 4 2.502186407978328 5 2.9133836923870993 6 3.2368629377813183
		 7 2.502186407978328;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.080799050629138947 0.10250349342823029 
		0.080799050629138947 0.08079904317855835 0.080799050629138947 0.045324809849262238;
	setAttr -s 7 ".kiy[1:6]"  0.99673044681549072 0.99473261833190918 
		0.99673044681549072 0.99673044681549072 0.99673044681549072 -0.99897229671478271;
createNode animCurveTL -n "rigw:mesh:robo104_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74158453082067 2 -0.74072518973433021
		 3 -0.73974160006301648 4 -0.74158453082067 5 -0.74072518973433021 6 -0.73974160006301648
		 7 -0.74158453082067;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.99966782331466675 0.99956488609313965 
		0.99966782331466675 0.99966782331466675 0.99966782331466675 0.99847501516342163;
	setAttr -s 7 ".kiy[1:6]"  0.025771668180823326 0.029494853690266609 
		0.025771668180823326 0.025771670043468475 0.025771668180823326 -0.055203616619110107;
createNode animCurveTL -n "rigw:mesh:robo104_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.9535464118303563 2 2.8091318627851467
		 3 2.6682093528066173 4 2.9535464118303563 5 2.8091318627851467 6 2.6682093528066173
		 7 2.9535464118303563;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.22490371763706207 0.23018485307693481 
		0.22490371763706207 0.22490368783473969 0.22490371763706207 0.11603182554244995;
	setAttr -s 7 ".kiy[1:6]"  -0.9743809700012207 -0.97314691543579102 
		-0.9743809700012207 -0.9743809700012207 -0.9743809700012207 0.99324548244476318;
createNode animCurveTL -n "rigw:mesh:robo104_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.6396222899081851 2 2.0786950019641104
		 3 2.3543435847512679 4 1.6396222899081851 5 2.0786950019641104 6 2.3543435847512679
		 7 1.6396222899081851;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.075699746608734131 0.12005230784416199 
		0.075699746608734131 0.075699731707572937 0.075699746608734131 0.046587582677602768;
	setAttr -s 7 ".kiy[1:6]"  0.99713069200515747 0.9927675724029541 
		0.99713069200515747 0.99713069200515747 0.99713069200515747 -0.99891418218612671;
createNode animCurveTL -n "rigw:mesh:robo107_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667742 2 -0.74059646635667742
		 3 -0.74059646635667742 4 -0.74059646635667742 5 -0.74059646635667742 6 -0.74059646635667742
		 7 -0.74059646635667742;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo107_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.8185610325301935 2 3.0315843479902234
		 3 2.8338672316871598 4 2.8185610325301935 5 3.0315843479902234 6 2.8338672316871598
		 7 2.8185610325301935;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15459617972373962 0.1662449836730957 
		0.15459617972373962 0.15459616482257843 0.15459617972373962 0.90877115726470947;
	setAttr -s 7 ".kiy[1:6]"  0.98797774314880371 -0.9860844612121582 
		0.98797774314880371 0.98797774314880371 0.98797774314880371 -0.41729497909545898;
createNode animCurveTL -n "rigw:mesh:robo107_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.9108168881052552 2 -2.5006191138723919
		 3 -2.3087307580908307 4 -2.9108168881052552 5 -2.5006191138723919 6 -2.3087307580908307
		 7 -2.9108168881052552;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.080994635820388794 0.17114900052547455 
		0.080994635820388794 0.080994628369808197 0.080994635820388794 0.055278409272432327;
	setAttr -s 7 ".kiy[1:6]"  0.99671453237533569 0.98524510860443115 
		0.99671453237533569 0.99671453237533569 0.99671453237533569 -0.9984709620475769;
createNode animCurveTL -n "rigw:mesh:robo108_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667742 2 -0.74059646635667742
		 3 -0.74059646635667742 4 -0.74059646635667742 5 -0.74059646635667742 6 -0.74059646635667742
		 7 -0.74059646635667742;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo108_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.0506563860218527 2 3.0506563860218527
		 3 3.0506563860218527 4 3.0506563860218527 5 3.0506563860218527 6 3.0506563860218527
		 7 3.0506563860218527;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo108_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.1228898597487844 2 -1.6606765465316304
		 3 -1.4127039637184258 4 -2.1228898597487844 5 -1.6606765465316304 6 -1.4127039637184258
		 7 -2.1228898597487844;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.13322518765926361 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.046884451061487198;
	setAttr -s 7 ".kiy[1:6]"  0.99740970134735107 0.99108582735061646 
		0.99740970134735107 0.99740970134735107 0.99740970134735107 -0.99890029430389404;
createNode animCurveTL -n "rigw:mesh:robo109_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667764 2 -0.74059646635667764
		 3 -0.74059646635667764 4 -0.74059646635667764 5 -0.74059646635667764 6 -0.74059646635667764
		 7 -0.74059646635667764;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo109_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.0506563860218527 2 3.0506563860218527
		 3 3.0506563860218527 4 3.0506563860218527 5 3.0506563860218527 6 3.0506563860218527
		 7 3.0506563860218527;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo109_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.1502270169957836 2 -0.68801370377862958
		 3 -0.4400411209654248 4 -1.1502270169957836 5 -0.68801370377862958 6 -0.4400411209654248
		 7 -1.1502270169957836;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.13322518765926361 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.046884451061487198;
	setAttr -s 7 ".kiy[1:6]"  0.99740970134735107 0.99108582735061646 
		0.99740970134735107 0.99740970134735107 0.99740970134735107 -0.99890029430389404;
createNode animCurveTL -n "rigw:mesh:robo110_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667764 2 -0.74059646635667764
		 3 -0.74059646635667764 4 -0.74059646635667764 5 -0.74059646635667764 6 -0.74059646635667764
		 7 -0.74059646635667764;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo110_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.0506563860218523 2 3.0506563860218523
		 3 3.0506563860218523 4 3.0506563860218523 5 3.0506563860218523 6 3.0506563860218523
		 7 3.0506563860218523;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo110_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.17756417424278401 2 0.28464913897437027
		 3 0.53262172178757505 4 -0.17756417424278401 5 0.28464913897437027 6 0.53262172178757505
		 7 -0.17756417424278401;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.13322518765926361 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.046884451061487198;
	setAttr -s 7 ".kiy[1:6]"  0.99740970134735107 0.99108582735061646 
		0.99740970134735107 0.99740970134735107 0.99740970134735107 -0.99890029430389404;
createNode animCurveTL -n "rigw:mesh:robo111_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667797 2 -0.74059646635667797
		 3 -0.74059646635667797 4 -0.74059646635667797 5 -0.74059646635667797 6 -0.74059646635667797
		 7 -0.74059646635667797;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo111_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.0506563860218523 2 3.0506563860218523
		 3 3.027679427898561 4 3.0506563860218523 5 3.0506563860218523 6 3.027679427898561
		 7 3.0506563860218523;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo111_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.79509866851021538 2 1.2573119817273697
		 3 1.4763959487200526 4 0.79509866851021538 5 1.2573119817273697 6 1.4763959487200526
		 7 0.79509866851021538;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.15041759610176086 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.048867806792259216;
	setAttr -s 7 ".kiy[1:6]"  0.99740970134735107 0.98862254619598389 
		0.99740970134735107 0.99740970134735107 0.99740970134735107 -0.99880522489547729;
createNode animCurveTL -n "rigw:mesh:robo86_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667709 2 -0.74059646635667709
		 3 -0.74059646635667709 4 -0.74059646635667709 5 -0.74059646635667709 6 -0.74059646635667709
		 7 -0.74059646635667709;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo86_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.3748038922897212 2 2.5878272077497511
		 3 2.6292298746413127 4 2.3748038922897212 5 2.5878272077497511 6 2.6292298746413127
		 7 2.3748038922897212;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15459617972373962 0.62711483240127563 
		0.15459617972373962 0.15459616482257843 0.15459617972373962 0.12990373373031616;
	setAttr -s 7 ".kiy[1:6]"  0.98797774314880371 0.77892684936523438 
		0.98797774314880371 0.98797774314880371 0.98797774314880371 -0.99152654409408569;
createNode animCurveTL -n "rigw:mesh:robo86_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.7653157998017872 2 -3.3551180255689244
		 3 -3.2753930350459499 4 -3.7653157998017872 5 -3.3551180255689244 6 -3.2753930350459499
		 7 -3.7653157998017872;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.080994635820388794 0.38574495911598206 
		0.080994635820388794 0.080994628369808197 0.080994635820388794 0.067881003022193909;
	setAttr -s 7 ".kiy[1:6]"  0.99671453237533569 0.92260545492172241 
		0.99671453237533569 0.99671453237533569 0.99671453237533569 -0.99769347906112671;
createNode animCurveTL -n "rigw:mesh:robo87_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667709 2 -0.74059646635667709
		 3 -0.74059646635667709 4 -0.74059646635667709 5 -0.74059646635667709 6 -0.74059646635667709
		 7 -0.74059646635667709;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo87_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.9244386107160751 2 2.1374619261761052
		 3 2.1788645930676669 4 1.9244386107160751 5 2.1374619261761052 6 2.1788645930676669
		 7 1.9244386107160751;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15459617972373962 0.62711483240127563 
		0.15459617972373962 0.15459616482257843 0.15459617972373962 0.12990373373031616;
	setAttr -s 7 ".kiy[1:6]"  0.98797774314880371 0.77892684936523438 
		0.98797774314880371 0.98797774314880371 0.98797774314880371 -0.99152654409408569;
createNode animCurveTL -n "rigw:mesh:robo87_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.6325393508685693 2 -4.2223415766357055
		 3 -4.1426165861127311 4 -4.6325393508685693 5 -4.2223415766357055 6 -4.1426165861127311
		 7 -4.6325393508685693;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.080994635820388794 0.38574495911598206 
		0.080994635820388794 0.080994628369808197 0.080994635820388794 0.067881003022193909;
	setAttr -s 7 ".kiy[1:6]"  0.99671453237533569 0.92260545492172241 
		0.99671453237533569 0.99671453237533569 0.99671453237533569 -0.99769347906112671;
createNode animCurveTL -n "rigw:mesh:robo88_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667697 2 -0.74059646635667697
		 3 -0.74059646635667697 4 -0.74059646635667697 5 -0.74059646635667697 6 -0.74059646635667697
		 7 -0.74059646635667697;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo88_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.4740733291424293 2 1.6870966446024591
		 3 1.7284993114940213 4 1.4740733291424293 5 1.6870966446024591 6 1.7284993114940213
		 7 1.4740733291424293;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15459617972373962 0.62711483240127563 
		0.15459617972373962 0.15459616482257843 0.15459617972373962 0.12990373373031616;
	setAttr -s 7 ".kiy[1:6]"  0.98797774314880371 0.77892684936523438 
		0.98797774314880371 0.98797774314880371 0.98797774314880371 -0.99152654409408569;
createNode animCurveTL -n "rigw:mesh:robo88_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -5.4997629019353509 2 -5.0895651277024871
		 3 -5.0098401371795127 4 -5.4997629019353509 5 -5.0895651277024871 6 -5.0098401371795127
		 7 -5.4997629019353509;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.080994635820388794 0.38574495911598206 
		0.080994635820388794 0.080994628369808197 0.080994635820388794 0.067881003022193909;
	setAttr -s 7 ".kiy[1:6]"  0.99671453237533569 0.92260545492172241 
		0.99671453237533569 0.99671453237533569 0.99671453237533569 -0.99769347906112671;
createNode animCurveTL -n "rigw:mesh:robo89_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667697 2 -0.74059646635667697
		 3 -0.74059646635667697 4 -0.74059646635667697 5 -0.74059646635667697 6 -0.74059646635667697
		 7 -0.74059646635667697;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo89_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.664250908213459 2 1.0877766657687065
		 3 1.1134687850358176 4 0.664250908213459 5 1.0877766657687065 6 1.1134687850358176
		 7 0.664250908213459;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.07336851954460144 0.79203689098358154 
		0.07336851954460144 0.078461751341819763 0.07336851954460144 0.073999591171741486;
	setAttr -s 7 ".kiy[1:6]"  0.99730491638183594 0.61047321557998657 
		0.99730491638183594 0.99691718816757202 0.99730491638183594 -0.99725830554962158;
createNode animCurveTL -n "rigw:mesh:robo89_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -5.5296883155265162 2 -5.4558193738214094
		 3 -5.3663525760165207 4 -5.5296883155265162 5 -5.4558193738214094 6 -5.3663525760165207
		 7 -5.5296883155265162;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.34289842844009399 0.34913262724876404 
		0.34289842844009399 0.41131165623664856 0.34289842844009399 0.19995716214179993;
	setAttr -s 7 ".kiy[1:6]"  -0.93937242031097412 0.93707334995269775 
		-0.93937242031097412 0.91149479150772095 -0.93937242031097412 -0.9798046350479126;
createNode animCurveTL -n "rigw:mesh:robo90_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667697 2 -0.74059646635667697
		 3 -0.74059646635667697 4 -0.74059646635667697 5 -0.74059646635667697 6 -0.74059646635667697
		 7 -0.74059646635667697;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo90_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958693 2 0.67263408511374156
		 3 0.82037398621589952 4 0.36048212035958693 5 0.67263408511374156 6 0.82037398621589952
		 7 0.36048212035958693;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 2 2;
	setAttr -s 7 ".kix[1:6]"  0.10618189722299576 0.22008942067623138 
		0.10618189722299576 0.10618189722299576 0.22008942067623138 0.072291165590286255;
	setAttr -s 7 ".kiy[1:6]"  0.99434667825698853 0.97547972202301025 
		0.99434667825698853 0.99434667825698853 0.97547972202301025 -0.99738359451293945;
createNode animCurveTL -n "rigw:mesh:robo90_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.8920852826847545 2 -5.212206962226861
		 3 -5.4003336199397571 4 -4.8920852826847545 5 -5.212206962226861 6 -5.4003336199397571
		 7 -4.8920852826847545;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.17446807026863098 
		0.071929976344108582 0.10356712341308594 0.071929976344108582 0.065444134175777435;
	setAttr -s 7 ".kiy[1:6]"  -0.99740970134735107 -0.98466283082962036 
		-0.99740970134735107 -0.99462246894836426 -0.99740970134735107 0.9978562593460083;
createNode animCurveTL -n "rigw:mesh:robo91_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667709 2 -0.74059646635667709
		 3 -0.74059646635667709 4 -0.74059646635667709 5 -0.74059646635667709 6 -0.74059646635667709
		 7 -0.74059646635667709;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo91_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958671 2 0.36048212035958671
		 3 0.36048212035958671 4 0.36048212035958671 5 0.36048212035958671 6 0.36048212035958671
		 7 0.36048212035958671;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo91_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.9146490117670689 2 -4.3768623249842227
		 3 -4.7418804501186731 4 -3.9146490117670689 5 -4.3768623249842227 6 -4.7418804501186731
		 7 -3.9146490117670689;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.090941250324249268 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.040262375026941299;
	setAttr -s 7 ".kiy[1:6]"  -0.99740970134735107 -0.99585622549057007 
		-0.99740970134735107 -0.99740970134735107 -0.99740970134735107 0.99918913841247559;
createNode animCurveTL -n "rigw:mesh:robo92_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667742 2 -0.74059646635667742
		 3 -0.74059646635667742 4 -0.74059646635667742 5 -0.74059646635667742 6 -0.74059646635667742
		 7 -0.74059646635667742;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo92_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.3604821203595866 2 0.3604821203595866
		 3 0.3604821203595866 4 0.3604821203595866 5 0.3604821203595866 6 0.3604821203595866
		 7 0.3604821203595866;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo92_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.9372127408493891 2 -3.3994260540665437
		 3 -3.7644441792009946 4 -2.9372127408493891 5 -3.3994260540665437 6 -3.7644441792009946
		 7 -2.9372127408493891;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.090941250324249268 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.040262375026941299;
	setAttr -s 7 ".kiy[1:6]"  -0.99740970134735107 -0.99585622549057007 
		-0.99740970134735107 -0.99740970134735107 -0.99740970134735107 0.99918913841247559;
createNode animCurveTL -n "rigw:mesh:robo93_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667764 2 -0.74059646635667764
		 3 -0.74059646635667764 4 -0.74059646635667764 5 -0.74059646635667764 6 -0.74059646635667764
		 7 -0.74059646635667764;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo93_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958638 2 0.36048212035958638
		 3 0.36048212035958638 4 0.36048212035958638 5 0.36048212035958638 6 0.36048212035958638
		 7 0.36048212035958638;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo93_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.9597764699317013 2 -2.4219897831488555
		 3 -2.7870079082833059 4 -1.9597764699317013 5 -2.4219897831488555 6 -2.7870079082833059
		 7 -1.9597764699317013;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.090941250324249268 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.040262375026941299;
	setAttr -s 7 ".kiy[1:6]"  -0.99740970134735107 -0.99585622549057007 
		-0.99740970134735107 -0.99740970134735107 -0.99740970134735107 0.99918913841247559;
createNode animCurveTL -n "rigw:mesh:robo94_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667797 2 -0.74059646635667797
		 3 -0.74059646635667797 4 -0.74059646635667797 5 -0.74059646635667797 6 -0.74059646635667797
		 7 -0.74059646635667797;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo94_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958627 2 0.36048212035958627
		 3 0.36048212035958627 4 0.36048212035958627 5 0.36048212035958627 6 0.36048212035958627
		 7 0.36048212035958627;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo94_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.98234019901401859 2 -1.4445535122311728
		 3 -1.809571637365623 4 -0.98234019901401859 5 -1.4445535122311728 6 -1.809571637365623
		 7 -0.98234019901401859;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.090941250324249268 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.040262375026941299;
	setAttr -s 7 ".kiy[1:6]"  -0.99740970134735107 -0.99585622549057007 
		-0.99740970134735107 -0.99740970134735107 -0.99740970134735107 0.99918913841247559;
createNode animCurveTL -n "rigw:mesh:robo95_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667797 2 -0.74059646635667797
		 3 -0.74059646635667797 4 -0.74059646635667797 5 -0.74059646635667797 6 -0.74059646635667797
		 7 -0.74059646635667797;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo95_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958616 2 0.36048212035958616
		 3 0.36048212035958616 4 0.36048212035958616 5 0.36048212035958616 6 0.36048212035958616
		 7 0.36048212035958616;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo95_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.0049039280963345602 2 -0.46711724131348886
		 3 -0.83213536644793873 4 -0.0049039280963345602 5 -0.46711724131348886 6 -0.83213536644793873
		 7 -0.0049039280963345602;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.090941250324249268 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.040262375026941299;
	setAttr -s 7 ".kiy[1:6]"  -0.99740970134735107 -0.99585622549057007 
		-0.99740970134735107 -0.99740970134735107 -0.99740970134735107 0.99918913841247559;
createNode animCurveTL -n "rigw:mesh:robo96_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667853 2 -0.74059646635667853
		 3 -0.74059646635667853 4 -0.74059646635667853 5 -0.74059646635667853 6 -0.74059646635667853
		 7 -0.74059646635667853;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo96_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958593 2 0.36048212035958593
		 3 0.36048212035958593 4 0.36048212035958593 5 0.36048212035958593 6 0.36048212035958593
		 7 0.36048212035958593;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo96_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.97253234282135093 2 0.51031902960419651
		 3 0.14530090446974642 4 0.97253234282135093 5 0.51031902960419651 6 0.14530090446974642
		 7 0.97253234282135093;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.090941250324249268 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.040262375026941299;
	setAttr -s 7 ".kiy[1:6]"  -0.99740970134735107 -0.99585622549057007 
		-0.99740970134735107 -0.99740970134735107 -0.99740970134735107 0.99918913841247559;
createNode animCurveTL -n "rigw:mesh:robo97_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667875 2 -0.74059646635667875
		 3 -0.74059646635667875 4 -0.74059646635667875 5 -0.74059646635667875 6 -0.74059646635667875
		 7 -0.74059646635667875;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo97_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958593 2 0.36048212035958593
		 3 0.36048212035958593 4 0.36048212035958593 5 0.36048212035958593 6 0.36048212035958593
		 7 0.36048212035958593;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo97_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.9499686137390344 2 1.48775530052188
		 3 1.1227371753874298 4 1.9499686137390344 5 1.48775530052188 6 1.1227371753874298
		 7 1.9499686137390344;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.090941250324249268 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.040262375026941299;
	setAttr -s 7 ".kiy[1:6]"  -0.99740970134735107 -0.99585622549057007 
		-0.99740970134735107 -0.99740970134735107 -0.99740970134735107 0.99918913841247559;
createNode animCurveTL -n "rigw:mesh:robo98_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667886 2 -0.74059646635667886
		 3 -0.74059646635667886 4 -0.74059646635667886 5 -0.74059646635667886 6 -0.74059646635667886
		 7 -0.74059646635667886;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo98_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958566 2 0.36048212035958566
		 3 0.36048212035958566 4 0.36048212035958566 5 0.36048212035958566 6 0.36048212035958566
		 7 0.36048212035958566;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo98_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.9274048846567204 2 2.4651915714395662
		 3 2.1001734463051158 4 2.9274048846567204 5 2.4651915714395662 6 2.1001734463051158
		 7 2.9274048846567204;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.090941250324249268 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.040262375026941299;
	setAttr -s 7 ".kiy[1:6]"  -0.99740970134735107 -0.99585622549057007 
		-0.99740970134735107 -0.99740970134735107 -0.99740970134735107 0.99918913841247559;
createNode animCurveTL -n "rigw:mesh:robo99_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.74059646635667886 2 -0.74059646635667886
		 3 -0.74059646635667886 4 -0.74059646635667886 5 -0.74059646635667886 6 -0.74059646635667886
		 7 -0.74059646635667886;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo99_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958554 2 0.36048212035958554
		 3 0.36048212035958554 4 0.36048212035958554 5 0.36048212035958554 6 0.36048212035958554
		 7 0.36048212035958554;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:mesh:robo99_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.9048411555744056 2 3.4426278423572523
		 3 3.0776097172228019 4 3.9048411555744056 5 3.4426278423572523 6 3.0776097172228019
		 7 3.9048411555744056;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 1 2;
	setAttr -s 7 ".kix[1:6]"  0.071929976344108582 0.090941250324249268 
		0.071929976344108582 0.071929968893527985 0.071929976344108582 0.040262375026941299;
	setAttr -s 7 ".kiy[1:6]"  -0.99740970134735107 -0.99585622549057007 
		-0.99740970134735107 -0.99740970134735107 -0.99740970134735107 0.99918913841247559;
createNode animCurveTU -n "rigw:mesh:robo102_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo102_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 152.82637013887523 2 152.05175022773844
		 3 152.05175022773844 4 152.82637013887523 5 152.05175022773844 6 152.05175022773844
		 7 152.82637013887523;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo102_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.5304005016724986e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo102_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo102_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo102_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo102_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170483 2 -0.89381656388170483
		 3 -0.89381656388170483 4 -0.89381656388170483 5 -0.89381656388170483 6 -0.89381656388170483
		 7 -0.89381656388170483;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo101_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo101_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 148.61269226737454 2 110.82136008483911
		 3 89.538673952071619 4 148.61269226737454 5 110.82136008483911 6 89.538673952071619
		 7 148.61269226737454;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 2 2;
	setAttr -s 7 ".kix[1:6]"  0.050472553819417953 0.089378543198108673 
		0.050472553819417953 0.050472553819417953 0.089378543198108673 0.03231305256485939;
	setAttr -s 7 ".kiy[1:6]"  -0.99872547388076782 -0.99599772691726685 
		-0.99872547388076782 -0.99872547388076782 -0.99599772691726685 0.9994778037071228;
createNode animCurveTA -n "rigw:mesh:robo101_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo101_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo101_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo101_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo101_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170427 2 -0.89381656388170427
		 3 -0.89381656388170427 4 -0.89381656388170427 5 -0.89381656388170427 6 -0.89381656388170427
		 7 -0.89381656388170427;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo100_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo100_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 74.561256627239814 2 48.135755134508088
		 3 13.370169134905394 4 74.561256627239814 5 48.135755134508088 6 13.370169134905394
		 7 74.561256627239814;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 2 2;
	setAttr -s 7 ".kix[1:6]"  0.072085320949554443 0.054852630943059921 
		0.072085320949554443 0.072085320949554443 0.054852630943059921 0.031196204945445061;
	setAttr -s 7 ".kiy[1:6]"  -0.9973984956741333 -0.99849438667297363 
		-0.9973984956741333 -0.9973984956741333 -0.99849438667297363 0.99951320886611938;
createNode animCurveTA -n "rigw:mesh:robo100_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo100_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo100_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo100_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170483 2 0.89381656388170483
		 3 0.89381656388170483 4 0.89381656388170483 5 0.89381656388170483 6 0.89381656388170483
		 7 0.89381656388170483;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo100_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170483 2 -0.89381656388170483
		 3 -0.89381656388170483 4 -0.89381656388170483 5 -0.89381656388170483 6 -0.89381656388170483
		 7 -0.89381656388170483;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo99_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo99_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo99_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo99_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo99_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo99_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo99_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo98_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo98_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo98_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo98_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo98_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo98_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo98_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo97_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo97_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo97_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo97_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo97_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo97_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo97_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo96_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo96_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo96_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo96_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo96_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo96_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo96_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo95_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo95_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo95_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo95_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo95_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo95_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo95_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo94_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo94_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo94_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo94_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo94_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo94_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo94_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo93_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo93_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo93_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo93_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo93_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo93_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo93_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo92_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo92_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo92_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo92_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo92_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo92_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo92_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo91_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo91_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo91_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo91_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo91_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo91_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo91_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo90_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo90_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -24.475037132858542 3 -38.143307698495178
		 4 0 5 -24.475037132858542 6 -38.143307698495178 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 2 2;
	setAttr -s 7 ".kix[1:6]"  0.077796444296836853 0.13838498294353485 
		0.077796444296836853 0.077796444296836853 0.13838498294353485 0.050007976591587067;
	setAttr -s 7 ".kiy[1:6]"  -0.99696928262710571 -0.99037843942642212 
		-0.99696928262710571 -0.99696928262710571 -0.99037843942642212 0.99874883890151978;
createNode animCurveTA -n "rigw:mesh:robo90_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo90_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo90_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo90_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo90_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo89_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo89_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -78.605434120036833 2 -100.1515014255866
		 3 -114.91163831338778 4 -78.605434120036833 5 -100.1515014255866 6 -114.91163831338778
		 7 -78.605434120036833;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 2 2 2;
	setAttr -s 7 ".kix[1:6]"  0.088294543325901031 0.12832328677177429 
		0.088294543325901031 0.088294543325901031 0.12832328677177429 0.05253157764673233;
	setAttr -s 7 ".kiy[1:6]"  -0.9960944652557373 -0.99173235893249512 
		-0.9960944652557373 -0.9960944652557373 -0.99173235893249512 0.99861925840377808;
createNode animCurveTA -n "rigw:mesh:robo89_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.0608010033449972e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo89_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo89_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo89_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170427 2 0.89381656388170427
		 3 0.89381656388170427 4 0.89381656388170427 5 0.89381656388170427 6 0.89381656388170427
		 7 0.89381656388170427;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo89_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170427 2 -0.89381656388170427
		 3 -0.89381656388170427 4 -0.89381656388170427 5 -0.89381656388170427 6 -0.89381656388170427
		 7 -0.89381656388170427;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo88_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo88_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.443689497109453 2 27.443689497109453
		 3 27.443689497109453 4 27.443689497109453 5 27.443689497109453 6 27.443689497109453
		 7 27.443689497109453;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo88_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo88_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo88_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo88_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo88_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.8938165638817045 2 -0.8938165638817045
		 3 -0.8938165638817045 4 -0.8938165638817045 5 -0.8938165638817045 6 -0.8938165638817045
		 7 -0.8938165638817045;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo87_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo87_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.443689497109453 2 27.443689497109453
		 3 27.443689497109453 4 27.443689497109453 5 27.443689497109453 6 27.443689497109453
		 7 27.443689497109453;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo87_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo87_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo87_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo87_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo87_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170461 2 -0.89381656388170461
		 3 -0.89381656388170461 4 -0.89381656388170461 5 -0.89381656388170461 6 -0.89381656388170461
		 7 -0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo86_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo86_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.443689497109453 2 27.443689497109453
		 3 27.443689497109453 4 27.443689497109453 5 27.443689497109453 6 27.443689497109453
		 7 27.443689497109453;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo86_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo86_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo86_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo86_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo86_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170461 2 -0.89381656388170461
		 3 -0.89381656388170461 4 -0.89381656388170461 5 -0.89381656388170461 6 -0.89381656388170461
		 7 -0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo107_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo107_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.443689497109453 2 27.443689497109453
		 3 10.316375816855611 4 27.443689497109453 5 27.443689497109453 6 10.316375816855611
		 7 27.443689497109453;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo107_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo107_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo107_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo107_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo107_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170461 2 -0.89381656388170461
		 3 -0.89381656388170461 4 -0.89381656388170461 5 -0.89381656388170461 6 -0.89381656388170461
		 7 -0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo108_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo108_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo108_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo108_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo108_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo108_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo108_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170461 2 -0.89381656388170461
		 3 -0.89381656388170461 4 -0.89381656388170461 5 -0.89381656388170461 6 -0.89381656388170461
		 7 -0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo109_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo109_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo109_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo109_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo109_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo109_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo109_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170461 2 -0.89381656388170461
		 3 -0.89381656388170461 4 -0.89381656388170461 5 -0.89381656388170461 6 -0.89381656388170461
		 7 -0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo110_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo110_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo110_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo110_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo110_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo110_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo110_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170461 2 -0.89381656388170461
		 3 -0.89381656388170461 4 -0.89381656388170461 5 -0.89381656388170461 6 -0.89381656388170461
		 7 -0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo111_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo111_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 -10.759948611138537 4 0 5 0 6 -10.759948611138537
		 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo111_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo111_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo111_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo111_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo111_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170461 2 -0.89381656388170461
		 3 -0.89381656388170461 4 -0.89381656388170461 5 -0.89381656388170461 6 -0.89381656388170461
		 7 -0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo104_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo104_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 161.79332643048855 2 161.79332643048855
		 3 152.92156171716096 4 161.79332643048855 5 161.79332643048855 6 152.92156171716096
		 7 161.79332643048855;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo104_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.054505859061901765 2 0.054505859061901765
		 3 0.054505859061901765 4 0.054505859061901765 5 0.054505859061901765 6 0.054505859061901765
		 7 0.054505859061901765;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo104_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -179.49348949359208 2 -179.49348949359208
		 3 -179.49348949359208 4 -179.49348949359208 5 -179.49348949359208 6 -179.49348949359208
		 7 -179.49348949359208;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo104_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399998549429494 2 0.89399998549429494
		 3 0.89399998549429494 4 0.89399998549429494 5 0.89399998549429494 6 0.89399998549429494
		 7 0.89399998549429494;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo104_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381657775082757 2 0.89381657775082757
		 3 0.89381657775082757 4 0.89381657775082757 5 0.89381657775082757 6 0.89381657775082757
		 7 0.89381657775082757;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo104_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656451531077 2 -0.89381656451531077
		 3 -0.89381656451531077 4 -0.89381656451531077 5 -0.89381656451531077 6 -0.89381656451531077
		 7 -0.89381656451531077;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo103_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo103_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 152.82637013887523 2 152.82637013887523
		 3 152.82637013887523 4 152.82637013887523 5 152.82637013887523 6 152.82637013887523
		 7 152.82637013887523;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo103_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.5304005016724986e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:mesh:robo103_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo103_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo103_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo103_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.89381656388170483 2 -0.89381656388170483
		 3 -0.89381656388170483 4 -0.89381656388170483 5 -0.89381656388170483 6 -0.89381656388170483
		 7 -0.89381656388170483;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode materialInfo -n "rigw1:mesh:materialInfo35";
createNode shadingEngine -n "rigw1:mesh:greydark1_matgrey_darkSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode blinn -n "rigw1:mesh:greydark1_matgrey_dark";
	setAttr ".dc" 0.30000001192092896;
	setAttr ".ambc" -type "float3" 0.16238651 0.16238651 0.16238651 ;
	setAttr ".sc" -type "float3" 0.17094682 0.17094682 0.17094682 ;
	setAttr ".rfl" 0;
	setAttr ".ec" 0.3635999858379364;
	setAttr ".sro" 0.29292929172515869;
createNode phong -n "rigw1:mesh:greydark1_Toon_grey1";
	setAttr ".dc" 0;
	setAttr ".sc" -type "float3" 0 0 0 ;
	setAttr ".rfl" 0;
createNode ramp -n "rigw1:mesh:greydark1_toonredoutline5";
	setAttr ".in" 0;
	setAttr -s 2 ".cel";
	setAttr ".cel[2].ep" 0.38499999046325684;
	setAttr ".cel[3].ep" 0;
	setAttr ".cel[3].ec" -type "float3" 0.1570306 0.1570306 0.1570306 ;
createNode place2dTexture -n "rigw1:mesh:greydark1_place2dTexture30";
createNode samplerInfo -n "rigw1:mesh:greydark1_samplerInfo11";
	setAttr ".n" -type "float3" 0 0 0.99999994 ;
	setAttr ".r" -type "float3" 0 0 0.99999994 ;
createNode ramp -n "rigw1:mesh:greydark1_toonred_color5";
	setAttr ".in" 3;
	setAttr -s 6 ".cel";
	setAttr ".cel[0].ep" 0.090000003576278687;
	setAttr ".cel[0].ec" -type "float3" 0.090196081 0.094117649 0.12156863 ;
	setAttr ".cel[1].ep" 0.39500001072883606;
	setAttr ".cel[1].ec" -type "float3" 0.19632258 0.2068513 0.23140307 ;
	setAttr ".cel[2].ep" 0.67000001668930054;
	setAttr ".cel[2].ec" -type "float3" 0.29266804 0.30185398 0.31404594 ;
	setAttr ".cel[3].ep" 1;
	setAttr ".cel[3].ec" -type "float3" 0.51338977 0.52060729 0.54544902 ;
	setAttr ".cel[4].ep" 0;
	setAttr ".cel[4].ec" -type "float3" 0.045360062 0.041522488 0.05882353 ;
	setAttr ".cel[5].ep" 0.93500000238418579;
	setAttr ".cel[5].ec" -type "float3" 0.42003509 0.42595559 0.4462806 ;
createNode place2dTexture -n "rigw1:mesh:greydark1_place2dTexture31";
createNode samplerInfo -n "rigw1:mesh:greydark1_samplerInfo12";
	setAttr ".n" -type "float3" 0 0 0.99999994 ;
	setAttr ".r" -type "float3" 0 0 0.99999994 ;
createNode mib_amb_occlusion -n "rigw1:mesh:greydark1_mib_amb_occlusion14";
	setAttr ".S00" 256;
	setAttr ".S02" -type "float3" 0.025528632 0.01230296 0.078431375 ;
	setAttr ".S03" 3;
createNode animCurveTL -n "rigw:mesh:robo58_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.7404445064674029 2 0.7404445064674029
		 3 0.7404445064674029 4 0.7404445064674029 5 0.7404445064674029 6 0.7404445064674029
		 7 0.7404445064674029;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo58_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.5626668742679257 2 2.6791338936666436
		 3 2.8398447389876784 4 2.5626668742679257 5 2.6791338936666436 6 2.8398447389876784
		 7 2.5626668742679257;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.28649234771728516 0.20308943092823029 
		0.28649234771728516 1 0.28649234771728516 0.11939940601587296;
	setAttr -s 7 ".kiy[1:6]"  0.95808255672454834 0.97916024923324585 
		0.95808255672454834 0 0.95808255672454834 -0.99284625053405762;
createNode animCurveTL -n "rigw:mesh:robo58_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.4035667729321575 2 -3.1792978377420389
		 3 -2.655572016357544 4 -3.4035667729321575 5 -3.1792978377420389 6 -2.655572016357544
		 7 -3.4035667729321575;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15345093607902527 0.063518010079860687 
		0.15345093607902527 1 0.15345093607902527 0.04451940581202507;
	setAttr -s 7 ".kiy[1:6]"  0.98815619945526123 0.99798065423965454 
		0.98815619945526123 0 0.98815619945526123 -0.99900859594345093;
createNode animCurveTL -n "rigw:mesh:robo59_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740301 2 0.74044450646740301
		 3 0.74044450646740301 4 0.74044450646740301 5 0.74044450646740301 6 0.74044450646740301
		 7 0.74044450646740301;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo59_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.1123015926942799 2 2.2217962139550891
		 3 2.5470139201628292 4 2.1123015926942799 5 2.2217962139550891 6 2.5470139201628292
		 7 2.1123015926942799;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.28649234771728516 0.10196126997470856 
		0.28649234771728516 1 0.28649234771728516 0.076454624533653259;
	setAttr -s 7 ".kiy[1:6]"  0.95808255672454834 0.99478834867477417 
		0.95808255672454834 0 0.95808255672454834 -0.99707305431365967;
createNode animCurveTL -n "rigw:mesh:robo59_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.2707903239989404 2 -4.0599474412156145
		 3 -3.5337750173934355 4 -4.2707903239989404 5 -4.0599474412156145 6 -3.5337750173934355
		 7 -4.2707903239989404;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15345093607902527 0.063223846256732941 
		0.15345093607902527 1 0.15345093607902527 0.0451812744140625;
	setAttr -s 7 ".kiy[1:6]"  0.98815619945526123 0.99799931049346924 
		0.98815619945526123 0 0.98815619945526123 -0.99897879362106323;
createNode animCurveTL -n "rigw:mesh:robo60_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.7404445064674029 2 0.7404445064674029
		 3 0.7404445064674029 4 0.7404445064674029 5 0.7404445064674029 6 0.7404445064674029
		 7 0.7404445064674029;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo60_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.661936311120634 2 1.7714309323814428
		 3 2.096648638589182 4 1.661936311120634 5 1.7714309323814428 6 2.096648638589182
		 7 1.661936311120634;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.28649234771728516 0.10196126997470856 
		0.28649234771728516 1 0.28649234771728516 0.076454624533653259;
	setAttr -s 7 ".kiy[1:6]"  0.95808255672454834 0.99478834867477417 
		0.95808255672454834 0 0.95808255672454834 -0.99707305431365967;
createNode animCurveTL -n "rigw:mesh:robo60_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -5.138013875065722 2 -4.9271709922823961
		 3 -4.4009985684602171 4 -5.138013875065722 5 -4.9271709922823961 6 -4.4009985684602171
		 7 -5.138013875065722;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15345093607902527 0.063223846256732941 
		0.15345093607902527 1 0.15345093607902527 0.0451812744140625;
	setAttr -s 7 ".kiy[1:6]"  0.98815619945526123 0.99799931049346924 
		0.98815619945526123 0 0.98815619945526123 -0.99897879362106323;
createNode animCurveTL -n "rigw:mesh:robo61_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.7404445064674029 2 0.7404445064674029
		 3 0.7404445064674029 4 0.7404445064674029 5 0.7404445064674029 6 0.7404445064674029
		 7 0.7404445064674029;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo61_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.078101960372372 2 1.210803521914144
		 3 1.3924609024903485 4 1.078101960372372 5 1.210803521914144 6 1.3924609024903485
		 7 1.078101960372372;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.2359204888343811 0.18048229813575745 
		0.2359204888343811 1 0.2359204888343811 0.10544475913047791;
	setAttr -s 7 ".kiy[1:6]"  0.97177237272262573 0.98357826471328735 
		0.97177237272262573 0 0.97177237272262573 -0.99442511796951294;
createNode animCurveTL -n "rigw:mesh:robo61_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -5.6130944792597601 2 -5.3898793380225074
		 3 -4.7459792035485293 4 -5.6130944792597601 5 -5.3898793380225074 6 -4.7459792035485293
		 7 -5.6130944792597601;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.63021224737167358 0.051698628813028336 
		0.63021224737167358 1 0.63021224737167358 0.038413267582654953;
	setAttr -s 7 ".kiy[1:6]"  0.77642291784286499 0.99866271018981934 
		0.77642291784286499 0 0.77642291784286499 -0.99926191568374634;
createNode animCurveTL -n "rigw:mesh:robo62_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740267 2 0.74044450646740267
		 3 0.74044450646740267 4 0.74044450646740267 5 0.74044450646740267 6 0.74044450646740267
		 7 0.74044450646740267;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo62_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.65983057145991053 2 0.90625757395371853
		 3 1.3542590876995353 4 0.65983057145991053 5 0.90625757395371853 6 1.3542590876995353
		 7 0.65983057145991053;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.31815794110298157 0.074199408292770386 
		0.31815794110298157 1 0.31815794110298157 0.047945890575647354;
	setAttr -s 7 ".kiy[1:6]"  0.94803768396377563 0.99724346399307251 
		0.94803768396377563 0 0.94803768396377563 -0.99884986877441406;
createNode animCurveTL -n "rigw:mesh:robo62_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.9571552252598128 2 -5.070883430247795
		 3 -5.1244982020021048 4 -4.9571552252598128 5 -5.070883430247795 6 -5.1244982020021048
		 7 -4.9571552252598128;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.23143622279167175 0.52799379825592041 
		0.23143622279167175 1 0.23143622279167175 0.19535383582115173;
	setAttr -s 7 ".kiy[1:6]"  -0.97285014390945435 -0.84924811124801636 
		-0.97285014390945435 0 -0.97285014390945435 0.98073279857635498;
createNode animCurveTL -n "rigw:mesh:robo63_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.7404445064674029 2 0.7404445064674029
		 3 0.7404445064674029 4 0.7404445064674029 5 0.7404445064674029 6 0.7404445064674029
		 7 0.7404445064674029;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo63_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958671 2 0.36048212035958671
		 3 0.47140860965961762 4 0.36048212035958671 5 0.36048212035958671 6 0.47140860965961762
		 7 0.36048212035958671;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 0.28778654336929321 1 1 1 0.28778654336929321;
	setAttr -s 7 ".kiy[1:6]"  0 0.95769459009170532 0 0 0 -0.95769459009170532;
createNode animCurveTL -n "rigw:mesh:robo63_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.1370653856430479 2 -4.5458733431211016
		 3 -5.0077378336403315 4 -4.1370653856430479 5 -4.5458733431211016 6 -5.0077378336403315
		 7 -4.1370653856430479;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.071984007954597473 
		0.13652408123016357 1 0.13652408123016357 0.038256555795669556;
	setAttr -s 7 ".kiy[1:6]"  -0.99063676595687866 -0.9974057674407959 
		-0.99063676595687866 0 -0.99063676595687866 0.99926793575286865;
createNode animCurveTL -n "rigw:mesh:robo64_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740301 2 0.74044450646740301
		 3 0.74044450646740301 4 0.74044450646740301 5 0.74044450646740301 6 0.74044450646740301
		 7 0.74044450646740301;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo64_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.3604821203595866 2 0.3604821203595866
		 3 0.3604821203595866 4 0.3604821203595866 5 0.3604821203595866 6 0.3604821203595866
		 7 0.3604821203595866;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo64_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.1596291147253681 2 -3.5684370722034235
		 3 -4.0511087211235717 4 -3.1596291147253681 5 -3.5684370722034235 6 -4.0511087211235717
		 7 -3.1596291147253681;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.068895958364009857 
		0.13652408123016357 1 0.13652408123016357 0.037364907562732697;
	setAttr -s 7 ".kiy[1:6]"  -0.99063676595687866 -0.99762386083602905 
		-0.99063676595687866 0 -0.99063676595687866 0.99930167198181152;
createNode animCurveTL -n "rigw:mesh:robo65_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740323 2 0.74044450646740323
		 3 0.74044450646740323 4 0.74044450646740323 5 0.74044450646740323 6 0.74044450646740323
		 7 0.74044450646740323;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo65_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958638 2 0.36048212035958638
		 3 0.36048212035958638 4 0.36048212035958638 5 0.36048212035958638 6 0.36048212035958638
		 7 0.36048212035958638;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo65_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.1821928438076807 2 -2.5910008012857362
		 3 -3.0736724502058852 4 -2.1821928438076807 5 -2.5910008012857362 6 -3.0736724502058852
		 7 -2.1821928438076807;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.068895958364009857 
		0.13652408123016357 1 0.13652408123016357 0.037364907562732697;
	setAttr -s 7 ".kiy[1:6]"  -0.99063676595687866 -0.99762386083602905 
		-0.99063676595687866 0 -0.99063676595687866 0.99930167198181152;
createNode animCurveTL -n "rigw:mesh:robo66_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740323 2 0.74044450646740323
		 3 0.74044450646740323 4 0.74044450646740323 5 0.74044450646740323 6 0.74044450646740323
		 7 0.74044450646740323;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo66_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958627 2 0.36048212035958627
		 3 0.36048212035958627 4 0.36048212035958627 5 0.36048212035958627 6 0.36048212035958627
		 7 0.36048212035958627;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo66_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.2047565728899976 2 -1.6135645303680519
		 3 -2.0962361792882005 4 -1.2047565728899976 5 -1.6135645303680519 6 -2.0962361792882005
		 7 -1.2047565728899976;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.068895958364009857 
		0.13652408123016357 1 0.13652408123016357 0.037364907562732697;
	setAttr -s 7 ".kiy[1:6]"  -0.99063676595687866 -0.99762386083602905 
		-0.99063676595687866 0 -0.99063676595687866 0.99930167198181152;
createNode animCurveTL -n "rigw:mesh:robo67_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740323 2 0.74044450646740323
		 3 0.74044450646740323 4 0.74044450646740323 5 0.74044450646740323 6 0.74044450646740323
		 7 0.74044450646740323;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo67_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958616 2 0.36048212035958616
		 3 0.36048212035958616 4 0.36048212035958616 5 0.36048212035958616 6 0.36048212035958616
		 7 0.36048212035958616;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo67_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.22732030197231384 2 -0.63612825945036833
		 3 -1.1187999083705169 4 -0.22732030197231384 5 -0.63612825945036833 6 -1.1187999083705169
		 7 -0.22732030197231384;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.068895958364009857 
		0.13652408123016357 1 0.13652408123016357 0.037364907562732697;
	setAttr -s 7 ".kiy[1:6]"  -0.99063676595687866 -0.99762386083602905 
		-0.99063676595687866 0 -0.99063676595687866 0.99930167198181152;
createNode animCurveTL -n "rigw:mesh:robo68_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740367 2 0.74044450646740367
		 3 0.74044450646740367 4 0.74044450646740367 5 0.74044450646740367 6 0.74044450646740367
		 7 0.74044450646740367;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo68_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958593 2 0.36048212035958593
		 3 0.36048212035958593 4 0.36048212035958593 5 0.36048212035958593 6 0.36048212035958593
		 7 0.36048212035958593;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo68_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.75011596894537169 2 0.34130801146731737
		 3 -0.14136363745283123 4 0.75011596894537169 5 0.34130801146731737 6 -0.14136363745283123
		 7 0.75011596894537169;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.068895958364009857 
		0.13652408123016357 1 0.13652408123016357 0.037364907562732697;
	setAttr -s 7 ".kiy[1:6]"  -0.99063676595687866 -0.99762386083602905 
		-0.99063676595687866 0 -0.99063676595687866 0.99930167198181152;
createNode animCurveTL -n "rigw:mesh:robo69_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740378 2 0.74044450646740378
		 3 0.74044450646740378 4 0.74044450646740378 5 0.74044450646740378 6 0.74044450646740378
		 7 0.74044450646740378;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo69_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958593 2 0.36048212035958593
		 3 0.36048212035958593 4 0.36048212035958593 5 0.36048212035958593 6 0.36048212035958593
		 7 0.36048212035958593;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo69_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.7275522398630552 2 1.3187442823850009
		 3 0.83607263346485217 4 1.7275522398630552 5 1.3187442823850009 6 0.83607263346485217
		 7 1.7275522398630552;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.068895958364009857 
		0.13652408123016357 1 0.13652408123016357 0.037364907562732697;
	setAttr -s 7 ".kiy[1:6]"  -0.99063676595687866 -0.99762386083602905 
		-0.99063676595687866 0 -0.99063676595687866 0.99930167198181152;
createNode animCurveTL -n "rigw:mesh:robo70_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.7404445064674039 2 0.7404445064674039
		 3 0.7404445064674039 4 0.7404445064674039 5 0.7404445064674039 6 0.7404445064674039
		 7 0.7404445064674039;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo70_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958566 2 0.36048212035958566
		 3 0.36048212035958566 4 0.36048212035958566 5 0.36048212035958566 6 0.36048212035958566
		 7 0.36048212035958566;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo70_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.7049885107807414 2 2.296180553302686
		 3 1.8135089043825372 4 2.7049885107807414 5 2.296180553302686 6 1.8135089043825372
		 7 2.7049885107807414;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.068895958364009857 
		0.13652408123016357 1 0.13652408123016357 0.037364907562732697;
	setAttr -s 7 ".kiy[1:6]"  -0.99063676595687866 -0.99762386083602905 
		-0.99063676595687866 0 -0.99063676595687866 0.99930167198181152;
createNode animCurveTL -n "rigw:mesh:robo71_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740378 2 0.74044450646740378
		 3 0.74044450646740378 4 0.74044450646740378 5 0.74044450646740378 6 0.74044450646740378
		 7 0.74044450646740378;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo71_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.36048212035958554 2 0.36048212035958554
		 3 0.36048212035958554 4 0.36048212035958554 5 0.36048212035958554 6 0.36048212035958554
		 7 0.36048212035958554;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo71_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.6824247816984266 2 3.6824247816984266
		 3 3.2736168242203711 4 3.2736168242203711 5 2.7909451753002221 6 2.7909451753002221
		 7 3.6824247816984266;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  0.13652408123016357 1 0.068895958364009857 
		1 0.037364907562732697;
	setAttr -s 7 ".kiy[2:6]"  -0.99063676595687866 0 -0.99762386083602905 
		0 0.99930167198181152;
createNode animCurveTL -n "rigw:mesh:robo72_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740367 2 0.74044450646740367
		 3 0.74044450646740367 4 0.74044450646740367 5 0.74044450646740367 6 0.74044450646740367
		 7 0.74044450646740367;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo72_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.71548628345357668 2 0.71548628345357668
		 3 0.80734918124302379 4 0.80734918124302379 5 0.8117638276424427 6 0.8117638276424427
		 7 0.71548628345357668;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  0.25406745076179504 1 0.99134367704391479 
		1 0.32716745138168335;
	setAttr -s 7 ".kiy[2:6]"  -0.9671865701675415 0 0.13129295408725739 
		0 -0.94496643543243408;
createNode animCurveTL -n "rigw:mesh:robo72_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.1656783853750365 2 4.1656783853750365
		 3 3.9144882484767702 4 3.9144882484767702 5 3.4318367887582037 6 3.4318367887582037
		 7 4.1656783853750365;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  0.14053027331829071 1 0.068898826837539673 
		1 0.045376274734735489;
	setAttr -s 7 ".kiy[2:6]"  -0.99007642269134521 0 -0.99762362241744995 
		0 0.99897003173828125;
createNode animCurveTL -n "rigw:mesh:robo73_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740367 2 0.74044450646740367
		 3 0.74044450646740367 4 0.74044450646740367 5 0.74044450646740367 6 0.74044450646740367
		 7 0.74044450646740367;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo73_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.2486610451998057 2 0.93431775360683877
		 3 0.61618351895849921 4 1.2486610451998057 5 0.93431775360683877 6 0.61618351895849921
		 7 1.2486610451998057;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.1302894800901413 0.1042071133852005 0.1302894800901413 
		1 0.1302894800901413 0.052629750221967697;
	setAttr -s 7 ".kiy[1:6]"  -0.99147599935531616 -0.99455559253692627 
		-0.99147599935531616 0 -0.99147599935531616 0.99861407279968262;
createNode animCurveTL -n "rigw:mesh:robo73_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.9346260588921438 2 3.9537627280655472
		 3 3.7219927660879999 4 3.9346260588921438 5 3.9537627280655472 6 3.7219927660879999
		 7 3.9346260588921438;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.33317312598228455 0.14235600829124451 
		0.33317312598228455 1 0.33317312598228455 0.15487293899059296;
	setAttr -s 7 ".kiy[1:6]"  -0.94286566972732544 -0.98981547355651855 
		-0.94286566972732544 0 -0.94286566972732544 0.987934410572052;
createNode animCurveTL -n "rigw:mesh:robo74_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740378 2 0.74044450646740378
		 3 0.74044450646740378 4 0.74044450646740378 5 0.74044450646740378 6 0.74044450646740378
		 7 0.74044450646740378;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo74_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.0063660816595283 2 1.7726153509890394
		 3 1.3002718148243808 4 2.0063660816595283 5 1.7726153509890394 6 1.3002718148243808
		 7 2.0063660816595283;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.28890129923820496 0.070395037531852722 
		0.28890129923820496 1 0.28890129923820496 0.047155529260635376;
	setAttr -s 7 ".kiy[1:6]"  -0.95735889673233032 -0.99751925468444824 
		-0.95735889673233032 0 -0.95735889673233032 0.99888753890991211;
createNode animCurveTL -n "rigw:mesh:robo74_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.8762856835017296 2 4.1214537340562929
		 3 4.0541851825667008 4 3.8762856835017296 5 4.1214537340562929 6 4.0541851825667008
		 7 3.8762856835017296;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15308669209480286 0.44400382041931152 
		0.15308669209480286 1 0.15308669209480286 0.18416671454906464;
	setAttr -s 7 ".kiy[1:6]"  0.98821276426315308 -0.89602482318878174 
		0.98821276426315308 0 0.98821276426315308 -0.98289501667022705;
createNode animCurveTL -n "rigw:mesh:robo75_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.7404445064674039 2 0.7404445064674039
		 3 0.7404445064674039 4 0.7404445064674039 5 0.7404445064674039 6 0.7404445064674039
		 7 0.7404445064674039;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo75_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.4430664892671996 2 2.3107076172446535
		 3 2.0904715738025321 4 2.4430664892671996 5 2.3107076172446535 6 2.0904715738025321
		 7 2.4430664892671996;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.28890129923820496 0.14964839816093445 
		0.28890129923820496 1 0.28890129923820496 0.094117552042007446;
	setAttr -s 7 ".kiy[1:6]"  -0.95735889673233032 -0.9887392520904541 
		-0.95735889673233032 0 -0.95735889673233032 0.99556112289428711;
createNode animCurveTL -n "rigw:mesh:robo75_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.9986636627782786 2 3.343966072022321
		 3 3.772984632174408 4 2.9986636627782786 5 3.343966072022321 6 3.772984632174408
		 7 2.9986636627782786;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15308669209480286 0.077463246881961823 
		0.15308669209480286 1 0.15308669209480286 0.043008632957935333;
	setAttr -s 7 ".kiy[1:6]"  0.98821276426315308 0.99699521064758301 
		0.98821276426315308 0 0.98821276426315308 -0.99907469749450684;
createNode animCurveTL -n "rigw:mesh:robo76_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74057197342305092 2 0.73968424028248159
		 3 0.73808729196546496 4 0.74057197342305092 5 0.73968424028248159 6 0.73808729196546496
		 7 0.74057197342305092;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.99990904331207275 0.99885433912277222 
		0.99990904331207275 1 0.99990904331207275 0.99723339080810547;
	setAttr -s 7 ".kiy[1:6]"  -0.013486513867974281 -0.047853566706180573 
		-0.013486513867974281 0 -0.013486513867974281 0.074334226548671722;
createNode animCurveTL -n "rigw:mesh:robo76_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.8029906046170452 2 2.6656462300154145
		 3 2.4392240244219421 4 2.8029906046170452 5 2.6656462300154145 6 2.4392240244219421
		 7 2.8029906046170452;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.40357327461242676 0.14564773440361023 
		0.40357327461242676 1 0.40357327461242676 0.091251544654369354;
	setAttr -s 7 ".kiy[1:6]"  -0.91494733095169067 -0.98933649063110352 
		-0.91494733095169067 0 -0.91494733095169067 0.99582791328430176;
createNode animCurveTL -n "rigw:mesh:robo76_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.1344708992307391 2 2.4778196425340755
		 3 2.9036027731960008 4 2.1344708992307391 5 2.4778196425340755 6 2.9036027731960008
		 7 2.1344708992307391;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.14357466995716095 0.078048303723335266 
		0.14357466995716095 1 0.14357466995716095 0.043298255652189255;
	setAttr -s 7 ".kiy[1:6]"  0.98963946104049683 0.99694961309432983 
		0.98963946104049683 0 0.98963946104049683 -0.999062180519104;
createNode animCurveTL -n "rigw:mesh:robo79_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740301 2 0.74044450646740301
		 3 0.74044450646740301 4 0.74044450646740301 5 0.74044450646740301 6 0.74044450646740301
		 7 0.74044450646740301;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo79_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.8293738353437874 2 2.8283894259024875
		 3 2.8707992130837545 4 2.8293738353437874 5 2.8283894259024875 6 2.8707992130837545
		 7 2.8293738353437874;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.28649234771728516 0.61795169115066528 
		0.28649234771728516 1 0.28649234771728516 0.62690615653991699;
	setAttr -s 7 ".kiy[1:6]"  0.95808255672454834 0.78621602058410645 
		0.95808255672454834 0 0.95808255672454834 -0.77909475564956665;
createNode animCurveTL -n "rigw:mesh:robo79_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.5252879745111865 2 -2.2185624781366817
		 3 -1.6948941509685145 4 -2.5252879745111865 5 -2.2185624781366817 6 -1.6948941509685145
		 7 -2.5252879745111865;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.15345093607902527 0.063524946570396423 
		0.15345093607902527 1 0.15345093607902527 0.040109291672706604;
	setAttr -s 7 ".kiy[1:6]"  0.98815619945526123 0.99798023700714111 
		0.98815619945526123 0 0.98815619945526123 -0.99919527769088745;
createNode animCurveTL -n "rigw:mesh:robo80_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.7404445064674029 2 0.7404445064674029
		 3 0.7404445064674029 4 0.7404445064674029 5 0.7404445064674029 6 0.7404445064674029
		 7 0.7404445064674029;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo80_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.0506563860218527 2 3.0506563860218527
		 3 3.0506563860218527 4 3.0506563860218527 5 3.0506563860218527 6 3.0506563860218527
		 7 3.0506563860218527;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo80_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.615166065581102 2 -1.3269798414649372
		 3 -0.80159702205810923 4 -1.615166065581102 5 -1.3269798414649372 6 -0.80159702205810923
		 7 -1.615166065581102;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.063318483531475067 
		0.13652408123016357 1 0.13652408123016357 0.040937386453151703;
	setAttr -s 7 ".kiy[1:6]"  0.99063676595687866 0.9979933500289917 
		0.99063676595687866 0 0.99063676595687866 -0.99916177988052368;
createNode animCurveTL -n "rigw:mesh:robo81_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740301 2 0.74044450646740301
		 3 0.74044450646740301 4 0.74044450646740301 5 0.74044450646740301 6 0.74044450646740301
		 7 0.74044450646740301;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo81_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.0506563860218527 2 3.0506563860218527
		 3 3.0506563860218527 4 3.0506563860218527 5 3.0506563860218527 6 3.0506563860218527
		 7 3.0506563860218527;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo81_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.64250322282810091 2 -0.35431699871193628
		 3 0.17106582069489157 4 -0.64250322282810091 5 -0.35431699871193628 6 0.17106582069489157
		 7 -0.64250322282810091;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.063318483531475067 
		0.13652408123016357 1 0.13652408123016357 0.040937386453151703;
	setAttr -s 7 ".kiy[1:6]"  0.99063676595687866 0.9979933500289917 
		0.99063676595687866 0 0.99063676595687866 -0.99916177988052368;
createNode animCurveTL -n "rigw:mesh:robo82_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.7404445064674029 2 0.7404445064674029
		 3 0.7404445064674029 4 0.7404445064674029 5 0.7404445064674029 6 0.7404445064674029
		 7 0.7404445064674029;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo82_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.0506563860218523 2 3.0506563860218523
		 3 3.0506563860218523 4 3.0506563860218523 5 3.0506563860218523 6 3.0506563860218523
		 7 3.0506563860218523;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo82_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.33015961992489873 2 0.6183458440410633
		 3 1.1437286634478911 4 0.33015961992489873 5 0.6183458440410633 6 1.1437286634478911
		 7 0.33015961992489873;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.063318483531475067 
		0.13652408123016357 1 0.13652408123016357 0.040937386453151703;
	setAttr -s 7 ".kiy[1:6]"  0.99063676595687866 0.9979933500289917 
		0.99063676595687866 0 0.99063676595687866 -0.99916177988052368;
createNode animCurveTL -n "rigw:mesh:robo83_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.74044450646740301 2 0.74044450646740301
		 3 0.74044450646740301 4 0.74044450646740301 5 0.74044450646740301 6 0.74044450646740301
		 7 0.74044450646740301;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo83_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.0378702685110057 2 3.05643076256725
		 3 2.9130333193825999 4 3.0378702685110057 5 3.05643076256725 6 2.9130333193825999
		 7 3.0378702685110057;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 0.22641739249229431 1 1 1 0.25797677040100098;
	setAttr -s 7 ".kiy[1:6]"  0 -0.97403037548065186 0 0 0 0.96615105867385864;
createNode animCurveTL -n "rigw:mesh:robo83_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.2592823815650809 2 1.5797429200512974
		 3 2.0631774366333713 4 1.2592823815650809 5 1.5797429200512974 6 2.0631774366333713
		 7 1.2592823815650809;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.13652408123016357 0.068787753582000732 
		0.13652408123016357 1 0.13652408123016357 0.041429180651903152;
	setAttr -s 7 ".kiy[1:6]"  0.99063676595687866 0.99763137102127075 
		0.99063676595687866 0 0.99063676595687866 -0.99914145469665527;
createNode animCurveTU -n "rigw:mesh:robo72_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 9 1 9 9 9 9;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo72_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 112.24634522419085 2 112.24634522419085
		 3 156.34475926784529 4 156.34475926784529 5 180.52405011569016 6 180.52405011569016
		 7 112.24634522419085;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  0.12382765859365463 1 0.078742139041423798 
		1 0.027960993349552155;
	setAttr -s 7 ".kiy[2:6]"  0.99230372905731201 0 0.99689501523971558 
		0 -0.99960899353027344;
createNode animCurveTA -n "rigw:mesh:robo72_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo72_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo72_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo72_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170483 2 0.89381656388170483
		 3 0.89381656388170483 4 0.89381656388170483 5 0.89381656388170483 6 0.89381656388170483
		 7 0.89381656388170483;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo72_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170483 2 0.89381656388170483
		 3 0.89381656388170483 4 0.89381656388170483 5 0.89381656388170483 6 0.89381656388170483
		 7 0.89381656388170483;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo73_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo73_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 87.636758359963039 2 99.217445972257408
		 3 118.09493859336486 4 87.636758359963039 5 99.217445972257408 6 118.09493859336486
		 7 87.636758359963039;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.075041241943836212 0.10065740346908569 
		0.075041241943836212 1 0.075041241943836212 0.062581397593021393;
	setAttr -s 7 ".kiy[1:6]"  0.99718046188354492 0.99492114782333374 
		0.99718046188354492 0 0.99718046188354492 -0.99803978204727173;
createNode animCurveTA -n "rigw:mesh:robo73_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo73_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo73_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo73_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo73_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170427 2 0.89381656388170427
		 3 0.89381656388170427 4 0.89381656388170427 5 0.89381656388170427 6 0.89381656388170427
		 7 0.89381656388170427;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo74_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo74_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.17362986112477 2 37.403513467873204
		 3 78.885670544351527 4 27.17362986112477 5 37.403513467873204 6 78.885670544351527
		 7 27.17362986112477;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 0.045991778373718262 1 1 1 0.036907419562339783;
	setAttr -s 7 ".kiy[1:6]"  0 0.99894183874130249 0 0 0 -0.99931871891021729;
createNode animCurveTA -n "rigw:mesh:robo74_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo74_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo74_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo74_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo74_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170483 2 0.89381656388170483
		 3 0.89381656388170483 4 0.89381656388170483 5 0.89381656388170483 6 0.89381656388170483
		 7 0.89381656388170483;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo75_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo75_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.17362986112477 2 27.17362986112477
		 3 27.17362986112477 4 27.17362986112477 5 27.17362986112477 6 27.17362986112477 7 27.17362986112477;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo75_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo75_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo75_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo75_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo75_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170483 2 0.89381656388170483
		 3 0.89381656388170483 4 0.89381656388170483 5 0.89381656388170483 6 0.89381656388170483
		 7 0.89381656388170483;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo76_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo76_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 28.003615576707961 2 28.003615576707961
		 3 28.003615576707961 4 28.003615576707961 5 28.003615576707961 6 28.003615576707961
		 7 28.003615576707961;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo76_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.054517044305025741 2 0.054517044305025741
		 3 0.054517044305025741 4 0.054517044305025741 5 0.054517044305025741 6 0.054517044305025741
		 7 0.054517044305025741;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo76_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.5066144510904842 2 -0.5066144510904842
		 3 -0.5066144510904842 4 -0.5066144510904842 5 -0.5066144510904842 6 -0.5066144510904842
		 7 -0.5066144510904842;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo76_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170494 2 0.89381656388170494
		 3 0.89381656388170494 4 0.89381656388170494 5 0.89381656388170494 6 0.89381656388170494
		 7 0.89381656388170494;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo76_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo76_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170483 2 0.89381656388170483
		 3 0.89381656388170483 4 0.89381656388170483 5 0.89381656388170483 6 0.89381656388170483
		 7 0.89381656388170483;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo83_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo83_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.4843074989775751 2 16.521529375027662
		 3 24.725671316618214 4 4.4843074989775751 5 16.521529375027662 6 24.725671316618214
		 7 4.4843074989775751;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 0.2267296314239502 1 1 1 0.093937046825885773;
	setAttr -s 7 ".kiy[1:6]"  0 0.97395777702331543 0 0 0 -0.9955781102180481;
createNode animCurveTA -n "rigw:mesh:robo83_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.9012298073891429e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo83_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo83_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo83_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo83_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo82_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo82_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo82_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.9012298073891429e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo82_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo82_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo82_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo82_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo81_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo81_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo81_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.9012298073891429e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo81_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo81_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo81_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo81_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo80_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo80_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo80_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.9012298073891429e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo80_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo80_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo80_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo80_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo79_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo79_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -23.380860706639773 2 -4.6300494860004786
		 3 -4.6300494860004786 4 -11.537008524736097 5 -4.6300494860004786 6 -4.6300494860004786
		 7 -23.380860706639773;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 0.10133048892021179;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 -0.99485284090042114;
createNode animCurveTA -n "rigw:mesh:robo79_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.5802459614778258e-30 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo79_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.5311250384401291e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo79_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo79_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo79_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo58_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo58_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -27.443689497109453 2 -27.443689497109453
		 3 -14.905922785146897 4 -27.443689497109453 5 -27.443689497109453 6 -14.905922785146897
		 7 -27.443689497109453;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 0.15059135854244232 1 1 1 0.15059135854244232;
	setAttr -s 7 ".kiy[1:6]"  0 0.98859608173370361 0 0 0 -0.98859608173370361;
createNode animCurveTA -n "rigw:mesh:robo58_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.5802459614778258e-30 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo58_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.5311250384401291e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo58_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo58_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo58_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo59_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo59_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -27.443689497109453 2 -27.443689497109453
		 3 -27.443689497109453 4 -27.443689497109453 5 -27.443689497109453 6 -27.443689497109453
		 7 -27.443689497109453;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo59_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.5802459614778258e-30 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo59_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.5311250384401291e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo59_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo59_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo59_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo60_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo60_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -27.443689497109453 2 -27.443689497109453
		 3 -27.443689497109453 4 -27.443689497109453 5 -27.443689497109453 6 -27.443689497109453
		 7 -27.443689497109453;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.27319267392158508 1 0.27319267392158508 
		1 0.27319267392158508 1;
	setAttr -s 7 ".kiy[1:6]"  -0.96195930242538452 0 -0.96195930242538452 
		0 -0.96195930242538452 0;
createNode animCurveTA -n "rigw:mesh:robo60_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.5802459614778258e-30 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo60_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.5311250384401291e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo60_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo60_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo60_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo61_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo61_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -101.39456587996317 2 -84.03261768368715
		 3 -31.424954870789676 4 -101.39456587996317 5 -84.03261768368715 6 -31.424954870789676
		 7 -101.39456587996317;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 0.036279920488595963 1 1 1 0.027285391464829445;
	setAttr -s 7 ".kiy[1:6]"  0 0.99934166669845581 0 0 0 -0.99962770938873291;
createNode animCurveTA -n "rigw:mesh:robo61_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.0167092985348775e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo61_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo61_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo61_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170427 2 0.89381656388170427
		 3 0.89381656388170427 4 0.89381656388170427 5 0.89381656388170427 6 0.89381656388170427
		 7 0.89381656388170427;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo61_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170427 2 0.89381656388170427
		 3 0.89381656388170427 4 0.89381656388170427 5 0.89381656388170427 6 0.89381656388170427
		 7 0.89381656388170427;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo62_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo62_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 210.8094617618224 2 227.89311780391441
		 3 267.90574350950061 4 210.8094617618224 5 227.89311780391441 6 267.90574350950061
		 7 210.8094617618224;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  0.14525775611400604 0.047677136957645416 
		0.14525775611400604 1 0.14525775611400604 0.033431101590394974;
	setAttr -s 7 ".kiy[1:6]"  0.98939377069473267 0.99886280298233032 
		0.98939377069473267 0 0.98939377069473267 -0.99944108724594116;
createNode animCurveTA -n "rigw:mesh:robo62_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo62_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo62_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo62_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo62_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo63_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo63_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 190.6238674198519 4 180 5 180
		 6 190.6238674198519 7 180;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 0.17693430185317993 1 1 1 0.17693430185317993;
	setAttr -s 7 ".kiy[1:6]"  0 0.98422259092330933 0 0 0 -0.98422259092330933;
createNode animCurveTA -n "rigw:mesh:robo63_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo63_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo63_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo63_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo63_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo64_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo64_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo64_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo64_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo64_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo64_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo64_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo65_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo65_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo65_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo65_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo65_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo65_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo65_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo66_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo66_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo66_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo66_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo66_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo66_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo66_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo67_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo67_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo67_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo67_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo67_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo67_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo67_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo68_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo68_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo68_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo68_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo68_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo68_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo68_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo69_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo69_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo69_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo69_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo69_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo69_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo69_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo70_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 1 9 1 1 1 9;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo70_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo70_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo70_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo70_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo70_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo70_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 1 2 1 1 1 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo71_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  9 9 1 9 9 9 9;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo71_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo71_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo71_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo71_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89381656388170461 2 0.89381656388170461
		 3 0.89381656388170461 4 0.89381656388170461 5 0.89381656388170461 6 0.89381656388170461
		 7 0.89381656388170461;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo71_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo71_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.8938165638817045 2 0.8938165638817045
		 3 0.8938165638817045 4 0.8938165638817045 5 0.8938165638817045 6 0.8938165638817045
		 7 0.8938165638817045;
	setAttr -s 7 ".kit[0:6]"  2 2 1 2 2 2 2;
	setAttr -s 7 ".kix[2:6]"  1 1 1 1 1;
	setAttr -s 7 ".kiy[2:6]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 240.00000000000003 3 240.00000000000003
		 4 720 5 720 6 720 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 240.00000000000003 3 240.00000000000003
		 4 720 5 720 6 720 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 240.00000000000003 3 240.00000000000003
		 4 720 5 720 6 720 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 240.00000000000003 3 240.00000000000003
		 4 720 5 720 6 720 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 299.47809846784889 3 299.47809846784889
		 4 720 5 720 6 720 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 240.00000000000003 3 240.00000000000003
		 4 720 5 720 6 720 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 240.00000000000003 3 240.00000000000003
		 4 720 5 720 6 720 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 240.00000000000003 3 240.00000000000003
		 4 720 5 720 6 720 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.60969552725771381 7 -0.60969552725771381;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -2.4671622769447919e-16 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_roll";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_rollAngle";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 25 7 25;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.57131784367607674 7 0.57131784367607674;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -2.4671622769447919e-16 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 6.1679056923619798e-17 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_roll";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_rollAngle";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 25 7 25;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode mia_exposure_simple -n "mia_exposure_simple1";
	setAttr ".S02" 1;
	setAttr ".S04" 1.1499999761581421;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 2 0 3 0 3.995 0 4 0 5 0 6 0 7 0;
	setAttr -s 8 ".kot[0:7]"  5 5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.086017228197441398 2 0.086017228197441398
		 3 -0.044242276821270909 4 -0.044242276821270909 5 -0.044242276821270909 6 0.086017228197441398
		 7 0.086017228197441398;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.13170091215445856 2 0.13170091215445856
		 3 0.13170091215445856 3.995 0.13170091215445856 4 0.13170091215445856 5 0.13170091215445856
		 6 0.13170091215445856 7 0.13170091215445856;
	setAttr -s 8 ".kot[0:7]"  5 5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 2 0 3 0 3.995 0 4 0 5 0 6 0 7 0;
	setAttr -s 8 ".kot[0:7]"  5 5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 2 0 3 0 3.995 0 4 0 5 0 6 0 7 0;
	setAttr -s 8 ".kot[0:7]"  5 5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 2 0 3 0 3.995 0 4 0 5 0 6 0 7 0;
	setAttr -s 8 ".kot[0:7]"  5 5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 2 0 3 0 3.995 0 4 0 5 0 6 0 7 0;
	setAttr -s 8 ".kot[0:7]"  5 5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 5.0076545383864612 2 5.0076545383864612
		 3 5.0076545383864612 4 5.0076545383864612 5 5.0076545383864612 6 5.0076545383864612
		 7 5.0076545383864612;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 5.1102304224489528 2 5.1102304224489528
		 3 5.1102304224489528 4 5.1102304224489528 5 5.1102304224489528 6 5.1102304224489528
		 7 5.1102304224489528;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 11.461923536076981 2 11.461923536076981
		 3 11.461923536076981 4 11.461923536076981 5 11.461923536076981 6 11.461923536076981
		 7 11.461923536076981;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 12.875867843047878 2 13.551116482626488
		 4 14.901634019445472 5 14.226385379866862 6 14.226385379866862 7 12.875867843047878;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.98473143577575684 0.9428057074546814 
		0.81648784875869751;
	setAttr -s 6 ".kiy[3:5]"  -0.17408047616481781 -0.3333427906036377 
		-0.5773625373840332;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -17.1468685540652 2 -24.372707268476201
		 4 -38.824601474627393 5 -31.598762760216392 6 -31.598762760216392 7 -17.1468685540652;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.46734049916267395 0.25553101301193237 
		0.13101378083229065;
	setAttr -s 6 ".kiy[3:5]"  0.88407737016677856 0.96680086851119995 
		0.99138057231903076;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 15.283507717084996 2 17.294451310067593
		 4 21.316398824943871 5 19.305455231961272 6 19.305455231961272 7 15.283507717084996;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.88486427068710327 0.68864226341247559 
		0.42895311117172241;
	setAttr -s 6 ".kiy[3:5]"  -0.46584895253181458 -0.72510135173797607 
		-0.90332674980163574;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 4.5696542071626363 2 0.33784839173938691
		 4 -8.1258901945511308 5 -3.8940843791278792 6 -3.8940843791278792 7 4.5696542071626363;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.67003917694091797 0.41135278344154358 
		0.2201174795627594;
	setAttr -s 6 ".kiy[3:5]"  0.74232572317123413 0.91147619485855103 
		0.97547334432601929;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 3.6674695198749774 2 -2.2440131536400161
		 4 -14.067155846923672 5 -8.1556731734086778 6 -8.1556731734086778 7 3.6674695198749774;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.54271453619003296 0.30742567777633667 
		0.15946848690509796;
	setAttr -s 6 ".kiy[3:5]"  0.83991712331771851 0.95157212018966675 
		0.98720300197601318;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -24.09925220273978 2 -22.800701455077832
		 4 -20.20356100284193 5 -21.502111750503879 6 -21.502111750503879 7 -24.09925220273978;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.94678425788879395 0.8269534707069397 
		0.59243011474609375;
	setAttr -s 6 ".kiy[3:5]"  -0.32186853885650635 -0.56227034330368042 
		-0.80562180280685425;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -19.225395691180399 2 -21.182387997556624
		 4 -25.096431320665364 5 -23.139439014289145 6 -23.139439014289145 7 -19.225395691180399;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.88999134302139282 0.69843059778213501 
		0.43852934241294861;
	setAttr -s 6 ".kiy[3:5]"  0.45597749948501587 0.71567773818969727 
		0.89871692657470703;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 21.318876477049805 2 14.325176315479505
		 4 0.33756617923592813 5 7.3312663408062289 6 7.3312663408062289 7 21.318876477049805;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.47933310270309448 0.26343294978141785 
		0.13528412580490112;
	setAttr -s 6 ".kiy[3:5]"  0.87763309478759766 0.96467775106430054 
		0.99080681800842285;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -5.0026258365066472 2 -2.9401092648338154
		 4 1.1849857546277589 5 -0.87753081704507285 6 -0.87753081704507285 7 -5.0026258365066472;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.87991809844970703 0.67942583560943604 
		0.42014041543006897;
	setAttr -s 6 ".kiy[3:5]"  -0.47512537240982056 -0.73374420404434204 
		-0.9074590802192688;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -10.204552660478932 2 -10.586076318328658
		 4 -11.349135079852308 5 -10.967611422002577 6 -10.967611422002577 7 -10.204552660478932;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.99504870176315308 0.98062437772750854 
		0.92862498760223389;
	setAttr -s 6 ".kiy[3:5]"  0.099388100206851959 0.19589769840240479 
		0.37101972103118896;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -1.6025976289315653 2 -8.8430078670383221
		 4 -23.324045557731122 5 -16.083635319624364 6 -16.083635319624364 7 -1.6025976289315653;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.46660494804382324 0.25505021214485168 
		0.13075461983680725;
	setAttr -s 6 ".kiy[3:5]"  0.88446587324142456 0.96692776679992676 
		0.99141472578048706;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 8.1840125709768738 2 9.7884703651149607
		 4 12.997434087606294 5 11.392976293468212 6 11.392976293468212 7 8.1840125709768738;
	setAttr -s 6 ".kit[0:5]"  2 2 2 1 9 2;
	setAttr -s 6 ".kix[3:5]"  0.92196696996688843 0.76566535234451294 
		0.51143676042556763;
	setAttr -s 6 ".kiy[3:5]"  -0.3872685432434082 -0.64323908090591431 
		-0.85932093858718872;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -22.028842725247312 2 -22.028842725247312
		 3 -22.028842725247312 4 -22.028842725247312 5 -22.028842725247312 6 -22.028842725247312
		 7 -22.028842725247312;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -16.267797140212188 2 -16.267797140212188
		 3 -16.267797140212188 4 -16.267797140212188 5 -16.267797140212188 6 -16.267797140212188
		 7 -16.267797140212188;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -39.607339347956533 2 -39.607339347956533
		 3 -39.607339347956533 4 -39.607339347956533 5 -39.607339347956533 6 -39.607339347956533
		 7 -39.607339347956533;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKIKBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKIKBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:Main_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:Main_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:Main_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:Main_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:Main_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:Main_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:Main_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:Main_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.8 2 1.8 3 1.8 4 1.8 5 1.8 6 1.8 7 1.8;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:Main_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.8 2 1.8 3 1.8 4 1.8 5 1.8 6 1.8 7 1.8;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:Main_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.8 2 1.8 3 1.8 4 1.8 5 1.8 6 1.8 7 1.8;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
createNode animCurveTU -n "rigw:mesh:robo27_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo57_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo56_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 3 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:mesh:robo28_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 3 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:mesh:robo29_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 3 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:mesh:robo77_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 3 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:mesh:robo30_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo78_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo27_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.3919414694234578;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo27_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.2748032187780167;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo27_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 3.2651741024147887;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo27_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo27_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo27_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -89.999999999999986;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo27_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.6598976221716818;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo27_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.57296953286450725;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo27_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.6598976221716818;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo57_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.98173678251855412;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo57_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.2750125226494036;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo57_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 3.2651741117687818;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo57_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.0259994758761929e-30;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo57_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.5580224440669704e-30;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo57_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -89.999999999999986;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo57_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.68832881915940214;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo57_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.68832881915940214;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo57_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.68832881915940181;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo56_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.79051463307634862;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo56_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.7148770585072126;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo56_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.92744114029962244;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo56_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo56_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.5580224440669701e-30;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo56_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -89.999999999999986;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo56_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.95717242638810685;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo56_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.95717242638810685;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo56_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.95717242638810651;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo28_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.3813825391270551;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo28_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.714876531569288;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo28_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.92744112762141739;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo28_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo28_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo28_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -89.999999999999986;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo28_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.1127287065582636;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo28_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.96614933253350277;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo28_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.1127287065582636;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo29_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.3813825391270551;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo29_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.714876531569288;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo29_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.7687120887364598;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo29_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo29_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo29_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -89.999999999999986;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo29_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.1127287065582636;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo29_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.96614933253350277;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo29_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.1127287065582636;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo77_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.79051463307634828;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo77_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.7148770585072126;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo77_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.7687120760582531;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo77_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo77_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.5580224440669701e-30;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo77_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -89.999999999999986;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo77_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.95717242638810685;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo77_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.95717242638810685;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo77_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.95717242638810651;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo78_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.0363939251094465;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo78_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.1942520074588334;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo78_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -4.0981176628478542;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo78_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo78_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.5580224440669701e-30;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo78_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -89.999999999999986;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo78_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.5935616841543635;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo78_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.5935616841543635;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo78_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.59356168415436339;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo30_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.4379515766303863;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo30_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.1942516803463732;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo30_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -4.0981176722018509;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo30_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo30_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo30_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -89.999999999999986;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo30_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.6598976221716818;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo30_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.57296953286450725;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo30_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.6598976221716818;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegBall_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:PoleLeg_R_follow";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 10 3 10 4 10 5 10 6 10 7 10;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegBall_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTU -n "rigw:rig:PoleLeg_L_follow";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  2 10 3 10 4 10 5 10 6 10 7 10;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
createNode animCurveTU -n "rigw:robo85_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:robo84_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:robo105_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:robo106_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:robo57_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:robo58_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:robo77_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:robo78_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:polySurface16_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:polySurface12_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:polySurface10_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo4_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo3_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo2_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo5_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo11_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo11_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo11_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo11_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo11_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo11_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo11_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo11_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo11_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo11_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0.97426179424707182;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo12_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo12_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo12_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 2.7663115476818358;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:mesh:robo12_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 -0.043264107793707672;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo12_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo12_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:mesh:robo12_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo12_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0.86245411060370092;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo12_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0.17395114371717799;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo12_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 0.84025608925252415;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo13_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo14_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo15_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo16_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo17_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo18_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo19_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo20_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo21_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo22_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo32_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo33_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo34_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo35_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo36_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo37_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo38_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo43_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo44_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo45_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo46_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo47_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo48_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo49_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo50_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo51_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo52_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo53_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo54_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo55_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo112_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo113_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo114_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo115_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo116_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo117_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo118_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo119_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo120_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo121_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo122_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo123_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo124_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo125_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:polySurface1_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:polySurface17_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:polySurface13_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:polySurface14_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:polySurface15_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo126_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo127_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:mesh:robo128_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  13 1;
	setAttr ".kot[0]"  5;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 17 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 18 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 35 ".tx";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 51 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w" 1280;
	setAttr -av ".h" 720;
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av ".dar" 1.7769999504089355;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 2 ".sol";
connectAttr "rigw:robo85_visibility.o" "rigwRN.phl[437]";
connectAttr "rigw:robo84_visibility.o" "rigwRN.phl[438]";
connectAttr "rigw:robo105_visibility.o" "rigwRN.phl[439]";
connectAttr "rigw:robo106_visibility.o" "rigwRN.phl[440]";
connectAttr "rigw:robo57_visibility.o" "rigwRN.phl[441]";
connectAttr "rigw:robo58_visibility.o" "rigwRN.phl[442]";
connectAttr "rigw:robo77_visibility.o" "rigwRN.phl[443]";
connectAttr "rigw:robo78_visibility.o" "rigwRN.phl[444]";
connectAttr "rigw:mesh:polySurface16_visibility.o" "rigwRN.phl[445]";
connectAttr "rigw:mesh:polySurface12_visibility.o" "rigwRN.phl[446]";
connectAttr "rigw:mesh:polySurface10_visibility.o" "rigwRN.phl[447]";
connectAttr "rigw:mesh:robo4_visibility.o" "rigwRN.phl[448]";
connectAttr "rigw:mesh:robo3_visibility.o" "rigwRN.phl[449]";
connectAttr "rigw:mesh:robo2_visibility.o" "rigwRN.phl[450]";
connectAttr "rigw:mesh:robo5_visibility.o" "rigwRN.phl[451]";
connectAttr "rigw:mesh:robo11_visibility.o" "rigwRN.phl[452]";
connectAttr "rigw:mesh:robo11_translateX.o" "rigwRN.phl[453]";
connectAttr "rigw:mesh:robo11_translateY.o" "rigwRN.phl[454]";
connectAttr "rigw:mesh:robo11_translateZ.o" "rigwRN.phl[455]";
connectAttr "rigw:mesh:robo11_rotateX.o" "rigwRN.phl[456]";
connectAttr "rigw:mesh:robo11_rotateY.o" "rigwRN.phl[457]";
connectAttr "rigw:mesh:robo11_rotateZ.o" "rigwRN.phl[458]";
connectAttr "rigw:mesh:robo11_scaleX.o" "rigwRN.phl[459]";
connectAttr "rigw:mesh:robo11_scaleY.o" "rigwRN.phl[460]";
connectAttr "rigw:mesh:robo11_scaleZ.o" "rigwRN.phl[461]";
connectAttr "rigw:mesh:robo12_visibility.o" "rigwRN.phl[462]";
connectAttr "rigw:mesh:robo12_translateX.o" "rigwRN.phl[463]";
connectAttr "rigw:mesh:robo12_translateY.o" "rigwRN.phl[464]";
connectAttr "rigw:mesh:robo12_translateZ.o" "rigwRN.phl[465]";
connectAttr "rigw:mesh:robo12_rotateX.o" "rigwRN.phl[466]";
connectAttr "rigw:mesh:robo12_rotateY.o" "rigwRN.phl[467]";
connectAttr "rigw:mesh:robo12_rotateZ.o" "rigwRN.phl[468]";
connectAttr "rigw:mesh:robo12_scaleX.o" "rigwRN.phl[469]";
connectAttr "rigw:mesh:robo12_scaleY.o" "rigwRN.phl[470]";
connectAttr "rigw:mesh:robo12_scaleZ.o" "rigwRN.phl[471]";
connectAttr "rigw:mesh:robo13_visibility.o" "rigwRN.phl[472]";
connectAttr "rigw:mesh:robo14_visibility.o" "rigwRN.phl[473]";
connectAttr "rigw:mesh:robo15_visibility.o" "rigwRN.phl[474]";
connectAttr "rigw:mesh:robo16_visibility.o" "rigwRN.phl[475]";
connectAttr "rigw:mesh:robo17_visibility.o" "rigwRN.phl[476]";
connectAttr "rigw:mesh:robo18_visibility.o" "rigwRN.phl[477]";
connectAttr "rigw:mesh:robo19_visibility.o" "rigwRN.phl[478]";
connectAttr "rigw:mesh:robo20_visibility.o" "rigwRN.phl[479]";
connectAttr "rigw:mesh:robo21_visibility.o" "rigwRN.phl[480]";
connectAttr "rigw:mesh:robo22_visibility.o" "rigwRN.phl[481]";
connectAttr "rigw:mesh:robo32_visibility.o" "rigwRN.phl[482]";
connectAttr "rigw:mesh:robo33_visibility.o" "rigwRN.phl[483]";
connectAttr "rigw:mesh:robo34_visibility.o" "rigwRN.phl[484]";
connectAttr "rigw:mesh:robo35_visibility.o" "rigwRN.phl[485]";
connectAttr "rigw:mesh:robo36_visibility.o" "rigwRN.phl[486]";
connectAttr "rigw:mesh:robo37_visibility.o" "rigwRN.phl[487]";
connectAttr "rigw:mesh:robo38_visibility.o" "rigwRN.phl[488]";
connectAttr "rigw:mesh:robo43_visibility.o" "rigwRN.phl[489]";
connectAttr "rigw:mesh:robo44_visibility.o" "rigwRN.phl[490]";
connectAttr "rigw:mesh:robo45_visibility.o" "rigwRN.phl[491]";
connectAttr "rigw:mesh:robo46_visibility.o" "rigwRN.phl[492]";
connectAttr "rigw:mesh:robo47_visibility.o" "rigwRN.phl[493]";
connectAttr "rigw:mesh:robo48_visibility.o" "rigwRN.phl[494]";
connectAttr "rigw:mesh:robo49_visibility.o" "rigwRN.phl[495]";
connectAttr "rigw:mesh:robo50_visibility.o" "rigwRN.phl[496]";
connectAttr "rigw:mesh:robo51_visibility.o" "rigwRN.phl[497]";
connectAttr "rigw:mesh:robo52_visibility.o" "rigwRN.phl[498]";
connectAttr "rigw:mesh:robo53_visibility.o" "rigwRN.phl[499]";
connectAttr "rigw:mesh:robo54_visibility.o" "rigwRN.phl[500]";
connectAttr "rigw:mesh:robo55_visibility.o" "rigwRN.phl[501]";
connectAttr "rigw:mesh:robo112_visibility.o" "rigwRN.phl[502]";
connectAttr "rigw:mesh:robo113_visibility.o" "rigwRN.phl[503]";
connectAttr "rigw:mesh:robo114_visibility.o" "rigwRN.phl[504]";
connectAttr "rigw:mesh:robo115_visibility.o" "rigwRN.phl[505]";
connectAttr "rigw:mesh:robo116_visibility.o" "rigwRN.phl[506]";
connectAttr "rigw:mesh:robo117_visibility.o" "rigwRN.phl[507]";
connectAttr "rigw:mesh:robo118_visibility.o" "rigwRN.phl[508]";
connectAttr "rigw:mesh:robo119_visibility.o" "rigwRN.phl[509]";
connectAttr "rigw:mesh:robo120_visibility.o" "rigwRN.phl[510]";
connectAttr "rigw:mesh:robo121_visibility.o" "rigwRN.phl[511]";
connectAttr "rigw:mesh:robo122_visibility.o" "rigwRN.phl[512]";
connectAttr "rigw:mesh:robo123_visibility.o" "rigwRN.phl[513]";
connectAttr "rigw:mesh:robo124_visibility.o" "rigwRN.phl[514]";
connectAttr "rigw:mesh:robo125_visibility.o" "rigwRN.phl[515]";
connectAttr "rigw:mesh:polySurface1_visibility.o" "rigwRN.phl[516]";
connectAttr "rigw:mesh:polySurface17_visibility.o" "rigwRN.phl[517]";
connectAttr "rigw:mesh:polySurface13_visibility.o" "rigwRN.phl[518]";
connectAttr "rigw:mesh:polySurface14_visibility.o" "rigwRN.phl[519]";
connectAttr "rigw:mesh:polySurface15_visibility.o" "rigwRN.phl[520]";
connectAttr "rigw:mesh:robo126_visibility.o" "rigwRN.phl[521]";
connectAttr "rigw:mesh:robo127_visibility.o" "rigwRN.phl[522]";
connectAttr "rigw:mesh:robo128_visibility.o" "rigwRN.phl[523]";
connectAttr "rigw:mesh:robo72_visibility.o" "rigwRN.phl[524]";
connectAttr "rigw:mesh:robo72_translateX.o" "rigwRN.phl[525]";
connectAttr "rigw:mesh:robo72_translateY.o" "rigwRN.phl[526]";
connectAttr "rigw:mesh:robo72_translateZ.o" "rigwRN.phl[527]";
connectAttr "rigw:mesh:robo72_rotateX.o" "rigwRN.phl[528]";
connectAttr "rigw:mesh:robo72_rotateY.o" "rigwRN.phl[529]";
connectAttr "rigw:mesh:robo72_rotateZ.o" "rigwRN.phl[530]";
connectAttr "rigw:mesh:robo72_scaleX.o" "rigwRN.phl[531]";
connectAttr "rigw:mesh:robo72_scaleY.o" "rigwRN.phl[532]";
connectAttr "rigw:mesh:robo72_scaleZ.o" "rigwRN.phl[533]";
connectAttr "rigw:mesh:robo73_visibility.o" "rigwRN.phl[534]";
connectAttr "rigw:mesh:robo73_translateX.o" "rigwRN.phl[535]";
connectAttr "rigw:mesh:robo73_translateY.o" "rigwRN.phl[536]";
connectAttr "rigw:mesh:robo73_translateZ.o" "rigwRN.phl[537]";
connectAttr "rigw:mesh:robo73_rotateX.o" "rigwRN.phl[538]";
connectAttr "rigw:mesh:robo73_rotateY.o" "rigwRN.phl[539]";
connectAttr "rigw:mesh:robo73_rotateZ.o" "rigwRN.phl[540]";
connectAttr "rigw:mesh:robo73_scaleX.o" "rigwRN.phl[541]";
connectAttr "rigw:mesh:robo73_scaleY.o" "rigwRN.phl[542]";
connectAttr "rigw:mesh:robo73_scaleZ.o" "rigwRN.phl[543]";
connectAttr "rigw:mesh:robo74_visibility.o" "rigwRN.phl[544]";
connectAttr "rigw:mesh:robo74_translateX.o" "rigwRN.phl[545]";
connectAttr "rigw:mesh:robo74_translateY.o" "rigwRN.phl[546]";
connectAttr "rigw:mesh:robo74_translateZ.o" "rigwRN.phl[547]";
connectAttr "rigw:mesh:robo74_rotateX.o" "rigwRN.phl[548]";
connectAttr "rigw:mesh:robo74_rotateY.o" "rigwRN.phl[549]";
connectAttr "rigw:mesh:robo74_rotateZ.o" "rigwRN.phl[550]";
connectAttr "rigw:mesh:robo74_scaleX.o" "rigwRN.phl[551]";
connectAttr "rigw:mesh:robo74_scaleY.o" "rigwRN.phl[552]";
connectAttr "rigw:mesh:robo74_scaleZ.o" "rigwRN.phl[553]";
connectAttr "rigw:mesh:robo75_visibility.o" "rigwRN.phl[554]";
connectAttr "rigw:mesh:robo75_translateX.o" "rigwRN.phl[555]";
connectAttr "rigw:mesh:robo75_translateY.o" "rigwRN.phl[556]";
connectAttr "rigw:mesh:robo75_translateZ.o" "rigwRN.phl[557]";
connectAttr "rigw:mesh:robo75_rotateX.o" "rigwRN.phl[558]";
connectAttr "rigw:mesh:robo75_rotateY.o" "rigwRN.phl[559]";
connectAttr "rigw:mesh:robo75_rotateZ.o" "rigwRN.phl[560]";
connectAttr "rigw:mesh:robo75_scaleX.o" "rigwRN.phl[561]";
connectAttr "rigw:mesh:robo75_scaleY.o" "rigwRN.phl[562]";
connectAttr "rigw:mesh:robo75_scaleZ.o" "rigwRN.phl[563]";
connectAttr "rigw:mesh:robo76_visibility.o" "rigwRN.phl[564]";
connectAttr "rigw:mesh:robo76_translateX.o" "rigwRN.phl[565]";
connectAttr "rigw:mesh:robo76_translateY.o" "rigwRN.phl[566]";
connectAttr "rigw:mesh:robo76_translateZ.o" "rigwRN.phl[567]";
connectAttr "rigw:mesh:robo76_rotateX.o" "rigwRN.phl[568]";
connectAttr "rigw:mesh:robo76_rotateY.o" "rigwRN.phl[569]";
connectAttr "rigw:mesh:robo76_rotateZ.o" "rigwRN.phl[570]";
connectAttr "rigw:mesh:robo76_scaleX.o" "rigwRN.phl[571]";
connectAttr "rigw:mesh:robo76_scaleY.o" "rigwRN.phl[572]";
connectAttr "rigw:mesh:robo76_scaleZ.o" "rigwRN.phl[573]";
connectAttr "rigw:mesh:robo83_visibility.o" "rigwRN.phl[574]";
connectAttr "rigw:mesh:robo83_translateX.o" "rigwRN.phl[575]";
connectAttr "rigw:mesh:robo83_translateY.o" "rigwRN.phl[576]";
connectAttr "rigw:mesh:robo83_translateZ.o" "rigwRN.phl[577]";
connectAttr "rigw:mesh:robo83_rotateX.o" "rigwRN.phl[578]";
connectAttr "rigw:mesh:robo83_rotateY.o" "rigwRN.phl[579]";
connectAttr "rigw:mesh:robo83_rotateZ.o" "rigwRN.phl[580]";
connectAttr "rigw:mesh:robo83_scaleX.o" "rigwRN.phl[581]";
connectAttr "rigw:mesh:robo83_scaleY.o" "rigwRN.phl[582]";
connectAttr "rigw:mesh:robo83_scaleZ.o" "rigwRN.phl[583]";
connectAttr "rigw:mesh:robo82_visibility.o" "rigwRN.phl[584]";
connectAttr "rigw:mesh:robo82_translateX.o" "rigwRN.phl[585]";
connectAttr "rigw:mesh:robo82_translateY.o" "rigwRN.phl[586]";
connectAttr "rigw:mesh:robo82_translateZ.o" "rigwRN.phl[587]";
connectAttr "rigw:mesh:robo82_rotateX.o" "rigwRN.phl[588]";
connectAttr "rigw:mesh:robo82_rotateY.o" "rigwRN.phl[589]";
connectAttr "rigw:mesh:robo82_rotateZ.o" "rigwRN.phl[590]";
connectAttr "rigw:mesh:robo82_scaleX.o" "rigwRN.phl[591]";
connectAttr "rigw:mesh:robo82_scaleY.o" "rigwRN.phl[592]";
connectAttr "rigw:mesh:robo82_scaleZ.o" "rigwRN.phl[593]";
connectAttr "rigw:mesh:robo81_visibility.o" "rigwRN.phl[594]";
connectAttr "rigw:mesh:robo81_translateX.o" "rigwRN.phl[595]";
connectAttr "rigw:mesh:robo81_translateY.o" "rigwRN.phl[596]";
connectAttr "rigw:mesh:robo81_translateZ.o" "rigwRN.phl[597]";
connectAttr "rigw:mesh:robo81_rotateX.o" "rigwRN.phl[598]";
connectAttr "rigw:mesh:robo81_rotateY.o" "rigwRN.phl[599]";
connectAttr "rigw:mesh:robo81_rotateZ.o" "rigwRN.phl[600]";
connectAttr "rigw:mesh:robo81_scaleX.o" "rigwRN.phl[601]";
connectAttr "rigw:mesh:robo81_scaleY.o" "rigwRN.phl[602]";
connectAttr "rigw:mesh:robo81_scaleZ.o" "rigwRN.phl[603]";
connectAttr "rigw:mesh:robo80_visibility.o" "rigwRN.phl[604]";
connectAttr "rigw:mesh:robo80_translateX.o" "rigwRN.phl[605]";
connectAttr "rigw:mesh:robo80_translateY.o" "rigwRN.phl[606]";
connectAttr "rigw:mesh:robo80_translateZ.o" "rigwRN.phl[607]";
connectAttr "rigw:mesh:robo80_rotateX.o" "rigwRN.phl[608]";
connectAttr "rigw:mesh:robo80_rotateY.o" "rigwRN.phl[609]";
connectAttr "rigw:mesh:robo80_rotateZ.o" "rigwRN.phl[610]";
connectAttr "rigw:mesh:robo80_scaleX.o" "rigwRN.phl[611]";
connectAttr "rigw:mesh:robo80_scaleY.o" "rigwRN.phl[612]";
connectAttr "rigw:mesh:robo80_scaleZ.o" "rigwRN.phl[613]";
connectAttr "rigw:mesh:robo79_visibility.o" "rigwRN.phl[614]";
connectAttr "rigw:mesh:robo79_translateX.o" "rigwRN.phl[615]";
connectAttr "rigw:mesh:robo79_translateY.o" "rigwRN.phl[616]";
connectAttr "rigw:mesh:robo79_translateZ.o" "rigwRN.phl[617]";
connectAttr "rigw:mesh:robo79_rotateX.o" "rigwRN.phl[618]";
connectAttr "rigw:mesh:robo79_rotateY.o" "rigwRN.phl[619]";
connectAttr "rigw:mesh:robo79_rotateZ.o" "rigwRN.phl[620]";
connectAttr "rigw:mesh:robo79_scaleX.o" "rigwRN.phl[621]";
connectAttr "rigw:mesh:robo79_scaleY.o" "rigwRN.phl[622]";
connectAttr "rigw:mesh:robo79_scaleZ.o" "rigwRN.phl[623]";
connectAttr "rigw:mesh:robo58_visibility.o" "rigwRN.phl[624]";
connectAttr "rigw:mesh:robo58_translateX.o" "rigwRN.phl[625]";
connectAttr "rigw:mesh:robo58_translateY.o" "rigwRN.phl[626]";
connectAttr "rigw:mesh:robo58_translateZ.o" "rigwRN.phl[627]";
connectAttr "rigw:mesh:robo58_rotateX.o" "rigwRN.phl[628]";
connectAttr "rigw:mesh:robo58_rotateY.o" "rigwRN.phl[629]";
connectAttr "rigw:mesh:robo58_rotateZ.o" "rigwRN.phl[630]";
connectAttr "rigw:mesh:robo58_scaleX.o" "rigwRN.phl[631]";
connectAttr "rigw:mesh:robo58_scaleY.o" "rigwRN.phl[632]";
connectAttr "rigw:mesh:robo58_scaleZ.o" "rigwRN.phl[633]";
connectAttr "rigw:mesh:robo59_visibility.o" "rigwRN.phl[634]";
connectAttr "rigw:mesh:robo59_translateX.o" "rigwRN.phl[635]";
connectAttr "rigw:mesh:robo59_translateY.o" "rigwRN.phl[636]";
connectAttr "rigw:mesh:robo59_translateZ.o" "rigwRN.phl[637]";
connectAttr "rigw:mesh:robo59_rotateX.o" "rigwRN.phl[638]";
connectAttr "rigw:mesh:robo59_rotateY.o" "rigwRN.phl[639]";
connectAttr "rigw:mesh:robo59_rotateZ.o" "rigwRN.phl[640]";
connectAttr "rigw:mesh:robo59_scaleX.o" "rigwRN.phl[641]";
connectAttr "rigw:mesh:robo59_scaleY.o" "rigwRN.phl[642]";
connectAttr "rigw:mesh:robo59_scaleZ.o" "rigwRN.phl[643]";
connectAttr "rigw:mesh:robo60_visibility.o" "rigwRN.phl[644]";
connectAttr "rigw:mesh:robo60_translateX.o" "rigwRN.phl[645]";
connectAttr "rigw:mesh:robo60_translateY.o" "rigwRN.phl[646]";
connectAttr "rigw:mesh:robo60_translateZ.o" "rigwRN.phl[647]";
connectAttr "rigw:mesh:robo60_rotateX.o" "rigwRN.phl[648]";
connectAttr "rigw:mesh:robo60_rotateY.o" "rigwRN.phl[649]";
connectAttr "rigw:mesh:robo60_rotateZ.o" "rigwRN.phl[650]";
connectAttr "rigw:mesh:robo60_scaleX.o" "rigwRN.phl[651]";
connectAttr "rigw:mesh:robo60_scaleY.o" "rigwRN.phl[652]";
connectAttr "rigw:mesh:robo60_scaleZ.o" "rigwRN.phl[653]";
connectAttr "rigw:mesh:robo61_visibility.o" "rigwRN.phl[654]";
connectAttr "rigw:mesh:robo61_translateX.o" "rigwRN.phl[655]";
connectAttr "rigw:mesh:robo61_translateY.o" "rigwRN.phl[656]";
connectAttr "rigw:mesh:robo61_translateZ.o" "rigwRN.phl[657]";
connectAttr "rigw:mesh:robo61_rotateX.o" "rigwRN.phl[658]";
connectAttr "rigw:mesh:robo61_rotateY.o" "rigwRN.phl[659]";
connectAttr "rigw:mesh:robo61_rotateZ.o" "rigwRN.phl[660]";
connectAttr "rigw:mesh:robo61_scaleX.o" "rigwRN.phl[661]";
connectAttr "rigw:mesh:robo61_scaleY.o" "rigwRN.phl[662]";
connectAttr "rigw:mesh:robo61_scaleZ.o" "rigwRN.phl[663]";
connectAttr "rigw:mesh:robo62_visibility.o" "rigwRN.phl[664]";
connectAttr "rigw:mesh:robo62_translateX.o" "rigwRN.phl[665]";
connectAttr "rigw:mesh:robo62_translateY.o" "rigwRN.phl[666]";
connectAttr "rigw:mesh:robo62_translateZ.o" "rigwRN.phl[667]";
connectAttr "rigw:mesh:robo62_rotateX.o" "rigwRN.phl[668]";
connectAttr "rigw:mesh:robo62_rotateY.o" "rigwRN.phl[669]";
connectAttr "rigw:mesh:robo62_rotateZ.o" "rigwRN.phl[670]";
connectAttr "rigw:mesh:robo62_scaleX.o" "rigwRN.phl[671]";
connectAttr "rigw:mesh:robo62_scaleY.o" "rigwRN.phl[672]";
connectAttr "rigw:mesh:robo62_scaleZ.o" "rigwRN.phl[673]";
connectAttr "rigw:mesh:robo63_visibility.o" "rigwRN.phl[674]";
connectAttr "rigw:mesh:robo63_translateX.o" "rigwRN.phl[675]";
connectAttr "rigw:mesh:robo63_translateY.o" "rigwRN.phl[676]";
connectAttr "rigw:mesh:robo63_translateZ.o" "rigwRN.phl[677]";
connectAttr "rigw:mesh:robo63_rotateX.o" "rigwRN.phl[678]";
connectAttr "rigw:mesh:robo63_rotateY.o" "rigwRN.phl[679]";
connectAttr "rigw:mesh:robo63_rotateZ.o" "rigwRN.phl[680]";
connectAttr "rigw:mesh:robo63_scaleX.o" "rigwRN.phl[681]";
connectAttr "rigw:mesh:robo63_scaleY.o" "rigwRN.phl[682]";
connectAttr "rigw:mesh:robo63_scaleZ.o" "rigwRN.phl[683]";
connectAttr "rigw:mesh:robo64_visibility.o" "rigwRN.phl[684]";
connectAttr "rigw:mesh:robo64_translateX.o" "rigwRN.phl[685]";
connectAttr "rigw:mesh:robo64_translateY.o" "rigwRN.phl[686]";
connectAttr "rigw:mesh:robo64_translateZ.o" "rigwRN.phl[687]";
connectAttr "rigw:mesh:robo64_rotateX.o" "rigwRN.phl[688]";
connectAttr "rigw:mesh:robo64_rotateY.o" "rigwRN.phl[689]";
connectAttr "rigw:mesh:robo64_rotateZ.o" "rigwRN.phl[690]";
connectAttr "rigw:mesh:robo64_scaleX.o" "rigwRN.phl[691]";
connectAttr "rigw:mesh:robo64_scaleY.o" "rigwRN.phl[692]";
connectAttr "rigw:mesh:robo64_scaleZ.o" "rigwRN.phl[693]";
connectAttr "rigw:mesh:robo65_visibility.o" "rigwRN.phl[694]";
connectAttr "rigw:mesh:robo65_translateX.o" "rigwRN.phl[695]";
connectAttr "rigw:mesh:robo65_translateY.o" "rigwRN.phl[696]";
connectAttr "rigw:mesh:robo65_translateZ.o" "rigwRN.phl[697]";
connectAttr "rigw:mesh:robo65_rotateX.o" "rigwRN.phl[698]";
connectAttr "rigw:mesh:robo65_rotateY.o" "rigwRN.phl[699]";
connectAttr "rigw:mesh:robo65_rotateZ.o" "rigwRN.phl[700]";
connectAttr "rigw:mesh:robo65_scaleX.o" "rigwRN.phl[701]";
connectAttr "rigw:mesh:robo65_scaleY.o" "rigwRN.phl[702]";
connectAttr "rigw:mesh:robo65_scaleZ.o" "rigwRN.phl[703]";
connectAttr "rigw:mesh:robo66_visibility.o" "rigwRN.phl[704]";
connectAttr "rigw:mesh:robo66_translateX.o" "rigwRN.phl[705]";
connectAttr "rigw:mesh:robo66_translateY.o" "rigwRN.phl[706]";
connectAttr "rigw:mesh:robo66_translateZ.o" "rigwRN.phl[707]";
connectAttr "rigw:mesh:robo66_rotateX.o" "rigwRN.phl[708]";
connectAttr "rigw:mesh:robo66_rotateY.o" "rigwRN.phl[709]";
connectAttr "rigw:mesh:robo66_rotateZ.o" "rigwRN.phl[710]";
connectAttr "rigw:mesh:robo66_scaleX.o" "rigwRN.phl[711]";
connectAttr "rigw:mesh:robo66_scaleY.o" "rigwRN.phl[712]";
connectAttr "rigw:mesh:robo66_scaleZ.o" "rigwRN.phl[713]";
connectAttr "rigw:mesh:robo67_visibility.o" "rigwRN.phl[714]";
connectAttr "rigw:mesh:robo67_translateX.o" "rigwRN.phl[715]";
connectAttr "rigw:mesh:robo67_translateY.o" "rigwRN.phl[716]";
connectAttr "rigw:mesh:robo67_translateZ.o" "rigwRN.phl[717]";
connectAttr "rigw:mesh:robo67_rotateX.o" "rigwRN.phl[718]";
connectAttr "rigw:mesh:robo67_rotateY.o" "rigwRN.phl[719]";
connectAttr "rigw:mesh:robo67_rotateZ.o" "rigwRN.phl[720]";
connectAttr "rigw:mesh:robo67_scaleX.o" "rigwRN.phl[721]";
connectAttr "rigw:mesh:robo67_scaleY.o" "rigwRN.phl[722]";
connectAttr "rigw:mesh:robo67_scaleZ.o" "rigwRN.phl[723]";
connectAttr "rigw:mesh:robo68_visibility.o" "rigwRN.phl[724]";
connectAttr "rigw:mesh:robo68_translateX.o" "rigwRN.phl[725]";
connectAttr "rigw:mesh:robo68_translateY.o" "rigwRN.phl[726]";
connectAttr "rigw:mesh:robo68_translateZ.o" "rigwRN.phl[727]";
connectAttr "rigw:mesh:robo68_rotateX.o" "rigwRN.phl[728]";
connectAttr "rigw:mesh:robo68_rotateY.o" "rigwRN.phl[729]";
connectAttr "rigw:mesh:robo68_rotateZ.o" "rigwRN.phl[730]";
connectAttr "rigw:mesh:robo68_scaleX.o" "rigwRN.phl[731]";
connectAttr "rigw:mesh:robo68_scaleY.o" "rigwRN.phl[732]";
connectAttr "rigw:mesh:robo68_scaleZ.o" "rigwRN.phl[733]";
connectAttr "rigw:mesh:robo69_visibility.o" "rigwRN.phl[734]";
connectAttr "rigw:mesh:robo69_translateX.o" "rigwRN.phl[735]";
connectAttr "rigw:mesh:robo69_translateY.o" "rigwRN.phl[736]";
connectAttr "rigw:mesh:robo69_translateZ.o" "rigwRN.phl[737]";
connectAttr "rigw:mesh:robo69_rotateX.o" "rigwRN.phl[738]";
connectAttr "rigw:mesh:robo69_rotateY.o" "rigwRN.phl[739]";
connectAttr "rigw:mesh:robo69_rotateZ.o" "rigwRN.phl[740]";
connectAttr "rigw:mesh:robo69_scaleX.o" "rigwRN.phl[741]";
connectAttr "rigw:mesh:robo69_scaleY.o" "rigwRN.phl[742]";
connectAttr "rigw:mesh:robo69_scaleZ.o" "rigwRN.phl[743]";
connectAttr "rigw:mesh:robo70_visibility.o" "rigwRN.phl[744]";
connectAttr "rigw:mesh:robo70_translateX.o" "rigwRN.phl[745]";
connectAttr "rigw:mesh:robo70_translateY.o" "rigwRN.phl[746]";
connectAttr "rigw:mesh:robo70_translateZ.o" "rigwRN.phl[747]";
connectAttr "rigw:mesh:robo70_rotateX.o" "rigwRN.phl[748]";
connectAttr "rigw:mesh:robo70_rotateY.o" "rigwRN.phl[749]";
connectAttr "rigw:mesh:robo70_rotateZ.o" "rigwRN.phl[750]";
connectAttr "rigw:mesh:robo70_scaleX.o" "rigwRN.phl[751]";
connectAttr "rigw:mesh:robo70_scaleY.o" "rigwRN.phl[752]";
connectAttr "rigw:mesh:robo70_scaleZ.o" "rigwRN.phl[753]";
connectAttr "rigw:mesh:robo71_visibility.o" "rigwRN.phl[754]";
connectAttr "rigw:mesh:robo71_translateX.o" "rigwRN.phl[755]";
connectAttr "rigw:mesh:robo71_translateY.o" "rigwRN.phl[756]";
connectAttr "rigw:mesh:robo71_translateZ.o" "rigwRN.phl[757]";
connectAttr "rigw:mesh:robo71_rotateX.o" "rigwRN.phl[758]";
connectAttr "rigw:mesh:robo71_rotateY.o" "rigwRN.phl[759]";
connectAttr "rigw:mesh:robo71_rotateZ.o" "rigwRN.phl[760]";
connectAttr "rigw:mesh:robo71_scaleX.o" "rigwRN.phl[761]";
connectAttr "rigw:mesh:robo71_scaleY.o" "rigwRN.phl[762]";
connectAttr "rigw:mesh:robo71_scaleZ.o" "rigwRN.phl[763]";
connectAttr "rigw:mesh:robo102_visibility.o" "rigwRN.phl[764]";
connectAttr "rigw:mesh:robo102_translateX.o" "rigwRN.phl[765]";
connectAttr "rigw:mesh:robo102_translateY.o" "rigwRN.phl[766]";
connectAttr "rigw:mesh:robo102_translateZ.o" "rigwRN.phl[767]";
connectAttr "rigw:mesh:robo102_rotateX.o" "rigwRN.phl[768]";
connectAttr "rigw:mesh:robo102_rotateY.o" "rigwRN.phl[769]";
connectAttr "rigw:mesh:robo102_rotateZ.o" "rigwRN.phl[770]";
connectAttr "rigw:mesh:robo102_scaleX.o" "rigwRN.phl[771]";
connectAttr "rigw:mesh:robo102_scaleY.o" "rigwRN.phl[772]";
connectAttr "rigw:mesh:robo102_scaleZ.o" "rigwRN.phl[773]";
connectAttr "rigw:mesh:robo101_visibility.o" "rigwRN.phl[774]";
connectAttr "rigw:mesh:robo101_translateX.o" "rigwRN.phl[775]";
connectAttr "rigw:mesh:robo101_translateY.o" "rigwRN.phl[776]";
connectAttr "rigw:mesh:robo101_translateZ.o" "rigwRN.phl[777]";
connectAttr "rigw:mesh:robo101_rotateX.o" "rigwRN.phl[778]";
connectAttr "rigw:mesh:robo101_rotateY.o" "rigwRN.phl[779]";
connectAttr "rigw:mesh:robo101_rotateZ.o" "rigwRN.phl[780]";
connectAttr "rigw:mesh:robo101_scaleX.o" "rigwRN.phl[781]";
connectAttr "rigw:mesh:robo101_scaleY.o" "rigwRN.phl[782]";
connectAttr "rigw:mesh:robo101_scaleZ.o" "rigwRN.phl[783]";
connectAttr "rigw:mesh:robo100_visibility.o" "rigwRN.phl[784]";
connectAttr "rigw:mesh:robo100_translateX.o" "rigwRN.phl[785]";
connectAttr "rigw:mesh:robo100_translateY.o" "rigwRN.phl[786]";
connectAttr "rigw:mesh:robo100_translateZ.o" "rigwRN.phl[787]";
connectAttr "rigw:mesh:robo100_rotateX.o" "rigwRN.phl[788]";
connectAttr "rigw:mesh:robo100_rotateY.o" "rigwRN.phl[789]";
connectAttr "rigw:mesh:robo100_rotateZ.o" "rigwRN.phl[790]";
connectAttr "rigw:mesh:robo100_scaleX.o" "rigwRN.phl[791]";
connectAttr "rigw:mesh:robo100_scaleY.o" "rigwRN.phl[792]";
connectAttr "rigw:mesh:robo100_scaleZ.o" "rigwRN.phl[793]";
connectAttr "rigw:mesh:robo99_visibility.o" "rigwRN.phl[794]";
connectAttr "rigw:mesh:robo99_translateX.o" "rigwRN.phl[795]";
connectAttr "rigw:mesh:robo99_translateY.o" "rigwRN.phl[796]";
connectAttr "rigw:mesh:robo99_translateZ.o" "rigwRN.phl[797]";
connectAttr "rigw:mesh:robo99_rotateX.o" "rigwRN.phl[798]";
connectAttr "rigw:mesh:robo99_rotateY.o" "rigwRN.phl[799]";
connectAttr "rigw:mesh:robo99_rotateZ.o" "rigwRN.phl[800]";
connectAttr "rigw:mesh:robo99_scaleX.o" "rigwRN.phl[801]";
connectAttr "rigw:mesh:robo99_scaleY.o" "rigwRN.phl[802]";
connectAttr "rigw:mesh:robo99_scaleZ.o" "rigwRN.phl[803]";
connectAttr "rigw:mesh:robo98_visibility.o" "rigwRN.phl[804]";
connectAttr "rigw:mesh:robo98_translateX.o" "rigwRN.phl[805]";
connectAttr "rigw:mesh:robo98_translateY.o" "rigwRN.phl[806]";
connectAttr "rigw:mesh:robo98_translateZ.o" "rigwRN.phl[807]";
connectAttr "rigw:mesh:robo98_rotateX.o" "rigwRN.phl[808]";
connectAttr "rigw:mesh:robo98_rotateY.o" "rigwRN.phl[809]";
connectAttr "rigw:mesh:robo98_rotateZ.o" "rigwRN.phl[810]";
connectAttr "rigw:mesh:robo98_scaleX.o" "rigwRN.phl[811]";
connectAttr "rigw:mesh:robo98_scaleY.o" "rigwRN.phl[812]";
connectAttr "rigw:mesh:robo98_scaleZ.o" "rigwRN.phl[813]";
connectAttr "rigw:mesh:robo97_visibility.o" "rigwRN.phl[814]";
connectAttr "rigw:mesh:robo97_translateX.o" "rigwRN.phl[815]";
connectAttr "rigw:mesh:robo97_translateY.o" "rigwRN.phl[816]";
connectAttr "rigw:mesh:robo97_translateZ.o" "rigwRN.phl[817]";
connectAttr "rigw:mesh:robo97_rotateX.o" "rigwRN.phl[818]";
connectAttr "rigw:mesh:robo97_rotateY.o" "rigwRN.phl[819]";
connectAttr "rigw:mesh:robo97_rotateZ.o" "rigwRN.phl[820]";
connectAttr "rigw:mesh:robo97_scaleX.o" "rigwRN.phl[821]";
connectAttr "rigw:mesh:robo97_scaleY.o" "rigwRN.phl[822]";
connectAttr "rigw:mesh:robo97_scaleZ.o" "rigwRN.phl[823]";
connectAttr "rigw:mesh:robo96_visibility.o" "rigwRN.phl[824]";
connectAttr "rigw:mesh:robo96_translateX.o" "rigwRN.phl[825]";
connectAttr "rigw:mesh:robo96_translateY.o" "rigwRN.phl[826]";
connectAttr "rigw:mesh:robo96_translateZ.o" "rigwRN.phl[827]";
connectAttr "rigw:mesh:robo96_rotateX.o" "rigwRN.phl[828]";
connectAttr "rigw:mesh:robo96_rotateY.o" "rigwRN.phl[829]";
connectAttr "rigw:mesh:robo96_rotateZ.o" "rigwRN.phl[830]";
connectAttr "rigw:mesh:robo96_scaleX.o" "rigwRN.phl[831]";
connectAttr "rigw:mesh:robo96_scaleY.o" "rigwRN.phl[832]";
connectAttr "rigw:mesh:robo96_scaleZ.o" "rigwRN.phl[833]";
connectAttr "rigw:mesh:robo95_visibility.o" "rigwRN.phl[834]";
connectAttr "rigw:mesh:robo95_translateX.o" "rigwRN.phl[835]";
connectAttr "rigw:mesh:robo95_translateY.o" "rigwRN.phl[836]";
connectAttr "rigw:mesh:robo95_translateZ.o" "rigwRN.phl[837]";
connectAttr "rigw:mesh:robo95_rotateX.o" "rigwRN.phl[838]";
connectAttr "rigw:mesh:robo95_rotateY.o" "rigwRN.phl[839]";
connectAttr "rigw:mesh:robo95_rotateZ.o" "rigwRN.phl[840]";
connectAttr "rigw:mesh:robo95_scaleX.o" "rigwRN.phl[841]";
connectAttr "rigw:mesh:robo95_scaleY.o" "rigwRN.phl[842]";
connectAttr "rigw:mesh:robo95_scaleZ.o" "rigwRN.phl[843]";
connectAttr "rigw:mesh:robo94_visibility.o" "rigwRN.phl[844]";
connectAttr "rigw:mesh:robo94_translateX.o" "rigwRN.phl[845]";
connectAttr "rigw:mesh:robo94_translateY.o" "rigwRN.phl[846]";
connectAttr "rigw:mesh:robo94_translateZ.o" "rigwRN.phl[847]";
connectAttr "rigw:mesh:robo94_rotateX.o" "rigwRN.phl[848]";
connectAttr "rigw:mesh:robo94_rotateY.o" "rigwRN.phl[849]";
connectAttr "rigw:mesh:robo94_rotateZ.o" "rigwRN.phl[850]";
connectAttr "rigw:mesh:robo94_scaleX.o" "rigwRN.phl[851]";
connectAttr "rigw:mesh:robo94_scaleY.o" "rigwRN.phl[852]";
connectAttr "rigw:mesh:robo94_scaleZ.o" "rigwRN.phl[853]";
connectAttr "rigw:mesh:robo93_visibility.o" "rigwRN.phl[854]";
connectAttr "rigw:mesh:robo93_translateX.o" "rigwRN.phl[855]";
connectAttr "rigw:mesh:robo93_translateY.o" "rigwRN.phl[856]";
connectAttr "rigw:mesh:robo93_translateZ.o" "rigwRN.phl[857]";
connectAttr "rigw:mesh:robo93_rotateX.o" "rigwRN.phl[858]";
connectAttr "rigw:mesh:robo93_rotateY.o" "rigwRN.phl[859]";
connectAttr "rigw:mesh:robo93_rotateZ.o" "rigwRN.phl[860]";
connectAttr "rigw:mesh:robo93_scaleX.o" "rigwRN.phl[861]";
connectAttr "rigw:mesh:robo93_scaleY.o" "rigwRN.phl[862]";
connectAttr "rigw:mesh:robo93_scaleZ.o" "rigwRN.phl[863]";
connectAttr "rigw:mesh:robo92_visibility.o" "rigwRN.phl[864]";
connectAttr "rigw:mesh:robo92_translateX.o" "rigwRN.phl[865]";
connectAttr "rigw:mesh:robo92_translateY.o" "rigwRN.phl[866]";
connectAttr "rigw:mesh:robo92_translateZ.o" "rigwRN.phl[867]";
connectAttr "rigw:mesh:robo92_rotateX.o" "rigwRN.phl[868]";
connectAttr "rigw:mesh:robo92_rotateY.o" "rigwRN.phl[869]";
connectAttr "rigw:mesh:robo92_rotateZ.o" "rigwRN.phl[870]";
connectAttr "rigw:mesh:robo92_scaleX.o" "rigwRN.phl[871]";
connectAttr "rigw:mesh:robo92_scaleY.o" "rigwRN.phl[872]";
connectAttr "rigw:mesh:robo92_scaleZ.o" "rigwRN.phl[873]";
connectAttr "rigw:mesh:robo91_visibility.o" "rigwRN.phl[874]";
connectAttr "rigw:mesh:robo91_translateX.o" "rigwRN.phl[875]";
connectAttr "rigw:mesh:robo91_translateY.o" "rigwRN.phl[876]";
connectAttr "rigw:mesh:robo91_translateZ.o" "rigwRN.phl[877]";
connectAttr "rigw:mesh:robo91_rotateX.o" "rigwRN.phl[878]";
connectAttr "rigw:mesh:robo91_rotateY.o" "rigwRN.phl[879]";
connectAttr "rigw:mesh:robo91_rotateZ.o" "rigwRN.phl[880]";
connectAttr "rigw:mesh:robo91_scaleX.o" "rigwRN.phl[881]";
connectAttr "rigw:mesh:robo91_scaleY.o" "rigwRN.phl[882]";
connectAttr "rigw:mesh:robo91_scaleZ.o" "rigwRN.phl[883]";
connectAttr "rigw:mesh:robo90_visibility.o" "rigwRN.phl[884]";
connectAttr "rigw:mesh:robo90_translateX.o" "rigwRN.phl[885]";
connectAttr "rigw:mesh:robo90_translateY.o" "rigwRN.phl[886]";
connectAttr "rigw:mesh:robo90_translateZ.o" "rigwRN.phl[887]";
connectAttr "rigw:mesh:robo90_rotateX.o" "rigwRN.phl[888]";
connectAttr "rigw:mesh:robo90_rotateY.o" "rigwRN.phl[889]";
connectAttr "rigw:mesh:robo90_rotateZ.o" "rigwRN.phl[890]";
connectAttr "rigw:mesh:robo90_scaleX.o" "rigwRN.phl[891]";
connectAttr "rigw:mesh:robo90_scaleY.o" "rigwRN.phl[892]";
connectAttr "rigw:mesh:robo90_scaleZ.o" "rigwRN.phl[893]";
connectAttr "rigw:mesh:robo89_visibility.o" "rigwRN.phl[894]";
connectAttr "rigw:mesh:robo89_translateX.o" "rigwRN.phl[895]";
connectAttr "rigw:mesh:robo89_translateY.o" "rigwRN.phl[896]";
connectAttr "rigw:mesh:robo89_translateZ.o" "rigwRN.phl[897]";
connectAttr "rigw:mesh:robo89_rotateX.o" "rigwRN.phl[898]";
connectAttr "rigw:mesh:robo89_rotateY.o" "rigwRN.phl[899]";
connectAttr "rigw:mesh:robo89_rotateZ.o" "rigwRN.phl[900]";
connectAttr "rigw:mesh:robo89_scaleX.o" "rigwRN.phl[901]";
connectAttr "rigw:mesh:robo89_scaleY.o" "rigwRN.phl[902]";
connectAttr "rigw:mesh:robo89_scaleZ.o" "rigwRN.phl[903]";
connectAttr "rigw:mesh:robo88_visibility.o" "rigwRN.phl[904]";
connectAttr "rigw:mesh:robo88_translateX.o" "rigwRN.phl[905]";
connectAttr "rigw:mesh:robo88_translateY.o" "rigwRN.phl[906]";
connectAttr "rigw:mesh:robo88_translateZ.o" "rigwRN.phl[907]";
connectAttr "rigw:mesh:robo88_rotateX.o" "rigwRN.phl[908]";
connectAttr "rigw:mesh:robo88_rotateY.o" "rigwRN.phl[909]";
connectAttr "rigw:mesh:robo88_rotateZ.o" "rigwRN.phl[910]";
connectAttr "rigw:mesh:robo88_scaleX.o" "rigwRN.phl[911]";
connectAttr "rigw:mesh:robo88_scaleY.o" "rigwRN.phl[912]";
connectAttr "rigw:mesh:robo88_scaleZ.o" "rigwRN.phl[913]";
connectAttr "rigw:mesh:robo87_visibility.o" "rigwRN.phl[914]";
connectAttr "rigw:mesh:robo87_translateX.o" "rigwRN.phl[915]";
connectAttr "rigw:mesh:robo87_translateY.o" "rigwRN.phl[916]";
connectAttr "rigw:mesh:robo87_translateZ.o" "rigwRN.phl[917]";
connectAttr "rigw:mesh:robo87_rotateX.o" "rigwRN.phl[918]";
connectAttr "rigw:mesh:robo87_rotateY.o" "rigwRN.phl[919]";
connectAttr "rigw:mesh:robo87_rotateZ.o" "rigwRN.phl[920]";
connectAttr "rigw:mesh:robo87_scaleX.o" "rigwRN.phl[921]";
connectAttr "rigw:mesh:robo87_scaleY.o" "rigwRN.phl[922]";
connectAttr "rigw:mesh:robo87_scaleZ.o" "rigwRN.phl[923]";
connectAttr "rigw:mesh:robo86_visibility.o" "rigwRN.phl[924]";
connectAttr "rigw:mesh:robo86_translateX.o" "rigwRN.phl[925]";
connectAttr "rigw:mesh:robo86_translateY.o" "rigwRN.phl[926]";
connectAttr "rigw:mesh:robo86_translateZ.o" "rigwRN.phl[927]";
connectAttr "rigw:mesh:robo86_rotateX.o" "rigwRN.phl[928]";
connectAttr "rigw:mesh:robo86_rotateY.o" "rigwRN.phl[929]";
connectAttr "rigw:mesh:robo86_rotateZ.o" "rigwRN.phl[930]";
connectAttr "rigw:mesh:robo86_scaleX.o" "rigwRN.phl[931]";
connectAttr "rigw:mesh:robo86_scaleY.o" "rigwRN.phl[932]";
connectAttr "rigw:mesh:robo86_scaleZ.o" "rigwRN.phl[933]";
connectAttr "rigw:mesh:robo107_visibility.o" "rigwRN.phl[934]";
connectAttr "rigw:mesh:robo107_translateX.o" "rigwRN.phl[935]";
connectAttr "rigw:mesh:robo107_translateY.o" "rigwRN.phl[936]";
connectAttr "rigw:mesh:robo107_translateZ.o" "rigwRN.phl[937]";
connectAttr "rigw:mesh:robo107_rotateX.o" "rigwRN.phl[938]";
connectAttr "rigw:mesh:robo107_rotateY.o" "rigwRN.phl[939]";
connectAttr "rigw:mesh:robo107_rotateZ.o" "rigwRN.phl[940]";
connectAttr "rigw:mesh:robo107_scaleX.o" "rigwRN.phl[941]";
connectAttr "rigw:mesh:robo107_scaleY.o" "rigwRN.phl[942]";
connectAttr "rigw:mesh:robo107_scaleZ.o" "rigwRN.phl[943]";
connectAttr "rigw:mesh:robo108_visibility.o" "rigwRN.phl[944]";
connectAttr "rigw:mesh:robo108_translateX.o" "rigwRN.phl[945]";
connectAttr "rigw:mesh:robo108_translateY.o" "rigwRN.phl[946]";
connectAttr "rigw:mesh:robo108_translateZ.o" "rigwRN.phl[947]";
connectAttr "rigw:mesh:robo108_rotateX.o" "rigwRN.phl[948]";
connectAttr "rigw:mesh:robo108_rotateY.o" "rigwRN.phl[949]";
connectAttr "rigw:mesh:robo108_rotateZ.o" "rigwRN.phl[950]";
connectAttr "rigw:mesh:robo108_scaleX.o" "rigwRN.phl[951]";
connectAttr "rigw:mesh:robo108_scaleY.o" "rigwRN.phl[952]";
connectAttr "rigw:mesh:robo108_scaleZ.o" "rigwRN.phl[953]";
connectAttr "rigw:mesh:robo109_visibility.o" "rigwRN.phl[954]";
connectAttr "rigw:mesh:robo109_translateX.o" "rigwRN.phl[955]";
connectAttr "rigw:mesh:robo109_translateY.o" "rigwRN.phl[956]";
connectAttr "rigw:mesh:robo109_translateZ.o" "rigwRN.phl[957]";
connectAttr "rigw:mesh:robo109_rotateX.o" "rigwRN.phl[958]";
connectAttr "rigw:mesh:robo109_rotateY.o" "rigwRN.phl[959]";
connectAttr "rigw:mesh:robo109_rotateZ.o" "rigwRN.phl[960]";
connectAttr "rigw:mesh:robo109_scaleX.o" "rigwRN.phl[961]";
connectAttr "rigw:mesh:robo109_scaleY.o" "rigwRN.phl[962]";
connectAttr "rigw:mesh:robo109_scaleZ.o" "rigwRN.phl[963]";
connectAttr "rigw:mesh:robo110_visibility.o" "rigwRN.phl[964]";
connectAttr "rigw:mesh:robo110_translateX.o" "rigwRN.phl[965]";
connectAttr "rigw:mesh:robo110_translateY.o" "rigwRN.phl[966]";
connectAttr "rigw:mesh:robo110_translateZ.o" "rigwRN.phl[967]";
connectAttr "rigw:mesh:robo110_rotateX.o" "rigwRN.phl[968]";
connectAttr "rigw:mesh:robo110_rotateY.o" "rigwRN.phl[969]";
connectAttr "rigw:mesh:robo110_rotateZ.o" "rigwRN.phl[970]";
connectAttr "rigw:mesh:robo110_scaleX.o" "rigwRN.phl[971]";
connectAttr "rigw:mesh:robo110_scaleY.o" "rigwRN.phl[972]";
connectAttr "rigw:mesh:robo110_scaleZ.o" "rigwRN.phl[973]";
connectAttr "rigw:mesh:robo111_visibility.o" "rigwRN.phl[974]";
connectAttr "rigw:mesh:robo111_translateX.o" "rigwRN.phl[975]";
connectAttr "rigw:mesh:robo111_translateY.o" "rigwRN.phl[976]";
connectAttr "rigw:mesh:robo111_translateZ.o" "rigwRN.phl[977]";
connectAttr "rigw:mesh:robo111_rotateX.o" "rigwRN.phl[978]";
connectAttr "rigw:mesh:robo111_rotateY.o" "rigwRN.phl[979]";
connectAttr "rigw:mesh:robo111_rotateZ.o" "rigwRN.phl[980]";
connectAttr "rigw:mesh:robo111_scaleX.o" "rigwRN.phl[981]";
connectAttr "rigw:mesh:robo111_scaleY.o" "rigwRN.phl[982]";
connectAttr "rigw:mesh:robo111_scaleZ.o" "rigwRN.phl[983]";
connectAttr "rigw:mesh:robo104_visibility.o" "rigwRN.phl[984]";
connectAttr "rigw:mesh:robo104_translateX.o" "rigwRN.phl[985]";
connectAttr "rigw:mesh:robo104_translateY.o" "rigwRN.phl[986]";
connectAttr "rigw:mesh:robo104_translateZ.o" "rigwRN.phl[987]";
connectAttr "rigw:mesh:robo104_rotateX.o" "rigwRN.phl[988]";
connectAttr "rigw:mesh:robo104_rotateY.o" "rigwRN.phl[989]";
connectAttr "rigw:mesh:robo104_rotateZ.o" "rigwRN.phl[990]";
connectAttr "rigw:mesh:robo104_scaleX.o" "rigwRN.phl[991]";
connectAttr "rigw:mesh:robo104_scaleY.o" "rigwRN.phl[992]";
connectAttr "rigw:mesh:robo104_scaleZ.o" "rigwRN.phl[993]";
connectAttr "rigw:mesh:robo103_visibility.o" "rigwRN.phl[994]";
connectAttr "rigw:mesh:robo103_translateX.o" "rigwRN.phl[995]";
connectAttr "rigw:mesh:robo103_translateY.o" "rigwRN.phl[996]";
connectAttr "rigw:mesh:robo103_translateZ.o" "rigwRN.phl[997]";
connectAttr "rigw:mesh:robo103_rotateX.o" "rigwRN.phl[998]";
connectAttr "rigw:mesh:robo103_rotateY.o" "rigwRN.phl[999]";
connectAttr "rigw:mesh:robo103_rotateZ.o" "rigwRN.phl[1000]";
connectAttr "rigw:mesh:robo103_scaleX.o" "rigwRN.phl[1001]";
connectAttr "rigw:mesh:robo103_scaleY.o" "rigwRN.phl[1002]";
connectAttr "rigw:mesh:robo103_scaleZ.o" "rigwRN.phl[1003]";
connectAttr "rigw:rig:Main_translateX.o" "rigwRN.phl[259]";
connectAttr "rigw:rig:Main_translateY.o" "rigwRN.phl[260]";
connectAttr "rigw:rig:Main_translateZ.o" "rigwRN.phl[261]";
connectAttr "rigw:rig:Main_visibility.o" "rigwRN.phl[262]";
connectAttr "rigw:rig:Main_rotateX.o" "rigwRN.phl[263]";
connectAttr "rigw:rig:Main_rotateY.o" "rigwRN.phl[264]";
connectAttr "rigw:rig:Main_rotateZ.o" "rigwRN.phl[265]";
connectAttr "rigw:rig:Main_scaleX.o" "rigwRN.phl[266]";
connectAttr "rigw:rig:Main_scaleY.o" "rigwRN.phl[267]";
connectAttr "rigw:rig:Main_scaleZ.o" "rigwRN.phl[268]";
connectAttr "rigw:rig:FKWheel1_R_rotateX.o" "rigwRN.phl[269]";
connectAttr "rigw:rig:FKWheel1_R_rotateY.o" "rigwRN.phl[270]";
connectAttr "rigw:rig:FKWheel1_R_rotateZ.o" "rigwRN.phl[271]";
connectAttr "rigw:rig:FKWheel2_R_rotateX.o" "rigwRN.phl[272]";
connectAttr "rigw:rig:FKWheel2_R_rotateY.o" "rigwRN.phl[273]";
connectAttr "rigw:rig:FKWheel2_R_rotateZ.o" "rigwRN.phl[274]";
connectAttr "rigw:rig:FKWheel3_R_rotateX.o" "rigwRN.phl[275]";
connectAttr "rigw:rig:FKWheel3_R_rotateY.o" "rigwRN.phl[276]";
connectAttr "rigw:rig:FKWheel3_R_rotateZ.o" "rigwRN.phl[277]";
connectAttr "rigw:rig:FKWheel4_R_rotateX.o" "rigwRN.phl[278]";
connectAttr "rigw:rig:FKWheel4_R_rotateY.o" "rigwRN.phl[279]";
connectAttr "rigw:rig:FKWheel4_R_rotateZ.o" "rigwRN.phl[280]";
connectAttr "rigw:rig:FKWheel1_L_rotateX.o" "rigwRN.phl[281]";
connectAttr "rigw:rig:FKWheel1_L_rotateY.o" "rigwRN.phl[282]";
connectAttr "rigw:rig:FKWheel1_L_rotateZ.o" "rigwRN.phl[283]";
connectAttr "rigw:rig:FKWheel2_L_rotateX.o" "rigwRN.phl[284]";
connectAttr "rigw:rig:FKWheel2_L_rotateY.o" "rigwRN.phl[285]";
connectAttr "rigw:rig:FKWheel2_L_rotateZ.o" "rigwRN.phl[286]";
connectAttr "rigw:rig:FKWheel3_L_rotateX.o" "rigwRN.phl[287]";
connectAttr "rigw:rig:FKWheel3_L_rotateY.o" "rigwRN.phl[288]";
connectAttr "rigw:rig:FKWheel3_L_rotateZ.o" "rigwRN.phl[289]";
connectAttr "rigw:rig:FKWheel4_L_rotateX.o" "rigwRN.phl[290]";
connectAttr "rigw:rig:FKWheel4_L_rotateY.o" "rigwRN.phl[291]";
connectAttr "rigw:rig:FKWheel4_L_rotateZ.o" "rigwRN.phl[292]";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[293]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[294]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[295]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[296]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[297]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[298]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[299]";
connectAttr "rigw:rig:FKBody_M_translateX.o" "rigwRN.phl[300]";
connectAttr "rigw:rig:FKBody_M_translateY.o" "rigwRN.phl[301]";
connectAttr "rigw:rig:FKBody_M_translateZ.o" "rigwRN.phl[302]";
connectAttr "rigw:rig:FKBody_M_rotateX.o" "rigwRN.phl[303]";
connectAttr "rigw:rig:FKBody_M_rotateY.o" "rigwRN.phl[304]";
connectAttr "rigw:rig:FKBody_M_rotateZ.o" "rigwRN.phl[305]";
connectAttr "rigw:rig:FKHead_M_translateX.o" "rigwRN.phl[306]";
connectAttr "rigw:rig:FKHead_M_translateY.o" "rigwRN.phl[307]";
connectAttr "rigw:rig:FKHead_M_translateZ.o" "rigwRN.phl[308]";
connectAttr "rigw:rig:FKHead_M_rotateX.o" "rigwRN.phl[309]";
connectAttr "rigw:rig:FKHead_M_rotateY.o" "rigwRN.phl[310]";
connectAttr "rigw:rig:FKHead_M_rotateZ.o" "rigwRN.phl[311]";
connectAttr "rigw:rig:FKShoulder_R_Global.o" "rigwRN.phl[312]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[313]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[314]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[315]";
connectAttr "rigw:rig:FKElbow5_R_rotateX.o" "rigwRN.phl[316]";
connectAttr "rigw:rig:FKElbow5_R_rotateY.o" "rigwRN.phl[317]";
connectAttr "rigw:rig:FKElbow5_R_rotateZ.o" "rigwRN.phl[318]";
connectAttr "rigw:rig:FKWrist1_R_translateX.o" "rigwRN.phl[319]";
connectAttr "rigw:rig:FKWrist1_R_translateY.o" "rigwRN.phl[320]";
connectAttr "rigw:rig:FKWrist1_R_translateZ.o" "rigwRN.phl[321]";
connectAttr "rigw:rig:FKWrist1_R_rotateX.o" "rigwRN.phl[322]";
connectAttr "rigw:rig:FKWrist1_R_rotateY.o" "rigwRN.phl[323]";
connectAttr "rigw:rig:FKWrist1_R_rotateZ.o" "rigwRN.phl[324]";
connectAttr "rigw:rig:FKShoulder1_L_Global.o" "rigwRN.phl[325]";
connectAttr "rigw:rig:FKShoulder1_L_rotateX.o" "rigwRN.phl[326]";
connectAttr "rigw:rig:FKShoulder1_L_rotateY.o" "rigwRN.phl[327]";
connectAttr "rigw:rig:FKShoulder1_L_rotateZ.o" "rigwRN.phl[328]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[329]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[330]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[331]";
connectAttr "rigw:rig:FKWrist_L_rotateX.o" "rigwRN.phl[332]";
connectAttr "rigw:rig:FKWrist_L_rotateY.o" "rigwRN.phl[333]";
connectAttr "rigw:rig:FKWrist_L_rotateZ.o" "rigwRN.phl[334]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateX.o" "rigwRN.phl[335]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateY.o" "rigwRN.phl[336]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateZ.o" "rigwRN.phl[337]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateX.o" "rigwRN.phl[338]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateY.o" "rigwRN.phl[339]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateZ.o" "rigwRN.phl[340]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateX.o" "rigwRN.phl[341]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateY.o" "rigwRN.phl[342]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateZ.o" "rigwRN.phl[343]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateX.o" "rigwRN.phl[344]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateY.o" "rigwRN.phl[345]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateZ.o" "rigwRN.phl[346]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateX.o" "rigwRN.phl[347]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateY.o" "rigwRN.phl[348]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateZ.o" "rigwRN.phl[349]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateX.o" "rigwRN.phl[350]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateY.o" "rigwRN.phl[351]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateZ.o" "rigwRN.phl[352]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateX.o" "rigwRN.phl[353]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateY.o" "rigwRN.phl[354]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateZ.o" "rigwRN.phl[355]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateX.o" "rigwRN.phl[356]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateY.o" "rigwRN.phl[357]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateZ.o" "rigwRN.phl[358]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateX.o" "rigwRN.phl[359]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateY.o" "rigwRN.phl[360]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateZ.o" "rigwRN.phl[361]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateX.o" "rigwRN.phl[362]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateY.o" "rigwRN.phl[363]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateZ.o" "rigwRN.phl[364]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateX.o" "rigwRN.phl[365]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateY.o" "rigwRN.phl[366]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateZ.o" "rigwRN.phl[367]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateX.o" "rigwRN.phl[368]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateY.o" "rigwRN.phl[369]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateZ.o" "rigwRN.phl[370]";
connectAttr "rigw:rig:FKHose1_R_translateX.o" "rigwRN.phl[371]";
connectAttr "rigw:rig:FKHose1_R_translateY.o" "rigwRN.phl[372]";
connectAttr "rigw:rig:FKHose1_R_translateZ.o" "rigwRN.phl[373]";
connectAttr "rigw:rig:FKHose1_R_rotateX.o" "rigwRN.phl[374]";
connectAttr "rigw:rig:FKHose1_R_rotateY.o" "rigwRN.phl[375]";
connectAttr "rigw:rig:FKHose1_R_rotateZ.o" "rigwRN.phl[376]";
connectAttr "rigw:rig:FKHose2_R_translateX.o" "rigwRN.phl[377]";
connectAttr "rigw:rig:FKHose2_R_translateY.o" "rigwRN.phl[378]";
connectAttr "rigw:rig:FKHose2_R_translateZ.o" "rigwRN.phl[379]";
connectAttr "rigw:rig:FKHose2_R_rotateX.o" "rigwRN.phl[380]";
connectAttr "rigw:rig:FKHose2_R_rotateY.o" "rigwRN.phl[381]";
connectAttr "rigw:rig:FKHose2_R_rotateZ.o" "rigwRN.phl[382]";
connectAttr "rigw:rig:FKHose1_L_rotateX.o" "rigwRN.phl[383]";
connectAttr "rigw:rig:FKHose1_L_rotateY.o" "rigwRN.phl[384]";
connectAttr "rigw:rig:FKHose1_L_rotateZ.o" "rigwRN.phl[385]";
connectAttr "rigw:rig:FKHose2_L_rotateX.o" "rigwRN.phl[386]";
connectAttr "rigw:rig:FKHose2_L_rotateY.o" "rigwRN.phl[387]";
connectAttr "rigw:rig:FKHose2_L_rotateZ.o" "rigwRN.phl[388]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[389]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[390]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[391]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[392]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[393]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[394]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[395]";
connectAttr "rigw:rig:IKLeg_R_rollAngle.o" "rigwRN.phl[396]";
connectAttr "rigw:rig:IKLeg_R_roll.o" "rigwRN.phl[397]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[398]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[399]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[400]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[401]";
connectAttr "rigw:rig:IKLegHeel_R_rotateX.o" "rigwRN.phl[402]";
connectAttr "rigw:rig:IKLegHeel_R_rotateY.o" "rigwRN.phl[403]";
connectAttr "rigw:rig:IKLegHeel_R_rotateZ.o" "rigwRN.phl[404]";
connectAttr "rigw:rig:IKLegBall_R_rotateX.o" "rigwRN.phl[405]";
connectAttr "rigw:rig:PoleLeg_R_translateX.o" "rigwRN.phl[406]";
connectAttr "rigw:rig:PoleLeg_R_translateY.o" "rigwRN.phl[407]";
connectAttr "rigw:rig:PoleLeg_R_translateZ.o" "rigwRN.phl[408]";
connectAttr "rigw:rig:PoleLeg_R_follow.o" "rigwRN.phl[409]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[410]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[411]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[412]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[413]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[414]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[415]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[416]";
connectAttr "rigw:rig:IKLeg_L_rollAngle.o" "rigwRN.phl[417]";
connectAttr "rigw:rig:IKLeg_L_roll.o" "rigwRN.phl[418]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[419]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[420]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[421]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[422]";
connectAttr "rigw:rig:IKLegHeel_L_rotateX.o" "rigwRN.phl[423]";
connectAttr "rigw:rig:IKLegHeel_L_rotateY.o" "rigwRN.phl[424]";
connectAttr "rigw:rig:IKLegHeel_L_rotateZ.o" "rigwRN.phl[425]";
connectAttr "rigw:rig:IKLegBall_L_rotateX.o" "rigwRN.phl[426]";
connectAttr "rigw:rig:PoleLeg_L_translateX.o" "rigwRN.phl[427]";
connectAttr "rigw:rig:PoleLeg_L_translateY.o" "rigwRN.phl[428]";
connectAttr "rigw:rig:PoleLeg_L_translateZ.o" "rigwRN.phl[429]";
connectAttr "rigw:rig:PoleLeg_L_follow.o" "rigwRN.phl[430]";
connectAttr "rigw:rig:FKIKLeg_R_FKIKBlend.o" "rigwRN.phl[431]";
connectAttr "rigw:rig:FKIKLeg_R_IKVis.o" "rigwRN.phl[432]";
connectAttr "rigw:rig:FKIKLeg_R_FKVis.o" "rigwRN.phl[433]";
connectAttr "rigw:rig:FKIKLeg_L_FKIKBlend.o" "rigwRN.phl[434]";
connectAttr "rigw:rig:FKIKLeg_L_IKVis.o" "rigwRN.phl[435]";
connectAttr "rigw:rig:FKIKLeg_L_FKVis.o" "rigwRN.phl[436]";
connectAttr "mia_exposure_simple1.msg" "orthCamShape.mils";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "rigw1:mesh:greydark1_matgrey_darkSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "rigw1:mesh:greydark1_matgrey_darkSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "rigw:mesh:robo27_visibility.o" "rigwRN.phl[179]";
connectAttr "rigw:mesh:robo27_translateX.o" "rigwRN.phl[180]";
connectAttr "rigw:mesh:robo27_translateY.o" "rigwRN.phl[181]";
connectAttr "rigw:mesh:robo27_translateZ.o" "rigwRN.phl[182]";
connectAttr "rigw:mesh:robo27_rotateX.o" "rigwRN.phl[183]";
connectAttr "rigw:mesh:robo27_rotateY.o" "rigwRN.phl[184]";
connectAttr "rigw:mesh:robo27_rotateZ.o" "rigwRN.phl[185]";
connectAttr "rigw:mesh:robo27_scaleX.o" "rigwRN.phl[186]";
connectAttr "rigw:mesh:robo27_scaleY.o" "rigwRN.phl[187]";
connectAttr "rigw:mesh:robo27_scaleZ.o" "rigwRN.phl[188]";
connectAttr "rigw:mesh:robo28_visibility.o" "rigwRN.phl[189]";
connectAttr "rigw:mesh:robo28_translateX.o" "rigwRN.phl[190]";
connectAttr "rigw:mesh:robo28_translateY.o" "rigwRN.phl[191]";
connectAttr "rigw:mesh:robo28_translateZ.o" "rigwRN.phl[192]";
connectAttr "rigw:mesh:robo28_rotateX.o" "rigwRN.phl[193]";
connectAttr "rigw:mesh:robo28_rotateY.o" "rigwRN.phl[194]";
connectAttr "rigw:mesh:robo28_rotateZ.o" "rigwRN.phl[195]";
connectAttr "rigw:mesh:robo28_scaleX.o" "rigwRN.phl[196]";
connectAttr "rigw:mesh:robo28_scaleY.o" "rigwRN.phl[197]";
connectAttr "rigw:mesh:robo28_scaleZ.o" "rigwRN.phl[198]";
connectAttr "rigw:mesh:robo29_visibility.o" "rigwRN.phl[199]";
connectAttr "rigw:mesh:robo29_translateX.o" "rigwRN.phl[200]";
connectAttr "rigw:mesh:robo29_translateY.o" "rigwRN.phl[201]";
connectAttr "rigw:mesh:robo29_translateZ.o" "rigwRN.phl[202]";
connectAttr "rigw:mesh:robo29_rotateX.o" "rigwRN.phl[203]";
connectAttr "rigw:mesh:robo29_rotateY.o" "rigwRN.phl[204]";
connectAttr "rigw:mesh:robo29_rotateZ.o" "rigwRN.phl[205]";
connectAttr "rigw:mesh:robo29_scaleX.o" "rigwRN.phl[206]";
connectAttr "rigw:mesh:robo29_scaleY.o" "rigwRN.phl[207]";
connectAttr "rigw:mesh:robo29_scaleZ.o" "rigwRN.phl[208]";
connectAttr "rigw:mesh:robo30_visibility.o" "rigwRN.phl[209]";
connectAttr "rigw:mesh:robo30_translateX.o" "rigwRN.phl[210]";
connectAttr "rigw:mesh:robo30_translateY.o" "rigwRN.phl[211]";
connectAttr "rigw:mesh:robo30_translateZ.o" "rigwRN.phl[212]";
connectAttr "rigw:mesh:robo30_rotateX.o" "rigwRN.phl[213]";
connectAttr "rigw:mesh:robo30_rotateY.o" "rigwRN.phl[214]";
connectAttr "rigw:mesh:robo30_rotateZ.o" "rigwRN.phl[215]";
connectAttr "rigw:mesh:robo30_scaleX.o" "rigwRN.phl[216]";
connectAttr "rigw:mesh:robo30_scaleY.o" "rigwRN.phl[217]";
connectAttr "rigw:mesh:robo30_scaleZ.o" "rigwRN.phl[218]";
connectAttr "rigw:mesh:robo56_visibility.o" "rigwRN.phl[219]";
connectAttr "rigw:mesh:robo56_translateX.o" "rigwRN.phl[220]";
connectAttr "rigw:mesh:robo56_translateY.o" "rigwRN.phl[221]";
connectAttr "rigw:mesh:robo56_translateZ.o" "rigwRN.phl[222]";
connectAttr "rigw:mesh:robo56_rotateX.o" "rigwRN.phl[223]";
connectAttr "rigw:mesh:robo56_rotateY.o" "rigwRN.phl[224]";
connectAttr "rigw:mesh:robo56_rotateZ.o" "rigwRN.phl[225]";
connectAttr "rigw:mesh:robo56_scaleX.o" "rigwRN.phl[226]";
connectAttr "rigw:mesh:robo56_scaleY.o" "rigwRN.phl[227]";
connectAttr "rigw:mesh:robo56_scaleZ.o" "rigwRN.phl[228]";
connectAttr "rigw:mesh:robo57_visibility.o" "rigwRN.phl[229]";
connectAttr "rigw:mesh:robo57_translateX.o" "rigwRN.phl[230]";
connectAttr "rigw:mesh:robo57_translateY.o" "rigwRN.phl[231]";
connectAttr "rigw:mesh:robo57_translateZ.o" "rigwRN.phl[232]";
connectAttr "rigw:mesh:robo57_rotateX.o" "rigwRN.phl[233]";
connectAttr "rigw:mesh:robo57_rotateY.o" "rigwRN.phl[234]";
connectAttr "rigw:mesh:robo57_rotateZ.o" "rigwRN.phl[235]";
connectAttr "rigw:mesh:robo57_scaleX.o" "rigwRN.phl[236]";
connectAttr "rigw:mesh:robo57_scaleY.o" "rigwRN.phl[237]";
connectAttr "rigw:mesh:robo57_scaleZ.o" "rigwRN.phl[238]";
connectAttr "rigw:mesh:robo77_visibility.o" "rigwRN.phl[239]";
connectAttr "rigw:mesh:robo77_translateX.o" "rigwRN.phl[240]";
connectAttr "rigw:mesh:robo77_translateY.o" "rigwRN.phl[241]";
connectAttr "rigw:mesh:robo77_translateZ.o" "rigwRN.phl[242]";
connectAttr "rigw:mesh:robo77_rotateX.o" "rigwRN.phl[243]";
connectAttr "rigw:mesh:robo77_rotateY.o" "rigwRN.phl[244]";
connectAttr "rigw:mesh:robo77_rotateZ.o" "rigwRN.phl[245]";
connectAttr "rigw:mesh:robo77_scaleX.o" "rigwRN.phl[246]";
connectAttr "rigw:mesh:robo77_scaleY.o" "rigwRN.phl[247]";
connectAttr "rigw:mesh:robo77_scaleZ.o" "rigwRN.phl[248]";
connectAttr "rigw:mesh:robo78_visibility.o" "rigwRN.phl[249]";
connectAttr "rigw:mesh:robo78_translateX.o" "rigwRN.phl[250]";
connectAttr "rigw:mesh:robo78_translateY.o" "rigwRN.phl[251]";
connectAttr "rigw:mesh:robo78_translateZ.o" "rigwRN.phl[252]";
connectAttr "rigw:mesh:robo78_rotateX.o" "rigwRN.phl[253]";
connectAttr "rigw:mesh:robo78_rotateY.o" "rigwRN.phl[254]";
connectAttr "rigw:mesh:robo78_rotateZ.o" "rigwRN.phl[255]";
connectAttr "rigw:mesh:robo78_scaleX.o" "rigwRN.phl[256]";
connectAttr "rigw:mesh:robo78_scaleY.o" "rigwRN.phl[257]";
connectAttr "rigw:mesh:robo78_scaleZ.o" "rigwRN.phl[258]";
connectAttr "rigw1:mesh:greydark1_matgrey_darkSG.msg" "rigw1:mesh:materialInfo35.sg"
		;
connectAttr "rigw1:mesh:greydark1_matgrey_dark.msg" "rigw1:mesh:materialInfo35.m"
		;
connectAttr "rigw1:mesh:greydark1_Toon_grey1.msg" "rigw1:mesh:materialInfo35.t" 
		-na;
connectAttr "rigw1:mesh:greydark1_matgrey_dark.oc" "rigw1:mesh:greydark1_matgrey_darkSG.ss"
		;
connectAttr "rigw1:mesh:greydark1_Toon_grey1.oc" "rigw1:mesh:greydark1_matgrey_dark.c"
		;
connectAttr "rigw1:mesh:greydark1_toonredoutline5.oc" "rigw1:mesh:greydark1_Toon_grey1.c"
		;
connectAttr "rigw1:mesh:greydark1_mib_amb_occlusion14.S11" "rigw1:mesh:greydark1_Toon_grey1.ambc"
		;
connectAttr "rigw1:mesh:greydark1_place2dTexture30.o" "rigw1:mesh:greydark1_toonredoutline5.uv"
		;
connectAttr "rigw1:mesh:greydark1_samplerInfo11.fr" "rigw1:mesh:greydark1_toonredoutline5.v"
		;
connectAttr "rigw1:mesh:greydark1_samplerInfo11.fr" "rigw1:mesh:greydark1_toonredoutline5.u"
		;
connectAttr "rigw1:mesh:greydark1_place2dTexture30.ofs" "rigw1:mesh:greydark1_toonredoutline5.fs"
		;
connectAttr "rigw1:mesh:greydark1_toonred_color5.oc" "rigw1:mesh:greydark1_toonredoutline5.cel[2].ec"
		;
connectAttr "rigw1:mesh:greydark1_place2dTexture31.o" "rigw1:mesh:greydark1_toonred_color5.uv"
		;
connectAttr "rigw1:mesh:greydark1_samplerInfo12.fr" "rigw1:mesh:greydark1_toonred_color5.u"
		;
connectAttr "rigw1:mesh:greydark1_samplerInfo12.fr" "rigw1:mesh:greydark1_toonred_color5.v"
		;
connectAttr "rigw1:mesh:greydark1_place2dTexture31.ofs" "rigw1:mesh:greydark1_toonred_color5.fs"
		;
connectAttr "rigw1:mesh:greydark1_matgrey_darkSG.pa" ":renderPartition.st" -na;
connectAttr "rigw1:mesh:greydark1_matgrey_dark.msg" ":defaultShaderList1.s" -na;
connectAttr "rigw1:mesh:greydark1_Toon_grey1.msg" ":defaultShaderList1.s" -na;
connectAttr "rigw1:mesh:greydark1_toonredoutline5.msg" ":defaultTextureList1.tx"
		 -na;
connectAttr "rigw1:mesh:greydark1_toonred_color5.msg" ":defaultTextureList1.tx" 
		-na;
connectAttr "rigw1:mesh:greydark1_mib_amb_occlusion14.msg" ":defaultTextureList1.tx"
		 -na;
connectAttr "rigw1:mesh:greydark1_place2dTexture30.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "rigw1:mesh:greydark1_samplerInfo11.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "rigw1:mesh:greydark1_place2dTexture31.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "rigw1:mesh:greydark1_samplerInfo12.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "mia_exposure_simple1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Medic__mc-rigw@Med_run.ma
