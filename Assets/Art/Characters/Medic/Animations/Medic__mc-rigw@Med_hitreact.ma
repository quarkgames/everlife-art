//Maya ASCII 2013 scene
//Name: Medic__mc-rigw@Med_hitreact.ma
//Last modified: Wed, Jun 04, 2014 09:39:28 AM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Medic/Medic__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Medic__rig.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Medic/Medic__mc-rigw.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 15.339254413691345 14.872946300177761 27.322765865081521 ;
	setAttr ".r" -type "double3" -19.53835272960697 -331.7999999999916 1.804460582794367e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 39.525278875954243;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 16 ".lnk";
	setAttr -s 16 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 658 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".phl[440]" 0;
	setAttr ".phl[441]" 0;
	setAttr ".phl[442]" 0;
	setAttr ".phl[443]" 0;
	setAttr ".phl[444]" 0;
	setAttr ".phl[445]" 0;
	setAttr ".phl[446]" 0;
	setAttr ".phl[447]" 0;
	setAttr ".phl[448]" 0;
	setAttr ".phl[449]" 0;
	setAttr ".phl[450]" 0;
	setAttr ".phl[451]" 0;
	setAttr ".phl[452]" 0;
	setAttr ".phl[453]" 0;
	setAttr ".phl[454]" 0;
	setAttr ".phl[455]" 0;
	setAttr ".phl[456]" 0;
	setAttr ".phl[457]" 0;
	setAttr ".phl[458]" 0;
	setAttr ".phl[459]" 0;
	setAttr ".phl[460]" 0;
	setAttr ".phl[461]" 0;
	setAttr ".phl[462]" 0;
	setAttr ".phl[463]" 0;
	setAttr ".phl[464]" 0;
	setAttr ".phl[465]" 0;
	setAttr ".phl[466]" 0;
	setAttr ".phl[467]" 0;
	setAttr ".phl[468]" 0;
	setAttr ".phl[469]" 0;
	setAttr ".phl[470]" 0;
	setAttr ".phl[471]" 0;
	setAttr ".phl[472]" 0;
	setAttr ".phl[473]" 0;
	setAttr ".phl[474]" 0;
	setAttr ".phl[475]" 0;
	setAttr ".phl[476]" 0;
	setAttr ".phl[477]" 0;
	setAttr ".phl[478]" 0;
	setAttr ".phl[479]" 0;
	setAttr ".phl[480]" 0;
	setAttr ".phl[481]" 0;
	setAttr ".phl[482]" 0;
	setAttr ".phl[483]" 0;
	setAttr ".phl[484]" 0;
	setAttr ".phl[485]" 0;
	setAttr ".phl[486]" 0;
	setAttr ".phl[487]" 0;
	setAttr ".phl[488]" 0;
	setAttr ".phl[489]" 0;
	setAttr ".phl[490]" 0;
	setAttr ".phl[491]" 0;
	setAttr ".phl[492]" 0;
	setAttr ".phl[493]" 0;
	setAttr ".phl[494]" 0;
	setAttr ".phl[495]" 0;
	setAttr ".phl[496]" 0;
	setAttr ".phl[497]" 0;
	setAttr ".phl[498]" 0;
	setAttr ".phl[499]" 0;
	setAttr ".phl[500]" 0;
	setAttr ".phl[501]" 0;
	setAttr ".phl[502]" 0;
	setAttr ".phl[503]" 0;
	setAttr ".phl[504]" 0;
	setAttr ".phl[505]" 0;
	setAttr ".phl[506]" 0;
	setAttr ".phl[507]" 0;
	setAttr ".phl[508]" 0;
	setAttr ".phl[509]" 0;
	setAttr ".phl[510]" 0;
	setAttr ".phl[511]" 0;
	setAttr ".phl[512]" 0;
	setAttr ".phl[513]" 0;
	setAttr ".phl[514]" 0;
	setAttr ".phl[515]" 0;
	setAttr ".phl[516]" 0;
	setAttr ".phl[517]" 0;
	setAttr ".phl[518]" 0;
	setAttr ".phl[519]" 0;
	setAttr ".phl[520]" 0;
	setAttr ".phl[521]" 0;
	setAttr ".phl[522]" 0;
	setAttr ".phl[523]" 0;
	setAttr ".phl[524]" 0;
	setAttr ".phl[525]" 0;
	setAttr ".phl[526]" 0;
	setAttr ".phl[527]" 0;
	setAttr ".phl[528]" 0;
	setAttr ".phl[529]" 0;
	setAttr ".phl[530]" 0;
	setAttr ".phl[531]" 0;
	setAttr ".phl[532]" 0;
	setAttr ".phl[533]" 0;
	setAttr ".phl[534]" 0;
	setAttr ".phl[535]" 0;
	setAttr ".phl[536]" 0;
	setAttr ".phl[537]" 0;
	setAttr ".phl[538]" 0;
	setAttr ".phl[539]" 0;
	setAttr ".phl[540]" 0;
	setAttr ".phl[541]" 0;
	setAttr ".phl[542]" 0;
	setAttr ".phl[543]" 0;
	setAttr ".phl[544]" 0;
	setAttr ".phl[545]" 0;
	setAttr ".phl[546]" 0;
	setAttr ".phl[547]" 0;
	setAttr ".phl[548]" 0;
	setAttr ".phl[549]" 0;
	setAttr ".phl[550]" 0;
	setAttr ".phl[551]" 0;
	setAttr ".phl[552]" 0;
	setAttr ".phl[553]" 0;
	setAttr ".phl[554]" 0;
	setAttr ".phl[555]" 0;
	setAttr ".phl[556]" 0;
	setAttr ".phl[557]" 0;
	setAttr ".phl[558]" 0;
	setAttr ".phl[559]" 0;
	setAttr ".phl[560]" 0;
	setAttr ".phl[561]" 0;
	setAttr ".phl[562]" 0;
	setAttr ".phl[563]" 0;
	setAttr ".phl[564]" 0;
	setAttr ".phl[565]" 0;
	setAttr ".phl[566]" 0;
	setAttr ".phl[567]" 0;
	setAttr ".phl[568]" 0;
	setAttr ".phl[569]" 0;
	setAttr ".phl[570]" 0;
	setAttr ".phl[571]" 0;
	setAttr ".phl[572]" 0;
	setAttr ".phl[573]" 0;
	setAttr ".phl[574]" 0;
	setAttr ".phl[575]" 0;
	setAttr ".phl[576]" 0;
	setAttr ".phl[577]" 0;
	setAttr ".phl[578]" 0;
	setAttr ".phl[579]" 0;
	setAttr ".phl[580]" 0;
	setAttr ".phl[581]" 0;
	setAttr ".phl[582]" 0;
	setAttr ".phl[583]" 0;
	setAttr ".phl[584]" 0;
	setAttr ".phl[585]" 0;
	setAttr ".phl[586]" 0;
	setAttr ".phl[587]" 0;
	setAttr ".phl[588]" 0;
	setAttr ".phl[589]" 0;
	setAttr ".phl[590]" 0;
	setAttr ".phl[591]" 0;
	setAttr ".phl[592]" 0;
	setAttr ".phl[593]" 0;
	setAttr ".phl[594]" 0;
	setAttr ".phl[595]" 0;
	setAttr ".phl[596]" 0;
	setAttr ".phl[597]" 0;
	setAttr ".phl[598]" 0;
	setAttr ".phl[599]" 0;
	setAttr ".phl[600]" 0;
	setAttr ".phl[601]" 0;
	setAttr ".phl[602]" 0;
	setAttr ".phl[603]" 0;
	setAttr ".phl[604]" 0;
	setAttr ".phl[605]" 0;
	setAttr ".phl[606]" 0;
	setAttr ".phl[607]" 0;
	setAttr ".phl[608]" 0;
	setAttr ".phl[609]" 0;
	setAttr ".phl[610]" 0;
	setAttr ".phl[611]" 0;
	setAttr ".phl[612]" 0;
	setAttr ".phl[613]" 0;
	setAttr ".phl[614]" 0;
	setAttr ".phl[615]" 0;
	setAttr ".phl[616]" 0;
	setAttr ".phl[617]" 0;
	setAttr ".phl[618]" 0;
	setAttr ".phl[619]" 0;
	setAttr ".phl[620]" 0;
	setAttr ".phl[621]" 0;
	setAttr ".phl[622]" 0;
	setAttr ".phl[623]" 0;
	setAttr ".phl[624]" 0;
	setAttr ".phl[625]" 0;
	setAttr ".phl[626]" 0;
	setAttr ".phl[627]" 0;
	setAttr ".phl[628]" 0;
	setAttr ".phl[629]" 0;
	setAttr ".phl[630]" 0;
	setAttr ".phl[631]" 0;
	setAttr ".phl[632]" 0;
	setAttr ".phl[633]" 0;
	setAttr ".phl[634]" 0;
	setAttr ".phl[635]" 0;
	setAttr ".phl[636]" 0;
	setAttr ".phl[637]" 0;
	setAttr ".phl[638]" 0;
	setAttr ".phl[639]" 0;
	setAttr ".phl[640]" 0;
	setAttr ".phl[641]" 0;
	setAttr ".phl[642]" 0;
	setAttr ".phl[643]" 0;
	setAttr ".phl[644]" 0;
	setAttr ".phl[645]" 0;
	setAttr ".phl[646]" 0;
	setAttr ".phl[647]" 0;
	setAttr ".phl[648]" 0;
	setAttr ".phl[649]" 0;
	setAttr ".phl[650]" 0;
	setAttr ".phl[651]" 0;
	setAttr ".phl[652]" 0;
	setAttr ".phl[653]" 0;
	setAttr ".phl[654]" 0;
	setAttr ".phl[655]" 0;
	setAttr ".phl[656]" 0;
	setAttr ".phl[657]" 0;
	setAttr ".phl[658]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 0
		"rigw:rigRN" 0
		"rigwRN" 706
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translate" " -type \"double3\" 0.740445 0.974319 4.426617"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotate" " -type \"double3\" 105.438743 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translate" " -type \"double3\" 0.740445 1.662469 4.221696"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translate" " -type \"double3\" 0.740445 2.156221 3.374191"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translate" " -type \"double3\" 0.740445 2.603864 2.502186"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translate" " -type \"double3\" 0.741432 2.953546 1.639622"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotate" " -type \"double3\" 18.206674 0.054517 -0.506614"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translate" " -type \"double3\" 0.740445 3.050656 0.795099"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translate" " -type \"double3\" 0.740445 3.050656 -0.177564"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translate" " -type \"double3\" 0.740445 3.050656 -1.150227"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translate" " -type \"double3\" 0.740445 3.050656 -2.12289"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translate" " -type \"double3\" 0.740445 2.818561 -2.910817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translate" " -type \"double3\" 0.740445 2.374804 -3.765316"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translate" " -type \"double3\" 0.740445 1.924439 -4.632539"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotate" " -type \"double3\" -27.443689 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translate" " -type \"double3\" 0.740445 1.474073 -5.499763"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotate" " -type \"double3\" -27.443689 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translate" " -type \"double3\" 0.740445 0.664251 -5.529688"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotate" " -type \"double3\" -101.394566 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translate" " -type \"double3\" 0.740445 0.360482 -4.892085"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translate" " -type \"double3\" 0.740445 0.360482 -3.914649"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translate" " -type \"double3\" 0.740445 0.360482 -2.937213"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translate" " -type \"double3\" 0.740445 0.360482 -1.959776"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translate" " -type \"double3\" 0.740445 0.360482 -0.98234"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translate" " -type \"double3\" 0.740445 0.360482 -0.00490393"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translate" " -type \"double3\" 0.740445 0.360482 0.972532"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translate" " -type \"double3\" 0.740445 0.360482 1.949969"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translate" " -type \"double3\" 0.740445 0.360482 2.927405"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translate" " -type \"double3\" 0.740445 0.360482 3.904841"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translate" " -type \"double3\" -0.740596 2.156221 3.374191"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translate" " -type \"double3\" -0.740596 1.662469 4.221696"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translate" " -type \"double3\" -0.740596 0.974319 4.426617"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotate" " -type \"double3\" 74.561257 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translate" " -type \"double3\" -0.740596 0.360482 3.904841"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translate" " -type \"double3\" -0.740596 0.360482 2.927405"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translate" " -type \"double3\" -0.740596 0.360482 1.949969"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translate" " -type \"double3\" -0.740596 0.360482 0.972532"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translate" " -type \"double3\" -0.740596 0.360482 -0.00490393"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translate" " -type \"double3\" -0.740596 0.360482 -0.98234"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translate" " -type \"double3\" -0.740596 0.360482 -1.959776"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translate" " -type \"double3\" -0.740596 0.360482 -2.937213"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translate" " -type \"double3\" -0.740596 0.360482 -3.914649"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translate" " -type \"double3\" -0.740596 0.360482 -4.892085"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translate" " -type \"double3\" -0.740596 0.664251 -5.529688"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotate" " -type \"double3\" -78.605434 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translate" " -type \"double3\" -0.740596 1.474073 -5.499763"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotate" " -type \"double3\" 27.443689 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translate" " -type \"double3\" -0.740596 1.924439 -4.632539"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotate" " -type \"double3\" 27.443689 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translate" " -type \"double3\" -0.740596 2.374804 -3.765316"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translate" " -type \"double3\" -0.740596 2.818561 -2.910817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translate" " -type \"double3\" -0.740596 3.050656 -2.12289"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translate" " -type \"double3\" -0.740596 3.050656 -1.150227"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translate" " -type \"double3\" -0.740596 3.050656 -0.177564"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translate" " -type \"double3\" -0.740596 3.050656 0.795099"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translate" " -type \"double3\" -0.741585 2.953546 1.639622"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotate" " -type \"double3\" 161.793326 0.0545059 -179.493489"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translate" " -type \"double3\" -0.740596 2.603864 2.502186"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateZ" " -av"
		
		2 "rigw:_mesh" "displayType" " 2"
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateX" 
		"rigwRN.placeHolderList[179]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateY" 
		"rigwRN.placeHolderList[180]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateZ" 
		"rigwRN.placeHolderList[181]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateX" 
		"rigwRN.placeHolderList[182]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateY" 
		"rigwRN.placeHolderList[183]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateZ" 
		"rigwRN.placeHolderList[184]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.visibility" 
		"rigwRN.placeHolderList[185]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleX" 
		"rigwRN.placeHolderList[186]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleY" 
		"rigwRN.placeHolderList[187]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleZ" 
		"rigwRN.placeHolderList[188]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateX" 
		"rigwRN.placeHolderList[189]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateY" 
		"rigwRN.placeHolderList[190]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateZ" 
		"rigwRN.placeHolderList[191]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.visibility" 
		"rigwRN.placeHolderList[192]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateX" 
		"rigwRN.placeHolderList[193]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateY" 
		"rigwRN.placeHolderList[194]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateZ" 
		"rigwRN.placeHolderList[195]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleX" 
		"rigwRN.placeHolderList[196]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleY" 
		"rigwRN.placeHolderList[197]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleZ" 
		"rigwRN.placeHolderList[198]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateX" 
		"rigwRN.placeHolderList[199]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateY" 
		"rigwRN.placeHolderList[200]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateZ" 
		"rigwRN.placeHolderList[201]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.visibility" 
		"rigwRN.placeHolderList[202]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateX" 
		"rigwRN.placeHolderList[203]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateY" 
		"rigwRN.placeHolderList[204]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateZ" 
		"rigwRN.placeHolderList[205]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleX" 
		"rigwRN.placeHolderList[206]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleY" 
		"rigwRN.placeHolderList[207]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleZ" 
		"rigwRN.placeHolderList[208]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateX" 
		"rigwRN.placeHolderList[209]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateY" 
		"rigwRN.placeHolderList[210]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateZ" 
		"rigwRN.placeHolderList[211]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.visibility" 
		"rigwRN.placeHolderList[212]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateX" 
		"rigwRN.placeHolderList[213]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateY" 
		"rigwRN.placeHolderList[214]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateZ" 
		"rigwRN.placeHolderList[215]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleX" 
		"rigwRN.placeHolderList[216]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleY" 
		"rigwRN.placeHolderList[217]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleZ" 
		"rigwRN.placeHolderList[218]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateX" 
		"rigwRN.placeHolderList[219]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateY" 
		"rigwRN.placeHolderList[220]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateZ" 
		"rigwRN.placeHolderList[221]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateX" 
		"rigwRN.placeHolderList[222]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateY" 
		"rigwRN.placeHolderList[223]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateZ" 
		"rigwRN.placeHolderList[224]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.visibility" 
		"rigwRN.placeHolderList[225]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleX" 
		"rigwRN.placeHolderList[226]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleY" 
		"rigwRN.placeHolderList[227]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleZ" 
		"rigwRN.placeHolderList[228]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateX" 
		"rigwRN.placeHolderList[229]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateY" 
		"rigwRN.placeHolderList[230]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateZ" 
		"rigwRN.placeHolderList[231]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.visibility" 
		"rigwRN.placeHolderList[232]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateX" 
		"rigwRN.placeHolderList[233]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateY" 
		"rigwRN.placeHolderList[234]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateZ" 
		"rigwRN.placeHolderList[235]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleX" 
		"rigwRN.placeHolderList[236]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleY" 
		"rigwRN.placeHolderList[237]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleZ" 
		"rigwRN.placeHolderList[238]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateX" 
		"rigwRN.placeHolderList[239]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateY" 
		"rigwRN.placeHolderList[240]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateZ" 
		"rigwRN.placeHolderList[241]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.visibility" 
		"rigwRN.placeHolderList[242]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateX" 
		"rigwRN.placeHolderList[243]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateY" 
		"rigwRN.placeHolderList[244]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateZ" 
		"rigwRN.placeHolderList[245]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleX" 
		"rigwRN.placeHolderList[246]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleY" 
		"rigwRN.placeHolderList[247]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleZ" 
		"rigwRN.placeHolderList[248]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateX" 
		"rigwRN.placeHolderList[249]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateY" 
		"rigwRN.placeHolderList[250]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateZ" 
		"rigwRN.placeHolderList[251]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.visibility" 
		"rigwRN.placeHolderList[252]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateX" 
		"rigwRN.placeHolderList[253]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateY" 
		"rigwRN.placeHolderList[254]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateZ" 
		"rigwRN.placeHolderList[255]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleX" 
		"rigwRN.placeHolderList[256]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleY" 
		"rigwRN.placeHolderList[257]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleZ" 
		"rigwRN.placeHolderList[258]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateX" 
		"rigwRN.placeHolderList[259]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateY" 
		"rigwRN.placeHolderList[260]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateZ" 
		"rigwRN.placeHolderList[261]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.visibility" 
		"rigwRN.placeHolderList[262]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateX" 
		"rigwRN.placeHolderList[263]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateY" 
		"rigwRN.placeHolderList[264]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateZ" 
		"rigwRN.placeHolderList[265]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleX" 
		"rigwRN.placeHolderList[266]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleY" 
		"rigwRN.placeHolderList[267]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleZ" 
		"rigwRN.placeHolderList[268]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateX" 
		"rigwRN.placeHolderList[269]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateY" 
		"rigwRN.placeHolderList[270]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateZ" 
		"rigwRN.placeHolderList[271]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.visibility" 
		"rigwRN.placeHolderList[272]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateX" 
		"rigwRN.placeHolderList[273]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateY" 
		"rigwRN.placeHolderList[274]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateZ" 
		"rigwRN.placeHolderList[275]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleX" 
		"rigwRN.placeHolderList[276]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleY" 
		"rigwRN.placeHolderList[277]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleZ" 
		"rigwRN.placeHolderList[278]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateX" 
		"rigwRN.placeHolderList[279]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateY" 
		"rigwRN.placeHolderList[280]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateZ" 
		"rigwRN.placeHolderList[281]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.visibility" 
		"rigwRN.placeHolderList[282]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateX" 
		"rigwRN.placeHolderList[283]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateY" 
		"rigwRN.placeHolderList[284]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateZ" 
		"rigwRN.placeHolderList[285]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleX" 
		"rigwRN.placeHolderList[286]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleY" 
		"rigwRN.placeHolderList[287]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleZ" 
		"rigwRN.placeHolderList[288]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateX" 
		"rigwRN.placeHolderList[289]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateY" 
		"rigwRN.placeHolderList[290]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateZ" 
		"rigwRN.placeHolderList[291]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateX" 
		"rigwRN.placeHolderList[292]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateY" 
		"rigwRN.placeHolderList[293]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateZ" 
		"rigwRN.placeHolderList[294]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.visibility" 
		"rigwRN.placeHolderList[295]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleX" 
		"rigwRN.placeHolderList[296]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleY" 
		"rigwRN.placeHolderList[297]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleZ" 
		"rigwRN.placeHolderList[298]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateX" 
		"rigwRN.placeHolderList[299]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateY" 
		"rigwRN.placeHolderList[300]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateZ" 
		"rigwRN.placeHolderList[301]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateX" 
		"rigwRN.placeHolderList[302]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateY" 
		"rigwRN.placeHolderList[303]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateZ" 
		"rigwRN.placeHolderList[304]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.visibility" 
		"rigwRN.placeHolderList[305]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleX" 
		"rigwRN.placeHolderList[306]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleY" 
		"rigwRN.placeHolderList[307]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleZ" 
		"rigwRN.placeHolderList[308]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateX" 
		"rigwRN.placeHolderList[309]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateY" 
		"rigwRN.placeHolderList[310]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateZ" 
		"rigwRN.placeHolderList[311]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateX" 
		"rigwRN.placeHolderList[312]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateY" 
		"rigwRN.placeHolderList[313]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateZ" 
		"rigwRN.placeHolderList[314]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.visibility" 
		"rigwRN.placeHolderList[315]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleX" 
		"rigwRN.placeHolderList[316]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleY" 
		"rigwRN.placeHolderList[317]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleZ" 
		"rigwRN.placeHolderList[318]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateX" 
		"rigwRN.placeHolderList[319]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateY" 
		"rigwRN.placeHolderList[320]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateZ" 
		"rigwRN.placeHolderList[321]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.visibility" 
		"rigwRN.placeHolderList[322]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateX" 
		"rigwRN.placeHolderList[323]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateY" 
		"rigwRN.placeHolderList[324]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateZ" 
		"rigwRN.placeHolderList[325]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleX" 
		"rigwRN.placeHolderList[326]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleY" 
		"rigwRN.placeHolderList[327]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleZ" 
		"rigwRN.placeHolderList[328]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateX" 
		"rigwRN.placeHolderList[329]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateY" 
		"rigwRN.placeHolderList[330]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateZ" 
		"rigwRN.placeHolderList[331]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.visibility" 
		"rigwRN.placeHolderList[332]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateX" 
		"rigwRN.placeHolderList[333]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateY" 
		"rigwRN.placeHolderList[334]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateZ" 
		"rigwRN.placeHolderList[335]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleX" 
		"rigwRN.placeHolderList[336]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleY" 
		"rigwRN.placeHolderList[337]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleZ" 
		"rigwRN.placeHolderList[338]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateX" 
		"rigwRN.placeHolderList[339]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateY" 
		"rigwRN.placeHolderList[340]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateZ" 
		"rigwRN.placeHolderList[341]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.visibility" 
		"rigwRN.placeHolderList[342]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateX" 
		"rigwRN.placeHolderList[343]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateY" 
		"rigwRN.placeHolderList[344]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateZ" 
		"rigwRN.placeHolderList[345]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleX" 
		"rigwRN.placeHolderList[346]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleY" 
		"rigwRN.placeHolderList[347]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleZ" 
		"rigwRN.placeHolderList[348]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateX" 
		"rigwRN.placeHolderList[349]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateY" 
		"rigwRN.placeHolderList[350]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateZ" 
		"rigwRN.placeHolderList[351]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.visibility" 
		"rigwRN.placeHolderList[352]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateX" 
		"rigwRN.placeHolderList[353]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateY" 
		"rigwRN.placeHolderList[354]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateZ" 
		"rigwRN.placeHolderList[355]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleX" 
		"rigwRN.placeHolderList[356]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleY" 
		"rigwRN.placeHolderList[357]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleZ" 
		"rigwRN.placeHolderList[358]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateX" 
		"rigwRN.placeHolderList[359]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateY" 
		"rigwRN.placeHolderList[360]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateZ" 
		"rigwRN.placeHolderList[361]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.visibility" 
		"rigwRN.placeHolderList[362]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateX" 
		"rigwRN.placeHolderList[363]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateY" 
		"rigwRN.placeHolderList[364]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateZ" 
		"rigwRN.placeHolderList[365]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleX" 
		"rigwRN.placeHolderList[366]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleY" 
		"rigwRN.placeHolderList[367]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleZ" 
		"rigwRN.placeHolderList[368]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateX" 
		"rigwRN.placeHolderList[369]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateY" 
		"rigwRN.placeHolderList[370]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateZ" 
		"rigwRN.placeHolderList[371]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.visibility" 
		"rigwRN.placeHolderList[372]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateX" 
		"rigwRN.placeHolderList[373]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateY" 
		"rigwRN.placeHolderList[374]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateZ" 
		"rigwRN.placeHolderList[375]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleX" 
		"rigwRN.placeHolderList[376]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleY" 
		"rigwRN.placeHolderList[377]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleZ" 
		"rigwRN.placeHolderList[378]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateX" 
		"rigwRN.placeHolderList[379]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateY" 
		"rigwRN.placeHolderList[380]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateZ" 
		"rigwRN.placeHolderList[381]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.visibility" 
		"rigwRN.placeHolderList[382]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateX" 
		"rigwRN.placeHolderList[383]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateY" 
		"rigwRN.placeHolderList[384]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateZ" 
		"rigwRN.placeHolderList[385]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleX" 
		"rigwRN.placeHolderList[386]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleY" 
		"rigwRN.placeHolderList[387]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleZ" 
		"rigwRN.placeHolderList[388]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateX" 
		"rigwRN.placeHolderList[389]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateY" 
		"rigwRN.placeHolderList[390]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateZ" 
		"rigwRN.placeHolderList[391]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.visibility" 
		"rigwRN.placeHolderList[392]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateX" 
		"rigwRN.placeHolderList[393]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateY" 
		"rigwRN.placeHolderList[394]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateZ" 
		"rigwRN.placeHolderList[395]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleX" 
		"rigwRN.placeHolderList[396]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleY" 
		"rigwRN.placeHolderList[397]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleZ" 
		"rigwRN.placeHolderList[398]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateX" 
		"rigwRN.placeHolderList[399]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateY" 
		"rigwRN.placeHolderList[400]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateZ" 
		"rigwRN.placeHolderList[401]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.visibility" 
		"rigwRN.placeHolderList[402]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateX" 
		"rigwRN.placeHolderList[403]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateY" 
		"rigwRN.placeHolderList[404]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateZ" 
		"rigwRN.placeHolderList[405]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleX" 
		"rigwRN.placeHolderList[406]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleY" 
		"rigwRN.placeHolderList[407]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleZ" 
		"rigwRN.placeHolderList[408]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateX" 
		"rigwRN.placeHolderList[409]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateY" 
		"rigwRN.placeHolderList[410]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateZ" 
		"rigwRN.placeHolderList[411]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateX" 
		"rigwRN.placeHolderList[412]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateY" 
		"rigwRN.placeHolderList[413]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateZ" 
		"rigwRN.placeHolderList[414]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.visibility" 
		"rigwRN.placeHolderList[415]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleX" 
		"rigwRN.placeHolderList[416]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleY" 
		"rigwRN.placeHolderList[417]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleZ" 
		"rigwRN.placeHolderList[418]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateX" 
		"rigwRN.placeHolderList[419]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateY" 
		"rigwRN.placeHolderList[420]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateZ" 
		"rigwRN.placeHolderList[421]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.visibility" 
		"rigwRN.placeHolderList[422]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateX" 
		"rigwRN.placeHolderList[423]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateY" 
		"rigwRN.placeHolderList[424]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateZ" 
		"rigwRN.placeHolderList[425]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleX" 
		"rigwRN.placeHolderList[426]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleY" 
		"rigwRN.placeHolderList[427]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleZ" 
		"rigwRN.placeHolderList[428]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateX" 
		"rigwRN.placeHolderList[429]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateY" 
		"rigwRN.placeHolderList[430]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateZ" 
		"rigwRN.placeHolderList[431]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.visibility" 
		"rigwRN.placeHolderList[432]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateX" 
		"rigwRN.placeHolderList[433]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateY" 
		"rigwRN.placeHolderList[434]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateZ" 
		"rigwRN.placeHolderList[435]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleX" 
		"rigwRN.placeHolderList[436]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleY" 
		"rigwRN.placeHolderList[437]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleZ" 
		"rigwRN.placeHolderList[438]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateX" 
		"rigwRN.placeHolderList[439]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateY" 
		"rigwRN.placeHolderList[440]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateZ" 
		"rigwRN.placeHolderList[441]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateX" 
		"rigwRN.placeHolderList[442]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateY" 
		"rigwRN.placeHolderList[443]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateZ" 
		"rigwRN.placeHolderList[444]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.visibility" 
		"rigwRN.placeHolderList[445]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleX" 
		"rigwRN.placeHolderList[446]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleY" 
		"rigwRN.placeHolderList[447]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleZ" 
		"rigwRN.placeHolderList[448]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateX" 
		"rigwRN.placeHolderList[449]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateY" 
		"rigwRN.placeHolderList[450]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateZ" 
		"rigwRN.placeHolderList[451]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateX" 
		"rigwRN.placeHolderList[452]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateY" 
		"rigwRN.placeHolderList[453]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateZ" 
		"rigwRN.placeHolderList[454]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.visibility" 
		"rigwRN.placeHolderList[455]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleX" 
		"rigwRN.placeHolderList[456]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleY" 
		"rigwRN.placeHolderList[457]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleZ" 
		"rigwRN.placeHolderList[458]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateX" 
		"rigwRN.placeHolderList[459]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateY" 
		"rigwRN.placeHolderList[460]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateZ" 
		"rigwRN.placeHolderList[461]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.visibility" 
		"rigwRN.placeHolderList[462]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateX" 
		"rigwRN.placeHolderList[463]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateY" 
		"rigwRN.placeHolderList[464]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateZ" 
		"rigwRN.placeHolderList[465]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleX" 
		"rigwRN.placeHolderList[466]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleY" 
		"rigwRN.placeHolderList[467]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleZ" 
		"rigwRN.placeHolderList[468]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateX" 
		"rigwRN.placeHolderList[469]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateY" 
		"rigwRN.placeHolderList[470]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateZ" 
		"rigwRN.placeHolderList[471]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.visibility" 
		"rigwRN.placeHolderList[472]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateX" 
		"rigwRN.placeHolderList[473]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateY" 
		"rigwRN.placeHolderList[474]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateZ" 
		"rigwRN.placeHolderList[475]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleX" 
		"rigwRN.placeHolderList[476]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleY" 
		"rigwRN.placeHolderList[477]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleZ" 
		"rigwRN.placeHolderList[478]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateX" 
		"rigwRN.placeHolderList[479]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateY" 
		"rigwRN.placeHolderList[480]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateZ" 
		"rigwRN.placeHolderList[481]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.visibility" 
		"rigwRN.placeHolderList[482]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateX" 
		"rigwRN.placeHolderList[483]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateY" 
		"rigwRN.placeHolderList[484]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateZ" 
		"rigwRN.placeHolderList[485]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleX" 
		"rigwRN.placeHolderList[486]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleY" 
		"rigwRN.placeHolderList[487]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleZ" 
		"rigwRN.placeHolderList[488]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateX" 
		"rigwRN.placeHolderList[489]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateY" 
		"rigwRN.placeHolderList[490]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateZ" 
		"rigwRN.placeHolderList[491]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.visibility" 
		"rigwRN.placeHolderList[492]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateX" 
		"rigwRN.placeHolderList[493]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateY" 
		"rigwRN.placeHolderList[494]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateZ" 
		"rigwRN.placeHolderList[495]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleX" 
		"rigwRN.placeHolderList[496]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleY" 
		"rigwRN.placeHolderList[497]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleZ" 
		"rigwRN.placeHolderList[498]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateX" 
		"rigwRN.placeHolderList[499]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateY" 
		"rigwRN.placeHolderList[500]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateZ" 
		"rigwRN.placeHolderList[501]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.visibility" 
		"rigwRN.placeHolderList[502]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateX" 
		"rigwRN.placeHolderList[503]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateY" 
		"rigwRN.placeHolderList[504]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateZ" 
		"rigwRN.placeHolderList[505]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleX" 
		"rigwRN.placeHolderList[506]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleY" 
		"rigwRN.placeHolderList[507]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleZ" 
		"rigwRN.placeHolderList[508]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateX" 
		"rigwRN.placeHolderList[509]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateY" 
		"rigwRN.placeHolderList[510]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateZ" 
		"rigwRN.placeHolderList[511]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.visibility" 
		"rigwRN.placeHolderList[512]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateX" 
		"rigwRN.placeHolderList[513]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateY" 
		"rigwRN.placeHolderList[514]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateZ" 
		"rigwRN.placeHolderList[515]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleX" 
		"rigwRN.placeHolderList[516]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleY" 
		"rigwRN.placeHolderList[517]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleZ" 
		"rigwRN.placeHolderList[518]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateX" 
		"rigwRN.placeHolderList[519]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateY" 
		"rigwRN.placeHolderList[520]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateZ" 
		"rigwRN.placeHolderList[521]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.visibility" 
		"rigwRN.placeHolderList[522]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateX" 
		"rigwRN.placeHolderList[523]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateY" 
		"rigwRN.placeHolderList[524]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateZ" 
		"rigwRN.placeHolderList[525]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleX" 
		"rigwRN.placeHolderList[526]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleY" 
		"rigwRN.placeHolderList[527]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleZ" 
		"rigwRN.placeHolderList[528]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateX" 
		"rigwRN.placeHolderList[529]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateY" 
		"rigwRN.placeHolderList[530]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateZ" 
		"rigwRN.placeHolderList[531]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.visibility" 
		"rigwRN.placeHolderList[532]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateX" 
		"rigwRN.placeHolderList[533]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateY" 
		"rigwRN.placeHolderList[534]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateZ" 
		"rigwRN.placeHolderList[535]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleX" 
		"rigwRN.placeHolderList[536]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleY" 
		"rigwRN.placeHolderList[537]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleZ" 
		"rigwRN.placeHolderList[538]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateX" 
		"rigwRN.placeHolderList[539]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateY" 
		"rigwRN.placeHolderList[540]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateZ" 
		"rigwRN.placeHolderList[541]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.visibility" 
		"rigwRN.placeHolderList[542]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateX" 
		"rigwRN.placeHolderList[543]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateY" 
		"rigwRN.placeHolderList[544]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateZ" 
		"rigwRN.placeHolderList[545]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleX" 
		"rigwRN.placeHolderList[546]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleY" 
		"rigwRN.placeHolderList[547]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleZ" 
		"rigwRN.placeHolderList[548]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateX" 
		"rigwRN.placeHolderList[549]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateY" 
		"rigwRN.placeHolderList[550]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateZ" 
		"rigwRN.placeHolderList[551]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateX" 
		"rigwRN.placeHolderList[552]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateY" 
		"rigwRN.placeHolderList[553]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateZ" 
		"rigwRN.placeHolderList[554]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.visibility" 
		"rigwRN.placeHolderList[555]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleX" 
		"rigwRN.placeHolderList[556]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleY" 
		"rigwRN.placeHolderList[557]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleZ" 
		"rigwRN.placeHolderList[558]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateX" 
		"rigwRN.placeHolderList[559]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateY" 
		"rigwRN.placeHolderList[560]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateZ" 
		"rigwRN.placeHolderList[561]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateX" 
		"rigwRN.placeHolderList[562]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateY" 
		"rigwRN.placeHolderList[563]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateZ" 
		"rigwRN.placeHolderList[564]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.visibility" 
		"rigwRN.placeHolderList[565]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleX" 
		"rigwRN.placeHolderList[566]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleY" 
		"rigwRN.placeHolderList[567]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleZ" 
		"rigwRN.placeHolderList[568]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateX" 
		"rigwRN.placeHolderList[569]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateY" 
		"rigwRN.placeHolderList[570]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateZ" 
		"rigwRN.placeHolderList[571]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateX" 
		"rigwRN.placeHolderList[572]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateY" 
		"rigwRN.placeHolderList[573]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateZ" 
		"rigwRN.placeHolderList[574]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.visibility" 
		"rigwRN.placeHolderList[575]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleX" 
		"rigwRN.placeHolderList[576]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleY" 
		"rigwRN.placeHolderList[577]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleZ" 
		"rigwRN.placeHolderList[578]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateX" 
		"rigwRN.placeHolderList[579]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateY" 
		"rigwRN.placeHolderList[580]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateZ" 
		"rigwRN.placeHolderList[581]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.visibility" 
		"rigwRN.placeHolderList[582]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateX" 
		"rigwRN.placeHolderList[583]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateY" 
		"rigwRN.placeHolderList[584]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateZ" 
		"rigwRN.placeHolderList[585]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleX" 
		"rigwRN.placeHolderList[586]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleY" 
		"rigwRN.placeHolderList[587]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleZ" 
		"rigwRN.placeHolderList[588]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateX" 
		"rigwRN.placeHolderList[589]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateY" 
		"rigwRN.placeHolderList[590]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateZ" 
		"rigwRN.placeHolderList[591]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.visibility" 
		"rigwRN.placeHolderList[592]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateX" 
		"rigwRN.placeHolderList[593]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateY" 
		"rigwRN.placeHolderList[594]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateZ" 
		"rigwRN.placeHolderList[595]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleX" 
		"rigwRN.placeHolderList[596]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleY" 
		"rigwRN.placeHolderList[597]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleZ" 
		"rigwRN.placeHolderList[598]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateX" 
		"rigwRN.placeHolderList[599]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateY" 
		"rigwRN.placeHolderList[600]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateZ" 
		"rigwRN.placeHolderList[601]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.visibility" 
		"rigwRN.placeHolderList[602]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateX" 
		"rigwRN.placeHolderList[603]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateY" 
		"rigwRN.placeHolderList[604]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateZ" 
		"rigwRN.placeHolderList[605]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleX" 
		"rigwRN.placeHolderList[606]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleY" 
		"rigwRN.placeHolderList[607]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleZ" 
		"rigwRN.placeHolderList[608]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateX" 
		"rigwRN.placeHolderList[609]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateY" 
		"rigwRN.placeHolderList[610]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateZ" 
		"rigwRN.placeHolderList[611]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.visibility" 
		"rigwRN.placeHolderList[612]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateX" 
		"rigwRN.placeHolderList[613]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateY" 
		"rigwRN.placeHolderList[614]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateZ" 
		"rigwRN.placeHolderList[615]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleX" 
		"rigwRN.placeHolderList[616]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleY" 
		"rigwRN.placeHolderList[617]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleZ" 
		"rigwRN.placeHolderList[618]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateX" 
		"rigwRN.placeHolderList[619]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateY" 
		"rigwRN.placeHolderList[620]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateZ" 
		"rigwRN.placeHolderList[621]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.visibility" 
		"rigwRN.placeHolderList[622]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateX" 
		"rigwRN.placeHolderList[623]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateY" 
		"rigwRN.placeHolderList[624]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateZ" 
		"rigwRN.placeHolderList[625]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleX" 
		"rigwRN.placeHolderList[626]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleY" 
		"rigwRN.placeHolderList[627]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleZ" 
		"rigwRN.placeHolderList[628]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateX" 
		"rigwRN.placeHolderList[629]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateY" 
		"rigwRN.placeHolderList[630]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateZ" 
		"rigwRN.placeHolderList[631]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.visibility" 
		"rigwRN.placeHolderList[632]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateX" 
		"rigwRN.placeHolderList[633]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateY" 
		"rigwRN.placeHolderList[634]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateZ" 
		"rigwRN.placeHolderList[635]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleX" 
		"rigwRN.placeHolderList[636]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleY" 
		"rigwRN.placeHolderList[637]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleZ" 
		"rigwRN.placeHolderList[638]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateX" 
		"rigwRN.placeHolderList[639]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateY" 
		"rigwRN.placeHolderList[640]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateZ" 
		"rigwRN.placeHolderList[641]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateX" 
		"rigwRN.placeHolderList[642]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateY" 
		"rigwRN.placeHolderList[643]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateZ" 
		"rigwRN.placeHolderList[644]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.visibility" 
		"rigwRN.placeHolderList[645]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleX" 
		"rigwRN.placeHolderList[646]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleY" 
		"rigwRN.placeHolderList[647]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleZ" 
		"rigwRN.placeHolderList[648]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateX" 
		"rigwRN.placeHolderList[649]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateY" 
		"rigwRN.placeHolderList[650]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateZ" 
		"rigwRN.placeHolderList[651]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.visibility" 
		"rigwRN.placeHolderList[652]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateX" 
		"rigwRN.placeHolderList[653]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateY" 
		"rigwRN.placeHolderList[654]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateZ" 
		"rigwRN.placeHolderList[655]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleX" 
		"rigwRN.placeHolderList[656]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleY" 
		"rigwRN.placeHolderList[657]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleZ" 
		"rigwRN.placeHolderList[658]" ""
		"rigw:rigRN" 406
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "visibility" " -av 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scale" " -type \"double3\" 1.8 1.8 1.8"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"CenterBtwFeet" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotate" " -type \"double3\" 0 2.182182 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" 0 0 11.837115"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"Global" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotate" " -type \"double3\" 0 0 -5.582734"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotate" " -type \"double3\" 0 0 -3.720942"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"Global" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 -10.631262"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"IKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"IKVis" " -av -k 1 1"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateX" "rigwRN.placeHolderList[1]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateY" "rigwRN.placeHolderList[2]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateZ" "rigwRN.placeHolderList[3]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.visibility" "rigwRN.placeHolderList[4]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateX" "rigwRN.placeHolderList[5]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateY" "rigwRN.placeHolderList[6]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateZ" "rigwRN.placeHolderList[7]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleX" "rigwRN.placeHolderList[8]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleY" "rigwRN.placeHolderList[9]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleZ" "rigwRN.placeHolderList[10]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateX" 
		"rigwRN.placeHolderList[11]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateY" 
		"rigwRN.placeHolderList[12]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateZ" 
		"rigwRN.placeHolderList[13]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateX" 
		"rigwRN.placeHolderList[14]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateY" 
		"rigwRN.placeHolderList[15]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateZ" 
		"rigwRN.placeHolderList[16]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateX" 
		"rigwRN.placeHolderList[17]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateY" 
		"rigwRN.placeHolderList[18]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateZ" 
		"rigwRN.placeHolderList[19]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateX" 
		"rigwRN.placeHolderList[20]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateY" 
		"rigwRN.placeHolderList[21]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateZ" 
		"rigwRN.placeHolderList[22]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateX" 
		"rigwRN.placeHolderList[23]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateY" 
		"rigwRN.placeHolderList[24]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateZ" 
		"rigwRN.placeHolderList[25]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateX" 
		"rigwRN.placeHolderList[26]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateY" 
		"rigwRN.placeHolderList[27]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateZ" 
		"rigwRN.placeHolderList[28]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateX" 
		"rigwRN.placeHolderList[29]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateY" 
		"rigwRN.placeHolderList[30]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateZ" 
		"rigwRN.placeHolderList[31]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateX" 
		"rigwRN.placeHolderList[32]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateY" 
		"rigwRN.placeHolderList[33]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateZ" 
		"rigwRN.placeHolderList[34]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[35]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[36]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[37]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[38]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[39]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[40]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[41]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateX" 
		"rigwRN.placeHolderList[42]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateY" 
		"rigwRN.placeHolderList[43]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateZ" 
		"rigwRN.placeHolderList[44]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateX" 
		"rigwRN.placeHolderList[45]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateY" 
		"rigwRN.placeHolderList[46]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateZ" 
		"rigwRN.placeHolderList[47]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateX" 
		"rigwRN.placeHolderList[48]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateY" 
		"rigwRN.placeHolderList[49]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateZ" 
		"rigwRN.placeHolderList[50]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateX" 
		"rigwRN.placeHolderList[51]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateY" 
		"rigwRN.placeHolderList[52]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateZ" 
		"rigwRN.placeHolderList[53]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.Global" 
		"rigwRN.placeHolderList[54]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[55]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[56]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[57]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateX" 
		"rigwRN.placeHolderList[58]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateY" 
		"rigwRN.placeHolderList[59]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateZ" 
		"rigwRN.placeHolderList[60]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateX" 
		"rigwRN.placeHolderList[61]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateY" 
		"rigwRN.placeHolderList[62]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateZ" 
		"rigwRN.placeHolderList[63]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateX" 
		"rigwRN.placeHolderList[64]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateY" 
		"rigwRN.placeHolderList[65]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateZ" 
		"rigwRN.placeHolderList[66]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.Global" 
		"rigwRN.placeHolderList[67]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateX" 
		"rigwRN.placeHolderList[68]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateY" 
		"rigwRN.placeHolderList[69]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateZ" 
		"rigwRN.placeHolderList[70]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[71]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[72]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[73]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateX" 
		"rigwRN.placeHolderList[74]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateY" 
		"rigwRN.placeHolderList[75]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateZ" 
		"rigwRN.placeHolderList[76]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateX" 
		"rigwRN.placeHolderList[77]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateY" 
		"rigwRN.placeHolderList[78]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[79]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateX" 
		"rigwRN.placeHolderList[80]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateY" 
		"rigwRN.placeHolderList[81]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[82]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateX" 
		"rigwRN.placeHolderList[83]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateY" 
		"rigwRN.placeHolderList[84]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[85]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateX" 
		"rigwRN.placeHolderList[86]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateY" 
		"rigwRN.placeHolderList[87]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[88]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateX" 
		"rigwRN.placeHolderList[89]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateY" 
		"rigwRN.placeHolderList[90]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[91]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateX" 
		"rigwRN.placeHolderList[92]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateY" 
		"rigwRN.placeHolderList[93]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[94]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateX" 
		"rigwRN.placeHolderList[95]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateY" 
		"rigwRN.placeHolderList[96]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[97]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateX" 
		"rigwRN.placeHolderList[98]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateY" 
		"rigwRN.placeHolderList[99]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[100]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateX" 
		"rigwRN.placeHolderList[101]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateY" 
		"rigwRN.placeHolderList[102]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[103]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateX" 
		"rigwRN.placeHolderList[104]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateY" 
		"rigwRN.placeHolderList[105]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[106]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateX" 
		"rigwRN.placeHolderList[107]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateY" 
		"rigwRN.placeHolderList[108]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[109]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateX" 
		"rigwRN.placeHolderList[110]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateY" 
		"rigwRN.placeHolderList[111]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[112]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateX" 
		"rigwRN.placeHolderList[113]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateY" 
		"rigwRN.placeHolderList[114]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateZ" 
		"rigwRN.placeHolderList[115]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateX" 
		"rigwRN.placeHolderList[116]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateY" 
		"rigwRN.placeHolderList[117]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateZ" 
		"rigwRN.placeHolderList[118]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateX" 
		"rigwRN.placeHolderList[119]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateY" 
		"rigwRN.placeHolderList[120]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateZ" 
		"rigwRN.placeHolderList[121]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateX" 
		"rigwRN.placeHolderList[122]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateY" 
		"rigwRN.placeHolderList[123]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateZ" 
		"rigwRN.placeHolderList[124]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateX" 
		"rigwRN.placeHolderList[125]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateY" 
		"rigwRN.placeHolderList[126]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateZ" 
		"rigwRN.placeHolderList[127]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateX" 
		"rigwRN.placeHolderList[128]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateY" 
		"rigwRN.placeHolderList[129]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateZ" 
		"rigwRN.placeHolderList[130]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[131]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[132]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[133]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[134]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[135]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[136]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[137]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rollAngle" 
		"rigwRN.placeHolderList[138]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.roll" 
		"rigwRN.placeHolderList[139]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[140]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[141]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[142]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[143]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateX" 
		"rigwRN.placeHolderList[144]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateY" 
		"rigwRN.placeHolderList[145]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateZ" 
		"rigwRN.placeHolderList[146]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R.rotateX" 
		"rigwRN.placeHolderList[147]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateX" 
		"rigwRN.placeHolderList[148]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateY" 
		"rigwRN.placeHolderList[149]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateZ" 
		"rigwRN.placeHolderList[150]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.follow" 
		"rigwRN.placeHolderList[151]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[152]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[153]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[154]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[155]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[156]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[157]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[158]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rollAngle" 
		"rigwRN.placeHolderList[159]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.roll" 
		"rigwRN.placeHolderList[160]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[161]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[162]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[163]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[164]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateX" 
		"rigwRN.placeHolderList[165]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateY" 
		"rigwRN.placeHolderList[166]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateZ" 
		"rigwRN.placeHolderList[167]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L.rotateX" 
		"rigwRN.placeHolderList[168]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateX" 
		"rigwRN.placeHolderList[169]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateY" 
		"rigwRN.placeHolderList[170]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateZ" 
		"rigwRN.placeHolderList[171]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.follow" 
		"rigwRN.placeHolderList[172]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKIKBlend" 
		"rigwRN.placeHolderList[173]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.IKVis" 
		"rigwRN.placeHolderList[174]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKVis" 
		"rigwRN.placeHolderList[175]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKIKBlend" 
		"rigwRN.placeHolderList[176]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.IKVis" 
		"rigwRN.placeHolderList[177]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKVis" 
		"rigwRN.placeHolderList[178]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -sn "imrVerbosity" -ln "imrVerbosity" -dv 3 -at "short";
	addAttr -ci true -sn "imrThreads" -ln "imrThreads" -dv 1 -at "short";
	addAttr -ci true -sn "imrThreadsAdjust" -ln "imrThreadsAdjust" -dv 1 -min 0 -max 
		1 -at "bool";
	addAttr -ci true -sn "imrTaskOrder" -ln "imrTaskOrder" -dv 1 -at "short";
	addAttr -ci true -sn "imrTaskSize" -ln "imrTaskSize" -at "short";
	addAttr -ci true -sn "imrTaskAdjust" -ln "imrTaskAdjust" -dv 1 -at "short";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
	setAttr ".imrThreads" 4;
	setAttr ".imrThreadsAdjust" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 7 -ast 1 -aet 7 ";
	setAttr ".st" 6;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 -5.7886983728895327 7 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 6.7328771978018862 7 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 -0.68573458287309641 7 0;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0.37299772769355088 7 0;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 -0.038078712616230664 7 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 -5.8696506280107226 7 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 6.7323927604380609 7 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 -0.69049610632921787 7 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  -4 2.1821817979999998 -1 28.961991191131943
		 1 2.1821817979999998 2 2.1821817979999998 5 28.961991191131943 7 2.1821817979999998
		 8 2.1821817979999998;
	setAttr -s 7 ".kit[2:6]"  2 3 3 2 3;
	setAttr -s 7 ".kot[2:6]"  2 3 3 2 3;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 12.544479846644046 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 10.939831935721095 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 11.837115320000001 4 40.460845438322437
		 7 11.837115320000001;
createNode animCurveTU -n "rigw:rig:FKShoulder_R_Global";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  -4 0 -1 0 5 0 8 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  -4 0 -1 0 5 0 8 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  -4 -5.5827342970000009 -1 6.3909229438096151
		 1 -5.5827342970000009 5 6.3909229438096151 7 -5.5827342970000009 8 -5.5827342970000009;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -3 0 0 0 3 0 6 0 9 0;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  -3 0 0 0.29988017916813303 1 0 3 0 6 0.29988017916813303
		 7 0 9 0;
	setAttr -s 7 ".kit[2:6]"  2 3 3 2 3;
	setAttr -s 7 ".kot[2:6]"  2 3 3 2 3;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -3 0 0 0 3 0 6 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -3 0 0 0 3 0 6 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -3 0 0 0 3 0 6 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -3 0 0 0 3 0 6 0 9 0;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 -20.393497737641191 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 8.0727697574154522 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.7209418909999994 4 -20.693455150094231
		 7 -3.7209418909999994;
createNode animCurveTU -n "rigw:rig:FKShoulder1_L_Global";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  -4 -10.63126231 -1 -42.210905525054919 1 -10.63126231
		 5 -42.210905525054919 7 -10.63126231 8 -10.63126231;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0.14683866174083188 7 0;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0.88691026596966926 7 0;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 -5.6331543324091902 7 0;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 22.55307915661648 7 0;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 6.4207193688950319 7 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_roll";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_rollAngle";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 25 4 25 7 25;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 4 1 7 1;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 4 1 7 1;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:IKLegBall_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_R_follow";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 10 4 10 7 10;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 6.4207193688950319 7 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_roll";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_rollAngle";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 25 4 25 7 25;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 4 1 7 1;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 4 1 7 1;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:IKLegBall_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_L_follow";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 10 4 10 7 10;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKIKBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 10 4 10 7 10;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 4 1 7 1;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKIKBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 10 4 10 7 10;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 4 1 7 1;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTU -n "rigw:rig:Main_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 4 1 7 1;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTL -n "rigw:rig:Main_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:Main_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:Main_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 -0.75058826460411765 7 0;
createNode animCurveTA -n "rigw:rig:Main_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:Main_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:Main_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTU -n "rigw:rig:Main_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.8 4 1.8 7 1.8;
createNode animCurveTU -n "rigw:rig:Main_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.8 4 1.8 7 1.8;
createNode animCurveTU -n "rigw:rig:Main_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.8 4 1.8 7 1.8;
createNode animCurveTL -n "rigw:mesh:robo58_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.7404445064674029 7 0.7404445064674029;
createNode animCurveTL -n "rigw:mesh:robo58_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.3748038922897212 4 2.2010948525397072
		 7 2.3748038922897212;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.49891090393066406;
	setAttr -s 3 ".koy[2]"  -0.86665326356887817;
createNode animCurveTL -n "rigw:mesh:robo58_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.7653157998017872 4 -4.0998099903353742
		 7 -3.7653157998017872;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.28643262386322021;
	setAttr -s 3 ".koy[2]"  -0.95810037851333618;
createNode animCurveTL -n "rigw:mesh:robo59_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740301 7 0.74044450646740301;
createNode animCurveTL -n "rigw:mesh:robo59_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.9244386107160751 4 1.7507295709660613
		 7 1.9244386107160751;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.49891090393066406;
	setAttr -s 3 ".koy[2]"  -0.86665326356887817;
createNode animCurveTL -n "rigw:mesh:robo59_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.6325393508685693 4 -4.9670335414021558
		 7 -4.6325393508685693;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.28643262386322021;
	setAttr -s 3 ".koy[2]"  -0.95810037851333618;
createNode animCurveTL -n "rigw:mesh:robo60_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.7404445064674029 7 0.7404445064674029;
createNode animCurveTL -n "rigw:mesh:robo60_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.4740733291424293 4 1.2366962252712164
		 7 1.4740733291424293;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.3882276713848114;
	setAttr -s 3 ".koy[2]"  -0.92156350612640381;
createNode animCurveTL -n "rigw:mesh:robo60_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -5.4997629019353509 4 -5.63499641315516
		 7 -5.4997629019353509;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.59456336498260498;
	setAttr -s 3 ".koy[2]"  -0.80404877662658691;
createNode animCurveTL -n "rigw:mesh:robo61_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.7404445064674029 7 0.7404445064674029;
createNode animCurveTL -n "rigw:mesh:robo61_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.664250908213459 4 0.29476977603708882
		 7 0.664250908213459;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.26125043630599976;
	setAttr -s 3 ".koy[2]"  -0.96527105569839478;
createNode animCurveTL -n "rigw:mesh:robo61_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -5.5296883155265162 4 -5.4552243178010418
		 7 -5.5296883155265162;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.80205881595611572;
	setAttr -s 3 ".koy[2]"  0.59724503755569458;
createNode animCurveTL -n "rigw:mesh:robo62_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740267 7 0.74044450646740267;
createNode animCurveTL -n "rigw:mesh:robo62_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958693 7 0.36048212035958693;
createNode animCurveTL -n "rigw:mesh:robo62_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.8920852826847545 4 -4.5151752218060475
		 7 -4.8920852826847545;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo63_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.7404445064674029 7 0.7404445064674029;
createNode animCurveTL -n "rigw:mesh:robo63_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958671 7 0.36048212035958671;
createNode animCurveTL -n "rigw:mesh:robo63_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.9146490117670689 4 -3.537738950888361
		 7 -3.9146490117670689;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo64_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740301 7 0.74044450646740301;
createNode animCurveTL -n "rigw:mesh:robo64_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.3604821203595866 7 0.3604821203595866;
createNode animCurveTL -n "rigw:mesh:robo64_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -2.9372127408493891 4 -2.5603026799706812
		 7 -2.9372127408493891;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo65_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740323 7 0.74044450646740323;
createNode animCurveTL -n "rigw:mesh:robo65_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958638 7 0.36048212035958638;
createNode animCurveTL -n "rigw:mesh:robo65_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.9597764699317013 4 -1.5828664090529927
		 7 -1.9597764699317013;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo66_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740323 7 0.74044450646740323;
createNode animCurveTL -n "rigw:mesh:robo66_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958627 7 0.36048212035958627;
createNode animCurveTL -n "rigw:mesh:robo66_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.98234019901401837 4 -0.60543013813530977
		 7 -0.98234019901401837;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo67_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740323 7 0.74044450646740323;
createNode animCurveTL -n "rigw:mesh:robo67_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958616 7 0.36048212035958616;
createNode animCurveTL -n "rigw:mesh:robo67_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.0049039280963344692 4 0.3720061327823741
		 7 -0.0049039280963344692;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo68_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740367 7 0.74044450646740367;
createNode animCurveTL -n "rigw:mesh:robo68_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958593 7 0.36048212035958593;
createNode animCurveTL -n "rigw:mesh:robo68_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.97253234282135104 4 1.3494424037000596
		 7 0.97253234282135104;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo69_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740378 7 0.74044450646740378;
createNode animCurveTL -n "rigw:mesh:robo69_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958593 7 0.36048212035958593;
createNode animCurveTL -n "rigw:mesh:robo69_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.9499686137390344 4 2.3268786746177423
		 7 1.9499686137390344;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo70_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.7404445064674039 7 0.7404445064674039;
createNode animCurveTL -n "rigw:mesh:robo70_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958566 7 0.36048212035958566;
createNode animCurveTL -n "rigw:mesh:robo70_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.9274048846567204 4 3.3043149455354284
		 7 2.9274048846567204;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo71_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740378 7 0.74044450646740378;
createNode animCurveTL -n "rigw:mesh:robo71_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958554 7 0.36048212035958554;
createNode animCurveTL -n "rigw:mesh:robo71_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.9048411555744056 4 4.2817512164531122
		 7 3.9048411555744056;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo72_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740367 7 0.74044450646740367;
createNode animCurveTL -n "rigw:mesh:robo72_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.97431852244656514 4 1.3290515999051573
		 7 0.97431852244656514;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.27132716774940491;
	setAttr -s 3 ".koy[2]"  0.96248716115951538;
createNode animCurveTL -n "rigw:mesh:robo72_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.4266167344947958 4 4.6311837508818678
		 7 4.4266167344947958;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.43917280435562134;
	setAttr -s 3 ".koy[2]"  0.89840257167816162;
createNode animCurveTL -n "rigw:mesh:robo73_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740367 7 0.74044450646740367;
createNode animCurveTL -n "rigw:mesh:robo73_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.6624688403975216 4 1.8587713414221461
		 7 1.6624688403975216;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.45391443371772766;
	setAttr -s 3 ".koy[2]"  0.89104533195495605;
createNode animCurveTL -n "rigw:mesh:robo73_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.2216964860081436 4 3.8999411098555927
		 7 4.2216964860081436;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.2967914342880249;
	setAttr -s 3 ".koy[2]"  -0.95494228601455688;
createNode animCurveTL -n "rigw:mesh:robo74_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740378 7 0.74044450646740378;
createNode animCurveTL -n "rigw:mesh:robo74_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.1562213913571369 4 2.3283518924581128
		 7 2.1562213913571369;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.50233560800552368;
	setAttr -s 3 ".koy[2]"  0.86467272043228149;
createNode animCurveTL -n "rigw:mesh:robo74_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.3741913489707507 4 3.0388821117250129
		 7 3.3741913489707507;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.28579330444335938;
	setAttr -s 3 ".koy[2]"  -0.95829129219055176;
createNode animCurveTL -n "rigw:mesh:robo75_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.7404445064674039 7 0.7404445064674039;
createNode animCurveTL -n "rigw:mesh:robo75_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.6038638383367565 4 2.7759943394377324
		 7 2.6038638383367565;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.50233560800552368;
	setAttr -s 3 ".koy[2]"  0.86467272043228149;
createNode animCurveTL -n "rigw:mesh:robo75_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.502186407978328 4 2.1668771707325902
		 7 2.502186407978328;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.28579330444335938;
	setAttr -s 3 ".koy[2]"  -0.95829129219055176;
createNode animCurveTL -n "rigw:mesh:robo76_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.74143236819458302 4 0.74213297078135743
		 7 0.74143236819458302;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99997550249099731;
	setAttr -s 3 ".koy[2]"  0.0070058535784482956;
createNode animCurveTL -n "rigw:mesh:robo76_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.9535464118303563 4 3.0713086981049393
		 7 2.9535464118303563;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.64728033542633057;
	setAttr -s 3 ".koy[2]"  0.76225203275680542;
createNode animCurveTL -n "rigw:mesh:robo76_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.6396222899081851 4 1.2815821426076479
		 7 1.6396222899081851;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.26900315284729004;
	setAttr -s 3 ".koy[2]"  -0.96313923597335815;
createNode animCurveTL -n "rigw:mesh:robo79_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740301 7 0.74044450646740301;
createNode animCurveTL -n "rigw:mesh:robo79_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.8185610325301935 4 2.6448519927801795
		 7 2.8185610325301935;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.49891090393066406;
	setAttr -s 3 ".koy[2]"  -0.86665326356887817;
createNode animCurveTL -n "rigw:mesh:robo79_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -2.9108168881052552 4 -3.2453110786388426
		 7 -2.9108168881052552;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.28643262386322021;
	setAttr -s 3 ".koy[2]"  -0.95810037851333618;
createNode animCurveTL -n "rigw:mesh:robo80_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.7404445064674029 7 0.7404445064674029;
createNode animCurveTL -n "rigw:mesh:robo80_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.0506563860218527 7 3.0506563860218527;
createNode animCurveTL -n "rigw:mesh:robo80_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -2.1228898597487844 4 -2.4997999206274928
		 7 -2.1228898597487844;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  -0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo81_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740301 7 0.74044450646740301;
createNode animCurveTL -n "rigw:mesh:robo81_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.0506563860218527 7 3.0506563860218527;
createNode animCurveTL -n "rigw:mesh:robo81_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.1502270169957836 4 -1.5271370778744922
		 7 -1.1502270169957836;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  -0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo82_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.7404445064674029 7 0.7404445064674029;
createNode animCurveTL -n "rigw:mesh:robo82_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.0506563860218523 7 3.0506563860218523;
createNode animCurveTL -n "rigw:mesh:robo82_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.17756417424278392 4 -0.55447423512149252
		 7 -0.17756417424278392;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  -0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo83_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.74044450646740301 7 0.74044450646740301;
createNode animCurveTL -n "rigw:mesh:robo83_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.0506563860218523 7 3.0506563860218523;
createNode animCurveTL -n "rigw:mesh:robo83_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.79509866851021549 4 0.41818860763150689
		 7 0.79509866851021549;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  -0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo100_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667886 7 -0.74059646635667886;
createNode animCurveTL -n "rigw:mesh:robo100_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.97431852244656514 4 1.3376280155561029
		 7 0.97431852244656514;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.26537826657295227;
	setAttr -s 3 ".koy[2]"  0.96414434909820557;
createNode animCurveTL -n "rigw:mesh:robo100_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.4266167344947958 4 4.5269531994917367
		 7 4.4266167344947958;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.70591819286346436;
	setAttr -s 3 ".koy[2]"  0.70829331874847412;
createNode animCurveTL -n "rigw:mesh:robo101_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667886 7 -0.74059646635667886;
createNode animCurveTL -n "rigw:mesh:robo101_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.6624688403975216 4 1.8587713414221465
		 7 1.6624688403975216;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.45391443371772766;
	setAttr -s 3 ".koy[2]"  0.89104533195495605;
createNode animCurveTL -n "rigw:mesh:robo101_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.2216964860081436 4 3.8999411098555927
		 7 4.2216964860081436;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.2967914342880249;
	setAttr -s 3 ".koy[2]"  -0.95494228601455688;
createNode animCurveTL -n "rigw:mesh:robo102_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667886 7 -0.74059646635667886;
createNode animCurveTL -n "rigw:mesh:robo102_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.1562213913571369 4 2.3283518924581128
		 7 2.1562213913571369;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.50233560800552368;
	setAttr -s 3 ".koy[2]"  0.86467272043228149;
createNode animCurveTL -n "rigw:mesh:robo102_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.3741913489707507 4 3.0388821117250129
		 7 3.3741913489707507;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.28579330444335938;
	setAttr -s 3 ".koy[2]"  -0.95829129219055176;
createNode animCurveTL -n "rigw:mesh:robo103_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667886 7 -0.74059646635667886;
createNode animCurveTL -n "rigw:mesh:robo103_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.6038638383367565 4 2.7759943394377324
		 7 2.6038638383367565;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.50233560800552368;
	setAttr -s 3 ".koy[2]"  0.86467272043228149;
createNode animCurveTL -n "rigw:mesh:robo103_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.502186407978328 4 2.1668771707325902
		 7 2.502186407978328;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.28579330444335938;
	setAttr -s 3 ".koy[2]"  -0.95829129219055176;
createNode animCurveTL -n "rigw:mesh:robo104_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.74158453082067 4 -0.74228527719016235
		 7 -0.74158453082067;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99997550249099731;
	setAttr -s 3 ".koy[2]"  -0.0070072915405035019;
createNode animCurveTL -n "rigw:mesh:robo104_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.9535464118303563 4 3.0713086980214261
		 7 2.9535464118303563;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.64728033542633057;
	setAttr -s 3 ".koy[2]"  0.76225197315216064;
createNode animCurveTL -n "rigw:mesh:robo104_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.6396222899081851 4 1.2815821428615586
		 7 1.6396222899081851;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.26900315284729004;
	setAttr -s 3 ".koy[2]"  -0.96313923597335815;
createNode animCurveTL -n "rigw:mesh:robo107_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667742 7 -0.74059646635667742;
createNode animCurveTL -n "rigw:mesh:robo107_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.8185610325301935 4 2.6448519927801795
		 7 2.8185610325301935;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.49891090393066406;
	setAttr -s 3 ".koy[2]"  -0.86665326356887817;
createNode animCurveTL -n "rigw:mesh:robo107_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -2.9108168881052552 4 -3.2453110786388426
		 7 -2.9108168881052552;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.28643262386322021;
	setAttr -s 3 ".koy[2]"  -0.95810037851333618;
createNode animCurveTL -n "rigw:mesh:robo108_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667742 7 -0.74059646635667742;
createNode animCurveTL -n "rigw:mesh:robo108_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.0506563860218527 7 3.0506563860218527;
createNode animCurveTL -n "rigw:mesh:robo108_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -2.1228898597487844 4 -2.4997999206274928
		 7 -2.1228898597487844;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  -0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo109_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667764 7 -0.74059646635667764;
createNode animCurveTL -n "rigw:mesh:robo109_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.0506563860218527 7 3.0506563860218527;
createNode animCurveTL -n "rigw:mesh:robo109_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.1502270169957836 4 -1.5271370778744922
		 7 -1.1502270169957836;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  -0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo110_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667764 7 -0.74059646635667764;
createNode animCurveTL -n "rigw:mesh:robo110_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.0506563860218523 7 3.0506563860218523;
createNode animCurveTL -n "rigw:mesh:robo110_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.17756417424278401 4 -0.55447423512149263
		 7 -0.17756417424278401;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  -0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo111_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667797 7 -0.74059646635667797;
createNode animCurveTL -n "rigw:mesh:robo111_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.0506563860218523 7 3.0506563860218523;
createNode animCurveTL -n "rigw:mesh:robo111_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.79509866851021538 4 0.41818860763150689
		 7 0.79509866851021538;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  -0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo86_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667709 7 -0.74059646635667709;
createNode animCurveTL -n "rigw:mesh:robo86_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.3748038922897212 4 2.2010948525397072
		 7 2.3748038922897212;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.49891090393066406;
	setAttr -s 3 ".koy[2]"  -0.86665326356887817;
createNode animCurveTL -n "rigw:mesh:robo86_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.7653157998017872 4 -4.0998099903353742
		 7 -3.7653157998017872;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.28643262386322021;
	setAttr -s 3 ".koy[2]"  -0.95810037851333618;
createNode animCurveTL -n "rigw:mesh:robo87_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667709 7 -0.74059646635667709;
createNode animCurveTL -n "rigw:mesh:robo87_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.9244386107160751 4 1.7507295709660613
		 7 1.9244386107160751;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.49891090393066406;
	setAttr -s 3 ".koy[2]"  -0.86665326356887817;
createNode animCurveTL -n "rigw:mesh:robo87_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.6325393508685693 4 -4.9670335414021558
		 7 -4.6325393508685693;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.28643262386322021;
	setAttr -s 3 ".koy[2]"  -0.95810037851333618;
createNode animCurveTL -n "rigw:mesh:robo88_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667697 7 -0.74059646635667697;
createNode animCurveTL -n "rigw:mesh:robo88_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.4740733291424293 4 1.222823805894995
		 7 1.4740733291424293;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.36979681253433228;
	setAttr -s 3 ".koy[2]"  -0.92911267280578613;
createNode animCurveTL -n "rigw:mesh:robo88_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -5.4997629019353509 4 -5.5146380123171257
		 7 -5.4997629019353509;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.98911678791046143;
	setAttr -s 3 ".koy[2]"  -0.14713220298290253;
createNode animCurveTL -n "rigw:mesh:robo89_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667697 7 -0.74059646635667697;
createNode animCurveTL -n "rigw:mesh:robo89_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.664250908213459 4 0.29476977603708887
		 7 0.664250908213459;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.26125043630599976;
	setAttr -s 3 ".koy[2]"  -0.96527105569839478;
createNode animCurveTL -n "rigw:mesh:robo89_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -5.5296883155265162 4 -5.4552243178010409
		 7 -5.5296883155265162;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.80205881595611572;
	setAttr -s 3 ".koy[2]"  0.59724503755569458;
createNode animCurveTL -n "rigw:mesh:robo90_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667697 7 -0.74059646635667697;
createNode animCurveTL -n "rigw:mesh:robo90_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958693 7 0.36048212035958693;
createNode animCurveTL -n "rigw:mesh:robo90_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.8920852826847545 4 -4.5151752218060466
		 7 -4.8920852826847545;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo91_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667709 7 -0.74059646635667709;
createNode animCurveTL -n "rigw:mesh:robo91_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958671 7 0.36048212035958671;
createNode animCurveTL -n "rigw:mesh:robo91_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.9146490117670689 4 -3.537738950888361
		 7 -3.9146490117670689;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo92_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667742 7 -0.74059646635667742;
createNode animCurveTL -n "rigw:mesh:robo92_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.3604821203595866 7 0.3604821203595866;
createNode animCurveTL -n "rigw:mesh:robo92_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -2.9372127408493891 4 -2.5603026799706807
		 7 -2.9372127408493891;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo93_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667764 7 -0.74059646635667764;
createNode animCurveTL -n "rigw:mesh:robo93_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958638 7 0.36048212035958638;
createNode animCurveTL -n "rigw:mesh:robo93_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.9597764699317013 4 -1.5828664090529927
		 7 -1.9597764699317013;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo94_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667797 7 -0.74059646635667797;
createNode animCurveTL -n "rigw:mesh:robo94_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958627 7 0.36048212035958627;
createNode animCurveTL -n "rigw:mesh:robo94_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.98234019901401859 4 -0.60543013813530988
		 7 -0.98234019901401859;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo95_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667797 7 -0.74059646635667797;
createNode animCurveTL -n "rigw:mesh:robo95_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958616 7 0.36048212035958616;
createNode animCurveTL -n "rigw:mesh:robo95_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.0049039280963345602 4 0.3720061327823741
		 7 -0.0049039280963345602;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo96_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667853 7 -0.74059646635667853;
createNode animCurveTL -n "rigw:mesh:robo96_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958593 7 0.36048212035958593;
createNode animCurveTL -n "rigw:mesh:robo96_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.97253234282135093 4 1.3494424037000594
		 7 0.97253234282135093;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo97_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667875 7 -0.74059646635667875;
createNode animCurveTL -n "rigw:mesh:robo97_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958593 7 0.36048212035958593;
createNode animCurveTL -n "rigw:mesh:robo97_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.9499686137390344 4 2.3268786746177423
		 7 1.9499686137390344;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo98_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667886 7 -0.74059646635667886;
createNode animCurveTL -n "rigw:mesh:robo98_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.36048212035958566 7 0.36048212035958566;
createNode animCurveTL -n "rigw:mesh:robo98_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.9274048846567204 4 3.3043149455354284
		 7 2.9274048846567204;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.25644299387931824;
	setAttr -s 3 ".koy[2]"  0.96655935049057007;
createNode animCurveTL -n "rigw:mesh:robo99_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.74059646635667886 7 -0.74059646635667886;
createNode animCurveTL -n "rigw:mesh:robo99_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.36048212035958554 4 0.33853872010756592
		 7 0.36048212035958554;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.9767603874206543;
	setAttr -s 3 ".koy[2]"  -0.21433441340923309;
createNode animCurveTL -n "rigw:mesh:robo99_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.9048411555744056 4 4.2522435300701886
		 7 3.9048411555744056;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.27661868929862976;
	setAttr -s 3 ".koy[2]"  0.96097975969314575;
createNode animCurveTU -n "rigw:mesh:robo103_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo103_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 152.82637013887523 7 152.82637013887523;
createNode animCurveTA -n "rigw:mesh:robo103_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.5304005016724951e-31 7 3.5304005016724951e-31;
createNode animCurveTA -n "rigw:mesh:robo103_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo103_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo103_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo103_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170483 7 -0.89381656388170483;
createNode animCurveTU -n "rigw:mesh:robo104_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo104_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 161.79332643048855 4 170.61381682602556
		 7 161.79332643048855;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.54473835229873657;
	setAttr -s 3 ".koy[2]"  0.83860605955123901;
createNode animCurveTA -n "rigw:mesh:robo104_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.054505859061901765 7 0.054505859061901765;
createNode animCurveTA -n "rigw:mesh:robo104_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -179.49348949359208 7 -179.49348949359208;
createNode animCurveTU -n "rigw:mesh:robo104_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399998549429494 7 0.89399998549429494;
createNode animCurveTU -n "rigw:mesh:robo104_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381657775082757 7 0.89381657775082757;
createNode animCurveTU -n "rigw:mesh:robo104_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656451531077 7 -0.89381656451531077;
createNode animCurveTU -n "rigw:mesh:robo111_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo111_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo111_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo111_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo111_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo111_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo111_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170461 7 -0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo110_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo110_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo110_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo110_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo110_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo110_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo110_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170461 7 -0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo109_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo109_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo109_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo109_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo109_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo109_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo109_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170461 7 -0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo108_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo108_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo108_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo108_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo108_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo108_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo108_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170461 7 -0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo107_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo107_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 27.443689497109453 7 27.443689497109453;
createNode animCurveTA -n "rigw:mesh:robo107_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo107_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo107_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo107_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo107_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170461 7 -0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo86_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo86_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 27.443689497109453 7 27.443689497109453;
createNode animCurveTA -n "rigw:mesh:robo86_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo86_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo86_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo86_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo86_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170461 7 -0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo87_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo87_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 27.443689497109453 4 41.938881505139243
		 7 27.443689497109453;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.36759907007217407;
	setAttr -s 3 ".koy[2]"  0.929984450340271;
createNode animCurveTA -n "rigw:mesh:robo87_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo87_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo87_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo87_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo87_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170461 7 -0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo88_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo88_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 27.443689497109453 4 76.363342860758308
		 7 27.443689497109453;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.11632708460092545;
	setAttr -s 3 ".koy[2]"  0.99321103096008301;
createNode animCurveTA -n "rigw:mesh:robo88_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo88_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo88_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo88_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo88_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo89_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo89_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -78.605434120036833 4 -46.997692096185965
		 7 -78.605434120036833;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.1783645898103714;
	setAttr -s 3 ".koy[2]"  0.98396450281143188;
createNode animCurveTA -n "rigw:mesh:robo89_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.0608010033449902e-31 7 -7.0608010033449902e-31;
createNode animCurveTA -n "rigw:mesh:robo89_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo89_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo89_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170427 7 0.89381656388170427;
createNode animCurveTU -n "rigw:mesh:robo89_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170427 7 -0.89381656388170427;
createNode animCurveTU -n "rigw:mesh:robo90_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo90_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo90_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo90_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo90_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo90_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo90_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo91_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo91_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo91_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo91_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo91_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo91_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo91_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo92_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo92_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo92_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo92_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo92_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo92_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo92_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo93_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo93_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo93_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo93_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo93_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo93_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo93_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo94_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo94_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo94_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo94_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo94_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo94_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo94_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo95_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo95_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo95_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo95_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo95_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo95_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo95_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo96_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo96_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo96_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo96_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo96_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo96_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo96_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo97_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo97_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo97_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo97_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo97_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo97_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo97_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo98_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo98_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo98_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo98_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo98_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo98_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo98_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo99_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo99_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 36.636348589214215 7 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.1545124351978302;
	setAttr -s 3 ".koy[2]"  0.9879908561706543;
createNode animCurveTA -n "rigw:mesh:robo99_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo99_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo99_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo99_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo99_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.8938165638817045 7 -0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo100_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo100_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 74.561256627239814 4 90.952010328071168
		 7 74.561256627239814;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.32998174428939819;
	setAttr -s 3 ".koy[2]"  0.94398730993270874;
createNode animCurveTA -n "rigw:mesh:robo100_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo100_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo100_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo100_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170483 7 0.89381656388170483;
createNode animCurveTU -n "rigw:mesh:robo100_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170483 7 -0.89381656388170483;
createNode animCurveTU -n "rigw:mesh:robo101_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo101_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 148.61269226737454 7 148.61269226737454;
createNode animCurveTA -n "rigw:mesh:robo101_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo101_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo101_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo101_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo101_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170427 7 -0.89381656388170427;
createNode animCurveTU -n "rigw:mesh:robo102_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo102_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 152.82637013887523 7 152.82637013887523;
createNode animCurveTA -n "rigw:mesh:robo102_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.5304005016724951e-31 7 3.5304005016724951e-31;
createNode animCurveTA -n "rigw:mesh:robo102_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTU -n "rigw:mesh:robo102_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89399999999999979 7 0.89399999999999979;
createNode animCurveTU -n "rigw:mesh:robo102_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo102_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.89381656388170483 7 -0.89381656388170483;
createNode animCurveTU -n "rigw:mesh:robo72_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo72_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 105.4387433727602 4 83.169862099623828
		 7 105.4387433727602;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.24917542934417725;
	setAttr -s 3 ".koy[2]"  -0.96845835447311401;
createNode animCurveTA -n "rigw:mesh:robo72_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo72_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo72_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo72_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170483 7 0.89381656388170483;
createNode animCurveTU -n "rigw:mesh:robo72_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170483 7 0.89381656388170483;
createNode animCurveTU -n "rigw:mesh:robo73_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo73_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 31.387307732625455 7 31.387307732625455;
createNode animCurveTA -n "rigw:mesh:robo73_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo73_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo73_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo73_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo73_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170427 7 0.89381656388170427;
createNode animCurveTU -n "rigw:mesh:robo74_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo74_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 27.17362986112477 7 27.17362986112477;
createNode animCurveTA -n "rigw:mesh:robo74_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo74_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo74_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo74_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo74_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170483 7 0.89381656388170483;
createNode animCurveTU -n "rigw:mesh:robo75_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo75_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 27.17362986112477 7 27.17362986112477;
createNode animCurveTA -n "rigw:mesh:robo75_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo75_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo75_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo75_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo75_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170483 7 0.89381656388170483;
createNode animCurveTU -n "rigw:mesh:robo76_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo76_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 18.206673660641989 4 10.819455335561658
		 7 18.206673660641989;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.61287128925323486;
	setAttr -s 3 ".koy[2]"  -0.79018276929855347;
createNode animCurveTA -n "rigw:mesh:robo76_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.054517044305025741 7 0.054517044305025741;
createNode animCurveTA -n "rigw:mesh:robo76_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5066144510904842 7 -0.5066144510904842;
createNode animCurveTU -n "rigw:mesh:robo76_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170494 7 0.89381656388170494;
createNode animCurveTU -n "rigw:mesh:robo76_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo76_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170483 7 0.89381656388170483;
createNode animCurveTU -n "rigw:mesh:robo83_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo83_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo83_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.9012298073891394e-31 7 -7.9012298073891394e-31;
createNode animCurveTA -n "rigw:mesh:robo83_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo83_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo83_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo83_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo82_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo82_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo82_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.9012298073891394e-31 7 -7.9012298073891394e-31;
createNode animCurveTA -n "rigw:mesh:robo82_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo82_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo82_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo82_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo81_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo81_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo81_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.9012298073891394e-31 7 -7.9012298073891394e-31;
createNode animCurveTA -n "rigw:mesh:robo81_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo81_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo81_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo81_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo80_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo80_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:mesh:robo80_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.9012298073891394e-31 7 -7.9012298073891394e-31;
createNode animCurveTA -n "rigw:mesh:robo80_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:mesh:robo80_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo80_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo80_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo79_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo79_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -27.443689497109453 7 -27.443689497109453;
createNode animCurveTA -n "rigw:mesh:robo79_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1.5802459614778258e-30 7 -1.5802459614778258e-30;
createNode animCurveTA -n "rigw:mesh:robo79_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.5311250384401291e-31 7 -3.5311250384401291e-31;
createNode animCurveTU -n "rigw:mesh:robo79_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo79_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo79_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo58_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo58_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -27.443689497109453 7 -27.443689497109453;
createNode animCurveTA -n "rigw:mesh:robo58_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1.5802459614778258e-30 7 -1.5802459614778258e-30;
createNode animCurveTA -n "rigw:mesh:robo58_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.5311250384401291e-31 7 -3.5311250384401291e-31;
createNode animCurveTU -n "rigw:mesh:robo58_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo58_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo58_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo59_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo59_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -27.443689497109453 4 -39.517900279817511
		 7 -27.443689497109453;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.42871052026748657;
	setAttr -s 3 ".koy[2]"  -0.9034419059753418;
createNode animCurveTA -n "rigw:mesh:robo59_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1.5802459614778258e-30 7 -1.5802459614778258e-30;
createNode animCurveTA -n "rigw:mesh:robo59_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.5311250384401291e-31 7 -3.5311250384401291e-31;
createNode animCurveTU -n "rigw:mesh:robo59_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo59_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo59_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo60_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo60_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -27.443689497109453 4 -72.280203064354055
		 7 -27.443689497109453;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.12675745785236359;
	setAttr -s 3 ".koy[2]"  -0.99193376302719116;
createNode animCurveTA -n "rigw:mesh:robo60_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1.5802459614778258e-30 7 -1.5802459614778258e-30;
createNode animCurveTA -n "rigw:mesh:robo60_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.5311250384401291e-31 7 -3.5311250384401291e-31;
createNode animCurveTU -n "rigw:mesh:robo60_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo60_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo60_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo61_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo61_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -101.39456587996317 4 -140.02695871653387
		 7 -101.39456587996317;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.14670552313327789;
	setAttr -s 3 ".koy[2]"  -0.98918020725250244;
createNode animCurveTA -n "rigw:mesh:robo61_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.0167092985348775e-15 7 -7.0167092985348775e-15;
createNode animCurveTA -n "rigw:mesh:robo61_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo61_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo61_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170427 7 0.89381656388170427;
createNode animCurveTU -n "rigw:mesh:robo61_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170427 7 0.89381656388170427;
createNode animCurveTU -n "rigw:mesh:robo62_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo62_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo62_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo62_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo62_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo62_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo62_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo63_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo63_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo63_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo63_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo63_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo63_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo63_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo64_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo64_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo64_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo64_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo64_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo64_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo64_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo65_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo65_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo65_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo65_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo65_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo65_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo65_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo66_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo66_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo66_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo66_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo66_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo66_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo66_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo67_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo67_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo67_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo67_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo67_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo67_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo67_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo68_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo68_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo68_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo68_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo68_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo68_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo68_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo69_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo69_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo69_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo69_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo69_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo69_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo69_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo70_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo70_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 7 180;
createNode animCurveTA -n "rigw:mesh:robo70_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo70_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo70_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo70_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo70_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo71_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 7 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:mesh:robo71_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 180 4 142.17933504242328 7 180;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.14978428184986115;
	setAttr -s 3 ".koy[2]"  -0.98871868848800659;
createNode animCurveTA -n "rigw:mesh:robo71_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -7.016709298534876e-15 7 -7.016709298534876e-15;
createNode animCurveTA -n "rigw:mesh:robo71_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTU -n "rigw:mesh:robo71_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.89381656388170461 7 0.89381656388170461;
createNode animCurveTU -n "rigw:mesh:robo71_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
createNode animCurveTU -n "rigw:mesh:robo71_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.8938165638817045 7 0.8938165638817045;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 7;
	setAttr ".unw" 7;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 16 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 16 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 32 ".tx";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 46 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w";
	setAttr -av ".h";
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 2 ".sol";
connectAttr "rigw:mesh:robo72_translateX.o" "rigwRN.phl[179]";
connectAttr "rigw:mesh:robo72_translateY.o" "rigwRN.phl[180]";
connectAttr "rigw:mesh:robo72_translateZ.o" "rigwRN.phl[181]";
connectAttr "rigw:mesh:robo72_rotateX.o" "rigwRN.phl[182]";
connectAttr "rigw:mesh:robo72_rotateY.o" "rigwRN.phl[183]";
connectAttr "rigw:mesh:robo72_rotateZ.o" "rigwRN.phl[184]";
connectAttr "rigw:mesh:robo72_visibility.o" "rigwRN.phl[185]";
connectAttr "rigw:mesh:robo72_scaleX.o" "rigwRN.phl[186]";
connectAttr "rigw:mesh:robo72_scaleY.o" "rigwRN.phl[187]";
connectAttr "rigw:mesh:robo72_scaleZ.o" "rigwRN.phl[188]";
connectAttr "rigw:mesh:robo73_translateX.o" "rigwRN.phl[189]";
connectAttr "rigw:mesh:robo73_translateY.o" "rigwRN.phl[190]";
connectAttr "rigw:mesh:robo73_translateZ.o" "rigwRN.phl[191]";
connectAttr "rigw:mesh:robo73_visibility.o" "rigwRN.phl[192]";
connectAttr "rigw:mesh:robo73_rotateX.o" "rigwRN.phl[193]";
connectAttr "rigw:mesh:robo73_rotateY.o" "rigwRN.phl[194]";
connectAttr "rigw:mesh:robo73_rotateZ.o" "rigwRN.phl[195]";
connectAttr "rigw:mesh:robo73_scaleX.o" "rigwRN.phl[196]";
connectAttr "rigw:mesh:robo73_scaleY.o" "rigwRN.phl[197]";
connectAttr "rigw:mesh:robo73_scaleZ.o" "rigwRN.phl[198]";
connectAttr "rigw:mesh:robo74_translateX.o" "rigwRN.phl[199]";
connectAttr "rigw:mesh:robo74_translateY.o" "rigwRN.phl[200]";
connectAttr "rigw:mesh:robo74_translateZ.o" "rigwRN.phl[201]";
connectAttr "rigw:mesh:robo74_visibility.o" "rigwRN.phl[202]";
connectAttr "rigw:mesh:robo74_rotateX.o" "rigwRN.phl[203]";
connectAttr "rigw:mesh:robo74_rotateY.o" "rigwRN.phl[204]";
connectAttr "rigw:mesh:robo74_rotateZ.o" "rigwRN.phl[205]";
connectAttr "rigw:mesh:robo74_scaleX.o" "rigwRN.phl[206]";
connectAttr "rigw:mesh:robo74_scaleY.o" "rigwRN.phl[207]";
connectAttr "rigw:mesh:robo74_scaleZ.o" "rigwRN.phl[208]";
connectAttr "rigw:mesh:robo75_translateX.o" "rigwRN.phl[209]";
connectAttr "rigw:mesh:robo75_translateY.o" "rigwRN.phl[210]";
connectAttr "rigw:mesh:robo75_translateZ.o" "rigwRN.phl[211]";
connectAttr "rigw:mesh:robo75_visibility.o" "rigwRN.phl[212]";
connectAttr "rigw:mesh:robo75_rotateX.o" "rigwRN.phl[213]";
connectAttr "rigw:mesh:robo75_rotateY.o" "rigwRN.phl[214]";
connectAttr "rigw:mesh:robo75_rotateZ.o" "rigwRN.phl[215]";
connectAttr "rigw:mesh:robo75_scaleX.o" "rigwRN.phl[216]";
connectAttr "rigw:mesh:robo75_scaleY.o" "rigwRN.phl[217]";
connectAttr "rigw:mesh:robo75_scaleZ.o" "rigwRN.phl[218]";
connectAttr "rigw:mesh:robo76_translateX.o" "rigwRN.phl[219]";
connectAttr "rigw:mesh:robo76_translateY.o" "rigwRN.phl[220]";
connectAttr "rigw:mesh:robo76_translateZ.o" "rigwRN.phl[221]";
connectAttr "rigw:mesh:robo76_rotateX.o" "rigwRN.phl[222]";
connectAttr "rigw:mesh:robo76_rotateY.o" "rigwRN.phl[223]";
connectAttr "rigw:mesh:robo76_rotateZ.o" "rigwRN.phl[224]";
connectAttr "rigw:mesh:robo76_visibility.o" "rigwRN.phl[225]";
connectAttr "rigw:mesh:robo76_scaleX.o" "rigwRN.phl[226]";
connectAttr "rigw:mesh:robo76_scaleY.o" "rigwRN.phl[227]";
connectAttr "rigw:mesh:robo76_scaleZ.o" "rigwRN.phl[228]";
connectAttr "rigw:mesh:robo83_translateX.o" "rigwRN.phl[229]";
connectAttr "rigw:mesh:robo83_translateY.o" "rigwRN.phl[230]";
connectAttr "rigw:mesh:robo83_translateZ.o" "rigwRN.phl[231]";
connectAttr "rigw:mesh:robo83_visibility.o" "rigwRN.phl[232]";
connectAttr "rigw:mesh:robo83_rotateX.o" "rigwRN.phl[233]";
connectAttr "rigw:mesh:robo83_rotateY.o" "rigwRN.phl[234]";
connectAttr "rigw:mesh:robo83_rotateZ.o" "rigwRN.phl[235]";
connectAttr "rigw:mesh:robo83_scaleX.o" "rigwRN.phl[236]";
connectAttr "rigw:mesh:robo83_scaleY.o" "rigwRN.phl[237]";
connectAttr "rigw:mesh:robo83_scaleZ.o" "rigwRN.phl[238]";
connectAttr "rigw:mesh:robo82_translateX.o" "rigwRN.phl[239]";
connectAttr "rigw:mesh:robo82_translateY.o" "rigwRN.phl[240]";
connectAttr "rigw:mesh:robo82_translateZ.o" "rigwRN.phl[241]";
connectAttr "rigw:mesh:robo82_visibility.o" "rigwRN.phl[242]";
connectAttr "rigw:mesh:robo82_rotateX.o" "rigwRN.phl[243]";
connectAttr "rigw:mesh:robo82_rotateY.o" "rigwRN.phl[244]";
connectAttr "rigw:mesh:robo82_rotateZ.o" "rigwRN.phl[245]";
connectAttr "rigw:mesh:robo82_scaleX.o" "rigwRN.phl[246]";
connectAttr "rigw:mesh:robo82_scaleY.o" "rigwRN.phl[247]";
connectAttr "rigw:mesh:robo82_scaleZ.o" "rigwRN.phl[248]";
connectAttr "rigw:mesh:robo81_translateX.o" "rigwRN.phl[249]";
connectAttr "rigw:mesh:robo81_translateY.o" "rigwRN.phl[250]";
connectAttr "rigw:mesh:robo81_translateZ.o" "rigwRN.phl[251]";
connectAttr "rigw:mesh:robo81_visibility.o" "rigwRN.phl[252]";
connectAttr "rigw:mesh:robo81_rotateX.o" "rigwRN.phl[253]";
connectAttr "rigw:mesh:robo81_rotateY.o" "rigwRN.phl[254]";
connectAttr "rigw:mesh:robo81_rotateZ.o" "rigwRN.phl[255]";
connectAttr "rigw:mesh:robo81_scaleX.o" "rigwRN.phl[256]";
connectAttr "rigw:mesh:robo81_scaleY.o" "rigwRN.phl[257]";
connectAttr "rigw:mesh:robo81_scaleZ.o" "rigwRN.phl[258]";
connectAttr "rigw:mesh:robo80_translateX.o" "rigwRN.phl[259]";
connectAttr "rigw:mesh:robo80_translateY.o" "rigwRN.phl[260]";
connectAttr "rigw:mesh:robo80_translateZ.o" "rigwRN.phl[261]";
connectAttr "rigw:mesh:robo80_visibility.o" "rigwRN.phl[262]";
connectAttr "rigw:mesh:robo80_rotateX.o" "rigwRN.phl[263]";
connectAttr "rigw:mesh:robo80_rotateY.o" "rigwRN.phl[264]";
connectAttr "rigw:mesh:robo80_rotateZ.o" "rigwRN.phl[265]";
connectAttr "rigw:mesh:robo80_scaleX.o" "rigwRN.phl[266]";
connectAttr "rigw:mesh:robo80_scaleY.o" "rigwRN.phl[267]";
connectAttr "rigw:mesh:robo80_scaleZ.o" "rigwRN.phl[268]";
connectAttr "rigw:mesh:robo79_translateX.o" "rigwRN.phl[269]";
connectAttr "rigw:mesh:robo79_translateY.o" "rigwRN.phl[270]";
connectAttr "rigw:mesh:robo79_translateZ.o" "rigwRN.phl[271]";
connectAttr "rigw:mesh:robo79_visibility.o" "rigwRN.phl[272]";
connectAttr "rigw:mesh:robo79_rotateX.o" "rigwRN.phl[273]";
connectAttr "rigw:mesh:robo79_rotateY.o" "rigwRN.phl[274]";
connectAttr "rigw:mesh:robo79_rotateZ.o" "rigwRN.phl[275]";
connectAttr "rigw:mesh:robo79_scaleX.o" "rigwRN.phl[276]";
connectAttr "rigw:mesh:robo79_scaleY.o" "rigwRN.phl[277]";
connectAttr "rigw:mesh:robo79_scaleZ.o" "rigwRN.phl[278]";
connectAttr "rigw:mesh:robo58_translateX.o" "rigwRN.phl[279]";
connectAttr "rigw:mesh:robo58_translateY.o" "rigwRN.phl[280]";
connectAttr "rigw:mesh:robo58_translateZ.o" "rigwRN.phl[281]";
connectAttr "rigw:mesh:robo58_visibility.o" "rigwRN.phl[282]";
connectAttr "rigw:mesh:robo58_rotateX.o" "rigwRN.phl[283]";
connectAttr "rigw:mesh:robo58_rotateY.o" "rigwRN.phl[284]";
connectAttr "rigw:mesh:robo58_rotateZ.o" "rigwRN.phl[285]";
connectAttr "rigw:mesh:robo58_scaleX.o" "rigwRN.phl[286]";
connectAttr "rigw:mesh:robo58_scaleY.o" "rigwRN.phl[287]";
connectAttr "rigw:mesh:robo58_scaleZ.o" "rigwRN.phl[288]";
connectAttr "rigw:mesh:robo59_translateX.o" "rigwRN.phl[289]";
connectAttr "rigw:mesh:robo59_translateY.o" "rigwRN.phl[290]";
connectAttr "rigw:mesh:robo59_translateZ.o" "rigwRN.phl[291]";
connectAttr "rigw:mesh:robo59_rotateX.o" "rigwRN.phl[292]";
connectAttr "rigw:mesh:robo59_rotateY.o" "rigwRN.phl[293]";
connectAttr "rigw:mesh:robo59_rotateZ.o" "rigwRN.phl[294]";
connectAttr "rigw:mesh:robo59_visibility.o" "rigwRN.phl[295]";
connectAttr "rigw:mesh:robo59_scaleX.o" "rigwRN.phl[296]";
connectAttr "rigw:mesh:robo59_scaleY.o" "rigwRN.phl[297]";
connectAttr "rigw:mesh:robo59_scaleZ.o" "rigwRN.phl[298]";
connectAttr "rigw:mesh:robo60_translateX.o" "rigwRN.phl[299]";
connectAttr "rigw:mesh:robo60_translateY.o" "rigwRN.phl[300]";
connectAttr "rigw:mesh:robo60_translateZ.o" "rigwRN.phl[301]";
connectAttr "rigw:mesh:robo60_rotateX.o" "rigwRN.phl[302]";
connectAttr "rigw:mesh:robo60_rotateY.o" "rigwRN.phl[303]";
connectAttr "rigw:mesh:robo60_rotateZ.o" "rigwRN.phl[304]";
connectAttr "rigw:mesh:robo60_visibility.o" "rigwRN.phl[305]";
connectAttr "rigw:mesh:robo60_scaleX.o" "rigwRN.phl[306]";
connectAttr "rigw:mesh:robo60_scaleY.o" "rigwRN.phl[307]";
connectAttr "rigw:mesh:robo60_scaleZ.o" "rigwRN.phl[308]";
connectAttr "rigw:mesh:robo61_translateX.o" "rigwRN.phl[309]";
connectAttr "rigw:mesh:robo61_translateY.o" "rigwRN.phl[310]";
connectAttr "rigw:mesh:robo61_translateZ.o" "rigwRN.phl[311]";
connectAttr "rigw:mesh:robo61_rotateX.o" "rigwRN.phl[312]";
connectAttr "rigw:mesh:robo61_rotateY.o" "rigwRN.phl[313]";
connectAttr "rigw:mesh:robo61_rotateZ.o" "rigwRN.phl[314]";
connectAttr "rigw:mesh:robo61_visibility.o" "rigwRN.phl[315]";
connectAttr "rigw:mesh:robo61_scaleX.o" "rigwRN.phl[316]";
connectAttr "rigw:mesh:robo61_scaleY.o" "rigwRN.phl[317]";
connectAttr "rigw:mesh:robo61_scaleZ.o" "rigwRN.phl[318]";
connectAttr "rigw:mesh:robo62_translateX.o" "rigwRN.phl[319]";
connectAttr "rigw:mesh:robo62_translateY.o" "rigwRN.phl[320]";
connectAttr "rigw:mesh:robo62_translateZ.o" "rigwRN.phl[321]";
connectAttr "rigw:mesh:robo62_visibility.o" "rigwRN.phl[322]";
connectAttr "rigw:mesh:robo62_rotateX.o" "rigwRN.phl[323]";
connectAttr "rigw:mesh:robo62_rotateY.o" "rigwRN.phl[324]";
connectAttr "rigw:mesh:robo62_rotateZ.o" "rigwRN.phl[325]";
connectAttr "rigw:mesh:robo62_scaleX.o" "rigwRN.phl[326]";
connectAttr "rigw:mesh:robo62_scaleY.o" "rigwRN.phl[327]";
connectAttr "rigw:mesh:robo62_scaleZ.o" "rigwRN.phl[328]";
connectAttr "rigw:mesh:robo63_translateX.o" "rigwRN.phl[329]";
connectAttr "rigw:mesh:robo63_translateY.o" "rigwRN.phl[330]";
connectAttr "rigw:mesh:robo63_translateZ.o" "rigwRN.phl[331]";
connectAttr "rigw:mesh:robo63_visibility.o" "rigwRN.phl[332]";
connectAttr "rigw:mesh:robo63_rotateX.o" "rigwRN.phl[333]";
connectAttr "rigw:mesh:robo63_rotateY.o" "rigwRN.phl[334]";
connectAttr "rigw:mesh:robo63_rotateZ.o" "rigwRN.phl[335]";
connectAttr "rigw:mesh:robo63_scaleX.o" "rigwRN.phl[336]";
connectAttr "rigw:mesh:robo63_scaleY.o" "rigwRN.phl[337]";
connectAttr "rigw:mesh:robo63_scaleZ.o" "rigwRN.phl[338]";
connectAttr "rigw:mesh:robo64_translateX.o" "rigwRN.phl[339]";
connectAttr "rigw:mesh:robo64_translateY.o" "rigwRN.phl[340]";
connectAttr "rigw:mesh:robo64_translateZ.o" "rigwRN.phl[341]";
connectAttr "rigw:mesh:robo64_visibility.o" "rigwRN.phl[342]";
connectAttr "rigw:mesh:robo64_rotateX.o" "rigwRN.phl[343]";
connectAttr "rigw:mesh:robo64_rotateY.o" "rigwRN.phl[344]";
connectAttr "rigw:mesh:robo64_rotateZ.o" "rigwRN.phl[345]";
connectAttr "rigw:mesh:robo64_scaleX.o" "rigwRN.phl[346]";
connectAttr "rigw:mesh:robo64_scaleY.o" "rigwRN.phl[347]";
connectAttr "rigw:mesh:robo64_scaleZ.o" "rigwRN.phl[348]";
connectAttr "rigw:mesh:robo65_translateX.o" "rigwRN.phl[349]";
connectAttr "rigw:mesh:robo65_translateY.o" "rigwRN.phl[350]";
connectAttr "rigw:mesh:robo65_translateZ.o" "rigwRN.phl[351]";
connectAttr "rigw:mesh:robo65_visibility.o" "rigwRN.phl[352]";
connectAttr "rigw:mesh:robo65_rotateX.o" "rigwRN.phl[353]";
connectAttr "rigw:mesh:robo65_rotateY.o" "rigwRN.phl[354]";
connectAttr "rigw:mesh:robo65_rotateZ.o" "rigwRN.phl[355]";
connectAttr "rigw:mesh:robo65_scaleX.o" "rigwRN.phl[356]";
connectAttr "rigw:mesh:robo65_scaleY.o" "rigwRN.phl[357]";
connectAttr "rigw:mesh:robo65_scaleZ.o" "rigwRN.phl[358]";
connectAttr "rigw:mesh:robo66_translateX.o" "rigwRN.phl[359]";
connectAttr "rigw:mesh:robo66_translateY.o" "rigwRN.phl[360]";
connectAttr "rigw:mesh:robo66_translateZ.o" "rigwRN.phl[361]";
connectAttr "rigw:mesh:robo66_visibility.o" "rigwRN.phl[362]";
connectAttr "rigw:mesh:robo66_rotateX.o" "rigwRN.phl[363]";
connectAttr "rigw:mesh:robo66_rotateY.o" "rigwRN.phl[364]";
connectAttr "rigw:mesh:robo66_rotateZ.o" "rigwRN.phl[365]";
connectAttr "rigw:mesh:robo66_scaleX.o" "rigwRN.phl[366]";
connectAttr "rigw:mesh:robo66_scaleY.o" "rigwRN.phl[367]";
connectAttr "rigw:mesh:robo66_scaleZ.o" "rigwRN.phl[368]";
connectAttr "rigw:mesh:robo67_translateX.o" "rigwRN.phl[369]";
connectAttr "rigw:mesh:robo67_translateY.o" "rigwRN.phl[370]";
connectAttr "rigw:mesh:robo67_translateZ.o" "rigwRN.phl[371]";
connectAttr "rigw:mesh:robo67_visibility.o" "rigwRN.phl[372]";
connectAttr "rigw:mesh:robo67_rotateX.o" "rigwRN.phl[373]";
connectAttr "rigw:mesh:robo67_rotateY.o" "rigwRN.phl[374]";
connectAttr "rigw:mesh:robo67_rotateZ.o" "rigwRN.phl[375]";
connectAttr "rigw:mesh:robo67_scaleX.o" "rigwRN.phl[376]";
connectAttr "rigw:mesh:robo67_scaleY.o" "rigwRN.phl[377]";
connectAttr "rigw:mesh:robo67_scaleZ.o" "rigwRN.phl[378]";
connectAttr "rigw:mesh:robo68_translateX.o" "rigwRN.phl[379]";
connectAttr "rigw:mesh:robo68_translateY.o" "rigwRN.phl[380]";
connectAttr "rigw:mesh:robo68_translateZ.o" "rigwRN.phl[381]";
connectAttr "rigw:mesh:robo68_visibility.o" "rigwRN.phl[382]";
connectAttr "rigw:mesh:robo68_rotateX.o" "rigwRN.phl[383]";
connectAttr "rigw:mesh:robo68_rotateY.o" "rigwRN.phl[384]";
connectAttr "rigw:mesh:robo68_rotateZ.o" "rigwRN.phl[385]";
connectAttr "rigw:mesh:robo68_scaleX.o" "rigwRN.phl[386]";
connectAttr "rigw:mesh:robo68_scaleY.o" "rigwRN.phl[387]";
connectAttr "rigw:mesh:robo68_scaleZ.o" "rigwRN.phl[388]";
connectAttr "rigw:mesh:robo69_translateX.o" "rigwRN.phl[389]";
connectAttr "rigw:mesh:robo69_translateY.o" "rigwRN.phl[390]";
connectAttr "rigw:mesh:robo69_translateZ.o" "rigwRN.phl[391]";
connectAttr "rigw:mesh:robo69_visibility.o" "rigwRN.phl[392]";
connectAttr "rigw:mesh:robo69_rotateX.o" "rigwRN.phl[393]";
connectAttr "rigw:mesh:robo69_rotateY.o" "rigwRN.phl[394]";
connectAttr "rigw:mesh:robo69_rotateZ.o" "rigwRN.phl[395]";
connectAttr "rigw:mesh:robo69_scaleX.o" "rigwRN.phl[396]";
connectAttr "rigw:mesh:robo69_scaleY.o" "rigwRN.phl[397]";
connectAttr "rigw:mesh:robo69_scaleZ.o" "rigwRN.phl[398]";
connectAttr "rigw:mesh:robo70_translateX.o" "rigwRN.phl[399]";
connectAttr "rigw:mesh:robo70_translateY.o" "rigwRN.phl[400]";
connectAttr "rigw:mesh:robo70_translateZ.o" "rigwRN.phl[401]";
connectAttr "rigw:mesh:robo70_visibility.o" "rigwRN.phl[402]";
connectAttr "rigw:mesh:robo70_rotateX.o" "rigwRN.phl[403]";
connectAttr "rigw:mesh:robo70_rotateY.o" "rigwRN.phl[404]";
connectAttr "rigw:mesh:robo70_rotateZ.o" "rigwRN.phl[405]";
connectAttr "rigw:mesh:robo70_scaleX.o" "rigwRN.phl[406]";
connectAttr "rigw:mesh:robo70_scaleY.o" "rigwRN.phl[407]";
connectAttr "rigw:mesh:robo70_scaleZ.o" "rigwRN.phl[408]";
connectAttr "rigw:mesh:robo71_translateX.o" "rigwRN.phl[409]";
connectAttr "rigw:mesh:robo71_translateY.o" "rigwRN.phl[410]";
connectAttr "rigw:mesh:robo71_translateZ.o" "rigwRN.phl[411]";
connectAttr "rigw:mesh:robo71_rotateX.o" "rigwRN.phl[412]";
connectAttr "rigw:mesh:robo71_rotateY.o" "rigwRN.phl[413]";
connectAttr "rigw:mesh:robo71_rotateZ.o" "rigwRN.phl[414]";
connectAttr "rigw:mesh:robo71_visibility.o" "rigwRN.phl[415]";
connectAttr "rigw:mesh:robo71_scaleX.o" "rigwRN.phl[416]";
connectAttr "rigw:mesh:robo71_scaleY.o" "rigwRN.phl[417]";
connectAttr "rigw:mesh:robo71_scaleZ.o" "rigwRN.phl[418]";
connectAttr "rigw:mesh:robo102_translateX.o" "rigwRN.phl[419]";
connectAttr "rigw:mesh:robo102_translateY.o" "rigwRN.phl[420]";
connectAttr "rigw:mesh:robo102_translateZ.o" "rigwRN.phl[421]";
connectAttr "rigw:mesh:robo102_visibility.o" "rigwRN.phl[422]";
connectAttr "rigw:mesh:robo102_rotateX.o" "rigwRN.phl[423]";
connectAttr "rigw:mesh:robo102_rotateY.o" "rigwRN.phl[424]";
connectAttr "rigw:mesh:robo102_rotateZ.o" "rigwRN.phl[425]";
connectAttr "rigw:mesh:robo102_scaleX.o" "rigwRN.phl[426]";
connectAttr "rigw:mesh:robo102_scaleY.o" "rigwRN.phl[427]";
connectAttr "rigw:mesh:robo102_scaleZ.o" "rigwRN.phl[428]";
connectAttr "rigw:mesh:robo101_translateX.o" "rigwRN.phl[429]";
connectAttr "rigw:mesh:robo101_translateY.o" "rigwRN.phl[430]";
connectAttr "rigw:mesh:robo101_translateZ.o" "rigwRN.phl[431]";
connectAttr "rigw:mesh:robo101_visibility.o" "rigwRN.phl[432]";
connectAttr "rigw:mesh:robo101_rotateX.o" "rigwRN.phl[433]";
connectAttr "rigw:mesh:robo101_rotateY.o" "rigwRN.phl[434]";
connectAttr "rigw:mesh:robo101_rotateZ.o" "rigwRN.phl[435]";
connectAttr "rigw:mesh:robo101_scaleX.o" "rigwRN.phl[436]";
connectAttr "rigw:mesh:robo101_scaleY.o" "rigwRN.phl[437]";
connectAttr "rigw:mesh:robo101_scaleZ.o" "rigwRN.phl[438]";
connectAttr "rigw:mesh:robo100_translateX.o" "rigwRN.phl[439]";
connectAttr "rigw:mesh:robo100_translateY.o" "rigwRN.phl[440]";
connectAttr "rigw:mesh:robo100_translateZ.o" "rigwRN.phl[441]";
connectAttr "rigw:mesh:robo100_rotateX.o" "rigwRN.phl[442]";
connectAttr "rigw:mesh:robo100_rotateY.o" "rigwRN.phl[443]";
connectAttr "rigw:mesh:robo100_rotateZ.o" "rigwRN.phl[444]";
connectAttr "rigw:mesh:robo100_visibility.o" "rigwRN.phl[445]";
connectAttr "rigw:mesh:robo100_scaleX.o" "rigwRN.phl[446]";
connectAttr "rigw:mesh:robo100_scaleY.o" "rigwRN.phl[447]";
connectAttr "rigw:mesh:robo100_scaleZ.o" "rigwRN.phl[448]";
connectAttr "rigw:mesh:robo99_translateX.o" "rigwRN.phl[449]";
connectAttr "rigw:mesh:robo99_translateY.o" "rigwRN.phl[450]";
connectAttr "rigw:mesh:robo99_translateZ.o" "rigwRN.phl[451]";
connectAttr "rigw:mesh:robo99_rotateX.o" "rigwRN.phl[452]";
connectAttr "rigw:mesh:robo99_rotateY.o" "rigwRN.phl[453]";
connectAttr "rigw:mesh:robo99_rotateZ.o" "rigwRN.phl[454]";
connectAttr "rigw:mesh:robo99_visibility.o" "rigwRN.phl[455]";
connectAttr "rigw:mesh:robo99_scaleX.o" "rigwRN.phl[456]";
connectAttr "rigw:mesh:robo99_scaleY.o" "rigwRN.phl[457]";
connectAttr "rigw:mesh:robo99_scaleZ.o" "rigwRN.phl[458]";
connectAttr "rigw:mesh:robo98_translateX.o" "rigwRN.phl[459]";
connectAttr "rigw:mesh:robo98_translateY.o" "rigwRN.phl[460]";
connectAttr "rigw:mesh:robo98_translateZ.o" "rigwRN.phl[461]";
connectAttr "rigw:mesh:robo98_visibility.o" "rigwRN.phl[462]";
connectAttr "rigw:mesh:robo98_rotateX.o" "rigwRN.phl[463]";
connectAttr "rigw:mesh:robo98_rotateY.o" "rigwRN.phl[464]";
connectAttr "rigw:mesh:robo98_rotateZ.o" "rigwRN.phl[465]";
connectAttr "rigw:mesh:robo98_scaleX.o" "rigwRN.phl[466]";
connectAttr "rigw:mesh:robo98_scaleY.o" "rigwRN.phl[467]";
connectAttr "rigw:mesh:robo98_scaleZ.o" "rigwRN.phl[468]";
connectAttr "rigw:mesh:robo97_translateX.o" "rigwRN.phl[469]";
connectAttr "rigw:mesh:robo97_translateY.o" "rigwRN.phl[470]";
connectAttr "rigw:mesh:robo97_translateZ.o" "rigwRN.phl[471]";
connectAttr "rigw:mesh:robo97_visibility.o" "rigwRN.phl[472]";
connectAttr "rigw:mesh:robo97_rotateX.o" "rigwRN.phl[473]";
connectAttr "rigw:mesh:robo97_rotateY.o" "rigwRN.phl[474]";
connectAttr "rigw:mesh:robo97_rotateZ.o" "rigwRN.phl[475]";
connectAttr "rigw:mesh:robo97_scaleX.o" "rigwRN.phl[476]";
connectAttr "rigw:mesh:robo97_scaleY.o" "rigwRN.phl[477]";
connectAttr "rigw:mesh:robo97_scaleZ.o" "rigwRN.phl[478]";
connectAttr "rigw:mesh:robo96_translateX.o" "rigwRN.phl[479]";
connectAttr "rigw:mesh:robo96_translateY.o" "rigwRN.phl[480]";
connectAttr "rigw:mesh:robo96_translateZ.o" "rigwRN.phl[481]";
connectAttr "rigw:mesh:robo96_visibility.o" "rigwRN.phl[482]";
connectAttr "rigw:mesh:robo96_rotateX.o" "rigwRN.phl[483]";
connectAttr "rigw:mesh:robo96_rotateY.o" "rigwRN.phl[484]";
connectAttr "rigw:mesh:robo96_rotateZ.o" "rigwRN.phl[485]";
connectAttr "rigw:mesh:robo96_scaleX.o" "rigwRN.phl[486]";
connectAttr "rigw:mesh:robo96_scaleY.o" "rigwRN.phl[487]";
connectAttr "rigw:mesh:robo96_scaleZ.o" "rigwRN.phl[488]";
connectAttr "rigw:mesh:robo95_translateX.o" "rigwRN.phl[489]";
connectAttr "rigw:mesh:robo95_translateY.o" "rigwRN.phl[490]";
connectAttr "rigw:mesh:robo95_translateZ.o" "rigwRN.phl[491]";
connectAttr "rigw:mesh:robo95_visibility.o" "rigwRN.phl[492]";
connectAttr "rigw:mesh:robo95_rotateX.o" "rigwRN.phl[493]";
connectAttr "rigw:mesh:robo95_rotateY.o" "rigwRN.phl[494]";
connectAttr "rigw:mesh:robo95_rotateZ.o" "rigwRN.phl[495]";
connectAttr "rigw:mesh:robo95_scaleX.o" "rigwRN.phl[496]";
connectAttr "rigw:mesh:robo95_scaleY.o" "rigwRN.phl[497]";
connectAttr "rigw:mesh:robo95_scaleZ.o" "rigwRN.phl[498]";
connectAttr "rigw:mesh:robo94_translateX.o" "rigwRN.phl[499]";
connectAttr "rigw:mesh:robo94_translateY.o" "rigwRN.phl[500]";
connectAttr "rigw:mesh:robo94_translateZ.o" "rigwRN.phl[501]";
connectAttr "rigw:mesh:robo94_visibility.o" "rigwRN.phl[502]";
connectAttr "rigw:mesh:robo94_rotateX.o" "rigwRN.phl[503]";
connectAttr "rigw:mesh:robo94_rotateY.o" "rigwRN.phl[504]";
connectAttr "rigw:mesh:robo94_rotateZ.o" "rigwRN.phl[505]";
connectAttr "rigw:mesh:robo94_scaleX.o" "rigwRN.phl[506]";
connectAttr "rigw:mesh:robo94_scaleY.o" "rigwRN.phl[507]";
connectAttr "rigw:mesh:robo94_scaleZ.o" "rigwRN.phl[508]";
connectAttr "rigw:mesh:robo93_translateX.o" "rigwRN.phl[509]";
connectAttr "rigw:mesh:robo93_translateY.o" "rigwRN.phl[510]";
connectAttr "rigw:mesh:robo93_translateZ.o" "rigwRN.phl[511]";
connectAttr "rigw:mesh:robo93_visibility.o" "rigwRN.phl[512]";
connectAttr "rigw:mesh:robo93_rotateX.o" "rigwRN.phl[513]";
connectAttr "rigw:mesh:robo93_rotateY.o" "rigwRN.phl[514]";
connectAttr "rigw:mesh:robo93_rotateZ.o" "rigwRN.phl[515]";
connectAttr "rigw:mesh:robo93_scaleX.o" "rigwRN.phl[516]";
connectAttr "rigw:mesh:robo93_scaleY.o" "rigwRN.phl[517]";
connectAttr "rigw:mesh:robo93_scaleZ.o" "rigwRN.phl[518]";
connectAttr "rigw:mesh:robo92_translateX.o" "rigwRN.phl[519]";
connectAttr "rigw:mesh:robo92_translateY.o" "rigwRN.phl[520]";
connectAttr "rigw:mesh:robo92_translateZ.o" "rigwRN.phl[521]";
connectAttr "rigw:mesh:robo92_visibility.o" "rigwRN.phl[522]";
connectAttr "rigw:mesh:robo92_rotateX.o" "rigwRN.phl[523]";
connectAttr "rigw:mesh:robo92_rotateY.o" "rigwRN.phl[524]";
connectAttr "rigw:mesh:robo92_rotateZ.o" "rigwRN.phl[525]";
connectAttr "rigw:mesh:robo92_scaleX.o" "rigwRN.phl[526]";
connectAttr "rigw:mesh:robo92_scaleY.o" "rigwRN.phl[527]";
connectAttr "rigw:mesh:robo92_scaleZ.o" "rigwRN.phl[528]";
connectAttr "rigw:mesh:robo91_translateX.o" "rigwRN.phl[529]";
connectAttr "rigw:mesh:robo91_translateY.o" "rigwRN.phl[530]";
connectAttr "rigw:mesh:robo91_translateZ.o" "rigwRN.phl[531]";
connectAttr "rigw:mesh:robo91_visibility.o" "rigwRN.phl[532]";
connectAttr "rigw:mesh:robo91_rotateX.o" "rigwRN.phl[533]";
connectAttr "rigw:mesh:robo91_rotateY.o" "rigwRN.phl[534]";
connectAttr "rigw:mesh:robo91_rotateZ.o" "rigwRN.phl[535]";
connectAttr "rigw:mesh:robo91_scaleX.o" "rigwRN.phl[536]";
connectAttr "rigw:mesh:robo91_scaleY.o" "rigwRN.phl[537]";
connectAttr "rigw:mesh:robo91_scaleZ.o" "rigwRN.phl[538]";
connectAttr "rigw:mesh:robo90_translateX.o" "rigwRN.phl[539]";
connectAttr "rigw:mesh:robo90_translateY.o" "rigwRN.phl[540]";
connectAttr "rigw:mesh:robo90_translateZ.o" "rigwRN.phl[541]";
connectAttr "rigw:mesh:robo90_visibility.o" "rigwRN.phl[542]";
connectAttr "rigw:mesh:robo90_rotateX.o" "rigwRN.phl[543]";
connectAttr "rigw:mesh:robo90_rotateY.o" "rigwRN.phl[544]";
connectAttr "rigw:mesh:robo90_rotateZ.o" "rigwRN.phl[545]";
connectAttr "rigw:mesh:robo90_scaleX.o" "rigwRN.phl[546]";
connectAttr "rigw:mesh:robo90_scaleY.o" "rigwRN.phl[547]";
connectAttr "rigw:mesh:robo90_scaleZ.o" "rigwRN.phl[548]";
connectAttr "rigw:mesh:robo89_translateX.o" "rigwRN.phl[549]";
connectAttr "rigw:mesh:robo89_translateY.o" "rigwRN.phl[550]";
connectAttr "rigw:mesh:robo89_translateZ.o" "rigwRN.phl[551]";
connectAttr "rigw:mesh:robo89_rotateX.o" "rigwRN.phl[552]";
connectAttr "rigw:mesh:robo89_rotateY.o" "rigwRN.phl[553]";
connectAttr "rigw:mesh:robo89_rotateZ.o" "rigwRN.phl[554]";
connectAttr "rigw:mesh:robo89_visibility.o" "rigwRN.phl[555]";
connectAttr "rigw:mesh:robo89_scaleX.o" "rigwRN.phl[556]";
connectAttr "rigw:mesh:robo89_scaleY.o" "rigwRN.phl[557]";
connectAttr "rigw:mesh:robo89_scaleZ.o" "rigwRN.phl[558]";
connectAttr "rigw:mesh:robo88_translateX.o" "rigwRN.phl[559]";
connectAttr "rigw:mesh:robo88_translateY.o" "rigwRN.phl[560]";
connectAttr "rigw:mesh:robo88_translateZ.o" "rigwRN.phl[561]";
connectAttr "rigw:mesh:robo88_rotateX.o" "rigwRN.phl[562]";
connectAttr "rigw:mesh:robo88_rotateY.o" "rigwRN.phl[563]";
connectAttr "rigw:mesh:robo88_rotateZ.o" "rigwRN.phl[564]";
connectAttr "rigw:mesh:robo88_visibility.o" "rigwRN.phl[565]";
connectAttr "rigw:mesh:robo88_scaleX.o" "rigwRN.phl[566]";
connectAttr "rigw:mesh:robo88_scaleY.o" "rigwRN.phl[567]";
connectAttr "rigw:mesh:robo88_scaleZ.o" "rigwRN.phl[568]";
connectAttr "rigw:mesh:robo87_translateX.o" "rigwRN.phl[569]";
connectAttr "rigw:mesh:robo87_translateY.o" "rigwRN.phl[570]";
connectAttr "rigw:mesh:robo87_translateZ.o" "rigwRN.phl[571]";
connectAttr "rigw:mesh:robo87_rotateX.o" "rigwRN.phl[572]";
connectAttr "rigw:mesh:robo87_rotateY.o" "rigwRN.phl[573]";
connectAttr "rigw:mesh:robo87_rotateZ.o" "rigwRN.phl[574]";
connectAttr "rigw:mesh:robo87_visibility.o" "rigwRN.phl[575]";
connectAttr "rigw:mesh:robo87_scaleX.o" "rigwRN.phl[576]";
connectAttr "rigw:mesh:robo87_scaleY.o" "rigwRN.phl[577]";
connectAttr "rigw:mesh:robo87_scaleZ.o" "rigwRN.phl[578]";
connectAttr "rigw:mesh:robo86_translateX.o" "rigwRN.phl[579]";
connectAttr "rigw:mesh:robo86_translateY.o" "rigwRN.phl[580]";
connectAttr "rigw:mesh:robo86_translateZ.o" "rigwRN.phl[581]";
connectAttr "rigw:mesh:robo86_visibility.o" "rigwRN.phl[582]";
connectAttr "rigw:mesh:robo86_rotateX.o" "rigwRN.phl[583]";
connectAttr "rigw:mesh:robo86_rotateY.o" "rigwRN.phl[584]";
connectAttr "rigw:mesh:robo86_rotateZ.o" "rigwRN.phl[585]";
connectAttr "rigw:mesh:robo86_scaleX.o" "rigwRN.phl[586]";
connectAttr "rigw:mesh:robo86_scaleY.o" "rigwRN.phl[587]";
connectAttr "rigw:mesh:robo86_scaleZ.o" "rigwRN.phl[588]";
connectAttr "rigw:mesh:robo107_translateX.o" "rigwRN.phl[589]";
connectAttr "rigw:mesh:robo107_translateY.o" "rigwRN.phl[590]";
connectAttr "rigw:mesh:robo107_translateZ.o" "rigwRN.phl[591]";
connectAttr "rigw:mesh:robo107_visibility.o" "rigwRN.phl[592]";
connectAttr "rigw:mesh:robo107_rotateX.o" "rigwRN.phl[593]";
connectAttr "rigw:mesh:robo107_rotateY.o" "rigwRN.phl[594]";
connectAttr "rigw:mesh:robo107_rotateZ.o" "rigwRN.phl[595]";
connectAttr "rigw:mesh:robo107_scaleX.o" "rigwRN.phl[596]";
connectAttr "rigw:mesh:robo107_scaleY.o" "rigwRN.phl[597]";
connectAttr "rigw:mesh:robo107_scaleZ.o" "rigwRN.phl[598]";
connectAttr "rigw:mesh:robo108_translateX.o" "rigwRN.phl[599]";
connectAttr "rigw:mesh:robo108_translateY.o" "rigwRN.phl[600]";
connectAttr "rigw:mesh:robo108_translateZ.o" "rigwRN.phl[601]";
connectAttr "rigw:mesh:robo108_visibility.o" "rigwRN.phl[602]";
connectAttr "rigw:mesh:robo108_rotateX.o" "rigwRN.phl[603]";
connectAttr "rigw:mesh:robo108_rotateY.o" "rigwRN.phl[604]";
connectAttr "rigw:mesh:robo108_rotateZ.o" "rigwRN.phl[605]";
connectAttr "rigw:mesh:robo108_scaleX.o" "rigwRN.phl[606]";
connectAttr "rigw:mesh:robo108_scaleY.o" "rigwRN.phl[607]";
connectAttr "rigw:mesh:robo108_scaleZ.o" "rigwRN.phl[608]";
connectAttr "rigw:mesh:robo109_translateX.o" "rigwRN.phl[609]";
connectAttr "rigw:mesh:robo109_translateY.o" "rigwRN.phl[610]";
connectAttr "rigw:mesh:robo109_translateZ.o" "rigwRN.phl[611]";
connectAttr "rigw:mesh:robo109_visibility.o" "rigwRN.phl[612]";
connectAttr "rigw:mesh:robo109_rotateX.o" "rigwRN.phl[613]";
connectAttr "rigw:mesh:robo109_rotateY.o" "rigwRN.phl[614]";
connectAttr "rigw:mesh:robo109_rotateZ.o" "rigwRN.phl[615]";
connectAttr "rigw:mesh:robo109_scaleX.o" "rigwRN.phl[616]";
connectAttr "rigw:mesh:robo109_scaleY.o" "rigwRN.phl[617]";
connectAttr "rigw:mesh:robo109_scaleZ.o" "rigwRN.phl[618]";
connectAttr "rigw:mesh:robo110_translateX.o" "rigwRN.phl[619]";
connectAttr "rigw:mesh:robo110_translateY.o" "rigwRN.phl[620]";
connectAttr "rigw:mesh:robo110_translateZ.o" "rigwRN.phl[621]";
connectAttr "rigw:mesh:robo110_visibility.o" "rigwRN.phl[622]";
connectAttr "rigw:mesh:robo110_rotateX.o" "rigwRN.phl[623]";
connectAttr "rigw:mesh:robo110_rotateY.o" "rigwRN.phl[624]";
connectAttr "rigw:mesh:robo110_rotateZ.o" "rigwRN.phl[625]";
connectAttr "rigw:mesh:robo110_scaleX.o" "rigwRN.phl[626]";
connectAttr "rigw:mesh:robo110_scaleY.o" "rigwRN.phl[627]";
connectAttr "rigw:mesh:robo110_scaleZ.o" "rigwRN.phl[628]";
connectAttr "rigw:mesh:robo111_translateX.o" "rigwRN.phl[629]";
connectAttr "rigw:mesh:robo111_translateY.o" "rigwRN.phl[630]";
connectAttr "rigw:mesh:robo111_translateZ.o" "rigwRN.phl[631]";
connectAttr "rigw:mesh:robo111_visibility.o" "rigwRN.phl[632]";
connectAttr "rigw:mesh:robo111_rotateX.o" "rigwRN.phl[633]";
connectAttr "rigw:mesh:robo111_rotateY.o" "rigwRN.phl[634]";
connectAttr "rigw:mesh:robo111_rotateZ.o" "rigwRN.phl[635]";
connectAttr "rigw:mesh:robo111_scaleX.o" "rigwRN.phl[636]";
connectAttr "rigw:mesh:robo111_scaleY.o" "rigwRN.phl[637]";
connectAttr "rigw:mesh:robo111_scaleZ.o" "rigwRN.phl[638]";
connectAttr "rigw:mesh:robo104_translateX.o" "rigwRN.phl[639]";
connectAttr "rigw:mesh:robo104_translateY.o" "rigwRN.phl[640]";
connectAttr "rigw:mesh:robo104_translateZ.o" "rigwRN.phl[641]";
connectAttr "rigw:mesh:robo104_rotateX.o" "rigwRN.phl[642]";
connectAttr "rigw:mesh:robo104_rotateY.o" "rigwRN.phl[643]";
connectAttr "rigw:mesh:robo104_rotateZ.o" "rigwRN.phl[644]";
connectAttr "rigw:mesh:robo104_visibility.o" "rigwRN.phl[645]";
connectAttr "rigw:mesh:robo104_scaleX.o" "rigwRN.phl[646]";
connectAttr "rigw:mesh:robo104_scaleY.o" "rigwRN.phl[647]";
connectAttr "rigw:mesh:robo104_scaleZ.o" "rigwRN.phl[648]";
connectAttr "rigw:mesh:robo103_translateX.o" "rigwRN.phl[649]";
connectAttr "rigw:mesh:robo103_translateY.o" "rigwRN.phl[650]";
connectAttr "rigw:mesh:robo103_translateZ.o" "rigwRN.phl[651]";
connectAttr "rigw:mesh:robo103_visibility.o" "rigwRN.phl[652]";
connectAttr "rigw:mesh:robo103_rotateX.o" "rigwRN.phl[653]";
connectAttr "rigw:mesh:robo103_rotateY.o" "rigwRN.phl[654]";
connectAttr "rigw:mesh:robo103_rotateZ.o" "rigwRN.phl[655]";
connectAttr "rigw:mesh:robo103_scaleX.o" "rigwRN.phl[656]";
connectAttr "rigw:mesh:robo103_scaleY.o" "rigwRN.phl[657]";
connectAttr "rigw:mesh:robo103_scaleZ.o" "rigwRN.phl[658]";
connectAttr "rigw:rig:Main_translateX.o" "rigwRN.phl[1]";
connectAttr "rigw:rig:Main_translateY.o" "rigwRN.phl[2]";
connectAttr "rigw:rig:Main_translateZ.o" "rigwRN.phl[3]";
connectAttr "rigw:rig:Main_visibility.o" "rigwRN.phl[4]";
connectAttr "rigw:rig:Main_rotateX.o" "rigwRN.phl[5]";
connectAttr "rigw:rig:Main_rotateY.o" "rigwRN.phl[6]";
connectAttr "rigw:rig:Main_rotateZ.o" "rigwRN.phl[7]";
connectAttr "rigw:rig:Main_scaleX.o" "rigwRN.phl[8]";
connectAttr "rigw:rig:Main_scaleY.o" "rigwRN.phl[9]";
connectAttr "rigw:rig:Main_scaleZ.o" "rigwRN.phl[10]";
connectAttr "rigw:rig:FKWheel1_R_rotateX.o" "rigwRN.phl[11]";
connectAttr "rigw:rig:FKWheel1_R_rotateY.o" "rigwRN.phl[12]";
connectAttr "rigw:rig:FKWheel1_R_rotateZ.o" "rigwRN.phl[13]";
connectAttr "rigw:rig:FKWheel2_R_rotateX.o" "rigwRN.phl[14]";
connectAttr "rigw:rig:FKWheel2_R_rotateY.o" "rigwRN.phl[15]";
connectAttr "rigw:rig:FKWheel2_R_rotateZ.o" "rigwRN.phl[16]";
connectAttr "rigw:rig:FKWheel3_R_rotateX.o" "rigwRN.phl[17]";
connectAttr "rigw:rig:FKWheel3_R_rotateY.o" "rigwRN.phl[18]";
connectAttr "rigw:rig:FKWheel3_R_rotateZ.o" "rigwRN.phl[19]";
connectAttr "rigw:rig:FKWheel4_R_rotateX.o" "rigwRN.phl[20]";
connectAttr "rigw:rig:FKWheel4_R_rotateY.o" "rigwRN.phl[21]";
connectAttr "rigw:rig:FKWheel4_R_rotateZ.o" "rigwRN.phl[22]";
connectAttr "rigw:rig:FKWheel1_L_rotateX.o" "rigwRN.phl[23]";
connectAttr "rigw:rig:FKWheel1_L_rotateY.o" "rigwRN.phl[24]";
connectAttr "rigw:rig:FKWheel1_L_rotateZ.o" "rigwRN.phl[25]";
connectAttr "rigw:rig:FKWheel2_L_rotateX.o" "rigwRN.phl[26]";
connectAttr "rigw:rig:FKWheel2_L_rotateY.o" "rigwRN.phl[27]";
connectAttr "rigw:rig:FKWheel2_L_rotateZ.o" "rigwRN.phl[28]";
connectAttr "rigw:rig:FKWheel3_L_rotateX.o" "rigwRN.phl[29]";
connectAttr "rigw:rig:FKWheel3_L_rotateY.o" "rigwRN.phl[30]";
connectAttr "rigw:rig:FKWheel3_L_rotateZ.o" "rigwRN.phl[31]";
connectAttr "rigw:rig:FKWheel4_L_rotateX.o" "rigwRN.phl[32]";
connectAttr "rigw:rig:FKWheel4_L_rotateY.o" "rigwRN.phl[33]";
connectAttr "rigw:rig:FKWheel4_L_rotateZ.o" "rigwRN.phl[34]";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[35]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[36]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[37]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[38]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[39]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[40]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[41]";
connectAttr "rigw:rig:FKBody_M_translateX.o" "rigwRN.phl[42]";
connectAttr "rigw:rig:FKBody_M_translateY.o" "rigwRN.phl[43]";
connectAttr "rigw:rig:FKBody_M_translateZ.o" "rigwRN.phl[44]";
connectAttr "rigw:rig:FKBody_M_rotateX.o" "rigwRN.phl[45]";
connectAttr "rigw:rig:FKBody_M_rotateY.o" "rigwRN.phl[46]";
connectAttr "rigw:rig:FKBody_M_rotateZ.o" "rigwRN.phl[47]";
connectAttr "rigw:rig:FKHead_M_translateX.o" "rigwRN.phl[48]";
connectAttr "rigw:rig:FKHead_M_translateY.o" "rigwRN.phl[49]";
connectAttr "rigw:rig:FKHead_M_translateZ.o" "rigwRN.phl[50]";
connectAttr "rigw:rig:FKHead_M_rotateX.o" "rigwRN.phl[51]";
connectAttr "rigw:rig:FKHead_M_rotateY.o" "rigwRN.phl[52]";
connectAttr "rigw:rig:FKHead_M_rotateZ.o" "rigwRN.phl[53]";
connectAttr "rigw:rig:FKShoulder_R_Global.o" "rigwRN.phl[54]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[55]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[56]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[57]";
connectAttr "rigw:rig:FKElbow5_R_rotateX.o" "rigwRN.phl[58]";
connectAttr "rigw:rig:FKElbow5_R_rotateY.o" "rigwRN.phl[59]";
connectAttr "rigw:rig:FKElbow5_R_rotateZ.o" "rigwRN.phl[60]";
connectAttr "rigw:rig:FKWrist1_R_translateX.o" "rigwRN.phl[61]";
connectAttr "rigw:rig:FKWrist1_R_translateY.o" "rigwRN.phl[62]";
connectAttr "rigw:rig:FKWrist1_R_translateZ.o" "rigwRN.phl[63]";
connectAttr "rigw:rig:FKWrist1_R_rotateX.o" "rigwRN.phl[64]";
connectAttr "rigw:rig:FKWrist1_R_rotateY.o" "rigwRN.phl[65]";
connectAttr "rigw:rig:FKWrist1_R_rotateZ.o" "rigwRN.phl[66]";
connectAttr "rigw:rig:FKShoulder1_L_Global.o" "rigwRN.phl[67]";
connectAttr "rigw:rig:FKShoulder1_L_rotateX.o" "rigwRN.phl[68]";
connectAttr "rigw:rig:FKShoulder1_L_rotateY.o" "rigwRN.phl[69]";
connectAttr "rigw:rig:FKShoulder1_L_rotateZ.o" "rigwRN.phl[70]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[71]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[72]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[73]";
connectAttr "rigw:rig:FKWrist_L_rotateX.o" "rigwRN.phl[74]";
connectAttr "rigw:rig:FKWrist_L_rotateY.o" "rigwRN.phl[75]";
connectAttr "rigw:rig:FKWrist_L_rotateZ.o" "rigwRN.phl[76]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateX.o" "rigwRN.phl[77]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateY.o" "rigwRN.phl[78]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateZ.o" "rigwRN.phl[79]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateX.o" "rigwRN.phl[80]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateY.o" "rigwRN.phl[81]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateZ.o" "rigwRN.phl[82]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateX.o" "rigwRN.phl[83]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateY.o" "rigwRN.phl[84]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateZ.o" "rigwRN.phl[85]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateX.o" "rigwRN.phl[86]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateY.o" "rigwRN.phl[87]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateZ.o" "rigwRN.phl[88]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateX.o" "rigwRN.phl[89]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateY.o" "rigwRN.phl[90]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateZ.o" "rigwRN.phl[91]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateX.o" "rigwRN.phl[92]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateY.o" "rigwRN.phl[93]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateZ.o" "rigwRN.phl[94]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateX.o" "rigwRN.phl[95]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateY.o" "rigwRN.phl[96]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateZ.o" "rigwRN.phl[97]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateX.o" "rigwRN.phl[98]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateY.o" "rigwRN.phl[99]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateZ.o" "rigwRN.phl[100]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateX.o" "rigwRN.phl[101]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateY.o" "rigwRN.phl[102]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateZ.o" "rigwRN.phl[103]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateX.o" "rigwRN.phl[104]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateY.o" "rigwRN.phl[105]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateZ.o" "rigwRN.phl[106]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateX.o" "rigwRN.phl[107]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateY.o" "rigwRN.phl[108]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateZ.o" "rigwRN.phl[109]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateX.o" "rigwRN.phl[110]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateY.o" "rigwRN.phl[111]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateZ.o" "rigwRN.phl[112]";
connectAttr "rigw:rig:FKHose1_R_translateX.o" "rigwRN.phl[113]";
connectAttr "rigw:rig:FKHose1_R_translateY.o" "rigwRN.phl[114]";
connectAttr "rigw:rig:FKHose1_R_translateZ.o" "rigwRN.phl[115]";
connectAttr "rigw:rig:FKHose1_R_rotateX.o" "rigwRN.phl[116]";
connectAttr "rigw:rig:FKHose1_R_rotateY.o" "rigwRN.phl[117]";
connectAttr "rigw:rig:FKHose1_R_rotateZ.o" "rigwRN.phl[118]";
connectAttr "rigw:rig:FKHose2_R_translateX.o" "rigwRN.phl[119]";
connectAttr "rigw:rig:FKHose2_R_translateY.o" "rigwRN.phl[120]";
connectAttr "rigw:rig:FKHose2_R_translateZ.o" "rigwRN.phl[121]";
connectAttr "rigw:rig:FKHose2_R_rotateX.o" "rigwRN.phl[122]";
connectAttr "rigw:rig:FKHose2_R_rotateY.o" "rigwRN.phl[123]";
connectAttr "rigw:rig:FKHose2_R_rotateZ.o" "rigwRN.phl[124]";
connectAttr "rigw:rig:FKHose1_L_rotateX.o" "rigwRN.phl[125]";
connectAttr "rigw:rig:FKHose1_L_rotateY.o" "rigwRN.phl[126]";
connectAttr "rigw:rig:FKHose1_L_rotateZ.o" "rigwRN.phl[127]";
connectAttr "rigw:rig:FKHose2_L_rotateX.o" "rigwRN.phl[128]";
connectAttr "rigw:rig:FKHose2_L_rotateY.o" "rigwRN.phl[129]";
connectAttr "rigw:rig:FKHose2_L_rotateZ.o" "rigwRN.phl[130]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[131]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[132]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[133]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[134]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[135]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[136]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[137]";
connectAttr "rigw:rig:IKLeg_R_rollAngle.o" "rigwRN.phl[138]";
connectAttr "rigw:rig:IKLeg_R_roll.o" "rigwRN.phl[139]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[140]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[141]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[142]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[143]";
connectAttr "rigw:rig:IKLegHeel_R_rotateX.o" "rigwRN.phl[144]";
connectAttr "rigw:rig:IKLegHeel_R_rotateY.o" "rigwRN.phl[145]";
connectAttr "rigw:rig:IKLegHeel_R_rotateZ.o" "rigwRN.phl[146]";
connectAttr "rigw:rig:IKLegBall_R_rotateX.o" "rigwRN.phl[147]";
connectAttr "rigw:rig:PoleLeg_R_translateX.o" "rigwRN.phl[148]";
connectAttr "rigw:rig:PoleLeg_R_translateY.o" "rigwRN.phl[149]";
connectAttr "rigw:rig:PoleLeg_R_translateZ.o" "rigwRN.phl[150]";
connectAttr "rigw:rig:PoleLeg_R_follow.o" "rigwRN.phl[151]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[152]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[153]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[154]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[155]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[156]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[157]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[158]";
connectAttr "rigw:rig:IKLeg_L_rollAngle.o" "rigwRN.phl[159]";
connectAttr "rigw:rig:IKLeg_L_roll.o" "rigwRN.phl[160]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[161]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[162]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[163]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[164]";
connectAttr "rigw:rig:IKLegHeel_L_rotateX.o" "rigwRN.phl[165]";
connectAttr "rigw:rig:IKLegHeel_L_rotateY.o" "rigwRN.phl[166]";
connectAttr "rigw:rig:IKLegHeel_L_rotateZ.o" "rigwRN.phl[167]";
connectAttr "rigw:rig:IKLegBall_L_rotateX.o" "rigwRN.phl[168]";
connectAttr "rigw:rig:PoleLeg_L_translateX.o" "rigwRN.phl[169]";
connectAttr "rigw:rig:PoleLeg_L_translateY.o" "rigwRN.phl[170]";
connectAttr "rigw:rig:PoleLeg_L_translateZ.o" "rigwRN.phl[171]";
connectAttr "rigw:rig:PoleLeg_L_follow.o" "rigwRN.phl[172]";
connectAttr "rigw:rig:FKIKLeg_R_FKIKBlend.o" "rigwRN.phl[173]";
connectAttr "rigw:rig:FKIKLeg_R_IKVis.o" "rigwRN.phl[174]";
connectAttr "rigw:rig:FKIKLeg_R_FKVis.o" "rigwRN.phl[175]";
connectAttr "rigw:rig:FKIKLeg_L_FKIKBlend.o" "rigwRN.phl[176]";
connectAttr "rigw:rig:FKIKLeg_L_IKVis.o" "rigwRN.phl[177]";
connectAttr "rigw:rig:FKIKLeg_L_FKVis.o" "rigwRN.phl[178]";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Medic__mc-rigw@Med_hitreact.ma
