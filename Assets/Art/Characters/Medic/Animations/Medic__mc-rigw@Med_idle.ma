//Maya ASCII 2013 scene
//Name: Medic__mc-rigw@Med_idle.ma
//Last modified: Thu, Oct 30, 2014 03:18:55 PM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Medic/Medic__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Medic__rig.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Medic/Medic__mc-rigw.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.4";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 18.555842875958163 37.429175080776275 59.204979143931553 ;
	setAttr ".r" -type "double3" -23.138352729603753 17.799999999992203 4.1755809474374078e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 68.490929517366823;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 3.5036784387165003 3.2819499165629367 1.5150991661172242 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" -1.1813132708813237 6.1690804146016589 500 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".ow" 222.59563544443228;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 16 ".lnk";
	setAttr -s 16 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 666 ".phl";
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".phl[440]" 0;
	setAttr ".phl[441]" 0;
	setAttr ".phl[442]" 0;
	setAttr ".phl[443]" 0;
	setAttr ".phl[444]" 0;
	setAttr ".phl[445]" 0;
	setAttr ".phl[446]" 0;
	setAttr ".phl[447]" 0;
	setAttr ".phl[448]" 0;
	setAttr ".phl[449]" 0;
	setAttr ".phl[450]" 0;
	setAttr ".phl[451]" 0;
	setAttr ".phl[452]" 0;
	setAttr ".phl[453]" 0;
	setAttr ".phl[454]" 0;
	setAttr ".phl[455]" 0;
	setAttr ".phl[456]" 0;
	setAttr ".phl[457]" 0;
	setAttr ".phl[458]" 0;
	setAttr ".phl[459]" 0;
	setAttr ".phl[460]" 0;
	setAttr ".phl[461]" 0;
	setAttr ".phl[462]" 0;
	setAttr ".phl[463]" 0;
	setAttr ".phl[464]" 0;
	setAttr ".phl[465]" 0;
	setAttr ".phl[466]" 0;
	setAttr ".phl[467]" 0;
	setAttr ".phl[468]" 0;
	setAttr ".phl[469]" 0;
	setAttr ".phl[470]" 0;
	setAttr ".phl[471]" 0;
	setAttr ".phl[472]" 0;
	setAttr ".phl[473]" 0;
	setAttr ".phl[474]" 0;
	setAttr ".phl[475]" 0;
	setAttr ".phl[476]" 0;
	setAttr ".phl[477]" 0;
	setAttr ".phl[478]" 0;
	setAttr ".phl[479]" 0;
	setAttr ".phl[480]" 0;
	setAttr ".phl[481]" 0;
	setAttr ".phl[482]" 0;
	setAttr ".phl[483]" 0;
	setAttr ".phl[484]" 0;
	setAttr ".phl[485]" 0;
	setAttr ".phl[486]" 0;
	setAttr ".phl[487]" 0;
	setAttr ".phl[488]" 0;
	setAttr ".phl[489]" 0;
	setAttr ".phl[490]" 0;
	setAttr ".phl[491]" 0;
	setAttr ".phl[492]" 0;
	setAttr ".phl[493]" 0;
	setAttr ".phl[494]" 0;
	setAttr ".phl[495]" 0;
	setAttr ".phl[496]" 0;
	setAttr ".phl[497]" 0;
	setAttr ".phl[498]" 0;
	setAttr ".phl[499]" 0;
	setAttr ".phl[500]" 0;
	setAttr ".phl[501]" 0;
	setAttr ".phl[502]" 0;
	setAttr ".phl[503]" 0;
	setAttr ".phl[504]" 0;
	setAttr ".phl[505]" 0;
	setAttr ".phl[506]" 0;
	setAttr ".phl[507]" 0;
	setAttr ".phl[508]" 0;
	setAttr ".phl[509]" 0;
	setAttr ".phl[510]" 0;
	setAttr ".phl[511]" 0;
	setAttr ".phl[512]" 0;
	setAttr ".phl[513]" 0;
	setAttr ".phl[514]" 0;
	setAttr ".phl[515]" 0;
	setAttr ".phl[516]" 0;
	setAttr ".phl[517]" 0;
	setAttr ".phl[518]" 0;
	setAttr ".phl[519]" 0;
	setAttr ".phl[520]" 0;
	setAttr ".phl[521]" 0;
	setAttr ".phl[522]" 0;
	setAttr ".phl[523]" 0;
	setAttr ".phl[524]" 0;
	setAttr ".phl[525]" 0;
	setAttr ".phl[526]" 0;
	setAttr ".phl[527]" 0;
	setAttr ".phl[528]" 0;
	setAttr ".phl[529]" 0;
	setAttr ".phl[530]" 0;
	setAttr ".phl[531]" 0;
	setAttr ".phl[532]" 0;
	setAttr ".phl[533]" 0;
	setAttr ".phl[534]" 0;
	setAttr ".phl[535]" 0;
	setAttr ".phl[536]" 0;
	setAttr ".phl[537]" 0;
	setAttr ".phl[538]" 0;
	setAttr ".phl[539]" 0;
	setAttr ".phl[540]" 0;
	setAttr ".phl[541]" 0;
	setAttr ".phl[542]" 0;
	setAttr ".phl[543]" 0;
	setAttr ".phl[544]" 0;
	setAttr ".phl[545]" 0;
	setAttr ".phl[546]" 0;
	setAttr ".phl[547]" 0;
	setAttr ".phl[548]" 0;
	setAttr ".phl[549]" 0;
	setAttr ".phl[550]" 0;
	setAttr ".phl[551]" 0;
	setAttr ".phl[552]" 0;
	setAttr ".phl[553]" 0;
	setAttr ".phl[554]" 0;
	setAttr ".phl[555]" 0;
	setAttr ".phl[556]" 0;
	setAttr ".phl[557]" 0;
	setAttr ".phl[558]" 0;
	setAttr ".phl[559]" 0;
	setAttr ".phl[560]" 0;
	setAttr ".phl[561]" 0;
	setAttr ".phl[562]" 0;
	setAttr ".phl[563]" 0;
	setAttr ".phl[564]" 0;
	setAttr ".phl[565]" 0;
	setAttr ".phl[566]" 0;
	setAttr ".phl[567]" 0;
	setAttr ".phl[568]" 0;
	setAttr ".phl[569]" 0;
	setAttr ".phl[570]" 0;
	setAttr ".phl[571]" 0;
	setAttr ".phl[572]" 0;
	setAttr ".phl[573]" 0;
	setAttr ".phl[574]" 0;
	setAttr ".phl[575]" 0;
	setAttr ".phl[576]" 0;
	setAttr ".phl[577]" 0;
	setAttr ".phl[578]" 0;
	setAttr ".phl[579]" 0;
	setAttr ".phl[580]" 0;
	setAttr ".phl[581]" 0;
	setAttr ".phl[582]" 0;
	setAttr ".phl[583]" 0;
	setAttr ".phl[584]" 0;
	setAttr ".phl[585]" 0;
	setAttr ".phl[586]" 0;
	setAttr ".phl[587]" 0;
	setAttr ".phl[588]" 0;
	setAttr ".phl[589]" 0;
	setAttr ".phl[590]" 0;
	setAttr ".phl[591]" 0;
	setAttr ".phl[592]" 0;
	setAttr ".phl[593]" 0;
	setAttr ".phl[594]" 0;
	setAttr ".phl[595]" 0;
	setAttr ".phl[596]" 0;
	setAttr ".phl[597]" 0;
	setAttr ".phl[598]" 0;
	setAttr ".phl[599]" 0;
	setAttr ".phl[600]" 0;
	setAttr ".phl[601]" 0;
	setAttr ".phl[602]" 0;
	setAttr ".phl[603]" 0;
	setAttr ".phl[604]" 0;
	setAttr ".phl[605]" 0;
	setAttr ".phl[606]" 0;
	setAttr ".phl[607]" 0;
	setAttr ".phl[608]" 0;
	setAttr ".phl[609]" 0;
	setAttr ".phl[610]" 0;
	setAttr ".phl[611]" 0;
	setAttr ".phl[612]" 0;
	setAttr ".phl[613]" 0;
	setAttr ".phl[614]" 0;
	setAttr ".phl[615]" 0;
	setAttr ".phl[616]" 0;
	setAttr ".phl[617]" 0;
	setAttr ".phl[618]" 0;
	setAttr ".phl[619]" 0;
	setAttr ".phl[620]" 0;
	setAttr ".phl[621]" 0;
	setAttr ".phl[622]" 0;
	setAttr ".phl[623]" 0;
	setAttr ".phl[624]" 0;
	setAttr ".phl[625]" 0;
	setAttr ".phl[626]" 0;
	setAttr ".phl[627]" 0;
	setAttr ".phl[628]" 0;
	setAttr ".phl[629]" 0;
	setAttr ".phl[630]" 0;
	setAttr ".phl[631]" 0;
	setAttr ".phl[632]" 0;
	setAttr ".phl[633]" 0;
	setAttr ".phl[634]" 0;
	setAttr ".phl[635]" 0;
	setAttr ".phl[636]" 0;
	setAttr ".phl[637]" 0;
	setAttr ".phl[638]" 0;
	setAttr ".phl[639]" 0;
	setAttr ".phl[640]" 0;
	setAttr ".phl[641]" 0;
	setAttr ".phl[642]" 0;
	setAttr ".phl[643]" 0;
	setAttr ".phl[644]" 0;
	setAttr ".phl[645]" 0;
	setAttr ".phl[646]" 0;
	setAttr ".phl[647]" 0;
	setAttr ".phl[648]" 0;
	setAttr ".phl[649]" 0;
	setAttr ".phl[650]" 0;
	setAttr ".phl[651]" 0;
	setAttr ".phl[652]" 0;
	setAttr ".phl[653]" 0;
	setAttr ".phl[654]" 0;
	setAttr ".phl[655]" 0;
	setAttr ".phl[656]" 0;
	setAttr ".phl[657]" 0;
	setAttr ".phl[658]" 0;
	setAttr ".phl[659]" 0;
	setAttr ".phl[660]" 0;
	setAttr ".phl[661]" 0;
	setAttr ".phl[662]" 0;
	setAttr ".phl[663]" 0;
	setAttr ".phl[664]" 0;
	setAttr ".phl[665]" 0;
	setAttr ".phl[666]" 0;
	setAttr ".phl[667]" 0;
	setAttr ".phl[668]" 0;
	setAttr ".phl[669]" 0;
	setAttr ".phl[670]" 0;
	setAttr ".phl[671]" 0;
	setAttr ".phl[672]" 0;
	setAttr ".phl[673]" 0;
	setAttr ".phl[674]" 0;
	setAttr ".phl[675]" 0;
	setAttr ".phl[676]" 0;
	setAttr ".phl[677]" 0;
	setAttr ".phl[678]" 0;
	setAttr ".phl[679]" 0;
	setAttr ".phl[680]" 0;
	setAttr ".phl[681]" 0;
	setAttr ".phl[682]" 0;
	setAttr ".phl[683]" 0;
	setAttr ".phl[684]" 0;
	setAttr ".phl[685]" 0;
	setAttr ".phl[686]" 0;
	setAttr ".phl[687]" 0;
	setAttr ".phl[688]" 0;
	setAttr ".phl[689]" 0;
	setAttr ".phl[690]" 0;
	setAttr ".phl[691]" 0;
	setAttr ".phl[692]" 0;
	setAttr ".phl[693]" 0;
	setAttr ".phl[694]" 0;
	setAttr ".phl[695]" 0;
	setAttr ".phl[696]" 0;
	setAttr ".phl[697]" 0;
	setAttr ".phl[698]" 0;
	setAttr ".phl[699]" 0;
	setAttr ".phl[700]" 0;
	setAttr ".phl[701]" 0;
	setAttr ".phl[702]" 0;
	setAttr ".phl[703]" 0;
	setAttr ".phl[704]" 0;
	setAttr ".phl[705]" 0;
	setAttr ".phl[706]" 0;
	setAttr ".phl[707]" 0;
	setAttr ".phl[708]" 0;
	setAttr ".phl[709]" 0;
	setAttr ".phl[710]" 0;
	setAttr ".phl[711]" 0;
	setAttr ".phl[712]" 0;
	setAttr ".phl[713]" 0;
	setAttr ".phl[714]" 0;
	setAttr ".phl[715]" 0;
	setAttr ".phl[716]" 0;
	setAttr ".phl[717]" 0;
	setAttr ".phl[718]" 0;
	setAttr ".phl[719]" 0;
	setAttr ".phl[720]" 0;
	setAttr ".phl[721]" 0;
	setAttr ".phl[722]" 0;
	setAttr ".phl[723]" 0;
	setAttr ".phl[724]" 0;
	setAttr ".phl[725]" 0;
	setAttr ".phl[726]" 0;
	setAttr ".phl[727]" 0;
	setAttr ".phl[728]" 0;
	setAttr ".phl[729]" 0;
	setAttr ".phl[730]" 0;
	setAttr ".phl[731]" 0;
	setAttr ".phl[732]" 0;
	setAttr ".phl[733]" 0;
	setAttr ".phl[734]" 0;
	setAttr ".phl[735]" 0;
	setAttr ".phl[736]" 0;
	setAttr ".phl[737]" 0;
	setAttr ".phl[738]" 0;
	setAttr ".phl[739]" 0;
	setAttr ".phl[740]" 0;
	setAttr ".phl[741]" 0;
	setAttr ".phl[742]" 0;
	setAttr ".phl[743]" 0;
	setAttr ".phl[744]" 0;
	setAttr ".phl[745]" 0;
	setAttr ".phl[746]" 0;
	setAttr ".phl[747]" 0;
	setAttr ".phl[748]" 0;
	setAttr ".phl[749]" 0;
	setAttr ".phl[750]" 0;
	setAttr ".phl[751]" 0;
	setAttr ".phl[752]" 0;
	setAttr ".phl[753]" 0;
	setAttr ".phl[754]" 0;
	setAttr ".phl[755]" 0;
	setAttr ".phl[756]" 0;
	setAttr ".phl[757]" 0;
	setAttr ".phl[758]" 0;
	setAttr ".phl[759]" 0;
	setAttr ".phl[760]" 0;
	setAttr ".phl[761]" 0;
	setAttr ".phl[762]" 0;
	setAttr ".phl[763]" 0;
	setAttr ".phl[764]" 0;
	setAttr ".phl[765]" 0;
	setAttr ".phl[766]" 0;
	setAttr ".phl[767]" 0;
	setAttr ".phl[768]" 0;
	setAttr ".phl[769]" 0;
	setAttr ".phl[770]" 0;
	setAttr ".phl[771]" 0;
	setAttr ".phl[772]" 0;
	setAttr ".phl[773]" 0;
	setAttr ".phl[774]" 0;
	setAttr ".phl[775]" 0;
	setAttr ".phl[776]" 0;
	setAttr ".phl[777]" 0;
	setAttr ".phl[778]" 0;
	setAttr ".phl[779]" 0;
	setAttr ".phl[780]" 0;
	setAttr ".phl[781]" 0;
	setAttr ".phl[782]" 0;
	setAttr ".phl[783]" 0;
	setAttr ".phl[784]" 0;
	setAttr ".phl[785]" 0;
	setAttr ".phl[786]" 0;
	setAttr ".phl[787]" 0;
	setAttr ".phl[788]" 0;
	setAttr ".phl[789]" 0;
	setAttr ".phl[790]" 0;
	setAttr ".phl[791]" 0;
	setAttr ".phl[792]" 0;
	setAttr ".phl[793]" 0;
	setAttr ".phl[794]" 0;
	setAttr ".phl[795]" 0;
	setAttr ".phl[796]" 0;
	setAttr ".phl[797]" 0;
	setAttr ".phl[798]" 0;
	setAttr ".phl[799]" 0;
	setAttr ".phl[800]" 0;
	setAttr ".phl[801]" 0;
	setAttr ".phl[802]" 0;
	setAttr ".phl[803]" 0;
	setAttr ".phl[804]" 0;
	setAttr ".phl[805]" 0;
	setAttr ".phl[806]" 0;
	setAttr ".phl[807]" 0;
	setAttr ".phl[808]" 0;
	setAttr ".phl[809]" 0;
	setAttr ".phl[810]" 0;
	setAttr ".phl[811]" 0;
	setAttr ".phl[812]" 0;
	setAttr ".phl[813]" 0;
	setAttr ".phl[814]" 0;
	setAttr ".phl[815]" 0;
	setAttr ".phl[816]" 0;
	setAttr ".phl[817]" 0;
	setAttr ".phl[818]" 0;
	setAttr ".phl[819]" 0;
	setAttr ".phl[820]" 0;
	setAttr ".phl[821]" 0;
	setAttr ".phl[822]" 0;
	setAttr ".phl[823]" 0;
	setAttr ".phl[824]" 0;
	setAttr ".phl[825]" 0;
	setAttr ".phl[826]" 0;
	setAttr ".phl[827]" 0;
	setAttr ".phl[828]" 0;
	setAttr ".phl[829]" 0;
	setAttr ".phl[830]" 0;
	setAttr ".phl[831]" 0;
	setAttr ".phl[832]" 0;
	setAttr ".phl[833]" 0;
	setAttr ".phl[834]" 0;
	setAttr ".phl[835]" 0;
	setAttr ".phl[836]" 0;
	setAttr ".phl[837]" 0;
	setAttr ".phl[838]" 0;
	setAttr ".phl[839]" 0;
	setAttr ".phl[840]" 0;
	setAttr ".phl[841]" 0;
	setAttr ".phl[842]" 0;
	setAttr ".phl[843]" 0;
	setAttr ".phl[844]" 0;
	setAttr ".phl[845]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 10
		2 "|rigw:robo85" "visibility" " -av 1"
		2 "|rigw:robo84" "visibility" " -av 1"
		2 "|rigw:robo105" "visibility" " -av 1"
		2 "|rigw:robo106" "visibility" " -av 1"
		2 "|rigw:robo58" "visibility" " -av 1"
		5 4 "rigwRN" "|rigw:robo85.visibility" "rigwRN.placeHolderList[179]" 
		""
		5 4 "rigwRN" "|rigw:robo84.visibility" "rigwRN.placeHolderList[180]" 
		""
		5 4 "rigwRN" "|rigw:robo105.visibility" "rigwRN.placeHolderList[181]" 
		""
		5 4 "rigwRN" "|rigw:robo106.visibility" "rigwRN.placeHolderList[182]" 
		""
		5 4 "rigwRN" "|rigw:robo58.visibility" "rigwRN.placeHolderList[184]" 
		""
		"rigw:rigRN" 0
		"rigwRN" 1111
		2 "|rigw:robo78" "visibility" " -av 1"
		2 "|rigw:robo77" "visibility" " -av 1"
		2 "|rigw:robo57" "visibility" " -av 1"
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translate" " -type \"double3\" 0.740445 0.715486 4.165678"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotate" " -type \"double3\" 112.246345 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translate" " -type \"double3\" 0.740445 1.248661 3.934626"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotate" " -type \"double3\" 87.636758 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translate" " -type \"double3\" 0.740445 2.006366 3.876286"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotate" " -type \"double3\" 27.17363 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translate" " -type \"double3\" 0.740445 2.443066 2.998664"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotate" " -type \"double3\" 27.17363 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translate" " -type \"double3\" 0.740572 2.802991 2.134471"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotate" " -type \"double3\" 28.003616 0.054517 -0.506614"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translate" " -type \"double3\" 0.740445 3.03787 1.259282"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotate" " -type \"double3\" 4.484307 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translate" " -type \"double3\" 0.740445 3.050656 0.33016"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translate" " -type \"double3\" 0.740445 3.050656 -0.642503"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translate" " -type \"double3\" 0.740445 3.050656 -1.615166"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translate" " -type \"double3\" 0.740445 2.829374 -2.525288"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotate" " -type \"double3\" -23.380861 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translate" " -type \"double3\" 0.740445 2.562667 -3.403567"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotate" " -type \"double3\" -27.44369 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translate" " -type \"double3\" 0.740445 2.112302 -4.27079"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotate" " -type \"double3\" -27.44369 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translate" " -type \"double3\" 0.740445 1.661936 -5.138014"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotate" " -type \"double3\" -27.44369 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translate" " -type \"double3\" 0.740445 1.078102 -5.613094"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotate" " -type \"double3\" -101.394566 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translate" " -type \"double3\" 0.740445 0.659831 -4.957155"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotate" " -type \"double3\" 210.809462 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translate" " -type \"double3\" 0.740445 0.360482 -4.137065"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translate" " -type \"double3\" 0.740445 0.360482 -3.159629"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translate" " -type \"double3\" 0.740445 0.360482 -2.182193"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translate" " -type \"double3\" 0.740445 0.360482 -1.204757"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translate" " -type \"double3\" 0.740445 0.360482 -0.22732"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translate" " -type \"double3\" 0.740445 0.360482 0.750116"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translate" " -type \"double3\" 0.740445 0.360482 1.727552"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translate" " -type \"double3\" 0.740445 0.360482 2.704989"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translate" " -type \"double3\" 0.740445 0.360482 3.682425"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translate" " -type \"double3\" -0.740596 2.156221 3.374191"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotate" " -type \"double3\" 152.82637 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translate" " -type \"double3\" -0.740596 1.662469 4.221696"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotate" " -type \"double3\" 148.612692 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translate" " -type \"double3\" -0.740596 0.974319 4.426617"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotate" " -type \"double3\" 74.561257 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translate" " -type \"double3\" -0.740596 0.360482 3.904841"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translate" " -type \"double3\" -0.740596 0.360482 2.927405"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translate" " -type \"double3\" -0.740596 0.360482 1.949969"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translate" " -type \"double3\" -0.740596 0.360482 0.972532"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translate" " -type \"double3\" -0.740596 0.360482 -0.00490393"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translate" " -type \"double3\" -0.740596 0.360482 -0.98234"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translate" " -type \"double3\" -0.740596 0.360482 -1.959776"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translate" " -type \"double3\" -0.740596 0.360482 -2.937213"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translate" " -type \"double3\" -0.740596 0.360482 -3.914649"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translate" " -type \"double3\" -0.740596 0.360482 -4.892085"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translate" " -type \"double3\" -0.740596 0.664251 -5.529688"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotate" " -type \"double3\" -78.605434 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translate" " -type \"double3\" -0.740596 1.474073 -5.499763"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotate" " -type \"double3\" 27.44369 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translate" " -type \"double3\" -0.740596 1.924439 -4.632539"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotate" " -type \"double3\" 27.44369 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translate" " -type \"double3\" -0.740596 2.374804 -3.765316"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotate" " -type \"double3\" 27.44369 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translate" " -type \"double3\" -0.740596 2.818561 -2.910817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotate" " -type \"double3\" 27.44369 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translate" " -type \"double3\" -0.740596 3.050656 -2.12289"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translate" " -type \"double3\" -0.740596 3.050656 -1.150227"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translate" " -type \"double3\" -0.740596 3.050656 -0.177564"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translate" " -type \"double3\" -0.740596 3.050656 0.795099"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translate" " -type \"double3\" -0.741585 2.953546 1.639622"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotate" " -type \"double3\" 161.793326 0.0545059 -179.49349"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translate" " -type \"double3\" -0.740596 2.603864 2.502186"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotate" " -type \"double3\" 152.82637 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scaleZ" " -av"
		
		2 "rigw:_mesh" "displayType" " 2"
		5 4 "rigwRN" "|rigw:robo78.visibility" "rigwRN.placeHolderList[363]" 
		""
		5 4 "rigwRN" "|rigw:robo77.visibility" "rigwRN.placeHolderList[364]" 
		""
		5 4 "rigwRN" "|rigw:robo57.visibility" "rigwRN.placeHolderList[365]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.visibility" 
		"rigwRN.placeHolderList[366]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateX" 
		"rigwRN.placeHolderList[367]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateY" 
		"rigwRN.placeHolderList[368]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateZ" 
		"rigwRN.placeHolderList[369]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateX" 
		"rigwRN.placeHolderList[370]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateY" 
		"rigwRN.placeHolderList[371]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateZ" 
		"rigwRN.placeHolderList[372]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleX" 
		"rigwRN.placeHolderList[373]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleY" 
		"rigwRN.placeHolderList[374]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleZ" 
		"rigwRN.placeHolderList[375]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.visibility" 
		"rigwRN.placeHolderList[376]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateX" 
		"rigwRN.placeHolderList[377]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateY" 
		"rigwRN.placeHolderList[378]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateZ" 
		"rigwRN.placeHolderList[379]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateX" 
		"rigwRN.placeHolderList[380]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateY" 
		"rigwRN.placeHolderList[381]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateZ" 
		"rigwRN.placeHolderList[382]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleX" 
		"rigwRN.placeHolderList[383]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleY" 
		"rigwRN.placeHolderList[384]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleZ" 
		"rigwRN.placeHolderList[385]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.visibility" 
		"rigwRN.placeHolderList[386]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateX" 
		"rigwRN.placeHolderList[387]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateY" 
		"rigwRN.placeHolderList[388]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateZ" 
		"rigwRN.placeHolderList[389]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateX" 
		"rigwRN.placeHolderList[390]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateY" 
		"rigwRN.placeHolderList[391]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateZ" 
		"rigwRN.placeHolderList[392]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleX" 
		"rigwRN.placeHolderList[393]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleY" 
		"rigwRN.placeHolderList[394]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleZ" 
		"rigwRN.placeHolderList[395]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.visibility" 
		"rigwRN.placeHolderList[396]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateX" 
		"rigwRN.placeHolderList[397]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateY" 
		"rigwRN.placeHolderList[398]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateZ" 
		"rigwRN.placeHolderList[399]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateX" 
		"rigwRN.placeHolderList[400]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateY" 
		"rigwRN.placeHolderList[401]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateZ" 
		"rigwRN.placeHolderList[402]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleX" 
		"rigwRN.placeHolderList[403]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleY" 
		"rigwRN.placeHolderList[404]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleZ" 
		"rigwRN.placeHolderList[405]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.visibility" 
		"rigwRN.placeHolderList[406]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateX" 
		"rigwRN.placeHolderList[407]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateY" 
		"rigwRN.placeHolderList[408]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateZ" 
		"rigwRN.placeHolderList[409]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateX" 
		"rigwRN.placeHolderList[410]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateY" 
		"rigwRN.placeHolderList[411]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateZ" 
		"rigwRN.placeHolderList[412]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleX" 
		"rigwRN.placeHolderList[413]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleY" 
		"rigwRN.placeHolderList[414]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleZ" 
		"rigwRN.placeHolderList[415]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.visibility" 
		"rigwRN.placeHolderList[416]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateX" 
		"rigwRN.placeHolderList[417]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateY" 
		"rigwRN.placeHolderList[418]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateZ" 
		"rigwRN.placeHolderList[419]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateX" 
		"rigwRN.placeHolderList[420]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateY" 
		"rigwRN.placeHolderList[421]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateZ" 
		"rigwRN.placeHolderList[422]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleX" 
		"rigwRN.placeHolderList[423]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleY" 
		"rigwRN.placeHolderList[424]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleZ" 
		"rigwRN.placeHolderList[425]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.visibility" 
		"rigwRN.placeHolderList[426]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateX" 
		"rigwRN.placeHolderList[427]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateY" 
		"rigwRN.placeHolderList[428]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateZ" 
		"rigwRN.placeHolderList[429]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateX" 
		"rigwRN.placeHolderList[430]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateY" 
		"rigwRN.placeHolderList[431]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateZ" 
		"rigwRN.placeHolderList[432]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleX" 
		"rigwRN.placeHolderList[433]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleY" 
		"rigwRN.placeHolderList[434]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleZ" 
		"rigwRN.placeHolderList[435]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.visibility" 
		"rigwRN.placeHolderList[436]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateX" 
		"rigwRN.placeHolderList[437]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateY" 
		"rigwRN.placeHolderList[438]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateZ" 
		"rigwRN.placeHolderList[439]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateX" 
		"rigwRN.placeHolderList[440]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateY" 
		"rigwRN.placeHolderList[441]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateZ" 
		"rigwRN.placeHolderList[442]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleX" 
		"rigwRN.placeHolderList[443]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleY" 
		"rigwRN.placeHolderList[444]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleZ" 
		"rigwRN.placeHolderList[445]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.visibility" 
		"rigwRN.placeHolderList[446]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateX" 
		"rigwRN.placeHolderList[447]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateY" 
		"rigwRN.placeHolderList[448]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateZ" 
		"rigwRN.placeHolderList[449]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateX" 
		"rigwRN.placeHolderList[450]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateY" 
		"rigwRN.placeHolderList[451]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateZ" 
		"rigwRN.placeHolderList[452]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleX" 
		"rigwRN.placeHolderList[453]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleY" 
		"rigwRN.placeHolderList[454]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleZ" 
		"rigwRN.placeHolderList[455]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.visibility" 
		"rigwRN.placeHolderList[456]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateX" 
		"rigwRN.placeHolderList[457]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateY" 
		"rigwRN.placeHolderList[458]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateZ" 
		"rigwRN.placeHolderList[459]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateX" 
		"rigwRN.placeHolderList[460]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateY" 
		"rigwRN.placeHolderList[461]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateZ" 
		"rigwRN.placeHolderList[462]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleX" 
		"rigwRN.placeHolderList[463]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleY" 
		"rigwRN.placeHolderList[464]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleZ" 
		"rigwRN.placeHolderList[465]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.visibility" 
		"rigwRN.placeHolderList[466]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateX" 
		"rigwRN.placeHolderList[467]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateY" 
		"rigwRN.placeHolderList[468]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateZ" 
		"rigwRN.placeHolderList[469]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateX" 
		"rigwRN.placeHolderList[470]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateY" 
		"rigwRN.placeHolderList[471]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateZ" 
		"rigwRN.placeHolderList[472]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleX" 
		"rigwRN.placeHolderList[473]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleY" 
		"rigwRN.placeHolderList[474]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleZ" 
		"rigwRN.placeHolderList[475]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.visibility" 
		"rigwRN.placeHolderList[476]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateX" 
		"rigwRN.placeHolderList[477]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateY" 
		"rigwRN.placeHolderList[478]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateZ" 
		"rigwRN.placeHolderList[479]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateX" 
		"rigwRN.placeHolderList[480]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateY" 
		"rigwRN.placeHolderList[481]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateZ" 
		"rigwRN.placeHolderList[482]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleX" 
		"rigwRN.placeHolderList[483]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleY" 
		"rigwRN.placeHolderList[484]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleZ" 
		"rigwRN.placeHolderList[485]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.visibility" 
		"rigwRN.placeHolderList[486]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateX" 
		"rigwRN.placeHolderList[487]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateY" 
		"rigwRN.placeHolderList[488]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateZ" 
		"rigwRN.placeHolderList[489]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateX" 
		"rigwRN.placeHolderList[490]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateY" 
		"rigwRN.placeHolderList[491]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateZ" 
		"rigwRN.placeHolderList[492]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleX" 
		"rigwRN.placeHolderList[493]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleY" 
		"rigwRN.placeHolderList[494]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleZ" 
		"rigwRN.placeHolderList[495]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.visibility" 
		"rigwRN.placeHolderList[496]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateX" 
		"rigwRN.placeHolderList[497]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateY" 
		"rigwRN.placeHolderList[498]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateZ" 
		"rigwRN.placeHolderList[499]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateX" 
		"rigwRN.placeHolderList[500]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateY" 
		"rigwRN.placeHolderList[501]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateZ" 
		"rigwRN.placeHolderList[502]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleX" 
		"rigwRN.placeHolderList[503]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleY" 
		"rigwRN.placeHolderList[504]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleZ" 
		"rigwRN.placeHolderList[505]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.visibility" 
		"rigwRN.placeHolderList[506]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateX" 
		"rigwRN.placeHolderList[507]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateY" 
		"rigwRN.placeHolderList[508]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateZ" 
		"rigwRN.placeHolderList[509]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateX" 
		"rigwRN.placeHolderList[510]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateY" 
		"rigwRN.placeHolderList[511]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateZ" 
		"rigwRN.placeHolderList[512]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleX" 
		"rigwRN.placeHolderList[513]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleY" 
		"rigwRN.placeHolderList[514]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleZ" 
		"rigwRN.placeHolderList[515]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.visibility" 
		"rigwRN.placeHolderList[516]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateX" 
		"rigwRN.placeHolderList[517]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateY" 
		"rigwRN.placeHolderList[518]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateZ" 
		"rigwRN.placeHolderList[519]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateX" 
		"rigwRN.placeHolderList[520]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateY" 
		"rigwRN.placeHolderList[521]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateZ" 
		"rigwRN.placeHolderList[522]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleX" 
		"rigwRN.placeHolderList[523]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleY" 
		"rigwRN.placeHolderList[524]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleZ" 
		"rigwRN.placeHolderList[525]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.visibility" 
		"rigwRN.placeHolderList[526]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateX" 
		"rigwRN.placeHolderList[527]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateY" 
		"rigwRN.placeHolderList[528]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateZ" 
		"rigwRN.placeHolderList[529]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateX" 
		"rigwRN.placeHolderList[530]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateY" 
		"rigwRN.placeHolderList[531]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateZ" 
		"rigwRN.placeHolderList[532]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleX" 
		"rigwRN.placeHolderList[533]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleY" 
		"rigwRN.placeHolderList[534]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleZ" 
		"rigwRN.placeHolderList[535]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.visibility" 
		"rigwRN.placeHolderList[536]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateX" 
		"rigwRN.placeHolderList[537]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateY" 
		"rigwRN.placeHolderList[538]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateZ" 
		"rigwRN.placeHolderList[539]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateX" 
		"rigwRN.placeHolderList[540]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateY" 
		"rigwRN.placeHolderList[541]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateZ" 
		"rigwRN.placeHolderList[542]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleX" 
		"rigwRN.placeHolderList[543]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleY" 
		"rigwRN.placeHolderList[544]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleZ" 
		"rigwRN.placeHolderList[545]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.visibility" 
		"rigwRN.placeHolderList[546]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateX" 
		"rigwRN.placeHolderList[547]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateY" 
		"rigwRN.placeHolderList[548]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateZ" 
		"rigwRN.placeHolderList[549]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateX" 
		"rigwRN.placeHolderList[550]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateY" 
		"rigwRN.placeHolderList[551]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateZ" 
		"rigwRN.placeHolderList[552]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleX" 
		"rigwRN.placeHolderList[553]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleY" 
		"rigwRN.placeHolderList[554]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleZ" 
		"rigwRN.placeHolderList[555]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.visibility" 
		"rigwRN.placeHolderList[556]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateX" 
		"rigwRN.placeHolderList[557]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateY" 
		"rigwRN.placeHolderList[558]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateZ" 
		"rigwRN.placeHolderList[559]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateX" 
		"rigwRN.placeHolderList[560]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateY" 
		"rigwRN.placeHolderList[561]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateZ" 
		"rigwRN.placeHolderList[562]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleX" 
		"rigwRN.placeHolderList[563]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleY" 
		"rigwRN.placeHolderList[564]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleZ" 
		"rigwRN.placeHolderList[565]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.visibility" 
		"rigwRN.placeHolderList[566]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateX" 
		"rigwRN.placeHolderList[567]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateY" 
		"rigwRN.placeHolderList[568]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateZ" 
		"rigwRN.placeHolderList[569]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateX" 
		"rigwRN.placeHolderList[570]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateY" 
		"rigwRN.placeHolderList[571]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateZ" 
		"rigwRN.placeHolderList[572]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleX" 
		"rigwRN.placeHolderList[573]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleY" 
		"rigwRN.placeHolderList[574]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleZ" 
		"rigwRN.placeHolderList[575]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.visibility" 
		"rigwRN.placeHolderList[576]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateX" 
		"rigwRN.placeHolderList[577]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateY" 
		"rigwRN.placeHolderList[578]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateZ" 
		"rigwRN.placeHolderList[579]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateX" 
		"rigwRN.placeHolderList[580]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateY" 
		"rigwRN.placeHolderList[581]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateZ" 
		"rigwRN.placeHolderList[582]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleX" 
		"rigwRN.placeHolderList[583]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleY" 
		"rigwRN.placeHolderList[584]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleZ" 
		"rigwRN.placeHolderList[585]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.visibility" 
		"rigwRN.placeHolderList[586]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateX" 
		"rigwRN.placeHolderList[587]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateY" 
		"rigwRN.placeHolderList[588]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateZ" 
		"rigwRN.placeHolderList[589]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateX" 
		"rigwRN.placeHolderList[590]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateY" 
		"rigwRN.placeHolderList[591]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateZ" 
		"rigwRN.placeHolderList[592]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleX" 
		"rigwRN.placeHolderList[593]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleY" 
		"rigwRN.placeHolderList[594]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleZ" 
		"rigwRN.placeHolderList[595]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.visibility" 
		"rigwRN.placeHolderList[596]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateX" 
		"rigwRN.placeHolderList[597]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateY" 
		"rigwRN.placeHolderList[598]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateZ" 
		"rigwRN.placeHolderList[599]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateX" 
		"rigwRN.placeHolderList[600]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateY" 
		"rigwRN.placeHolderList[601]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateZ" 
		"rigwRN.placeHolderList[602]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleX" 
		"rigwRN.placeHolderList[603]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleY" 
		"rigwRN.placeHolderList[604]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleZ" 
		"rigwRN.placeHolderList[605]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.visibility" 
		"rigwRN.placeHolderList[606]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateX" 
		"rigwRN.placeHolderList[607]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateY" 
		"rigwRN.placeHolderList[608]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateZ" 
		"rigwRN.placeHolderList[609]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateX" 
		"rigwRN.placeHolderList[610]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateY" 
		"rigwRN.placeHolderList[611]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateZ" 
		"rigwRN.placeHolderList[612]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleX" 
		"rigwRN.placeHolderList[613]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleY" 
		"rigwRN.placeHolderList[614]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleZ" 
		"rigwRN.placeHolderList[615]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.visibility" 
		"rigwRN.placeHolderList[616]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateX" 
		"rigwRN.placeHolderList[617]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateY" 
		"rigwRN.placeHolderList[618]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateZ" 
		"rigwRN.placeHolderList[619]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateX" 
		"rigwRN.placeHolderList[620]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateY" 
		"rigwRN.placeHolderList[621]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateZ" 
		"rigwRN.placeHolderList[622]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleX" 
		"rigwRN.placeHolderList[623]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleY" 
		"rigwRN.placeHolderList[624]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleZ" 
		"rigwRN.placeHolderList[625]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.visibility" 
		"rigwRN.placeHolderList[626]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateX" 
		"rigwRN.placeHolderList[627]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateY" 
		"rigwRN.placeHolderList[628]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateZ" 
		"rigwRN.placeHolderList[629]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateX" 
		"rigwRN.placeHolderList[630]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateY" 
		"rigwRN.placeHolderList[631]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateZ" 
		"rigwRN.placeHolderList[632]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleX" 
		"rigwRN.placeHolderList[633]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleY" 
		"rigwRN.placeHolderList[634]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleZ" 
		"rigwRN.placeHolderList[635]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.visibility" 
		"rigwRN.placeHolderList[636]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateX" 
		"rigwRN.placeHolderList[637]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateY" 
		"rigwRN.placeHolderList[638]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateZ" 
		"rigwRN.placeHolderList[639]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateX" 
		"rigwRN.placeHolderList[640]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateY" 
		"rigwRN.placeHolderList[641]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateZ" 
		"rigwRN.placeHolderList[642]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleX" 
		"rigwRN.placeHolderList[643]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleY" 
		"rigwRN.placeHolderList[644]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleZ" 
		"rigwRN.placeHolderList[645]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.visibility" 
		"rigwRN.placeHolderList[646]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateX" 
		"rigwRN.placeHolderList[647]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateY" 
		"rigwRN.placeHolderList[648]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateZ" 
		"rigwRN.placeHolderList[649]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateX" 
		"rigwRN.placeHolderList[650]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateY" 
		"rigwRN.placeHolderList[651]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateZ" 
		"rigwRN.placeHolderList[652]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleX" 
		"rigwRN.placeHolderList[653]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleY" 
		"rigwRN.placeHolderList[654]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleZ" 
		"rigwRN.placeHolderList[655]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.visibility" 
		"rigwRN.placeHolderList[656]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateX" 
		"rigwRN.placeHolderList[657]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateY" 
		"rigwRN.placeHolderList[658]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateZ" 
		"rigwRN.placeHolderList[659]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateX" 
		"rigwRN.placeHolderList[660]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateY" 
		"rigwRN.placeHolderList[661]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateZ" 
		"rigwRN.placeHolderList[662]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleX" 
		"rigwRN.placeHolderList[663]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleY" 
		"rigwRN.placeHolderList[664]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleZ" 
		"rigwRN.placeHolderList[665]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.visibility" 
		"rigwRN.placeHolderList[666]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateX" 
		"rigwRN.placeHolderList[667]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateY" 
		"rigwRN.placeHolderList[668]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateZ" 
		"rigwRN.placeHolderList[669]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateX" 
		"rigwRN.placeHolderList[670]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateY" 
		"rigwRN.placeHolderList[671]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateZ" 
		"rigwRN.placeHolderList[672]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleX" 
		"rigwRN.placeHolderList[673]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleY" 
		"rigwRN.placeHolderList[674]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleZ" 
		"rigwRN.placeHolderList[675]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.visibility" 
		"rigwRN.placeHolderList[676]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateX" 
		"rigwRN.placeHolderList[677]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateY" 
		"rigwRN.placeHolderList[678]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateZ" 
		"rigwRN.placeHolderList[679]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateX" 
		"rigwRN.placeHolderList[680]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateY" 
		"rigwRN.placeHolderList[681]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateZ" 
		"rigwRN.placeHolderList[682]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleX" 
		"rigwRN.placeHolderList[683]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleY" 
		"rigwRN.placeHolderList[684]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleZ" 
		"rigwRN.placeHolderList[685]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.visibility" 
		"rigwRN.placeHolderList[686]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateX" 
		"rigwRN.placeHolderList[687]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateY" 
		"rigwRN.placeHolderList[688]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateZ" 
		"rigwRN.placeHolderList[689]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateX" 
		"rigwRN.placeHolderList[690]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateY" 
		"rigwRN.placeHolderList[691]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateZ" 
		"rigwRN.placeHolderList[692]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleX" 
		"rigwRN.placeHolderList[693]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleY" 
		"rigwRN.placeHolderList[694]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleZ" 
		"rigwRN.placeHolderList[695]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.visibility" 
		"rigwRN.placeHolderList[696]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateX" 
		"rigwRN.placeHolderList[697]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateY" 
		"rigwRN.placeHolderList[698]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateZ" 
		"rigwRN.placeHolderList[699]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateX" 
		"rigwRN.placeHolderList[700]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateY" 
		"rigwRN.placeHolderList[701]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateZ" 
		"rigwRN.placeHolderList[702]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleX" 
		"rigwRN.placeHolderList[703]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleY" 
		"rigwRN.placeHolderList[704]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleZ" 
		"rigwRN.placeHolderList[705]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.visibility" 
		"rigwRN.placeHolderList[706]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateX" 
		"rigwRN.placeHolderList[707]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateY" 
		"rigwRN.placeHolderList[708]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateZ" 
		"rigwRN.placeHolderList[709]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateX" 
		"rigwRN.placeHolderList[710]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateY" 
		"rigwRN.placeHolderList[711]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateZ" 
		"rigwRN.placeHolderList[712]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleX" 
		"rigwRN.placeHolderList[713]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleY" 
		"rigwRN.placeHolderList[714]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleZ" 
		"rigwRN.placeHolderList[715]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.visibility" 
		"rigwRN.placeHolderList[716]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateX" 
		"rigwRN.placeHolderList[717]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateY" 
		"rigwRN.placeHolderList[718]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateZ" 
		"rigwRN.placeHolderList[719]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateX" 
		"rigwRN.placeHolderList[720]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateY" 
		"rigwRN.placeHolderList[721]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateZ" 
		"rigwRN.placeHolderList[722]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleX" 
		"rigwRN.placeHolderList[723]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleY" 
		"rigwRN.placeHolderList[724]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleZ" 
		"rigwRN.placeHolderList[725]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.visibility" 
		"rigwRN.placeHolderList[726]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateX" 
		"rigwRN.placeHolderList[727]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateY" 
		"rigwRN.placeHolderList[728]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateZ" 
		"rigwRN.placeHolderList[729]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateX" 
		"rigwRN.placeHolderList[730]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateY" 
		"rigwRN.placeHolderList[731]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateZ" 
		"rigwRN.placeHolderList[732]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleX" 
		"rigwRN.placeHolderList[733]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleY" 
		"rigwRN.placeHolderList[734]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleZ" 
		"rigwRN.placeHolderList[735]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.visibility" 
		"rigwRN.placeHolderList[736]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateX" 
		"rigwRN.placeHolderList[737]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateY" 
		"rigwRN.placeHolderList[738]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateZ" 
		"rigwRN.placeHolderList[739]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateX" 
		"rigwRN.placeHolderList[740]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateY" 
		"rigwRN.placeHolderList[741]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateZ" 
		"rigwRN.placeHolderList[742]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleX" 
		"rigwRN.placeHolderList[743]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleY" 
		"rigwRN.placeHolderList[744]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleZ" 
		"rigwRN.placeHolderList[745]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.visibility" 
		"rigwRN.placeHolderList[746]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateX" 
		"rigwRN.placeHolderList[747]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateY" 
		"rigwRN.placeHolderList[748]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateZ" 
		"rigwRN.placeHolderList[749]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateX" 
		"rigwRN.placeHolderList[750]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateY" 
		"rigwRN.placeHolderList[751]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateZ" 
		"rigwRN.placeHolderList[752]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleX" 
		"rigwRN.placeHolderList[753]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleY" 
		"rigwRN.placeHolderList[754]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleZ" 
		"rigwRN.placeHolderList[755]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.visibility" 
		"rigwRN.placeHolderList[756]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateX" 
		"rigwRN.placeHolderList[757]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateY" 
		"rigwRN.placeHolderList[758]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateZ" 
		"rigwRN.placeHolderList[759]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateX" 
		"rigwRN.placeHolderList[760]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateY" 
		"rigwRN.placeHolderList[761]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateZ" 
		"rigwRN.placeHolderList[762]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleX" 
		"rigwRN.placeHolderList[763]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleY" 
		"rigwRN.placeHolderList[764]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleZ" 
		"rigwRN.placeHolderList[765]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.visibility" 
		"rigwRN.placeHolderList[766]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateX" 
		"rigwRN.placeHolderList[767]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateY" 
		"rigwRN.placeHolderList[768]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateZ" 
		"rigwRN.placeHolderList[769]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateX" 
		"rigwRN.placeHolderList[770]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateY" 
		"rigwRN.placeHolderList[771]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateZ" 
		"rigwRN.placeHolderList[772]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleX" 
		"rigwRN.placeHolderList[773]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleY" 
		"rigwRN.placeHolderList[774]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleZ" 
		"rigwRN.placeHolderList[775]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.visibility" 
		"rigwRN.placeHolderList[776]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateX" 
		"rigwRN.placeHolderList[777]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateY" 
		"rigwRN.placeHolderList[778]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateZ" 
		"rigwRN.placeHolderList[779]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateX" 
		"rigwRN.placeHolderList[780]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateY" 
		"rigwRN.placeHolderList[781]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateZ" 
		"rigwRN.placeHolderList[782]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleX" 
		"rigwRN.placeHolderList[783]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleY" 
		"rigwRN.placeHolderList[784]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleZ" 
		"rigwRN.placeHolderList[785]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.visibility" 
		"rigwRN.placeHolderList[786]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateX" 
		"rigwRN.placeHolderList[787]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateY" 
		"rigwRN.placeHolderList[788]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateZ" 
		"rigwRN.placeHolderList[789]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateX" 
		"rigwRN.placeHolderList[790]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateY" 
		"rigwRN.placeHolderList[791]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateZ" 
		"rigwRN.placeHolderList[792]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleX" 
		"rigwRN.placeHolderList[793]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleY" 
		"rigwRN.placeHolderList[794]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleZ" 
		"rigwRN.placeHolderList[795]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.visibility" 
		"rigwRN.placeHolderList[796]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateX" 
		"rigwRN.placeHolderList[797]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateY" 
		"rigwRN.placeHolderList[798]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateZ" 
		"rigwRN.placeHolderList[799]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateX" 
		"rigwRN.placeHolderList[800]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateY" 
		"rigwRN.placeHolderList[801]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateZ" 
		"rigwRN.placeHolderList[802]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleX" 
		"rigwRN.placeHolderList[803]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleY" 
		"rigwRN.placeHolderList[804]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleZ" 
		"rigwRN.placeHolderList[805]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.visibility" 
		"rigwRN.placeHolderList[806]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateX" 
		"rigwRN.placeHolderList[807]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateY" 
		"rigwRN.placeHolderList[808]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateZ" 
		"rigwRN.placeHolderList[809]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateX" 
		"rigwRN.placeHolderList[810]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateY" 
		"rigwRN.placeHolderList[811]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateZ" 
		"rigwRN.placeHolderList[812]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleX" 
		"rigwRN.placeHolderList[813]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleY" 
		"rigwRN.placeHolderList[814]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleZ" 
		"rigwRN.placeHolderList[815]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.visibility" 
		"rigwRN.placeHolderList[816]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateX" 
		"rigwRN.placeHolderList[817]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateY" 
		"rigwRN.placeHolderList[818]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateZ" 
		"rigwRN.placeHolderList[819]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateX" 
		"rigwRN.placeHolderList[820]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateY" 
		"rigwRN.placeHolderList[821]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateZ" 
		"rigwRN.placeHolderList[822]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleX" 
		"rigwRN.placeHolderList[823]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleY" 
		"rigwRN.placeHolderList[824]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleZ" 
		"rigwRN.placeHolderList[825]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.visibility" 
		"rigwRN.placeHolderList[826]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateX" 
		"rigwRN.placeHolderList[827]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateY" 
		"rigwRN.placeHolderList[828]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateZ" 
		"rigwRN.placeHolderList[829]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateX" 
		"rigwRN.placeHolderList[830]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateY" 
		"rigwRN.placeHolderList[831]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateZ" 
		"rigwRN.placeHolderList[832]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleX" 
		"rigwRN.placeHolderList[833]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleY" 
		"rigwRN.placeHolderList[834]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleZ" 
		"rigwRN.placeHolderList[835]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.visibility" 
		"rigwRN.placeHolderList[836]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateX" 
		"rigwRN.placeHolderList[837]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateY" 
		"rigwRN.placeHolderList[838]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateZ" 
		"rigwRN.placeHolderList[839]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateX" 
		"rigwRN.placeHolderList[840]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateY" 
		"rigwRN.placeHolderList[841]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateZ" 
		"rigwRN.placeHolderList[842]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleX" 
		"rigwRN.placeHolderList[843]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleY" 
		"rigwRN.placeHolderList[844]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleZ" 
		"rigwRN.placeHolderList[845]" ""
		"rigw:rigRN" 406
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "visibility" " -av 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scale" " -type \"double3\" 1.8 1.8 1.8"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translate" " -type \"double3\" 0 0.0860172 0.131701"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"CenterBtwFeet" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" -10.216078 -12.316063 23.837782"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"Global" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotate" " -type \"double3\" 0 0 -33.36369"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotate" " -type \"double3\" 5.047571 7.017677 8.922846"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"Global" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 -23.632635"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translate" " -type \"double3\" 1.043829 0.0845126 0.128291"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotate" " -type \"double3\" 0 -3.253483 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translate" " -type \"double3\" 1.435517 -0.619525 -0.45237"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotate" " -type \"double3\" 0 -14.12842 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotate" " -type \"double3\" -22.028843 -16.267797 -39.607339"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" -0.609696 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0.571318 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"IKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"IKVis" " -av -k 1 1"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateX" "rigwRN.placeHolderList[185]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateY" "rigwRN.placeHolderList[186]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateZ" "rigwRN.placeHolderList[187]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.visibility" "rigwRN.placeHolderList[188]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateX" "rigwRN.placeHolderList[189]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateY" "rigwRN.placeHolderList[190]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateZ" "rigwRN.placeHolderList[191]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleX" "rigwRN.placeHolderList[192]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleY" "rigwRN.placeHolderList[193]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleZ" "rigwRN.placeHolderList[194]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateX" 
		"rigwRN.placeHolderList[195]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateY" 
		"rigwRN.placeHolderList[196]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateZ" 
		"rigwRN.placeHolderList[197]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateX" 
		"rigwRN.placeHolderList[198]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateY" 
		"rigwRN.placeHolderList[199]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateZ" 
		"rigwRN.placeHolderList[200]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateX" 
		"rigwRN.placeHolderList[201]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateY" 
		"rigwRN.placeHolderList[202]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateZ" 
		"rigwRN.placeHolderList[203]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateX" 
		"rigwRN.placeHolderList[204]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateY" 
		"rigwRN.placeHolderList[205]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateZ" 
		"rigwRN.placeHolderList[206]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateX" 
		"rigwRN.placeHolderList[207]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateY" 
		"rigwRN.placeHolderList[208]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateZ" 
		"rigwRN.placeHolderList[209]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateX" 
		"rigwRN.placeHolderList[210]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateY" 
		"rigwRN.placeHolderList[211]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateZ" 
		"rigwRN.placeHolderList[212]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateX" 
		"rigwRN.placeHolderList[213]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateY" 
		"rigwRN.placeHolderList[214]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateZ" 
		"rigwRN.placeHolderList[215]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateX" 
		"rigwRN.placeHolderList[216]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateY" 
		"rigwRN.placeHolderList[217]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateZ" 
		"rigwRN.placeHolderList[218]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[219]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[220]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[221]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[222]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[223]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[224]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[225]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateX" 
		"rigwRN.placeHolderList[226]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateY" 
		"rigwRN.placeHolderList[227]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateZ" 
		"rigwRN.placeHolderList[228]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateX" 
		"rigwRN.placeHolderList[229]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateY" 
		"rigwRN.placeHolderList[230]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateZ" 
		"rigwRN.placeHolderList[231]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateX" 
		"rigwRN.placeHolderList[232]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateY" 
		"rigwRN.placeHolderList[233]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateZ" 
		"rigwRN.placeHolderList[234]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateX" 
		"rigwRN.placeHolderList[235]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateY" 
		"rigwRN.placeHolderList[236]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateZ" 
		"rigwRN.placeHolderList[237]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.Global" 
		"rigwRN.placeHolderList[238]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[239]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[240]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[241]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateX" 
		"rigwRN.placeHolderList[242]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateY" 
		"rigwRN.placeHolderList[243]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateZ" 
		"rigwRN.placeHolderList[244]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateX" 
		"rigwRN.placeHolderList[245]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateY" 
		"rigwRN.placeHolderList[246]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateZ" 
		"rigwRN.placeHolderList[247]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateX" 
		"rigwRN.placeHolderList[248]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateY" 
		"rigwRN.placeHolderList[249]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateZ" 
		"rigwRN.placeHolderList[250]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.Global" 
		"rigwRN.placeHolderList[251]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateX" 
		"rigwRN.placeHolderList[252]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateY" 
		"rigwRN.placeHolderList[253]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateZ" 
		"rigwRN.placeHolderList[254]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[255]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[256]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[257]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateX" 
		"rigwRN.placeHolderList[258]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateY" 
		"rigwRN.placeHolderList[259]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateZ" 
		"rigwRN.placeHolderList[260]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateX" 
		"rigwRN.placeHolderList[261]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateY" 
		"rigwRN.placeHolderList[262]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[263]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateX" 
		"rigwRN.placeHolderList[264]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateY" 
		"rigwRN.placeHolderList[265]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[266]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateX" 
		"rigwRN.placeHolderList[267]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateY" 
		"rigwRN.placeHolderList[268]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[269]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateX" 
		"rigwRN.placeHolderList[270]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateY" 
		"rigwRN.placeHolderList[271]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[272]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateX" 
		"rigwRN.placeHolderList[273]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateY" 
		"rigwRN.placeHolderList[274]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[275]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateX" 
		"rigwRN.placeHolderList[276]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateY" 
		"rigwRN.placeHolderList[277]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[278]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateX" 
		"rigwRN.placeHolderList[279]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateY" 
		"rigwRN.placeHolderList[280]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[281]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateX" 
		"rigwRN.placeHolderList[282]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateY" 
		"rigwRN.placeHolderList[283]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[284]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateX" 
		"rigwRN.placeHolderList[285]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateY" 
		"rigwRN.placeHolderList[286]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[287]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateX" 
		"rigwRN.placeHolderList[288]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateY" 
		"rigwRN.placeHolderList[289]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[290]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateX" 
		"rigwRN.placeHolderList[291]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateY" 
		"rigwRN.placeHolderList[292]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[293]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateX" 
		"rigwRN.placeHolderList[294]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateY" 
		"rigwRN.placeHolderList[295]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[296]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateX" 
		"rigwRN.placeHolderList[297]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateY" 
		"rigwRN.placeHolderList[298]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateZ" 
		"rigwRN.placeHolderList[299]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateX" 
		"rigwRN.placeHolderList[300]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateY" 
		"rigwRN.placeHolderList[301]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateZ" 
		"rigwRN.placeHolderList[302]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateX" 
		"rigwRN.placeHolderList[303]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateY" 
		"rigwRN.placeHolderList[304]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateZ" 
		"rigwRN.placeHolderList[305]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateX" 
		"rigwRN.placeHolderList[306]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateY" 
		"rigwRN.placeHolderList[307]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateZ" 
		"rigwRN.placeHolderList[308]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateX" 
		"rigwRN.placeHolderList[309]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateY" 
		"rigwRN.placeHolderList[310]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateZ" 
		"rigwRN.placeHolderList[311]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateX" 
		"rigwRN.placeHolderList[312]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateY" 
		"rigwRN.placeHolderList[313]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateZ" 
		"rigwRN.placeHolderList[314]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[315]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[316]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[317]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[318]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[319]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[320]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[321]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rollAngle" 
		"rigwRN.placeHolderList[322]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.roll" 
		"rigwRN.placeHolderList[323]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[324]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[325]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[326]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[327]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateX" 
		"rigwRN.placeHolderList[328]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateY" 
		"rigwRN.placeHolderList[329]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateZ" 
		"rigwRN.placeHolderList[330]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R.rotateX" 
		"rigwRN.placeHolderList[331]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateX" 
		"rigwRN.placeHolderList[332]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateY" 
		"rigwRN.placeHolderList[333]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateZ" 
		"rigwRN.placeHolderList[334]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.follow" 
		"rigwRN.placeHolderList[335]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[336]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[337]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[338]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[339]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[340]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[341]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[342]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rollAngle" 
		"rigwRN.placeHolderList[343]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.roll" 
		"rigwRN.placeHolderList[344]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[345]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[346]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[347]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[348]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateX" 
		"rigwRN.placeHolderList[349]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateY" 
		"rigwRN.placeHolderList[350]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateZ" 
		"rigwRN.placeHolderList[351]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L.rotateX" 
		"rigwRN.placeHolderList[352]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateX" 
		"rigwRN.placeHolderList[353]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateY" 
		"rigwRN.placeHolderList[354]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateZ" 
		"rigwRN.placeHolderList[355]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.follow" 
		"rigwRN.placeHolderList[356]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKIKBlend" 
		"rigwRN.placeHolderList[357]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.IKVis" 
		"rigwRN.placeHolderList[358]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKVis" 
		"rigwRN.placeHolderList[359]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKIKBlend" 
		"rigwRN.placeHolderList[360]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.IKVis" 
		"rigwRN.placeHolderList[361]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKVis" 
		"rigwRN.placeHolderList[362]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -sn "imrVerbosity" -ln "imrVerbosity" -dv 3 -at "short";
	addAttr -ci true -sn "imrThreads" -ln "imrThreads" -dv 1 -at "short";
	addAttr -ci true -sn "imrThreadsAdjust" -ln "imrThreadsAdjust" -dv 1 -min 0 -max 
		1 -at "bool";
	addAttr -ci true -sn "imrTaskOrder" -ln "imrTaskOrder" -dv 1 -at "short";
	addAttr -ci true -sn "imrTaskSize" -ln "imrTaskSize" -at "short";
	addAttr -ci true -sn "imrTaskAdjust" -ln "imrTaskAdjust" -dv 1 -at "short";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
	setAttr ".imrThreads" 4;
	setAttr ".imrThreadsAdjust" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 10 -ast 1 -aet 100 ";
	setAttr ".st" 6;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 5.0475705993160744 2 5.0475705993160744
		 3 5.0475705993160744 4 5.0475705993160744 5 5.0475705993160744 6 5.0475705993160744
		 7 5.0475705993160744 8 5.0475705993160744 9 5.0475705993160744 10 5.0475705993160744
		 11 5.0475705993160744;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 7.0176766386373517 2 7.0176766386373526
		 3 7.0176766386373526 4 7.0176766386373517 5 7.0176766386373517 6 7.0176766386373517
		 7 7.0176766386373526 8 7.0176766386373526 9 7.0176766386373517 10 7.0176766386373517
		 11 7.0176766386373517;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 8.9228458627239142 2 8.832255827154583
		 3 8.7416657915852536 4 8.016945507030611 5 7.2922252224759658 6 7.1110451513373061
		 7 7.2016351869066355 8 7.2922252224759658 9 8.016945507030611 10 8.7416657915852536
		 11 8.9228458627239142;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0.92189639806747437 0.99887698888778687 
		0.99887698888778687 0.93495029211044312 0.93495029211044312 0.99553036689758301 0.99887698888778687 
		0.99887698888778687 0.93495029211044312 0.93495029211044312 0.99553036689758301;
	setAttr -s 11 ".kiy[0:10]"  0.38743641972541809 -0.047379560768604279 
		-0.047379564493894577 -0.35477864742279053 -0.3547787070274353 -0.094441652297973633 
		0.047379564493894577 0.047379542142152786 0.3547787070274353 0.3547787070274353 0.094441652297973633;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -23.632634767597757 2 -24.300765270546293
		 3 -24.968895773494832 4 -30.313939797083115 5 -35.658983820671395 6 -36.995244826568467
		 7 -36.327114323619931 8 -35.658983820671395 9 -30.313939797083115 10 -24.968895773494829
		 11 -23.632634767597757;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0.52611863613128662 0.94390773773193359 
		0.94390773773193359 0.33647939562797546 0.33647933602333069 0.81936097145080566 0.94390773773193359 
		0.94390773773193359 0.33647933602333069 0.33647933602333069 0.81936097145080566;
	setAttr -s 11 ".kiy[0:10]"  0.85041123628616333 -0.3302094042301178 
		-0.33020943403244019 -0.94169074296951294 -0.94169086217880249 -0.57327795028686523 
		0.33020943403244019 0.33020931482315063 0.94169086217880249 0.94169086217880249 0.57327795028686523;
createNode animCurveTU -n "rigw:rig:FKShoulder1_L_Global";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -10.216077631697251 2 -10.216077631697251
		 3 -10.216077631697251 4 -10.216077631697251 5 -10.216077631697251 6 -10.216077631697251
		 7 -10.216077631697251 8 -10.216077631697251 9 -10.216077631697251 10 -10.216077631697251
		 11 -10.216077631697251;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -12.316062795065106 2 -12.316062795065106
		 3 -12.316062795065106 4 -12.316062795065106 5 -12.316062795065106 6 -12.316062795065106
		 7 -12.316062795065106 8 -12.316062795065106 9 -12.316062795065106 10 -12.316062795065106
		 11 -12.316062795065106;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 23.837782049291409 2 23.61685777781771
		 3 23.395933506344011 4 21.628539334554297 5 19.861145162764679 6 19.419296619817246
		 7 19.640220891290962 8 19.861145162764679 9 21.628539334554297 10 23.395933506344011
		 11 23.837782049291409;
	setAttr -s 11 ".kit[0:10]"  3 2 2 2 2 2 2 2 
		2 2 2;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 -0.37960280222764831 3 -0.75920560445529661
		 4 -3.7960280222764826 5 -6.8328504400976691 6 -7.5920560445529652 7 -7.212453242325318
		 8 -6.8328504400976691 9 -3.7960280222764826 10 -0.75920560445529639 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 0.98081398010253906 0.98081398010253906 
		0.53237134218215942 0.53237122297286987 0.92926955223083496 0.98081398010253906 0.98081398010253906 
		0.53237122297286987 0.53237122297286987 0.92926955223083496;
	setAttr -s 11 ".kiy[0:10]"  0 -0.19494614005088806 -0.19494616985321045 
		-0.84651100635528564 -0.84651100635528564 -0.3694024384021759 0.19494616985321045 
		0.19494608044624329 0.84651100635528564 0.84651100635528564 0.3694024384021759;
createNode animCurveTU -n "rigw:rig:FKShoulder_R_Global";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 4.9960036108132044e-16 2 0 3 0 4 0 5 0
		 6 0 7 0 8 0 9 0 10 0 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -1.7763568394002505e-15 2 0 3 0 4 0 5 0
		 6 0 7 0 8 0 9 0 10 0 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 1.0438294766492273 3 1.2530115448195853
		 4 1.4621936129899431 5 1.6713756811603009 6 1.8805577493306589 8 1.6713756811603009
		 9 1.4621936129899429 10 1.2530115448195849 11 1.0438294766492273;
	setAttr -s 9 ".kit[0:8]"  1 2 2 2 2 2 2 2 
		2;
	setAttr -s 9 ".kix[0:8]"  0 0.30365335941314697 0.15736539661884308 
		0.15736536681652069 0.15736536681652069 0.30365341901779175 0.15736536681652069 0.15736536681652069 
		0.15736536681652069;
	setAttr -s 9 ".kiy[0:8]"  0 0.95278257131576538 0.98754042387008667 
		0.98754042387008667 0.98754042387008667 -0.95278257131576538 -0.98754042387008667 
		-0.98754042387008667 -0.98754042387008667;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0.08451261838265392 3 0.10998610748546908
		 4 0.13545959658828424 5 0.1609330856910994 6 0.18640657479391456 8 0.1609330856910994
		 9 0.13545959658828424 10 0.10998610748546908 11 0.08451261838265392;
	setAttr -s 9 ".kit[0:8]"  1 2 2 2 2 2 2 2 
		2;
	setAttr -s 9 ".kix[0:8]"  0 0.93412989377975464 0.79455000162124634 
		0.79454994201660156 0.79454994201660156 0.93412989377975464 0.79454994201660156 0.79454994201660156 
		0.79454994201660156;
	setAttr -s 9 ".kiy[0:8]"  0 0.35693323612213135 0.60719871520996094 
		0.60719877481460571 0.60719877481460571 -0.35693314671516418 -0.60719877481460571 
		-0.60719877481460571 -0.60719877481460571;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0.12829119479210227 3 -0.33060496594546024
		 4 -0.78950112668302275 5 -1.2483972874205853 6 -1.7072934481581479 8 -1.2483972874205853
		 9 -0.78950112668302275 10 -0.33060496594546024 11 0.12829119479210227;
	setAttr -s 9 ".kot[0:8]"  5 5 5 5 5 5 5 5 
		5;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 1.4355168762344563 3 1.4355168762344563
		 4 1.4355168762344563 5 1.47177887375878 6 1.5080408712831037 8 1.47177887375878 9 1.4355168762344563
		 10 1.4355168762344563 11 1.4355168762344563;
	setAttr -s 9 ".kit[0:8]"  1 2 2 2 2 2 2 2 
		2;
	setAttr -s 9 ".kix[0:8]"  0 1 1 0.67675179243087769 0.67675179243087769 
		0.87845796346664429 0.67675179243087769 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0.73621124029159546 0.73621124029159546 
		-0.4778195321559906 -0.73621124029159546 0 0;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 -0.61952478608195372 3 -0.61952480557654288
		 4 -0.61952482507113205 5 -1.3081342463437911 6 -1.9967436676164501 8 -1.3081342463437911
		 9 -0.61952482507113205 10 -0.61952480557654288 11 -0.61952478608195372;
	setAttr -s 9 ".kot[0:8]"  5 5 5 5 5 5 5 5 
		5;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 -0.45236972036464546 3 -0.0063855202306331293
		 4 0.4395986799033792 5 0.53287652308684474 6 0.62615436627031029 8 0.53287652308684474
		 9 0.4395986799033792 10 -0.0063855202306331293 11 -0.45236972036464546;
	setAttr -s 9 ".kot[0:8]"  5 5 5 5 5 5 5 5 
		5;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 3 0 4 0 5 20.045183729354576 6 40.090367458709153
		 8 20.045183729354576 9 0 10 0 11 0;
	setAttr -s 9 ".kot[0:8]"  5 5 5 5 5 5 5 5 
		5;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 -14.128420092077203 3 -15.88391030220604
		 4 -17.639400512334877 5 -18.79944136915039 6 -19.959482225965903 8 -18.79944136915039
		 9 -17.639400512334877 10 -15.88391030220604 11 -14.128420092077203;
	setAttr -s 9 ".kot[0:8]"  5 5 5 5 5 5 5 5 
		5;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 3 0 4 0 5 19.604847722961424 6 39.209695445922847
		 8 19.604847722961424 9 0 10 0 11 0;
	setAttr -s 9 ".kot[0:8]"  5 5 5 5 5 5 5 5 
		5;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 3 0 4 0 5 0 6 0 8 0 9 0 10 0 11 0;
	setAttr -s 9 ".kit[0:8]"  1 2 2 2 2 2 2 2 
		2;
	setAttr -s 9 ".kix[0:8]"  0 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 -3.2534834846123113 3 -8.4621315500108398
		 4 -13.670779615409369 5 -18.879427680807904 6 -24.088075746206428 8 -18.879427680807904
		 9 -13.670779615409369 10 -8.4621315500108398 11 -3.2534834846123113;
	setAttr -s 9 ".kit[0:8]"  1 2 2 2 2 2 2 2 
		2;
	setAttr -s 9 ".kix[0:8]"  0 0.59136801958084106 0.34425812959671021 
		0.34425806999206543 0.34425806999206543 0.59136807918548584 0.34425806999206543 0.34425806999206543 
		0.34425806999206543;
	setAttr -s 9 ".kiy[0:8]"  0 -0.80640178918838501 -0.93887501955032349 
		-0.93887507915496826 -0.93887507915496826 0.80640166997909546 0.93887507915496826 
		0.93887507915496826 0.93887507915496826;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  1 0 3 0 4 0 5 0 6 0 8 0 9 0 10 0 11 0;
	setAttr -s 9 ".kit[0:8]"  1 2 2 2 2 2 2 2 
		2;
	setAttr -s 9 ".kix[0:8]"  0 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 -2.410603891341474 3 -4.821207782682948
		 4 -6.9787832948017456 5 -4.7268463193978514 6 0 7 2.2183089745160074 8 4.4366179490320148
		 9 5.9716390966147124 10 2.7886697368518729 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0.95178627967834473 0.62099570035934448 
		0.62099558115005493 0.66281437873840332 0.64680510759353638 0.37462162971496582 0.6524541974067688 
		0.65245437622070312 0.77944660186767578 0.51451116800308228 0.56505101919174194;
	setAttr -s 11 ".kiy[0:10]"  -0.30676189064979553 -0.78381401300430298 
		-0.78381401300430298 -0.74878382682800293 0.76265537738800049 0.92717772722244263 
		0.75782811641693115 0.7578279972076416 0.6264687180519104 -0.85748374462127686 -0.82505595684051514;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  3 2 2 2 2 2 2 2 
		2 2 2;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  3 2 2 2 2 2 2 2 
		2 2 2;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  3 2 2 2 2 2 2 2 
		2 2 2;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  3 2 2 2 2 2 2 2 
		2 2 2;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 -0.39812789775738194 3 -0.79625579551476389
		 4 -1.4787607630988517 5 -2.5025182144749789 6 -3.0143969401630448 7 -2.4740805074923102
		 8 -1.9337640748215752 9 -1.3780100297888185 10 -0.74068039101149052 11 0;
	setAttr -s 11 ".kit[0:10]"  3 2 2 2 2 2 2 2 
		2 2 2;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  3 2 2 2 2 2 2 2 
		2 2 2;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -33.363690476608248 2 -32.587759086145205
		 3 -31.811827695682158 4 -25.604376571977763 5 -19.396925448273375 6 -17.845062667347278
		 7 -18.620994057810329 8 -19.396925448273379 9 -25.604376571977763 10 -31.811827695682151
		 11 -33.363690476608248;
	setAttr -s 11 ".kit[0:10]"  1 2 2 2 2 2 2 2 
		2 2 2;
	setAttr -s 11 ".kix[0:10]"  0.5174863338470459 0.92645800113677979 
		0.92645788192749023 0.29406821727752686 0.29406815767288208 0.77609366178512573 0.92645788192749023 
		0.92645800113677979 0.29406815767288208 0.29406815767288208 0.77609366178512573;
	setAttr -s 11 ".kiy[0:10]"  0.85569143295288086 0.37639829516410828 
		0.37639829516410828 0.9557843804359436 0.95578444004058838 0.63061761856079102 -0.37639829516410828 
		-0.37639817595481873 -0.95578444004058838 -0.95578444004058838 -0.63061761856079102;
createNode mia_exposure_simple -n "mia_exposure_simple1";
	setAttr ".S02" 1;
	setAttr ".S04" 1.1499999761581421;
createNode animCurveTU -n "rigw:robo85_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:robo84_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:robo105_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:robo106_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:robo57_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:robo58_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:robo77_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:robo78_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 30 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.086017200000000002 2 0.086017200000000002
		 3 0.086017200000000002 4 0.086017200000000002 5 0.086017200000000002 6 0.086017200000000002
		 7 0.086017200000000002 8 0.086017200000000002 9 0.086017200000000002 10 0.086017200000000002
		 11 0.086017200000000002;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.131701 2 0.131701 3 0.131701 4 0.131701
		 5 0.131701 6 0.131701 7 0.131701 8 0.131701 9 0.131701 10 0.131701 11 0.131701;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0.25038274999999999 3 0.50076549999999997
		 4 2.5038274999999999 5 4.5068894999999998 6 5.007655 7 4.7572722500000006 8 4.5068894999999998
		 9 2.5038274999999999 10 0.50076549999999986 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0.2555115 3 0.511023 4 2.555115 5 4.599207
		 6 5.11023 7 4.8547184999999997 8 4.599207 9 2.555115 10 0.51102299999999989 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0.57309620000000006 3 1.1461924000000001
		 4 5.730962 5 10.315731600000001 6 11.461924 7 10.888827800000001 8 10.315731600000001
		 9 5.730962 10 1.1461923999999997 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 -0.56612436786877962 4 -1.6983900875072138
		 5 -3.3967801750144275 6 -4.2459752187680344 7 -3.3967801750144275 8 -2.2645314392768685
		 9 -1.1322657196384343 10 -0.37742945498427566 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -22.028843 2 -22.028843000000002 3 -22.028843000000002
		 4 -22.028843 5 -22.028843 6 -22.028843 7 -22.028843000000002 8 -22.028843000000002
		 9 -22.028843 10 -22.028843 11 -22.028843;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -16.267797000000005 2 -16.267797000000005
		 3 -16.267797000000005 4 -16.267797000000005 5 -16.267797000000005 6 -16.267797000000005
		 7 -16.267797000000005 8 -16.267797000000005 9 -16.267797000000005 10 -16.267797000000005
		 11 -16.267797000000005;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -39.607339 2 -39.607339 3 -39.607339 4 -39.607339
		 5 -39.607339 6 -39.607339 7 -39.607339 8 -39.607339 9 -39.607339 10 -39.607339 11 -39.607339;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.609696 2 -0.609696 3 -0.609696 4 -0.609696
		 5 -0.609696 6 -0.609696 7 -0.609696 8 -0.609696 9 -0.609696 10 -0.609696 11 -0.609696;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_roll";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_rollAngle";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 25 2 25 3 25 4 25 5 25 6 25 7 25 8 25
		 9 25 10 25 11 25;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegBall_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:PoleLeg_R_follow";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10 8 10
		 9 10 10 10 11 10;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.571318 2 0.571318 3 0.571318 4 0.571318
		 5 0.571318 6 0.571318 7 0.571318 8 0.571318 9 0.571318 10 0.571318 11 0.571318;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_roll";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_rollAngle";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 25 2 25 3 25 4 25 5 25 6 25 7 25 8 25
		 9 25 10 25 11 25;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:IKLegBall_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:PoleLeg_L_follow";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10 8 10
		 9 10 10 10 11 10;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKIKBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10 8 10
		 9 10 10 10 11 10;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKIKBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10 8 10
		 9 10 10 10 11 10;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:Main_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:Main_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:Main_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:rig:Main_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:Main_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:Main_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:rig:Main_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:Main_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.8 2 1.8000000000000003 3 1.8000000000000003
		 4 1.8 5 1.8 6 1.8 7 1.8000000000000003 8 1.8000000000000003 9 1.8 10 1.8 11 1.8;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:Main_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.8 2 1.8000000000000003 3 1.8000000000000003
		 4 1.8 5 1.8 6 1.8 7 1.8000000000000003 8 1.8000000000000003 9 1.8 10 1.8 11 1.8;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:rig:Main_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.8 2 1.8000000000000003 3 1.8000000000000003
		 4 1.8 5 1.8 6 1.8 7 1.8000000000000003 8 1.8000000000000003 9 1.8 10 1.8 11 1.8;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo102_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo102_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo102_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.156221 2 2.156221 3 2.156221 4 2.156221
		 5 2.156221 6 2.156221 7 2.156221 8 2.156221 9 2.156221 10 2.156221 11 2.156221;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo102_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.374191 2 3.374191 3 3.374191 4 3.374191
		 5 3.374191 6 3.374191 7 3.374191 8 3.374191 9 3.374191 10 3.374191 11 3.374191;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo102_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 152.82637 2 152.82637 3 152.82637 4 152.82637
		 5 152.82637 6 152.82637 7 152.82637 8 152.82637 9 152.82637 10 152.82637 11 152.82637;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo102_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo102_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo102_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo102_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo102_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo101_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo101_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo101_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.662469 2 1.662469 3 1.662469 4 1.662469
		 5 1.662469 6 1.662469 7 1.662469 8 1.662469 9 1.662469 10 1.662469 11 1.662469;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo101_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 4.221696 2 4.221696 3 4.221696 4 4.221696
		 5 4.221696 6 4.221696 7 4.221696 8 4.221696 9 4.221696 10 4.221696 11 4.221696;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo101_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 148.612692 2 148.61269200000004 3 148.61269200000004
		 4 148.612692 5 148.612692 6 148.612692 7 148.61269200000004 8 148.61269200000004
		 9 148.612692 10 148.612692 11 148.612692;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo101_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo101_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo101_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo101_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo101_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo100_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo100_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo100_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.974319 2 0.97431900000000016 3 0.97431900000000016
		 4 0.974319 5 0.97431900000000016 6 0.974319 7 0.97431900000000016 8 0.97431900000000016
		 9 0.974319 10 0.97431900000000016 11 0.974319;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo100_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 4.426617 2 4.426617 3 4.426617 4 4.426617
		 5 4.426617 6 4.426617 7 4.426617 8 4.426617 9 4.426617 10 4.426617 11 4.426617;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo100_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 74.561257 2 74.561257 3 74.561257 4 74.561257
		 5 74.561257 6 74.561257 7 74.561257 8 74.561257 9 74.561257 10 74.561257 11 74.561257;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo100_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo100_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo100_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo100_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo100_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo99_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo99_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo99_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo99_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.904841 2 3.9048410000000002 3 3.9048410000000002
		 4 3.904841 5 3.904841 6 3.904841 7 3.9048410000000002 8 3.9048410000000002 9 3.904841
		 10 3.904841 11 3.904841;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo99_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo99_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo99_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo99_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo99_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo99_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo98_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo98_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo98_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo98_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.927405 2 2.9274050000000003 3 2.9274050000000003
		 4 2.927405 5 2.927405 6 2.927405 7 2.9274050000000003 8 2.9274050000000003 9 2.927405
		 10 2.927405 11 2.927405;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo98_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo98_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo98_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo98_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo98_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo98_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo97_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo97_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo97_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo97_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.949969 2 1.9499690000000003 3 1.9499690000000003
		 4 1.949969 5 1.9499690000000003 6 1.949969 7 1.9499690000000003 8 1.9499690000000003
		 9 1.949969 10 1.9499690000000003 11 1.949969;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo97_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo97_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo97_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo97_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo97_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo97_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo96_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo96_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo96_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo96_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.972532 2 0.972532 3 0.972532 4 0.972532
		 5 0.972532 6 0.972532 7 0.972532 8 0.972532 9 0.972532 10 0.972532 11 0.972532;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo96_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo96_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo96_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo96_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo96_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo96_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo95_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo95_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo95_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo95_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.0049039299999999999 2 -0.0049039299999999999
		 3 -0.0049039299999999999 4 -0.0049039299999999999 5 -0.0049039299999999999 6 -0.0049039299999999999
		 7 -0.0049039299999999999 8 -0.0049039299999999999 9 -0.0049039299999999999 10 -0.0049039299999999999
		 11 -0.0049039299999999999;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo95_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo95_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo95_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo95_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo95_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo95_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo94_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo94_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo94_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo94_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.98234 2 -0.98234 3 -0.98234 4 -0.98234
		 5 -0.98234 6 -0.98234 7 -0.98234 8 -0.98234 9 -0.98234 10 -0.98234 11 -0.98234;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo94_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo94_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo94_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo94_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo94_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo94_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo93_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo93_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo93_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo93_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -1.959776 2 -1.959776 3 -1.959776 4 -1.959776
		 5 -1.959776 6 -1.959776 7 -1.959776 8 -1.959776 9 -1.959776 10 -1.959776 11 -1.959776;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo93_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo93_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo93_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo93_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo93_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo93_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo92_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo92_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo92_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo92_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -2.937213 2 -2.937213 3 -2.9372130000000003
		 4 -2.937213 5 -2.937213 6 -2.937213 7 -2.937213 8 -2.9372130000000003 9 -2.937213
		 10 -2.937213 11 -2.937213;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo92_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo92_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo92_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo92_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo92_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo92_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo91_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo91_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo91_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo91_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -3.914649 2 -3.914649 3 -3.914649 4 -3.914649
		 5 -3.914649 6 -3.914649 7 -3.914649 8 -3.914649 9 -3.914649 10 -3.914649 11 -3.914649;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo91_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo91_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo91_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo91_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo91_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo91_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo90_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo90_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo90_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo90_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -4.892085 2 -4.892085 3 -4.892085 4 -4.892085
		 5 -4.892085 6 -4.892085 7 -4.892085 8 -4.892085 9 -4.892085 10 -4.892085 11 -4.892085;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo90_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo90_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo90_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo90_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo90_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo90_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo89_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo89_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo89_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.664251 2 0.664251 3 0.664251 4 0.664251
		 5 0.66425099999999992 6 0.664251 7 0.664251 8 0.664251 9 0.664251 10 0.66425099999999992
		 11 0.664251;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo89_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -5.529688 2 -5.529688 3 -5.529688 4 -5.529688
		 5 -5.529688 6 -5.529688 7 -5.529688 8 -5.529688 9 -5.529688 10 -5.529688 11 -5.529688;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo89_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -78.605434 2 -78.605434 3 -78.605434 4 -78.605434
		 5 -78.605434 6 -78.605434 7 -78.605434 8 -78.605434 9 -78.605434 10 -78.605434 11 -78.605434;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo89_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo89_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo89_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo89_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo89_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo88_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo88_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo88_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.474073 2 1.474073 3 1.474073 4 1.474073
		 5 1.4740729999999997 6 1.474073 7 1.474073 8 1.474073 9 1.474073 10 1.4740729999999997
		 11 1.474073;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo88_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -5.499763 2 -5.499763 3 -5.499763 4 -5.499763
		 5 -5.499763 6 -5.499763 7 -5.499763 8 -5.499763 9 -5.499763 10 -5.499763 11 -5.499763;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo88_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 27.443690000000004 2 27.443690000000004
		 3 27.443690000000007 4 27.443690000000004 5 27.443690000000004 6 27.443690000000004
		 7 27.443690000000004 8 27.443690000000007 9 27.443690000000004 10 27.443690000000004
		 11 27.443690000000004;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo88_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo88_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo88_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo88_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo88_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo87_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo87_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo87_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.924439 2 1.924439 3 1.924439 4 1.924439
		 5 1.924439 6 1.924439 7 1.924439 8 1.924439 9 1.924439 10 1.924439 11 1.924439;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo87_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -4.632539 2 -4.632539 3 -4.632539 4 -4.632539
		 5 -4.632539 6 -4.632539 7 -4.632539 8 -4.632539 9 -4.632539 10 -4.632539 11 -4.632539;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo87_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 27.443690000000004 2 27.443690000000004
		 3 27.443690000000007 4 27.443690000000004 5 27.443690000000004 6 27.443690000000004
		 7 27.443690000000004 8 27.443690000000007 9 27.443690000000004 10 27.443690000000004
		 11 27.443690000000004;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo87_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo87_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo87_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo87_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo87_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo86_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo86_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo86_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.374804 2 2.374804 3 2.374804 4 2.374804
		 5 2.374804 6 2.374804 7 2.374804 8 2.374804 9 2.374804 10 2.374804 11 2.374804;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo86_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -3.765316 2 -3.7653160000000003 3 -3.7653160000000003
		 4 -3.765316 5 -3.765316 6 -3.765316 7 -3.7653160000000003 8 -3.7653160000000003 9 -3.765316
		 10 -3.765316 11 -3.765316;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo86_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 27.443690000000004 2 27.443690000000004
		 3 27.443690000000007 4 27.443690000000004 5 27.443690000000004 6 27.443690000000004
		 7 27.443690000000004 8 27.443690000000007 9 27.443690000000004 10 27.443690000000004
		 11 27.443690000000004;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo86_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo86_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo86_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo86_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo86_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo107_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo107_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo107_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.818561 2 2.818561 3 2.818561 4 2.818561
		 5 2.818561 6 2.818561 7 2.818561 8 2.818561 9 2.818561 10 2.818561 11 2.818561;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo107_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -2.910817 2 -2.9108170000000007 3 -2.9108170000000007
		 4 -2.910817 5 -2.910817 6 -2.910817 7 -2.9108170000000007 8 -2.9108170000000007 9 -2.910817
		 10 -2.910817 11 -2.910817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo107_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 27.443690000000004 2 27.443690000000004
		 3 27.443690000000007 4 27.443690000000004 5 27.443690000000004 6 27.443690000000004
		 7 27.443690000000004 8 27.443690000000007 9 27.443690000000004 10 27.443690000000004
		 11 27.443690000000004;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo107_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo107_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo107_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo107_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo107_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo108_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo108_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo108_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.050656 2 3.050656 3 3.0506560000000005
		 4 3.050656 5 3.050656 6 3.050656 7 3.050656 8 3.0506560000000005 9 3.050656 10 3.050656
		 11 3.050656;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo108_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -2.12289 2 -2.12289 3 -2.12289 4 -2.12289
		 5 -2.12289 6 -2.12289 7 -2.12289 8 -2.12289 9 -2.12289 10 -2.12289 11 -2.12289;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo108_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo108_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo108_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo108_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo108_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo108_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo109_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo109_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo109_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.050656 2 3.050656 3 3.0506560000000005
		 4 3.050656 5 3.050656 6 3.050656 7 3.050656 8 3.0506560000000005 9 3.050656 10 3.050656
		 11 3.050656;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo109_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -1.150227 2 -1.150227 3 -1.150227 4 -1.150227
		 5 -1.150227 6 -1.150227 7 -1.150227 8 -1.150227 9 -1.150227 10 -1.150227 11 -1.150227;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo109_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo109_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo109_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo109_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo109_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo109_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo110_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo110_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo110_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.050656 2 3.050656 3 3.0506560000000005
		 4 3.050656 5 3.050656 6 3.050656 7 3.050656 8 3.0506560000000005 9 3.050656 10 3.050656
		 11 3.050656;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo110_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.177564 2 -0.177564 3 -0.17756400000000003
		 4 -0.177564 5 -0.177564 6 -0.177564 7 -0.177564 8 -0.17756400000000003 9 -0.177564
		 10 -0.177564 11 -0.177564;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo110_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo110_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo110_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo110_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo110_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo110_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo111_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo111_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo111_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.050656 2 3.050656 3 3.0506560000000005
		 4 3.050656 5 3.050656 6 3.050656 7 3.050656 8 3.0506560000000005 9 3.050656 10 3.050656
		 11 3.050656;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo111_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.795099 2 0.795099 3 0.79509900000000011
		 4 0.795099 5 0.795099 6 0.795099 7 0.795099 8 0.79509900000000011 9 0.795099 10 0.795099
		 11 0.795099;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo111_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo111_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo111_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo111_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo111_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo111_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo104_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo104_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.741585 2 -0.741585 3 -0.741585 4 -0.741585
		 5 -0.74158499999999994 6 -0.741585 7 -0.741585 8 -0.741585 9 -0.741585 10 -0.74158499999999994
		 11 -0.741585;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo104_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.953546 2 2.953546 3 2.953546 4 2.953546
		 5 2.9535459999999993 6 2.953546 7 2.953546 8 2.953546 9 2.953546 10 2.9535459999999993
		 11 2.953546;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo104_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.639622 2 1.6396220000000001 3 1.6396220000000001
		 4 1.639622 5 1.639622 6 1.639622 7 1.6396220000000001 8 1.6396220000000001 9 1.639622
		 10 1.639622 11 1.639622;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo104_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 161.793326 2 161.79332600000004 3 161.79332600000004
		 4 161.793326 5 161.793326 6 161.793326 7 161.79332600000004 8 161.79332600000004
		 9 161.793326 10 161.793326 11 161.793326;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo104_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.05450590000000001 2 0.05450590000000001
		 3 0.054505900000000017 4 0.05450590000000001 5 0.05450590000000001 6 0.05450590000000001
		 7 0.05450590000000001 8 0.054505900000000017 9 0.05450590000000001 10 0.05450590000000001
		 11 0.05450590000000001;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo104_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -179.49349 2 -179.49349 3 -179.49349 4 -179.49349
		 5 -179.49349 6 -179.49349 7 -179.49349 8 -179.49349 9 -179.49349 10 -179.49349 11 -179.49349;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo104_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo104_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo104_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo103_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo103_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596 8 -0.740596 9 -0.740596 10 -0.740596 11 -0.740596;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo103_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.603864 2 2.603864 3 2.603864 4 2.603864
		 5 2.603864 6 2.603864 7 2.603864 8 2.603864 9 2.603864 10 2.603864 11 2.603864;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo103_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.502186 2 2.502186 3 2.502186 4 2.502186
		 5 2.502186 6 2.502186 7 2.502186 8 2.502186 9 2.502186 10 2.502186 11 2.502186;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo103_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 152.82637 2 152.82637 3 152.82637 4 152.82637
		 5 152.82637 6 152.82637 7 152.82637 8 152.82637 9 152.82637 10 152.82637 11 152.82637;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo103_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo103_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo103_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.894 2 0.89400000000000013 3 0.89400000000000013
		 4 0.894 5 0.894 6 0.894 7 0.89400000000000013 8 0.89400000000000013 9 0.894 10 0.894
		 11 0.894;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo103_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo103_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.893817 2 -0.89381700000000008 3 -0.89381700000000008
		 4 -0.893817 5 -0.89381700000000008 6 -0.893817 7 -0.89381700000000008 8 -0.89381700000000008
		 9 -0.893817 10 -0.89381700000000008 11 -0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo72_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo72_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo72_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.715486 2 0.71548600000000007 3 0.71548600000000007
		 4 0.715486 5 0.715486 6 0.715486 7 0.71548600000000007 8 0.71548600000000007 9 0.715486
		 10 0.715486 11 0.715486;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo72_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 4.165678 2 4.165678 3 4.165678 4 4.165678
		 5 4.165678 6 4.165678 7 4.165678 8 4.165678 9 4.165678 10 4.165678 11 4.165678;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo72_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 112.246345 2 112.246345 3 112.246345 4 112.246345
		 5 112.246345 6 112.246345 7 112.246345 8 112.246345 9 112.246345 10 112.246345 11 112.246345;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo72_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo72_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo72_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo72_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo72_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo73_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo73_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo73_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.248661 2 1.248661 3 1.248661 4 1.248661
		 5 1.248661 6 1.248661 7 1.248661 8 1.248661 9 1.248661 10 1.248661 11 1.248661;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo73_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.934626 2 3.9346260000000006 3 3.9346260000000006
		 4 3.934626 5 3.934626 6 3.934626 7 3.9346260000000006 8 3.9346260000000006 9 3.934626
		 10 3.934626 11 3.934626;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo73_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 87.636758 2 87.636758 3 87.636758 4 87.636758
		 5 87.636757999999986 6 87.636758 7 87.636758 8 87.636758 9 87.636758 10 87.636757999999986
		 11 87.636758;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo73_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo73_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo73_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo73_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo73_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo74_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo74_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo74_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.006366 2 2.006366 3 2.006366 4 2.006366
		 5 2.006366 6 2.006366 7 2.006366 8 2.006366 9 2.006366 10 2.006366 11 2.006366;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo74_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.876286 2 3.876286 3 3.876286 4 3.876286
		 5 3.8762859999999995 6 3.876286 7 3.876286 8 3.876286 9 3.876286 10 3.8762859999999995
		 11 3.876286;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo74_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 27.173630000000003 2 27.173630000000003
		 3 27.173630000000006 4 27.173630000000003 5 27.173630000000003 6 27.173630000000003
		 7 27.173630000000003 8 27.173630000000006 9 27.173630000000003 10 27.173630000000003
		 11 27.173630000000003;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo74_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo74_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo74_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo74_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo74_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo75_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo75_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo75_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.443066 2 2.443066 3 2.443066 4 2.443066
		 5 2.443066 6 2.443066 7 2.443066 8 2.443066 9 2.443066 10 2.443066 11 2.443066;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo75_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.998664 2 2.998664 3 2.998664 4 2.998664
		 5 2.998664 6 2.998664 7 2.998664 8 2.998664 9 2.998664 10 2.998664 11 2.998664;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo75_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 27.173630000000003 2 27.173630000000003
		 3 27.173630000000006 4 27.173630000000003 5 27.173630000000003 6 27.173630000000003
		 7 27.173630000000003 8 27.173630000000006 9 27.173630000000003 10 27.173630000000003
		 11 27.173630000000003;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo75_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo75_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo75_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo75_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo75_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo76_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo76_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740572 2 0.740572 3 0.740572 4 0.740572
		 5 0.740572 6 0.740572 7 0.740572 8 0.740572 9 0.740572 10 0.740572 11 0.740572;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo76_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.802991 2 2.8029910000000005 3 2.8029910000000005
		 4 2.802991 5 2.802991 6 2.802991 7 2.8029910000000005 8 2.8029910000000005 9 2.802991
		 10 2.802991 11 2.802991;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo76_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.134471 2 2.134471 3 2.134471 4 2.134471
		 5 2.134471 6 2.134471 7 2.134471 8 2.134471 9 2.134471 10 2.134471 11 2.134471;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo76_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 28.003616 2 28.003616 3 28.003616000000005
		 4 28.003616 5 28.003616 6 28.003616 7 28.003616 8 28.003616000000005 9 28.003616
		 10 28.003616 11 28.003616;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo76_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.054517 2 0.054517 3 0.054517 4 0.054517
		 5 0.054517 6 0.054517 7 0.054517 8 0.054517 9 0.054517 10 0.054517 11 0.054517;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo76_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.506614 2 -0.506614 3 -0.506614 4 -0.506614
		 5 -0.506614 6 -0.506614 7 -0.506614 8 -0.506614 9 -0.506614 10 -0.506614 11 -0.506614;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo76_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo76_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo76_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo83_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo83_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo83_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.03787 2 3.03787 3 3.0378700000000003
		 4 3.03787 5 3.03787 6 3.03787 7 3.03787 8 3.0378700000000003 9 3.03787 10 3.03787
		 11 3.03787;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo83_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.259282 2 1.2592820000000002 3 1.2592820000000002
		 4 1.259282 5 1.259282 6 1.259282 7 1.2592820000000002 8 1.2592820000000002 9 1.259282
		 10 1.259282 11 1.259282;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo83_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 4.484307 2 4.4843070000000012 3 4.4843070000000012
		 4 4.484307 5 4.484307 6 4.484307 7 4.4843070000000012 8 4.4843070000000012 9 4.484307
		 10 4.484307 11 4.484307;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo83_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo83_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo83_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo83_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo83_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo82_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo82_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo82_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.050656 2 3.050656 3 3.0506560000000005
		 4 3.050656 5 3.050656 6 3.050656 7 3.050656 8 3.0506560000000005 9 3.050656 10 3.050656
		 11 3.050656;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo82_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.33016 2 0.33016 3 0.33016 4 0.33016
		 5 0.33016 6 0.33016 7 0.33016 8 0.33016 9 0.33016 10 0.33016 11 0.33016;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo82_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo82_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo82_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo82_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo82_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo82_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo81_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo81_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo81_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.050656 2 3.050656 3 3.0506560000000005
		 4 3.050656 5 3.050656 6 3.050656 7 3.050656 8 3.0506560000000005 9 3.050656 10 3.050656
		 11 3.050656;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo81_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.642503 2 -0.642503 3 -0.642503 4 -0.642503
		 5 -0.642503 6 -0.642503 7 -0.642503 8 -0.642503 9 -0.642503 10 -0.642503 11 -0.642503;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo81_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo81_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo81_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo81_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo81_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo81_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo80_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo80_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo80_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.050656 2 3.050656 3 3.0506560000000005
		 4 3.050656 5 3.050656 6 3.050656 7 3.050656 8 3.0506560000000005 9 3.050656 10 3.050656
		 11 3.050656;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo80_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -1.615166 2 -1.615166 3 -1.615166 4 -1.615166
		 5 -1.615166 6 -1.615166 7 -1.615166 8 -1.615166 9 -1.615166 10 -1.615166 11 -1.615166;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo80_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo80_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo80_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo80_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo80_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo80_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo79_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo79_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo79_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.829374 2 2.829374 3 2.829374 4 2.829374
		 5 2.829374 6 2.829374 7 2.829374 8 2.829374 9 2.829374 10 2.829374 11 2.829374;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo79_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -2.525288 2 -2.5252880000000006 3 -2.5252880000000006
		 4 -2.525288 5 -2.525288 6 -2.525288 7 -2.5252880000000006 8 -2.5252880000000006 9 -2.525288
		 10 -2.525288 11 -2.525288;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo79_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -23.380861 2 -23.380861 3 -23.380861 4 -23.380861
		 5 -23.380861 6 -23.380861 7 -23.380861 8 -23.380861 9 -23.380861 10 -23.380861 11 -23.380861;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo79_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo79_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo79_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo79_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo79_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo58_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo58_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo58_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.562667 2 2.5626670000000003 3 2.5626670000000003
		 4 2.562667 5 2.562667 6 2.562667 7 2.5626670000000003 8 2.5626670000000003 9 2.562667
		 10 2.562667 11 2.562667;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo58_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -3.403567 2 -3.403567 3 -3.4035670000000002
		 4 -3.403567 5 -3.403567 6 -3.403567 7 -3.403567 8 -3.4035670000000002 9 -3.403567
		 10 -3.403567 11 -3.403567;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo58_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -27.443690000000004 2 -27.443690000000004
		 3 -27.443690000000007 4 -27.443690000000004 5 -27.443690000000004 6 -27.443690000000004
		 7 -27.443690000000004 8 -27.443690000000007 9 -27.443690000000004 10 -27.443690000000004
		 11 -27.443690000000004;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo58_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo58_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo58_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo58_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo58_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo59_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo59_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo59_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.112302 2 2.1123020000000006 3 2.1123020000000006
		 4 2.112302 5 2.112302 6 2.112302 7 2.1123020000000006 8 2.1123020000000006 9 2.112302
		 10 2.112302 11 2.112302;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo59_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -4.27079 2 -4.27079 3 -4.27079 4 -4.27079
		 5 -4.27079 6 -4.27079 7 -4.27079 8 -4.27079 9 -4.27079 10 -4.27079 11 -4.27079;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo59_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -27.443690000000004 2 -27.443690000000004
		 3 -27.443690000000007 4 -27.443690000000004 5 -27.443690000000004 6 -27.443690000000004
		 7 -27.443690000000004 8 -27.443690000000007 9 -27.443690000000004 10 -27.443690000000004
		 11 -27.443690000000004;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo59_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo59_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo59_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo59_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo59_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo60_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo60_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo60_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.661936 2 1.6619360000000003 3 1.6619360000000003
		 4 1.661936 5 1.661936 6 1.661936 7 1.6619360000000003 8 1.6619360000000003 9 1.661936
		 10 1.661936 11 1.661936;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo60_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -5.138014 2 -5.138014 3 -5.138014000000001
		 4 -5.138014 5 -5.138014 6 -5.138014 7 -5.138014 8 -5.138014000000001 9 -5.138014
		 10 -5.138014 11 -5.138014;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo60_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -27.443690000000004 2 -27.443690000000004
		 3 -27.443690000000007 4 -27.443690000000004 5 -27.443690000000004 6 -27.443690000000004
		 7 -27.443690000000004 8 -27.443690000000007 9 -27.443690000000004 10 -27.443690000000004
		 11 -27.443690000000004;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo60_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo60_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo60_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo60_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo60_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo61_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo61_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo61_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.078102 2 1.078102 3 1.078102 4 1.078102
		 5 1.078102 6 1.078102 7 1.078102 8 1.078102 9 1.078102 10 1.078102 11 1.078102;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo61_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -5.613094 2 -5.613094 3 -5.613094 4 -5.613094
		 5 -5.613094 6 -5.613094 7 -5.613094 8 -5.613094 9 -5.613094 10 -5.613094 11 -5.613094;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo61_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -101.39456600000001 2 -101.39456600000003
		 3 -101.39456600000003 4 -101.39456600000001 5 -101.39456600000003 6 -101.39456600000001
		 7 -101.39456600000003 8 -101.39456600000003 9 -101.39456600000001 10 -101.39456600000003
		 11 -101.39456600000001;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo61_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo61_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo61_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo61_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo61_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo62_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo62_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo62_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.659831 2 0.65983100000000006 3 0.65983100000000006
		 4 0.659831 5 0.659831 6 0.659831 7 0.65983100000000006 8 0.65983100000000006 9 0.659831
		 10 0.659831 11 0.659831;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo62_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -4.957155 2 -4.957155 3 -4.957155 4 -4.957155
		 5 -4.957155 6 -4.957155 7 -4.957155 8 -4.957155 9 -4.957155 10 -4.957155 11 -4.957155;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo62_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 210.809462 2 210.809462 3 210.809462 4 210.809462
		 5 210.809462 6 210.809462 7 210.809462 8 210.809462 9 210.809462 10 210.809462 11 210.809462;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo62_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo62_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo62_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo62_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo62_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo63_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo63_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo63_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo63_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -4.137065 2 -4.137065 3 -4.137065 4 -4.137065
		 5 -4.137065 6 -4.137065 7 -4.137065 8 -4.137065 9 -4.137065 10 -4.137065 11 -4.137065;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo63_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo63_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo63_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo63_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo63_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo63_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo64_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo64_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo64_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo64_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -3.159629 2 -3.159629 3 -3.159629 4 -3.159629
		 5 -3.159629 6 -3.159629 7 -3.159629 8 -3.159629 9 -3.159629 10 -3.159629 11 -3.159629;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo64_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo64_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo64_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo64_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo64_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo64_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo65_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo65_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo65_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo65_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -2.182193 2 -2.182193 3 -2.182193 4 -2.182193
		 5 -2.182193 6 -2.182193 7 -2.182193 8 -2.182193 9 -2.182193 10 -2.182193 11 -2.182193;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo65_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo65_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo65_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo65_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo65_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo65_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo66_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo66_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo66_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo66_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -1.204757 2 -1.2047570000000003 3 -1.2047570000000003
		 4 -1.204757 5 -1.204757 6 -1.204757 7 -1.2047570000000003 8 -1.2047570000000003 9 -1.204757
		 10 -1.204757 11 -1.204757;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo66_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo66_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo66_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo66_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo66_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo66_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo67_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo67_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo67_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo67_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 -0.22732 2 -0.22732000000000002 3 -0.22732000000000002
		 4 -0.22732 5 -0.22732000000000002 6 -0.22732 7 -0.22732000000000002 8 -0.22732000000000002
		 9 -0.22732 10 -0.22732000000000002 11 -0.22732;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo67_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo67_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo67_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo67_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo67_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo67_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo68_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo68_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo68_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo68_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.750116 2 0.750116 3 0.75011600000000012
		 4 0.750116 5 0.750116 6 0.750116 7 0.750116 8 0.75011600000000012 9 0.750116 10 0.750116
		 11 0.750116;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo68_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo68_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo68_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo68_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo68_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo68_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo69_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo69_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo69_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo69_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1.727552 2 1.727552 3 1.727552 4 1.727552
		 5 1.727552 6 1.727552 7 1.727552 8 1.727552 9 1.727552 10 1.727552 11 1.727552;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo69_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo69_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo69_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo69_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo69_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo69_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo70_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo70_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo70_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo70_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 2.704989 2 2.704989 3 2.704989 4 2.704989
		 5 2.704989 6 2.704989 7 2.704989 8 2.704989 9 2.704989 10 2.704989 11 2.704989;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo70_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo70_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo70_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo70_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo70_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo70_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo71_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 1
		 11 1;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo71_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445 8 0.740445 9 0.740445 10 0.740445 11 0.740445;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo71_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482 8 0.360482 9 0.360482 10 0.360482 11 0.360482;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "rigw:mesh:robo71_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 3.682425 2 3.6824250000000003 3 3.6824250000000003
		 4 3.682425 5 3.6824250000000003 6 3.682425 7 3.6824250000000003 8 3.6824250000000003
		 9 3.682425 10 3.6824250000000003 11 3.682425;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo71_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180
		 8 180 9 180 10 180 11 180;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo71_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "rigw:mesh:robo71_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 8 0 9 0 10 0
		 11 0;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo71_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo71_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "rigw:mesh:robo71_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  1 0.893817 2 0.89381700000000008 3 0.89381700000000008
		 4 0.893817 5 0.89381700000000008 6 0.893817 7 0.89381700000000008 8 0.89381700000000008
		 9 0.893817 10 0.89381700000000008 11 0.893817;
	setAttr -s 11 ".kot[0:10]"  5 5 5 5 5 5 5 5 
		5 5 5;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 11;
	setAttr -av ".unw" 11;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 16 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 16 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 32 ".tx";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 47 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w";
	setAttr -av ".h";
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 2 ".sol";
connectAttr "rigw:robo78_visibility.o" "rigwRN.phl[363]";
connectAttr "rigw:robo77_visibility.o" "rigwRN.phl[364]";
connectAttr "rigw:robo57_visibility.o" "rigwRN.phl[365]";
connectAttr "rigw:mesh:robo72_visibility.o" "rigwRN.phl[366]";
connectAttr "rigw:mesh:robo72_translateX.o" "rigwRN.phl[367]";
connectAttr "rigw:mesh:robo72_translateY.o" "rigwRN.phl[368]";
connectAttr "rigw:mesh:robo72_translateZ.o" "rigwRN.phl[369]";
connectAttr "rigw:mesh:robo72_rotateX.o" "rigwRN.phl[370]";
connectAttr "rigw:mesh:robo72_rotateY.o" "rigwRN.phl[371]";
connectAttr "rigw:mesh:robo72_rotateZ.o" "rigwRN.phl[372]";
connectAttr "rigw:mesh:robo72_scaleX.o" "rigwRN.phl[373]";
connectAttr "rigw:mesh:robo72_scaleY.o" "rigwRN.phl[374]";
connectAttr "rigw:mesh:robo72_scaleZ.o" "rigwRN.phl[375]";
connectAttr "rigw:mesh:robo73_visibility.o" "rigwRN.phl[376]";
connectAttr "rigw:mesh:robo73_translateX.o" "rigwRN.phl[377]";
connectAttr "rigw:mesh:robo73_translateY.o" "rigwRN.phl[378]";
connectAttr "rigw:mesh:robo73_translateZ.o" "rigwRN.phl[379]";
connectAttr "rigw:mesh:robo73_rotateX.o" "rigwRN.phl[380]";
connectAttr "rigw:mesh:robo73_rotateY.o" "rigwRN.phl[381]";
connectAttr "rigw:mesh:robo73_rotateZ.o" "rigwRN.phl[382]";
connectAttr "rigw:mesh:robo73_scaleX.o" "rigwRN.phl[383]";
connectAttr "rigw:mesh:robo73_scaleY.o" "rigwRN.phl[384]";
connectAttr "rigw:mesh:robo73_scaleZ.o" "rigwRN.phl[385]";
connectAttr "rigw:mesh:robo74_visibility.o" "rigwRN.phl[386]";
connectAttr "rigw:mesh:robo74_translateX.o" "rigwRN.phl[387]";
connectAttr "rigw:mesh:robo74_translateY.o" "rigwRN.phl[388]";
connectAttr "rigw:mesh:robo74_translateZ.o" "rigwRN.phl[389]";
connectAttr "rigw:mesh:robo74_rotateX.o" "rigwRN.phl[390]";
connectAttr "rigw:mesh:robo74_rotateY.o" "rigwRN.phl[391]";
connectAttr "rigw:mesh:robo74_rotateZ.o" "rigwRN.phl[392]";
connectAttr "rigw:mesh:robo74_scaleX.o" "rigwRN.phl[393]";
connectAttr "rigw:mesh:robo74_scaleY.o" "rigwRN.phl[394]";
connectAttr "rigw:mesh:robo74_scaleZ.o" "rigwRN.phl[395]";
connectAttr "rigw:mesh:robo75_visibility.o" "rigwRN.phl[396]";
connectAttr "rigw:mesh:robo75_translateX.o" "rigwRN.phl[397]";
connectAttr "rigw:mesh:robo75_translateY.o" "rigwRN.phl[398]";
connectAttr "rigw:mesh:robo75_translateZ.o" "rigwRN.phl[399]";
connectAttr "rigw:mesh:robo75_rotateX.o" "rigwRN.phl[400]";
connectAttr "rigw:mesh:robo75_rotateY.o" "rigwRN.phl[401]";
connectAttr "rigw:mesh:robo75_rotateZ.o" "rigwRN.phl[402]";
connectAttr "rigw:mesh:robo75_scaleX.o" "rigwRN.phl[403]";
connectAttr "rigw:mesh:robo75_scaleY.o" "rigwRN.phl[404]";
connectAttr "rigw:mesh:robo75_scaleZ.o" "rigwRN.phl[405]";
connectAttr "rigw:mesh:robo76_visibility.o" "rigwRN.phl[406]";
connectAttr "rigw:mesh:robo76_translateX.o" "rigwRN.phl[407]";
connectAttr "rigw:mesh:robo76_translateY.o" "rigwRN.phl[408]";
connectAttr "rigw:mesh:robo76_translateZ.o" "rigwRN.phl[409]";
connectAttr "rigw:mesh:robo76_rotateX.o" "rigwRN.phl[410]";
connectAttr "rigw:mesh:robo76_rotateY.o" "rigwRN.phl[411]";
connectAttr "rigw:mesh:robo76_rotateZ.o" "rigwRN.phl[412]";
connectAttr "rigw:mesh:robo76_scaleX.o" "rigwRN.phl[413]";
connectAttr "rigw:mesh:robo76_scaleY.o" "rigwRN.phl[414]";
connectAttr "rigw:mesh:robo76_scaleZ.o" "rigwRN.phl[415]";
connectAttr "rigw:mesh:robo83_visibility.o" "rigwRN.phl[416]";
connectAttr "rigw:mesh:robo83_translateX.o" "rigwRN.phl[417]";
connectAttr "rigw:mesh:robo83_translateY.o" "rigwRN.phl[418]";
connectAttr "rigw:mesh:robo83_translateZ.o" "rigwRN.phl[419]";
connectAttr "rigw:mesh:robo83_rotateX.o" "rigwRN.phl[420]";
connectAttr "rigw:mesh:robo83_rotateY.o" "rigwRN.phl[421]";
connectAttr "rigw:mesh:robo83_rotateZ.o" "rigwRN.phl[422]";
connectAttr "rigw:mesh:robo83_scaleX.o" "rigwRN.phl[423]";
connectAttr "rigw:mesh:robo83_scaleY.o" "rigwRN.phl[424]";
connectAttr "rigw:mesh:robo83_scaleZ.o" "rigwRN.phl[425]";
connectAttr "rigw:mesh:robo82_visibility.o" "rigwRN.phl[426]";
connectAttr "rigw:mesh:robo82_translateX.o" "rigwRN.phl[427]";
connectAttr "rigw:mesh:robo82_translateY.o" "rigwRN.phl[428]";
connectAttr "rigw:mesh:robo82_translateZ.o" "rigwRN.phl[429]";
connectAttr "rigw:mesh:robo82_rotateX.o" "rigwRN.phl[430]";
connectAttr "rigw:mesh:robo82_rotateY.o" "rigwRN.phl[431]";
connectAttr "rigw:mesh:robo82_rotateZ.o" "rigwRN.phl[432]";
connectAttr "rigw:mesh:robo82_scaleX.o" "rigwRN.phl[433]";
connectAttr "rigw:mesh:robo82_scaleY.o" "rigwRN.phl[434]";
connectAttr "rigw:mesh:robo82_scaleZ.o" "rigwRN.phl[435]";
connectAttr "rigw:mesh:robo81_visibility.o" "rigwRN.phl[436]";
connectAttr "rigw:mesh:robo81_translateX.o" "rigwRN.phl[437]";
connectAttr "rigw:mesh:robo81_translateY.o" "rigwRN.phl[438]";
connectAttr "rigw:mesh:robo81_translateZ.o" "rigwRN.phl[439]";
connectAttr "rigw:mesh:robo81_rotateX.o" "rigwRN.phl[440]";
connectAttr "rigw:mesh:robo81_rotateY.o" "rigwRN.phl[441]";
connectAttr "rigw:mesh:robo81_rotateZ.o" "rigwRN.phl[442]";
connectAttr "rigw:mesh:robo81_scaleX.o" "rigwRN.phl[443]";
connectAttr "rigw:mesh:robo81_scaleY.o" "rigwRN.phl[444]";
connectAttr "rigw:mesh:robo81_scaleZ.o" "rigwRN.phl[445]";
connectAttr "rigw:mesh:robo80_visibility.o" "rigwRN.phl[446]";
connectAttr "rigw:mesh:robo80_translateX.o" "rigwRN.phl[447]";
connectAttr "rigw:mesh:robo80_translateY.o" "rigwRN.phl[448]";
connectAttr "rigw:mesh:robo80_translateZ.o" "rigwRN.phl[449]";
connectAttr "rigw:mesh:robo80_rotateX.o" "rigwRN.phl[450]";
connectAttr "rigw:mesh:robo80_rotateY.o" "rigwRN.phl[451]";
connectAttr "rigw:mesh:robo80_rotateZ.o" "rigwRN.phl[452]";
connectAttr "rigw:mesh:robo80_scaleX.o" "rigwRN.phl[453]";
connectAttr "rigw:mesh:robo80_scaleY.o" "rigwRN.phl[454]";
connectAttr "rigw:mesh:robo80_scaleZ.o" "rigwRN.phl[455]";
connectAttr "rigw:mesh:robo79_visibility.o" "rigwRN.phl[456]";
connectAttr "rigw:mesh:robo79_translateX.o" "rigwRN.phl[457]";
connectAttr "rigw:mesh:robo79_translateY.o" "rigwRN.phl[458]";
connectAttr "rigw:mesh:robo79_translateZ.o" "rigwRN.phl[459]";
connectAttr "rigw:mesh:robo79_rotateX.o" "rigwRN.phl[460]";
connectAttr "rigw:mesh:robo79_rotateY.o" "rigwRN.phl[461]";
connectAttr "rigw:mesh:robo79_rotateZ.o" "rigwRN.phl[462]";
connectAttr "rigw:mesh:robo79_scaleX.o" "rigwRN.phl[463]";
connectAttr "rigw:mesh:robo79_scaleY.o" "rigwRN.phl[464]";
connectAttr "rigw:mesh:robo79_scaleZ.o" "rigwRN.phl[465]";
connectAttr "rigw:mesh:robo58_visibility.o" "rigwRN.phl[466]";
connectAttr "rigw:mesh:robo58_translateX.o" "rigwRN.phl[467]";
connectAttr "rigw:mesh:robo58_translateY.o" "rigwRN.phl[468]";
connectAttr "rigw:mesh:robo58_translateZ.o" "rigwRN.phl[469]";
connectAttr "rigw:mesh:robo58_rotateX.o" "rigwRN.phl[470]";
connectAttr "rigw:mesh:robo58_rotateY.o" "rigwRN.phl[471]";
connectAttr "rigw:mesh:robo58_rotateZ.o" "rigwRN.phl[472]";
connectAttr "rigw:mesh:robo58_scaleX.o" "rigwRN.phl[473]";
connectAttr "rigw:mesh:robo58_scaleY.o" "rigwRN.phl[474]";
connectAttr "rigw:mesh:robo58_scaleZ.o" "rigwRN.phl[475]";
connectAttr "rigw:mesh:robo59_visibility.o" "rigwRN.phl[476]";
connectAttr "rigw:mesh:robo59_translateX.o" "rigwRN.phl[477]";
connectAttr "rigw:mesh:robo59_translateY.o" "rigwRN.phl[478]";
connectAttr "rigw:mesh:robo59_translateZ.o" "rigwRN.phl[479]";
connectAttr "rigw:mesh:robo59_rotateX.o" "rigwRN.phl[480]";
connectAttr "rigw:mesh:robo59_rotateY.o" "rigwRN.phl[481]";
connectAttr "rigw:mesh:robo59_rotateZ.o" "rigwRN.phl[482]";
connectAttr "rigw:mesh:robo59_scaleX.o" "rigwRN.phl[483]";
connectAttr "rigw:mesh:robo59_scaleY.o" "rigwRN.phl[484]";
connectAttr "rigw:mesh:robo59_scaleZ.o" "rigwRN.phl[485]";
connectAttr "rigw:mesh:robo60_visibility.o" "rigwRN.phl[486]";
connectAttr "rigw:mesh:robo60_translateX.o" "rigwRN.phl[487]";
connectAttr "rigw:mesh:robo60_translateY.o" "rigwRN.phl[488]";
connectAttr "rigw:mesh:robo60_translateZ.o" "rigwRN.phl[489]";
connectAttr "rigw:mesh:robo60_rotateX.o" "rigwRN.phl[490]";
connectAttr "rigw:mesh:robo60_rotateY.o" "rigwRN.phl[491]";
connectAttr "rigw:mesh:robo60_rotateZ.o" "rigwRN.phl[492]";
connectAttr "rigw:mesh:robo60_scaleX.o" "rigwRN.phl[493]";
connectAttr "rigw:mesh:robo60_scaleY.o" "rigwRN.phl[494]";
connectAttr "rigw:mesh:robo60_scaleZ.o" "rigwRN.phl[495]";
connectAttr "rigw:mesh:robo61_visibility.o" "rigwRN.phl[496]";
connectAttr "rigw:mesh:robo61_translateX.o" "rigwRN.phl[497]";
connectAttr "rigw:mesh:robo61_translateY.o" "rigwRN.phl[498]";
connectAttr "rigw:mesh:robo61_translateZ.o" "rigwRN.phl[499]";
connectAttr "rigw:mesh:robo61_rotateX.o" "rigwRN.phl[500]";
connectAttr "rigw:mesh:robo61_rotateY.o" "rigwRN.phl[501]";
connectAttr "rigw:mesh:robo61_rotateZ.o" "rigwRN.phl[502]";
connectAttr "rigw:mesh:robo61_scaleX.o" "rigwRN.phl[503]";
connectAttr "rigw:mesh:robo61_scaleY.o" "rigwRN.phl[504]";
connectAttr "rigw:mesh:robo61_scaleZ.o" "rigwRN.phl[505]";
connectAttr "rigw:mesh:robo62_visibility.o" "rigwRN.phl[506]";
connectAttr "rigw:mesh:robo62_translateX.o" "rigwRN.phl[507]";
connectAttr "rigw:mesh:robo62_translateY.o" "rigwRN.phl[508]";
connectAttr "rigw:mesh:robo62_translateZ.o" "rigwRN.phl[509]";
connectAttr "rigw:mesh:robo62_rotateX.o" "rigwRN.phl[510]";
connectAttr "rigw:mesh:robo62_rotateY.o" "rigwRN.phl[511]";
connectAttr "rigw:mesh:robo62_rotateZ.o" "rigwRN.phl[512]";
connectAttr "rigw:mesh:robo62_scaleX.o" "rigwRN.phl[513]";
connectAttr "rigw:mesh:robo62_scaleY.o" "rigwRN.phl[514]";
connectAttr "rigw:mesh:robo62_scaleZ.o" "rigwRN.phl[515]";
connectAttr "rigw:mesh:robo63_visibility.o" "rigwRN.phl[516]";
connectAttr "rigw:mesh:robo63_translateX.o" "rigwRN.phl[517]";
connectAttr "rigw:mesh:robo63_translateY.o" "rigwRN.phl[518]";
connectAttr "rigw:mesh:robo63_translateZ.o" "rigwRN.phl[519]";
connectAttr "rigw:mesh:robo63_rotateX.o" "rigwRN.phl[520]";
connectAttr "rigw:mesh:robo63_rotateY.o" "rigwRN.phl[521]";
connectAttr "rigw:mesh:robo63_rotateZ.o" "rigwRN.phl[522]";
connectAttr "rigw:mesh:robo63_scaleX.o" "rigwRN.phl[523]";
connectAttr "rigw:mesh:robo63_scaleY.o" "rigwRN.phl[524]";
connectAttr "rigw:mesh:robo63_scaleZ.o" "rigwRN.phl[525]";
connectAttr "rigw:mesh:robo64_visibility.o" "rigwRN.phl[526]";
connectAttr "rigw:mesh:robo64_translateX.o" "rigwRN.phl[527]";
connectAttr "rigw:mesh:robo64_translateY.o" "rigwRN.phl[528]";
connectAttr "rigw:mesh:robo64_translateZ.o" "rigwRN.phl[529]";
connectAttr "rigw:mesh:robo64_rotateX.o" "rigwRN.phl[530]";
connectAttr "rigw:mesh:robo64_rotateY.o" "rigwRN.phl[531]";
connectAttr "rigw:mesh:robo64_rotateZ.o" "rigwRN.phl[532]";
connectAttr "rigw:mesh:robo64_scaleX.o" "rigwRN.phl[533]";
connectAttr "rigw:mesh:robo64_scaleY.o" "rigwRN.phl[534]";
connectAttr "rigw:mesh:robo64_scaleZ.o" "rigwRN.phl[535]";
connectAttr "rigw:mesh:robo65_visibility.o" "rigwRN.phl[536]";
connectAttr "rigw:mesh:robo65_translateX.o" "rigwRN.phl[537]";
connectAttr "rigw:mesh:robo65_translateY.o" "rigwRN.phl[538]";
connectAttr "rigw:mesh:robo65_translateZ.o" "rigwRN.phl[539]";
connectAttr "rigw:mesh:robo65_rotateX.o" "rigwRN.phl[540]";
connectAttr "rigw:mesh:robo65_rotateY.o" "rigwRN.phl[541]";
connectAttr "rigw:mesh:robo65_rotateZ.o" "rigwRN.phl[542]";
connectAttr "rigw:mesh:robo65_scaleX.o" "rigwRN.phl[543]";
connectAttr "rigw:mesh:robo65_scaleY.o" "rigwRN.phl[544]";
connectAttr "rigw:mesh:robo65_scaleZ.o" "rigwRN.phl[545]";
connectAttr "rigw:mesh:robo66_visibility.o" "rigwRN.phl[546]";
connectAttr "rigw:mesh:robo66_translateX.o" "rigwRN.phl[547]";
connectAttr "rigw:mesh:robo66_translateY.o" "rigwRN.phl[548]";
connectAttr "rigw:mesh:robo66_translateZ.o" "rigwRN.phl[549]";
connectAttr "rigw:mesh:robo66_rotateX.o" "rigwRN.phl[550]";
connectAttr "rigw:mesh:robo66_rotateY.o" "rigwRN.phl[551]";
connectAttr "rigw:mesh:robo66_rotateZ.o" "rigwRN.phl[552]";
connectAttr "rigw:mesh:robo66_scaleX.o" "rigwRN.phl[553]";
connectAttr "rigw:mesh:robo66_scaleY.o" "rigwRN.phl[554]";
connectAttr "rigw:mesh:robo66_scaleZ.o" "rigwRN.phl[555]";
connectAttr "rigw:mesh:robo67_visibility.o" "rigwRN.phl[556]";
connectAttr "rigw:mesh:robo67_translateX.o" "rigwRN.phl[557]";
connectAttr "rigw:mesh:robo67_translateY.o" "rigwRN.phl[558]";
connectAttr "rigw:mesh:robo67_translateZ.o" "rigwRN.phl[559]";
connectAttr "rigw:mesh:robo67_rotateX.o" "rigwRN.phl[560]";
connectAttr "rigw:mesh:robo67_rotateY.o" "rigwRN.phl[561]";
connectAttr "rigw:mesh:robo67_rotateZ.o" "rigwRN.phl[562]";
connectAttr "rigw:mesh:robo67_scaleX.o" "rigwRN.phl[563]";
connectAttr "rigw:mesh:robo67_scaleY.o" "rigwRN.phl[564]";
connectAttr "rigw:mesh:robo67_scaleZ.o" "rigwRN.phl[565]";
connectAttr "rigw:mesh:robo68_visibility.o" "rigwRN.phl[566]";
connectAttr "rigw:mesh:robo68_translateX.o" "rigwRN.phl[567]";
connectAttr "rigw:mesh:robo68_translateY.o" "rigwRN.phl[568]";
connectAttr "rigw:mesh:robo68_translateZ.o" "rigwRN.phl[569]";
connectAttr "rigw:mesh:robo68_rotateX.o" "rigwRN.phl[570]";
connectAttr "rigw:mesh:robo68_rotateY.o" "rigwRN.phl[571]";
connectAttr "rigw:mesh:robo68_rotateZ.o" "rigwRN.phl[572]";
connectAttr "rigw:mesh:robo68_scaleX.o" "rigwRN.phl[573]";
connectAttr "rigw:mesh:robo68_scaleY.o" "rigwRN.phl[574]";
connectAttr "rigw:mesh:robo68_scaleZ.o" "rigwRN.phl[575]";
connectAttr "rigw:mesh:robo69_visibility.o" "rigwRN.phl[576]";
connectAttr "rigw:mesh:robo69_translateX.o" "rigwRN.phl[577]";
connectAttr "rigw:mesh:robo69_translateY.o" "rigwRN.phl[578]";
connectAttr "rigw:mesh:robo69_translateZ.o" "rigwRN.phl[579]";
connectAttr "rigw:mesh:robo69_rotateX.o" "rigwRN.phl[580]";
connectAttr "rigw:mesh:robo69_rotateY.o" "rigwRN.phl[581]";
connectAttr "rigw:mesh:robo69_rotateZ.o" "rigwRN.phl[582]";
connectAttr "rigw:mesh:robo69_scaleX.o" "rigwRN.phl[583]";
connectAttr "rigw:mesh:robo69_scaleY.o" "rigwRN.phl[584]";
connectAttr "rigw:mesh:robo69_scaleZ.o" "rigwRN.phl[585]";
connectAttr "rigw:mesh:robo70_visibility.o" "rigwRN.phl[586]";
connectAttr "rigw:mesh:robo70_translateX.o" "rigwRN.phl[587]";
connectAttr "rigw:mesh:robo70_translateY.o" "rigwRN.phl[588]";
connectAttr "rigw:mesh:robo70_translateZ.o" "rigwRN.phl[589]";
connectAttr "rigw:mesh:robo70_rotateX.o" "rigwRN.phl[590]";
connectAttr "rigw:mesh:robo70_rotateY.o" "rigwRN.phl[591]";
connectAttr "rigw:mesh:robo70_rotateZ.o" "rigwRN.phl[592]";
connectAttr "rigw:mesh:robo70_scaleX.o" "rigwRN.phl[593]";
connectAttr "rigw:mesh:robo70_scaleY.o" "rigwRN.phl[594]";
connectAttr "rigw:mesh:robo70_scaleZ.o" "rigwRN.phl[595]";
connectAttr "rigw:mesh:robo71_visibility.o" "rigwRN.phl[596]";
connectAttr "rigw:mesh:robo71_translateX.o" "rigwRN.phl[597]";
connectAttr "rigw:mesh:robo71_translateY.o" "rigwRN.phl[598]";
connectAttr "rigw:mesh:robo71_translateZ.o" "rigwRN.phl[599]";
connectAttr "rigw:mesh:robo71_rotateX.o" "rigwRN.phl[600]";
connectAttr "rigw:mesh:robo71_rotateY.o" "rigwRN.phl[601]";
connectAttr "rigw:mesh:robo71_rotateZ.o" "rigwRN.phl[602]";
connectAttr "rigw:mesh:robo71_scaleX.o" "rigwRN.phl[603]";
connectAttr "rigw:mesh:robo71_scaleY.o" "rigwRN.phl[604]";
connectAttr "rigw:mesh:robo71_scaleZ.o" "rigwRN.phl[605]";
connectAttr "rigw:mesh:robo102_visibility.o" "rigwRN.phl[606]";
connectAttr "rigw:mesh:robo102_translateX.o" "rigwRN.phl[607]";
connectAttr "rigw:mesh:robo102_translateY.o" "rigwRN.phl[608]";
connectAttr "rigw:mesh:robo102_translateZ.o" "rigwRN.phl[609]";
connectAttr "rigw:mesh:robo102_rotateX.o" "rigwRN.phl[610]";
connectAttr "rigw:mesh:robo102_rotateY.o" "rigwRN.phl[611]";
connectAttr "rigw:mesh:robo102_rotateZ.o" "rigwRN.phl[612]";
connectAttr "rigw:mesh:robo102_scaleX.o" "rigwRN.phl[613]";
connectAttr "rigw:mesh:robo102_scaleY.o" "rigwRN.phl[614]";
connectAttr "rigw:mesh:robo102_scaleZ.o" "rigwRN.phl[615]";
connectAttr "rigw:mesh:robo101_visibility.o" "rigwRN.phl[616]";
connectAttr "rigw:mesh:robo101_translateX.o" "rigwRN.phl[617]";
connectAttr "rigw:mesh:robo101_translateY.o" "rigwRN.phl[618]";
connectAttr "rigw:mesh:robo101_translateZ.o" "rigwRN.phl[619]";
connectAttr "rigw:mesh:robo101_rotateX.o" "rigwRN.phl[620]";
connectAttr "rigw:mesh:robo101_rotateY.o" "rigwRN.phl[621]";
connectAttr "rigw:mesh:robo101_rotateZ.o" "rigwRN.phl[622]";
connectAttr "rigw:mesh:robo101_scaleX.o" "rigwRN.phl[623]";
connectAttr "rigw:mesh:robo101_scaleY.o" "rigwRN.phl[624]";
connectAttr "rigw:mesh:robo101_scaleZ.o" "rigwRN.phl[625]";
connectAttr "rigw:mesh:robo100_visibility.o" "rigwRN.phl[626]";
connectAttr "rigw:mesh:robo100_translateX.o" "rigwRN.phl[627]";
connectAttr "rigw:mesh:robo100_translateY.o" "rigwRN.phl[628]";
connectAttr "rigw:mesh:robo100_translateZ.o" "rigwRN.phl[629]";
connectAttr "rigw:mesh:robo100_rotateX.o" "rigwRN.phl[630]";
connectAttr "rigw:mesh:robo100_rotateY.o" "rigwRN.phl[631]";
connectAttr "rigw:mesh:robo100_rotateZ.o" "rigwRN.phl[632]";
connectAttr "rigw:mesh:robo100_scaleX.o" "rigwRN.phl[633]";
connectAttr "rigw:mesh:robo100_scaleY.o" "rigwRN.phl[634]";
connectAttr "rigw:mesh:robo100_scaleZ.o" "rigwRN.phl[635]";
connectAttr "rigw:mesh:robo99_visibility.o" "rigwRN.phl[636]";
connectAttr "rigw:mesh:robo99_translateX.o" "rigwRN.phl[637]";
connectAttr "rigw:mesh:robo99_translateY.o" "rigwRN.phl[638]";
connectAttr "rigw:mesh:robo99_translateZ.o" "rigwRN.phl[639]";
connectAttr "rigw:mesh:robo99_rotateX.o" "rigwRN.phl[640]";
connectAttr "rigw:mesh:robo99_rotateY.o" "rigwRN.phl[641]";
connectAttr "rigw:mesh:robo99_rotateZ.o" "rigwRN.phl[642]";
connectAttr "rigw:mesh:robo99_scaleX.o" "rigwRN.phl[643]";
connectAttr "rigw:mesh:robo99_scaleY.o" "rigwRN.phl[644]";
connectAttr "rigw:mesh:robo99_scaleZ.o" "rigwRN.phl[645]";
connectAttr "rigw:mesh:robo98_visibility.o" "rigwRN.phl[646]";
connectAttr "rigw:mesh:robo98_translateX.o" "rigwRN.phl[647]";
connectAttr "rigw:mesh:robo98_translateY.o" "rigwRN.phl[648]";
connectAttr "rigw:mesh:robo98_translateZ.o" "rigwRN.phl[649]";
connectAttr "rigw:mesh:robo98_rotateX.o" "rigwRN.phl[650]";
connectAttr "rigw:mesh:robo98_rotateY.o" "rigwRN.phl[651]";
connectAttr "rigw:mesh:robo98_rotateZ.o" "rigwRN.phl[652]";
connectAttr "rigw:mesh:robo98_scaleX.o" "rigwRN.phl[653]";
connectAttr "rigw:mesh:robo98_scaleY.o" "rigwRN.phl[654]";
connectAttr "rigw:mesh:robo98_scaleZ.o" "rigwRN.phl[655]";
connectAttr "rigw:mesh:robo97_visibility.o" "rigwRN.phl[656]";
connectAttr "rigw:mesh:robo97_translateX.o" "rigwRN.phl[657]";
connectAttr "rigw:mesh:robo97_translateY.o" "rigwRN.phl[658]";
connectAttr "rigw:mesh:robo97_translateZ.o" "rigwRN.phl[659]";
connectAttr "rigw:mesh:robo97_rotateX.o" "rigwRN.phl[660]";
connectAttr "rigw:mesh:robo97_rotateY.o" "rigwRN.phl[661]";
connectAttr "rigw:mesh:robo97_rotateZ.o" "rigwRN.phl[662]";
connectAttr "rigw:mesh:robo97_scaleX.o" "rigwRN.phl[663]";
connectAttr "rigw:mesh:robo97_scaleY.o" "rigwRN.phl[664]";
connectAttr "rigw:mesh:robo97_scaleZ.o" "rigwRN.phl[665]";
connectAttr "rigw:mesh:robo96_visibility.o" "rigwRN.phl[666]";
connectAttr "rigw:mesh:robo96_translateX.o" "rigwRN.phl[667]";
connectAttr "rigw:mesh:robo96_translateY.o" "rigwRN.phl[668]";
connectAttr "rigw:mesh:robo96_translateZ.o" "rigwRN.phl[669]";
connectAttr "rigw:mesh:robo96_rotateX.o" "rigwRN.phl[670]";
connectAttr "rigw:mesh:robo96_rotateY.o" "rigwRN.phl[671]";
connectAttr "rigw:mesh:robo96_rotateZ.o" "rigwRN.phl[672]";
connectAttr "rigw:mesh:robo96_scaleX.o" "rigwRN.phl[673]";
connectAttr "rigw:mesh:robo96_scaleY.o" "rigwRN.phl[674]";
connectAttr "rigw:mesh:robo96_scaleZ.o" "rigwRN.phl[675]";
connectAttr "rigw:mesh:robo95_visibility.o" "rigwRN.phl[676]";
connectAttr "rigw:mesh:robo95_translateX.o" "rigwRN.phl[677]";
connectAttr "rigw:mesh:robo95_translateY.o" "rigwRN.phl[678]";
connectAttr "rigw:mesh:robo95_translateZ.o" "rigwRN.phl[679]";
connectAttr "rigw:mesh:robo95_rotateX.o" "rigwRN.phl[680]";
connectAttr "rigw:mesh:robo95_rotateY.o" "rigwRN.phl[681]";
connectAttr "rigw:mesh:robo95_rotateZ.o" "rigwRN.phl[682]";
connectAttr "rigw:mesh:robo95_scaleX.o" "rigwRN.phl[683]";
connectAttr "rigw:mesh:robo95_scaleY.o" "rigwRN.phl[684]";
connectAttr "rigw:mesh:robo95_scaleZ.o" "rigwRN.phl[685]";
connectAttr "rigw:mesh:robo94_visibility.o" "rigwRN.phl[686]";
connectAttr "rigw:mesh:robo94_translateX.o" "rigwRN.phl[687]";
connectAttr "rigw:mesh:robo94_translateY.o" "rigwRN.phl[688]";
connectAttr "rigw:mesh:robo94_translateZ.o" "rigwRN.phl[689]";
connectAttr "rigw:mesh:robo94_rotateX.o" "rigwRN.phl[690]";
connectAttr "rigw:mesh:robo94_rotateY.o" "rigwRN.phl[691]";
connectAttr "rigw:mesh:robo94_rotateZ.o" "rigwRN.phl[692]";
connectAttr "rigw:mesh:robo94_scaleX.o" "rigwRN.phl[693]";
connectAttr "rigw:mesh:robo94_scaleY.o" "rigwRN.phl[694]";
connectAttr "rigw:mesh:robo94_scaleZ.o" "rigwRN.phl[695]";
connectAttr "rigw:mesh:robo93_visibility.o" "rigwRN.phl[696]";
connectAttr "rigw:mesh:robo93_translateX.o" "rigwRN.phl[697]";
connectAttr "rigw:mesh:robo93_translateY.o" "rigwRN.phl[698]";
connectAttr "rigw:mesh:robo93_translateZ.o" "rigwRN.phl[699]";
connectAttr "rigw:mesh:robo93_rotateX.o" "rigwRN.phl[700]";
connectAttr "rigw:mesh:robo93_rotateY.o" "rigwRN.phl[701]";
connectAttr "rigw:mesh:robo93_rotateZ.o" "rigwRN.phl[702]";
connectAttr "rigw:mesh:robo93_scaleX.o" "rigwRN.phl[703]";
connectAttr "rigw:mesh:robo93_scaleY.o" "rigwRN.phl[704]";
connectAttr "rigw:mesh:robo93_scaleZ.o" "rigwRN.phl[705]";
connectAttr "rigw:mesh:robo92_visibility.o" "rigwRN.phl[706]";
connectAttr "rigw:mesh:robo92_translateX.o" "rigwRN.phl[707]";
connectAttr "rigw:mesh:robo92_translateY.o" "rigwRN.phl[708]";
connectAttr "rigw:mesh:robo92_translateZ.o" "rigwRN.phl[709]";
connectAttr "rigw:mesh:robo92_rotateX.o" "rigwRN.phl[710]";
connectAttr "rigw:mesh:robo92_rotateY.o" "rigwRN.phl[711]";
connectAttr "rigw:mesh:robo92_rotateZ.o" "rigwRN.phl[712]";
connectAttr "rigw:mesh:robo92_scaleX.o" "rigwRN.phl[713]";
connectAttr "rigw:mesh:robo92_scaleY.o" "rigwRN.phl[714]";
connectAttr "rigw:mesh:robo92_scaleZ.o" "rigwRN.phl[715]";
connectAttr "rigw:mesh:robo91_visibility.o" "rigwRN.phl[716]";
connectAttr "rigw:mesh:robo91_translateX.o" "rigwRN.phl[717]";
connectAttr "rigw:mesh:robo91_translateY.o" "rigwRN.phl[718]";
connectAttr "rigw:mesh:robo91_translateZ.o" "rigwRN.phl[719]";
connectAttr "rigw:mesh:robo91_rotateX.o" "rigwRN.phl[720]";
connectAttr "rigw:mesh:robo91_rotateY.o" "rigwRN.phl[721]";
connectAttr "rigw:mesh:robo91_rotateZ.o" "rigwRN.phl[722]";
connectAttr "rigw:mesh:robo91_scaleX.o" "rigwRN.phl[723]";
connectAttr "rigw:mesh:robo91_scaleY.o" "rigwRN.phl[724]";
connectAttr "rigw:mesh:robo91_scaleZ.o" "rigwRN.phl[725]";
connectAttr "rigw:mesh:robo90_visibility.o" "rigwRN.phl[726]";
connectAttr "rigw:mesh:robo90_translateX.o" "rigwRN.phl[727]";
connectAttr "rigw:mesh:robo90_translateY.o" "rigwRN.phl[728]";
connectAttr "rigw:mesh:robo90_translateZ.o" "rigwRN.phl[729]";
connectAttr "rigw:mesh:robo90_rotateX.o" "rigwRN.phl[730]";
connectAttr "rigw:mesh:robo90_rotateY.o" "rigwRN.phl[731]";
connectAttr "rigw:mesh:robo90_rotateZ.o" "rigwRN.phl[732]";
connectAttr "rigw:mesh:robo90_scaleX.o" "rigwRN.phl[733]";
connectAttr "rigw:mesh:robo90_scaleY.o" "rigwRN.phl[734]";
connectAttr "rigw:mesh:robo90_scaleZ.o" "rigwRN.phl[735]";
connectAttr "rigw:mesh:robo89_visibility.o" "rigwRN.phl[736]";
connectAttr "rigw:mesh:robo89_translateX.o" "rigwRN.phl[737]";
connectAttr "rigw:mesh:robo89_translateY.o" "rigwRN.phl[738]";
connectAttr "rigw:mesh:robo89_translateZ.o" "rigwRN.phl[739]";
connectAttr "rigw:mesh:robo89_rotateX.o" "rigwRN.phl[740]";
connectAttr "rigw:mesh:robo89_rotateY.o" "rigwRN.phl[741]";
connectAttr "rigw:mesh:robo89_rotateZ.o" "rigwRN.phl[742]";
connectAttr "rigw:mesh:robo89_scaleX.o" "rigwRN.phl[743]";
connectAttr "rigw:mesh:robo89_scaleY.o" "rigwRN.phl[744]";
connectAttr "rigw:mesh:robo89_scaleZ.o" "rigwRN.phl[745]";
connectAttr "rigw:mesh:robo88_visibility.o" "rigwRN.phl[746]";
connectAttr "rigw:mesh:robo88_translateX.o" "rigwRN.phl[747]";
connectAttr "rigw:mesh:robo88_translateY.o" "rigwRN.phl[748]";
connectAttr "rigw:mesh:robo88_translateZ.o" "rigwRN.phl[749]";
connectAttr "rigw:mesh:robo88_rotateX.o" "rigwRN.phl[750]";
connectAttr "rigw:mesh:robo88_rotateY.o" "rigwRN.phl[751]";
connectAttr "rigw:mesh:robo88_rotateZ.o" "rigwRN.phl[752]";
connectAttr "rigw:mesh:robo88_scaleX.o" "rigwRN.phl[753]";
connectAttr "rigw:mesh:robo88_scaleY.o" "rigwRN.phl[754]";
connectAttr "rigw:mesh:robo88_scaleZ.o" "rigwRN.phl[755]";
connectAttr "rigw:mesh:robo87_visibility.o" "rigwRN.phl[756]";
connectAttr "rigw:mesh:robo87_translateX.o" "rigwRN.phl[757]";
connectAttr "rigw:mesh:robo87_translateY.o" "rigwRN.phl[758]";
connectAttr "rigw:mesh:robo87_translateZ.o" "rigwRN.phl[759]";
connectAttr "rigw:mesh:robo87_rotateX.o" "rigwRN.phl[760]";
connectAttr "rigw:mesh:robo87_rotateY.o" "rigwRN.phl[761]";
connectAttr "rigw:mesh:robo87_rotateZ.o" "rigwRN.phl[762]";
connectAttr "rigw:mesh:robo87_scaleX.o" "rigwRN.phl[763]";
connectAttr "rigw:mesh:robo87_scaleY.o" "rigwRN.phl[764]";
connectAttr "rigw:mesh:robo87_scaleZ.o" "rigwRN.phl[765]";
connectAttr "rigw:mesh:robo86_visibility.o" "rigwRN.phl[766]";
connectAttr "rigw:mesh:robo86_translateX.o" "rigwRN.phl[767]";
connectAttr "rigw:mesh:robo86_translateY.o" "rigwRN.phl[768]";
connectAttr "rigw:mesh:robo86_translateZ.o" "rigwRN.phl[769]";
connectAttr "rigw:mesh:robo86_rotateX.o" "rigwRN.phl[770]";
connectAttr "rigw:mesh:robo86_rotateY.o" "rigwRN.phl[771]";
connectAttr "rigw:mesh:robo86_rotateZ.o" "rigwRN.phl[772]";
connectAttr "rigw:mesh:robo86_scaleX.o" "rigwRN.phl[773]";
connectAttr "rigw:mesh:robo86_scaleY.o" "rigwRN.phl[774]";
connectAttr "rigw:mesh:robo86_scaleZ.o" "rigwRN.phl[775]";
connectAttr "rigw:mesh:robo107_visibility.o" "rigwRN.phl[776]";
connectAttr "rigw:mesh:robo107_translateX.o" "rigwRN.phl[777]";
connectAttr "rigw:mesh:robo107_translateY.o" "rigwRN.phl[778]";
connectAttr "rigw:mesh:robo107_translateZ.o" "rigwRN.phl[779]";
connectAttr "rigw:mesh:robo107_rotateX.o" "rigwRN.phl[780]";
connectAttr "rigw:mesh:robo107_rotateY.o" "rigwRN.phl[781]";
connectAttr "rigw:mesh:robo107_rotateZ.o" "rigwRN.phl[782]";
connectAttr "rigw:mesh:robo107_scaleX.o" "rigwRN.phl[783]";
connectAttr "rigw:mesh:robo107_scaleY.o" "rigwRN.phl[784]";
connectAttr "rigw:mesh:robo107_scaleZ.o" "rigwRN.phl[785]";
connectAttr "rigw:mesh:robo108_visibility.o" "rigwRN.phl[786]";
connectAttr "rigw:mesh:robo108_translateX.o" "rigwRN.phl[787]";
connectAttr "rigw:mesh:robo108_translateY.o" "rigwRN.phl[788]";
connectAttr "rigw:mesh:robo108_translateZ.o" "rigwRN.phl[789]";
connectAttr "rigw:mesh:robo108_rotateX.o" "rigwRN.phl[790]";
connectAttr "rigw:mesh:robo108_rotateY.o" "rigwRN.phl[791]";
connectAttr "rigw:mesh:robo108_rotateZ.o" "rigwRN.phl[792]";
connectAttr "rigw:mesh:robo108_scaleX.o" "rigwRN.phl[793]";
connectAttr "rigw:mesh:robo108_scaleY.o" "rigwRN.phl[794]";
connectAttr "rigw:mesh:robo108_scaleZ.o" "rigwRN.phl[795]";
connectAttr "rigw:mesh:robo109_visibility.o" "rigwRN.phl[796]";
connectAttr "rigw:mesh:robo109_translateX.o" "rigwRN.phl[797]";
connectAttr "rigw:mesh:robo109_translateY.o" "rigwRN.phl[798]";
connectAttr "rigw:mesh:robo109_translateZ.o" "rigwRN.phl[799]";
connectAttr "rigw:mesh:robo109_rotateX.o" "rigwRN.phl[800]";
connectAttr "rigw:mesh:robo109_rotateY.o" "rigwRN.phl[801]";
connectAttr "rigw:mesh:robo109_rotateZ.o" "rigwRN.phl[802]";
connectAttr "rigw:mesh:robo109_scaleX.o" "rigwRN.phl[803]";
connectAttr "rigw:mesh:robo109_scaleY.o" "rigwRN.phl[804]";
connectAttr "rigw:mesh:robo109_scaleZ.o" "rigwRN.phl[805]";
connectAttr "rigw:mesh:robo110_visibility.o" "rigwRN.phl[806]";
connectAttr "rigw:mesh:robo110_translateX.o" "rigwRN.phl[807]";
connectAttr "rigw:mesh:robo110_translateY.o" "rigwRN.phl[808]";
connectAttr "rigw:mesh:robo110_translateZ.o" "rigwRN.phl[809]";
connectAttr "rigw:mesh:robo110_rotateX.o" "rigwRN.phl[810]";
connectAttr "rigw:mesh:robo110_rotateY.o" "rigwRN.phl[811]";
connectAttr "rigw:mesh:robo110_rotateZ.o" "rigwRN.phl[812]";
connectAttr "rigw:mesh:robo110_scaleX.o" "rigwRN.phl[813]";
connectAttr "rigw:mesh:robo110_scaleY.o" "rigwRN.phl[814]";
connectAttr "rigw:mesh:robo110_scaleZ.o" "rigwRN.phl[815]";
connectAttr "rigw:mesh:robo111_visibility.o" "rigwRN.phl[816]";
connectAttr "rigw:mesh:robo111_translateX.o" "rigwRN.phl[817]";
connectAttr "rigw:mesh:robo111_translateY.o" "rigwRN.phl[818]";
connectAttr "rigw:mesh:robo111_translateZ.o" "rigwRN.phl[819]";
connectAttr "rigw:mesh:robo111_rotateX.o" "rigwRN.phl[820]";
connectAttr "rigw:mesh:robo111_rotateY.o" "rigwRN.phl[821]";
connectAttr "rigw:mesh:robo111_rotateZ.o" "rigwRN.phl[822]";
connectAttr "rigw:mesh:robo111_scaleX.o" "rigwRN.phl[823]";
connectAttr "rigw:mesh:robo111_scaleY.o" "rigwRN.phl[824]";
connectAttr "rigw:mesh:robo111_scaleZ.o" "rigwRN.phl[825]";
connectAttr "rigw:mesh:robo104_visibility.o" "rigwRN.phl[826]";
connectAttr "rigw:mesh:robo104_translateX.o" "rigwRN.phl[827]";
connectAttr "rigw:mesh:robo104_translateY.o" "rigwRN.phl[828]";
connectAttr "rigw:mesh:robo104_translateZ.o" "rigwRN.phl[829]";
connectAttr "rigw:mesh:robo104_rotateX.o" "rigwRN.phl[830]";
connectAttr "rigw:mesh:robo104_rotateY.o" "rigwRN.phl[831]";
connectAttr "rigw:mesh:robo104_rotateZ.o" "rigwRN.phl[832]";
connectAttr "rigw:mesh:robo104_scaleX.o" "rigwRN.phl[833]";
connectAttr "rigw:mesh:robo104_scaleY.o" "rigwRN.phl[834]";
connectAttr "rigw:mesh:robo104_scaleZ.o" "rigwRN.phl[835]";
connectAttr "rigw:mesh:robo103_visibility.o" "rigwRN.phl[836]";
connectAttr "rigw:mesh:robo103_translateX.o" "rigwRN.phl[837]";
connectAttr "rigw:mesh:robo103_translateY.o" "rigwRN.phl[838]";
connectAttr "rigw:mesh:robo103_translateZ.o" "rigwRN.phl[839]";
connectAttr "rigw:mesh:robo103_rotateX.o" "rigwRN.phl[840]";
connectAttr "rigw:mesh:robo103_rotateY.o" "rigwRN.phl[841]";
connectAttr "rigw:mesh:robo103_rotateZ.o" "rigwRN.phl[842]";
connectAttr "rigw:mesh:robo103_scaleX.o" "rigwRN.phl[843]";
connectAttr "rigw:mesh:robo103_scaleY.o" "rigwRN.phl[844]";
connectAttr "rigw:mesh:robo103_scaleZ.o" "rigwRN.phl[845]";
connectAttr "rigw:rig:Main_translateX.o" "rigwRN.phl[185]";
connectAttr "rigw:rig:Main_translateY.o" "rigwRN.phl[186]";
connectAttr "rigw:rig:Main_translateZ.o" "rigwRN.phl[187]";
connectAttr "rigw:rig:Main_visibility.o" "rigwRN.phl[188]";
connectAttr "rigw:rig:Main_rotateX.o" "rigwRN.phl[189]";
connectAttr "rigw:rig:Main_rotateY.o" "rigwRN.phl[190]";
connectAttr "rigw:rig:Main_rotateZ.o" "rigwRN.phl[191]";
connectAttr "rigw:rig:Main_scaleX.o" "rigwRN.phl[192]";
connectAttr "rigw:rig:Main_scaleY.o" "rigwRN.phl[193]";
connectAttr "rigw:rig:Main_scaleZ.o" "rigwRN.phl[194]";
connectAttr "rigw:rig:FKWheel1_R_rotateX.o" "rigwRN.phl[195]";
connectAttr "rigw:rig:FKWheel1_R_rotateY.o" "rigwRN.phl[196]";
connectAttr "rigw:rig:FKWheel1_R_rotateZ.o" "rigwRN.phl[197]";
connectAttr "rigw:rig:FKWheel2_R_rotateX.o" "rigwRN.phl[198]";
connectAttr "rigw:rig:FKWheel2_R_rotateY.o" "rigwRN.phl[199]";
connectAttr "rigw:rig:FKWheel2_R_rotateZ.o" "rigwRN.phl[200]";
connectAttr "rigw:rig:FKWheel3_R_rotateX.o" "rigwRN.phl[201]";
connectAttr "rigw:rig:FKWheel3_R_rotateY.o" "rigwRN.phl[202]";
connectAttr "rigw:rig:FKWheel3_R_rotateZ.o" "rigwRN.phl[203]";
connectAttr "rigw:rig:FKWheel4_R_rotateX.o" "rigwRN.phl[204]";
connectAttr "rigw:rig:FKWheel4_R_rotateY.o" "rigwRN.phl[205]";
connectAttr "rigw:rig:FKWheel4_R_rotateZ.o" "rigwRN.phl[206]";
connectAttr "rigw:rig:FKWheel1_L_rotateX.o" "rigwRN.phl[207]";
connectAttr "rigw:rig:FKWheel1_L_rotateY.o" "rigwRN.phl[208]";
connectAttr "rigw:rig:FKWheel1_L_rotateZ.o" "rigwRN.phl[209]";
connectAttr "rigw:rig:FKWheel2_L_rotateX.o" "rigwRN.phl[210]";
connectAttr "rigw:rig:FKWheel2_L_rotateY.o" "rigwRN.phl[211]";
connectAttr "rigw:rig:FKWheel2_L_rotateZ.o" "rigwRN.phl[212]";
connectAttr "rigw:rig:FKWheel3_L_rotateX.o" "rigwRN.phl[213]";
connectAttr "rigw:rig:FKWheel3_L_rotateY.o" "rigwRN.phl[214]";
connectAttr "rigw:rig:FKWheel3_L_rotateZ.o" "rigwRN.phl[215]";
connectAttr "rigw:rig:FKWheel4_L_rotateX.o" "rigwRN.phl[216]";
connectAttr "rigw:rig:FKWheel4_L_rotateY.o" "rigwRN.phl[217]";
connectAttr "rigw:rig:FKWheel4_L_rotateZ.o" "rigwRN.phl[218]";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[219]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[220]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[221]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[222]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[223]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[224]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[225]";
connectAttr "rigw:rig:FKBody_M_translateX.o" "rigwRN.phl[226]";
connectAttr "rigw:rig:FKBody_M_translateY.o" "rigwRN.phl[227]";
connectAttr "rigw:rig:FKBody_M_translateZ.o" "rigwRN.phl[228]";
connectAttr "rigw:rig:FKBody_M_rotateX.o" "rigwRN.phl[229]";
connectAttr "rigw:rig:FKBody_M_rotateY.o" "rigwRN.phl[230]";
connectAttr "rigw:rig:FKBody_M_rotateZ.o" "rigwRN.phl[231]";
connectAttr "rigw:rig:FKHead_M_translateX.o" "rigwRN.phl[232]";
connectAttr "rigw:rig:FKHead_M_translateY.o" "rigwRN.phl[233]";
connectAttr "rigw:rig:FKHead_M_translateZ.o" "rigwRN.phl[234]";
connectAttr "rigw:rig:FKHead_M_rotateX.o" "rigwRN.phl[235]";
connectAttr "rigw:rig:FKHead_M_rotateY.o" "rigwRN.phl[236]";
connectAttr "rigw:rig:FKHead_M_rotateZ.o" "rigwRN.phl[237]";
connectAttr "rigw:rig:FKShoulder_R_Global.o" "rigwRN.phl[238]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[239]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[240]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[241]";
connectAttr "rigw:rig:FKElbow5_R_rotateX.o" "rigwRN.phl[242]";
connectAttr "rigw:rig:FKElbow5_R_rotateY.o" "rigwRN.phl[243]";
connectAttr "rigw:rig:FKElbow5_R_rotateZ.o" "rigwRN.phl[244]";
connectAttr "rigw:rig:FKWrist1_R_translateX.o" "rigwRN.phl[245]";
connectAttr "rigw:rig:FKWrist1_R_translateY.o" "rigwRN.phl[246]";
connectAttr "rigw:rig:FKWrist1_R_translateZ.o" "rigwRN.phl[247]";
connectAttr "rigw:rig:FKWrist1_R_rotateX.o" "rigwRN.phl[248]";
connectAttr "rigw:rig:FKWrist1_R_rotateY.o" "rigwRN.phl[249]";
connectAttr "rigw:rig:FKWrist1_R_rotateZ.o" "rigwRN.phl[250]";
connectAttr "rigw:rig:FKShoulder1_L_Global.o" "rigwRN.phl[251]";
connectAttr "rigw:rig:FKShoulder1_L_rotateX.o" "rigwRN.phl[252]";
connectAttr "rigw:rig:FKShoulder1_L_rotateY.o" "rigwRN.phl[253]";
connectAttr "rigw:rig:FKShoulder1_L_rotateZ.o" "rigwRN.phl[254]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[255]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[256]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[257]";
connectAttr "rigw:rig:FKWrist_L_rotateX.o" "rigwRN.phl[258]";
connectAttr "rigw:rig:FKWrist_L_rotateY.o" "rigwRN.phl[259]";
connectAttr "rigw:rig:FKWrist_L_rotateZ.o" "rigwRN.phl[260]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateX.o" "rigwRN.phl[261]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateY.o" "rigwRN.phl[262]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateZ.o" "rigwRN.phl[263]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateX.o" "rigwRN.phl[264]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateY.o" "rigwRN.phl[265]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateZ.o" "rigwRN.phl[266]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateX.o" "rigwRN.phl[267]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateY.o" "rigwRN.phl[268]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateZ.o" "rigwRN.phl[269]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateX.o" "rigwRN.phl[270]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateY.o" "rigwRN.phl[271]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateZ.o" "rigwRN.phl[272]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateX.o" "rigwRN.phl[273]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateY.o" "rigwRN.phl[274]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateZ.o" "rigwRN.phl[275]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateX.o" "rigwRN.phl[276]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateY.o" "rigwRN.phl[277]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateZ.o" "rigwRN.phl[278]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateX.o" "rigwRN.phl[279]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateY.o" "rigwRN.phl[280]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateZ.o" "rigwRN.phl[281]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateX.o" "rigwRN.phl[282]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateY.o" "rigwRN.phl[283]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateZ.o" "rigwRN.phl[284]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateX.o" "rigwRN.phl[285]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateY.o" "rigwRN.phl[286]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateZ.o" "rigwRN.phl[287]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateX.o" "rigwRN.phl[288]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateY.o" "rigwRN.phl[289]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateZ.o" "rigwRN.phl[290]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateX.o" "rigwRN.phl[291]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateY.o" "rigwRN.phl[292]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateZ.o" "rigwRN.phl[293]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateX.o" "rigwRN.phl[294]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateY.o" "rigwRN.phl[295]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateZ.o" "rigwRN.phl[296]";
connectAttr "rigw:rig:FKHose1_R_translateX.o" "rigwRN.phl[297]";
connectAttr "rigw:rig:FKHose1_R_translateY.o" "rigwRN.phl[298]";
connectAttr "rigw:rig:FKHose1_R_translateZ.o" "rigwRN.phl[299]";
connectAttr "rigw:rig:FKHose1_R_rotateX.o" "rigwRN.phl[300]";
connectAttr "rigw:rig:FKHose1_R_rotateY.o" "rigwRN.phl[301]";
connectAttr "rigw:rig:FKHose1_R_rotateZ.o" "rigwRN.phl[302]";
connectAttr "rigw:rig:FKHose2_R_translateX.o" "rigwRN.phl[303]";
connectAttr "rigw:rig:FKHose2_R_translateY.o" "rigwRN.phl[304]";
connectAttr "rigw:rig:FKHose2_R_translateZ.o" "rigwRN.phl[305]";
connectAttr "rigw:rig:FKHose2_R_rotateX.o" "rigwRN.phl[306]";
connectAttr "rigw:rig:FKHose2_R_rotateY.o" "rigwRN.phl[307]";
connectAttr "rigw:rig:FKHose2_R_rotateZ.o" "rigwRN.phl[308]";
connectAttr "rigw:rig:FKHose1_L_rotateX.o" "rigwRN.phl[309]";
connectAttr "rigw:rig:FKHose1_L_rotateY.o" "rigwRN.phl[310]";
connectAttr "rigw:rig:FKHose1_L_rotateZ.o" "rigwRN.phl[311]";
connectAttr "rigw:rig:FKHose2_L_rotateX.o" "rigwRN.phl[312]";
connectAttr "rigw:rig:FKHose2_L_rotateY.o" "rigwRN.phl[313]";
connectAttr "rigw:rig:FKHose2_L_rotateZ.o" "rigwRN.phl[314]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[315]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[316]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[317]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[318]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[319]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[320]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[321]";
connectAttr "rigw:rig:IKLeg_R_rollAngle.o" "rigwRN.phl[322]";
connectAttr "rigw:rig:IKLeg_R_roll.o" "rigwRN.phl[323]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[324]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[325]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[326]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[327]";
connectAttr "rigw:rig:IKLegHeel_R_rotateX.o" "rigwRN.phl[328]";
connectAttr "rigw:rig:IKLegHeel_R_rotateY.o" "rigwRN.phl[329]";
connectAttr "rigw:rig:IKLegHeel_R_rotateZ.o" "rigwRN.phl[330]";
connectAttr "rigw:rig:IKLegBall_R_rotateX.o" "rigwRN.phl[331]";
connectAttr "rigw:rig:PoleLeg_R_translateX.o" "rigwRN.phl[332]";
connectAttr "rigw:rig:PoleLeg_R_translateY.o" "rigwRN.phl[333]";
connectAttr "rigw:rig:PoleLeg_R_translateZ.o" "rigwRN.phl[334]";
connectAttr "rigw:rig:PoleLeg_R_follow.o" "rigwRN.phl[335]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[336]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[337]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[338]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[339]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[340]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[341]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[342]";
connectAttr "rigw:rig:IKLeg_L_rollAngle.o" "rigwRN.phl[343]";
connectAttr "rigw:rig:IKLeg_L_roll.o" "rigwRN.phl[344]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[345]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[346]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[347]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[348]";
connectAttr "rigw:rig:IKLegHeel_L_rotateX.o" "rigwRN.phl[349]";
connectAttr "rigw:rig:IKLegHeel_L_rotateY.o" "rigwRN.phl[350]";
connectAttr "rigw:rig:IKLegHeel_L_rotateZ.o" "rigwRN.phl[351]";
connectAttr "rigw:rig:IKLegBall_L_rotateX.o" "rigwRN.phl[352]";
connectAttr "rigw:rig:PoleLeg_L_translateX.o" "rigwRN.phl[353]";
connectAttr "rigw:rig:PoleLeg_L_translateY.o" "rigwRN.phl[354]";
connectAttr "rigw:rig:PoleLeg_L_translateZ.o" "rigwRN.phl[355]";
connectAttr "rigw:rig:PoleLeg_L_follow.o" "rigwRN.phl[356]";
connectAttr "rigw:rig:FKIKLeg_R_FKIKBlend.o" "rigwRN.phl[357]";
connectAttr "rigw:rig:FKIKLeg_R_IKVis.o" "rigwRN.phl[358]";
connectAttr "rigw:rig:FKIKLeg_R_FKVis.o" "rigwRN.phl[359]";
connectAttr "rigw:rig:FKIKLeg_L_FKIKBlend.o" "rigwRN.phl[360]";
connectAttr "rigw:rig:FKIKLeg_L_IKVis.o" "rigwRN.phl[361]";
connectAttr "rigw:rig:FKIKLeg_L_FKVis.o" "rigwRN.phl[362]";
connectAttr "mia_exposure_simple1.msg" "orthCamShape.mils";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "rigw:robo85_visibility.o" "rigwRN.phl[179]";
connectAttr "rigw:robo84_visibility.o" "rigwRN.phl[180]";
connectAttr "rigw:robo105_visibility.o" "rigwRN.phl[181]";
connectAttr "rigw:robo106_visibility.o" "rigwRN.phl[182]";
connectAttr "rigw:robo58_visibility.o" "rigwRN.phl[184]";
connectAttr "mia_exposure_simple1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Medic__mc-rigw@Med_idle.ma
