//Maya ASCII 2013 scene
//Name: Medic__mc-rigw@Med_pose.ma
//Last modified: Thu, Jun 12, 2014 01:54:25 PM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Medic/Medic__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Medic__rig.ma";
file -rdi 1 -rpr "everlife" -rfn "everlifeRN" "/Users/jmiller/Art/everlife//Assets/Art/LightingResources/everlife_LightRig_Characters.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Medic/Medic__mc-rigw.ma";
file -r -rpr "everlife" -dr 1 -rfn "everlifeRN" "/Users/jmiller/Art/everlife//Assets/Art/LightingResources/everlife_LightRig_Characters.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.3";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 21.9781679642523 17.462631343465169 37.226844847408671 ;
	setAttr ".r" -type "double3" -11.138352729603925 30.200000000000031 -4.6000338582616196e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ovr" 1.3;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 44.356062073760071;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".dr" yes;
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" 0 0 500 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".ow" 72;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 16 ".lnk";
	setAttr -s 16 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 68 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 0
		"rigw:rigRN" 0
		"rigw:rigRN" 124
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotate" " -type \"double3\" 0 2.182182 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" 0 0 11.837115"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotate" " -type \"double3\" 0 0 -5.582734"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotate" " -type \"double3\" 0 0 -3.720942"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 -10.631262"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateZ" " -av"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateX" 
		"rigwRN.placeHolderList[1]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateY" 
		"rigwRN.placeHolderList[2]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateZ" 
		"rigwRN.placeHolderList[3]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateX" 
		"rigwRN.placeHolderList[4]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateY" 
		"rigwRN.placeHolderList[5]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateZ" 
		"rigwRN.placeHolderList[6]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateX" 
		"rigwRN.placeHolderList[7]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateY" 
		"rigwRN.placeHolderList[8]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateZ" 
		"rigwRN.placeHolderList[9]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateX" 
		"rigwRN.placeHolderList[10]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateY" 
		"rigwRN.placeHolderList[11]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateZ" 
		"rigwRN.placeHolderList[12]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.Global" 
		"rigwRN.placeHolderList[13]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[14]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[15]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[16]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateX" 
		"rigwRN.placeHolderList[17]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateY" 
		"rigwRN.placeHolderList[18]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateZ" 
		"rigwRN.placeHolderList[19]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateX" 
		"rigwRN.placeHolderList[20]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateY" 
		"rigwRN.placeHolderList[21]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateZ" 
		"rigwRN.placeHolderList[22]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateX" 
		"rigwRN.placeHolderList[23]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateY" 
		"rigwRN.placeHolderList[24]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateZ" 
		"rigwRN.placeHolderList[25]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.Global" 
		"rigwRN.placeHolderList[26]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateX" 
		"rigwRN.placeHolderList[27]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateY" 
		"rigwRN.placeHolderList[28]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateZ" 
		"rigwRN.placeHolderList[29]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[30]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[31]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[32]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateX" 
		"rigwRN.placeHolderList[33]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateY" 
		"rigwRN.placeHolderList[34]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[35]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateX" 
		"rigwRN.placeHolderList[36]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateY" 
		"rigwRN.placeHolderList[37]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[38]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateX" 
		"rigwRN.placeHolderList[39]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateY" 
		"rigwRN.placeHolderList[40]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[41]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateX" 
		"rigwRN.placeHolderList[42]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateY" 
		"rigwRN.placeHolderList[43]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[44]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateX" 
		"rigwRN.placeHolderList[45]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateY" 
		"rigwRN.placeHolderList[46]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[47]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateX" 
		"rigwRN.placeHolderList[48]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateY" 
		"rigwRN.placeHolderList[49]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[50]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateX" 
		"rigwRN.placeHolderList[51]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateY" 
		"rigwRN.placeHolderList[52]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[53]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateX" 
		"rigwRN.placeHolderList[54]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateY" 
		"rigwRN.placeHolderList[55]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[56]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateX" 
		"rigwRN.placeHolderList[57]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateY" 
		"rigwRN.placeHolderList[58]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateZ" 
		"rigwRN.placeHolderList[59]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateX" 
		"rigwRN.placeHolderList[60]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateY" 
		"rigwRN.placeHolderList[61]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateZ" 
		"rigwRN.placeHolderList[62]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateX" 
		"rigwRN.placeHolderList[63]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateY" 
		"rigwRN.placeHolderList[64]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateZ" 
		"rigwRN.placeHolderList[65]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateX" 
		"rigwRN.placeHolderList[66]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateY" 
		"rigwRN.placeHolderList[67]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateZ" 
		"rigwRN.placeHolderList[68]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -sn "imrVerbosity" -ln "imrVerbosity" -dv 3 -at "short";
	addAttr -ci true -sn "imrThreads" -ln "imrThreads" -dv 1 -at "short";
	addAttr -ci true -sn "imrThreadsAdjust" -ln "imrThreadsAdjust" -dv 1 -min 0 -max 
		1 -at "bool";
	addAttr -ci true -sn "imrTaskOrder" -ln "imrTaskOrder" -dv 1 -at "short";
	addAttr -ci true -sn "imrTaskSize" -ln "imrTaskSize" -at "short";
	addAttr -ci true -sn "imrTaskAdjust" -ln "imrTaskAdjust" -dv 1 -at "short";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
	setAttr ".imrThreads" 4;
	setAttr ".imrThreadsAdjust" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".fil" 2;
	setAttr ".filw" 0.83333331346511841;
	setAttr ".filh" 0.83333331346511841;
	setAttr ".maxr" 2;
	setAttr ".gi" yes;
	setAttr ".fg" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 6 -ast 1 -aet 6 ";
	setAttr ".st" 6;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 -14.352204438175198 2 0 5 -14.352204438175198
		 8 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 3 0 9 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 3 0 9 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -3 0 0 -14.352204438175198 3 0 6 -14.352204438175198
		 9 0;
createNode animCurveTU -n "rigw:rig:FKShoulder1_L_Global";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 11.83711532077935 4 -13.613346850149323
		 7 11.83711532077935;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:FKShoulder_R_Global";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 4.9960036108132044e-16;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.7763568394002505e-15;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0.91307367041641951 7 0;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 2.9936586290420677 7 0;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0.52242621575681014 7 0;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 -31.62822903274925 7 0;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 16.214902573917453 7 0;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 3.2045755788787438 7 0;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 8.4169869349295645 2 0 5 8.4169869349295645
		 8 0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0 2 0 5 0 8 0;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0.17131710120184795 7 0;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 3.7050212361176729 7 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 12.823726028464051 7 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.40792986750602722;
	setAttr -s 3 ".koy[2]"  0.91301321983337402;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 12.823726028464051 7 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.40792986750602722;
	setAttr -s 3 ".koy[2]"  0.91301321983337402;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 12.823726028464051 7 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.40792986750602722;
	setAttr -s 3 ".koy[2]"  0.91301321983337402;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 12.823726028464051 7 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.40792986750602722;
	setAttr -s 3 ".koy[2]"  0.91301321983337402;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 12.823726028464051 7 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.40792986750602722;
	setAttr -s 3 ".koy[2]"  0.91301321983337402;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 12.823726028464051 7 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.40792986750602722;
	setAttr -s 3 ".koy[2]"  0.91301321983337402;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 12.823726028464051 7 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.40792986750602722;
	setAttr -s 3 ".koy[2]"  0.91301321983337402;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 12.823726028464051 7 0;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.40792986750602722;
	setAttr -s 3 ".koy[2]"  0.91301321983337402;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 -1.7762315293699733 -1 -16.458456488777752
		 2 -1.7762315293699733 5 -16.458456488777752 8 -1.7762315293699733;
createNode reference -n "everlifeRN";
	setAttr ".ed" -type "dataReferenceEdits" 
		"everlifeRN"
		"everlifeRN" 0;
lockNode -l 1 ;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 16 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -s 35 ".gn";
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 16 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 32 ".tx";
select -ne :lightList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 6 ".l";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 46 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 4 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren" -type "string" "mentalRay";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 32;
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar" 0;
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn" no;
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff" yes;
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultRenderQuality;
	setAttr ".ert" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w" 2048;
	setAttr -av ".h" 2048;
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al" yes;
	setAttr -av ".dar" 1;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -s 6 ".dsm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 2 ".sol";
connectAttr "rigw:rig:FKBody_M_translateX.o" "rigwRN.phl[1]";
connectAttr "rigw:rig:FKBody_M_translateY.o" "rigwRN.phl[2]";
connectAttr "rigw:rig:FKBody_M_translateZ.o" "rigwRN.phl[3]";
connectAttr "rigw:rig:FKBody_M_rotateX.o" "rigwRN.phl[4]";
connectAttr "rigw:rig:FKBody_M_rotateY.o" "rigwRN.phl[5]";
connectAttr "rigw:rig:FKBody_M_rotateZ.o" "rigwRN.phl[6]";
connectAttr "rigw:rig:FKHead_M_rotateX.o" "rigwRN.phl[7]";
connectAttr "rigw:rig:FKHead_M_rotateY.o" "rigwRN.phl[8]";
connectAttr "rigw:rig:FKHead_M_rotateZ.o" "rigwRN.phl[9]";
connectAttr "rigw:rig:FKHead_M_translateX.o" "rigwRN.phl[10]";
connectAttr "rigw:rig:FKHead_M_translateY.o" "rigwRN.phl[11]";
connectAttr "rigw:rig:FKHead_M_translateZ.o" "rigwRN.phl[12]";
connectAttr "rigw:rig:FKShoulder_R_Global.o" "rigwRN.phl[13]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[14]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[15]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[16]";
connectAttr "rigw:rig:FKElbow5_R_rotateX.o" "rigwRN.phl[17]";
connectAttr "rigw:rig:FKElbow5_R_rotateY.o" "rigwRN.phl[18]";
connectAttr "rigw:rig:FKElbow5_R_rotateZ.o" "rigwRN.phl[19]";
connectAttr "rigw:rig:FKWrist1_R_rotateX.o" "rigwRN.phl[20]";
connectAttr "rigw:rig:FKWrist1_R_rotateY.o" "rigwRN.phl[21]";
connectAttr "rigw:rig:FKWrist1_R_rotateZ.o" "rigwRN.phl[22]";
connectAttr "rigw:rig:FKWrist1_R_translateX.o" "rigwRN.phl[23]";
connectAttr "rigw:rig:FKWrist1_R_translateY.o" "rigwRN.phl[24]";
connectAttr "rigw:rig:FKWrist1_R_translateZ.o" "rigwRN.phl[25]";
connectAttr "rigw:rig:FKShoulder1_L_Global.o" "rigwRN.phl[26]";
connectAttr "rigw:rig:FKShoulder1_L_rotateX.o" "rigwRN.phl[27]";
connectAttr "rigw:rig:FKShoulder1_L_rotateY.o" "rigwRN.phl[28]";
connectAttr "rigw:rig:FKShoulder1_L_rotateZ.o" "rigwRN.phl[29]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[30]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[31]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[32]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateX.o" "rigwRN.phl[33]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateY.o" "rigwRN.phl[34]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateZ.o" "rigwRN.phl[35]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateX.o" "rigwRN.phl[36]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateY.o" "rigwRN.phl[37]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateZ.o" "rigwRN.phl[38]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateX.o" "rigwRN.phl[39]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateY.o" "rigwRN.phl[40]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateZ.o" "rigwRN.phl[41]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateX.o" "rigwRN.phl[42]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateY.o" "rigwRN.phl[43]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateZ.o" "rigwRN.phl[44]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateX.o" "rigwRN.phl[45]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateY.o" "rigwRN.phl[46]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateZ.o" "rigwRN.phl[47]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateX.o" "rigwRN.phl[48]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateY.o" "rigwRN.phl[49]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateZ.o" "rigwRN.phl[50]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateX.o" "rigwRN.phl[51]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateY.o" "rigwRN.phl[52]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateZ.o" "rigwRN.phl[53]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateX.o" "rigwRN.phl[54]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateY.o" "rigwRN.phl[55]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateZ.o" "rigwRN.phl[56]";
connectAttr "rigw:rig:FKHose1_R_translateX.o" "rigwRN.phl[57]";
connectAttr "rigw:rig:FKHose1_R_translateY.o" "rigwRN.phl[58]";
connectAttr "rigw:rig:FKHose1_R_translateZ.o" "rigwRN.phl[59]";
connectAttr "rigw:rig:FKHose1_R_rotateX.o" "rigwRN.phl[60]";
connectAttr "rigw:rig:FKHose1_R_rotateY.o" "rigwRN.phl[61]";
connectAttr "rigw:rig:FKHose1_R_rotateZ.o" "rigwRN.phl[62]";
connectAttr "rigw:rig:FKHose2_R_translateX.o" "rigwRN.phl[63]";
connectAttr "rigw:rig:FKHose2_R_translateY.o" "rigwRN.phl[64]";
connectAttr "rigw:rig:FKHose2_R_translateZ.o" "rigwRN.phl[65]";
connectAttr "rigw:rig:FKHose2_R_rotateX.o" "rigwRN.phl[66]";
connectAttr "rigw:rig:FKHose2_R_rotateY.o" "rigwRN.phl[67]";
connectAttr "rigw:rig:FKHose2_R_rotateZ.o" "rigwRN.phl[68]";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Medic__mc-rigw@Med_pose.ma
