//Maya ASCII 2013 scene
//Name: Medic__mc-rigw@Med_death.ma
//Last modified: Mon, Nov 03, 2014 04:19:40 PM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Medic/Medic__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Medic__rig.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Medic/Medic__mc-rigw.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.4";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 10.835794721046669 61.414273765749442 86.294824486082547 ;
	setAttr ".r" -type "double3" 147.26164727039796 176.59999999999889 180.00000000000006 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 108.47925168272118;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -11.896410670459375 6.607724104275186 -5.8498888627994461 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" 0 0 500 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".ow" 89.657111707367363;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode transform -n "pPlane1";
	setAttr ".s" -type "double3" 119.81552795306514 119.81552795306514 119.81552795306514 ;
createNode mesh -n "pPlaneShape1" -p "pPlane1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 16 ".lnk";
	setAttr -s 16 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".dli[1]"  1;
	setAttr -s 2 ".dli";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 658 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".phl[440]" 0;
	setAttr ".phl[441]" 0;
	setAttr ".phl[442]" 0;
	setAttr ".phl[443]" 0;
	setAttr ".phl[444]" 0;
	setAttr ".phl[445]" 0;
	setAttr ".phl[446]" 0;
	setAttr ".phl[447]" 0;
	setAttr ".phl[448]" 0;
	setAttr ".phl[449]" 0;
	setAttr ".phl[450]" 0;
	setAttr ".phl[451]" 0;
	setAttr ".phl[452]" 0;
	setAttr ".phl[453]" 0;
	setAttr ".phl[454]" 0;
	setAttr ".phl[455]" 0;
	setAttr ".phl[456]" 0;
	setAttr ".phl[457]" 0;
	setAttr ".phl[458]" 0;
	setAttr ".phl[459]" 0;
	setAttr ".phl[460]" 0;
	setAttr ".phl[461]" 0;
	setAttr ".phl[462]" 0;
	setAttr ".phl[463]" 0;
	setAttr ".phl[464]" 0;
	setAttr ".phl[465]" 0;
	setAttr ".phl[466]" 0;
	setAttr ".phl[467]" 0;
	setAttr ".phl[468]" 0;
	setAttr ".phl[469]" 0;
	setAttr ".phl[470]" 0;
	setAttr ".phl[471]" 0;
	setAttr ".phl[472]" 0;
	setAttr ".phl[473]" 0;
	setAttr ".phl[474]" 0;
	setAttr ".phl[475]" 0;
	setAttr ".phl[476]" 0;
	setAttr ".phl[477]" 0;
	setAttr ".phl[478]" 0;
	setAttr ".phl[479]" 0;
	setAttr ".phl[480]" 0;
	setAttr ".phl[481]" 0;
	setAttr ".phl[482]" 0;
	setAttr ".phl[483]" 0;
	setAttr ".phl[484]" 0;
	setAttr ".phl[485]" 0;
	setAttr ".phl[486]" 0;
	setAttr ".phl[487]" 0;
	setAttr ".phl[488]" 0;
	setAttr ".phl[489]" 0;
	setAttr ".phl[490]" 0;
	setAttr ".phl[491]" 0;
	setAttr ".phl[492]" 0;
	setAttr ".phl[493]" 0;
	setAttr ".phl[494]" 0;
	setAttr ".phl[495]" 0;
	setAttr ".phl[496]" 0;
	setAttr ".phl[497]" 0;
	setAttr ".phl[498]" 0;
	setAttr ".phl[499]" 0;
	setAttr ".phl[500]" 0;
	setAttr ".phl[501]" 0;
	setAttr ".phl[502]" 0;
	setAttr ".phl[503]" 0;
	setAttr ".phl[504]" 0;
	setAttr ".phl[505]" 0;
	setAttr ".phl[506]" 0;
	setAttr ".phl[507]" 0;
	setAttr ".phl[508]" 0;
	setAttr ".phl[509]" 0;
	setAttr ".phl[510]" 0;
	setAttr ".phl[511]" 0;
	setAttr ".phl[512]" 0;
	setAttr ".phl[513]" 0;
	setAttr ".phl[514]" 0;
	setAttr ".phl[515]" 0;
	setAttr ".phl[516]" 0;
	setAttr ".phl[517]" 0;
	setAttr ".phl[518]" 0;
	setAttr ".phl[519]" 0;
	setAttr ".phl[520]" 0;
	setAttr ".phl[521]" 0;
	setAttr ".phl[522]" 0;
	setAttr ".phl[523]" 0;
	setAttr ".phl[524]" 0;
	setAttr ".phl[525]" 0;
	setAttr ".phl[526]" 0;
	setAttr ".phl[527]" 0;
	setAttr ".phl[528]" 0;
	setAttr ".phl[529]" 0;
	setAttr ".phl[530]" 0;
	setAttr ".phl[531]" 0;
	setAttr ".phl[532]" 0;
	setAttr ".phl[533]" 0;
	setAttr ".phl[534]" 0;
	setAttr ".phl[535]" 0;
	setAttr ".phl[536]" 0;
	setAttr ".phl[537]" 0;
	setAttr ".phl[538]" 0;
	setAttr ".phl[539]" 0;
	setAttr ".phl[540]" 0;
	setAttr ".phl[541]" 0;
	setAttr ".phl[542]" 0;
	setAttr ".phl[543]" 0;
	setAttr ".phl[544]" 0;
	setAttr ".phl[545]" 0;
	setAttr ".phl[546]" 0;
	setAttr ".phl[547]" 0;
	setAttr ".phl[548]" 0;
	setAttr ".phl[549]" 0;
	setAttr ".phl[550]" 0;
	setAttr ".phl[551]" 0;
	setAttr ".phl[552]" 0;
	setAttr ".phl[553]" 0;
	setAttr ".phl[554]" 0;
	setAttr ".phl[555]" 0;
	setAttr ".phl[556]" 0;
	setAttr ".phl[557]" 0;
	setAttr ".phl[558]" 0;
	setAttr ".phl[559]" 0;
	setAttr ".phl[560]" 0;
	setAttr ".phl[561]" 0;
	setAttr ".phl[562]" 0;
	setAttr ".phl[563]" 0;
	setAttr ".phl[564]" 0;
	setAttr ".phl[565]" 0;
	setAttr ".phl[566]" 0;
	setAttr ".phl[567]" 0;
	setAttr ".phl[568]" 0;
	setAttr ".phl[569]" 0;
	setAttr ".phl[570]" 0;
	setAttr ".phl[571]" 0;
	setAttr ".phl[572]" 0;
	setAttr ".phl[573]" 0;
	setAttr ".phl[574]" 0;
	setAttr ".phl[575]" 0;
	setAttr ".phl[576]" 0;
	setAttr ".phl[577]" 0;
	setAttr ".phl[578]" 0;
	setAttr ".phl[579]" 0;
	setAttr ".phl[580]" 0;
	setAttr ".phl[581]" 0;
	setAttr ".phl[582]" 0;
	setAttr ".phl[583]" 0;
	setAttr ".phl[584]" 0;
	setAttr ".phl[585]" 0;
	setAttr ".phl[586]" 0;
	setAttr ".phl[587]" 0;
	setAttr ".phl[588]" 0;
	setAttr ".phl[589]" 0;
	setAttr ".phl[590]" 0;
	setAttr ".phl[591]" 0;
	setAttr ".phl[592]" 0;
	setAttr ".phl[593]" 0;
	setAttr ".phl[594]" 0;
	setAttr ".phl[595]" 0;
	setAttr ".phl[596]" 0;
	setAttr ".phl[597]" 0;
	setAttr ".phl[598]" 0;
	setAttr ".phl[599]" 0;
	setAttr ".phl[600]" 0;
	setAttr ".phl[601]" 0;
	setAttr ".phl[602]" 0;
	setAttr ".phl[603]" 0;
	setAttr ".phl[604]" 0;
	setAttr ".phl[605]" 0;
	setAttr ".phl[606]" 0;
	setAttr ".phl[607]" 0;
	setAttr ".phl[608]" 0;
	setAttr ".phl[609]" 0;
	setAttr ".phl[610]" 0;
	setAttr ".phl[611]" 0;
	setAttr ".phl[612]" 0;
	setAttr ".phl[613]" 0;
	setAttr ".phl[614]" 0;
	setAttr ".phl[615]" 0;
	setAttr ".phl[616]" 0;
	setAttr ".phl[617]" 0;
	setAttr ".phl[618]" 0;
	setAttr ".phl[619]" 0;
	setAttr ".phl[620]" 0;
	setAttr ".phl[621]" 0;
	setAttr ".phl[622]" 0;
	setAttr ".phl[623]" 0;
	setAttr ".phl[624]" 0;
	setAttr ".phl[625]" 0;
	setAttr ".phl[626]" 0;
	setAttr ".phl[627]" 0;
	setAttr ".phl[628]" 0;
	setAttr ".phl[629]" 0;
	setAttr ".phl[630]" 0;
	setAttr ".phl[631]" 0;
	setAttr ".phl[632]" 0;
	setAttr ".phl[633]" 0;
	setAttr ".phl[634]" 0;
	setAttr ".phl[635]" 0;
	setAttr ".phl[636]" 0;
	setAttr ".phl[637]" 0;
	setAttr ".phl[638]" 0;
	setAttr ".phl[639]" 0;
	setAttr ".phl[640]" 0;
	setAttr ".phl[641]" 0;
	setAttr ".phl[642]" 0;
	setAttr ".phl[643]" 0;
	setAttr ".phl[644]" 0;
	setAttr ".phl[645]" 0;
	setAttr ".phl[646]" 0;
	setAttr ".phl[647]" 0;
	setAttr ".phl[648]" 0;
	setAttr ".phl[649]" 0;
	setAttr ".phl[650]" 0;
	setAttr ".phl[651]" 0;
	setAttr ".phl[652]" 0;
	setAttr ".phl[653]" 0;
	setAttr ".phl[654]" 0;
	setAttr ".phl[655]" 0;
	setAttr ".phl[656]" 0;
	setAttr ".phl[657]" 0;
	setAttr ".phl[658]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 0
		"rigw:rigRN" 0
		"rigwRN" 1105
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translate" " -type \"double3\" 0.740445 0.715486 4.165678"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotate" " -type \"double3\" 112.246345 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translate" " -type \"double3\" 0.740445 1.248661 3.934626"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotate" " -type \"double3\" 87.636758 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translate" " -type \"double3\" 0.740445 2.006366 3.876286"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotate" " -type \"double3\" 27.17363 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translate" " -type \"double3\" 0.740445 2.443066 2.998664"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotate" " -type \"double3\" 27.17363 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translate" " -type \"double3\" 0.740572 2.802991 2.134471"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotate" " -type \"double3\" 28.003616 0.054517 -0.506614"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translate" " -type \"double3\" 0.740445 3.03787 1.259282"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotate" " -type \"double3\" 4.484307 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translate" " -type \"double3\" 0.740445 3.050656 0.33016"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translate" " -type \"double3\" 0.740445 3.050656 -0.642503"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translate" " -type \"double3\" 0.740445 3.050656 -1.615166"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translate" " -type \"double3\" 0.740445 2.829374 -2.525288"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotate" " -type \"double3\" -23.380861 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translate" " -type \"double3\" 0.740445 2.562667 -3.403567"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotate" " -type \"double3\" -27.44369 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translate" " -type \"double3\" 0.740445 2.112302 -4.27079"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotate" " -type \"double3\" -27.44369 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translate" " -type \"double3\" 0.740445 1.661936 -5.138014"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotate" " -type \"double3\" -27.44369 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translate" " -type \"double3\" 0.740445 1.078102 -5.613094"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotate" " -type \"double3\" -101.394566 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translate" " -type \"double3\" 0.740445 0.659831 -4.957155"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotate" " -type \"double3\" 210.809462 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translate" " -type \"double3\" 0.740445 0.360482 -4.137065"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translate" " -type \"double3\" 0.740445 0.360482 -3.159629"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translate" " -type \"double3\" 0.740445 0.360482 -2.182193"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translate" " -type \"double3\" 0.740445 0.360482 -1.204757"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translate" " -type \"double3\" 0.740445 0.360482 -0.22732"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translate" " -type \"double3\" 0.740445 0.360482 0.750116"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translate" " -type \"double3\" 0.740445 0.360482 1.727552"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translate" " -type \"double3\" 0.740445 0.360482 2.704989"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translate" " -type \"double3\" 0.740445 0.360482 3.682425"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotate" " -type \"double3\" 180 0 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scale" " -type \"double3\" 0.893817 0.893817 0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translate" " -type \"double3\" -0.740596 2.156221 3.374191"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotate" " -type \"double3\" 152.82637 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translate" " -type \"double3\" -0.740596 1.662469 4.221696"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotate" " -type \"double3\" 148.612692 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translate" " -type \"double3\" -0.740596 0.974319 4.426617"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotate" " -type \"double3\" 74.561257 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translate" " -type \"double3\" -0.740596 0.360482 3.904841"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translate" " -type \"double3\" -0.740596 0.360482 2.927405"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translate" " -type \"double3\" -0.740596 0.360482 1.949969"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translate" " -type \"double3\" -0.740596 0.360482 0.972532"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translate" " -type \"double3\" -0.740596 0.360482 -0.00490393"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translate" " -type \"double3\" -0.740596 0.360482 -0.98234"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translate" " -type \"double3\" -0.740596 0.360482 -1.959776"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translate" " -type \"double3\" -0.740596 0.360482 -2.937213"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translate" " -type \"double3\" -0.740596 0.360482 -3.914649"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translate" " -type \"double3\" -0.740596 0.360482 -4.892085"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotate" " -type \"double3\" 0 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translate" " -type \"double3\" -0.740596 0.664251 -5.529688"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotate" " -type \"double3\" -78.605434 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translate" " -type \"double3\" -0.740596 1.474073 -5.499763"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotate" " -type \"double3\" 27.44369 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translate" " -type \"double3\" -0.740596 1.924439 -4.632539"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotate" " -type \"double3\" 27.44369 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translate" " -type \"double3\" -0.740596 2.374804 -3.765316"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotate" " -type \"double3\" 27.44369 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translate" " -type \"double3\" -0.740596 2.818561 -2.910817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotate" " -type \"double3\" 27.44369 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translate" " -type \"double3\" -0.740596 3.050656 -2.12289"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translate" " -type \"double3\" -0.740596 3.050656 -1.150227"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translate" " -type \"double3\" -0.740596 3.050656 -0.177564"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translate" " -type \"double3\" -0.740596 3.050656 0.795099"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotate" " -type \"double3\" 0 180 0"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translate" " -type \"double3\" -0.741585 2.953546 1.639622"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotate" " -type \"double3\" 161.793326 0.0545059 -179.49349"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104" "scaleZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "visibility" " -av 1"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translate" " -type \"double3\" -0.740596 2.603864 2.502186"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "translateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotate" " -type \"double3\" 152.82637 0 180"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotateX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "rotateZ" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scale" " -type \"double3\" 0.894 0.893817 -0.893817"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scaleX" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scaleY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103" "scaleZ" " -av"
		
		2 "rigw:_mesh" "displayType" " 2"
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateX" 
		"rigwRN.placeHolderList[179]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateY" 
		"rigwRN.placeHolderList[180]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.translateZ" 
		"rigwRN.placeHolderList[181]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateX" 
		"rigwRN.placeHolderList[182]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateY" 
		"rigwRN.placeHolderList[183]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.rotateZ" 
		"rigwRN.placeHolderList[184]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.visibility" 
		"rigwRN.placeHolderList[185]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleX" 
		"rigwRN.placeHolderList[186]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleY" 
		"rigwRN.placeHolderList[187]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo72.scaleZ" 
		"rigwRN.placeHolderList[188]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateX" 
		"rigwRN.placeHolderList[189]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateY" 
		"rigwRN.placeHolderList[190]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.translateZ" 
		"rigwRN.placeHolderList[191]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateX" 
		"rigwRN.placeHolderList[192]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateY" 
		"rigwRN.placeHolderList[193]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.rotateZ" 
		"rigwRN.placeHolderList[194]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.visibility" 
		"rigwRN.placeHolderList[195]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleX" 
		"rigwRN.placeHolderList[196]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleY" 
		"rigwRN.placeHolderList[197]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo73.scaleZ" 
		"rigwRN.placeHolderList[198]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateX" 
		"rigwRN.placeHolderList[199]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateY" 
		"rigwRN.placeHolderList[200]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.translateZ" 
		"rigwRN.placeHolderList[201]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.visibility" 
		"rigwRN.placeHolderList[202]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateX" 
		"rigwRN.placeHolderList[203]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateY" 
		"rigwRN.placeHolderList[204]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.rotateZ" 
		"rigwRN.placeHolderList[205]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleX" 
		"rigwRN.placeHolderList[206]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleY" 
		"rigwRN.placeHolderList[207]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo74.scaleZ" 
		"rigwRN.placeHolderList[208]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateX" 
		"rigwRN.placeHolderList[209]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateY" 
		"rigwRN.placeHolderList[210]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.translateZ" 
		"rigwRN.placeHolderList[211]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.visibility" 
		"rigwRN.placeHolderList[212]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateX" 
		"rigwRN.placeHolderList[213]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateY" 
		"rigwRN.placeHolderList[214]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.rotateZ" 
		"rigwRN.placeHolderList[215]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleX" 
		"rigwRN.placeHolderList[216]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleY" 
		"rigwRN.placeHolderList[217]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo75.scaleZ" 
		"rigwRN.placeHolderList[218]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateX" 
		"rigwRN.placeHolderList[219]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateY" 
		"rigwRN.placeHolderList[220]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.translateZ" 
		"rigwRN.placeHolderList[221]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.visibility" 
		"rigwRN.placeHolderList[222]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateX" 
		"rigwRN.placeHolderList[223]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateY" 
		"rigwRN.placeHolderList[224]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.rotateZ" 
		"rigwRN.placeHolderList[225]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleX" 
		"rigwRN.placeHolderList[226]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleY" 
		"rigwRN.placeHolderList[227]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo76.scaleZ" 
		"rigwRN.placeHolderList[228]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateX" 
		"rigwRN.placeHolderList[229]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateY" 
		"rigwRN.placeHolderList[230]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.translateZ" 
		"rigwRN.placeHolderList[231]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.visibility" 
		"rigwRN.placeHolderList[232]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateX" 
		"rigwRN.placeHolderList[233]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateY" 
		"rigwRN.placeHolderList[234]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.rotateZ" 
		"rigwRN.placeHolderList[235]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleX" 
		"rigwRN.placeHolderList[236]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleY" 
		"rigwRN.placeHolderList[237]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo83.scaleZ" 
		"rigwRN.placeHolderList[238]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateX" 
		"rigwRN.placeHolderList[239]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateY" 
		"rigwRN.placeHolderList[240]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.translateZ" 
		"rigwRN.placeHolderList[241]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.visibility" 
		"rigwRN.placeHolderList[242]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateX" 
		"rigwRN.placeHolderList[243]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateY" 
		"rigwRN.placeHolderList[244]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.rotateZ" 
		"rigwRN.placeHolderList[245]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleX" 
		"rigwRN.placeHolderList[246]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleY" 
		"rigwRN.placeHolderList[247]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo82.scaleZ" 
		"rigwRN.placeHolderList[248]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateX" 
		"rigwRN.placeHolderList[249]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateY" 
		"rigwRN.placeHolderList[250]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.translateZ" 
		"rigwRN.placeHolderList[251]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.visibility" 
		"rigwRN.placeHolderList[252]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateX" 
		"rigwRN.placeHolderList[253]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateY" 
		"rigwRN.placeHolderList[254]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.rotateZ" 
		"rigwRN.placeHolderList[255]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleX" 
		"rigwRN.placeHolderList[256]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleY" 
		"rigwRN.placeHolderList[257]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo81.scaleZ" 
		"rigwRN.placeHolderList[258]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateX" 
		"rigwRN.placeHolderList[259]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateY" 
		"rigwRN.placeHolderList[260]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.translateZ" 
		"rigwRN.placeHolderList[261]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.visibility" 
		"rigwRN.placeHolderList[262]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateX" 
		"rigwRN.placeHolderList[263]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateY" 
		"rigwRN.placeHolderList[264]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.rotateZ" 
		"rigwRN.placeHolderList[265]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleX" 
		"rigwRN.placeHolderList[266]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleY" 
		"rigwRN.placeHolderList[267]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo80.scaleZ" 
		"rigwRN.placeHolderList[268]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateX" 
		"rigwRN.placeHolderList[269]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateY" 
		"rigwRN.placeHolderList[270]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.translateZ" 
		"rigwRN.placeHolderList[271]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.visibility" 
		"rigwRN.placeHolderList[272]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateX" 
		"rigwRN.placeHolderList[273]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateY" 
		"rigwRN.placeHolderList[274]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.rotateZ" 
		"rigwRN.placeHolderList[275]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleX" 
		"rigwRN.placeHolderList[276]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleY" 
		"rigwRN.placeHolderList[277]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo79.scaleZ" 
		"rigwRN.placeHolderList[278]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateX" 
		"rigwRN.placeHolderList[279]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateY" 
		"rigwRN.placeHolderList[280]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.translateZ" 
		"rigwRN.placeHolderList[281]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.visibility" 
		"rigwRN.placeHolderList[282]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateX" 
		"rigwRN.placeHolderList[283]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateY" 
		"rigwRN.placeHolderList[284]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.rotateZ" 
		"rigwRN.placeHolderList[285]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleX" 
		"rigwRN.placeHolderList[286]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleY" 
		"rigwRN.placeHolderList[287]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo58.scaleZ" 
		"rigwRN.placeHolderList[288]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateX" 
		"rigwRN.placeHolderList[289]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateY" 
		"rigwRN.placeHolderList[290]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.translateZ" 
		"rigwRN.placeHolderList[291]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.visibility" 
		"rigwRN.placeHolderList[292]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateX" 
		"rigwRN.placeHolderList[293]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateY" 
		"rigwRN.placeHolderList[294]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.rotateZ" 
		"rigwRN.placeHolderList[295]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleX" 
		"rigwRN.placeHolderList[296]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleY" 
		"rigwRN.placeHolderList[297]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo59.scaleZ" 
		"rigwRN.placeHolderList[298]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateX" 
		"rigwRN.placeHolderList[299]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateY" 
		"rigwRN.placeHolderList[300]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.translateZ" 
		"rigwRN.placeHolderList[301]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateX" 
		"rigwRN.placeHolderList[302]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateY" 
		"rigwRN.placeHolderList[303]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.rotateZ" 
		"rigwRN.placeHolderList[304]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.visibility" 
		"rigwRN.placeHolderList[305]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleX" 
		"rigwRN.placeHolderList[306]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleY" 
		"rigwRN.placeHolderList[307]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo60.scaleZ" 
		"rigwRN.placeHolderList[308]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateX" 
		"rigwRN.placeHolderList[309]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateY" 
		"rigwRN.placeHolderList[310]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.translateZ" 
		"rigwRN.placeHolderList[311]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.visibility" 
		"rigwRN.placeHolderList[312]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateX" 
		"rigwRN.placeHolderList[313]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateY" 
		"rigwRN.placeHolderList[314]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.rotateZ" 
		"rigwRN.placeHolderList[315]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleX" 
		"rigwRN.placeHolderList[316]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleY" 
		"rigwRN.placeHolderList[317]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo61.scaleZ" 
		"rigwRN.placeHolderList[318]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateX" 
		"rigwRN.placeHolderList[319]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateY" 
		"rigwRN.placeHolderList[320]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.translateZ" 
		"rigwRN.placeHolderList[321]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateX" 
		"rigwRN.placeHolderList[322]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateY" 
		"rigwRN.placeHolderList[323]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.rotateZ" 
		"rigwRN.placeHolderList[324]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.visibility" 
		"rigwRN.placeHolderList[325]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleX" 
		"rigwRN.placeHolderList[326]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleY" 
		"rigwRN.placeHolderList[327]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo62.scaleZ" 
		"rigwRN.placeHolderList[328]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateX" 
		"rigwRN.placeHolderList[329]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateY" 
		"rigwRN.placeHolderList[330]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.translateZ" 
		"rigwRN.placeHolderList[331]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.visibility" 
		"rigwRN.placeHolderList[332]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateX" 
		"rigwRN.placeHolderList[333]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateY" 
		"rigwRN.placeHolderList[334]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.rotateZ" 
		"rigwRN.placeHolderList[335]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleX" 
		"rigwRN.placeHolderList[336]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleY" 
		"rigwRN.placeHolderList[337]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo63.scaleZ" 
		"rigwRN.placeHolderList[338]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateX" 
		"rigwRN.placeHolderList[339]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateY" 
		"rigwRN.placeHolderList[340]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.translateZ" 
		"rigwRN.placeHolderList[341]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.visibility" 
		"rigwRN.placeHolderList[342]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateX" 
		"rigwRN.placeHolderList[343]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateY" 
		"rigwRN.placeHolderList[344]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.rotateZ" 
		"rigwRN.placeHolderList[345]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleX" 
		"rigwRN.placeHolderList[346]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleY" 
		"rigwRN.placeHolderList[347]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo64.scaleZ" 
		"rigwRN.placeHolderList[348]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateX" 
		"rigwRN.placeHolderList[349]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateY" 
		"rigwRN.placeHolderList[350]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.translateZ" 
		"rigwRN.placeHolderList[351]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.visibility" 
		"rigwRN.placeHolderList[352]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateX" 
		"rigwRN.placeHolderList[353]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateY" 
		"rigwRN.placeHolderList[354]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.rotateZ" 
		"rigwRN.placeHolderList[355]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleX" 
		"rigwRN.placeHolderList[356]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleY" 
		"rigwRN.placeHolderList[357]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo65.scaleZ" 
		"rigwRN.placeHolderList[358]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateX" 
		"rigwRN.placeHolderList[359]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateY" 
		"rigwRN.placeHolderList[360]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.translateZ" 
		"rigwRN.placeHolderList[361]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.visibility" 
		"rigwRN.placeHolderList[362]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateX" 
		"rigwRN.placeHolderList[363]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateY" 
		"rigwRN.placeHolderList[364]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.rotateZ" 
		"rigwRN.placeHolderList[365]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleX" 
		"rigwRN.placeHolderList[366]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleY" 
		"rigwRN.placeHolderList[367]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo66.scaleZ" 
		"rigwRN.placeHolderList[368]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateX" 
		"rigwRN.placeHolderList[369]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateY" 
		"rigwRN.placeHolderList[370]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.translateZ" 
		"rigwRN.placeHolderList[371]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.visibility" 
		"rigwRN.placeHolderList[372]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateX" 
		"rigwRN.placeHolderList[373]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateY" 
		"rigwRN.placeHolderList[374]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.rotateZ" 
		"rigwRN.placeHolderList[375]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleX" 
		"rigwRN.placeHolderList[376]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleY" 
		"rigwRN.placeHolderList[377]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo67.scaleZ" 
		"rigwRN.placeHolderList[378]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateX" 
		"rigwRN.placeHolderList[379]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateY" 
		"rigwRN.placeHolderList[380]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.translateZ" 
		"rigwRN.placeHolderList[381]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.visibility" 
		"rigwRN.placeHolderList[382]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateX" 
		"rigwRN.placeHolderList[383]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateY" 
		"rigwRN.placeHolderList[384]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.rotateZ" 
		"rigwRN.placeHolderList[385]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleX" 
		"rigwRN.placeHolderList[386]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleY" 
		"rigwRN.placeHolderList[387]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo68.scaleZ" 
		"rigwRN.placeHolderList[388]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateX" 
		"rigwRN.placeHolderList[389]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateY" 
		"rigwRN.placeHolderList[390]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.translateZ" 
		"rigwRN.placeHolderList[391]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.visibility" 
		"rigwRN.placeHolderList[392]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateX" 
		"rigwRN.placeHolderList[393]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateY" 
		"rigwRN.placeHolderList[394]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.rotateZ" 
		"rigwRN.placeHolderList[395]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleX" 
		"rigwRN.placeHolderList[396]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleY" 
		"rigwRN.placeHolderList[397]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo69.scaleZ" 
		"rigwRN.placeHolderList[398]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateX" 
		"rigwRN.placeHolderList[399]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateY" 
		"rigwRN.placeHolderList[400]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.translateZ" 
		"rigwRN.placeHolderList[401]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.visibility" 
		"rigwRN.placeHolderList[402]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateX" 
		"rigwRN.placeHolderList[403]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateY" 
		"rigwRN.placeHolderList[404]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.rotateZ" 
		"rigwRN.placeHolderList[405]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleX" 
		"rigwRN.placeHolderList[406]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleY" 
		"rigwRN.placeHolderList[407]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo70.scaleZ" 
		"rigwRN.placeHolderList[408]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateX" 
		"rigwRN.placeHolderList[409]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateY" 
		"rigwRN.placeHolderList[410]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.translateZ" 
		"rigwRN.placeHolderList[411]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.visibility" 
		"rigwRN.placeHolderList[412]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateX" 
		"rigwRN.placeHolderList[413]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateY" 
		"rigwRN.placeHolderList[414]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.rotateZ" 
		"rigwRN.placeHolderList[415]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleX" 
		"rigwRN.placeHolderList[416]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleY" 
		"rigwRN.placeHolderList[417]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackL|rigw:mesh:robo71.scaleZ" 
		"rigwRN.placeHolderList[418]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateX" 
		"rigwRN.placeHolderList[419]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateY" 
		"rigwRN.placeHolderList[420]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.translateZ" 
		"rigwRN.placeHolderList[421]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.visibility" 
		"rigwRN.placeHolderList[422]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateX" 
		"rigwRN.placeHolderList[423]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateY" 
		"rigwRN.placeHolderList[424]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.rotateZ" 
		"rigwRN.placeHolderList[425]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleX" 
		"rigwRN.placeHolderList[426]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleY" 
		"rigwRN.placeHolderList[427]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo102.scaleZ" 
		"rigwRN.placeHolderList[428]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateX" 
		"rigwRN.placeHolderList[429]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateY" 
		"rigwRN.placeHolderList[430]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.translateZ" 
		"rigwRN.placeHolderList[431]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateX" 
		"rigwRN.placeHolderList[432]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateY" 
		"rigwRN.placeHolderList[433]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.rotateZ" 
		"rigwRN.placeHolderList[434]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.visibility" 
		"rigwRN.placeHolderList[435]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleX" 
		"rigwRN.placeHolderList[436]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleY" 
		"rigwRN.placeHolderList[437]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo101.scaleZ" 
		"rigwRN.placeHolderList[438]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateX" 
		"rigwRN.placeHolderList[439]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateY" 
		"rigwRN.placeHolderList[440]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.translateZ" 
		"rigwRN.placeHolderList[441]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateX" 
		"rigwRN.placeHolderList[442]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateY" 
		"rigwRN.placeHolderList[443]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.rotateZ" 
		"rigwRN.placeHolderList[444]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.visibility" 
		"rigwRN.placeHolderList[445]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleX" 
		"rigwRN.placeHolderList[446]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleY" 
		"rigwRN.placeHolderList[447]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo100.scaleZ" 
		"rigwRN.placeHolderList[448]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateX" 
		"rigwRN.placeHolderList[449]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateY" 
		"rigwRN.placeHolderList[450]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.translateZ" 
		"rigwRN.placeHolderList[451]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.visibility" 
		"rigwRN.placeHolderList[452]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateX" 
		"rigwRN.placeHolderList[453]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateY" 
		"rigwRN.placeHolderList[454]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.rotateZ" 
		"rigwRN.placeHolderList[455]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleX" 
		"rigwRN.placeHolderList[456]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleY" 
		"rigwRN.placeHolderList[457]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo99.scaleZ" 
		"rigwRN.placeHolderList[458]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateX" 
		"rigwRN.placeHolderList[459]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateY" 
		"rigwRN.placeHolderList[460]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.translateZ" 
		"rigwRN.placeHolderList[461]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.visibility" 
		"rigwRN.placeHolderList[462]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateX" 
		"rigwRN.placeHolderList[463]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateY" 
		"rigwRN.placeHolderList[464]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.rotateZ" 
		"rigwRN.placeHolderList[465]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleX" 
		"rigwRN.placeHolderList[466]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleY" 
		"rigwRN.placeHolderList[467]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo98.scaleZ" 
		"rigwRN.placeHolderList[468]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateX" 
		"rigwRN.placeHolderList[469]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateY" 
		"rigwRN.placeHolderList[470]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.translateZ" 
		"rigwRN.placeHolderList[471]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.visibility" 
		"rigwRN.placeHolderList[472]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateX" 
		"rigwRN.placeHolderList[473]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateY" 
		"rigwRN.placeHolderList[474]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.rotateZ" 
		"rigwRN.placeHolderList[475]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleX" 
		"rigwRN.placeHolderList[476]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleY" 
		"rigwRN.placeHolderList[477]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo97.scaleZ" 
		"rigwRN.placeHolderList[478]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateX" 
		"rigwRN.placeHolderList[479]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateY" 
		"rigwRN.placeHolderList[480]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.translateZ" 
		"rigwRN.placeHolderList[481]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.visibility" 
		"rigwRN.placeHolderList[482]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateX" 
		"rigwRN.placeHolderList[483]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateY" 
		"rigwRN.placeHolderList[484]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.rotateZ" 
		"rigwRN.placeHolderList[485]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleX" 
		"rigwRN.placeHolderList[486]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleY" 
		"rigwRN.placeHolderList[487]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo96.scaleZ" 
		"rigwRN.placeHolderList[488]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateX" 
		"rigwRN.placeHolderList[489]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateY" 
		"rigwRN.placeHolderList[490]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.translateZ" 
		"rigwRN.placeHolderList[491]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.visibility" 
		"rigwRN.placeHolderList[492]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateX" 
		"rigwRN.placeHolderList[493]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateY" 
		"rigwRN.placeHolderList[494]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.rotateZ" 
		"rigwRN.placeHolderList[495]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleX" 
		"rigwRN.placeHolderList[496]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleY" 
		"rigwRN.placeHolderList[497]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo95.scaleZ" 
		"rigwRN.placeHolderList[498]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateX" 
		"rigwRN.placeHolderList[499]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateY" 
		"rigwRN.placeHolderList[500]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.translateZ" 
		"rigwRN.placeHolderList[501]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.visibility" 
		"rigwRN.placeHolderList[502]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateX" 
		"rigwRN.placeHolderList[503]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateY" 
		"rigwRN.placeHolderList[504]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.rotateZ" 
		"rigwRN.placeHolderList[505]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleX" 
		"rigwRN.placeHolderList[506]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleY" 
		"rigwRN.placeHolderList[507]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo94.scaleZ" 
		"rigwRN.placeHolderList[508]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateX" 
		"rigwRN.placeHolderList[509]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateY" 
		"rigwRN.placeHolderList[510]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.translateZ" 
		"rigwRN.placeHolderList[511]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.visibility" 
		"rigwRN.placeHolderList[512]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateX" 
		"rigwRN.placeHolderList[513]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateY" 
		"rigwRN.placeHolderList[514]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.rotateZ" 
		"rigwRN.placeHolderList[515]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleX" 
		"rigwRN.placeHolderList[516]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleY" 
		"rigwRN.placeHolderList[517]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo93.scaleZ" 
		"rigwRN.placeHolderList[518]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateX" 
		"rigwRN.placeHolderList[519]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateY" 
		"rigwRN.placeHolderList[520]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.translateZ" 
		"rigwRN.placeHolderList[521]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.visibility" 
		"rigwRN.placeHolderList[522]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateX" 
		"rigwRN.placeHolderList[523]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateY" 
		"rigwRN.placeHolderList[524]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.rotateZ" 
		"rigwRN.placeHolderList[525]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleX" 
		"rigwRN.placeHolderList[526]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleY" 
		"rigwRN.placeHolderList[527]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo92.scaleZ" 
		"rigwRN.placeHolderList[528]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateX" 
		"rigwRN.placeHolderList[529]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateY" 
		"rigwRN.placeHolderList[530]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.translateZ" 
		"rigwRN.placeHolderList[531]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.visibility" 
		"rigwRN.placeHolderList[532]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateX" 
		"rigwRN.placeHolderList[533]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateY" 
		"rigwRN.placeHolderList[534]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.rotateZ" 
		"rigwRN.placeHolderList[535]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleX" 
		"rigwRN.placeHolderList[536]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleY" 
		"rigwRN.placeHolderList[537]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo91.scaleZ" 
		"rigwRN.placeHolderList[538]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateX" 
		"rigwRN.placeHolderList[539]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateY" 
		"rigwRN.placeHolderList[540]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.translateZ" 
		"rigwRN.placeHolderList[541]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateX" 
		"rigwRN.placeHolderList[542]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateY" 
		"rigwRN.placeHolderList[543]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.rotateZ" 
		"rigwRN.placeHolderList[544]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.visibility" 
		"rigwRN.placeHolderList[545]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleX" 
		"rigwRN.placeHolderList[546]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleY" 
		"rigwRN.placeHolderList[547]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo90.scaleZ" 
		"rigwRN.placeHolderList[548]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateX" 
		"rigwRN.placeHolderList[549]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateY" 
		"rigwRN.placeHolderList[550]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.translateZ" 
		"rigwRN.placeHolderList[551]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.visibility" 
		"rigwRN.placeHolderList[552]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateX" 
		"rigwRN.placeHolderList[553]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateY" 
		"rigwRN.placeHolderList[554]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.rotateZ" 
		"rigwRN.placeHolderList[555]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleX" 
		"rigwRN.placeHolderList[556]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleY" 
		"rigwRN.placeHolderList[557]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo89.scaleZ" 
		"rigwRN.placeHolderList[558]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateX" 
		"rigwRN.placeHolderList[559]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateY" 
		"rigwRN.placeHolderList[560]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.translateZ" 
		"rigwRN.placeHolderList[561]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateX" 
		"rigwRN.placeHolderList[562]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateY" 
		"rigwRN.placeHolderList[563]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.rotateZ" 
		"rigwRN.placeHolderList[564]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.visibility" 
		"rigwRN.placeHolderList[565]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleX" 
		"rigwRN.placeHolderList[566]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleY" 
		"rigwRN.placeHolderList[567]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo88.scaleZ" 
		"rigwRN.placeHolderList[568]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateX" 
		"rigwRN.placeHolderList[569]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateY" 
		"rigwRN.placeHolderList[570]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.translateZ" 
		"rigwRN.placeHolderList[571]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.visibility" 
		"rigwRN.placeHolderList[572]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateX" 
		"rigwRN.placeHolderList[573]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateY" 
		"rigwRN.placeHolderList[574]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.rotateZ" 
		"rigwRN.placeHolderList[575]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleX" 
		"rigwRN.placeHolderList[576]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleY" 
		"rigwRN.placeHolderList[577]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo87.scaleZ" 
		"rigwRN.placeHolderList[578]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateX" 
		"rigwRN.placeHolderList[579]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateY" 
		"rigwRN.placeHolderList[580]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.translateZ" 
		"rigwRN.placeHolderList[581]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.visibility" 
		"rigwRN.placeHolderList[582]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateX" 
		"rigwRN.placeHolderList[583]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateY" 
		"rigwRN.placeHolderList[584]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.rotateZ" 
		"rigwRN.placeHolderList[585]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleX" 
		"rigwRN.placeHolderList[586]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleY" 
		"rigwRN.placeHolderList[587]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo86.scaleZ" 
		"rigwRN.placeHolderList[588]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateX" 
		"rigwRN.placeHolderList[589]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateY" 
		"rigwRN.placeHolderList[590]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.translateZ" 
		"rigwRN.placeHolderList[591]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.visibility" 
		"rigwRN.placeHolderList[592]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateX" 
		"rigwRN.placeHolderList[593]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateY" 
		"rigwRN.placeHolderList[594]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.rotateZ" 
		"rigwRN.placeHolderList[595]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleX" 
		"rigwRN.placeHolderList[596]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleY" 
		"rigwRN.placeHolderList[597]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo107.scaleZ" 
		"rigwRN.placeHolderList[598]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateX" 
		"rigwRN.placeHolderList[599]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateY" 
		"rigwRN.placeHolderList[600]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.translateZ" 
		"rigwRN.placeHolderList[601]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.visibility" 
		"rigwRN.placeHolderList[602]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateX" 
		"rigwRN.placeHolderList[603]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateY" 
		"rigwRN.placeHolderList[604]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.rotateZ" 
		"rigwRN.placeHolderList[605]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleX" 
		"rigwRN.placeHolderList[606]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleY" 
		"rigwRN.placeHolderList[607]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo108.scaleZ" 
		"rigwRN.placeHolderList[608]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateX" 
		"rigwRN.placeHolderList[609]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateY" 
		"rigwRN.placeHolderList[610]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.translateZ" 
		"rigwRN.placeHolderList[611]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.visibility" 
		"rigwRN.placeHolderList[612]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateX" 
		"rigwRN.placeHolderList[613]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateY" 
		"rigwRN.placeHolderList[614]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.rotateZ" 
		"rigwRN.placeHolderList[615]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleX" 
		"rigwRN.placeHolderList[616]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleY" 
		"rigwRN.placeHolderList[617]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo109.scaleZ" 
		"rigwRN.placeHolderList[618]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateX" 
		"rigwRN.placeHolderList[619]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateY" 
		"rigwRN.placeHolderList[620]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.translateZ" 
		"rigwRN.placeHolderList[621]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.visibility" 
		"rigwRN.placeHolderList[622]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateX" 
		"rigwRN.placeHolderList[623]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateY" 
		"rigwRN.placeHolderList[624]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.rotateZ" 
		"rigwRN.placeHolderList[625]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleX" 
		"rigwRN.placeHolderList[626]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleY" 
		"rigwRN.placeHolderList[627]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo110.scaleZ" 
		"rigwRN.placeHolderList[628]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateX" 
		"rigwRN.placeHolderList[629]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateY" 
		"rigwRN.placeHolderList[630]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.translateZ" 
		"rigwRN.placeHolderList[631]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.visibility" 
		"rigwRN.placeHolderList[632]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateX" 
		"rigwRN.placeHolderList[633]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateY" 
		"rigwRN.placeHolderList[634]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.rotateZ" 
		"rigwRN.placeHolderList[635]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleX" 
		"rigwRN.placeHolderList[636]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleY" 
		"rigwRN.placeHolderList[637]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo111.scaleZ" 
		"rigwRN.placeHolderList[638]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateX" 
		"rigwRN.placeHolderList[639]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateY" 
		"rigwRN.placeHolderList[640]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.translateZ" 
		"rigwRN.placeHolderList[641]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.visibility" 
		"rigwRN.placeHolderList[642]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateX" 
		"rigwRN.placeHolderList[643]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateY" 
		"rigwRN.placeHolderList[644]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.rotateZ" 
		"rigwRN.placeHolderList[645]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleX" 
		"rigwRN.placeHolderList[646]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleY" 
		"rigwRN.placeHolderList[647]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo104.scaleZ" 
		"rigwRN.placeHolderList[648]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateX" 
		"rigwRN.placeHolderList[649]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateY" 
		"rigwRN.placeHolderList[650]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.translateZ" 
		"rigwRN.placeHolderList[651]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.visibility" 
		"rigwRN.placeHolderList[652]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateX" 
		"rigwRN.placeHolderList[653]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateY" 
		"rigwRN.placeHolderList[654]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.rotateZ" 
		"rigwRN.placeHolderList[655]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleX" 
		"rigwRN.placeHolderList[656]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleY" 
		"rigwRN.placeHolderList[657]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:TrackR|rigw:mesh:robo103.scaleZ" 
		"rigwRN.placeHolderList[658]" ""
		"rigw:rigRN" 406
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "visibility" " -av 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scale" " -type \"double3\" 1.8 1.8 1.8"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translate" " -type \"double3\" 0 0.0860172 0.131701"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"CenterBtwFeet" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" -10.216078 -12.316063 23.837782"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"Global" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotate" " -type \"double3\" 0 0 -33.36369"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotate" " -type \"double3\" 5.047571 7.017677 8.922846"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L" 
		"Global" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 -23.632635"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotate" " -type \"double3\" 123.25697 67.640021 -124.664778"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translate" " -type \"double3\" 1.043829 0.0845126 0.128291"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotate" " -type \"double3\" 0 -3.253483 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translate" " -type \"double3\" 1.435517 -0.619525 -0.45237"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotate" " -type \"double3\" 0 -14.12842 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotate" " -type \"double3\" -22.028843 -16.267797 -39.607339"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" -0.609696 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0.571318 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"IKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"IKVis" " -av -k 1 1"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateX" "rigwRN.placeHolderList[1]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateY" "rigwRN.placeHolderList[2]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateZ" "rigwRN.placeHolderList[3]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.visibility" "rigwRN.placeHolderList[4]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateX" "rigwRN.placeHolderList[5]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateY" "rigwRN.placeHolderList[6]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateZ" "rigwRN.placeHolderList[7]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleX" "rigwRN.placeHolderList[8]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleY" "rigwRN.placeHolderList[9]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleZ" "rigwRN.placeHolderList[10]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateX" 
		"rigwRN.placeHolderList[11]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateY" 
		"rigwRN.placeHolderList[12]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateZ" 
		"rigwRN.placeHolderList[13]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateX" 
		"rigwRN.placeHolderList[14]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateY" 
		"rigwRN.placeHolderList[15]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateZ" 
		"rigwRN.placeHolderList[16]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateX" 
		"rigwRN.placeHolderList[17]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateY" 
		"rigwRN.placeHolderList[18]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateZ" 
		"rigwRN.placeHolderList[19]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateX" 
		"rigwRN.placeHolderList[20]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateY" 
		"rigwRN.placeHolderList[21]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateZ" 
		"rigwRN.placeHolderList[22]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateX" 
		"rigwRN.placeHolderList[23]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateY" 
		"rigwRN.placeHolderList[24]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateZ" 
		"rigwRN.placeHolderList[25]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateX" 
		"rigwRN.placeHolderList[26]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateY" 
		"rigwRN.placeHolderList[27]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateZ" 
		"rigwRN.placeHolderList[28]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateX" 
		"rigwRN.placeHolderList[29]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateY" 
		"rigwRN.placeHolderList[30]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateZ" 
		"rigwRN.placeHolderList[31]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateX" 
		"rigwRN.placeHolderList[32]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateY" 
		"rigwRN.placeHolderList[33]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateZ" 
		"rigwRN.placeHolderList[34]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[35]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[36]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[37]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[38]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[39]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[40]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[41]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateX" 
		"rigwRN.placeHolderList[42]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateY" 
		"rigwRN.placeHolderList[43]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.translateZ" 
		"rigwRN.placeHolderList[44]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateX" 
		"rigwRN.placeHolderList[45]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateY" 
		"rigwRN.placeHolderList[46]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateZ" 
		"rigwRN.placeHolderList[47]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateX" 
		"rigwRN.placeHolderList[48]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateY" 
		"rigwRN.placeHolderList[49]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.translateZ" 
		"rigwRN.placeHolderList[50]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateX" 
		"rigwRN.placeHolderList[51]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateY" 
		"rigwRN.placeHolderList[52]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateZ" 
		"rigwRN.placeHolderList[53]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.Global" 
		"rigwRN.placeHolderList[54]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[55]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[56]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[57]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateX" 
		"rigwRN.placeHolderList[58]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateY" 
		"rigwRN.placeHolderList[59]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R.rotateZ" 
		"rigwRN.placeHolderList[60]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateX" 
		"rigwRN.placeHolderList[61]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateY" 
		"rigwRN.placeHolderList[62]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.translateZ" 
		"rigwRN.placeHolderList[63]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateX" 
		"rigwRN.placeHolderList[64]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateY" 
		"rigwRN.placeHolderList[65]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow5_R|rigw:rig:FKExtraElbow5_R|rigw:rig:FKElbow5_R|rigw:rig:FKXElbow5_R|rigw:rig:FKOffsetWrist1_R|rigw:rig:FKExtraWrist1_R|rigw:rig:FKWrist1_R.rotateZ" 
		"rigwRN.placeHolderList[66]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.Global" 
		"rigwRN.placeHolderList[67]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateX" 
		"rigwRN.placeHolderList[68]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateY" 
		"rigwRN.placeHolderList[69]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L.rotateZ" 
		"rigwRN.placeHolderList[70]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[71]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[72]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[73]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateX" 
		"rigwRN.placeHolderList[74]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateY" 
		"rigwRN.placeHolderList[75]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateZ" 
		"rigwRN.placeHolderList[76]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateX" 
		"rigwRN.placeHolderList[77]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateY" 
		"rigwRN.placeHolderList[78]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[79]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateX" 
		"rigwRN.placeHolderList[80]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateY" 
		"rigwRN.placeHolderList[81]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[82]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateX" 
		"rigwRN.placeHolderList[83]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateY" 
		"rigwRN.placeHolderList[84]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L|rigw:rig:FKXMiddleFinger2_L|rigw:rig:FKOffsetMiddleFinger3_L|rigw:rig:FKExtraMiddleFinger3_L|rigw:rig:FKMiddleFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[85]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateX" 
		"rigwRN.placeHolderList[86]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateY" 
		"rigwRN.placeHolderList[87]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[88]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateX" 
		"rigwRN.placeHolderList[89]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateY" 
		"rigwRN.placeHolderList[90]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[91]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateX" 
		"rigwRN.placeHolderList[92]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateY" 
		"rigwRN.placeHolderList[93]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L|rigw:rig:FKXIndexFinger2_L|rigw:rig:FKOffsetIndexFinger3_L|rigw:rig:FKExtraIndexFinger3_L|rigw:rig:FKIndexFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[94]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateX" 
		"rigwRN.placeHolderList[95]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateY" 
		"rigwRN.placeHolderList[96]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[97]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateX" 
		"rigwRN.placeHolderList[98]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateY" 
		"rigwRN.placeHolderList[99]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[100]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateX" 
		"rigwRN.placeHolderList[101]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateY" 
		"rigwRN.placeHolderList[102]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L|rigw:rig:FKXThumbFinger2_L|rigw:rig:FKOffsetThumbFinger3_L|rigw:rig:FKExtraThumbFinger3_L|rigw:rig:FKThumbFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[103]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateX" 
		"rigwRN.placeHolderList[104]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateY" 
		"rigwRN.placeHolderList[105]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[106]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateX" 
		"rigwRN.placeHolderList[107]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateY" 
		"rigwRN.placeHolderList[108]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[109]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateX" 
		"rigwRN.placeHolderList[110]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateY" 
		"rigwRN.placeHolderList[111]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder1_L|rigw:rig:FKGlobalStaticShoulder1_L|rigw:rig:FKGlobalShoulder1_L|rigw:rig:FKExtraShoulder1_L|rigw:rig:FKShoulder1_L|rigw:rig:FKXShoulder1_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L|rigw:rig:FKXWrist_L|rigw:rig:FKOffsetPinkyFinger1_L|rigw:rig:FKExtraPinkyFinger1_L|rigw:rig:FKPinkyFinger1_L|rigw:rig:FKXPinkyFinger1_L|rigw:rig:FKOffsetPinkyFinger2_L|rigw:rig:FKExtraPinkyFinger2_L|rigw:rig:FKPinkyFinger2_L|rigw:rig:FKXPinkyFinger2_L|rigw:rig:FKOffsetPinkyFinger3_L|rigw:rig:FKExtraPinkyFinger3_L|rigw:rig:FKPinkyFinger3_L.rotateZ" 
		"rigwRN.placeHolderList[112]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateX" 
		"rigwRN.placeHolderList[113]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateY" 
		"rigwRN.placeHolderList[114]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.translateZ" 
		"rigwRN.placeHolderList[115]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateX" 
		"rigwRN.placeHolderList[116]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateY" 
		"rigwRN.placeHolderList[117]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_R|rigw:rig:FKExtraHose1_R|rigw:rig:FKHose1_R.rotateZ" 
		"rigwRN.placeHolderList[118]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateX" 
		"rigwRN.placeHolderList[119]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateY" 
		"rigwRN.placeHolderList[120]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.translateZ" 
		"rigwRN.placeHolderList[121]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateX" 
		"rigwRN.placeHolderList[122]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateY" 
		"rigwRN.placeHolderList[123]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_R|rigw:rig:FKExtraHose2_R|rigw:rig:FKHose2_R.rotateZ" 
		"rigwRN.placeHolderList[124]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateX" 
		"rigwRN.placeHolderList[125]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateY" 
		"rigwRN.placeHolderList[126]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose1_L|rigw:rig:FKExtraHose1_L|rigw:rig:FKHose1_L.rotateZ" 
		"rigwRN.placeHolderList[127]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateX" 
		"rigwRN.placeHolderList[128]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateY" 
		"rigwRN.placeHolderList[129]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetHose2_L|rigw:rig:FKExtraHose2_L|rigw:rig:FKHose2_L.rotateZ" 
		"rigwRN.placeHolderList[130]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[131]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[132]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[133]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[134]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[135]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[136]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[137]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rollAngle" 
		"rigwRN.placeHolderList[138]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.roll" 
		"rigwRN.placeHolderList[139]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[140]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[141]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[142]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[143]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateX" 
		"rigwRN.placeHolderList[144]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateY" 
		"rigwRN.placeHolderList[145]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateZ" 
		"rigwRN.placeHolderList[146]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R.rotateX" 
		"rigwRN.placeHolderList[147]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateX" 
		"rigwRN.placeHolderList[148]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateY" 
		"rigwRN.placeHolderList[149]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateZ" 
		"rigwRN.placeHolderList[150]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.follow" 
		"rigwRN.placeHolderList[151]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[152]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[153]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[154]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[155]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[156]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[157]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[158]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rollAngle" 
		"rigwRN.placeHolderList[159]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.roll" 
		"rigwRN.placeHolderList[160]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[161]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[162]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[163]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[164]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateX" 
		"rigwRN.placeHolderList[165]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateY" 
		"rigwRN.placeHolderList[166]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateZ" 
		"rigwRN.placeHolderList[167]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L.rotateX" 
		"rigwRN.placeHolderList[168]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateX" 
		"rigwRN.placeHolderList[169]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateY" 
		"rigwRN.placeHolderList[170]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateZ" 
		"rigwRN.placeHolderList[171]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.follow" 
		"rigwRN.placeHolderList[172]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKIKBlend" 
		"rigwRN.placeHolderList[173]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.IKVis" 
		"rigwRN.placeHolderList[174]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKVis" 
		"rigwRN.placeHolderList[175]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKIKBlend" 
		"rigwRN.placeHolderList[176]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.IKVis" 
		"rigwRN.placeHolderList[177]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKVis" 
		"rigwRN.placeHolderList[178]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -sn "imrVerbosity" -ln "imrVerbosity" -dv 3 -at "short";
	addAttr -ci true -sn "imrThreads" -ln "imrThreads" -dv 1 -at "short";
	addAttr -ci true -sn "imrThreadsAdjust" -ln "imrThreadsAdjust" -dv 1 -min 0 -max 
		1 -at "bool";
	addAttr -ci true -sn "imrTaskOrder" -ln "imrTaskOrder" -dv 1 -at "short";
	addAttr -ci true -sn "imrTaskSize" -ln "imrTaskSize" -at "short";
	addAttr -ci true -sn "imrTaskAdjust" -ln "imrTaskAdjust" -dv 1 -at "short";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
	setAttr ".imrThreads" 4;
	setAttr ".imrThreadsAdjust" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 7 -ast 1 -aet 60 ";
	setAttr ".st" 6;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0.00082916948397379945 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -0.20761128658066907 3 0.27699645740847301
		 4 0.49325661750423649 5 0.70951677759999998 6 0.70951677759999998 7 0.70951677759999998;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.15233640372753143 0.15233635902404785 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.049254857003688812 -0.04925483837723732 
		0.98832869529724121 0.98832869529724121 0 0;
createNode animCurveTL -n "rigw:rig:FKBody_M_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -0.68997195150000001 5 -1.379943903
		 6 -1.379943903 7 -1.379943903;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 0.048254873603582382 0.048254862427711487 
		1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 -0.99883502721786499 -0.99883502721786499 
		0 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -10.11715387988369 3 -13.238826038106653
		 4 16.060415725946672 5 45.359657489999996 6 46.295244996229485 7 46.529141872786845;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.065046563744544983 0.065046548843383789 0.89803552627563477 0.99258410930633545;
	setAttr -s 7 ".kiy[0:6]"  0 0.013205680064857006 0.013205681927502155 
		0.9978821873664856 0.9978821873664856 0.43992286920547485 0.12155991047620773;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 -13.443088773087796 4 -9.5149792580438977
		 5 -5.5868697430000003 6 -5.5868697430000003 7 -5.5868697430000003;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 0.43725970387458801 0.43725964426994324 
		1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.89933526515960693 0.89933532476425171 
		0 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 6.7326365044054262 3 -3.773050389413132
		 4 1.6946823678315359 5 -4.0056906669999996 6 -4.0056906669999996 7 -4.0056906669999996;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 0.32975861430168152 0.31768476963043213 
		1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.94406527280807495 -0.9481964111328125 
		0 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.086017200000000002 2 -0.037697547889670308
		 3 0.086017200000000002 4 -0.22285558859999999 5 -0.65287701490597749 6 -1.5129327682897296
		 7 -1.7713753042160276;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.10729630291461945 0.077283672988414764 0.038728099316358566 0.12791815400123596;
	setAttr -s 7 ".kiy[0:6]"  0 -0.037697549909353256 -0.037697549909353256 
		-0.99422711133956909 -0.99700915813446045 -0.99924981594085693 -0.99178475141525269;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.131701 2 0 3 0.131701 4 0.131701 5 0.131701
		 6 0.131701 7 0.131701;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 1.4723719135727238;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 0.79197227954864502;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0.610556960105896;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0.11031661132544462;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 0.99833589792251587;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0.057665526866912842;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -0.30000347290204699 3 0 4 0 5 0 6 0
		 7 0.039951131622656698;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 0.99978131055831909;
	setAttr -s 7 ".kiy[0:6]"  0 -0.0052360487170517445 -0.0052360468544065952 
		0 0 0 0.020913790911436081;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 5.0475705990000002 2 -19.107747765700992
		 3 -24.389275862894646 4 13.923080498298551 5 94.026689028268365 6 61.542549138494621
		 7 61.542549138494621;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.049787886440753937 0.023835586383938789 0.058692235499620438 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.041108999401330948 0.041108991950750351 
		0.99875986576080322 0.99971586465835571 -0.99827611446380615 0;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.0176766390000003 2 -28.723199186656728
		 3 -1.6249975352299471 4 -68.877108417798453 5 -53.841399080389948 6 -33.950875273619886
		 7 -33.950875273619886;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.028387062251567841 0.12600907683372498 0.095578961074352264 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.015248852781951427 0.015248850919306278 
		-0.99959695339202881 0.99202901124954224 0.9954218864440918 0;
createNode animCurveTA -n "rigw:rig:FKShoulder1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 8.9228458629999992 2 -8.2388636367743135
		 3 -27.311528516217216 4 27.167803436542822 5 96.915838336547125 6 64.880105864305193
		 7 64.880105864305193;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.035035070031881332 0.027372004464268684 0.059510871767997742 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.083044253289699554 0.083044245839118958 
		0.99938607215881348 0.99962526559829712 -0.99822765588760376 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 15.218826252810103 3 7.0632829990720234
		 4 1.4964090430000001 5 6.1127706211912951 6 6.8967972881131985 7 6.8967972881131985;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.3245093822479248 0.38229033350944519 0.92508447170257568 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.0047212955541908741 -0.0047212964855134487 
		-0.94588249921798706 0.9240422248840332 0.3797614574432373 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 6.2735731332743647 3 2.9116583447103723
		 4 0.81225907450000001 5 -2.6228610286114087 6 3.2498087483150129 7 3.2498087483150129;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.67292642593383789 0.48592662811279297 0.30926790833473206 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.0063288821838796139 0.0063288812525570393 
		-0.73970943689346313 -0.87399965524673462 0.95097494125366211 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -23.632634769999999 2 -13.28084556477484
		 3 -31.607245380336941 4 -26.3651738836767 5 5.8053475659058256 6 -72.388419816643079
		 7 -72.388419816643079;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.34232109785079956 0.059262402355670929 0.024417415261268616 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.14894311130046844 -0.14894314110279083 
		0.93958300352096558 0.99824237823486328 -0.99970179796218872 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -10.216077629999999 2 45.381709199963382
		 3 24.720860904939066 4 20.636092605656629 5 13.843799410548939 6 0.25900924949999998
		 7 0.25900924949999998;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.42354720830917358 0.2706834077835083 0.13921895623207092 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.050696391612291336 -0.050696391612291336 
		-0.90587401390075684 -0.96266841888427734 -0.99026161432266235 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -12.316062799999999 2 13.99666895643692
		 3 10.126048671227009 4 4.1552361588597746 5 2.2679942801806958 6 -1.506546095 7 -1.506546095;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.30465990304946899 0.71130597591400146 0.45148041844367981 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.01955043151974678 -0.01955043151974678 
		-0.95246124267578125 -0.70288240909576416 -0.89228105545043945 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 23.837782050000001 2 18.624755062073323
		 3 30.194888690900978 4 28.953411994617859 5 35.363020796349588 6 48.182430689999997
		 7 48.182430689999997;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.83842998743057251 0.28556090593338013 0.14735549688339233 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.027860365808010101 0.027860362082719803 
		-0.54500937461853027 0.95836055278778076 0.98908358812332153 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 3.1073663060161345 3 -1.0659080142662145
		 4 3.4056439255000002 5 4.5408472151869148 6 6.8112878510000003 7 6.8112878510000003;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 0.3927861750125885 0.85961270332336426 
		0.64372330904006958 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.91962981224060059 0.51094615459442139 
		0.7652583122253418 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 5.0101750147426038 3 2.9053966704298184
		 4 0.11154255355000001 5 0.1487230329248215 6 0.22308510710000001 7 0.22308510710000001;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 0.56433671712875366 0.99981057643890381 
		0.99924284219741821 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 -0.82554471492767334 0.019463967531919479 
		0.038906414061784744 0;
createNode animCurveTA -n "rigw:rig:FKElbow5_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -33.363690480000002 2 -36.668088982723539
		 3 -56.418083445343925 4 -38.508625295000002 5 -40.223586416883947 6 -43.653560110000001
		 7 -43.653560110000001;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.10603848844766617 0.74405151605606079 0.48648321628570557 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.15904158353805542 -0.15904158353805542 
		0.9943619966506958 -0.66812223196029663 -0.87368988990783691 0;
createNode animCurveTU -n "rigw:rig:FKShoulder_R_Global";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKShoulder1_L_Global";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0.67861842192498323 3 0 4 5.0304165599999999
		 5 6.7072053119448007 6 10.06083312 7 10.06083312;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 0.94228363037109375 0.9422835111618042 
		0.35494187474250793 0.75147157907485962 0.49486881494522095 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.33481574058532715 -0.33481574058532715 
		0.93488836288452148 0.65976542234420776 0.86896765232086182 0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -8.748707598784442 3 14.927177483585481
		 4 10.414517862293934 5 6.943046623255495 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0.079326696693897247 0.21327905356884003 
		0.080405674874782562 0.3897537887096405 0.48202535510063171 0.26522380113601685 1;
	setAttr -s 7 ".kiy[0:6]"  0.9968487024307251 -0.97699129581451416 
		0.99676221609115601 -0.92091906070709229 -0.87615728378295898 -0.9641869068145752 
		0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -0.6358678421885996 3 0 4 0 5 0 6 0
		 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 0.94879525899887085 0.94879525899887085 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.31589150428771973 0.31589153409004211 
		0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -0.19377124209999999 5 -0.258361010229193
		 6 -0.38754248419999998 7 -0.38754248419999998;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.16953401267528534 0.45860680937767029 
		0.24985113739967346 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 -0.98552435636520386 -0.88863927125930786 
		-0.9682842493057251 0;
createNode animCurveTL -n "rigw:rig:FKHead_M_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0.0041126582014999998 5 0.0054835305598059953
		 6 0.0082253164029999996 7 0.0082253164029999996;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.99247455596923828 0.99915534257888794 
		0.99663424491882324 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.12245123833417892 0.041091438382863998 
		0.081976734101772308 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 2.3093778265 5 3.0791627374072448
		 6 -9.6165551689254656 7 -9.6165551689254656;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.033333331346511841 0.63730037212371826 
		0.92749536037445068 0.14875951409339905 1;
	setAttr -s 7 ".kiy[1:6]"  0 0.091922745108604431 0.77061545848846436 
		0.37383481860160828 -0.98887336254119873 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -6.62972378 5 -8.8396096075874002
		 6 -27.210039780352307 7 -27.210039780352307;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.27681797742843628 0.65387898683547974 
		0.10340643674135208 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 -0.96092236042022705 -0.75659912824630737 
		-0.99463915824890137 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 12.377098245000001 5 16.502756403005854
		 6 12.40922407509551 7 12.40922407509551;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.15250106155872345 0.42009317874908447 
		0.4228026270866394 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0.98830330371856689 0.90748095512390137 
		-0.90622186660766602 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 43.048374913580368 4 21.524187456790184
		 5 14.349530051818313 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.033333331346511841 0.088383615016937256 
		0.25723722577095032 0.13193215429782867 1;
	setAttr -s 7 ".kiy[1:6]"  0 0.091922745108604431 -0.99608653783798218 
		-0.96634829044342041 -0.99125874042510986 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -2.2229313839999998 5 -2.9639011022287196
		 6 -4.4458627679999996 7 -4.4458627679999996;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.033333331346511841 0.65167415142059326 
		0.93229347467422485 0.790050208568573 1;
	setAttr -s 7 ".kiy[1:6]"  0 0.091922745108604431 -0.7584991455078125 
		-0.36170271039009094 -0.61304211616516113 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -3.9834792800000001 5 -5.3112924284023997
		 6 -7.9669585600000001 7 -7.9669585600000001;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.43232432007789612 0.82106328010559082 
		0.58385759592056274 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 -0.90171808004379272 -0.57083714008331299 
		-0.81185609102249146 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -3.9855887834999999 5 -5.3141050927040556
		 6 -7.9711775669999998 7 -7.9711775669999998;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.43213826417922974 0.82092154026031494 
		0.58365386724472046 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 -0.90180736780166626 -0.57104086875915527 
		-0.81200259923934937 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 43.048374913580368 4 30.100829831790186
		 5 25.785024629677064 6 -30.373947758139362 7 -30.373947758139362;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.033333331346511841 0.14592842757701874 
		0.40467351675033569 0.033988434821367264 1;
	setAttr -s 7 ".kiy[1:6]"  0 0.14628009498119354 -0.98929512500762939 
		-0.91446119546890259 -0.99942219257354736 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 4.980616748500001 5 6.6408057292775062
		 6 6.1774428279464644 7 6.1774428279464644;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.35803788900375366 0.7547147274017334 
		0.97180736064910889 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0.93370705842971802 0.65605312585830688 
		-0.23577627539634705 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -2.9386032494999998 5 -3.9181278706558342
		 6 -5.4742873822621325 7 -5.4742873822621325;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.54494172334671021 0.8897966742515564 
		0.77523940801620483 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 -0.83847397565841675 -0.45635706186294556 
		-0.6316676139831543 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.033333331346511841 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0.14628009498119354 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 26.667789391291564 4 -0.30131692935421839
		 5 -9.290929139215077 6 32.452886665199713 7 32.452886665199713;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.033333331346511841 0.070639669895172119 
		0.20781362056732178 0.045704100281000137 1;
	setAttr -s 7 ".kiy[1:6]"  0 0.14628009498119354 -0.99750196933746338 
		-0.97816842794418335 0.99895507097244263 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 2.6975834259420508
		 7 2.6975834259420508;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 0.57783013582229614 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0.81615704298019409 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 -2.1543284120288981
		 7 -2.1543284120288981;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 0.66337382793426514 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 -0.74828815460205078 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 43.048374913580368 4 19.944292281290185
		 5 12.243008417468898 6 -12.652220937568869 7 -12.652220937568869;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.033333331346511841 0.082382313907146454 
		0.24070115387439728 0.076491117477416992 1;
	setAttr -s 7 ".kiy[1:6]"  0 0.10019879043102264 -0.99660086631774902 
		-0.97059929370880127 -0.9970703125 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -1.1981954365 5 -1.5975899213485452
		 6 23.524133125119057 7 23.524133125119057;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.84709340333938599 0.97882592678070068 
		0.075805462896823883 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 -0.5314440131187439 -0.20469449460506439 
		0.99712264537811279 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -14.779734935 5 -19.706263980883552
		 6 -18.350527878987378 7 -18.350527878987378;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.12815594673156738 0.36145749688148499 
		0.81543618440628052 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 -0.99175399541854858 -0.93238860368728638 
		0.57884699106216431 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.033333331346511841 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0.10019879043102264 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 26.667789391291564 4 13.333894695645782
		 5 8.8893075767461731 6 14.206529214310791 7 14.206529214310791;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.033333331346511841 0.14178639650344849 
		0.39479860663414001 0.33803927898406982 1;
	setAttr -s 7 ".kiy[1:6]"  0 0.10019879043102264 -0.98989725112915039 
		-0.91876763105392456 0.94113200902938843 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 7.1876963112535774
		 7 7.1876963112535774;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 0.25680139660835266 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0.96646416187286377 0;
createNode animCurveTA -n "rigw:rig:FKPinkyFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 -8.1170866956412038
		 7 -8.1170866956412038;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 0.22903439402580261 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 -0.97341835498809814 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -1.8886743561174946 3 43.048374913580368
		 4 13.632786981790183 5 3.8276890564865607 6 -30.22767389268672 7 -30.22767389268672;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.06479036808013916 0.19118916988372803 0.055993027985095978 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.0329635851085186 -0.0329635851085186 
		-0.99789887666702271 -0.98155319690704346 -0.99843114614486694 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 2.9541086855000001 5 3.9388017336377152
		 6 0.90561338038911976 7 0.90561338038911976;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 0.5429261326789856 0.88881796598434448 
		0.53282803297042847 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.83978044986724854 0.45826041698455811 
		-0.84622353315353394 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 2.1504921499999998 5 2.8673156983594996
		 6 14.758581526545504 7 14.758581526545504;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 0.66403573751449585 0.93622815608978271 
		0.15857796370983124 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.74770087003707886 0.35139259696006775 
		0.98734641075134277 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -1.8886743561174946 3 0 4 0 5 0 6 0
		 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.0329635851085186 -0.0329635851085186 
		0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -1.8886743561174946 3 26.667789391291564
		 4 13.333894695645782 5 8.8893075767461731 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.14178639650344849 0.39479860663414001 0.2100556343793869 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.0329635851085186 -0.0329635851085186 
		-0.98989725112915039 -0.91876763105392456 -0.97768938541412354 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.9960036109999998e-16 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:FKWrist1_R_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.776356839e-15 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 12.237193151870068 4 6.118596575935034
		 5 4.0790847792786087 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.2979620099067688 0.68352514505386353 
		0.42403125762939453 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 -0.95457768440246582 -0.72992688417434692 
		-0.90564757585525513 0;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 43.733574138478112 4 21.866787069239056
		 5 14.577930935449603 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.087009422481060028 0.25346782803535461 
		0.12990027666091919 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 -0.99620753526687622 -0.96734380722045898 
		-0.99152714014053345 0;
createNode animCurveTA -n "rigw:rig:FKWrist1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 -43.950835314344147 4 0 5 0 6 0
		 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.043413486331701279 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.99905717372894287 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.2534834849999998 2 -3.2534834849999998
		 3 -6.9891946902905424 4 -35.717840612500005 5 -46.539184773809581 6 -68.182197740000007
		 7 -68.182197740000007;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.066332854330539703 0.17380388081073761 
		0.08790210634469986 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 -0.99779748916625977 -0.98478031158447266 
		-0.99612915515899658 0;
createNode animCurveTA -n "rigw:rig:FKHose1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 -16.759349641888111 4 18.509901580000001
		 5 24.679807073661404 6 37.019803160000002 7 37.019803160000002;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.054071616381406784 0.29570162296295166 
		0.15294882655143738 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.99853700399398804 0.9552803635597229 
		0.98823410272598267 0;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 18.084703777973193 4 0 5 0 6 0
		 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.10502233356237411 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 -0.99446982145309448 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -14.128420090000001 2 30.330393720929312
		 3 -21.517626989185839 4 -14.128420090000001 5 -14.128420090000001 6 -14.128420090000001
		 7 -14.128420090000001;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.25024256110191345 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.96818321943283081 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHose2_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 -5.3518050399924855 4 0 5 0 6 0
		 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.33610236644744873 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.94182544946670532 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.4355168760000001 2 -1.5180959754446899
		 3 0.5282170393848904 4 1.4355168760000001 5 1.4355168760000001 6 1.4355168760000001
		 7 1.4355168760000001;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.036714289337396622 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.28043684363365173 0.28043681383132935 
		0.99932581186294556 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.61952478609999995 2 -2.0600307650815237
		 3 1.4256780564082803 4 -0.61952478609999995 5 -0.61952478609999995 6 -0.61952478609999995
		 7 -0.61952478609999995;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.016296140849590302 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 -0.99986726045608521 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHose2_R_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.45236972040000001 2 2.7670296122990155
		 3 -0.12728818340609882 4 -0.45236972040000001 5 -0.45236972040000001 6 -0.45236972040000001
		 7 -0.45236972040000001;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.1020035594701767 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.3504951000213623 -0.35049504041671753 
		-0.99478399753570557 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.0438294770000001 2 -1.779958922988381
		 3 -0.58140974760872144 4 1.0438294770000001 5 1.0438294770000001 6 1.0438294770000001
		 7 1.0438294770000001;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.020505491644144058 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.9997897744178772 0 0 0;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.084512618380000001 2 -0.7970625757456592
		 3 -0.26255789615865066 4 -1.4408548158100001 5 -1.9493055426485526 6 -2.9662222499999999
		 7 -2.9662222499999999;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.028278110548853874 0.065418191254138947 
		0.032761223614215851 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 -0.9996001124382019 -0.99785792827606201 
		-0.99946314096450806 0;
createNode animCurveTL -n "rigw:rig:FKHose1_R_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.12829119480000001 2 0.74420539781622097
		 3 0.81605146756223779 4 -0.75745919709999998 5 -1.0527063752320269 6 -1.643209589
		 7 -1.643209589;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		0.021179305389523506 0.11218702793121338 0.056359302252531052 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.32176366448402405 -0.32176357507705688 
		-0.99977570772171021 -0.99368715286254883 -0.99841052293777466 0;
createNode animCurveTL -n "rigw:rig:Main_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:Main_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:Main_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  3 3 3 2 2 2 2;
createNode animCurveTU -n "rigw:rig:Main_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:Main_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:Main_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:Main_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:Main_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.8 2 1.8 3 1.8 4 1.8 5 1.7999999999999998
		 6 1.8 7 1.8;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:Main_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.8 2 1.8 3 1.8 4 1.8 5 1.7999999999999998
		 6 1.8 7 1.8;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:Main_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.8 2 1.8 3 1.8 4 1.8 5 1.7999999999999998
		 6 1.8 7 1.8;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo58_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo58_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.562667 2 2.562667 3 2.562667 4 2.562667
		 5 2.562667 6 2.562667 7 2.562667;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo58_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.403567 2 -3.403567 3 -3.403567 4 -3.403567
		 5 -3.403567 6 -3.403567 7 -3.403567;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo59_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo59_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.112302 2 2.112302 3 2.112302 4 2.112302
		 5 2.112302 6 2.112302 7 2.112302;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo59_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.27079 2 -4.27079 3 -4.27079 4 -4.27079
		 5 -4.27079 6 -4.27079 7 -4.27079;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo60_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo60_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.661936 2 1.661936 3 1.661936 4 1.661936
		 5 1.6619359999999999 6 1.661936 7 1.661936;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo60_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -5.138014 2 -5.138014 3 -5.138014 4 -5.138014
		 5 -5.138014 6 -5.138014 7 -5.138014;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo61_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo61_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.078102 2 1.078102 3 1.078102 4 1.078102
		 5 1.078102 6 1.078102 7 1.078102;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo61_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -5.613094 2 -5.613094 3 -5.613094 4 -5.613094
		 5 -5.613094 6 -5.613094 7 -5.613094;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo62_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo62_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.659831 2 0.659831 3 0.659831 4 0.659831
		 5 0.659831 6 0.659831 7 0.659831;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo62_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.957155 2 -4.957155 3 -4.957155 4 -4.957155
		 5 -4.957155 6 -4.957155 7 -4.957155;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo63_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo63_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo63_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.137065 2 -4.137065 3 -4.137065 4 -4.137065
		 5 -4.137065 6 -4.137065 7 -4.137065;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo64_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo64_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo64_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.159629 2 -3.159629 3 -3.159629 4 -3.159629
		 5 -3.159629 6 -3.159629 7 -3.159629;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo65_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo65_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo65_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.182193 2 -2.182193 3 -2.182193 4 -2.182193
		 5 -2.182193 6 -2.182193 7 -2.182193;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo66_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo66_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo66_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.204757 2 -1.204757 3 -1.204757 4 -1.204757
		 5 -1.204757 6 -1.204757 7 -1.204757;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo67_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo67_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo67_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.22732 2 -0.22732 3 -0.22732 4 -0.22732
		 5 -0.22732 6 -0.22732 7 -0.22732;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo68_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo68_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo68_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.750116 2 0.750116 3 0.750116 4 0.750116
		 5 0.750116 6 0.750116 7 0.750116;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo69_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo69_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo69_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.727552 2 1.727552 3 1.727552 4 1.727552
		 5 1.7275519999999998 6 1.727552 7 1.727552;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo70_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo70_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo70_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.704989 2 2.704989 3 2.704989 4 2.704989
		 5 2.704989 6 2.704989 7 2.704989;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo71_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo71_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo71_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.682425 2 3.682425 3 3.682425 4 3.682425
		 5 3.682425 6 3.682425 7 3.682425;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo72_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo72_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.715486 2 0.715486 3 0.715486 4 0.715486
		 5 0.715486 6 0.715486 7 0.715486;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo72_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.165678 2 4.165678 3 4.165678 4 4.165678
		 5 4.165678 6 4.165678 7 4.165678;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo73_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo73_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.248661 2 1.248661 3 1.248661 4 1.248661
		 5 1.248661 6 1.248661 7 1.248661;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo73_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.934626 2 3.934626 3 3.934626 4 3.934626
		 5 3.9346260000000006 6 3.934626 7 3.934626;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo74_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo74_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.006366 2 2.006366 3 2.006366 4 2.006366
		 5 2.006366 6 2.006366 7 2.006366;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo74_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.876286 2 3.876286 3 3.876286 4 3.876286
		 5 3.876286 6 3.876286 7 3.876286;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo75_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo75_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.443066 2 2.443066 3 2.443066 4 2.443066
		 5 2.443066 6 2.443066 7 2.443066;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo75_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.998664 2 2.998664 3 2.998664 4 2.998664
		 5 2.998664 6 2.998664 7 2.998664;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo76_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740572 2 0.740572 3 0.740572 4 0.740572
		 5 0.740572 6 0.740572 7 0.740572;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo76_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.802991 2 2.802991 3 2.802991 4 2.802991
		 5 2.802991 6 2.802991 7 2.802991;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo76_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.134471 2 2.134471 3 2.134471 4 2.134471
		 5 2.134471 6 2.134471 7 2.134471;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo79_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo79_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.829374 2 2.829374 3 2.829374 4 2.829374
		 5 2.829374 6 2.829374 7 2.829374;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo79_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.525288 2 -2.525288 3 -2.525288 4 -2.525288
		 5 -2.525288 6 -2.525288 7 -2.525288;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo80_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo80_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.050656 2 3.050656 3 3.050656 4 3.050656
		 5 3.050656 6 3.050656 7 3.050656;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo80_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.615166 2 -1.615166 3 -1.615166 4 -1.615166
		 5 -1.6151659999999999 6 -1.615166 7 -1.615166;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo81_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo81_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.050656 2 3.050656 3 3.050656 4 3.050656
		 5 3.050656 6 3.050656 7 3.050656;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo81_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.642503 2 -0.642503 3 -0.642503 4 -0.642503
		 5 -0.642503 6 -0.642503 7 -0.642503;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo82_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo82_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.050656 2 3.050656 3 3.050656 4 3.050656
		 5 3.050656 6 3.050656 7 3.050656;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo82_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.33016 2 0.33016 3 0.33016 4 0.33016
		 5 0.33016 6 0.33016 7 0.33016;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo83_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.740445 2 0.740445 3 0.740445 4 0.740445
		 5 0.740445 6 0.740445 7 0.740445;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo83_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.03787 2 3.03787 3 3.03787 4 3.03787
		 5 3.03787 6 3.03787 7 3.03787;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo83_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.259282 2 1.259282 3 1.259282 4 1.259282
		 5 1.259282 6 1.259282 7 1.259282;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo72_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo72_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 112.246345 2 112.246345 3 112.246345 4 112.246345
		 5 112.246345 6 112.246345 7 112.246345;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo72_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo72_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo72_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo72_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo72_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo73_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo73_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 87.636758 2 87.636758 3 87.636758 4 87.636758
		 5 87.636758 6 87.636758 7 87.636758;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo73_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo73_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo73_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo73_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo73_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo74_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo74_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.173630000000003 2 27.173630000000003
		 3 27.173630000000003 4 27.173630000000003 5 27.173630000000003 6 27.173630000000003
		 7 27.173630000000003;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo74_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo74_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo74_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo74_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo74_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo75_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo75_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.173630000000003 2 27.173630000000003
		 3 27.173630000000003 4 27.173630000000003 5 27.173630000000003 6 27.173630000000003
		 7 27.173630000000003;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo75_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo75_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo75_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo75_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo75_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo76_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo76_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 28.003616 2 28.003616 3 28.003616 4 28.003616
		 5 28.003616 6 28.003616 7 28.003616;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo76_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.054517 2 0.054517 3 0.054517 4 0.054517
		 5 0.05451700000000001 6 0.054517 7 0.054517;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo76_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.506614 2 -0.506614 3 -0.506614 4 -0.506614
		 5 -0.506614 6 -0.506614 7 -0.506614;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo76_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo76_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo76_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo83_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo83_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.484307 2 4.484307 3 4.484307 4 4.484307
		 5 4.484307 6 4.484307 7 4.484307;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo83_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.9012298073891429e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo83_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo83_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo83_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo83_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo82_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo82_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo82_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.9012298073891429e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo82_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo82_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo82_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo82_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo81_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo81_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo81_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.9012298073891429e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo81_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo81_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo81_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo81_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo80_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo80_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo80_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.9012298073891429e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo80_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo80_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo80_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo80_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo79_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo79_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -23.380861 2 -23.380861 3 -23.380861 4 -23.380861
		 5 -23.380861 6 -23.380861 7 -23.380861;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo79_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.5802459614778258e-30 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo79_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.5311250384401291e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo79_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo79_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo79_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo58_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo58_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -27.443690000000004 2 -27.443690000000004
		 3 -27.443690000000004 4 -27.443690000000004 5 -27.443690000000004 6 -27.443690000000004
		 7 -27.443690000000004;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo58_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.5802459614778258e-30 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo58_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.5311250384401291e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo58_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo58_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo58_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo59_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo59_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -27.443690000000004 2 -27.443690000000004
		 3 -27.443690000000004 4 -27.443690000000004 5 -27.443690000000004 6 -27.443690000000004
		 7 -27.443690000000004;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo59_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.5802459614778258e-30 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo59_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.5311250384401291e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo59_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo59_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo59_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo60_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo60_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -27.443690000000004 2 -27.443690000000004
		 3 -27.443690000000004 4 -27.443690000000004 5 -27.443690000000004 6 -27.443690000000004
		 7 -27.443690000000004;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo60_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.5802459614778258e-30 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo60_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.5311250384401291e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo60_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo60_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo60_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo61_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo61_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -101.39456600000001 2 -101.39456600000001
		 3 -101.39456600000001 4 -101.39456600000001 5 -101.39456600000001 6 -101.39456600000001
		 7 -101.39456600000001;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo61_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.0167092985348775e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo61_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo61_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo61_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo61_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo62_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo62_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 210.809462 2 210.809462 3 210.809462 4 210.809462
		 5 210.809462 6 210.809462 7 210.809462;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo62_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo62_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo62_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo62_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo62_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo63_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo63_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo63_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo63_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo63_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo63_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo63_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo64_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo64_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo64_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo64_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo64_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo64_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo64_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo65_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo65_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo65_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo65_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo65_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo65_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo65_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo66_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo66_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo66_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo66_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo66_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo66_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo66_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo67_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo67_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo67_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo67_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo67_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo67_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo67_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo68_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo68_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo68_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo68_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo68_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo68_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo68_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo69_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo69_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo69_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo69_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo69_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo69_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo69_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo70_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo70_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo70_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo70_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo70_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo70_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo70_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo71_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo71_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo71_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo71_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.016709298534876e-15 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo71_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo71_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo71_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo100_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo100_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.974319 2 0.974319 3 0.974319 4 0.974319
		 5 0.974319 6 0.974319 7 0.974319;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo100_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.426617 2 4.426617 3 4.426617 4 4.426617
		 5 4.426617 6 4.426617 7 4.426617;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo101_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo101_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.662469 2 1.662469 3 1.662469 4 1.662469
		 5 1.6624690000000002 6 1.662469 7 1.662469;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo101_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 4.221696 2 4.221696 3 4.221696 4 4.221696
		 5 4.221696 6 4.221696 7 4.221696;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo102_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo102_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.156221 2 2.156221 3 2.156221 4 2.156221
		 5 2.156221 6 2.156221 7 2.156221;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo102_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.374191 2 3.374191 3 3.374191 4 3.374191
		 5 3.3741909999999997 6 3.374191 7 3.374191;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo103_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo103_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.603864 2 2.603864 3 2.603864 4 2.603864
		 5 2.603864 6 2.603864 7 2.603864;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo103_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.502186 2 2.502186 3 2.502186 4 2.502186
		 5 2.502186 6 2.502186 7 2.502186;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo104_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.741585 2 -0.741585 3 -0.741585 4 -0.741585
		 5 -0.741585 6 -0.741585 7 -0.741585;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo104_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.953546 2 2.953546 3 2.953546 4 2.953546
		 5 2.953546 6 2.953546 7 2.953546;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo104_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.639622 2 1.639622 3 1.639622 4 1.639622
		 5 1.639622 6 1.639622 7 1.639622;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo107_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo107_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.818561 2 2.818561 3 2.818561 4 2.818561
		 5 2.818561 6 2.818561 7 2.818561;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo107_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.910817 2 -2.910817 3 -2.910817 4 -2.910817
		 5 -2.910817 6 -2.910817 7 -2.910817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo108_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo108_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.050656 2 3.050656 3 3.050656 4 3.050656
		 5 3.050656 6 3.050656 7 3.050656;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo108_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.12289 2 -2.12289 3 -2.12289 4 -2.12289
		 5 -2.12289 6 -2.12289 7 -2.12289;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo109_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo109_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.050656 2 3.050656 3 3.050656 4 3.050656
		 5 3.050656 6 3.050656 7 3.050656;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo109_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.150227 2 -1.150227 3 -1.150227 4 -1.150227
		 5 -1.150227 6 -1.150227 7 -1.150227;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo110_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo110_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.050656 2 3.050656 3 3.050656 4 3.050656
		 5 3.050656 6 3.050656 7 3.050656;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo110_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.177564 2 -0.177564 3 -0.177564 4 -0.177564
		 5 -0.177564 6 -0.177564 7 -0.177564;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo111_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo111_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.050656 2 3.050656 3 3.050656 4 3.050656
		 5 3.050656 6 3.050656 7 3.050656;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo111_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.795099 2 0.795099 3 0.795099 4 0.795099
		 5 0.795099 6 0.795099 7 0.795099;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo86_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo86_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.374804 2 2.374804 3 2.374804 4 2.374804
		 5 2.374804 6 2.374804 7 2.374804;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo86_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.765316 2 -3.765316 3 -3.765316 4 -3.765316
		 5 -3.7653159999999994 6 -3.765316 7 -3.765316;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo87_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo87_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.924439 2 1.924439 3 1.924439 4 1.924439
		 5 1.924439 6 1.924439 7 1.924439;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo87_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.632539 2 -4.632539 3 -4.632539 4 -4.632539
		 5 -4.632539 6 -4.632539 7 -4.632539;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo88_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo88_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.474073 2 1.474073 3 1.474073 4 1.474073
		 5 1.474073 6 1.474073 7 1.474073;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo88_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -5.499763 2 -5.499763 3 -5.499763 4 -5.499763
		 5 -5.499763 6 -5.499763 7 -5.499763;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo89_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo89_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.664251 2 0.664251 3 0.664251 4 0.664251
		 5 0.664251 6 0.664251 7 0.664251;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo89_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -5.529688 2 -5.529688 3 -5.529688 4 -5.529688
		 5 -5.529688 6 -5.529688 7 -5.529688;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo90_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo90_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo90_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.892085 2 -4.892085 3 -4.892085 4 -4.892085
		 5 -4.892085 6 -4.892085 7 -4.892085;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo91_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo91_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo91_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.914649 2 -3.914649 3 -3.914649 4 -3.914649
		 5 -3.914649 6 -3.914649 7 -3.914649;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo92_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo92_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo92_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -2.937213 2 -2.937213 3 -2.937213 4 -2.937213
		 5 -2.937213 6 -2.937213 7 -2.937213;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo93_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo93_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo93_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.959776 2 -1.959776 3 -1.959776 4 -1.959776
		 5 -1.9597760000000002 6 -1.959776 7 -1.959776;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo94_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo94_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo94_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.98234 2 -0.98234 3 -0.98234 4 -0.98234
		 5 -0.98234 6 -0.98234 7 -0.98234;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo95_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo95_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo95_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.0049039299999999999 2 -0.0049039299999999999
		 3 -0.0049039299999999999 4 -0.0049039299999999999 5 -0.0049039299999999999 6 -0.0049039299999999999
		 7 -0.0049039299999999999;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo96_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo96_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo96_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.972532 2 0.972532 3 0.972532 4 0.972532
		 5 0.972532 6 0.972532 7 0.972532;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo97_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo97_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo97_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1.949969 2 1.949969 3 1.949969 4 1.949969
		 5 1.949969 6 1.949969 7 1.949969;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo98_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo98_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo98_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 2.927405 2 2.927405 3 2.927405 4 2.927405
		 5 2.927405 6 2.927405 7 2.927405;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo99_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.740596 2 -0.740596 3 -0.740596 4 -0.740596
		 5 -0.740596 6 -0.740596 7 -0.740596;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo99_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.360482 2 0.360482 3 0.360482 4 0.360482
		 5 0.360482 6 0.360482 7 0.360482;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:mesh:robo99_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.904841 2 3.904841 3 3.904841 4 3.904841
		 5 3.9048409999999993 6 3.904841 7 3.904841;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo102_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo102_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 152.82637 2 152.82637 3 152.82637 4 152.82637
		 5 152.82637 6 152.82637 7 152.82637;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo102_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.5304005016724968e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo102_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo102_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo102_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo102_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo101_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo101_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 148.612692 2 148.612692 3 148.612692 4 148.612692
		 5 148.612692 6 148.612692 7 148.612692;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo101_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo101_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo101_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo101_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo101_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo100_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo100_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 74.561257 2 74.561257 3 74.561257 4 74.561257
		 5 74.561257 6 74.561257 7 74.561257;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo100_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo100_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo100_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo100_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo100_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo99_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo99_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo99_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo99_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo99_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo99_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo99_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo98_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo98_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo98_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo98_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo98_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo98_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo98_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo97_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo97_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo97_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo97_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo97_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo97_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo97_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo96_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo96_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo96_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo96_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo96_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo96_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo96_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo95_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo95_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo95_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo95_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo95_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo95_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo95_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo94_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo94_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo94_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo94_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo94_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo94_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo94_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo93_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo93_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo93_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo93_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo93_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo93_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo93_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo92_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo92_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo92_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo92_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo92_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo92_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo92_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo91_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo91_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo91_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo91_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo91_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo91_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo91_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo90_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo90_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo90_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo90_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo90_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo90_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo90_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo89_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo89_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -78.605434 2 -78.605434 3 -78.605434 4 -78.605434
		 5 -78.605434 6 -78.605434 7 -78.605434;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo89_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -7.0608010033449937e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo89_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo89_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo89_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo89_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo88_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo88_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.443690000000004 2 27.443690000000004
		 3 27.443690000000004 4 27.443690000000004 5 27.443690000000004 6 27.443690000000004
		 7 27.443690000000004;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo88_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo88_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo88_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo88_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo88_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo87_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo87_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.443690000000004 2 27.443690000000004
		 3 27.443690000000004 4 27.443690000000004 5 27.443690000000004 6 27.443690000000004
		 7 27.443690000000004;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo87_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo87_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo87_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo87_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo87_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo86_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo86_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.443690000000004 2 27.443690000000004
		 3 27.443690000000004 4 27.443690000000004 5 27.443690000000004 6 27.443690000000004
		 7 27.443690000000004;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo86_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo86_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo86_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo86_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo86_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo107_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo107_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 27.443690000000004 2 27.443690000000004
		 3 27.443690000000004 4 27.443690000000004 5 27.443690000000004 6 27.443690000000004
		 7 27.443690000000004;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo107_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo107_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo107_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo107_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo107_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo108_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo108_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo108_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo108_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo108_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo108_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo108_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo109_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo109_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo109_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo109_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo109_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo109_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo109_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo110_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo110_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo110_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo110_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo110_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo110_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo110_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo111_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo111_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo111_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo111_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo111_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo111_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo111_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo104_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo104_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 161.793326 2 161.793326 3 161.793326 4 161.793326
		 5 161.793326 6 161.793326 7 161.793326;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo104_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.05450590000000001 2 0.05450590000000001
		 3 0.05450590000000001 4 0.05450590000000001 5 0.05450590000000001 6 0.05450590000000001
		 7 0.05450590000000001;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo104_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -179.49349 2 -179.49349 3 -179.49349 4 -179.49349
		 5 -179.49349 6 -179.49349 7 -179.49349;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo104_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.894 2 0.894 3 0.894 4 0.894 5 0.89399999999999991
		 6 0.894 7 0.894;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo104_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo104_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo103_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo103_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 152.82637 2 152.82637 3 152.82637 4 152.82637
		 5 152.82637 6 152.82637 7 152.82637;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo103_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 3.5304005016724968e-31 2 0 3 0 4 0 5 0
		 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:mesh:robo103_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 180 2 180 3 180 4 180 5 180 6 180 7 180;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo103_scaleX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.89399999999999979 2 0.89399999999999979
		 3 0.89399999999999979 4 0.89399999999999979 5 0.89399999999999979 6 0.89399999999999979
		 7 0.89399999999999979;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo103_scaleY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.893817 2 0.893817 3 0.893817 4 0.893817
		 5 0.893817 6 0.893817 7 0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:mesh:robo103_scaleZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.893817 2 -0.893817 3 -0.893817 4 -0.893817
		 5 -0.893817 6 -0.893817 7 -0.893817;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 0.033333331346511841 0.033333331346511841 
		1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHose1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.609696 2 -0.609696 3 -0.66068793316022956
		 4 -1.6176288094950102 5 -2.1215989940000002 6 -2.4476638468767176 7 -2.6107011643367799;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.23568788170814514 0.034812115132808685 
		0.06599726527929306 0.1016991063952446 0.20030850172042847;
	setAttr -s 7 ".kiy[1:6]"  0 -0.97182875871658325 -0.99939388036727905 
		-0.99781972169876099 -0.99481523036956787 -0.97973287105560303;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -0.3270648596819396 5 -0.98120439108972968
		 6 -0.98120439108972968 7 -0.98120439108972968;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.10139137506484985 0.050891496241092682 
		1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 -0.99484658241271973 -0.99870425462722778 
		0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0.28156681413701118 4 -8.9816291676028435
		 5 -13.570792828591147 6 -11.489045819664028 7 -10.448141088683068;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.20192994177341461 0.38422238826751709 
		0.67603057622909546 0.87805718183517456;
	setAttr -s 7 ".kiy[1:6]"  0 0 -0.97939997911453247 -0.9232405424118042 
		0.73687350749969482 0.47855561971664429;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 -2.9635551740247141 4 -8.1429384562937734
		 5 -11.816020248016256 6 -15.972667847004381 7 -18.051053996835929;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.34597104787826538 0.46132549643516541 
		0.41750884056091309 0.67662352323532104;
	setAttr -s 7 ".kiy[1:6]"  0 0 -0.93824523687362671 -0.88723099231719971 
		-0.90867280960083008 -0.73632913827896118;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -6.4398229178404458 3 4.246806974189087
		 4 -39.969341683061216 5 -60.074312329972123 6 -60.916593162696024 7 -61.337746213396791;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.32672756910324097 0.043153475970029831 
		0.094568647444248199 0.91497153043746948 0.97653877735137939;
	setAttr -s 7 ".kiy[1:6]"  0 -0.94511860609054565 -0.9990684986114502 
		-0.99551838636398315 -0.40351822972297668 -0.21534167230129242;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_roll";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_rollAngle";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 25 2 25 3 25 4 25 5 25 6 25 7 25;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegBall_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_R_follow";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.571318 2 0.571318 3 0.72828796875355739
		 4 1.0553224753191199 5 1.2973265279999999 6 1.428254895581033 7 1.4937210433167027;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.45081225037574768 0.10140069574117661 
		0.13645045459270477 0.24672175943851471 0.45373824238777161;
	setAttr -s 7 ".kiy[1:6]"  0 0.89261877536773682 0.99484562873840332 
		0.99064689874649048 0.96908634901046753 0.89113491773605347;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 -0.3270648596819396 5 -0.98120439108972968
		 6 -0.80382349925235808 7 -0.71513039259368727;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 0.10139137506484985 0.050891496241092682 
		0.1846868097782135 0.35180267691612244;
	setAttr -s 7 ".kiy[1:6]"  0 0 -0.99484658241271973 -0.99870425462722778 
		0.98279744386672974 0.93607425689697266;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0.46506086496019267 5 2.2450452231651212
		 6 1.6456607817222253 7 1.3459595701442471;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.87425613403320312 0.97160905599594116 
		0.73154240846633911 0.9541161060333252 0.98791038990020752;
	setAttr -s 7 ".kiy[1:6]"  0 0.48546501994132996 0.23659190535545349 
		0.68179583549499512 -0.29943692684173584 -0.15502604842185974;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 -0.6392010247793547 4 2.8241560333216356
		 5 2.4334078900673042 6 8.7315554967323692 7 11.880723773223734;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.98085451126098633 0.48289158940315247 
		0.97970539331436157 0.29019242525100708 0.51855432987213135;
	setAttr -s 7 ".kiy[1:6]"  0 -0.19474206864833832 0.87568008899688721 
		-0.2004430741071701 0.95696836709976196 0.85504460334777832;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 7.7481603654856821 3 -6.6391500812776378
		 4 29.507224457129489 5 44.498613621070255 6 49.960594367377759 7 52.691666671062002;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 0.42773020267486572 0.052763223648071289 
		0.12637566030025482 0.33006805181503296 0.573081374168396;
	setAttr -s 7 ".kiy[1:6]"  0 0.90390646457672119 0.99860703945159912 
		0.99198246002197266 0.94395720958709717 0.81949847936630249;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_roll";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_rollAngle";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 25 2 25 3 25 4 25 5 25 6 25 7 25;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegBall_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_L_follow";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10;
	setAttr -s 7 ".kit[0:6]"  2 1 1 2 2 2 2;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 123.25696979999999 2 123.25696979999999
		 3 123.25696979999999 4 123.25696979999999 5 123.25696979999999 6 264.4000512462074
		 7 258.25361732731625;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 0.013530130498111248 0.29673156142234802;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0.999908447265625 -0.95496094226837158;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 67.64002069 2 67.64002069 3 67.64002069
		 4 67.64002069 5 67.64002069 6 47.538219719026905 7 64.276841366003765;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 0.094583421945571899 0.11336342990398407;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 -0.99551689624786377 0.99355363845825195;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -124.66477840000002 2 -124.66477840000002
		 3 -124.66477840000002 4 -124.66477840000002 5 -124.66477840000002 6 -247.42952692231052
		 7 -250.17511575867823;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 0.015555183403193951 0.57104122638702393;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 -0.99987906217575073 -0.82092142105102539;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -22.028843 2 -22.028843 3 -22.028843 4 -22.028843
		 5 -22.028843 6 -22.028843 7 -22.028843;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -16.267797000000005 2 -16.267797000000005
		 3 -16.267797000000005 4 -16.267797000000005 5 -16.267797000000005 6 -16.267797000000005
		 7 -16.267797000000005;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHose2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -39.607339 2 -39.607339 3 -39.607339 4 -39.607339
		 5 -39.607339 6 -39.607339 7 -39.607339;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKIKBlend";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKIKBlend";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10;
	setAttr -s 7 ".kit[0:6]"  1 1 1 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1;
	setAttr -s 7 ".kit[0:6]"  1 1 1 9 9 9 9;
	setAttr -s 7 ".kix[0:6]"  0 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode mia_exposure_simple -n "mia_exposure_simple1";
	setAttr ".S02" 1;
	setAttr ".S04" 1.1499999761581421;
createNode polyPlane -n "polyPlane1";
	setAttr ".cuv" 2;
createNode displayLayer -n "layer1";
	setAttr ".dt" 2;
	setAttr ".do" 1;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 16 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 16 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 32 ".tx";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 47 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w";
	setAttr -av ".h";
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 2 ".sol";
connectAttr "rigw:mesh:robo72_translateX.o" "rigwRN.phl[179]";
connectAttr "rigw:mesh:robo72_translateY.o" "rigwRN.phl[180]";
connectAttr "rigw:mesh:robo72_translateZ.o" "rigwRN.phl[181]";
connectAttr "rigw:mesh:robo72_rotateX.o" "rigwRN.phl[182]";
connectAttr "rigw:mesh:robo72_rotateY.o" "rigwRN.phl[183]";
connectAttr "rigw:mesh:robo72_rotateZ.o" "rigwRN.phl[184]";
connectAttr "rigw:mesh:robo72_visibility.o" "rigwRN.phl[185]";
connectAttr "rigw:mesh:robo72_scaleX.o" "rigwRN.phl[186]";
connectAttr "rigw:mesh:robo72_scaleY.o" "rigwRN.phl[187]";
connectAttr "rigw:mesh:robo72_scaleZ.o" "rigwRN.phl[188]";
connectAttr "rigw:mesh:robo73_translateX.o" "rigwRN.phl[189]";
connectAttr "rigw:mesh:robo73_translateY.o" "rigwRN.phl[190]";
connectAttr "rigw:mesh:robo73_translateZ.o" "rigwRN.phl[191]";
connectAttr "rigw:mesh:robo73_rotateX.o" "rigwRN.phl[192]";
connectAttr "rigw:mesh:robo73_rotateY.o" "rigwRN.phl[193]";
connectAttr "rigw:mesh:robo73_rotateZ.o" "rigwRN.phl[194]";
connectAttr "rigw:mesh:robo73_visibility.o" "rigwRN.phl[195]";
connectAttr "rigw:mesh:robo73_scaleX.o" "rigwRN.phl[196]";
connectAttr "rigw:mesh:robo73_scaleY.o" "rigwRN.phl[197]";
connectAttr "rigw:mesh:robo73_scaleZ.o" "rigwRN.phl[198]";
connectAttr "rigw:mesh:robo74_translateX.o" "rigwRN.phl[199]";
connectAttr "rigw:mesh:robo74_translateY.o" "rigwRN.phl[200]";
connectAttr "rigw:mesh:robo74_translateZ.o" "rigwRN.phl[201]";
connectAttr "rigw:mesh:robo74_visibility.o" "rigwRN.phl[202]";
connectAttr "rigw:mesh:robo74_rotateX.o" "rigwRN.phl[203]";
connectAttr "rigw:mesh:robo74_rotateY.o" "rigwRN.phl[204]";
connectAttr "rigw:mesh:robo74_rotateZ.o" "rigwRN.phl[205]";
connectAttr "rigw:mesh:robo74_scaleX.o" "rigwRN.phl[206]";
connectAttr "rigw:mesh:robo74_scaleY.o" "rigwRN.phl[207]";
connectAttr "rigw:mesh:robo74_scaleZ.o" "rigwRN.phl[208]";
connectAttr "rigw:mesh:robo75_translateX.o" "rigwRN.phl[209]";
connectAttr "rigw:mesh:robo75_translateY.o" "rigwRN.phl[210]";
connectAttr "rigw:mesh:robo75_translateZ.o" "rigwRN.phl[211]";
connectAttr "rigw:mesh:robo75_visibility.o" "rigwRN.phl[212]";
connectAttr "rigw:mesh:robo75_rotateX.o" "rigwRN.phl[213]";
connectAttr "rigw:mesh:robo75_rotateY.o" "rigwRN.phl[214]";
connectAttr "rigw:mesh:robo75_rotateZ.o" "rigwRN.phl[215]";
connectAttr "rigw:mesh:robo75_scaleX.o" "rigwRN.phl[216]";
connectAttr "rigw:mesh:robo75_scaleY.o" "rigwRN.phl[217]";
connectAttr "rigw:mesh:robo75_scaleZ.o" "rigwRN.phl[218]";
connectAttr "rigw:mesh:robo76_translateX.o" "rigwRN.phl[219]";
connectAttr "rigw:mesh:robo76_translateY.o" "rigwRN.phl[220]";
connectAttr "rigw:mesh:robo76_translateZ.o" "rigwRN.phl[221]";
connectAttr "rigw:mesh:robo76_visibility.o" "rigwRN.phl[222]";
connectAttr "rigw:mesh:robo76_rotateX.o" "rigwRN.phl[223]";
connectAttr "rigw:mesh:robo76_rotateY.o" "rigwRN.phl[224]";
connectAttr "rigw:mesh:robo76_rotateZ.o" "rigwRN.phl[225]";
connectAttr "rigw:mesh:robo76_scaleX.o" "rigwRN.phl[226]";
connectAttr "rigw:mesh:robo76_scaleY.o" "rigwRN.phl[227]";
connectAttr "rigw:mesh:robo76_scaleZ.o" "rigwRN.phl[228]";
connectAttr "rigw:mesh:robo83_translateX.o" "rigwRN.phl[229]";
connectAttr "rigw:mesh:robo83_translateY.o" "rigwRN.phl[230]";
connectAttr "rigw:mesh:robo83_translateZ.o" "rigwRN.phl[231]";
connectAttr "rigw:mesh:robo83_visibility.o" "rigwRN.phl[232]";
connectAttr "rigw:mesh:robo83_rotateX.o" "rigwRN.phl[233]";
connectAttr "rigw:mesh:robo83_rotateY.o" "rigwRN.phl[234]";
connectAttr "rigw:mesh:robo83_rotateZ.o" "rigwRN.phl[235]";
connectAttr "rigw:mesh:robo83_scaleX.o" "rigwRN.phl[236]";
connectAttr "rigw:mesh:robo83_scaleY.o" "rigwRN.phl[237]";
connectAttr "rigw:mesh:robo83_scaleZ.o" "rigwRN.phl[238]";
connectAttr "rigw:mesh:robo82_translateX.o" "rigwRN.phl[239]";
connectAttr "rigw:mesh:robo82_translateY.o" "rigwRN.phl[240]";
connectAttr "rigw:mesh:robo82_translateZ.o" "rigwRN.phl[241]";
connectAttr "rigw:mesh:robo82_visibility.o" "rigwRN.phl[242]";
connectAttr "rigw:mesh:robo82_rotateX.o" "rigwRN.phl[243]";
connectAttr "rigw:mesh:robo82_rotateY.o" "rigwRN.phl[244]";
connectAttr "rigw:mesh:robo82_rotateZ.o" "rigwRN.phl[245]";
connectAttr "rigw:mesh:robo82_scaleX.o" "rigwRN.phl[246]";
connectAttr "rigw:mesh:robo82_scaleY.o" "rigwRN.phl[247]";
connectAttr "rigw:mesh:robo82_scaleZ.o" "rigwRN.phl[248]";
connectAttr "rigw:mesh:robo81_translateX.o" "rigwRN.phl[249]";
connectAttr "rigw:mesh:robo81_translateY.o" "rigwRN.phl[250]";
connectAttr "rigw:mesh:robo81_translateZ.o" "rigwRN.phl[251]";
connectAttr "rigw:mesh:robo81_visibility.o" "rigwRN.phl[252]";
connectAttr "rigw:mesh:robo81_rotateX.o" "rigwRN.phl[253]";
connectAttr "rigw:mesh:robo81_rotateY.o" "rigwRN.phl[254]";
connectAttr "rigw:mesh:robo81_rotateZ.o" "rigwRN.phl[255]";
connectAttr "rigw:mesh:robo81_scaleX.o" "rigwRN.phl[256]";
connectAttr "rigw:mesh:robo81_scaleY.o" "rigwRN.phl[257]";
connectAttr "rigw:mesh:robo81_scaleZ.o" "rigwRN.phl[258]";
connectAttr "rigw:mesh:robo80_translateX.o" "rigwRN.phl[259]";
connectAttr "rigw:mesh:robo80_translateY.o" "rigwRN.phl[260]";
connectAttr "rigw:mesh:robo80_translateZ.o" "rigwRN.phl[261]";
connectAttr "rigw:mesh:robo80_visibility.o" "rigwRN.phl[262]";
connectAttr "rigw:mesh:robo80_rotateX.o" "rigwRN.phl[263]";
connectAttr "rigw:mesh:robo80_rotateY.o" "rigwRN.phl[264]";
connectAttr "rigw:mesh:robo80_rotateZ.o" "rigwRN.phl[265]";
connectAttr "rigw:mesh:robo80_scaleX.o" "rigwRN.phl[266]";
connectAttr "rigw:mesh:robo80_scaleY.o" "rigwRN.phl[267]";
connectAttr "rigw:mesh:robo80_scaleZ.o" "rigwRN.phl[268]";
connectAttr "rigw:mesh:robo79_translateX.o" "rigwRN.phl[269]";
connectAttr "rigw:mesh:robo79_translateY.o" "rigwRN.phl[270]";
connectAttr "rigw:mesh:robo79_translateZ.o" "rigwRN.phl[271]";
connectAttr "rigw:mesh:robo79_visibility.o" "rigwRN.phl[272]";
connectAttr "rigw:mesh:robo79_rotateX.o" "rigwRN.phl[273]";
connectAttr "rigw:mesh:robo79_rotateY.o" "rigwRN.phl[274]";
connectAttr "rigw:mesh:robo79_rotateZ.o" "rigwRN.phl[275]";
connectAttr "rigw:mesh:robo79_scaleX.o" "rigwRN.phl[276]";
connectAttr "rigw:mesh:robo79_scaleY.o" "rigwRN.phl[277]";
connectAttr "rigw:mesh:robo79_scaleZ.o" "rigwRN.phl[278]";
connectAttr "rigw:mesh:robo58_translateX.o" "rigwRN.phl[279]";
connectAttr "rigw:mesh:robo58_translateY.o" "rigwRN.phl[280]";
connectAttr "rigw:mesh:robo58_translateZ.o" "rigwRN.phl[281]";
connectAttr "rigw:mesh:robo58_visibility.o" "rigwRN.phl[282]";
connectAttr "rigw:mesh:robo58_rotateX.o" "rigwRN.phl[283]";
connectAttr "rigw:mesh:robo58_rotateY.o" "rigwRN.phl[284]";
connectAttr "rigw:mesh:robo58_rotateZ.o" "rigwRN.phl[285]";
connectAttr "rigw:mesh:robo58_scaleX.o" "rigwRN.phl[286]";
connectAttr "rigw:mesh:robo58_scaleY.o" "rigwRN.phl[287]";
connectAttr "rigw:mesh:robo58_scaleZ.o" "rigwRN.phl[288]";
connectAttr "rigw:mesh:robo59_translateX.o" "rigwRN.phl[289]";
connectAttr "rigw:mesh:robo59_translateY.o" "rigwRN.phl[290]";
connectAttr "rigw:mesh:robo59_translateZ.o" "rigwRN.phl[291]";
connectAttr "rigw:mesh:robo59_visibility.o" "rigwRN.phl[292]";
connectAttr "rigw:mesh:robo59_rotateX.o" "rigwRN.phl[293]";
connectAttr "rigw:mesh:robo59_rotateY.o" "rigwRN.phl[294]";
connectAttr "rigw:mesh:robo59_rotateZ.o" "rigwRN.phl[295]";
connectAttr "rigw:mesh:robo59_scaleX.o" "rigwRN.phl[296]";
connectAttr "rigw:mesh:robo59_scaleY.o" "rigwRN.phl[297]";
connectAttr "rigw:mesh:robo59_scaleZ.o" "rigwRN.phl[298]";
connectAttr "rigw:mesh:robo60_translateX.o" "rigwRN.phl[299]";
connectAttr "rigw:mesh:robo60_translateY.o" "rigwRN.phl[300]";
connectAttr "rigw:mesh:robo60_translateZ.o" "rigwRN.phl[301]";
connectAttr "rigw:mesh:robo60_rotateX.o" "rigwRN.phl[302]";
connectAttr "rigw:mesh:robo60_rotateY.o" "rigwRN.phl[303]";
connectAttr "rigw:mesh:robo60_rotateZ.o" "rigwRN.phl[304]";
connectAttr "rigw:mesh:robo60_visibility.o" "rigwRN.phl[305]";
connectAttr "rigw:mesh:robo60_scaleX.o" "rigwRN.phl[306]";
connectAttr "rigw:mesh:robo60_scaleY.o" "rigwRN.phl[307]";
connectAttr "rigw:mesh:robo60_scaleZ.o" "rigwRN.phl[308]";
connectAttr "rigw:mesh:robo61_translateX.o" "rigwRN.phl[309]";
connectAttr "rigw:mesh:robo61_translateY.o" "rigwRN.phl[310]";
connectAttr "rigw:mesh:robo61_translateZ.o" "rigwRN.phl[311]";
connectAttr "rigw:mesh:robo61_visibility.o" "rigwRN.phl[312]";
connectAttr "rigw:mesh:robo61_rotateX.o" "rigwRN.phl[313]";
connectAttr "rigw:mesh:robo61_rotateY.o" "rigwRN.phl[314]";
connectAttr "rigw:mesh:robo61_rotateZ.o" "rigwRN.phl[315]";
connectAttr "rigw:mesh:robo61_scaleX.o" "rigwRN.phl[316]";
connectAttr "rigw:mesh:robo61_scaleY.o" "rigwRN.phl[317]";
connectAttr "rigw:mesh:robo61_scaleZ.o" "rigwRN.phl[318]";
connectAttr "rigw:mesh:robo62_translateX.o" "rigwRN.phl[319]";
connectAttr "rigw:mesh:robo62_translateY.o" "rigwRN.phl[320]";
connectAttr "rigw:mesh:robo62_translateZ.o" "rigwRN.phl[321]";
connectAttr "rigw:mesh:robo62_rotateX.o" "rigwRN.phl[322]";
connectAttr "rigw:mesh:robo62_rotateY.o" "rigwRN.phl[323]";
connectAttr "rigw:mesh:robo62_rotateZ.o" "rigwRN.phl[324]";
connectAttr "rigw:mesh:robo62_visibility.o" "rigwRN.phl[325]";
connectAttr "rigw:mesh:robo62_scaleX.o" "rigwRN.phl[326]";
connectAttr "rigw:mesh:robo62_scaleY.o" "rigwRN.phl[327]";
connectAttr "rigw:mesh:robo62_scaleZ.o" "rigwRN.phl[328]";
connectAttr "rigw:mesh:robo63_translateX.o" "rigwRN.phl[329]";
connectAttr "rigw:mesh:robo63_translateY.o" "rigwRN.phl[330]";
connectAttr "rigw:mesh:robo63_translateZ.o" "rigwRN.phl[331]";
connectAttr "rigw:mesh:robo63_visibility.o" "rigwRN.phl[332]";
connectAttr "rigw:mesh:robo63_rotateX.o" "rigwRN.phl[333]";
connectAttr "rigw:mesh:robo63_rotateY.o" "rigwRN.phl[334]";
connectAttr "rigw:mesh:robo63_rotateZ.o" "rigwRN.phl[335]";
connectAttr "rigw:mesh:robo63_scaleX.o" "rigwRN.phl[336]";
connectAttr "rigw:mesh:robo63_scaleY.o" "rigwRN.phl[337]";
connectAttr "rigw:mesh:robo63_scaleZ.o" "rigwRN.phl[338]";
connectAttr "rigw:mesh:robo64_translateX.o" "rigwRN.phl[339]";
connectAttr "rigw:mesh:robo64_translateY.o" "rigwRN.phl[340]";
connectAttr "rigw:mesh:robo64_translateZ.o" "rigwRN.phl[341]";
connectAttr "rigw:mesh:robo64_visibility.o" "rigwRN.phl[342]";
connectAttr "rigw:mesh:robo64_rotateX.o" "rigwRN.phl[343]";
connectAttr "rigw:mesh:robo64_rotateY.o" "rigwRN.phl[344]";
connectAttr "rigw:mesh:robo64_rotateZ.o" "rigwRN.phl[345]";
connectAttr "rigw:mesh:robo64_scaleX.o" "rigwRN.phl[346]";
connectAttr "rigw:mesh:robo64_scaleY.o" "rigwRN.phl[347]";
connectAttr "rigw:mesh:robo64_scaleZ.o" "rigwRN.phl[348]";
connectAttr "rigw:mesh:robo65_translateX.o" "rigwRN.phl[349]";
connectAttr "rigw:mesh:robo65_translateY.o" "rigwRN.phl[350]";
connectAttr "rigw:mesh:robo65_translateZ.o" "rigwRN.phl[351]";
connectAttr "rigw:mesh:robo65_visibility.o" "rigwRN.phl[352]";
connectAttr "rigw:mesh:robo65_rotateX.o" "rigwRN.phl[353]";
connectAttr "rigw:mesh:robo65_rotateY.o" "rigwRN.phl[354]";
connectAttr "rigw:mesh:robo65_rotateZ.o" "rigwRN.phl[355]";
connectAttr "rigw:mesh:robo65_scaleX.o" "rigwRN.phl[356]";
connectAttr "rigw:mesh:robo65_scaleY.o" "rigwRN.phl[357]";
connectAttr "rigw:mesh:robo65_scaleZ.o" "rigwRN.phl[358]";
connectAttr "rigw:mesh:robo66_translateX.o" "rigwRN.phl[359]";
connectAttr "rigw:mesh:robo66_translateY.o" "rigwRN.phl[360]";
connectAttr "rigw:mesh:robo66_translateZ.o" "rigwRN.phl[361]";
connectAttr "rigw:mesh:robo66_visibility.o" "rigwRN.phl[362]";
connectAttr "rigw:mesh:robo66_rotateX.o" "rigwRN.phl[363]";
connectAttr "rigw:mesh:robo66_rotateY.o" "rigwRN.phl[364]";
connectAttr "rigw:mesh:robo66_rotateZ.o" "rigwRN.phl[365]";
connectAttr "rigw:mesh:robo66_scaleX.o" "rigwRN.phl[366]";
connectAttr "rigw:mesh:robo66_scaleY.o" "rigwRN.phl[367]";
connectAttr "rigw:mesh:robo66_scaleZ.o" "rigwRN.phl[368]";
connectAttr "rigw:mesh:robo67_translateX.o" "rigwRN.phl[369]";
connectAttr "rigw:mesh:robo67_translateY.o" "rigwRN.phl[370]";
connectAttr "rigw:mesh:robo67_translateZ.o" "rigwRN.phl[371]";
connectAttr "rigw:mesh:robo67_visibility.o" "rigwRN.phl[372]";
connectAttr "rigw:mesh:robo67_rotateX.o" "rigwRN.phl[373]";
connectAttr "rigw:mesh:robo67_rotateY.o" "rigwRN.phl[374]";
connectAttr "rigw:mesh:robo67_rotateZ.o" "rigwRN.phl[375]";
connectAttr "rigw:mesh:robo67_scaleX.o" "rigwRN.phl[376]";
connectAttr "rigw:mesh:robo67_scaleY.o" "rigwRN.phl[377]";
connectAttr "rigw:mesh:robo67_scaleZ.o" "rigwRN.phl[378]";
connectAttr "rigw:mesh:robo68_translateX.o" "rigwRN.phl[379]";
connectAttr "rigw:mesh:robo68_translateY.o" "rigwRN.phl[380]";
connectAttr "rigw:mesh:robo68_translateZ.o" "rigwRN.phl[381]";
connectAttr "rigw:mesh:robo68_visibility.o" "rigwRN.phl[382]";
connectAttr "rigw:mesh:robo68_rotateX.o" "rigwRN.phl[383]";
connectAttr "rigw:mesh:robo68_rotateY.o" "rigwRN.phl[384]";
connectAttr "rigw:mesh:robo68_rotateZ.o" "rigwRN.phl[385]";
connectAttr "rigw:mesh:robo68_scaleX.o" "rigwRN.phl[386]";
connectAttr "rigw:mesh:robo68_scaleY.o" "rigwRN.phl[387]";
connectAttr "rigw:mesh:robo68_scaleZ.o" "rigwRN.phl[388]";
connectAttr "rigw:mesh:robo69_translateX.o" "rigwRN.phl[389]";
connectAttr "rigw:mesh:robo69_translateY.o" "rigwRN.phl[390]";
connectAttr "rigw:mesh:robo69_translateZ.o" "rigwRN.phl[391]";
connectAttr "rigw:mesh:robo69_visibility.o" "rigwRN.phl[392]";
connectAttr "rigw:mesh:robo69_rotateX.o" "rigwRN.phl[393]";
connectAttr "rigw:mesh:robo69_rotateY.o" "rigwRN.phl[394]";
connectAttr "rigw:mesh:robo69_rotateZ.o" "rigwRN.phl[395]";
connectAttr "rigw:mesh:robo69_scaleX.o" "rigwRN.phl[396]";
connectAttr "rigw:mesh:robo69_scaleY.o" "rigwRN.phl[397]";
connectAttr "rigw:mesh:robo69_scaleZ.o" "rigwRN.phl[398]";
connectAttr "rigw:mesh:robo70_translateX.o" "rigwRN.phl[399]";
connectAttr "rigw:mesh:robo70_translateY.o" "rigwRN.phl[400]";
connectAttr "rigw:mesh:robo70_translateZ.o" "rigwRN.phl[401]";
connectAttr "rigw:mesh:robo70_visibility.o" "rigwRN.phl[402]";
connectAttr "rigw:mesh:robo70_rotateX.o" "rigwRN.phl[403]";
connectAttr "rigw:mesh:robo70_rotateY.o" "rigwRN.phl[404]";
connectAttr "rigw:mesh:robo70_rotateZ.o" "rigwRN.phl[405]";
connectAttr "rigw:mesh:robo70_scaleX.o" "rigwRN.phl[406]";
connectAttr "rigw:mesh:robo70_scaleY.o" "rigwRN.phl[407]";
connectAttr "rigw:mesh:robo70_scaleZ.o" "rigwRN.phl[408]";
connectAttr "rigw:mesh:robo71_translateX.o" "rigwRN.phl[409]";
connectAttr "rigw:mesh:robo71_translateY.o" "rigwRN.phl[410]";
connectAttr "rigw:mesh:robo71_translateZ.o" "rigwRN.phl[411]";
connectAttr "rigw:mesh:robo71_visibility.o" "rigwRN.phl[412]";
connectAttr "rigw:mesh:robo71_rotateX.o" "rigwRN.phl[413]";
connectAttr "rigw:mesh:robo71_rotateY.o" "rigwRN.phl[414]";
connectAttr "rigw:mesh:robo71_rotateZ.o" "rigwRN.phl[415]";
connectAttr "rigw:mesh:robo71_scaleX.o" "rigwRN.phl[416]";
connectAttr "rigw:mesh:robo71_scaleY.o" "rigwRN.phl[417]";
connectAttr "rigw:mesh:robo71_scaleZ.o" "rigwRN.phl[418]";
connectAttr "rigw:mesh:robo102_translateX.o" "rigwRN.phl[419]";
connectAttr "rigw:mesh:robo102_translateY.o" "rigwRN.phl[420]";
connectAttr "rigw:mesh:robo102_translateZ.o" "rigwRN.phl[421]";
connectAttr "rigw:mesh:robo102_visibility.o" "rigwRN.phl[422]";
connectAttr "rigw:mesh:robo102_rotateX.o" "rigwRN.phl[423]";
connectAttr "rigw:mesh:robo102_rotateY.o" "rigwRN.phl[424]";
connectAttr "rigw:mesh:robo102_rotateZ.o" "rigwRN.phl[425]";
connectAttr "rigw:mesh:robo102_scaleX.o" "rigwRN.phl[426]";
connectAttr "rigw:mesh:robo102_scaleY.o" "rigwRN.phl[427]";
connectAttr "rigw:mesh:robo102_scaleZ.o" "rigwRN.phl[428]";
connectAttr "rigw:mesh:robo101_translateX.o" "rigwRN.phl[429]";
connectAttr "rigw:mesh:robo101_translateY.o" "rigwRN.phl[430]";
connectAttr "rigw:mesh:robo101_translateZ.o" "rigwRN.phl[431]";
connectAttr "rigw:mesh:robo101_rotateX.o" "rigwRN.phl[432]";
connectAttr "rigw:mesh:robo101_rotateY.o" "rigwRN.phl[433]";
connectAttr "rigw:mesh:robo101_rotateZ.o" "rigwRN.phl[434]";
connectAttr "rigw:mesh:robo101_visibility.o" "rigwRN.phl[435]";
connectAttr "rigw:mesh:robo101_scaleX.o" "rigwRN.phl[436]";
connectAttr "rigw:mesh:robo101_scaleY.o" "rigwRN.phl[437]";
connectAttr "rigw:mesh:robo101_scaleZ.o" "rigwRN.phl[438]";
connectAttr "rigw:mesh:robo100_translateX.o" "rigwRN.phl[439]";
connectAttr "rigw:mesh:robo100_translateY.o" "rigwRN.phl[440]";
connectAttr "rigw:mesh:robo100_translateZ.o" "rigwRN.phl[441]";
connectAttr "rigw:mesh:robo100_rotateX.o" "rigwRN.phl[442]";
connectAttr "rigw:mesh:robo100_rotateY.o" "rigwRN.phl[443]";
connectAttr "rigw:mesh:robo100_rotateZ.o" "rigwRN.phl[444]";
connectAttr "rigw:mesh:robo100_visibility.o" "rigwRN.phl[445]";
connectAttr "rigw:mesh:robo100_scaleX.o" "rigwRN.phl[446]";
connectAttr "rigw:mesh:robo100_scaleY.o" "rigwRN.phl[447]";
connectAttr "rigw:mesh:robo100_scaleZ.o" "rigwRN.phl[448]";
connectAttr "rigw:mesh:robo99_translateX.o" "rigwRN.phl[449]";
connectAttr "rigw:mesh:robo99_translateY.o" "rigwRN.phl[450]";
connectAttr "rigw:mesh:robo99_translateZ.o" "rigwRN.phl[451]";
connectAttr "rigw:mesh:robo99_visibility.o" "rigwRN.phl[452]";
connectAttr "rigw:mesh:robo99_rotateX.o" "rigwRN.phl[453]";
connectAttr "rigw:mesh:robo99_rotateY.o" "rigwRN.phl[454]";
connectAttr "rigw:mesh:robo99_rotateZ.o" "rigwRN.phl[455]";
connectAttr "rigw:mesh:robo99_scaleX.o" "rigwRN.phl[456]";
connectAttr "rigw:mesh:robo99_scaleY.o" "rigwRN.phl[457]";
connectAttr "rigw:mesh:robo99_scaleZ.o" "rigwRN.phl[458]";
connectAttr "rigw:mesh:robo98_translateX.o" "rigwRN.phl[459]";
connectAttr "rigw:mesh:robo98_translateY.o" "rigwRN.phl[460]";
connectAttr "rigw:mesh:robo98_translateZ.o" "rigwRN.phl[461]";
connectAttr "rigw:mesh:robo98_visibility.o" "rigwRN.phl[462]";
connectAttr "rigw:mesh:robo98_rotateX.o" "rigwRN.phl[463]";
connectAttr "rigw:mesh:robo98_rotateY.o" "rigwRN.phl[464]";
connectAttr "rigw:mesh:robo98_rotateZ.o" "rigwRN.phl[465]";
connectAttr "rigw:mesh:robo98_scaleX.o" "rigwRN.phl[466]";
connectAttr "rigw:mesh:robo98_scaleY.o" "rigwRN.phl[467]";
connectAttr "rigw:mesh:robo98_scaleZ.o" "rigwRN.phl[468]";
connectAttr "rigw:mesh:robo97_translateX.o" "rigwRN.phl[469]";
connectAttr "rigw:mesh:robo97_translateY.o" "rigwRN.phl[470]";
connectAttr "rigw:mesh:robo97_translateZ.o" "rigwRN.phl[471]";
connectAttr "rigw:mesh:robo97_visibility.o" "rigwRN.phl[472]";
connectAttr "rigw:mesh:robo97_rotateX.o" "rigwRN.phl[473]";
connectAttr "rigw:mesh:robo97_rotateY.o" "rigwRN.phl[474]";
connectAttr "rigw:mesh:robo97_rotateZ.o" "rigwRN.phl[475]";
connectAttr "rigw:mesh:robo97_scaleX.o" "rigwRN.phl[476]";
connectAttr "rigw:mesh:robo97_scaleY.o" "rigwRN.phl[477]";
connectAttr "rigw:mesh:robo97_scaleZ.o" "rigwRN.phl[478]";
connectAttr "rigw:mesh:robo96_translateX.o" "rigwRN.phl[479]";
connectAttr "rigw:mesh:robo96_translateY.o" "rigwRN.phl[480]";
connectAttr "rigw:mesh:robo96_translateZ.o" "rigwRN.phl[481]";
connectAttr "rigw:mesh:robo96_visibility.o" "rigwRN.phl[482]";
connectAttr "rigw:mesh:robo96_rotateX.o" "rigwRN.phl[483]";
connectAttr "rigw:mesh:robo96_rotateY.o" "rigwRN.phl[484]";
connectAttr "rigw:mesh:robo96_rotateZ.o" "rigwRN.phl[485]";
connectAttr "rigw:mesh:robo96_scaleX.o" "rigwRN.phl[486]";
connectAttr "rigw:mesh:robo96_scaleY.o" "rigwRN.phl[487]";
connectAttr "rigw:mesh:robo96_scaleZ.o" "rigwRN.phl[488]";
connectAttr "rigw:mesh:robo95_translateX.o" "rigwRN.phl[489]";
connectAttr "rigw:mesh:robo95_translateY.o" "rigwRN.phl[490]";
connectAttr "rigw:mesh:robo95_translateZ.o" "rigwRN.phl[491]";
connectAttr "rigw:mesh:robo95_visibility.o" "rigwRN.phl[492]";
connectAttr "rigw:mesh:robo95_rotateX.o" "rigwRN.phl[493]";
connectAttr "rigw:mesh:robo95_rotateY.o" "rigwRN.phl[494]";
connectAttr "rigw:mesh:robo95_rotateZ.o" "rigwRN.phl[495]";
connectAttr "rigw:mesh:robo95_scaleX.o" "rigwRN.phl[496]";
connectAttr "rigw:mesh:robo95_scaleY.o" "rigwRN.phl[497]";
connectAttr "rigw:mesh:robo95_scaleZ.o" "rigwRN.phl[498]";
connectAttr "rigw:mesh:robo94_translateX.o" "rigwRN.phl[499]";
connectAttr "rigw:mesh:robo94_translateY.o" "rigwRN.phl[500]";
connectAttr "rigw:mesh:robo94_translateZ.o" "rigwRN.phl[501]";
connectAttr "rigw:mesh:robo94_visibility.o" "rigwRN.phl[502]";
connectAttr "rigw:mesh:robo94_rotateX.o" "rigwRN.phl[503]";
connectAttr "rigw:mesh:robo94_rotateY.o" "rigwRN.phl[504]";
connectAttr "rigw:mesh:robo94_rotateZ.o" "rigwRN.phl[505]";
connectAttr "rigw:mesh:robo94_scaleX.o" "rigwRN.phl[506]";
connectAttr "rigw:mesh:robo94_scaleY.o" "rigwRN.phl[507]";
connectAttr "rigw:mesh:robo94_scaleZ.o" "rigwRN.phl[508]";
connectAttr "rigw:mesh:robo93_translateX.o" "rigwRN.phl[509]";
connectAttr "rigw:mesh:robo93_translateY.o" "rigwRN.phl[510]";
connectAttr "rigw:mesh:robo93_translateZ.o" "rigwRN.phl[511]";
connectAttr "rigw:mesh:robo93_visibility.o" "rigwRN.phl[512]";
connectAttr "rigw:mesh:robo93_rotateX.o" "rigwRN.phl[513]";
connectAttr "rigw:mesh:robo93_rotateY.o" "rigwRN.phl[514]";
connectAttr "rigw:mesh:robo93_rotateZ.o" "rigwRN.phl[515]";
connectAttr "rigw:mesh:robo93_scaleX.o" "rigwRN.phl[516]";
connectAttr "rigw:mesh:robo93_scaleY.o" "rigwRN.phl[517]";
connectAttr "rigw:mesh:robo93_scaleZ.o" "rigwRN.phl[518]";
connectAttr "rigw:mesh:robo92_translateX.o" "rigwRN.phl[519]";
connectAttr "rigw:mesh:robo92_translateY.o" "rigwRN.phl[520]";
connectAttr "rigw:mesh:robo92_translateZ.o" "rigwRN.phl[521]";
connectAttr "rigw:mesh:robo92_visibility.o" "rigwRN.phl[522]";
connectAttr "rigw:mesh:robo92_rotateX.o" "rigwRN.phl[523]";
connectAttr "rigw:mesh:robo92_rotateY.o" "rigwRN.phl[524]";
connectAttr "rigw:mesh:robo92_rotateZ.o" "rigwRN.phl[525]";
connectAttr "rigw:mesh:robo92_scaleX.o" "rigwRN.phl[526]";
connectAttr "rigw:mesh:robo92_scaleY.o" "rigwRN.phl[527]";
connectAttr "rigw:mesh:robo92_scaleZ.o" "rigwRN.phl[528]";
connectAttr "rigw:mesh:robo91_translateX.o" "rigwRN.phl[529]";
connectAttr "rigw:mesh:robo91_translateY.o" "rigwRN.phl[530]";
connectAttr "rigw:mesh:robo91_translateZ.o" "rigwRN.phl[531]";
connectAttr "rigw:mesh:robo91_visibility.o" "rigwRN.phl[532]";
connectAttr "rigw:mesh:robo91_rotateX.o" "rigwRN.phl[533]";
connectAttr "rigw:mesh:robo91_rotateY.o" "rigwRN.phl[534]";
connectAttr "rigw:mesh:robo91_rotateZ.o" "rigwRN.phl[535]";
connectAttr "rigw:mesh:robo91_scaleX.o" "rigwRN.phl[536]";
connectAttr "rigw:mesh:robo91_scaleY.o" "rigwRN.phl[537]";
connectAttr "rigw:mesh:robo91_scaleZ.o" "rigwRN.phl[538]";
connectAttr "rigw:mesh:robo90_translateX.o" "rigwRN.phl[539]";
connectAttr "rigw:mesh:robo90_translateY.o" "rigwRN.phl[540]";
connectAttr "rigw:mesh:robo90_translateZ.o" "rigwRN.phl[541]";
connectAttr "rigw:mesh:robo90_rotateX.o" "rigwRN.phl[542]";
connectAttr "rigw:mesh:robo90_rotateY.o" "rigwRN.phl[543]";
connectAttr "rigw:mesh:robo90_rotateZ.o" "rigwRN.phl[544]";
connectAttr "rigw:mesh:robo90_visibility.o" "rigwRN.phl[545]";
connectAttr "rigw:mesh:robo90_scaleX.o" "rigwRN.phl[546]";
connectAttr "rigw:mesh:robo90_scaleY.o" "rigwRN.phl[547]";
connectAttr "rigw:mesh:robo90_scaleZ.o" "rigwRN.phl[548]";
connectAttr "rigw:mesh:robo89_translateX.o" "rigwRN.phl[549]";
connectAttr "rigw:mesh:robo89_translateY.o" "rigwRN.phl[550]";
connectAttr "rigw:mesh:robo89_translateZ.o" "rigwRN.phl[551]";
connectAttr "rigw:mesh:robo89_visibility.o" "rigwRN.phl[552]";
connectAttr "rigw:mesh:robo89_rotateX.o" "rigwRN.phl[553]";
connectAttr "rigw:mesh:robo89_rotateY.o" "rigwRN.phl[554]";
connectAttr "rigw:mesh:robo89_rotateZ.o" "rigwRN.phl[555]";
connectAttr "rigw:mesh:robo89_scaleX.o" "rigwRN.phl[556]";
connectAttr "rigw:mesh:robo89_scaleY.o" "rigwRN.phl[557]";
connectAttr "rigw:mesh:robo89_scaleZ.o" "rigwRN.phl[558]";
connectAttr "rigw:mesh:robo88_translateX.o" "rigwRN.phl[559]";
connectAttr "rigw:mesh:robo88_translateY.o" "rigwRN.phl[560]";
connectAttr "rigw:mesh:robo88_translateZ.o" "rigwRN.phl[561]";
connectAttr "rigw:mesh:robo88_rotateX.o" "rigwRN.phl[562]";
connectAttr "rigw:mesh:robo88_rotateY.o" "rigwRN.phl[563]";
connectAttr "rigw:mesh:robo88_rotateZ.o" "rigwRN.phl[564]";
connectAttr "rigw:mesh:robo88_visibility.o" "rigwRN.phl[565]";
connectAttr "rigw:mesh:robo88_scaleX.o" "rigwRN.phl[566]";
connectAttr "rigw:mesh:robo88_scaleY.o" "rigwRN.phl[567]";
connectAttr "rigw:mesh:robo88_scaleZ.o" "rigwRN.phl[568]";
connectAttr "rigw:mesh:robo87_translateX.o" "rigwRN.phl[569]";
connectAttr "rigw:mesh:robo87_translateY.o" "rigwRN.phl[570]";
connectAttr "rigw:mesh:robo87_translateZ.o" "rigwRN.phl[571]";
connectAttr "rigw:mesh:robo87_visibility.o" "rigwRN.phl[572]";
connectAttr "rigw:mesh:robo87_rotateX.o" "rigwRN.phl[573]";
connectAttr "rigw:mesh:robo87_rotateY.o" "rigwRN.phl[574]";
connectAttr "rigw:mesh:robo87_rotateZ.o" "rigwRN.phl[575]";
connectAttr "rigw:mesh:robo87_scaleX.o" "rigwRN.phl[576]";
connectAttr "rigw:mesh:robo87_scaleY.o" "rigwRN.phl[577]";
connectAttr "rigw:mesh:robo87_scaleZ.o" "rigwRN.phl[578]";
connectAttr "rigw:mesh:robo86_translateX.o" "rigwRN.phl[579]";
connectAttr "rigw:mesh:robo86_translateY.o" "rigwRN.phl[580]";
connectAttr "rigw:mesh:robo86_translateZ.o" "rigwRN.phl[581]";
connectAttr "rigw:mesh:robo86_visibility.o" "rigwRN.phl[582]";
connectAttr "rigw:mesh:robo86_rotateX.o" "rigwRN.phl[583]";
connectAttr "rigw:mesh:robo86_rotateY.o" "rigwRN.phl[584]";
connectAttr "rigw:mesh:robo86_rotateZ.o" "rigwRN.phl[585]";
connectAttr "rigw:mesh:robo86_scaleX.o" "rigwRN.phl[586]";
connectAttr "rigw:mesh:robo86_scaleY.o" "rigwRN.phl[587]";
connectAttr "rigw:mesh:robo86_scaleZ.o" "rigwRN.phl[588]";
connectAttr "rigw:mesh:robo107_translateX.o" "rigwRN.phl[589]";
connectAttr "rigw:mesh:robo107_translateY.o" "rigwRN.phl[590]";
connectAttr "rigw:mesh:robo107_translateZ.o" "rigwRN.phl[591]";
connectAttr "rigw:mesh:robo107_visibility.o" "rigwRN.phl[592]";
connectAttr "rigw:mesh:robo107_rotateX.o" "rigwRN.phl[593]";
connectAttr "rigw:mesh:robo107_rotateY.o" "rigwRN.phl[594]";
connectAttr "rigw:mesh:robo107_rotateZ.o" "rigwRN.phl[595]";
connectAttr "rigw:mesh:robo107_scaleX.o" "rigwRN.phl[596]";
connectAttr "rigw:mesh:robo107_scaleY.o" "rigwRN.phl[597]";
connectAttr "rigw:mesh:robo107_scaleZ.o" "rigwRN.phl[598]";
connectAttr "rigw:mesh:robo108_translateX.o" "rigwRN.phl[599]";
connectAttr "rigw:mesh:robo108_translateY.o" "rigwRN.phl[600]";
connectAttr "rigw:mesh:robo108_translateZ.o" "rigwRN.phl[601]";
connectAttr "rigw:mesh:robo108_visibility.o" "rigwRN.phl[602]";
connectAttr "rigw:mesh:robo108_rotateX.o" "rigwRN.phl[603]";
connectAttr "rigw:mesh:robo108_rotateY.o" "rigwRN.phl[604]";
connectAttr "rigw:mesh:robo108_rotateZ.o" "rigwRN.phl[605]";
connectAttr "rigw:mesh:robo108_scaleX.o" "rigwRN.phl[606]";
connectAttr "rigw:mesh:robo108_scaleY.o" "rigwRN.phl[607]";
connectAttr "rigw:mesh:robo108_scaleZ.o" "rigwRN.phl[608]";
connectAttr "rigw:mesh:robo109_translateX.o" "rigwRN.phl[609]";
connectAttr "rigw:mesh:robo109_translateY.o" "rigwRN.phl[610]";
connectAttr "rigw:mesh:robo109_translateZ.o" "rigwRN.phl[611]";
connectAttr "rigw:mesh:robo109_visibility.o" "rigwRN.phl[612]";
connectAttr "rigw:mesh:robo109_rotateX.o" "rigwRN.phl[613]";
connectAttr "rigw:mesh:robo109_rotateY.o" "rigwRN.phl[614]";
connectAttr "rigw:mesh:robo109_rotateZ.o" "rigwRN.phl[615]";
connectAttr "rigw:mesh:robo109_scaleX.o" "rigwRN.phl[616]";
connectAttr "rigw:mesh:robo109_scaleY.o" "rigwRN.phl[617]";
connectAttr "rigw:mesh:robo109_scaleZ.o" "rigwRN.phl[618]";
connectAttr "rigw:mesh:robo110_translateX.o" "rigwRN.phl[619]";
connectAttr "rigw:mesh:robo110_translateY.o" "rigwRN.phl[620]";
connectAttr "rigw:mesh:robo110_translateZ.o" "rigwRN.phl[621]";
connectAttr "rigw:mesh:robo110_visibility.o" "rigwRN.phl[622]";
connectAttr "rigw:mesh:robo110_rotateX.o" "rigwRN.phl[623]";
connectAttr "rigw:mesh:robo110_rotateY.o" "rigwRN.phl[624]";
connectAttr "rigw:mesh:robo110_rotateZ.o" "rigwRN.phl[625]";
connectAttr "rigw:mesh:robo110_scaleX.o" "rigwRN.phl[626]";
connectAttr "rigw:mesh:robo110_scaleY.o" "rigwRN.phl[627]";
connectAttr "rigw:mesh:robo110_scaleZ.o" "rigwRN.phl[628]";
connectAttr "rigw:mesh:robo111_translateX.o" "rigwRN.phl[629]";
connectAttr "rigw:mesh:robo111_translateY.o" "rigwRN.phl[630]";
connectAttr "rigw:mesh:robo111_translateZ.o" "rigwRN.phl[631]";
connectAttr "rigw:mesh:robo111_visibility.o" "rigwRN.phl[632]";
connectAttr "rigw:mesh:robo111_rotateX.o" "rigwRN.phl[633]";
connectAttr "rigw:mesh:robo111_rotateY.o" "rigwRN.phl[634]";
connectAttr "rigw:mesh:robo111_rotateZ.o" "rigwRN.phl[635]";
connectAttr "rigw:mesh:robo111_scaleX.o" "rigwRN.phl[636]";
connectAttr "rigw:mesh:robo111_scaleY.o" "rigwRN.phl[637]";
connectAttr "rigw:mesh:robo111_scaleZ.o" "rigwRN.phl[638]";
connectAttr "rigw:mesh:robo104_translateX.o" "rigwRN.phl[639]";
connectAttr "rigw:mesh:robo104_translateY.o" "rigwRN.phl[640]";
connectAttr "rigw:mesh:robo104_translateZ.o" "rigwRN.phl[641]";
connectAttr "rigw:mesh:robo104_visibility.o" "rigwRN.phl[642]";
connectAttr "rigw:mesh:robo104_rotateX.o" "rigwRN.phl[643]";
connectAttr "rigw:mesh:robo104_rotateY.o" "rigwRN.phl[644]";
connectAttr "rigw:mesh:robo104_rotateZ.o" "rigwRN.phl[645]";
connectAttr "rigw:mesh:robo104_scaleX.o" "rigwRN.phl[646]";
connectAttr "rigw:mesh:robo104_scaleY.o" "rigwRN.phl[647]";
connectAttr "rigw:mesh:robo104_scaleZ.o" "rigwRN.phl[648]";
connectAttr "rigw:mesh:robo103_translateX.o" "rigwRN.phl[649]";
connectAttr "rigw:mesh:robo103_translateY.o" "rigwRN.phl[650]";
connectAttr "rigw:mesh:robo103_translateZ.o" "rigwRN.phl[651]";
connectAttr "rigw:mesh:robo103_visibility.o" "rigwRN.phl[652]";
connectAttr "rigw:mesh:robo103_rotateX.o" "rigwRN.phl[653]";
connectAttr "rigw:mesh:robo103_rotateY.o" "rigwRN.phl[654]";
connectAttr "rigw:mesh:robo103_rotateZ.o" "rigwRN.phl[655]";
connectAttr "rigw:mesh:robo103_scaleX.o" "rigwRN.phl[656]";
connectAttr "rigw:mesh:robo103_scaleY.o" "rigwRN.phl[657]";
connectAttr "rigw:mesh:robo103_scaleZ.o" "rigwRN.phl[658]";
connectAttr "rigw:rig:Main_translateX.o" "rigwRN.phl[1]";
connectAttr "rigw:rig:Main_translateY.o" "rigwRN.phl[2]";
connectAttr "rigw:rig:Main_translateZ.o" "rigwRN.phl[3]";
connectAttr "rigw:rig:Main_visibility.o" "rigwRN.phl[4]";
connectAttr "rigw:rig:Main_rotateX.o" "rigwRN.phl[5]";
connectAttr "rigw:rig:Main_rotateY.o" "rigwRN.phl[6]";
connectAttr "rigw:rig:Main_rotateZ.o" "rigwRN.phl[7]";
connectAttr "rigw:rig:Main_scaleX.o" "rigwRN.phl[8]";
connectAttr "rigw:rig:Main_scaleY.o" "rigwRN.phl[9]";
connectAttr "rigw:rig:Main_scaleZ.o" "rigwRN.phl[10]";
connectAttr "rigw:rig:FKWheel1_R_rotateX.o" "rigwRN.phl[11]";
connectAttr "rigw:rig:FKWheel1_R_rotateY.o" "rigwRN.phl[12]";
connectAttr "rigw:rig:FKWheel1_R_rotateZ.o" "rigwRN.phl[13]";
connectAttr "rigw:rig:FKWheel2_R_rotateX.o" "rigwRN.phl[14]";
connectAttr "rigw:rig:FKWheel2_R_rotateY.o" "rigwRN.phl[15]";
connectAttr "rigw:rig:FKWheel2_R_rotateZ.o" "rigwRN.phl[16]";
connectAttr "rigw:rig:FKWheel3_R_rotateX.o" "rigwRN.phl[17]";
connectAttr "rigw:rig:FKWheel3_R_rotateY.o" "rigwRN.phl[18]";
connectAttr "rigw:rig:FKWheel3_R_rotateZ.o" "rigwRN.phl[19]";
connectAttr "rigw:rig:FKWheel4_R_rotateX.o" "rigwRN.phl[20]";
connectAttr "rigw:rig:FKWheel4_R_rotateY.o" "rigwRN.phl[21]";
connectAttr "rigw:rig:FKWheel4_R_rotateZ.o" "rigwRN.phl[22]";
connectAttr "rigw:rig:FKWheel1_L_rotateX.o" "rigwRN.phl[23]";
connectAttr "rigw:rig:FKWheel1_L_rotateY.o" "rigwRN.phl[24]";
connectAttr "rigw:rig:FKWheel1_L_rotateZ.o" "rigwRN.phl[25]";
connectAttr "rigw:rig:FKWheel2_L_rotateX.o" "rigwRN.phl[26]";
connectAttr "rigw:rig:FKWheel2_L_rotateY.o" "rigwRN.phl[27]";
connectAttr "rigw:rig:FKWheel2_L_rotateZ.o" "rigwRN.phl[28]";
connectAttr "rigw:rig:FKWheel3_L_rotateX.o" "rigwRN.phl[29]";
connectAttr "rigw:rig:FKWheel3_L_rotateY.o" "rigwRN.phl[30]";
connectAttr "rigw:rig:FKWheel3_L_rotateZ.o" "rigwRN.phl[31]";
connectAttr "rigw:rig:FKWheel4_L_rotateX.o" "rigwRN.phl[32]";
connectAttr "rigw:rig:FKWheel4_L_rotateY.o" "rigwRN.phl[33]";
connectAttr "rigw:rig:FKWheel4_L_rotateZ.o" "rigwRN.phl[34]";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[35]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[36]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[37]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[38]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[39]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[40]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[41]";
connectAttr "rigw:rig:FKBody_M_translateX.o" "rigwRN.phl[42]";
connectAttr "rigw:rig:FKBody_M_translateY.o" "rigwRN.phl[43]";
connectAttr "rigw:rig:FKBody_M_translateZ.o" "rigwRN.phl[44]";
connectAttr "rigw:rig:FKBody_M_rotateX.o" "rigwRN.phl[45]";
connectAttr "rigw:rig:FKBody_M_rotateY.o" "rigwRN.phl[46]";
connectAttr "rigw:rig:FKBody_M_rotateZ.o" "rigwRN.phl[47]";
connectAttr "rigw:rig:FKHead_M_translateX.o" "rigwRN.phl[48]";
connectAttr "rigw:rig:FKHead_M_translateY.o" "rigwRN.phl[49]";
connectAttr "rigw:rig:FKHead_M_translateZ.o" "rigwRN.phl[50]";
connectAttr "rigw:rig:FKHead_M_rotateX.o" "rigwRN.phl[51]";
connectAttr "rigw:rig:FKHead_M_rotateY.o" "rigwRN.phl[52]";
connectAttr "rigw:rig:FKHead_M_rotateZ.o" "rigwRN.phl[53]";
connectAttr "rigw:rig:FKShoulder_R_Global.o" "rigwRN.phl[54]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[55]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[56]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[57]";
connectAttr "rigw:rig:FKElbow5_R_rotateX.o" "rigwRN.phl[58]";
connectAttr "rigw:rig:FKElbow5_R_rotateY.o" "rigwRN.phl[59]";
connectAttr "rigw:rig:FKElbow5_R_rotateZ.o" "rigwRN.phl[60]";
connectAttr "rigw:rig:FKWrist1_R_translateX.o" "rigwRN.phl[61]";
connectAttr "rigw:rig:FKWrist1_R_translateY.o" "rigwRN.phl[62]";
connectAttr "rigw:rig:FKWrist1_R_translateZ.o" "rigwRN.phl[63]";
connectAttr "rigw:rig:FKWrist1_R_rotateX.o" "rigwRN.phl[64]";
connectAttr "rigw:rig:FKWrist1_R_rotateY.o" "rigwRN.phl[65]";
connectAttr "rigw:rig:FKWrist1_R_rotateZ.o" "rigwRN.phl[66]";
connectAttr "rigw:rig:FKShoulder1_L_Global.o" "rigwRN.phl[67]";
connectAttr "rigw:rig:FKShoulder1_L_rotateX.o" "rigwRN.phl[68]";
connectAttr "rigw:rig:FKShoulder1_L_rotateY.o" "rigwRN.phl[69]";
connectAttr "rigw:rig:FKShoulder1_L_rotateZ.o" "rigwRN.phl[70]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[71]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[72]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[73]";
connectAttr "rigw:rig:FKWrist_L_rotateX.o" "rigwRN.phl[74]";
connectAttr "rigw:rig:FKWrist_L_rotateY.o" "rigwRN.phl[75]";
connectAttr "rigw:rig:FKWrist_L_rotateZ.o" "rigwRN.phl[76]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateX.o" "rigwRN.phl[77]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateY.o" "rigwRN.phl[78]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateZ.o" "rigwRN.phl[79]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateX.o" "rigwRN.phl[80]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateY.o" "rigwRN.phl[81]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateZ.o" "rigwRN.phl[82]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateX.o" "rigwRN.phl[83]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateY.o" "rigwRN.phl[84]";
connectAttr "rigw:rig:FKMiddleFinger3_L_rotateZ.o" "rigwRN.phl[85]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateX.o" "rigwRN.phl[86]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateY.o" "rigwRN.phl[87]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateZ.o" "rigwRN.phl[88]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateX.o" "rigwRN.phl[89]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateY.o" "rigwRN.phl[90]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateZ.o" "rigwRN.phl[91]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateX.o" "rigwRN.phl[92]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateY.o" "rigwRN.phl[93]";
connectAttr "rigw:rig:FKIndexFinger3_L_rotateZ.o" "rigwRN.phl[94]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateX.o" "rigwRN.phl[95]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateY.o" "rigwRN.phl[96]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateZ.o" "rigwRN.phl[97]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateX.o" "rigwRN.phl[98]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateY.o" "rigwRN.phl[99]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateZ.o" "rigwRN.phl[100]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateX.o" "rigwRN.phl[101]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateY.o" "rigwRN.phl[102]";
connectAttr "rigw:rig:FKThumbFinger3_L_rotateZ.o" "rigwRN.phl[103]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateX.o" "rigwRN.phl[104]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateY.o" "rigwRN.phl[105]";
connectAttr "rigw:rig:FKPinkyFinger1_L_rotateZ.o" "rigwRN.phl[106]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateX.o" "rigwRN.phl[107]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateY.o" "rigwRN.phl[108]";
connectAttr "rigw:rig:FKPinkyFinger2_L_rotateZ.o" "rigwRN.phl[109]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateX.o" "rigwRN.phl[110]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateY.o" "rigwRN.phl[111]";
connectAttr "rigw:rig:FKPinkyFinger3_L_rotateZ.o" "rigwRN.phl[112]";
connectAttr "rigw:rig:FKHose1_R_translateX.o" "rigwRN.phl[113]";
connectAttr "rigw:rig:FKHose1_R_translateY.o" "rigwRN.phl[114]";
connectAttr "rigw:rig:FKHose1_R_translateZ.o" "rigwRN.phl[115]";
connectAttr "rigw:rig:FKHose1_R_rotateX.o" "rigwRN.phl[116]";
connectAttr "rigw:rig:FKHose1_R_rotateY.o" "rigwRN.phl[117]";
connectAttr "rigw:rig:FKHose1_R_rotateZ.o" "rigwRN.phl[118]";
connectAttr "rigw:rig:FKHose2_R_translateX.o" "rigwRN.phl[119]";
connectAttr "rigw:rig:FKHose2_R_translateY.o" "rigwRN.phl[120]";
connectAttr "rigw:rig:FKHose2_R_translateZ.o" "rigwRN.phl[121]";
connectAttr "rigw:rig:FKHose2_R_rotateX.o" "rigwRN.phl[122]";
connectAttr "rigw:rig:FKHose2_R_rotateY.o" "rigwRN.phl[123]";
connectAttr "rigw:rig:FKHose2_R_rotateZ.o" "rigwRN.phl[124]";
connectAttr "rigw:rig:FKHose1_L_rotateX.o" "rigwRN.phl[125]";
connectAttr "rigw:rig:FKHose1_L_rotateY.o" "rigwRN.phl[126]";
connectAttr "rigw:rig:FKHose1_L_rotateZ.o" "rigwRN.phl[127]";
connectAttr "rigw:rig:FKHose2_L_rotateX.o" "rigwRN.phl[128]";
connectAttr "rigw:rig:FKHose2_L_rotateY.o" "rigwRN.phl[129]";
connectAttr "rigw:rig:FKHose2_L_rotateZ.o" "rigwRN.phl[130]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[131]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[132]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[133]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[134]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[135]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[136]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[137]";
connectAttr "rigw:rig:IKLeg_R_rollAngle.o" "rigwRN.phl[138]";
connectAttr "rigw:rig:IKLeg_R_roll.o" "rigwRN.phl[139]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[140]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[141]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[142]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[143]";
connectAttr "rigw:rig:IKLegHeel_R_rotateX.o" "rigwRN.phl[144]";
connectAttr "rigw:rig:IKLegHeel_R_rotateY.o" "rigwRN.phl[145]";
connectAttr "rigw:rig:IKLegHeel_R_rotateZ.o" "rigwRN.phl[146]";
connectAttr "rigw:rig:IKLegBall_R_rotateX.o" "rigwRN.phl[147]";
connectAttr "rigw:rig:PoleLeg_R_translateX.o" "rigwRN.phl[148]";
connectAttr "rigw:rig:PoleLeg_R_translateY.o" "rigwRN.phl[149]";
connectAttr "rigw:rig:PoleLeg_R_translateZ.o" "rigwRN.phl[150]";
connectAttr "rigw:rig:PoleLeg_R_follow.o" "rigwRN.phl[151]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[152]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[153]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[154]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[155]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[156]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[157]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[158]";
connectAttr "rigw:rig:IKLeg_L_rollAngle.o" "rigwRN.phl[159]";
connectAttr "rigw:rig:IKLeg_L_roll.o" "rigwRN.phl[160]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[161]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[162]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[163]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[164]";
connectAttr "rigw:rig:IKLegHeel_L_rotateX.o" "rigwRN.phl[165]";
connectAttr "rigw:rig:IKLegHeel_L_rotateY.o" "rigwRN.phl[166]";
connectAttr "rigw:rig:IKLegHeel_L_rotateZ.o" "rigwRN.phl[167]";
connectAttr "rigw:rig:IKLegBall_L_rotateX.o" "rigwRN.phl[168]";
connectAttr "rigw:rig:PoleLeg_L_translateX.o" "rigwRN.phl[169]";
connectAttr "rigw:rig:PoleLeg_L_translateY.o" "rigwRN.phl[170]";
connectAttr "rigw:rig:PoleLeg_L_translateZ.o" "rigwRN.phl[171]";
connectAttr "rigw:rig:PoleLeg_L_follow.o" "rigwRN.phl[172]";
connectAttr "rigw:rig:FKIKLeg_R_FKIKBlend.o" "rigwRN.phl[173]";
connectAttr "rigw:rig:FKIKLeg_R_IKVis.o" "rigwRN.phl[174]";
connectAttr "rigw:rig:FKIKLeg_R_FKVis.o" "rigwRN.phl[175]";
connectAttr "rigw:rig:FKIKLeg_L_FKIKBlend.o" "rigwRN.phl[176]";
connectAttr "rigw:rig:FKIKLeg_L_IKVis.o" "rigwRN.phl[177]";
connectAttr "rigw:rig:FKIKLeg_L_FKVis.o" "rigwRN.phl[178]";
connectAttr "mia_exposure_simple1.msg" "orthCamShape.mils";
connectAttr "layer1.di" "pPlane1.do";
connectAttr "polyPlane1.out" "pPlaneShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "layerManager.dli[1]" "layer1.id";
connectAttr "pPlaneShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "mia_exposure_simple1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Medic__mc-rigw@Med_death.ma
