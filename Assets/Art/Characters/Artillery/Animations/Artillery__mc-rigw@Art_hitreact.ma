//Maya ASCII 2013 scene
//Name: Artillery__mc-rigw@Art_hitreact.ma
//Last modified: Wed, Jun 04, 2014 11:38:08 AM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Artillery/Artillery__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Artillery__rig.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Artillery/Artillery__mc-rigw.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 59.335869363230671 38.956608457864746 63.897974399027888 ;
	setAttr ".r" -type "double3" -20.73835272958145 -318.59999999999883 0 ;
	setAttr ".rp" -type "double3" 8.8817841970012523e-16 -8.8817841970012523e-16 -3.5527136788005009e-15 ;
	setAttr ".rpt" -type "double3" -1.6572417134521235e-15 -1.0478424984037786e-15 3.8751841069560376e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 101.85562474408371;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 17 ".lnk";
	setAttr -s 17 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 745 ".phl";
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".phl[440]" 0;
	setAttr ".phl[441]" 0;
	setAttr ".phl[442]" 0;
	setAttr ".phl[443]" 0;
	setAttr ".phl[444]" 0;
	setAttr ".phl[445]" 0;
	setAttr ".phl[446]" 0;
	setAttr ".phl[447]" 0;
	setAttr ".phl[448]" 0;
	setAttr ".phl[449]" 0;
	setAttr ".phl[450]" 0;
	setAttr ".phl[451]" 0;
	setAttr ".phl[452]" 0;
	setAttr ".phl[453]" 0;
	setAttr ".phl[454]" 0;
	setAttr ".phl[455]" 0;
	setAttr ".phl[456]" 0;
	setAttr ".phl[457]" 0;
	setAttr ".phl[458]" 0;
	setAttr ".phl[459]" 0;
	setAttr ".phl[460]" 0;
	setAttr ".phl[461]" 0;
	setAttr ".phl[462]" 0;
	setAttr ".phl[463]" 0;
	setAttr ".phl[464]" 0;
	setAttr ".phl[465]" 0;
	setAttr ".phl[466]" 0;
	setAttr ".phl[467]" 0;
	setAttr ".phl[468]" 0;
	setAttr ".phl[469]" 0;
	setAttr ".phl[470]" 0;
	setAttr ".phl[471]" 0;
	setAttr ".phl[472]" 0;
	setAttr ".phl[473]" 0;
	setAttr ".phl[474]" 0;
	setAttr ".phl[475]" 0;
	setAttr ".phl[476]" 0;
	setAttr ".phl[477]" 0;
	setAttr ".phl[478]" 0;
	setAttr ".phl[479]" 0;
	setAttr ".phl[480]" 0;
	setAttr ".phl[481]" 0;
	setAttr ".phl[482]" 0;
	setAttr ".phl[483]" 0;
	setAttr ".phl[484]" 0;
	setAttr ".phl[485]" 0;
	setAttr ".phl[486]" 0;
	setAttr ".phl[487]" 0;
	setAttr ".phl[488]" 0;
	setAttr ".phl[489]" 0;
	setAttr ".phl[490]" 0;
	setAttr ".phl[491]" 0;
	setAttr ".phl[492]" 0;
	setAttr ".phl[493]" 0;
	setAttr ".phl[494]" 0;
	setAttr ".phl[495]" 0;
	setAttr ".phl[496]" 0;
	setAttr ".phl[497]" 0;
	setAttr ".phl[498]" 0;
	setAttr ".phl[499]" 0;
	setAttr ".phl[500]" 0;
	setAttr ".phl[501]" 0;
	setAttr ".phl[502]" 0;
	setAttr ".phl[503]" 0;
	setAttr ".phl[504]" 0;
	setAttr ".phl[505]" 0;
	setAttr ".phl[506]" 0;
	setAttr ".phl[507]" 0;
	setAttr ".phl[508]" 0;
	setAttr ".phl[509]" 0;
	setAttr ".phl[510]" 0;
	setAttr ".phl[511]" 0;
	setAttr ".phl[512]" 0;
	setAttr ".phl[513]" 0;
	setAttr ".phl[514]" 0;
	setAttr ".phl[515]" 0;
	setAttr ".phl[516]" 0;
	setAttr ".phl[517]" 0;
	setAttr ".phl[518]" 0;
	setAttr ".phl[519]" 0;
	setAttr ".phl[520]" 0;
	setAttr ".phl[521]" 0;
	setAttr ".phl[522]" 0;
	setAttr ".phl[523]" 0;
	setAttr ".phl[524]" 0;
	setAttr ".phl[525]" 0;
	setAttr ".phl[526]" 0;
	setAttr ".phl[527]" 0;
	setAttr ".phl[528]" 0;
	setAttr ".phl[529]" 0;
	setAttr ".phl[530]" 0;
	setAttr ".phl[531]" 0;
	setAttr ".phl[532]" 0;
	setAttr ".phl[533]" 0;
	setAttr ".phl[534]" 0;
	setAttr ".phl[535]" 0;
	setAttr ".phl[536]" 0;
	setAttr ".phl[537]" 0;
	setAttr ".phl[538]" 0;
	setAttr ".phl[539]" 0;
	setAttr ".phl[540]" 0;
	setAttr ".phl[541]" 0;
	setAttr ".phl[542]" 0;
	setAttr ".phl[543]" 0;
	setAttr ".phl[544]" 0;
	setAttr ".phl[545]" 0;
	setAttr ".phl[546]" 0;
	setAttr ".phl[547]" 0;
	setAttr ".phl[548]" 0;
	setAttr ".phl[549]" 0;
	setAttr ".phl[550]" 0;
	setAttr ".phl[551]" 0;
	setAttr ".phl[552]" 0;
	setAttr ".phl[553]" 0;
	setAttr ".phl[554]" 0;
	setAttr ".phl[555]" 0;
	setAttr ".phl[556]" 0;
	setAttr ".phl[557]" 0;
	setAttr ".phl[558]" 0;
	setAttr ".phl[559]" 0;
	setAttr ".phl[560]" 0;
	setAttr ".phl[561]" 0;
	setAttr ".phl[562]" 0;
	setAttr ".phl[563]" 0;
	setAttr ".phl[564]" 0;
	setAttr ".phl[565]" 0;
	setAttr ".phl[566]" 0;
	setAttr ".phl[567]" 0;
	setAttr ".phl[568]" 0;
	setAttr ".phl[569]" 0;
	setAttr ".phl[570]" 0;
	setAttr ".phl[571]" 0;
	setAttr ".phl[572]" 0;
	setAttr ".phl[573]" 0;
	setAttr ".phl[574]" 0;
	setAttr ".phl[575]" 0;
	setAttr ".phl[576]" 0;
	setAttr ".phl[577]" 0;
	setAttr ".phl[578]" 0;
	setAttr ".phl[579]" 0;
	setAttr ".phl[580]" 0;
	setAttr ".phl[581]" 0;
	setAttr ".phl[582]" 0;
	setAttr ".phl[583]" 0;
	setAttr ".phl[584]" 0;
	setAttr ".phl[585]" 0;
	setAttr ".phl[586]" 0;
	setAttr ".phl[587]" 0;
	setAttr ".phl[588]" 0;
	setAttr ".phl[589]" 0;
	setAttr ".phl[590]" 0;
	setAttr ".phl[591]" 0;
	setAttr ".phl[592]" 0;
	setAttr ".phl[593]" 0;
	setAttr ".phl[594]" 0;
	setAttr ".phl[595]" 0;
	setAttr ".phl[596]" 0;
	setAttr ".phl[597]" 0;
	setAttr ".phl[598]" 0;
	setAttr ".phl[599]" 0;
	setAttr ".phl[600]" 0;
	setAttr ".phl[601]" 0;
	setAttr ".phl[602]" 0;
	setAttr ".phl[603]" 0;
	setAttr ".phl[604]" 0;
	setAttr ".phl[605]" 0;
	setAttr ".phl[606]" 0;
	setAttr ".phl[607]" 0;
	setAttr ".phl[608]" 0;
	setAttr ".phl[609]" 0;
	setAttr ".phl[610]" 0;
	setAttr ".phl[611]" 0;
	setAttr ".phl[612]" 0;
	setAttr ".phl[613]" 0;
	setAttr ".phl[614]" 0;
	setAttr ".phl[615]" 0;
	setAttr ".phl[616]" 0;
	setAttr ".phl[617]" 0;
	setAttr ".phl[618]" 0;
	setAttr ".phl[619]" 0;
	setAttr ".phl[620]" 0;
	setAttr ".phl[621]" 0;
	setAttr ".phl[622]" 0;
	setAttr ".phl[623]" 0;
	setAttr ".phl[624]" 0;
	setAttr ".phl[625]" 0;
	setAttr ".phl[626]" 0;
	setAttr ".phl[627]" 0;
	setAttr ".phl[628]" 0;
	setAttr ".phl[629]" 0;
	setAttr ".phl[630]" 0;
	setAttr ".phl[631]" 0;
	setAttr ".phl[632]" 0;
	setAttr ".phl[633]" 0;
	setAttr ".phl[634]" 0;
	setAttr ".phl[635]" 0;
	setAttr ".phl[636]" 0;
	setAttr ".phl[637]" 0;
	setAttr ".phl[638]" 0;
	setAttr ".phl[639]" 0;
	setAttr ".phl[640]" 0;
	setAttr ".phl[641]" 0;
	setAttr ".phl[642]" 0;
	setAttr ".phl[643]" 0;
	setAttr ".phl[644]" 0;
	setAttr ".phl[645]" 0;
	setAttr ".phl[646]" 0;
	setAttr ".phl[647]" 0;
	setAttr ".phl[648]" 0;
	setAttr ".phl[649]" 0;
	setAttr ".phl[650]" 0;
	setAttr ".phl[651]" 0;
	setAttr ".phl[652]" 0;
	setAttr ".phl[653]" 0;
	setAttr ".phl[654]" 0;
	setAttr ".phl[655]" 0;
	setAttr ".phl[656]" 0;
	setAttr ".phl[657]" 0;
	setAttr ".phl[658]" 0;
	setAttr ".phl[659]" 0;
	setAttr ".phl[660]" 0;
	setAttr ".phl[661]" 0;
	setAttr ".phl[662]" 0;
	setAttr ".phl[663]" 0;
	setAttr ".phl[664]" 0;
	setAttr ".phl[665]" 0;
	setAttr ".phl[666]" 0;
	setAttr ".phl[667]" 0;
	setAttr ".phl[668]" 0;
	setAttr ".phl[669]" 0;
	setAttr ".phl[670]" 0;
	setAttr ".phl[671]" 0;
	setAttr ".phl[672]" 0;
	setAttr ".phl[673]" 0;
	setAttr ".phl[674]" 0;
	setAttr ".phl[675]" 0;
	setAttr ".phl[676]" 0;
	setAttr ".phl[677]" 0;
	setAttr ".phl[678]" 0;
	setAttr ".phl[679]" 0;
	setAttr ".phl[680]" 0;
	setAttr ".phl[681]" 0;
	setAttr ".phl[682]" 0;
	setAttr ".phl[683]" 0;
	setAttr ".phl[684]" 0;
	setAttr ".phl[685]" 0;
	setAttr ".phl[686]" 0;
	setAttr ".phl[687]" 0;
	setAttr ".phl[688]" 0;
	setAttr ".phl[689]" 0;
	setAttr ".phl[690]" 0;
	setAttr ".phl[691]" 0;
	setAttr ".phl[692]" 0;
	setAttr ".phl[693]" 0;
	setAttr ".phl[694]" 0;
	setAttr ".phl[695]" 0;
	setAttr ".phl[696]" 0;
	setAttr ".phl[697]" 0;
	setAttr ".phl[698]" 0;
	setAttr ".phl[699]" 0;
	setAttr ".phl[700]" 0;
	setAttr ".phl[701]" 0;
	setAttr ".phl[702]" 0;
	setAttr ".phl[703]" 0;
	setAttr ".phl[704]" 0;
	setAttr ".phl[705]" 0;
	setAttr ".phl[706]" 0;
	setAttr ".phl[707]" 0;
	setAttr ".phl[708]" 0;
	setAttr ".phl[709]" 0;
	setAttr ".phl[710]" 0;
	setAttr ".phl[711]" 0;
	setAttr ".phl[712]" 0;
	setAttr ".phl[713]" 0;
	setAttr ".phl[714]" 0;
	setAttr ".phl[715]" 0;
	setAttr ".phl[716]" 0;
	setAttr ".phl[717]" 0;
	setAttr ".phl[718]" 0;
	setAttr ".phl[719]" 0;
	setAttr ".phl[720]" 0;
	setAttr ".phl[721]" 0;
	setAttr ".phl[722]" 0;
	setAttr ".phl[723]" 0;
	setAttr ".phl[724]" 0;
	setAttr ".phl[725]" 0;
	setAttr ".phl[726]" 0;
	setAttr ".phl[727]" 0;
	setAttr ".phl[728]" 0;
	setAttr ".phl[729]" 0;
	setAttr ".phl[730]" 0;
	setAttr ".phl[731]" 0;
	setAttr ".phl[732]" 0;
	setAttr ".phl[733]" 0;
	setAttr ".phl[734]" 0;
	setAttr ".phl[735]" 0;
	setAttr ".phl[736]" 0;
	setAttr ".phl[737]" 0;
	setAttr ".phl[738]" 0;
	setAttr ".phl[739]" 0;
	setAttr ".phl[740]" 0;
	setAttr ".phl[741]" 0;
	setAttr ".phl[742]" 0;
	setAttr ".phl[743]" 0;
	setAttr ".phl[744]" 0;
	setAttr ".phl[745]" 0;
	setAttr ".phl[746]" 0;
	setAttr ".phl[747]" 0;
	setAttr ".phl[748]" 0;
	setAttr ".phl[749]" 0;
	setAttr ".phl[750]" 0;
	setAttr ".phl[751]" 0;
	setAttr ".phl[752]" 0;
	setAttr ".phl[753]" 0;
	setAttr ".phl[754]" 0;
	setAttr ".phl[755]" 0;
	setAttr ".phl[756]" 0;
	setAttr ".phl[757]" 0;
	setAttr ".phl[758]" 0;
	setAttr ".phl[759]" 0;
	setAttr ".phl[760]" 0;
	setAttr ".phl[761]" 0;
	setAttr ".phl[762]" 0;
	setAttr ".phl[763]" 0;
	setAttr ".phl[764]" 0;
	setAttr ".phl[765]" 0;
	setAttr ".phl[766]" 0;
	setAttr ".phl[767]" 0;
	setAttr ".phl[768]" 0;
	setAttr ".phl[769]" 0;
	setAttr ".phl[770]" 0;
	setAttr ".phl[771]" 0;
	setAttr ".phl[772]" 0;
	setAttr ".phl[773]" 0;
	setAttr ".phl[774]" 0;
	setAttr ".phl[775]" 0;
	setAttr ".phl[776]" 0;
	setAttr ".phl[777]" 0;
	setAttr ".phl[778]" 0;
	setAttr ".phl[779]" 0;
	setAttr ".phl[780]" 0;
	setAttr ".phl[781]" 0;
	setAttr ".phl[782]" 0;
	setAttr ".phl[783]" 0;
	setAttr ".phl[784]" 0;
	setAttr ".phl[785]" 0;
	setAttr ".phl[786]" 0;
	setAttr ".phl[787]" 0;
	setAttr ".phl[788]" 0;
	setAttr ".phl[789]" 0;
	setAttr ".phl[790]" 0;
	setAttr ".phl[791]" 0;
	setAttr ".phl[792]" 0;
	setAttr ".phl[793]" 0;
	setAttr ".phl[794]" 0;
	setAttr ".phl[795]" 0;
	setAttr ".phl[796]" 0;
	setAttr ".phl[797]" 0;
	setAttr ".phl[798]" 0;
	setAttr ".phl[799]" 0;
	setAttr ".phl[800]" 0;
	setAttr ".phl[801]" 0;
	setAttr ".phl[802]" 0;
	setAttr ".phl[803]" 0;
	setAttr ".phl[804]" 0;
	setAttr ".phl[805]" 0;
	setAttr ".phl[806]" 0;
	setAttr ".phl[807]" 0;
	setAttr ".phl[808]" 0;
	setAttr ".phl[809]" 0;
	setAttr ".phl[810]" 0;
	setAttr ".phl[811]" 0;
	setAttr ".phl[812]" 0;
	setAttr ".phl[813]" 0;
	setAttr ".phl[814]" 0;
	setAttr ".phl[815]" 0;
	setAttr ".phl[816]" 0;
	setAttr ".phl[817]" 0;
	setAttr ".phl[818]" 0;
	setAttr ".phl[819]" 0;
	setAttr ".phl[820]" 0;
	setAttr ".phl[821]" 0;
	setAttr ".phl[822]" 0;
	setAttr ".phl[823]" 0;
	setAttr ".phl[824]" 0;
	setAttr ".phl[825]" 0;
	setAttr ".phl[826]" 0;
	setAttr ".phl[827]" 0;
	setAttr ".phl[828]" 0;
	setAttr ".phl[829]" 0;
	setAttr ".phl[830]" 0;
	setAttr ".phl[831]" 0;
	setAttr ".phl[832]" 0;
	setAttr ".phl[833]" 0;
	setAttr ".phl[834]" 0;
	setAttr ".phl[835]" 0;
	setAttr ".phl[836]" 0;
	setAttr ".phl[837]" 0;
	setAttr ".phl[838]" 0;
	setAttr ".phl[839]" 0;
	setAttr ".phl[840]" 0;
	setAttr ".phl[841]" 0;
	setAttr ".phl[842]" 0;
	setAttr ".phl[843]" 0;
	setAttr ".phl[844]" 0;
	setAttr ".phl[845]" 0;
	setAttr ".phl[846]" 0;
	setAttr ".phl[847]" 0;
	setAttr ".phl[848]" 0;
	setAttr ".phl[849]" 0;
	setAttr ".phl[850]" 0;
	setAttr ".phl[851]" 0;
	setAttr ".phl[852]" 0;
	setAttr ".phl[853]" 0;
	setAttr ".phl[854]" 0;
	setAttr ".phl[855]" 0;
	setAttr ".phl[856]" 0;
	setAttr ".phl[857]" 0;
	setAttr ".phl[858]" 0;
	setAttr ".phl[859]" 0;
	setAttr ".phl[860]" 0;
	setAttr ".phl[861]" 0;
	setAttr ".phl[862]" 0;
	setAttr ".phl[863]" 0;
	setAttr ".phl[864]" 0;
	setAttr ".phl[865]" 0;
	setAttr ".phl[866]" 0;
	setAttr ".phl[867]" 0;
	setAttr ".phl[868]" 0;
	setAttr ".phl[869]" 0;
	setAttr ".phl[870]" 0;
	setAttr ".phl[871]" 0;
	setAttr ".phl[872]" 0;
	setAttr ".phl[873]" 0;
	setAttr ".phl[874]" 0;
	setAttr ".phl[875]" 0;
	setAttr ".phl[876]" 0;
	setAttr ".phl[877]" 0;
	setAttr ".phl[878]" 0;
	setAttr ".phl[879]" 0;
	setAttr ".phl[880]" 0;
	setAttr ".phl[881]" 0;
	setAttr ".phl[882]" 0;
	setAttr ".phl[883]" 0;
	setAttr ".phl[884]" 0;
	setAttr ".phl[885]" 0;
	setAttr ".phl[886]" 0;
	setAttr ".phl[887]" 0;
	setAttr ".phl[888]" 0;
	setAttr ".phl[889]" 0;
	setAttr ".phl[890]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 72
		2 "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR" "rotateX" " -av"
		
		2 "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR" "rotateY" " -av"
		
		2 "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL" "rotateX" " -av"
		
		2 "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL" "rotateY" " -av"
		
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "visibility" " -av 1"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "translate" " -type \"double3\" -1.883742 -0.258923 -0.00926718"
		
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "translateZ" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "translateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "translateX" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "rotate" " -type \"double3\" 179.999946 7.4012e-08 90"
		
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "rotateZ" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "rotateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "rotateX" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "scale" " -type \"double3\" 2.071058 2.071058 2.071058"
		
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "scaleZ" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "scaleY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle2" "scaleX" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "visibility" " -av 1"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "translate" " -type \"double3\" 1.896132 0.258923 0.00926719"
		
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "translateZ" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "translateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "translateX" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "rotate" " -type \"double3\" 5.37433e-05 7.4012e-08 -90"
		
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "rotateZ" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "rotateY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "rotateX" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "scale" " -type \"double3\" 2.071058 2.071058 2.071058"
		
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "scaleZ" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "scaleY" " -av"
		2 "|rigw:rigRNfosterParent1|rigw:nurbsCircle1" "scaleX" " -av"
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.translateX" 
		"rigwRN.placeHolderList[146]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.translateY" 
		"rigwRN.placeHolderList[147]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.translateZ" 
		"rigwRN.placeHolderList[148]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.visibility" 
		"rigwRN.placeHolderList[149]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.rotateX" 
		"rigwRN.placeHolderList[150]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.rotateY" 
		"rigwRN.placeHolderList[151]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.rotateZ" 
		"rigwRN.placeHolderList[152]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.scaleX" 
		"rigwRN.placeHolderList[153]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.scaleY" 
		"rigwRN.placeHolderList[154]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.scaleZ" 
		"rigwRN.placeHolderList[155]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.translateX" 
		"rigwRN.placeHolderList[156]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.translateY" 
		"rigwRN.placeHolderList[157]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.translateZ" 
		"rigwRN.placeHolderList[158]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.visibility" 
		"rigwRN.placeHolderList[159]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.rotateX" 
		"rigwRN.placeHolderList[160]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.rotateY" 
		"rigwRN.placeHolderList[161]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.rotateZ" 
		"rigwRN.placeHolderList[162]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.scaleX" 
		"rigwRN.placeHolderList[163]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.scaleY" 
		"rigwRN.placeHolderList[164]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.scaleZ" 
		"rigwRN.placeHolderList[165]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle2.scaleZ" "rigwRN.placeHolderList[166]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle2.scaleY" "rigwRN.placeHolderList[167]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle2.scaleX" "rigwRN.placeHolderList[168]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle2.rotateZ" "rigwRN.placeHolderList[169]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle2.rotateY" "rigwRN.placeHolderList[170]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle2.rotateX" "rigwRN.placeHolderList[171]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle2.translateZ" 
		"rigwRN.placeHolderList[172]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle2.translateY" 
		"rigwRN.placeHolderList[173]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle2.translateX" 
		"rigwRN.placeHolderList[174]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle2.visibility" 
		"rigwRN.placeHolderList[175]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle1.scaleZ" "rigwRN.placeHolderList[176]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle1.scaleY" "rigwRN.placeHolderList[177]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle1.scaleX" "rigwRN.placeHolderList[178]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle1.rotateZ" "rigwRN.placeHolderList[179]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle1.rotateY" "rigwRN.placeHolderList[180]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle1.rotateX" "rigwRN.placeHolderList[181]" 
		""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle1.translateZ" 
		"rigwRN.placeHolderList[182]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle1.translateY" 
		"rigwRN.placeHolderList[183]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle1.translateX" 
		"rigwRN.placeHolderList[184]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:nurbsCircle1.visibility" 
		"rigwRN.placeHolderList[185]" ""
		"rigw:rigRN" 0
		"rigwRN" 812
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "translate" 
		" -type \"double3\" 3.936553 0.8362 6.035284"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "rotate" 
		" -type \"double3\" 94.752968 0.460223 0.626802"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "rotateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "translate" 
		" -type \"double3\" 3.92481 1.51982 5.504459"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49" "translate" 
		" -type \"double3\" 3.912503 1.951248 4.559938"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48" "translate" 
		" -type \"double3\" 3.900196 2.382676 3.615418"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42" "translate" 
		" -type \"double3\" 3.88959 2.698813 2.682868"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43" "translate" 
		" -type \"double3\" 3.88959 2.754467 1.698309"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44" "translate" 
		" -type \"double3\" 3.88959 2.754467 0.6702"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45" "translate" 
		" -type \"double3\" 3.88959 2.754467 -0.357909"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46" "translate" 
		" -type \"double3\" 3.88959 2.754467 -1.386019"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47" "translate" 
		" -type \"double3\" 3.88959 2.754467 -2.414128"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52" "translate" 
		" -type \"double3\" 3.88959 2.630392 -3.498546"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53" "translate" 
		" -type \"double3\" 3.88959 2.322473 -4.454273"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54" "translate" 
		" -type \"double3\" 3.88959 1.887221 -5.435571"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55" "translate" 
		" -type \"double3\" 3.88959 1.451968 -6.41687"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55" "rotate" 
		" -type \"double3\" -23.91957 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "translate" 
		" -type \"double3\" 3.88959 0.786892 -6.874434"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "rotate" 
		" -type \"double3\" -90.735333 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "translate" 
		" -type \"double3\" 3.88959 0.0771255 -6.485033"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "rotate" 
		" -type \"double3\" -151.216669 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -5.448837"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -4.429445"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -3.410054"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -2.390662"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -1.371271"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -0.351879"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60" "translate" 
		" -type \"double3\" 3.88959 0.00360929 0.667512"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59" "translate" 
		" -type \"double3\" 3.88959 0.00360929 1.686904"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58" "translate" 
		" -type \"double3\" 3.88959 0.00360929 2.706296"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57" "translate" 
		" -type \"double3\" 3.88959 0.00360929 3.725687"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56" "translate" 
		" -type \"double3\" 3.88959 0.00360929 4.745079"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "translate" 
		" -type \"double3\" 3.88959 0.117146 5.720589"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "rotate" 
		" -type \"double3\" 164.498227 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "translate" 
		" -type \"double3\" -3.936553 0.8362 6.035284"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "rotate" 
		" -type \"double3\" 85.247032 0.460223 179.373198"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "translate" 
		" -type \"double3\" -3.92481 1.51982 5.504459"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92" "translate" 
		" -type \"double3\" -3.912503 1.951248 4.559938"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91" "translate" 
		" -type \"double3\" -3.900196 2.382676 3.615418"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85" "translate" 
		" -type \"double3\" -3.88959 2.698813 2.682868"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86" "translate" 
		" -type \"double3\" -3.88959 2.754467 1.698309"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87" "translate" 
		" -type \"double3\" -3.88959 2.754467 0.6702"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88" "translate" 
		" -type \"double3\" -3.88959 2.754467 -0.357909"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89" "translate" 
		" -type \"double3\" -3.88959 2.754467 -1.386019"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90" "translate" 
		" -type \"double3\" -3.88959 2.754467 -2.414128"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95" "translate" 
		" -type \"double3\" -3.88959 2.630392 -3.498546"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96" "translate" 
		" -type \"double3\" -3.88959 2.322473 -4.454273"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97" "translate" 
		" -type \"double3\" -3.88959 1.887221 -5.435571"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "translate" 
		" -type \"double3\" -3.88959 1.451968 -6.41687"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "rotate" 
		" -type \"double3\" 23.91957 180 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "rotateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "translate" 
		" -type \"double3\" -3.88959 0.786892 -6.874434"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "rotate" 
		" -type \"double3\" -89.264667 0 180"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "translate" 
		" -type \"double3\" -3.88959 0.0771255 -6.485033"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "rotate" 
		" -type \"double3\" -28.783331 0 180"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -5.448837"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -4.429445"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -3.410054"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -2.390662"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -1.371271"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -0.351879"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103" "translate" 
		" -type \"double3\" -3.88959 0.00360929 0.667512"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102" "translate" 
		" -type \"double3\" -3.88959 0.00360929 1.686904"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101" "translate" 
		" -type \"double3\" -3.88959 0.00360929 2.706296"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100" "translate" 
		" -type \"double3\" -3.88959 0.00360929 3.725687"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99" "translate" 
		" -type \"double3\" -3.88959 0.00360929 4.745079"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "translate" 
		" -type \"double3\" -3.88959 0.117146 5.720589"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "rotate" 
		" -type \"double3\" 15.501773 0 180"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "rotateY" 
		" -av"
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.visibility" 
		"rigwRN.placeHolderList[331]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.translateX" 
		"rigwRN.placeHolderList[332]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.translateY" 
		"rigwRN.placeHolderList[333]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.translateZ" 
		"rigwRN.placeHolderList[334]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.rotateX" 
		"rigwRN.placeHolderList[335]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.rotateY" 
		"rigwRN.placeHolderList[336]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.rotateZ" 
		"rigwRN.placeHolderList[337]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.scaleX" 
		"rigwRN.placeHolderList[338]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.scaleY" 
		"rigwRN.placeHolderList[339]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.scaleZ" 
		"rigwRN.placeHolderList[340]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.visibility" 
		"rigwRN.placeHolderList[341]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.translateX" 
		"rigwRN.placeHolderList[342]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.translateY" 
		"rigwRN.placeHolderList[343]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.translateZ" 
		"rigwRN.placeHolderList[344]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.rotateX" 
		"rigwRN.placeHolderList[345]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.rotateY" 
		"rigwRN.placeHolderList[346]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.rotateZ" 
		"rigwRN.placeHolderList[347]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.scaleX" 
		"rigwRN.placeHolderList[348]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.scaleY" 
		"rigwRN.placeHolderList[349]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.scaleZ" 
		"rigwRN.placeHolderList[350]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.visibility" 
		"rigwRN.placeHolderList[351]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.translateX" 
		"rigwRN.placeHolderList[352]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.translateY" 
		"rigwRN.placeHolderList[353]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.translateZ" 
		"rigwRN.placeHolderList[354]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.rotateX" 
		"rigwRN.placeHolderList[355]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.rotateY" 
		"rigwRN.placeHolderList[356]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.rotateZ" 
		"rigwRN.placeHolderList[357]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.scaleX" 
		"rigwRN.placeHolderList[358]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.scaleY" 
		"rigwRN.placeHolderList[359]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.scaleZ" 
		"rigwRN.placeHolderList[360]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.visibility" 
		"rigwRN.placeHolderList[361]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.translateX" 
		"rigwRN.placeHolderList[362]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.translateY" 
		"rigwRN.placeHolderList[363]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.translateZ" 
		"rigwRN.placeHolderList[364]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.rotateX" 
		"rigwRN.placeHolderList[365]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.rotateY" 
		"rigwRN.placeHolderList[366]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.rotateZ" 
		"rigwRN.placeHolderList[367]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.scaleX" 
		"rigwRN.placeHolderList[368]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.scaleY" 
		"rigwRN.placeHolderList[369]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.scaleZ" 
		"rigwRN.placeHolderList[370]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.visibility" 
		"rigwRN.placeHolderList[371]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.translateX" 
		"rigwRN.placeHolderList[372]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.translateY" 
		"rigwRN.placeHolderList[373]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.translateZ" 
		"rigwRN.placeHolderList[374]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.rotateX" 
		"rigwRN.placeHolderList[375]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.rotateY" 
		"rigwRN.placeHolderList[376]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.rotateZ" 
		"rigwRN.placeHolderList[377]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.scaleX" 
		"rigwRN.placeHolderList[378]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.scaleY" 
		"rigwRN.placeHolderList[379]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.scaleZ" 
		"rigwRN.placeHolderList[380]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.visibility" 
		"rigwRN.placeHolderList[381]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.translateX" 
		"rigwRN.placeHolderList[382]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.translateY" 
		"rigwRN.placeHolderList[383]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.translateZ" 
		"rigwRN.placeHolderList[384]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.rotateX" 
		"rigwRN.placeHolderList[385]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.rotateY" 
		"rigwRN.placeHolderList[386]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.rotateZ" 
		"rigwRN.placeHolderList[387]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.scaleX" 
		"rigwRN.placeHolderList[388]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.scaleY" 
		"rigwRN.placeHolderList[389]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.scaleZ" 
		"rigwRN.placeHolderList[390]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.visibility" 
		"rigwRN.placeHolderList[391]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.translateX" 
		"rigwRN.placeHolderList[392]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.translateY" 
		"rigwRN.placeHolderList[393]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.translateZ" 
		"rigwRN.placeHolderList[394]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.rotateX" 
		"rigwRN.placeHolderList[395]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.rotateY" 
		"rigwRN.placeHolderList[396]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.rotateZ" 
		"rigwRN.placeHolderList[397]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.scaleX" 
		"rigwRN.placeHolderList[398]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.scaleY" 
		"rigwRN.placeHolderList[399]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.scaleZ" 
		"rigwRN.placeHolderList[400]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.visibility" 
		"rigwRN.placeHolderList[401]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.translateX" 
		"rigwRN.placeHolderList[402]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.translateY" 
		"rigwRN.placeHolderList[403]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.translateZ" 
		"rigwRN.placeHolderList[404]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.rotateX" 
		"rigwRN.placeHolderList[405]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.rotateY" 
		"rigwRN.placeHolderList[406]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.rotateZ" 
		"rigwRN.placeHolderList[407]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.scaleX" 
		"rigwRN.placeHolderList[408]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.scaleY" 
		"rigwRN.placeHolderList[409]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.scaleZ" 
		"rigwRN.placeHolderList[410]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.visibility" 
		"rigwRN.placeHolderList[411]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.translateX" 
		"rigwRN.placeHolderList[412]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.translateY" 
		"rigwRN.placeHolderList[413]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.translateZ" 
		"rigwRN.placeHolderList[414]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.rotateX" 
		"rigwRN.placeHolderList[415]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.rotateY" 
		"rigwRN.placeHolderList[416]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.rotateZ" 
		"rigwRN.placeHolderList[417]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.scaleX" 
		"rigwRN.placeHolderList[418]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.scaleY" 
		"rigwRN.placeHolderList[419]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.scaleZ" 
		"rigwRN.placeHolderList[420]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.visibility" 
		"rigwRN.placeHolderList[421]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.translateX" 
		"rigwRN.placeHolderList[422]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.translateY" 
		"rigwRN.placeHolderList[423]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.translateZ" 
		"rigwRN.placeHolderList[424]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.rotateX" 
		"rigwRN.placeHolderList[425]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.rotateY" 
		"rigwRN.placeHolderList[426]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.rotateZ" 
		"rigwRN.placeHolderList[427]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.scaleX" 
		"rigwRN.placeHolderList[428]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.scaleY" 
		"rigwRN.placeHolderList[429]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.scaleZ" 
		"rigwRN.placeHolderList[430]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.visibility" 
		"rigwRN.placeHolderList[431]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.translateX" 
		"rigwRN.placeHolderList[432]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.translateY" 
		"rigwRN.placeHolderList[433]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.translateZ" 
		"rigwRN.placeHolderList[434]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.rotateX" 
		"rigwRN.placeHolderList[435]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.rotateY" 
		"rigwRN.placeHolderList[436]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.rotateZ" 
		"rigwRN.placeHolderList[437]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.scaleX" 
		"rigwRN.placeHolderList[438]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.scaleY" 
		"rigwRN.placeHolderList[439]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.scaleZ" 
		"rigwRN.placeHolderList[440]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.visibility" 
		"rigwRN.placeHolderList[441]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.translateX" 
		"rigwRN.placeHolderList[442]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.translateY" 
		"rigwRN.placeHolderList[443]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.translateZ" 
		"rigwRN.placeHolderList[444]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.rotateX" 
		"rigwRN.placeHolderList[445]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.rotateY" 
		"rigwRN.placeHolderList[446]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.rotateZ" 
		"rigwRN.placeHolderList[447]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.scaleX" 
		"rigwRN.placeHolderList[448]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.scaleY" 
		"rigwRN.placeHolderList[449]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.scaleZ" 
		"rigwRN.placeHolderList[450]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.visibility" 
		"rigwRN.placeHolderList[451]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.translateX" 
		"rigwRN.placeHolderList[452]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.translateY" 
		"rigwRN.placeHolderList[453]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.translateZ" 
		"rigwRN.placeHolderList[454]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.rotateX" 
		"rigwRN.placeHolderList[455]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.rotateY" 
		"rigwRN.placeHolderList[456]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.rotateZ" 
		"rigwRN.placeHolderList[457]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.scaleX" 
		"rigwRN.placeHolderList[458]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.scaleY" 
		"rigwRN.placeHolderList[459]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.scaleZ" 
		"rigwRN.placeHolderList[460]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.visibility" 
		"rigwRN.placeHolderList[461]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.translateX" 
		"rigwRN.placeHolderList[462]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.translateY" 
		"rigwRN.placeHolderList[463]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.translateZ" 
		"rigwRN.placeHolderList[464]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.rotateX" 
		"rigwRN.placeHolderList[465]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.rotateY" 
		"rigwRN.placeHolderList[466]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.rotateZ" 
		"rigwRN.placeHolderList[467]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.scaleX" 
		"rigwRN.placeHolderList[468]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.scaleY" 
		"rigwRN.placeHolderList[469]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.scaleZ" 
		"rigwRN.placeHolderList[470]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.visibility" 
		"rigwRN.placeHolderList[471]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.translateX" 
		"rigwRN.placeHolderList[472]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.translateY" 
		"rigwRN.placeHolderList[473]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.translateZ" 
		"rigwRN.placeHolderList[474]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.rotateX" 
		"rigwRN.placeHolderList[475]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.rotateY" 
		"rigwRN.placeHolderList[476]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.rotateZ" 
		"rigwRN.placeHolderList[477]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.scaleX" 
		"rigwRN.placeHolderList[478]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.scaleY" 
		"rigwRN.placeHolderList[479]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.scaleZ" 
		"rigwRN.placeHolderList[480]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.visibility" 
		"rigwRN.placeHolderList[481]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.translateX" 
		"rigwRN.placeHolderList[482]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.translateY" 
		"rigwRN.placeHolderList[483]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.translateZ" 
		"rigwRN.placeHolderList[484]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.rotateX" 
		"rigwRN.placeHolderList[485]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.rotateY" 
		"rigwRN.placeHolderList[486]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.rotateZ" 
		"rigwRN.placeHolderList[487]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.scaleX" 
		"rigwRN.placeHolderList[488]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.scaleY" 
		"rigwRN.placeHolderList[489]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.scaleZ" 
		"rigwRN.placeHolderList[490]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.visibility" 
		"rigwRN.placeHolderList[491]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.translateX" 
		"rigwRN.placeHolderList[492]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.translateY" 
		"rigwRN.placeHolderList[493]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.translateZ" 
		"rigwRN.placeHolderList[494]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.rotateX" 
		"rigwRN.placeHolderList[495]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.rotateY" 
		"rigwRN.placeHolderList[496]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.rotateZ" 
		"rigwRN.placeHolderList[497]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.scaleX" 
		"rigwRN.placeHolderList[498]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.scaleY" 
		"rigwRN.placeHolderList[499]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.scaleZ" 
		"rigwRN.placeHolderList[500]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.visibility" 
		"rigwRN.placeHolderList[501]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.translateX" 
		"rigwRN.placeHolderList[502]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.translateY" 
		"rigwRN.placeHolderList[503]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.translateZ" 
		"rigwRN.placeHolderList[504]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.rotateX" 
		"rigwRN.placeHolderList[505]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.rotateY" 
		"rigwRN.placeHolderList[506]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.rotateZ" 
		"rigwRN.placeHolderList[507]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.scaleX" 
		"rigwRN.placeHolderList[508]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.scaleY" 
		"rigwRN.placeHolderList[509]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.scaleZ" 
		"rigwRN.placeHolderList[510]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.visibility" 
		"rigwRN.placeHolderList[511]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.translateX" 
		"rigwRN.placeHolderList[512]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.translateY" 
		"rigwRN.placeHolderList[513]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.translateZ" 
		"rigwRN.placeHolderList[514]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.rotateX" 
		"rigwRN.placeHolderList[515]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.rotateY" 
		"rigwRN.placeHolderList[516]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.rotateZ" 
		"rigwRN.placeHolderList[517]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.scaleX" 
		"rigwRN.placeHolderList[518]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.scaleY" 
		"rigwRN.placeHolderList[519]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.scaleZ" 
		"rigwRN.placeHolderList[520]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.visibility" 
		"rigwRN.placeHolderList[521]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.translateX" 
		"rigwRN.placeHolderList[522]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.translateY" 
		"rigwRN.placeHolderList[523]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.translateZ" 
		"rigwRN.placeHolderList[524]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.rotateX" 
		"rigwRN.placeHolderList[525]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.rotateY" 
		"rigwRN.placeHolderList[526]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.rotateZ" 
		"rigwRN.placeHolderList[527]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.scaleX" 
		"rigwRN.placeHolderList[528]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.scaleY" 
		"rigwRN.placeHolderList[529]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.scaleZ" 
		"rigwRN.placeHolderList[530]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.visibility" 
		"rigwRN.placeHolderList[531]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.translateX" 
		"rigwRN.placeHolderList[532]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.translateY" 
		"rigwRN.placeHolderList[533]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.translateZ" 
		"rigwRN.placeHolderList[534]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.rotateX" 
		"rigwRN.placeHolderList[535]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.rotateY" 
		"rigwRN.placeHolderList[536]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.rotateZ" 
		"rigwRN.placeHolderList[537]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.scaleX" 
		"rigwRN.placeHolderList[538]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.scaleY" 
		"rigwRN.placeHolderList[539]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.scaleZ" 
		"rigwRN.placeHolderList[540]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.visibility" 
		"rigwRN.placeHolderList[541]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.translateX" 
		"rigwRN.placeHolderList[542]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.translateY" 
		"rigwRN.placeHolderList[543]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.translateZ" 
		"rigwRN.placeHolderList[544]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.rotateX" 
		"rigwRN.placeHolderList[545]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.rotateY" 
		"rigwRN.placeHolderList[546]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.rotateZ" 
		"rigwRN.placeHolderList[547]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.scaleX" 
		"rigwRN.placeHolderList[548]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.scaleY" 
		"rigwRN.placeHolderList[549]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.scaleZ" 
		"rigwRN.placeHolderList[550]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.visibility" 
		"rigwRN.placeHolderList[551]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.translateX" 
		"rigwRN.placeHolderList[552]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.translateY" 
		"rigwRN.placeHolderList[553]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.translateZ" 
		"rigwRN.placeHolderList[554]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.rotateX" 
		"rigwRN.placeHolderList[555]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.rotateY" 
		"rigwRN.placeHolderList[556]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.rotateZ" 
		"rigwRN.placeHolderList[557]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.scaleX" 
		"rigwRN.placeHolderList[558]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.scaleY" 
		"rigwRN.placeHolderList[559]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.scaleZ" 
		"rigwRN.placeHolderList[560]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.visibility" 
		"rigwRN.placeHolderList[561]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.translateX" 
		"rigwRN.placeHolderList[562]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.translateY" 
		"rigwRN.placeHolderList[563]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.translateZ" 
		"rigwRN.placeHolderList[564]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.rotateX" 
		"rigwRN.placeHolderList[565]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.rotateY" 
		"rigwRN.placeHolderList[566]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.rotateZ" 
		"rigwRN.placeHolderList[567]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.scaleX" 
		"rigwRN.placeHolderList[568]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.scaleY" 
		"rigwRN.placeHolderList[569]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.scaleZ" 
		"rigwRN.placeHolderList[570]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.visibility" 
		"rigwRN.placeHolderList[571]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.translateX" 
		"rigwRN.placeHolderList[572]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.translateY" 
		"rigwRN.placeHolderList[573]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.translateZ" 
		"rigwRN.placeHolderList[574]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.rotateX" 
		"rigwRN.placeHolderList[575]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.rotateY" 
		"rigwRN.placeHolderList[576]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.rotateZ" 
		"rigwRN.placeHolderList[577]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.scaleX" 
		"rigwRN.placeHolderList[578]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.scaleY" 
		"rigwRN.placeHolderList[579]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.scaleZ" 
		"rigwRN.placeHolderList[580]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.visibility" 
		"rigwRN.placeHolderList[581]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.translateX" 
		"rigwRN.placeHolderList[582]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.translateY" 
		"rigwRN.placeHolderList[583]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.translateZ" 
		"rigwRN.placeHolderList[584]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.rotateX" 
		"rigwRN.placeHolderList[585]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.rotateY" 
		"rigwRN.placeHolderList[586]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.rotateZ" 
		"rigwRN.placeHolderList[587]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.scaleX" 
		"rigwRN.placeHolderList[588]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.scaleY" 
		"rigwRN.placeHolderList[589]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.scaleZ" 
		"rigwRN.placeHolderList[590]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.visibility" 
		"rigwRN.placeHolderList[591]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.translateX" 
		"rigwRN.placeHolderList[592]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.translateY" 
		"rigwRN.placeHolderList[593]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.translateZ" 
		"rigwRN.placeHolderList[594]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.rotateX" 
		"rigwRN.placeHolderList[595]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.rotateY" 
		"rigwRN.placeHolderList[596]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.rotateZ" 
		"rigwRN.placeHolderList[597]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.scaleX" 
		"rigwRN.placeHolderList[598]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.scaleY" 
		"rigwRN.placeHolderList[599]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.scaleZ" 
		"rigwRN.placeHolderList[600]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.visibility" 
		"rigwRN.placeHolderList[601]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.translateX" 
		"rigwRN.placeHolderList[602]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.translateY" 
		"rigwRN.placeHolderList[603]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.translateZ" 
		"rigwRN.placeHolderList[604]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.rotateX" 
		"rigwRN.placeHolderList[605]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.rotateY" 
		"rigwRN.placeHolderList[606]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.rotateZ" 
		"rigwRN.placeHolderList[607]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.scaleX" 
		"rigwRN.placeHolderList[608]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.scaleY" 
		"rigwRN.placeHolderList[609]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.scaleZ" 
		"rigwRN.placeHolderList[610]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.visibility" 
		"rigwRN.placeHolderList[611]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.translateX" 
		"rigwRN.placeHolderList[612]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.translateY" 
		"rigwRN.placeHolderList[613]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.translateZ" 
		"rigwRN.placeHolderList[614]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.rotateX" 
		"rigwRN.placeHolderList[615]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.rotateY" 
		"rigwRN.placeHolderList[616]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.rotateZ" 
		"rigwRN.placeHolderList[617]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.scaleX" 
		"rigwRN.placeHolderList[618]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.scaleY" 
		"rigwRN.placeHolderList[619]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.scaleZ" 
		"rigwRN.placeHolderList[620]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.visibility" 
		"rigwRN.placeHolderList[621]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.translateX" 
		"rigwRN.placeHolderList[622]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.translateY" 
		"rigwRN.placeHolderList[623]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.translateZ" 
		"rigwRN.placeHolderList[624]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.rotateX" 
		"rigwRN.placeHolderList[625]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.rotateY" 
		"rigwRN.placeHolderList[626]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.rotateZ" 
		"rigwRN.placeHolderList[627]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.scaleX" 
		"rigwRN.placeHolderList[628]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.scaleY" 
		"rigwRN.placeHolderList[629]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.scaleZ" 
		"rigwRN.placeHolderList[630]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.visibility" 
		"rigwRN.placeHolderList[631]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.translateX" 
		"rigwRN.placeHolderList[632]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.translateY" 
		"rigwRN.placeHolderList[633]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.translateZ" 
		"rigwRN.placeHolderList[634]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.rotateX" 
		"rigwRN.placeHolderList[635]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.rotateY" 
		"rigwRN.placeHolderList[636]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.rotateZ" 
		"rigwRN.placeHolderList[637]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.scaleX" 
		"rigwRN.placeHolderList[638]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.scaleY" 
		"rigwRN.placeHolderList[639]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.scaleZ" 
		"rigwRN.placeHolderList[640]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.visibility" 
		"rigwRN.placeHolderList[641]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.translateX" 
		"rigwRN.placeHolderList[642]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.translateY" 
		"rigwRN.placeHolderList[643]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.translateZ" 
		"rigwRN.placeHolderList[644]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.rotateX" 
		"rigwRN.placeHolderList[645]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.rotateY" 
		"rigwRN.placeHolderList[646]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.rotateZ" 
		"rigwRN.placeHolderList[647]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.scaleX" 
		"rigwRN.placeHolderList[648]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.scaleY" 
		"rigwRN.placeHolderList[649]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.scaleZ" 
		"rigwRN.placeHolderList[650]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.visibility" 
		"rigwRN.placeHolderList[651]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.translateX" 
		"rigwRN.placeHolderList[652]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.translateY" 
		"rigwRN.placeHolderList[653]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.translateZ" 
		"rigwRN.placeHolderList[654]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.rotateX" 
		"rigwRN.placeHolderList[655]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.rotateY" 
		"rigwRN.placeHolderList[656]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.rotateZ" 
		"rigwRN.placeHolderList[657]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.scaleX" 
		"rigwRN.placeHolderList[658]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.scaleY" 
		"rigwRN.placeHolderList[659]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.scaleZ" 
		"rigwRN.placeHolderList[660]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.visibility" 
		"rigwRN.placeHolderList[661]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.translateX" 
		"rigwRN.placeHolderList[662]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.translateY" 
		"rigwRN.placeHolderList[663]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.translateZ" 
		"rigwRN.placeHolderList[664]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.rotateX" 
		"rigwRN.placeHolderList[665]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.rotateY" 
		"rigwRN.placeHolderList[666]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.rotateZ" 
		"rigwRN.placeHolderList[667]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.scaleX" 
		"rigwRN.placeHolderList[668]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.scaleY" 
		"rigwRN.placeHolderList[669]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.scaleZ" 
		"rigwRN.placeHolderList[670]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.visibility" 
		"rigwRN.placeHolderList[671]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.translateX" 
		"rigwRN.placeHolderList[672]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.translateY" 
		"rigwRN.placeHolderList[673]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.translateZ" 
		"rigwRN.placeHolderList[674]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.rotateX" 
		"rigwRN.placeHolderList[675]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.rotateY" 
		"rigwRN.placeHolderList[676]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.rotateZ" 
		"rigwRN.placeHolderList[677]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.scaleX" 
		"rigwRN.placeHolderList[678]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.scaleY" 
		"rigwRN.placeHolderList[679]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.scaleZ" 
		"rigwRN.placeHolderList[680]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.visibility" 
		"rigwRN.placeHolderList[681]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.translateX" 
		"rigwRN.placeHolderList[682]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.translateY" 
		"rigwRN.placeHolderList[683]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.translateZ" 
		"rigwRN.placeHolderList[684]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.rotateX" 
		"rigwRN.placeHolderList[685]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.rotateY" 
		"rigwRN.placeHolderList[686]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.rotateZ" 
		"rigwRN.placeHolderList[687]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.scaleX" 
		"rigwRN.placeHolderList[688]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.scaleY" 
		"rigwRN.placeHolderList[689]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.scaleZ" 
		"rigwRN.placeHolderList[690]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.visibility" 
		"rigwRN.placeHolderList[691]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.translateX" 
		"rigwRN.placeHolderList[692]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.translateY" 
		"rigwRN.placeHolderList[693]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.translateZ" 
		"rigwRN.placeHolderList[694]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.rotateX" 
		"rigwRN.placeHolderList[695]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.rotateY" 
		"rigwRN.placeHolderList[696]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.rotateZ" 
		"rigwRN.placeHolderList[697]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.scaleX" 
		"rigwRN.placeHolderList[698]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.scaleY" 
		"rigwRN.placeHolderList[699]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.scaleZ" 
		"rigwRN.placeHolderList[700]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.visibility" 
		"rigwRN.placeHolderList[701]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.translateX" 
		"rigwRN.placeHolderList[702]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.translateY" 
		"rigwRN.placeHolderList[703]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.translateZ" 
		"rigwRN.placeHolderList[704]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.rotateX" 
		"rigwRN.placeHolderList[705]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.rotateY" 
		"rigwRN.placeHolderList[706]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.rotateZ" 
		"rigwRN.placeHolderList[707]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.scaleX" 
		"rigwRN.placeHolderList[708]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.scaleY" 
		"rigwRN.placeHolderList[709]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.scaleZ" 
		"rigwRN.placeHolderList[710]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.visibility" 
		"rigwRN.placeHolderList[711]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.translateX" 
		"rigwRN.placeHolderList[712]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.translateY" 
		"rigwRN.placeHolderList[713]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.translateZ" 
		"rigwRN.placeHolderList[714]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.rotateX" 
		"rigwRN.placeHolderList[715]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.rotateY" 
		"rigwRN.placeHolderList[716]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.rotateZ" 
		"rigwRN.placeHolderList[717]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.scaleX" 
		"rigwRN.placeHolderList[718]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.scaleY" 
		"rigwRN.placeHolderList[719]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.scaleZ" 
		"rigwRN.placeHolderList[720]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.visibility" 
		"rigwRN.placeHolderList[721]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.translateX" 
		"rigwRN.placeHolderList[722]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.translateY" 
		"rigwRN.placeHolderList[723]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.translateZ" 
		"rigwRN.placeHolderList[724]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.rotateX" 
		"rigwRN.placeHolderList[725]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.rotateY" 
		"rigwRN.placeHolderList[726]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.rotateZ" 
		"rigwRN.placeHolderList[727]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.scaleX" 
		"rigwRN.placeHolderList[728]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.scaleY" 
		"rigwRN.placeHolderList[729]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.scaleZ" 
		"rigwRN.placeHolderList[730]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.visibility" 
		"rigwRN.placeHolderList[731]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.translateX" 
		"rigwRN.placeHolderList[732]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.translateY" 
		"rigwRN.placeHolderList[733]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.translateZ" 
		"rigwRN.placeHolderList[734]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.rotateX" 
		"rigwRN.placeHolderList[735]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.rotateY" 
		"rigwRN.placeHolderList[736]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.rotateZ" 
		"rigwRN.placeHolderList[737]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.scaleX" 
		"rigwRN.placeHolderList[738]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.scaleY" 
		"rigwRN.placeHolderList[739]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.scaleZ" 
		"rigwRN.placeHolderList[740]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.visibility" 
		"rigwRN.placeHolderList[741]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.translateX" 
		"rigwRN.placeHolderList[742]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.translateY" 
		"rigwRN.placeHolderList[743]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.translateZ" 
		"rigwRN.placeHolderList[744]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.rotateX" 
		"rigwRN.placeHolderList[745]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.rotateY" 
		"rigwRN.placeHolderList[746]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.rotateZ" 
		"rigwRN.placeHolderList[747]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.scaleX" 
		"rigwRN.placeHolderList[748]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.scaleY" 
		"rigwRN.placeHolderList[749]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.scaleZ" 
		"rigwRN.placeHolderList[750]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.visibility" 
		"rigwRN.placeHolderList[751]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.translateX" 
		"rigwRN.placeHolderList[752]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.translateY" 
		"rigwRN.placeHolderList[753]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.translateZ" 
		"rigwRN.placeHolderList[754]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.rotateX" 
		"rigwRN.placeHolderList[755]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.rotateY" 
		"rigwRN.placeHolderList[756]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.rotateZ" 
		"rigwRN.placeHolderList[757]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.scaleX" 
		"rigwRN.placeHolderList[758]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.scaleY" 
		"rigwRN.placeHolderList[759]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.scaleZ" 
		"rigwRN.placeHolderList[760]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.visibility" 
		"rigwRN.placeHolderList[761]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.translateX" 
		"rigwRN.placeHolderList[762]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.translateY" 
		"rigwRN.placeHolderList[763]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.translateZ" 
		"rigwRN.placeHolderList[764]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.rotateX" 
		"rigwRN.placeHolderList[765]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.rotateY" 
		"rigwRN.placeHolderList[766]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.rotateZ" 
		"rigwRN.placeHolderList[767]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.scaleX" 
		"rigwRN.placeHolderList[768]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.scaleY" 
		"rigwRN.placeHolderList[769]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.scaleZ" 
		"rigwRN.placeHolderList[770]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.visibility" 
		"rigwRN.placeHolderList[771]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.translateX" 
		"rigwRN.placeHolderList[772]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.translateY" 
		"rigwRN.placeHolderList[773]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.translateZ" 
		"rigwRN.placeHolderList[774]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.rotateX" 
		"rigwRN.placeHolderList[775]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.rotateY" 
		"rigwRN.placeHolderList[776]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.rotateZ" 
		"rigwRN.placeHolderList[777]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.scaleX" 
		"rigwRN.placeHolderList[778]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.scaleY" 
		"rigwRN.placeHolderList[779]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.scaleZ" 
		"rigwRN.placeHolderList[780]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.visibility" 
		"rigwRN.placeHolderList[781]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.translateX" 
		"rigwRN.placeHolderList[782]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.translateY" 
		"rigwRN.placeHolderList[783]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.translateZ" 
		"rigwRN.placeHolderList[784]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.rotateX" 
		"rigwRN.placeHolderList[785]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.rotateY" 
		"rigwRN.placeHolderList[786]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.rotateZ" 
		"rigwRN.placeHolderList[787]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.scaleX" 
		"rigwRN.placeHolderList[788]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.scaleY" 
		"rigwRN.placeHolderList[789]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.scaleZ" 
		"rigwRN.placeHolderList[790]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.visibility" 
		"rigwRN.placeHolderList[791]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.translateX" 
		"rigwRN.placeHolderList[792]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.translateY" 
		"rigwRN.placeHolderList[793]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.translateZ" 
		"rigwRN.placeHolderList[794]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.rotateX" 
		"rigwRN.placeHolderList[795]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.rotateY" 
		"rigwRN.placeHolderList[796]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.rotateZ" 
		"rigwRN.placeHolderList[797]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.scaleX" 
		"rigwRN.placeHolderList[798]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.scaleY" 
		"rigwRN.placeHolderList[799]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.scaleZ" 
		"rigwRN.placeHolderList[800]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.visibility" 
		"rigwRN.placeHolderList[801]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.translateX" 
		"rigwRN.placeHolderList[802]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.translateY" 
		"rigwRN.placeHolderList[803]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.translateZ" 
		"rigwRN.placeHolderList[804]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.rotateX" 
		"rigwRN.placeHolderList[805]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.rotateY" 
		"rigwRN.placeHolderList[806]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.rotateZ" 
		"rigwRN.placeHolderList[807]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.scaleX" 
		"rigwRN.placeHolderList[808]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.scaleY" 
		"rigwRN.placeHolderList[809]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.scaleZ" 
		"rigwRN.placeHolderList[810]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.visibility" 
		"rigwRN.placeHolderList[811]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.translateX" 
		"rigwRN.placeHolderList[812]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.translateY" 
		"rigwRN.placeHolderList[813]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.translateZ" 
		"rigwRN.placeHolderList[814]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.rotateX" 
		"rigwRN.placeHolderList[815]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.rotateY" 
		"rigwRN.placeHolderList[816]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.rotateZ" 
		"rigwRN.placeHolderList[817]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.scaleX" 
		"rigwRN.placeHolderList[818]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.scaleY" 
		"rigwRN.placeHolderList[819]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.scaleZ" 
		"rigwRN.placeHolderList[820]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.visibility" 
		"rigwRN.placeHolderList[821]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.translateX" 
		"rigwRN.placeHolderList[822]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.translateY" 
		"rigwRN.placeHolderList[823]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.translateZ" 
		"rigwRN.placeHolderList[824]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.rotateX" 
		"rigwRN.placeHolderList[825]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.rotateY" 
		"rigwRN.placeHolderList[826]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.rotateZ" 
		"rigwRN.placeHolderList[827]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.scaleX" 
		"rigwRN.placeHolderList[828]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.scaleY" 
		"rigwRN.placeHolderList[829]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.scaleZ" 
		"rigwRN.placeHolderList[830]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.visibility" 
		"rigwRN.placeHolderList[831]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.translateX" 
		"rigwRN.placeHolderList[832]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.translateY" 
		"rigwRN.placeHolderList[833]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.translateZ" 
		"rigwRN.placeHolderList[834]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.rotateX" 
		"rigwRN.placeHolderList[835]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.rotateY" 
		"rigwRN.placeHolderList[836]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.rotateZ" 
		"rigwRN.placeHolderList[837]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.scaleX" 
		"rigwRN.placeHolderList[838]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.scaleY" 
		"rigwRN.placeHolderList[839]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.scaleZ" 
		"rigwRN.placeHolderList[840]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.visibility" 
		"rigwRN.placeHolderList[841]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.translateX" 
		"rigwRN.placeHolderList[842]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.translateY" 
		"rigwRN.placeHolderList[843]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.translateZ" 
		"rigwRN.placeHolderList[844]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.rotateX" 
		"rigwRN.placeHolderList[845]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.rotateY" 
		"rigwRN.placeHolderList[846]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.rotateZ" 
		"rigwRN.placeHolderList[847]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.scaleX" 
		"rigwRN.placeHolderList[848]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.scaleY" 
		"rigwRN.placeHolderList[849]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.scaleZ" 
		"rigwRN.placeHolderList[850]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.visibility" 
		"rigwRN.placeHolderList[851]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.translateX" 
		"rigwRN.placeHolderList[852]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.translateY" 
		"rigwRN.placeHolderList[853]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.translateZ" 
		"rigwRN.placeHolderList[854]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.rotateX" 
		"rigwRN.placeHolderList[855]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.rotateY" 
		"rigwRN.placeHolderList[856]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.rotateZ" 
		"rigwRN.placeHolderList[857]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.scaleX" 
		"rigwRN.placeHolderList[858]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.scaleY" 
		"rigwRN.placeHolderList[859]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.scaleZ" 
		"rigwRN.placeHolderList[860]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.visibility" 
		"rigwRN.placeHolderList[861]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.translateX" 
		"rigwRN.placeHolderList[862]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.translateY" 
		"rigwRN.placeHolderList[863]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.translateZ" 
		"rigwRN.placeHolderList[864]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.rotateX" 
		"rigwRN.placeHolderList[865]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.rotateY" 
		"rigwRN.placeHolderList[866]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.rotateZ" 
		"rigwRN.placeHolderList[867]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.scaleX" 
		"rigwRN.placeHolderList[868]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.scaleY" 
		"rigwRN.placeHolderList[869]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.scaleZ" 
		"rigwRN.placeHolderList[870]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.visibility" 
		"rigwRN.placeHolderList[871]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.translateX" 
		"rigwRN.placeHolderList[872]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.translateY" 
		"rigwRN.placeHolderList[873]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.translateZ" 
		"rigwRN.placeHolderList[874]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.rotateX" 
		"rigwRN.placeHolderList[875]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.rotateY" 
		"rigwRN.placeHolderList[876]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.rotateZ" 
		"rigwRN.placeHolderList[877]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.scaleX" 
		"rigwRN.placeHolderList[878]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.scaleY" 
		"rigwRN.placeHolderList[879]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.scaleZ" 
		"rigwRN.placeHolderList[880]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.visibility" 
		"rigwRN.placeHolderList[881]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.translateX" 
		"rigwRN.placeHolderList[882]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.translateY" 
		"rigwRN.placeHolderList[883]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.translateZ" 
		"rigwRN.placeHolderList[884]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.rotateX" 
		"rigwRN.placeHolderList[885]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.rotateY" 
		"rigwRN.placeHolderList[886]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.rotateZ" 
		"rigwRN.placeHolderList[887]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.scaleX" 
		"rigwRN.placeHolderList[888]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.scaleY" 
		"rigwRN.placeHolderList[889]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.scaleZ" 
		"rigwRN.placeHolderList[890]" ""
		"rigw:rigRN" 331
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "visibility" " -av 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scale" " -type \"double3\" 2 2 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translate" " -type \"double3\" 0 -0.305163 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"CenterBtwFeet" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M" 
		"rotate" " -type \"double3\" 0.700384 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" 0 0 10.540935"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"Global" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotate" " -type \"double3\" 0 0 6.254666"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotate" " -type \"double3\" 4.915441 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotate" " -type \"double3\" 4.915441 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotate" " -type \"double3\" 4.915441 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotate" " -type \"double3\" 4.915441 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotate" " -type \"double3\" 4.915441 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotate" " -type \"double3\" 4.915441 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotate" " -type \"double3\" 0 0 6.254666"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"Global" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 -1.705547"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotate" " -type \"double3\" 3.239061 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotate" " -type \"double3\" -4.324663 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotate" " -type \"double3\" 3.239061 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotate" " -type \"double3\" -4.324663 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotate" " -type \"double3\" 3.239061 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotate" " -type \"double3\" -4.324663 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"IKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"IKVis" " -av -k 1 1"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateX" "rigwRN.placeHolderList[186]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateY" "rigwRN.placeHolderList[187]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateZ" "rigwRN.placeHolderList[188]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.visibility" "rigwRN.placeHolderList[189]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateX" "rigwRN.placeHolderList[190]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateY" "rigwRN.placeHolderList[191]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateZ" "rigwRN.placeHolderList[192]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleX" "rigwRN.placeHolderList[193]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleY" "rigwRN.placeHolderList[194]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleZ" "rigwRN.placeHolderList[195]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateX" 
		"rigwRN.placeHolderList[196]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateY" 
		"rigwRN.placeHolderList[197]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateZ" 
		"rigwRN.placeHolderList[198]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateX" 
		"rigwRN.placeHolderList[199]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateY" 
		"rigwRN.placeHolderList[200]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateZ" 
		"rigwRN.placeHolderList[201]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateX" 
		"rigwRN.placeHolderList[202]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateY" 
		"rigwRN.placeHolderList[203]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateZ" 
		"rigwRN.placeHolderList[204]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateX" 
		"rigwRN.placeHolderList[205]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateY" 
		"rigwRN.placeHolderList[206]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateZ" 
		"rigwRN.placeHolderList[207]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R.rotateX" 
		"rigwRN.placeHolderList[208]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R.rotateY" 
		"rigwRN.placeHolderList[209]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R.rotateZ" 
		"rigwRN.placeHolderList[210]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R.rotateX" 
		"rigwRN.placeHolderList[211]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R.rotateY" 
		"rigwRN.placeHolderList[212]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R.rotateZ" 
		"rigwRN.placeHolderList[213]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateX" 
		"rigwRN.placeHolderList[214]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateY" 
		"rigwRN.placeHolderList[215]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateZ" 
		"rigwRN.placeHolderList[216]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateX" 
		"rigwRN.placeHolderList[217]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateY" 
		"rigwRN.placeHolderList[218]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateZ" 
		"rigwRN.placeHolderList[219]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateX" 
		"rigwRN.placeHolderList[220]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateY" 
		"rigwRN.placeHolderList[221]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateZ" 
		"rigwRN.placeHolderList[222]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateX" 
		"rigwRN.placeHolderList[223]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateY" 
		"rigwRN.placeHolderList[224]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateZ" 
		"rigwRN.placeHolderList[225]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L.rotateX" 
		"rigwRN.placeHolderList[226]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L.rotateY" 
		"rigwRN.placeHolderList[227]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L.rotateZ" 
		"rigwRN.placeHolderList[228]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L.rotateX" 
		"rigwRN.placeHolderList[229]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L.rotateY" 
		"rigwRN.placeHolderList[230]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L.rotateZ" 
		"rigwRN.placeHolderList[231]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[232]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[233]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[234]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[235]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[236]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[237]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[238]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateX" 
		"rigwRN.placeHolderList[239]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateY" 
		"rigwRN.placeHolderList[240]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M.rotateZ" 
		"rigwRN.placeHolderList[241]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M.rotateX" 
		"rigwRN.placeHolderList[242]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M.rotateY" 
		"rigwRN.placeHolderList[243]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M.rotateZ" 
		"rigwRN.placeHolderList[244]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.Global" 
		"rigwRN.placeHolderList[245]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[246]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[247]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[248]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateX" 
		"rigwRN.placeHolderList[249]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateY" 
		"rigwRN.placeHolderList[250]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateZ" 
		"rigwRN.placeHolderList[251]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateX" 
		"rigwRN.placeHolderList[252]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateY" 
		"rigwRN.placeHolderList[253]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[254]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateX" 
		"rigwRN.placeHolderList[255]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateY" 
		"rigwRN.placeHolderList[256]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[257]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateX" 
		"rigwRN.placeHolderList[258]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateY" 
		"rigwRN.placeHolderList[259]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[260]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateX" 
		"rigwRN.placeHolderList[261]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateY" 
		"rigwRN.placeHolderList[262]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[263]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateX" 
		"rigwRN.placeHolderList[264]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateY" 
		"rigwRN.placeHolderList[265]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[266]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateX" 
		"rigwRN.placeHolderList[267]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateY" 
		"rigwRN.placeHolderList[268]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[269]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.Global" 
		"rigwRN.placeHolderList[270]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateX" 
		"rigwRN.placeHolderList[271]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateY" 
		"rigwRN.placeHolderList[272]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateZ" 
		"rigwRN.placeHolderList[273]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[274]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[275]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[276]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateX" 
		"rigwRN.placeHolderList[277]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateY" 
		"rigwRN.placeHolderList[278]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[279]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateX" 
		"rigwRN.placeHolderList[280]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateY" 
		"rigwRN.placeHolderList[281]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[282]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateX" 
		"rigwRN.placeHolderList[283]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateY" 
		"rigwRN.placeHolderList[284]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[285]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateX" 
		"rigwRN.placeHolderList[286]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateY" 
		"rigwRN.placeHolderList[287]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[288]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateX" 
		"rigwRN.placeHolderList[289]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateY" 
		"rigwRN.placeHolderList[290]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[291]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateX" 
		"rigwRN.placeHolderList[292]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateY" 
		"rigwRN.placeHolderList[293]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[294]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[295]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[296]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[297]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[298]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[299]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[300]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[301]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[302]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[303]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[304]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[305]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateX" 
		"rigwRN.placeHolderList[306]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateY" 
		"rigwRN.placeHolderList[307]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateZ" 
		"rigwRN.placeHolderList[308]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.follow" 
		"rigwRN.placeHolderList[309]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[310]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[311]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[312]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[313]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[314]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[315]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[316]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[317]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[318]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[319]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[320]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateX" 
		"rigwRN.placeHolderList[321]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateY" 
		"rigwRN.placeHolderList[322]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateZ" 
		"rigwRN.placeHolderList[323]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.follow" 
		"rigwRN.placeHolderList[324]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKIKBlend" 
		"rigwRN.placeHolderList[325]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.IKVis" 
		"rigwRN.placeHolderList[326]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKVis" 
		"rigwRN.placeHolderList[327]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKIKBlend" 
		"rigwRN.placeHolderList[328]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.IKVis" 
		"rigwRN.placeHolderList[329]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKVis" 
		"rigwRN.placeHolderList[330]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
	setAttr -s 3 ".opt";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -sn "imrVerbosity" -ln "imrVerbosity" -dv 3 -at "short";
	addAttr -ci true -sn "imrThreads" -ln "imrThreads" -dv 1 -at "short";
	addAttr -ci true -sn "imrThreadsAdjust" -ln "imrThreadsAdjust" -dv 1 -min 0 -max 
		1 -at "bool";
	addAttr -ci true -sn "imrTaskOrder" -ln "imrTaskOrder" -dv 1 -at "short";
	addAttr -ci true -sn "imrTaskSize" -ln "imrTaskSize" -at "short";
	addAttr -ci true -sn "imrTaskAdjust" -ln "imrTaskAdjust" -dv 1 -at "short";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
	setAttr ".imrThreads" 4;
	setAttr ".imrThreadsAdjust" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 6 -ast 1 -aet 6 ";
	setAttr ".st" 6;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel5_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel5_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel5_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel6_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel6_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel6_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:nurbsCircle1_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:nurbsCircle1_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1.8961321840000001 6 1.8961321840000001;
createNode animCurveTL -n "rigw:nurbsCircle1_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.25892283179999998 6 0.25892283179999998;
createNode animCurveTL -n "rigw:nurbsCircle1_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0092671947530000003 6 0.0092671947530000003;
createNode animCurveTA -n "rigw:nurbsCircle1_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 5.3743348480000001e-05 6 5.3743348480000001e-05;
createNode animCurveTA -n "rigw:nurbsCircle1_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.4012048789999999e-08 6 7.4012048789999999e-08;
createNode animCurveTA -n "rigw:nurbsCircle1_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -90 6 -90;
createNode animCurveTU -n "rigw:nurbsCircle1_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.0710576700000001 6 2.0710576700000001;
createNode animCurveTU -n "rigw:nurbsCircle1_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.0710576700000001 6 2.0710576700000001;
createNode animCurveTU -n "rigw:nurbsCircle1_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.0710576700000001 6 2.0710576700000001;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel5_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel5_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel5_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel6_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel6_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKWheel6_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:nurbsCircle2_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:nurbsCircle2_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1.8837424650000001 6 -1.8837424650000001;
createNode animCurveTL -n "rigw:nurbsCircle2_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.25892283179999998 6 -0.25892283179999998;
createNode animCurveTL -n "rigw:nurbsCircle2_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.0092671827969999995 6 -0.0092671827969999995;
createNode animCurveTA -n "rigw:nurbsCircle2_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 179.9999463 6 179.9999463;
createNode animCurveTA -n "rigw:nurbsCircle2_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.4012048789999999e-08 6 7.4012048789999999e-08;
createNode animCurveTA -n "rigw:nurbsCircle2_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 90 6 90;
createNode animCurveTU -n "rigw:nurbsCircle2_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.0710576700000001 6 2.0710576700000001;
createNode animCurveTU -n "rigw:nurbsCircle2_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.0710576700000001 6 2.0710576700000001;
createNode animCurveTU -n "rigw:nurbsCircle2_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.0710576700000001 6 2.0710576700000001;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -0.032723530145607034 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.30516318329999997 3 -0.33572500719692316
		 6 -0.30516318329999997;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -0.26423395504819935 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -6.5634538846649653 6 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 6.6092575406115328 6 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -3.9183830192500833 6 0;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -7.0171045651471626 6 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 6.593739124124105 6 0;
createNode animCurveTA -n "rigw:rig:FKBody_M_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -3.9445156207878522 6 0;
createNode animCurveTA -n "rigw:rig:FKWeapon_M_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  -3 0.70038422919999999 -1 -19.521830540101089
		 1 0.70038422919999999 2 0.70038422919999999 4 -19.521830540101089 6 0.70038422919999999
		 7 0.70038422919999999;
	setAttr -s 7 ".kit[2:6]"  2 3 3 2 3;
	setAttr -s 7 ".kot[2:6]"  2 3 3 2 3;
createNode animCurveTA -n "rigw:rig:FKWeapon_M_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 2 0 7 0;
createNode animCurveTA -n "rigw:rig:FKWeapon_M_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 2 0 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 10.540935 3 -12.264681158332396 6 10.540935;
createNode animCurveTU -n "rigw:rig:FKShoulder_R_Global";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 10 6 10;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 2 0 7 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 2 0 7 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  -3 6.2546662299999998 -1 -16.550949928332383
		 1 6.2546662299999998 2 6.2546662299999998 4 -16.550949928332383 6 6.2546662299999998
		 7 6.2546662299999998;
	setAttr -s 7 ".kit[2:6]"  2 3 3 2 3;
	setAttr -s 7 ".kot[2:6]"  2 3 3 2 3;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.9154412150000004 3 -20.108635079527609
		 6 4.9154412150000004;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.9154412150000004 3 -20.108635079527609
		 6 4.9154412150000004;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.9154412150000004 3 -3.8691703219345568
		 6 4.9154412150000004;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.9154412150000004 3 -3.8691703219345568
		 6 4.9154412150000004;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.9154412150000004 3 -3.8691703219345568
		 6 4.9154412150000004;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.9154412150000004 3 -3.8691703219345568
		 6 4.9154412150000004;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 6.2546662299999998 3 -16.550949928332383
		 6 6.2546662299999998;
createNode animCurveTU -n "rigw:rig:FKShoulder_L_Global";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 10 6 10;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 2 0 7 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 2 0 7 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  -3 -1.7055469249999999 -1 -2.6400138430538318
		 1 -1.7055469249999999 2 -1.7055469249999999 4 -2.6400138430538318 6 -1.7055469249999999
		 7 -1.7055469249999999;
	setAttr -s 7 ".kit[2:6]"  2 3 3 2 3;
	setAttr -s 7 ".kot[2:6]"  2 3 3 2 3;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.2390606610000003 3 -16.953731309664352
		 6 3.2390606610000003;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.3246627489999998 3 -24.51745471966435
		 6 -4.3246627489999998;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.2390606610000003 3 10.119938977423145
		 4 23.052408192112377 6 3.2390606610000003;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -4.3246627489999998 3 2.5562155674231417
		 4 15.488684782112381 6 -4.3246627489999998;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.2390606610000003 3 10.119938977423145
		 4 23.052408192112377 6 3.2390606610000003;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -4.3246627489999998 3 2.5562155674231417
		 4 15.488684782112381 6 -4.3246627489999998;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -0.029566175799830844 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 0.0051963185061781316 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -0.28018267006032899 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 1.0566306874463405 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 6.023823272915978 6 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_R_follow";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 10 6 10;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -0.033906193075516067 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -0.0060877775435072331 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -0.27967266243822597 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -1.2386251794070715 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 6.9540888779834322 6 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 1.9222559465525169 6 0;
	setAttr -s 3 ".kit[1:2]"  2 3;
	setAttr -s 3 ".kot[1:2]"  2 3;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_L_follow";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 10 6 10;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKIKBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 10 6 10;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKIKBlend";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 10 6 10;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "rigw:rig:Main_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:rig:Main_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTL -n "rigw:rig:Main_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTL -n "rigw:rig:Main_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:Main_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:Main_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:rig:Main_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:rig:Main_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2 6 2;
createNode animCurveTU -n "rigw:rig:Main_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2 6 2;
createNode animCurveTU -n "rigw:rig:Main_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2 6 2;
createNode animCurveTL -n "rigw:TrackL_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 16.706767680580423;
createNode animCurveTL -n "rigw:TrackL_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:TrackL_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:TrackR_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 8.9064141376358492;
createNode animCurveTL -n "rigw:TrackR_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:TrackR_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:TrackR_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:TrackR_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -4.6689097212968234 6 0;
createNode animCurveTA -n "rigw:TrackR_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:TrackR_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:TrackR_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:TrackR_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:TrackR_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:TrackL_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "rigw:TrackL_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 3 -4.6689097212968234 6 0;
createNode animCurveTA -n "rigw:TrackL_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:TrackL_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:TrackL_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:TrackL_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:TrackL_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode mia_exposure_photographic -n "mia_exposure_photographic1";
	setAttr ".S00" 2.0999999046325684;
	setAttr ".S02" 1600;
	setAttr ".S04" 0.80000001192092896;
	setAttr ".S06" 0.80000001192092896;
	setAttr ".S07" 0;
	setAttr ".S08" 1.1000000238418579;
	setAttr ".S09" 1.2000000476837158;
createNode animCurveTU -n "rigw:tracks:stuff93_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff93_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.9248102972057626 3 -3.9200737746553065
		 6 -3.9248102972057626;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99748563766479492;
	setAttr -s 3 ".koy[2]"  0.070869199931621552;
createNode animCurveTL -n "rigw:tracks:stuff93_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.5198204208337878 3 1.6858575072574282
		 6 1.5198204208337878;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.37260371446609497;
	setAttr -s 3 ".koy[2]"  0.92799055576324463;
createNode animCurveTL -n "rigw:tracks:stuff93_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 5.5044586659036074 3 5.1409552817173765
		 6 5.5044586659036074;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.18039166927337646;
	setAttr -s 3 ".koy[2]"  -0.98359477519989014;
createNode animCurveTA -n "rigw:tracks:stuff93_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 155.4457850056454 6 155.4457850056454;
createNode animCurveTA -n "rigw:tracks:stuff93_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.46022316200377744 6 0.46022316200377744;
createNode animCurveTA -n "rigw:tracks:stuff93_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 179.37319838705099 6 179.37319838705099;
createNode animCurveTU -n "rigw:tracks:stuff93_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff93_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff93_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff92_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff92_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.9125030062156698 3 -3.9077664836652137
		 6 -3.9125030062156698;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99748563766479492;
	setAttr -s 3 ".koy[2]"  0.070869199931621552;
createNode animCurveTL -n "rigw:tracks:stuff92_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.9512480591474912 3 2.1172851455711315
		 6 1.9512480591474912;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.37260371446609497;
	setAttr -s 3 ".koy[2]"  0.92799055576324463;
createNode animCurveTL -n "rigw:tracks:stuff92_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.5599383184532076 3 4.1964349342669767
		 6 4.5599383184532076;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.18039166927337646;
	setAttr -s 3 ".koy[2]"  -0.98359477519989014;
createNode animCurveTA -n "rigw:tracks:stuff92_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 155.44578500564543 6 155.44578500564543;
createNode animCurveTA -n "rigw:tracks:stuff92_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.46022316200377594 6 0.46022316200377594;
createNode animCurveTA -n "rigw:tracks:stuff92_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 179.37319838705096 6 179.37319838705096;
createNode animCurveTU -n "rigw:tracks:stuff92_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff92_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff92_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff91_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff91_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.9001957152255784 3 -3.8954591926751223
		 6 -3.9001957152255784;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99748563766479492;
	setAttr -s 3 ".koy[2]"  0.070869199931621552;
createNode animCurveTL -n "rigw:tracks:stuff91_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.3826756974611949 3 2.5487127838848349
		 6 2.3826756974611949;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.37260371446609497;
	setAttr -s 3 ".koy[2]"  0.92799055576324463;
createNode animCurveTL -n "rigw:tracks:stuff91_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.6154179710028074 3 3.251914586816576
		 6 3.6154179710028074;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.18039166927337646;
	setAttr -s 3 ".koy[2]"  -0.98359477519989014;
createNode animCurveTA -n "rigw:tracks:stuff91_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 155.44578500564543 6 155.44578500564543;
createNode animCurveTA -n "rigw:tracks:stuff91_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.46022316200377594 6 0.46022316200377594;
createNode animCurveTA -n "rigw:tracks:stuff91_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 179.37319838705096 6 179.37319838705096;
createNode animCurveTU -n "rigw:tracks:stuff91_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff91_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff91_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff85_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff85_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373498 6 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff85_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.6988127858634878 3 2.7640099807682148
		 6 2.6988127858634878;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.71494227647781372;
	setAttr -s 3 ".koy[2]"  0.69918346405029297;
createNode animCurveTL -n "rigw:tracks:stuff85_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.6828680972306351 3 2.2885651872675958
		 6 2.6828680972306351;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16670873761177063;
	setAttr -s 3 ".koy[2]"  -0.98600614070892334;
createNode animCurveTA -n "rigw:tracks:stuff85_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 170.61120588335984 6 170.61120588335984;
createNode animCurveTA -n "rigw:tracks:stuff85_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff85_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff85_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff85_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff85_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff86_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff86_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373489 6 -3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff86_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.7544668665867889 6 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff86_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.6983091210659895 3 1.2986524446188603
		 6 1.6983091210659895;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  -0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff86_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff86_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff86_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff86_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff86_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff86_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff87_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff87_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373489 6 -3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff87_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.7544668665867889 6 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff87_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.67019983678636175 3 0.27054316033923226
		 6 0.67019983678636175;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  -0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff87_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff87_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff87_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff87_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff87_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff87_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff88_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff88_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373498 6 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff88_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.7544668665867889 6 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff88_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.35790944749326581 3 -0.75756612394039535
		 6 -0.35790944749326581;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  -0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff88_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff88_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff88_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff88_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff88_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff88_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff89_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff89_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373502 6 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff89_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.7544668665867889 6 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff89_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.3860187317728931 3 -1.7856754082200224
		 6 -1.3860187317728931;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  -0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff89_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff89_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff89_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff89_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff89_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff89_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff90_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff90_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373502 6 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff90_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.7544668665867889 6 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff90_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -2.4141280160525209 3 -2.813784692499651
		 6 -2.4141280160525209;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  -0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff90_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff90_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff90_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff90_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff90_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff90_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff95_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff95_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373502 6 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff95_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.6303919141490808 3 2.5301702255526664
		 6 2.6303919141490808;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.55384981632232666;
	setAttr -s 3 ".koy[2]"  -0.83261650800704956;
createNode animCurveTL -n "rigw:tracks:stuff95_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.4985456653335549 3 -3.8854320467992882
		 6 -3.4985456653335549;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16981320083141327;
	setAttr -s 3 ".koy[2]"  -0.98547625541687012;
createNode animCurveTA -n "rigw:tracks:stuff95_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 14.523049474597975 6 14.523049474597975;
createNode animCurveTA -n "rigw:tracks:stuff95_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff95_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff95_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff95_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff95_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff96_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff96_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373502 6 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff96_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.3224732280064515 3 2.1604308952783295
		 6 2.3224732280064515;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.38047334551811218;
	setAttr -s 3 ".koy[2]"  -0.92479187250137329;
createNode animCurveTL -n "rigw:tracks:stuff96_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.4542731959710018 3 -4.8196055671520854
		 6 -4.4542731959710018;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.17951774597167969;
	setAttr -s 3 ".koy[2]"  -0.98375469446182251;
createNode animCurveTA -n "rigw:tracks:stuff96_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 23.919569992035171 6 23.919569992035171;
createNode animCurveTA -n "rigw:tracks:stuff96_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff96_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff96_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff96_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff96_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff97_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff97_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373502 6 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff97_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.8872206549298032 3 1.7251783222016814
		 6 1.8872206549298032;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.38047334551811218;
	setAttr -s 3 ".koy[2]"  -0.92479187250137329;
createNode animCurveTL -n "rigw:tracks:stuff97_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -5.4355714277827456 3 -5.8009037989638292
		 6 -5.4355714277827456;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.17951774597167969;
	setAttr -s 3 ".koy[2]"  -0.98375469446182251;
createNode animCurveTA -n "rigw:tracks:stuff97_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 23.919569992035171 6 23.919569992035171;
createNode animCurveTA -n "rigw:tracks:stuff97_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff97_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff97_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff97_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff97_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff98_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff98_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373498 6 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff98_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.4519680818531553 3 1.1770318929435577
		 6 1.4519680818531553;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.38047334551811218;
	setAttr -s 3 ".koy[2]"  -0.92479187250137329;
createNode animCurveTL -n "rigw:tracks:stuff98_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -6.4168696595944894 3 -6.680647684229144
		 6 -6.4168696595944894;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.17951774597167969;
	setAttr -s 3 ".koy[2]"  -0.98375469446182251;
createNode animCurveTA -n "rigw:tracks:stuff98_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 23.919569992035171 3 41.973154756083154
		 6 23.919569992035171;
createNode animCurveTA -n "rigw:tracks:stuff98_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff98_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff98_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff98_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff98_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.99999999999999989 6 -0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff111_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff111_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373529 6 -3.8895896675373529;
createNode animCurveTL -n "rigw:tracks:stuff111_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.78689185839297315 3 0.38726809542670487
		 6 0.78689185839297315;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.1645495593547821;
	setAttr -s 3 ".koy[2]"  -0.98636877536773682;
createNode animCurveTL -n "rigw:tracks:stuff111_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -6.8744340999844455 3 -6.8693050550981765
		 6 -6.8744340999844455;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99705344438552856;
	setAttr -s 3 ".koy[2]"  0.076708987355232239;
createNode animCurveTA -n "rigw:tracks:stuff111_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -89.264667126223671 3 -77.312584435308437
		 6 -89.264667126223671;
createNode animCurveTA -n "rigw:tracks:stuff111_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff111_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff111_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff111_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff111_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff110_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff110_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373529 6 -3.8895896675373529;
createNode animCurveTL -n "rigw:tracks:stuff110_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.077125482214109045 3 -0.042819806078586965
		 6 0.077125482214109045;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.32735100388526917;
	setAttr -s 3 ".koy[2]"  -0.94490283727645874;
createNode animCurveTL -n "rigw:tracks:stuff110_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -6.4850333431961644 3 -6.1230639850466568
		 6 -6.4850333431961644;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.18696889281272888;
	setAttr -s 3 ".koy[2]"  0.98236584663391113;
createNode animCurveTA -n "rigw:tracks:stuff110_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -28.783331426910969 3 -9.1621841465676575
		 6 -28.783331426910969;
createNode animCurveTA -n "rigw:tracks:stuff110_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff110_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff110_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff110_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1.0000000000000002 6 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff110_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1.0000000000000002 6 -1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff109_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff109_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.889589667537352 6 -3.889589667537352;
createNode animCurveTL -n "rigw:tracks:stuff109_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590271651 6 0.0036092918590271651;
createNode animCurveTL -n "rigw:tracks:stuff109_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -5.4488368443595645 3 -5.0491801679124357
		 6 -5.4488368443595645;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff109_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff109_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff109_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff109_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff109_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff109_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff108_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff108_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.889589667537352 6 -3.889589667537352;
createNode animCurveTL -n "rigw:tracks:stuff108_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590272896 6 0.0036092918590272896;
createNode animCurveTL -n "rigw:tracks:stuff108_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.4294452888197569 3 -4.0297886123726281
		 6 -4.4294452888197569;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff108_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff108_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff108_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff108_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff108_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff108_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff107_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff107_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.889589667537352 6 -3.889589667537352;
createNode animCurveTL -n "rigw:tracks:stuff107_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.003609291859027414 6 0.003609291859027414;
createNode animCurveTL -n "rigw:tracks:stuff107_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.4100537332799488 3 -3.0103970568328187
		 6 -3.4100537332799488;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff107_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff107_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff107_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff107_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff107_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff107_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff106_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff106_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373502 6 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff106_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590275385 6 0.0036092918590275385;
createNode animCurveTL -n "rigw:tracks:stuff106_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -2.3906621777401411 3 -1.991005501293011
		 6 -2.3906621777401411;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff106_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff106_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff106_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff106_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff106_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff106_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff105_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff105_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373511 6 -3.8895896675373511;
createNode animCurveTL -n "rigw:tracks:stuff105_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.003609291859027663 6 0.003609291859027663;
createNode animCurveTL -n "rigw:tracks:stuff105_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.3712706222003337 3 -0.97161394575320437
		 6 -1.3712706222003337;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff105_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff105_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff105_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff105_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff105_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff105_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff104_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff104_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373502 6 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff104_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590277874 6 0.0036092918590277874;
createNode animCurveTL -n "rigw:tracks:stuff104_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.35187906666052582 3 0.047777609786603656
		 6 -0.35187906666052582;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff104_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff104_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff104_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff104_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff104_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff104_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff103_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff103_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373502 6 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff103_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590279119 6 0.0036092918590279119;
createNode animCurveTL -n "rigw:tracks:stuff103_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.66751248887928183 3 1.0671691653264113
		 6 0.66751248887928183;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff103_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff103_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff103_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff103_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff103_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff103_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff102_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff102_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373498 6 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff102_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590280364 6 0.0036092918590280364;
createNode animCurveTL -n "rigw:tracks:stuff102_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.6869040444190897 3 2.0865607208662191
		 6 1.6869040444190897;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff102_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff102_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff102_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff102_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff102_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff102_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff101_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff101_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373498 6 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff101_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590281608 6 0.0036092918590281608;
createNode animCurveTL -n "rigw:tracks:stuff101_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.7062955999588971 3 3.1059522764060272
		 6 2.7062955999588971;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff101_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff101_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff101_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff101_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff101_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff101_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff100_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff100_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373498 6 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff100_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590282853 6 0.0036092918590282853;
createNode animCurveTL -n "rigw:tracks:stuff100_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.7256871554987057 3 4.1253438319458358
		 6 3.7256871554987057;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff100_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff100_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff100_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff100_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff100_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff100_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff99_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff99_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373498 6 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff99_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590284093 6 0.0036092918590284093;
createNode animCurveTL -n "rigw:tracks:stuff99_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.745078711038512 3 5.1447353874856407
		 6 4.745078711038512;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff99_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff99_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff99_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff99_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff99_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff99_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 6 -1;
createNode animCurveTU -n "rigw:tracks:stuff112_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff112_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -3.8895896675373498 6 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff112_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.11714647165146076 3 0.29029830831977482
		 6 0.11714647165146076;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.52946746349334717;
	setAttr -s 3 ".koy[2]"  0.84833019971847534;
createNode animCurveTL -n "rigw:tracks:stuff112_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 5.7205885340857714 3 6.0465859011821772
		 6 5.7205885340857714;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.17057029902935028;
	setAttr -s 3 ".koy[2]"  0.98534548282623291;
createNode animCurveTA -n "rigw:tracks:stuff112_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 15.501773238377783 3 41.708265960795046
		 6 15.501773238377783;
createNode animCurveTA -n "rigw:tracks:stuff112_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 7.016709298534876e-15 6 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff112_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTU -n "rigw:tracks:stuff112_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff112_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1.0000000000000002 6 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff112_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1.0000000000000002 6 -1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff51_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff51_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.9365533838983895 3 3.931922869587384
		 6 3.9365533838983895;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99812239408493042;
	setAttr -s 3 ".koy[2]"  -0.061250455677509308;
createNode animCurveTL -n "rigw:tracks:stuff51_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.8362001562078627 3 1.1833019081988372
		 6 0.8362001562078627;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16509711742401123;
	setAttr -s 3 ".koy[2]"  0.98627728223800659;
createNode animCurveTL -n "rigw:tracks:stuff51_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 6.0352838681597616 3 5.9315680401807995
		 6 6.0352838681597616;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.89560043811798096;
	setAttr -s 3 ".koy[2]"  0.44485929608345032;
createNode animCurveTA -n "rigw:tracks:stuff51_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 94.75296763901126 3 69.503555186609589
		 6 94.75296763901126;
createNode animCurveTA -n "rigw:tracks:stuff51_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.46022316200377128 6 0.46022316200377128;
createNode animCurveTA -n "rigw:tracks:stuff51_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.62680161294900905 6 0.62680161294900905;
createNode animCurveTU -n "rigw:tracks:stuff51_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff51_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff51_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1.0000000000000002 6 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff50_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff50_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.9248102972057617 3 3.9200737746553056
		 6 3.9248102972057617;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99748563766479492;
	setAttr -s 3 ".koy[2]"  -0.070869199931621552;
createNode animCurveTL -n "rigw:tracks:stuff50_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.5198204208337878 3 1.6858575072574282
		 6 1.5198204208337878;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.37260371446609497;
	setAttr -s 3 ".koy[2]"  0.92799055576324463;
createNode animCurveTL -n "rigw:tracks:stuff50_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 5.5044586659036083 3 5.1409552817173774
		 6 5.5044586659036083;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.18039166927337646;
	setAttr -s 3 ".koy[2]"  -0.98359477519989014;
createNode animCurveTA -n "rigw:tracks:stuff50_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 24.554214994354588 6 24.554214994354588;
createNode animCurveTA -n "rigw:tracks:stuff50_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.46022316200377039 6 0.46022316200377039;
createNode animCurveTA -n "rigw:tracks:stuff50_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.62680161294901959 6 0.62680161294901959;
createNode animCurveTU -n "rigw:tracks:stuff50_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff50_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff50_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff49_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff49_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.9125030062156698 3 3.9077664836652137
		 6 3.9125030062156698;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99748563766479492;
	setAttr -s 3 ".koy[2]"  -0.070869199931621552;
createNode animCurveTL -n "rigw:tracks:stuff49_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.9512480591474912 3 2.1172851455711315
		 6 1.9512480591474912;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.37260371446609497;
	setAttr -s 3 ".koy[2]"  0.92799055576324463;
createNode animCurveTL -n "rigw:tracks:stuff49_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.5599383184532085 3 4.1964349342669776
		 6 4.5599383184532085;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.18039166927337646;
	setAttr -s 3 ".koy[2]"  -0.98359477519989014;
createNode animCurveTA -n "rigw:tracks:stuff49_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 24.554214994354584 6 24.554214994354584;
createNode animCurveTA -n "rigw:tracks:stuff49_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.46022316200376889 6 0.46022316200376889;
createNode animCurveTA -n "rigw:tracks:stuff49_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.62680161294903081 6 0.62680161294903081;
createNode animCurveTU -n "rigw:tracks:stuff49_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff49_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff49_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff48_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff48_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.900195715225578 3 3.8954591926751219
		 6 3.900195715225578;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99748563766479492;
	setAttr -s 3 ".koy[2]"  -0.070869199931621552;
createNode animCurveTL -n "rigw:tracks:stuff48_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.3826756974611949 3 2.5487127838848349
		 6 2.3826756974611949;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.37260371446609497;
	setAttr -s 3 ".koy[2]"  0.92799055576324463;
createNode animCurveTL -n "rigw:tracks:stuff48_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.6154179710028078 3 3.2519145868165764
		 6 3.6154179710028078;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.18039166927337646;
	setAttr -s 3 ".koy[2]"  -0.98359477519989014;
createNode animCurveTA -n "rigw:tracks:stuff48_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 24.554214994354584 6 24.554214994354584;
createNode animCurveTA -n "rigw:tracks:stuff48_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.46022316200376889 6 0.46022316200376889;
createNode animCurveTA -n "rigw:tracks:stuff48_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.62680161294903081 6 0.62680161294903081;
createNode animCurveTU -n "rigw:tracks:stuff48_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff48_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff48_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff42_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff42_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373489 6 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff42_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.6988127858634878 3 2.7640099807682148
		 6 2.6988127858634878;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.71494227647781372;
	setAttr -s 3 ".koy[2]"  0.69918346405029297;
createNode animCurveTL -n "rigw:tracks:stuff42_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.6828680972306356 3 2.2885651872675963
		 6 2.6828680972306356;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16670873761177063;
	setAttr -s 3 ".koy[2]"  -0.98600614070892334;
createNode animCurveTA -n "rigw:tracks:stuff42_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 9.3887941166401472 6 9.3887941166401472;
createNode animCurveTA -n "rigw:tracks:stuff42_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff42_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff42_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff42_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff42_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff43_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff43_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373489 6 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff43_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.7544668665867889 6 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff43_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.69830912106599 3 1.2986524446188608
		 6 1.69830912106599;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  -0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff43_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff43_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff43_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff43_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff43_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff43_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff44_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff44_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373489 6 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff44_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.7544668665867889 6 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff44_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.67019983678636219 3 0.27054316033923276
		 6 0.67019983678636219;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  -0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff44_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff44_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff44_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff44_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff44_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff44_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff45_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff45_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373498 6 3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff45_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.7544668665867889 6 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff45_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.35790944749326536 3 -0.7575661239403948
		 6 -0.35790944749326536;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  -0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff45_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff45_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff45_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff45_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff45_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff45_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff46_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff46_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373502 6 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff46_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.7544668665867889 6 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff46_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.3860187317728927 3 -1.7856754082200219
		 6 -1.3860187317728927;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  -0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff46_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff46_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff46_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff46_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff46_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff46_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff47_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff47_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373502 6 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff47_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.7544668665867889 6 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff47_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -2.4141280160525205 3 -2.8137846924996506
		 6 -2.4141280160525205;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  -0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff47_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff47_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff47_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff47_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff47_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff47_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff52_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff52_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373502 6 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff52_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.6303919141490808 3 2.5301702255526664
		 6 2.6303919141490808;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.55384981632232666;
	setAttr -s 3 ".koy[2]"  -0.83261650800704956;
createNode animCurveTL -n "rigw:tracks:stuff52_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.4985456653335545 3 -3.8854320467992878
		 6 -3.4985456653335545;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16981320083141327;
	setAttr -s 3 ".koy[2]"  -0.98547625541687012;
createNode animCurveTA -n "rigw:tracks:stuff52_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -14.523049474597972 6 -14.523049474597972;
createNode animCurveTA -n "rigw:tracks:stuff52_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff52_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff52_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff52_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff52_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff53_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff53_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373502 6 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff53_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.3224732280064515 3 2.1604308952783295
		 6 2.3224732280064515;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.38047334551811218;
	setAttr -s 3 ".koy[2]"  -0.92479187250137329;
createNode animCurveTL -n "rigw:tracks:stuff53_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.4542731959710009 3 -4.8196055671520837
		 6 -4.4542731959710009;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.17951774597167969;
	setAttr -s 3 ".koy[2]"  -0.98375469446182251;
createNode animCurveTA -n "rigw:tracks:stuff53_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -23.919569992035186 6 -23.919569992035186;
createNode animCurveTA -n "rigw:tracks:stuff53_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff53_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff53_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff53_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff53_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff54_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff54_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373502 6 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff54_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.8872206549298032 3 1.7251783222016812
		 6 1.8872206549298032;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.38047334551811218;
	setAttr -s 3 ".koy[2]"  -0.92479187250137329;
createNode animCurveTL -n "rigw:tracks:stuff54_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -5.4355714277827447 3 -5.8009037989638275
		 6 -5.4355714277827447;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.17951774597167969;
	setAttr -s 3 ".koy[2]"  -0.98375469446182251;
createNode animCurveTA -n "rigw:tracks:stuff54_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -23.919569992035186 6 -23.919569992035186;
createNode animCurveTA -n "rigw:tracks:stuff54_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff54_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff54_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff54_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff54_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff55_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff55_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373502 6 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff55_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.4519680818531553 3 1.2899257491250333
		 6 1.4519680818531553;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.38047334551811218;
	setAttr -s 3 ".koy[2]"  -0.92479187250137329;
createNode animCurveTL -n "rigw:tracks:stuff55_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -6.4168696595944885 3 -6.7822020307755713
		 6 -6.4168696595944885;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.17951774597167969;
	setAttr -s 3 ".koy[2]"  -0.98375469446182251;
createNode animCurveTA -n "rigw:tracks:stuff55_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -23.919569992035186 3 -42.69171098748356
		 6 -23.919569992035186;
createNode animCurveTA -n "rigw:tracks:stuff55_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff55_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff55_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff55_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff55_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff68_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff68_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373538 6 3.8895896675373538;
createNode animCurveTL -n "rigw:tracks:stuff68_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.78689185839297315 3 0.38726809542670498
		 6 0.78689185839297315;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.1645495593547821;
	setAttr -s 3 ".koy[2]"  -0.98636877536773682;
createNode animCurveTL -n "rigw:tracks:stuff68_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -6.8744340999844447 3 -6.8693050550981756
		 6 -6.8744340999844447;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99705344438552856;
	setAttr -s 3 ".koy[2]"  0.076708987355232239;
createNode animCurveTA -n "rigw:tracks:stuff68_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -90.735332873776329 3 -101.43128609651859
		 6 -90.735332873776329;
createNode animCurveTA -n "rigw:tracks:stuff68_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff68_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff68_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff68_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff68_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff67_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff67_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373538 6 3.8895896675373538;
createNode animCurveTL -n "rigw:tracks:stuff67_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.077125482214109045 3 -0.018105308194305416
		 6 0.077125482214109045;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.32735100388526917;
	setAttr -s 3 ".koy[2]"  -0.94490283727645874;
createNode animCurveTL -n "rigw:tracks:stuff67_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -6.4850333431961635 3 -6.1067410344186861
		 6 -6.4850333431961635;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.18696889281272888;
	setAttr -s 3 ".koy[2]"  0.98236584663391113;
createNode animCurveTA -n "rigw:tracks:stuff67_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -151.21666857308904 3 -163.92275763992058
		 6 -151.21666857308904;
createNode animCurveTA -n "rigw:tracks:stuff67_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff67_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff67_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff67_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1.0000000000000002 6 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff67_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1.0000000000000002 6 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff66_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff66_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373529 6 3.8895896675373529;
createNode animCurveTL -n "rigw:tracks:stuff66_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590271651 6 0.0036092918590271651;
createNode animCurveTL -n "rigw:tracks:stuff66_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -5.4488368443595636 3 -5.0491801679124357
		 6 -5.4488368443595636;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff66_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff66_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff66_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff66_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff66_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff66_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff65_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff65_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.889589667537352 6 3.889589667537352;
createNode animCurveTL -n "rigw:tracks:stuff65_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590272896 6 0.0036092918590272896;
createNode animCurveTL -n "rigw:tracks:stuff65_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -4.429445288819756 3 -4.0297886123726281
		 6 -4.429445288819756;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff65_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff65_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff65_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff65_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff65_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff65_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff64_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff64_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.889589667537352 6 3.889589667537352;
createNode animCurveTL -n "rigw:tracks:stuff64_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.003609291859027414 6 0.003609291859027414;
createNode animCurveTL -n "rigw:tracks:stuff64_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.4100537332799483 3 -3.0103970568328182
		 6 -3.4100537332799483;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff64_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff64_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff64_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff64_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff64_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff64_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff63_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff63_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373511 6 3.8895896675373511;
createNode animCurveTL -n "rigw:tracks:stuff63_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590275385 6 0.0036092918590275385;
createNode animCurveTL -n "rigw:tracks:stuff63_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -2.3906621777401407 3 -1.9910055012930106
		 6 -2.3906621777401407;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff63_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff63_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff63_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff63_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff63_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff63_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff62_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff62_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373511 6 3.8895896675373511;
createNode animCurveTL -n "rigw:tracks:stuff62_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.003609291859027663 6 0.003609291859027663;
createNode animCurveTL -n "rigw:tracks:stuff62_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -1.371270622200333 3 -0.97161394575320348
		 6 -1.371270622200333;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff62_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff62_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff62_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff62_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff62_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff62_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff61_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff61_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373502 6 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff61_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590277874 6 0.0036092918590277874;
createNode animCurveTL -n "rigw:tracks:stuff61_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.35187906666052537 3 0.047777609786604128
		 6 -0.35187906666052537;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff61_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff61_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff61_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff61_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff61_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff61_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff60_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff60_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373502 6 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff60_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590279119 6 0.0036092918590279119;
createNode animCurveTL -n "rigw:tracks:stuff60_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.66751248887928227 3 1.0671691653264117
		 6 0.66751248887928227;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff60_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff60_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff60_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff60_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff60_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff60_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff59_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff59_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373498 6 3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff59_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590280364 6 0.0036092918590280364;
createNode animCurveTL -n "rigw:tracks:stuff59_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1.6869040444190899 3 2.0865607208662196
		 6 1.6869040444190899;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff59_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff59_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff59_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff59_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff59_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff59_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff58_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff58_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373489 6 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff58_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590281608 6 0.0036092918590281608;
createNode animCurveTL -n "rigw:tracks:stuff58_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.7062955999588976 3 3.1059522764060277
		 6 2.7062955999588976;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff58_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff58_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff58_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff58_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff58_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff58_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff57_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff57_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373489 6 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff57_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590282853 6 0.0036092918590282853;
createNode animCurveTL -n "rigw:tracks:stuff57_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.7256871554987057 3 4.1253438319458349
		 6 3.7256871554987057;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff57_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff57_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff57_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff57_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff57_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff57_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff56_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff56_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373489 6 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff56_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.0036092918590284093 6 0.0036092918590284093;
createNode animCurveTL -n "rigw:tracks:stuff56_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 4.7450787110385129 3 5.1447353874856407
		 6 4.7450787110385129;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16453638672828674;
	setAttr -s 3 ".koy[2]"  0.98637104034423828;
createNode animCurveTA -n "rigw:tracks:stuff56_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 180 6 180;
createNode animCurveTA -n "rigw:tracks:stuff56_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff56_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff56_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff56_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff56_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff69_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff69_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3.8895896675373489 6 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff69_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.11714647165146076 3 0.33805176939402176
		 6 0.11714647165146076;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.52946746349334717;
	setAttr -s 3 ".koy[2]"  0.84833019971847534;
createNode animCurveTL -n "rigw:tracks:stuff69_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 5.7205885340857723 3 6.0162715977565098
		 6 5.7205885340857723;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.17057029902935028;
	setAttr -s 3 ".koy[2]"  0.98534548282623291;
createNode animCurveTA -n "rigw:tracks:stuff69_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 164.49822676162222 3 141.90710308929582
		 6 164.49822676162222;
createNode animCurveTA -n "rigw:tracks:stuff69_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTA -n "rigw:tracks:stuff69_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 6 0;
createNode animCurveTU -n "rigw:tracks:stuff69_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff69_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1.0000000000000002 6 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff69_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1.0000000000000002 6 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff94_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "rigw:tracks:stuff94_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.9365533838983904 3 -3.9321824966283843
		 6 -3.9365533838983904;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.99812239408493042;
	setAttr -s 3 ".koy[2]"  0.061250455677509308;
createNode animCurveTL -n "rigw:tracks:stuff94_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0.8362001562078627 3 1.2024006663527662
		 6 0.8362001562078627;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.16509711742401123;
	setAttr -s 3 ".koy[2]"  0.98627728223800659;
createNode animCurveTL -n "rigw:tracks:stuff94_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 6.0352838681597607 3 5.9898983504818784
		 6 6.0352838681597607;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  0.89560043811798096;
	setAttr -s 3 ".koy[2]"  0.44485929608345032;
createNode animCurveTA -n "rigw:tracks:stuff94_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 85.247032360988754 3 112.212303854872
		 6 85.247032360988754;
createNode animCurveTA -n "rigw:tracks:stuff94_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.46022316200377822 6 0.46022316200377822;
createNode animCurveTA -n "rigw:tracks:stuff94_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 179.37319838705099 6 179.37319838705099;
createNode animCurveTU -n "rigw:tracks:stuff94_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 6 1;
createNode animCurveTU -n "rigw:tracks:stuff94_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.99999999999999989 6 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff94_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1.0000000000000002 6 -1.0000000000000002;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 17 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -s 30 ".gn";
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 24 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 37 ".tx";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 48 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren" -type "string" "mentalRay";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w";
	setAttr -av ".h";
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
connectAttr "rigw:tracks:stuff51_visibility.o" "rigwRN.phl[331]";
connectAttr "rigw:tracks:stuff51_translateX.o" "rigwRN.phl[332]";
connectAttr "rigw:tracks:stuff51_translateY.o" "rigwRN.phl[333]";
connectAttr "rigw:tracks:stuff51_translateZ.o" "rigwRN.phl[334]";
connectAttr "rigw:tracks:stuff51_rotateX.o" "rigwRN.phl[335]";
connectAttr "rigw:tracks:stuff51_rotateY.o" "rigwRN.phl[336]";
connectAttr "rigw:tracks:stuff51_rotateZ.o" "rigwRN.phl[337]";
connectAttr "rigw:tracks:stuff51_scaleX.o" "rigwRN.phl[338]";
connectAttr "rigw:tracks:stuff51_scaleY.o" "rigwRN.phl[339]";
connectAttr "rigw:tracks:stuff51_scaleZ.o" "rigwRN.phl[340]";
connectAttr "rigw:tracks:stuff50_visibility.o" "rigwRN.phl[341]";
connectAttr "rigw:tracks:stuff50_translateX.o" "rigwRN.phl[342]";
connectAttr "rigw:tracks:stuff50_translateY.o" "rigwRN.phl[343]";
connectAttr "rigw:tracks:stuff50_translateZ.o" "rigwRN.phl[344]";
connectAttr "rigw:tracks:stuff50_rotateX.o" "rigwRN.phl[345]";
connectAttr "rigw:tracks:stuff50_rotateY.o" "rigwRN.phl[346]";
connectAttr "rigw:tracks:stuff50_rotateZ.o" "rigwRN.phl[347]";
connectAttr "rigw:tracks:stuff50_scaleX.o" "rigwRN.phl[348]";
connectAttr "rigw:tracks:stuff50_scaleY.o" "rigwRN.phl[349]";
connectAttr "rigw:tracks:stuff50_scaleZ.o" "rigwRN.phl[350]";
connectAttr "rigw:tracks:stuff49_visibility.o" "rigwRN.phl[351]";
connectAttr "rigw:tracks:stuff49_translateX.o" "rigwRN.phl[352]";
connectAttr "rigw:tracks:stuff49_translateY.o" "rigwRN.phl[353]";
connectAttr "rigw:tracks:stuff49_translateZ.o" "rigwRN.phl[354]";
connectAttr "rigw:tracks:stuff49_rotateX.o" "rigwRN.phl[355]";
connectAttr "rigw:tracks:stuff49_rotateY.o" "rigwRN.phl[356]";
connectAttr "rigw:tracks:stuff49_rotateZ.o" "rigwRN.phl[357]";
connectAttr "rigw:tracks:stuff49_scaleX.o" "rigwRN.phl[358]";
connectAttr "rigw:tracks:stuff49_scaleY.o" "rigwRN.phl[359]";
connectAttr "rigw:tracks:stuff49_scaleZ.o" "rigwRN.phl[360]";
connectAttr "rigw:tracks:stuff48_visibility.o" "rigwRN.phl[361]";
connectAttr "rigw:tracks:stuff48_translateX.o" "rigwRN.phl[362]";
connectAttr "rigw:tracks:stuff48_translateY.o" "rigwRN.phl[363]";
connectAttr "rigw:tracks:stuff48_translateZ.o" "rigwRN.phl[364]";
connectAttr "rigw:tracks:stuff48_rotateX.o" "rigwRN.phl[365]";
connectAttr "rigw:tracks:stuff48_rotateY.o" "rigwRN.phl[366]";
connectAttr "rigw:tracks:stuff48_rotateZ.o" "rigwRN.phl[367]";
connectAttr "rigw:tracks:stuff48_scaleX.o" "rigwRN.phl[368]";
connectAttr "rigw:tracks:stuff48_scaleY.o" "rigwRN.phl[369]";
connectAttr "rigw:tracks:stuff48_scaleZ.o" "rigwRN.phl[370]";
connectAttr "rigw:tracks:stuff42_visibility.o" "rigwRN.phl[371]";
connectAttr "rigw:tracks:stuff42_translateX.o" "rigwRN.phl[372]";
connectAttr "rigw:tracks:stuff42_translateY.o" "rigwRN.phl[373]";
connectAttr "rigw:tracks:stuff42_translateZ.o" "rigwRN.phl[374]";
connectAttr "rigw:tracks:stuff42_rotateX.o" "rigwRN.phl[375]";
connectAttr "rigw:tracks:stuff42_rotateY.o" "rigwRN.phl[376]";
connectAttr "rigw:tracks:stuff42_rotateZ.o" "rigwRN.phl[377]";
connectAttr "rigw:tracks:stuff42_scaleX.o" "rigwRN.phl[378]";
connectAttr "rigw:tracks:stuff42_scaleY.o" "rigwRN.phl[379]";
connectAttr "rigw:tracks:stuff42_scaleZ.o" "rigwRN.phl[380]";
connectAttr "rigw:tracks:stuff43_visibility.o" "rigwRN.phl[381]";
connectAttr "rigw:tracks:stuff43_translateX.o" "rigwRN.phl[382]";
connectAttr "rigw:tracks:stuff43_translateY.o" "rigwRN.phl[383]";
connectAttr "rigw:tracks:stuff43_translateZ.o" "rigwRN.phl[384]";
connectAttr "rigw:tracks:stuff43_rotateX.o" "rigwRN.phl[385]";
connectAttr "rigw:tracks:stuff43_rotateY.o" "rigwRN.phl[386]";
connectAttr "rigw:tracks:stuff43_rotateZ.o" "rigwRN.phl[387]";
connectAttr "rigw:tracks:stuff43_scaleX.o" "rigwRN.phl[388]";
connectAttr "rigw:tracks:stuff43_scaleY.o" "rigwRN.phl[389]";
connectAttr "rigw:tracks:stuff43_scaleZ.o" "rigwRN.phl[390]";
connectAttr "rigw:tracks:stuff44_visibility.o" "rigwRN.phl[391]";
connectAttr "rigw:tracks:stuff44_translateX.o" "rigwRN.phl[392]";
connectAttr "rigw:tracks:stuff44_translateY.o" "rigwRN.phl[393]";
connectAttr "rigw:tracks:stuff44_translateZ.o" "rigwRN.phl[394]";
connectAttr "rigw:tracks:stuff44_rotateX.o" "rigwRN.phl[395]";
connectAttr "rigw:tracks:stuff44_rotateY.o" "rigwRN.phl[396]";
connectAttr "rigw:tracks:stuff44_rotateZ.o" "rigwRN.phl[397]";
connectAttr "rigw:tracks:stuff44_scaleX.o" "rigwRN.phl[398]";
connectAttr "rigw:tracks:stuff44_scaleY.o" "rigwRN.phl[399]";
connectAttr "rigw:tracks:stuff44_scaleZ.o" "rigwRN.phl[400]";
connectAttr "rigw:tracks:stuff45_visibility.o" "rigwRN.phl[401]";
connectAttr "rigw:tracks:stuff45_translateX.o" "rigwRN.phl[402]";
connectAttr "rigw:tracks:stuff45_translateY.o" "rigwRN.phl[403]";
connectAttr "rigw:tracks:stuff45_translateZ.o" "rigwRN.phl[404]";
connectAttr "rigw:tracks:stuff45_rotateX.o" "rigwRN.phl[405]";
connectAttr "rigw:tracks:stuff45_rotateY.o" "rigwRN.phl[406]";
connectAttr "rigw:tracks:stuff45_rotateZ.o" "rigwRN.phl[407]";
connectAttr "rigw:tracks:stuff45_scaleX.o" "rigwRN.phl[408]";
connectAttr "rigw:tracks:stuff45_scaleY.o" "rigwRN.phl[409]";
connectAttr "rigw:tracks:stuff45_scaleZ.o" "rigwRN.phl[410]";
connectAttr "rigw:tracks:stuff46_visibility.o" "rigwRN.phl[411]";
connectAttr "rigw:tracks:stuff46_translateX.o" "rigwRN.phl[412]";
connectAttr "rigw:tracks:stuff46_translateY.o" "rigwRN.phl[413]";
connectAttr "rigw:tracks:stuff46_translateZ.o" "rigwRN.phl[414]";
connectAttr "rigw:tracks:stuff46_rotateX.o" "rigwRN.phl[415]";
connectAttr "rigw:tracks:stuff46_rotateY.o" "rigwRN.phl[416]";
connectAttr "rigw:tracks:stuff46_rotateZ.o" "rigwRN.phl[417]";
connectAttr "rigw:tracks:stuff46_scaleX.o" "rigwRN.phl[418]";
connectAttr "rigw:tracks:stuff46_scaleY.o" "rigwRN.phl[419]";
connectAttr "rigw:tracks:stuff46_scaleZ.o" "rigwRN.phl[420]";
connectAttr "rigw:tracks:stuff47_visibility.o" "rigwRN.phl[421]";
connectAttr "rigw:tracks:stuff47_translateX.o" "rigwRN.phl[422]";
connectAttr "rigw:tracks:stuff47_translateY.o" "rigwRN.phl[423]";
connectAttr "rigw:tracks:stuff47_translateZ.o" "rigwRN.phl[424]";
connectAttr "rigw:tracks:stuff47_rotateX.o" "rigwRN.phl[425]";
connectAttr "rigw:tracks:stuff47_rotateY.o" "rigwRN.phl[426]";
connectAttr "rigw:tracks:stuff47_rotateZ.o" "rigwRN.phl[427]";
connectAttr "rigw:tracks:stuff47_scaleX.o" "rigwRN.phl[428]";
connectAttr "rigw:tracks:stuff47_scaleY.o" "rigwRN.phl[429]";
connectAttr "rigw:tracks:stuff47_scaleZ.o" "rigwRN.phl[430]";
connectAttr "rigw:tracks:stuff52_visibility.o" "rigwRN.phl[431]";
connectAttr "rigw:tracks:stuff52_translateX.o" "rigwRN.phl[432]";
connectAttr "rigw:tracks:stuff52_translateY.o" "rigwRN.phl[433]";
connectAttr "rigw:tracks:stuff52_translateZ.o" "rigwRN.phl[434]";
connectAttr "rigw:tracks:stuff52_rotateX.o" "rigwRN.phl[435]";
connectAttr "rigw:tracks:stuff52_rotateY.o" "rigwRN.phl[436]";
connectAttr "rigw:tracks:stuff52_rotateZ.o" "rigwRN.phl[437]";
connectAttr "rigw:tracks:stuff52_scaleX.o" "rigwRN.phl[438]";
connectAttr "rigw:tracks:stuff52_scaleY.o" "rigwRN.phl[439]";
connectAttr "rigw:tracks:stuff52_scaleZ.o" "rigwRN.phl[440]";
connectAttr "rigw:tracks:stuff53_visibility.o" "rigwRN.phl[441]";
connectAttr "rigw:tracks:stuff53_translateX.o" "rigwRN.phl[442]";
connectAttr "rigw:tracks:stuff53_translateY.o" "rigwRN.phl[443]";
connectAttr "rigw:tracks:stuff53_translateZ.o" "rigwRN.phl[444]";
connectAttr "rigw:tracks:stuff53_rotateX.o" "rigwRN.phl[445]";
connectAttr "rigw:tracks:stuff53_rotateY.o" "rigwRN.phl[446]";
connectAttr "rigw:tracks:stuff53_rotateZ.o" "rigwRN.phl[447]";
connectAttr "rigw:tracks:stuff53_scaleX.o" "rigwRN.phl[448]";
connectAttr "rigw:tracks:stuff53_scaleY.o" "rigwRN.phl[449]";
connectAttr "rigw:tracks:stuff53_scaleZ.o" "rigwRN.phl[450]";
connectAttr "rigw:tracks:stuff54_visibility.o" "rigwRN.phl[451]";
connectAttr "rigw:tracks:stuff54_translateX.o" "rigwRN.phl[452]";
connectAttr "rigw:tracks:stuff54_translateY.o" "rigwRN.phl[453]";
connectAttr "rigw:tracks:stuff54_translateZ.o" "rigwRN.phl[454]";
connectAttr "rigw:tracks:stuff54_rotateX.o" "rigwRN.phl[455]";
connectAttr "rigw:tracks:stuff54_rotateY.o" "rigwRN.phl[456]";
connectAttr "rigw:tracks:stuff54_rotateZ.o" "rigwRN.phl[457]";
connectAttr "rigw:tracks:stuff54_scaleX.o" "rigwRN.phl[458]";
connectAttr "rigw:tracks:stuff54_scaleY.o" "rigwRN.phl[459]";
connectAttr "rigw:tracks:stuff54_scaleZ.o" "rigwRN.phl[460]";
connectAttr "rigw:tracks:stuff55_visibility.o" "rigwRN.phl[461]";
connectAttr "rigw:tracks:stuff55_translateX.o" "rigwRN.phl[462]";
connectAttr "rigw:tracks:stuff55_translateY.o" "rigwRN.phl[463]";
connectAttr "rigw:tracks:stuff55_translateZ.o" "rigwRN.phl[464]";
connectAttr "rigw:tracks:stuff55_rotateX.o" "rigwRN.phl[465]";
connectAttr "rigw:tracks:stuff55_rotateY.o" "rigwRN.phl[466]";
connectAttr "rigw:tracks:stuff55_rotateZ.o" "rigwRN.phl[467]";
connectAttr "rigw:tracks:stuff55_scaleX.o" "rigwRN.phl[468]";
connectAttr "rigw:tracks:stuff55_scaleY.o" "rigwRN.phl[469]";
connectAttr "rigw:tracks:stuff55_scaleZ.o" "rigwRN.phl[470]";
connectAttr "rigw:tracks:stuff68_visibility.o" "rigwRN.phl[471]";
connectAttr "rigw:tracks:stuff68_translateX.o" "rigwRN.phl[472]";
connectAttr "rigw:tracks:stuff68_translateY.o" "rigwRN.phl[473]";
connectAttr "rigw:tracks:stuff68_translateZ.o" "rigwRN.phl[474]";
connectAttr "rigw:tracks:stuff68_rotateX.o" "rigwRN.phl[475]";
connectAttr "rigw:tracks:stuff68_rotateY.o" "rigwRN.phl[476]";
connectAttr "rigw:tracks:stuff68_rotateZ.o" "rigwRN.phl[477]";
connectAttr "rigw:tracks:stuff68_scaleX.o" "rigwRN.phl[478]";
connectAttr "rigw:tracks:stuff68_scaleY.o" "rigwRN.phl[479]";
connectAttr "rigw:tracks:stuff68_scaleZ.o" "rigwRN.phl[480]";
connectAttr "rigw:tracks:stuff67_visibility.o" "rigwRN.phl[481]";
connectAttr "rigw:tracks:stuff67_translateX.o" "rigwRN.phl[482]";
connectAttr "rigw:tracks:stuff67_translateY.o" "rigwRN.phl[483]";
connectAttr "rigw:tracks:stuff67_translateZ.o" "rigwRN.phl[484]";
connectAttr "rigw:tracks:stuff67_rotateX.o" "rigwRN.phl[485]";
connectAttr "rigw:tracks:stuff67_rotateY.o" "rigwRN.phl[486]";
connectAttr "rigw:tracks:stuff67_rotateZ.o" "rigwRN.phl[487]";
connectAttr "rigw:tracks:stuff67_scaleX.o" "rigwRN.phl[488]";
connectAttr "rigw:tracks:stuff67_scaleY.o" "rigwRN.phl[489]";
connectAttr "rigw:tracks:stuff67_scaleZ.o" "rigwRN.phl[490]";
connectAttr "rigw:tracks:stuff66_visibility.o" "rigwRN.phl[491]";
connectAttr "rigw:tracks:stuff66_translateX.o" "rigwRN.phl[492]";
connectAttr "rigw:tracks:stuff66_translateY.o" "rigwRN.phl[493]";
connectAttr "rigw:tracks:stuff66_translateZ.o" "rigwRN.phl[494]";
connectAttr "rigw:tracks:stuff66_rotateX.o" "rigwRN.phl[495]";
connectAttr "rigw:tracks:stuff66_rotateY.o" "rigwRN.phl[496]";
connectAttr "rigw:tracks:stuff66_rotateZ.o" "rigwRN.phl[497]";
connectAttr "rigw:tracks:stuff66_scaleX.o" "rigwRN.phl[498]";
connectAttr "rigw:tracks:stuff66_scaleY.o" "rigwRN.phl[499]";
connectAttr "rigw:tracks:stuff66_scaleZ.o" "rigwRN.phl[500]";
connectAttr "rigw:tracks:stuff65_visibility.o" "rigwRN.phl[501]";
connectAttr "rigw:tracks:stuff65_translateX.o" "rigwRN.phl[502]";
connectAttr "rigw:tracks:stuff65_translateY.o" "rigwRN.phl[503]";
connectAttr "rigw:tracks:stuff65_translateZ.o" "rigwRN.phl[504]";
connectAttr "rigw:tracks:stuff65_rotateX.o" "rigwRN.phl[505]";
connectAttr "rigw:tracks:stuff65_rotateY.o" "rigwRN.phl[506]";
connectAttr "rigw:tracks:stuff65_rotateZ.o" "rigwRN.phl[507]";
connectAttr "rigw:tracks:stuff65_scaleX.o" "rigwRN.phl[508]";
connectAttr "rigw:tracks:stuff65_scaleY.o" "rigwRN.phl[509]";
connectAttr "rigw:tracks:stuff65_scaleZ.o" "rigwRN.phl[510]";
connectAttr "rigw:tracks:stuff64_visibility.o" "rigwRN.phl[511]";
connectAttr "rigw:tracks:stuff64_translateX.o" "rigwRN.phl[512]";
connectAttr "rigw:tracks:stuff64_translateY.o" "rigwRN.phl[513]";
connectAttr "rigw:tracks:stuff64_translateZ.o" "rigwRN.phl[514]";
connectAttr "rigw:tracks:stuff64_rotateX.o" "rigwRN.phl[515]";
connectAttr "rigw:tracks:stuff64_rotateY.o" "rigwRN.phl[516]";
connectAttr "rigw:tracks:stuff64_rotateZ.o" "rigwRN.phl[517]";
connectAttr "rigw:tracks:stuff64_scaleX.o" "rigwRN.phl[518]";
connectAttr "rigw:tracks:stuff64_scaleY.o" "rigwRN.phl[519]";
connectAttr "rigw:tracks:stuff64_scaleZ.o" "rigwRN.phl[520]";
connectAttr "rigw:tracks:stuff63_visibility.o" "rigwRN.phl[521]";
connectAttr "rigw:tracks:stuff63_translateX.o" "rigwRN.phl[522]";
connectAttr "rigw:tracks:stuff63_translateY.o" "rigwRN.phl[523]";
connectAttr "rigw:tracks:stuff63_translateZ.o" "rigwRN.phl[524]";
connectAttr "rigw:tracks:stuff63_rotateX.o" "rigwRN.phl[525]";
connectAttr "rigw:tracks:stuff63_rotateY.o" "rigwRN.phl[526]";
connectAttr "rigw:tracks:stuff63_rotateZ.o" "rigwRN.phl[527]";
connectAttr "rigw:tracks:stuff63_scaleX.o" "rigwRN.phl[528]";
connectAttr "rigw:tracks:stuff63_scaleY.o" "rigwRN.phl[529]";
connectAttr "rigw:tracks:stuff63_scaleZ.o" "rigwRN.phl[530]";
connectAttr "rigw:tracks:stuff62_visibility.o" "rigwRN.phl[531]";
connectAttr "rigw:tracks:stuff62_translateX.o" "rigwRN.phl[532]";
connectAttr "rigw:tracks:stuff62_translateY.o" "rigwRN.phl[533]";
connectAttr "rigw:tracks:stuff62_translateZ.o" "rigwRN.phl[534]";
connectAttr "rigw:tracks:stuff62_rotateX.o" "rigwRN.phl[535]";
connectAttr "rigw:tracks:stuff62_rotateY.o" "rigwRN.phl[536]";
connectAttr "rigw:tracks:stuff62_rotateZ.o" "rigwRN.phl[537]";
connectAttr "rigw:tracks:stuff62_scaleX.o" "rigwRN.phl[538]";
connectAttr "rigw:tracks:stuff62_scaleY.o" "rigwRN.phl[539]";
connectAttr "rigw:tracks:stuff62_scaleZ.o" "rigwRN.phl[540]";
connectAttr "rigw:tracks:stuff61_visibility.o" "rigwRN.phl[541]";
connectAttr "rigw:tracks:stuff61_translateX.o" "rigwRN.phl[542]";
connectAttr "rigw:tracks:stuff61_translateY.o" "rigwRN.phl[543]";
connectAttr "rigw:tracks:stuff61_translateZ.o" "rigwRN.phl[544]";
connectAttr "rigw:tracks:stuff61_rotateX.o" "rigwRN.phl[545]";
connectAttr "rigw:tracks:stuff61_rotateY.o" "rigwRN.phl[546]";
connectAttr "rigw:tracks:stuff61_rotateZ.o" "rigwRN.phl[547]";
connectAttr "rigw:tracks:stuff61_scaleX.o" "rigwRN.phl[548]";
connectAttr "rigw:tracks:stuff61_scaleY.o" "rigwRN.phl[549]";
connectAttr "rigw:tracks:stuff61_scaleZ.o" "rigwRN.phl[550]";
connectAttr "rigw:tracks:stuff60_visibility.o" "rigwRN.phl[551]";
connectAttr "rigw:tracks:stuff60_translateX.o" "rigwRN.phl[552]";
connectAttr "rigw:tracks:stuff60_translateY.o" "rigwRN.phl[553]";
connectAttr "rigw:tracks:stuff60_translateZ.o" "rigwRN.phl[554]";
connectAttr "rigw:tracks:stuff60_rotateX.o" "rigwRN.phl[555]";
connectAttr "rigw:tracks:stuff60_rotateY.o" "rigwRN.phl[556]";
connectAttr "rigw:tracks:stuff60_rotateZ.o" "rigwRN.phl[557]";
connectAttr "rigw:tracks:stuff60_scaleX.o" "rigwRN.phl[558]";
connectAttr "rigw:tracks:stuff60_scaleY.o" "rigwRN.phl[559]";
connectAttr "rigw:tracks:stuff60_scaleZ.o" "rigwRN.phl[560]";
connectAttr "rigw:tracks:stuff59_visibility.o" "rigwRN.phl[561]";
connectAttr "rigw:tracks:stuff59_translateX.o" "rigwRN.phl[562]";
connectAttr "rigw:tracks:stuff59_translateY.o" "rigwRN.phl[563]";
connectAttr "rigw:tracks:stuff59_translateZ.o" "rigwRN.phl[564]";
connectAttr "rigw:tracks:stuff59_rotateX.o" "rigwRN.phl[565]";
connectAttr "rigw:tracks:stuff59_rotateY.o" "rigwRN.phl[566]";
connectAttr "rigw:tracks:stuff59_rotateZ.o" "rigwRN.phl[567]";
connectAttr "rigw:tracks:stuff59_scaleX.o" "rigwRN.phl[568]";
connectAttr "rigw:tracks:stuff59_scaleY.o" "rigwRN.phl[569]";
connectAttr "rigw:tracks:stuff59_scaleZ.o" "rigwRN.phl[570]";
connectAttr "rigw:tracks:stuff58_visibility.o" "rigwRN.phl[571]";
connectAttr "rigw:tracks:stuff58_translateX.o" "rigwRN.phl[572]";
connectAttr "rigw:tracks:stuff58_translateY.o" "rigwRN.phl[573]";
connectAttr "rigw:tracks:stuff58_translateZ.o" "rigwRN.phl[574]";
connectAttr "rigw:tracks:stuff58_rotateX.o" "rigwRN.phl[575]";
connectAttr "rigw:tracks:stuff58_rotateY.o" "rigwRN.phl[576]";
connectAttr "rigw:tracks:stuff58_rotateZ.o" "rigwRN.phl[577]";
connectAttr "rigw:tracks:stuff58_scaleX.o" "rigwRN.phl[578]";
connectAttr "rigw:tracks:stuff58_scaleY.o" "rigwRN.phl[579]";
connectAttr "rigw:tracks:stuff58_scaleZ.o" "rigwRN.phl[580]";
connectAttr "rigw:tracks:stuff57_visibility.o" "rigwRN.phl[581]";
connectAttr "rigw:tracks:stuff57_translateX.o" "rigwRN.phl[582]";
connectAttr "rigw:tracks:stuff57_translateY.o" "rigwRN.phl[583]";
connectAttr "rigw:tracks:stuff57_translateZ.o" "rigwRN.phl[584]";
connectAttr "rigw:tracks:stuff57_rotateX.o" "rigwRN.phl[585]";
connectAttr "rigw:tracks:stuff57_rotateY.o" "rigwRN.phl[586]";
connectAttr "rigw:tracks:stuff57_rotateZ.o" "rigwRN.phl[587]";
connectAttr "rigw:tracks:stuff57_scaleX.o" "rigwRN.phl[588]";
connectAttr "rigw:tracks:stuff57_scaleY.o" "rigwRN.phl[589]";
connectAttr "rigw:tracks:stuff57_scaleZ.o" "rigwRN.phl[590]";
connectAttr "rigw:tracks:stuff56_visibility.o" "rigwRN.phl[591]";
connectAttr "rigw:tracks:stuff56_translateX.o" "rigwRN.phl[592]";
connectAttr "rigw:tracks:stuff56_translateY.o" "rigwRN.phl[593]";
connectAttr "rigw:tracks:stuff56_translateZ.o" "rigwRN.phl[594]";
connectAttr "rigw:tracks:stuff56_rotateX.o" "rigwRN.phl[595]";
connectAttr "rigw:tracks:stuff56_rotateY.o" "rigwRN.phl[596]";
connectAttr "rigw:tracks:stuff56_rotateZ.o" "rigwRN.phl[597]";
connectAttr "rigw:tracks:stuff56_scaleX.o" "rigwRN.phl[598]";
connectAttr "rigw:tracks:stuff56_scaleY.o" "rigwRN.phl[599]";
connectAttr "rigw:tracks:stuff56_scaleZ.o" "rigwRN.phl[600]";
connectAttr "rigw:tracks:stuff69_visibility.o" "rigwRN.phl[601]";
connectAttr "rigw:tracks:stuff69_translateX.o" "rigwRN.phl[602]";
connectAttr "rigw:tracks:stuff69_translateY.o" "rigwRN.phl[603]";
connectAttr "rigw:tracks:stuff69_translateZ.o" "rigwRN.phl[604]";
connectAttr "rigw:tracks:stuff69_rotateX.o" "rigwRN.phl[605]";
connectAttr "rigw:tracks:stuff69_rotateY.o" "rigwRN.phl[606]";
connectAttr "rigw:tracks:stuff69_rotateZ.o" "rigwRN.phl[607]";
connectAttr "rigw:tracks:stuff69_scaleX.o" "rigwRN.phl[608]";
connectAttr "rigw:tracks:stuff69_scaleY.o" "rigwRN.phl[609]";
connectAttr "rigw:tracks:stuff69_scaleZ.o" "rigwRN.phl[610]";
connectAttr "rigw:tracks:stuff94_visibility.o" "rigwRN.phl[611]";
connectAttr "rigw:tracks:stuff94_translateX.o" "rigwRN.phl[612]";
connectAttr "rigw:tracks:stuff94_translateY.o" "rigwRN.phl[613]";
connectAttr "rigw:tracks:stuff94_translateZ.o" "rigwRN.phl[614]";
connectAttr "rigw:tracks:stuff94_rotateX.o" "rigwRN.phl[615]";
connectAttr "rigw:tracks:stuff94_rotateY.o" "rigwRN.phl[616]";
connectAttr "rigw:tracks:stuff94_rotateZ.o" "rigwRN.phl[617]";
connectAttr "rigw:tracks:stuff94_scaleX.o" "rigwRN.phl[618]";
connectAttr "rigw:tracks:stuff94_scaleY.o" "rigwRN.phl[619]";
connectAttr "rigw:tracks:stuff94_scaleZ.o" "rigwRN.phl[620]";
connectAttr "rigw:tracks:stuff93_visibility.o" "rigwRN.phl[621]";
connectAttr "rigw:tracks:stuff93_translateX.o" "rigwRN.phl[622]";
connectAttr "rigw:tracks:stuff93_translateY.o" "rigwRN.phl[623]";
connectAttr "rigw:tracks:stuff93_translateZ.o" "rigwRN.phl[624]";
connectAttr "rigw:tracks:stuff93_rotateX.o" "rigwRN.phl[625]";
connectAttr "rigw:tracks:stuff93_rotateY.o" "rigwRN.phl[626]";
connectAttr "rigw:tracks:stuff93_rotateZ.o" "rigwRN.phl[627]";
connectAttr "rigw:tracks:stuff93_scaleX.o" "rigwRN.phl[628]";
connectAttr "rigw:tracks:stuff93_scaleY.o" "rigwRN.phl[629]";
connectAttr "rigw:tracks:stuff93_scaleZ.o" "rigwRN.phl[630]";
connectAttr "rigw:tracks:stuff92_visibility.o" "rigwRN.phl[631]";
connectAttr "rigw:tracks:stuff92_translateX.o" "rigwRN.phl[632]";
connectAttr "rigw:tracks:stuff92_translateY.o" "rigwRN.phl[633]";
connectAttr "rigw:tracks:stuff92_translateZ.o" "rigwRN.phl[634]";
connectAttr "rigw:tracks:stuff92_rotateX.o" "rigwRN.phl[635]";
connectAttr "rigw:tracks:stuff92_rotateY.o" "rigwRN.phl[636]";
connectAttr "rigw:tracks:stuff92_rotateZ.o" "rigwRN.phl[637]";
connectAttr "rigw:tracks:stuff92_scaleX.o" "rigwRN.phl[638]";
connectAttr "rigw:tracks:stuff92_scaleY.o" "rigwRN.phl[639]";
connectAttr "rigw:tracks:stuff92_scaleZ.o" "rigwRN.phl[640]";
connectAttr "rigw:tracks:stuff91_visibility.o" "rigwRN.phl[641]";
connectAttr "rigw:tracks:stuff91_translateX.o" "rigwRN.phl[642]";
connectAttr "rigw:tracks:stuff91_translateY.o" "rigwRN.phl[643]";
connectAttr "rigw:tracks:stuff91_translateZ.o" "rigwRN.phl[644]";
connectAttr "rigw:tracks:stuff91_rotateX.o" "rigwRN.phl[645]";
connectAttr "rigw:tracks:stuff91_rotateY.o" "rigwRN.phl[646]";
connectAttr "rigw:tracks:stuff91_rotateZ.o" "rigwRN.phl[647]";
connectAttr "rigw:tracks:stuff91_scaleX.o" "rigwRN.phl[648]";
connectAttr "rigw:tracks:stuff91_scaleY.o" "rigwRN.phl[649]";
connectAttr "rigw:tracks:stuff91_scaleZ.o" "rigwRN.phl[650]";
connectAttr "rigw:tracks:stuff85_visibility.o" "rigwRN.phl[651]";
connectAttr "rigw:tracks:stuff85_translateX.o" "rigwRN.phl[652]";
connectAttr "rigw:tracks:stuff85_translateY.o" "rigwRN.phl[653]";
connectAttr "rigw:tracks:stuff85_translateZ.o" "rigwRN.phl[654]";
connectAttr "rigw:tracks:stuff85_rotateX.o" "rigwRN.phl[655]";
connectAttr "rigw:tracks:stuff85_rotateY.o" "rigwRN.phl[656]";
connectAttr "rigw:tracks:stuff85_rotateZ.o" "rigwRN.phl[657]";
connectAttr "rigw:tracks:stuff85_scaleX.o" "rigwRN.phl[658]";
connectAttr "rigw:tracks:stuff85_scaleY.o" "rigwRN.phl[659]";
connectAttr "rigw:tracks:stuff85_scaleZ.o" "rigwRN.phl[660]";
connectAttr "rigw:tracks:stuff86_visibility.o" "rigwRN.phl[661]";
connectAttr "rigw:tracks:stuff86_translateX.o" "rigwRN.phl[662]";
connectAttr "rigw:tracks:stuff86_translateY.o" "rigwRN.phl[663]";
connectAttr "rigw:tracks:stuff86_translateZ.o" "rigwRN.phl[664]";
connectAttr "rigw:tracks:stuff86_rotateX.o" "rigwRN.phl[665]";
connectAttr "rigw:tracks:stuff86_rotateY.o" "rigwRN.phl[666]";
connectAttr "rigw:tracks:stuff86_rotateZ.o" "rigwRN.phl[667]";
connectAttr "rigw:tracks:stuff86_scaleX.o" "rigwRN.phl[668]";
connectAttr "rigw:tracks:stuff86_scaleY.o" "rigwRN.phl[669]";
connectAttr "rigw:tracks:stuff86_scaleZ.o" "rigwRN.phl[670]";
connectAttr "rigw:tracks:stuff87_visibility.o" "rigwRN.phl[671]";
connectAttr "rigw:tracks:stuff87_translateX.o" "rigwRN.phl[672]";
connectAttr "rigw:tracks:stuff87_translateY.o" "rigwRN.phl[673]";
connectAttr "rigw:tracks:stuff87_translateZ.o" "rigwRN.phl[674]";
connectAttr "rigw:tracks:stuff87_rotateX.o" "rigwRN.phl[675]";
connectAttr "rigw:tracks:stuff87_rotateY.o" "rigwRN.phl[676]";
connectAttr "rigw:tracks:stuff87_rotateZ.o" "rigwRN.phl[677]";
connectAttr "rigw:tracks:stuff87_scaleX.o" "rigwRN.phl[678]";
connectAttr "rigw:tracks:stuff87_scaleY.o" "rigwRN.phl[679]";
connectAttr "rigw:tracks:stuff87_scaleZ.o" "rigwRN.phl[680]";
connectAttr "rigw:tracks:stuff88_visibility.o" "rigwRN.phl[681]";
connectAttr "rigw:tracks:stuff88_translateX.o" "rigwRN.phl[682]";
connectAttr "rigw:tracks:stuff88_translateY.o" "rigwRN.phl[683]";
connectAttr "rigw:tracks:stuff88_translateZ.o" "rigwRN.phl[684]";
connectAttr "rigw:tracks:stuff88_rotateX.o" "rigwRN.phl[685]";
connectAttr "rigw:tracks:stuff88_rotateY.o" "rigwRN.phl[686]";
connectAttr "rigw:tracks:stuff88_rotateZ.o" "rigwRN.phl[687]";
connectAttr "rigw:tracks:stuff88_scaleX.o" "rigwRN.phl[688]";
connectAttr "rigw:tracks:stuff88_scaleY.o" "rigwRN.phl[689]";
connectAttr "rigw:tracks:stuff88_scaleZ.o" "rigwRN.phl[690]";
connectAttr "rigw:tracks:stuff89_visibility.o" "rigwRN.phl[691]";
connectAttr "rigw:tracks:stuff89_translateX.o" "rigwRN.phl[692]";
connectAttr "rigw:tracks:stuff89_translateY.o" "rigwRN.phl[693]";
connectAttr "rigw:tracks:stuff89_translateZ.o" "rigwRN.phl[694]";
connectAttr "rigw:tracks:stuff89_rotateX.o" "rigwRN.phl[695]";
connectAttr "rigw:tracks:stuff89_rotateY.o" "rigwRN.phl[696]";
connectAttr "rigw:tracks:stuff89_rotateZ.o" "rigwRN.phl[697]";
connectAttr "rigw:tracks:stuff89_scaleX.o" "rigwRN.phl[698]";
connectAttr "rigw:tracks:stuff89_scaleY.o" "rigwRN.phl[699]";
connectAttr "rigw:tracks:stuff89_scaleZ.o" "rigwRN.phl[700]";
connectAttr "rigw:tracks:stuff90_visibility.o" "rigwRN.phl[701]";
connectAttr "rigw:tracks:stuff90_translateX.o" "rigwRN.phl[702]";
connectAttr "rigw:tracks:stuff90_translateY.o" "rigwRN.phl[703]";
connectAttr "rigw:tracks:stuff90_translateZ.o" "rigwRN.phl[704]";
connectAttr "rigw:tracks:stuff90_rotateX.o" "rigwRN.phl[705]";
connectAttr "rigw:tracks:stuff90_rotateY.o" "rigwRN.phl[706]";
connectAttr "rigw:tracks:stuff90_rotateZ.o" "rigwRN.phl[707]";
connectAttr "rigw:tracks:stuff90_scaleX.o" "rigwRN.phl[708]";
connectAttr "rigw:tracks:stuff90_scaleY.o" "rigwRN.phl[709]";
connectAttr "rigw:tracks:stuff90_scaleZ.o" "rigwRN.phl[710]";
connectAttr "rigw:tracks:stuff95_visibility.o" "rigwRN.phl[711]";
connectAttr "rigw:tracks:stuff95_translateX.o" "rigwRN.phl[712]";
connectAttr "rigw:tracks:stuff95_translateY.o" "rigwRN.phl[713]";
connectAttr "rigw:tracks:stuff95_translateZ.o" "rigwRN.phl[714]";
connectAttr "rigw:tracks:stuff95_rotateX.o" "rigwRN.phl[715]";
connectAttr "rigw:tracks:stuff95_rotateY.o" "rigwRN.phl[716]";
connectAttr "rigw:tracks:stuff95_rotateZ.o" "rigwRN.phl[717]";
connectAttr "rigw:tracks:stuff95_scaleX.o" "rigwRN.phl[718]";
connectAttr "rigw:tracks:stuff95_scaleY.o" "rigwRN.phl[719]";
connectAttr "rigw:tracks:stuff95_scaleZ.o" "rigwRN.phl[720]";
connectAttr "rigw:tracks:stuff96_visibility.o" "rigwRN.phl[721]";
connectAttr "rigw:tracks:stuff96_translateX.o" "rigwRN.phl[722]";
connectAttr "rigw:tracks:stuff96_translateY.o" "rigwRN.phl[723]";
connectAttr "rigw:tracks:stuff96_translateZ.o" "rigwRN.phl[724]";
connectAttr "rigw:tracks:stuff96_rotateX.o" "rigwRN.phl[725]";
connectAttr "rigw:tracks:stuff96_rotateY.o" "rigwRN.phl[726]";
connectAttr "rigw:tracks:stuff96_rotateZ.o" "rigwRN.phl[727]";
connectAttr "rigw:tracks:stuff96_scaleX.o" "rigwRN.phl[728]";
connectAttr "rigw:tracks:stuff96_scaleY.o" "rigwRN.phl[729]";
connectAttr "rigw:tracks:stuff96_scaleZ.o" "rigwRN.phl[730]";
connectAttr "rigw:tracks:stuff97_visibility.o" "rigwRN.phl[731]";
connectAttr "rigw:tracks:stuff97_translateX.o" "rigwRN.phl[732]";
connectAttr "rigw:tracks:stuff97_translateY.o" "rigwRN.phl[733]";
connectAttr "rigw:tracks:stuff97_translateZ.o" "rigwRN.phl[734]";
connectAttr "rigw:tracks:stuff97_rotateX.o" "rigwRN.phl[735]";
connectAttr "rigw:tracks:stuff97_rotateY.o" "rigwRN.phl[736]";
connectAttr "rigw:tracks:stuff97_rotateZ.o" "rigwRN.phl[737]";
connectAttr "rigw:tracks:stuff97_scaleX.o" "rigwRN.phl[738]";
connectAttr "rigw:tracks:stuff97_scaleY.o" "rigwRN.phl[739]";
connectAttr "rigw:tracks:stuff97_scaleZ.o" "rigwRN.phl[740]";
connectAttr "rigw:tracks:stuff98_visibility.o" "rigwRN.phl[741]";
connectAttr "rigw:tracks:stuff98_translateX.o" "rigwRN.phl[742]";
connectAttr "rigw:tracks:stuff98_translateY.o" "rigwRN.phl[743]";
connectAttr "rigw:tracks:stuff98_translateZ.o" "rigwRN.phl[744]";
connectAttr "rigw:tracks:stuff98_rotateX.o" "rigwRN.phl[745]";
connectAttr "rigw:tracks:stuff98_rotateY.o" "rigwRN.phl[746]";
connectAttr "rigw:tracks:stuff98_rotateZ.o" "rigwRN.phl[747]";
connectAttr "rigw:tracks:stuff98_scaleX.o" "rigwRN.phl[748]";
connectAttr "rigw:tracks:stuff98_scaleY.o" "rigwRN.phl[749]";
connectAttr "rigw:tracks:stuff98_scaleZ.o" "rigwRN.phl[750]";
connectAttr "rigw:tracks:stuff111_visibility.o" "rigwRN.phl[751]";
connectAttr "rigw:tracks:stuff111_translateX.o" "rigwRN.phl[752]";
connectAttr "rigw:tracks:stuff111_translateY.o" "rigwRN.phl[753]";
connectAttr "rigw:tracks:stuff111_translateZ.o" "rigwRN.phl[754]";
connectAttr "rigw:tracks:stuff111_rotateX.o" "rigwRN.phl[755]";
connectAttr "rigw:tracks:stuff111_rotateY.o" "rigwRN.phl[756]";
connectAttr "rigw:tracks:stuff111_rotateZ.o" "rigwRN.phl[757]";
connectAttr "rigw:tracks:stuff111_scaleX.o" "rigwRN.phl[758]";
connectAttr "rigw:tracks:stuff111_scaleY.o" "rigwRN.phl[759]";
connectAttr "rigw:tracks:stuff111_scaleZ.o" "rigwRN.phl[760]";
connectAttr "rigw:tracks:stuff110_visibility.o" "rigwRN.phl[761]";
connectAttr "rigw:tracks:stuff110_translateX.o" "rigwRN.phl[762]";
connectAttr "rigw:tracks:stuff110_translateY.o" "rigwRN.phl[763]";
connectAttr "rigw:tracks:stuff110_translateZ.o" "rigwRN.phl[764]";
connectAttr "rigw:tracks:stuff110_rotateX.o" "rigwRN.phl[765]";
connectAttr "rigw:tracks:stuff110_rotateY.o" "rigwRN.phl[766]";
connectAttr "rigw:tracks:stuff110_rotateZ.o" "rigwRN.phl[767]";
connectAttr "rigw:tracks:stuff110_scaleX.o" "rigwRN.phl[768]";
connectAttr "rigw:tracks:stuff110_scaleY.o" "rigwRN.phl[769]";
connectAttr "rigw:tracks:stuff110_scaleZ.o" "rigwRN.phl[770]";
connectAttr "rigw:tracks:stuff109_visibility.o" "rigwRN.phl[771]";
connectAttr "rigw:tracks:stuff109_translateX.o" "rigwRN.phl[772]";
connectAttr "rigw:tracks:stuff109_translateY.o" "rigwRN.phl[773]";
connectAttr "rigw:tracks:stuff109_translateZ.o" "rigwRN.phl[774]";
connectAttr "rigw:tracks:stuff109_rotateX.o" "rigwRN.phl[775]";
connectAttr "rigw:tracks:stuff109_rotateY.o" "rigwRN.phl[776]";
connectAttr "rigw:tracks:stuff109_rotateZ.o" "rigwRN.phl[777]";
connectAttr "rigw:tracks:stuff109_scaleX.o" "rigwRN.phl[778]";
connectAttr "rigw:tracks:stuff109_scaleY.o" "rigwRN.phl[779]";
connectAttr "rigw:tracks:stuff109_scaleZ.o" "rigwRN.phl[780]";
connectAttr "rigw:tracks:stuff108_visibility.o" "rigwRN.phl[781]";
connectAttr "rigw:tracks:stuff108_translateX.o" "rigwRN.phl[782]";
connectAttr "rigw:tracks:stuff108_translateY.o" "rigwRN.phl[783]";
connectAttr "rigw:tracks:stuff108_translateZ.o" "rigwRN.phl[784]";
connectAttr "rigw:tracks:stuff108_rotateX.o" "rigwRN.phl[785]";
connectAttr "rigw:tracks:stuff108_rotateY.o" "rigwRN.phl[786]";
connectAttr "rigw:tracks:stuff108_rotateZ.o" "rigwRN.phl[787]";
connectAttr "rigw:tracks:stuff108_scaleX.o" "rigwRN.phl[788]";
connectAttr "rigw:tracks:stuff108_scaleY.o" "rigwRN.phl[789]";
connectAttr "rigw:tracks:stuff108_scaleZ.o" "rigwRN.phl[790]";
connectAttr "rigw:tracks:stuff107_visibility.o" "rigwRN.phl[791]";
connectAttr "rigw:tracks:stuff107_translateX.o" "rigwRN.phl[792]";
connectAttr "rigw:tracks:stuff107_translateY.o" "rigwRN.phl[793]";
connectAttr "rigw:tracks:stuff107_translateZ.o" "rigwRN.phl[794]";
connectAttr "rigw:tracks:stuff107_rotateX.o" "rigwRN.phl[795]";
connectAttr "rigw:tracks:stuff107_rotateY.o" "rigwRN.phl[796]";
connectAttr "rigw:tracks:stuff107_rotateZ.o" "rigwRN.phl[797]";
connectAttr "rigw:tracks:stuff107_scaleX.o" "rigwRN.phl[798]";
connectAttr "rigw:tracks:stuff107_scaleY.o" "rigwRN.phl[799]";
connectAttr "rigw:tracks:stuff107_scaleZ.o" "rigwRN.phl[800]";
connectAttr "rigw:tracks:stuff106_visibility.o" "rigwRN.phl[801]";
connectAttr "rigw:tracks:stuff106_translateX.o" "rigwRN.phl[802]";
connectAttr "rigw:tracks:stuff106_translateY.o" "rigwRN.phl[803]";
connectAttr "rigw:tracks:stuff106_translateZ.o" "rigwRN.phl[804]";
connectAttr "rigw:tracks:stuff106_rotateX.o" "rigwRN.phl[805]";
connectAttr "rigw:tracks:stuff106_rotateY.o" "rigwRN.phl[806]";
connectAttr "rigw:tracks:stuff106_rotateZ.o" "rigwRN.phl[807]";
connectAttr "rigw:tracks:stuff106_scaleX.o" "rigwRN.phl[808]";
connectAttr "rigw:tracks:stuff106_scaleY.o" "rigwRN.phl[809]";
connectAttr "rigw:tracks:stuff106_scaleZ.o" "rigwRN.phl[810]";
connectAttr "rigw:tracks:stuff105_visibility.o" "rigwRN.phl[811]";
connectAttr "rigw:tracks:stuff105_translateX.o" "rigwRN.phl[812]";
connectAttr "rigw:tracks:stuff105_translateY.o" "rigwRN.phl[813]";
connectAttr "rigw:tracks:stuff105_translateZ.o" "rigwRN.phl[814]";
connectAttr "rigw:tracks:stuff105_rotateX.o" "rigwRN.phl[815]";
connectAttr "rigw:tracks:stuff105_rotateY.o" "rigwRN.phl[816]";
connectAttr "rigw:tracks:stuff105_rotateZ.o" "rigwRN.phl[817]";
connectAttr "rigw:tracks:stuff105_scaleX.o" "rigwRN.phl[818]";
connectAttr "rigw:tracks:stuff105_scaleY.o" "rigwRN.phl[819]";
connectAttr "rigw:tracks:stuff105_scaleZ.o" "rigwRN.phl[820]";
connectAttr "rigw:tracks:stuff104_visibility.o" "rigwRN.phl[821]";
connectAttr "rigw:tracks:stuff104_translateX.o" "rigwRN.phl[822]";
connectAttr "rigw:tracks:stuff104_translateY.o" "rigwRN.phl[823]";
connectAttr "rigw:tracks:stuff104_translateZ.o" "rigwRN.phl[824]";
connectAttr "rigw:tracks:stuff104_rotateX.o" "rigwRN.phl[825]";
connectAttr "rigw:tracks:stuff104_rotateY.o" "rigwRN.phl[826]";
connectAttr "rigw:tracks:stuff104_rotateZ.o" "rigwRN.phl[827]";
connectAttr "rigw:tracks:stuff104_scaleX.o" "rigwRN.phl[828]";
connectAttr "rigw:tracks:stuff104_scaleY.o" "rigwRN.phl[829]";
connectAttr "rigw:tracks:stuff104_scaleZ.o" "rigwRN.phl[830]";
connectAttr "rigw:tracks:stuff103_visibility.o" "rigwRN.phl[831]";
connectAttr "rigw:tracks:stuff103_translateX.o" "rigwRN.phl[832]";
connectAttr "rigw:tracks:stuff103_translateY.o" "rigwRN.phl[833]";
connectAttr "rigw:tracks:stuff103_translateZ.o" "rigwRN.phl[834]";
connectAttr "rigw:tracks:stuff103_rotateX.o" "rigwRN.phl[835]";
connectAttr "rigw:tracks:stuff103_rotateY.o" "rigwRN.phl[836]";
connectAttr "rigw:tracks:stuff103_rotateZ.o" "rigwRN.phl[837]";
connectAttr "rigw:tracks:stuff103_scaleX.o" "rigwRN.phl[838]";
connectAttr "rigw:tracks:stuff103_scaleY.o" "rigwRN.phl[839]";
connectAttr "rigw:tracks:stuff103_scaleZ.o" "rigwRN.phl[840]";
connectAttr "rigw:tracks:stuff102_visibility.o" "rigwRN.phl[841]";
connectAttr "rigw:tracks:stuff102_translateX.o" "rigwRN.phl[842]";
connectAttr "rigw:tracks:stuff102_translateY.o" "rigwRN.phl[843]";
connectAttr "rigw:tracks:stuff102_translateZ.o" "rigwRN.phl[844]";
connectAttr "rigw:tracks:stuff102_rotateX.o" "rigwRN.phl[845]";
connectAttr "rigw:tracks:stuff102_rotateY.o" "rigwRN.phl[846]";
connectAttr "rigw:tracks:stuff102_rotateZ.o" "rigwRN.phl[847]";
connectAttr "rigw:tracks:stuff102_scaleX.o" "rigwRN.phl[848]";
connectAttr "rigw:tracks:stuff102_scaleY.o" "rigwRN.phl[849]";
connectAttr "rigw:tracks:stuff102_scaleZ.o" "rigwRN.phl[850]";
connectAttr "rigw:tracks:stuff101_visibility.o" "rigwRN.phl[851]";
connectAttr "rigw:tracks:stuff101_translateX.o" "rigwRN.phl[852]";
connectAttr "rigw:tracks:stuff101_translateY.o" "rigwRN.phl[853]";
connectAttr "rigw:tracks:stuff101_translateZ.o" "rigwRN.phl[854]";
connectAttr "rigw:tracks:stuff101_rotateX.o" "rigwRN.phl[855]";
connectAttr "rigw:tracks:stuff101_rotateY.o" "rigwRN.phl[856]";
connectAttr "rigw:tracks:stuff101_rotateZ.o" "rigwRN.phl[857]";
connectAttr "rigw:tracks:stuff101_scaleX.o" "rigwRN.phl[858]";
connectAttr "rigw:tracks:stuff101_scaleY.o" "rigwRN.phl[859]";
connectAttr "rigw:tracks:stuff101_scaleZ.o" "rigwRN.phl[860]";
connectAttr "rigw:tracks:stuff100_visibility.o" "rigwRN.phl[861]";
connectAttr "rigw:tracks:stuff100_translateX.o" "rigwRN.phl[862]";
connectAttr "rigw:tracks:stuff100_translateY.o" "rigwRN.phl[863]";
connectAttr "rigw:tracks:stuff100_translateZ.o" "rigwRN.phl[864]";
connectAttr "rigw:tracks:stuff100_rotateX.o" "rigwRN.phl[865]";
connectAttr "rigw:tracks:stuff100_rotateY.o" "rigwRN.phl[866]";
connectAttr "rigw:tracks:stuff100_rotateZ.o" "rigwRN.phl[867]";
connectAttr "rigw:tracks:stuff100_scaleX.o" "rigwRN.phl[868]";
connectAttr "rigw:tracks:stuff100_scaleY.o" "rigwRN.phl[869]";
connectAttr "rigw:tracks:stuff100_scaleZ.o" "rigwRN.phl[870]";
connectAttr "rigw:tracks:stuff99_visibility.o" "rigwRN.phl[871]";
connectAttr "rigw:tracks:stuff99_translateX.o" "rigwRN.phl[872]";
connectAttr "rigw:tracks:stuff99_translateY.o" "rigwRN.phl[873]";
connectAttr "rigw:tracks:stuff99_translateZ.o" "rigwRN.phl[874]";
connectAttr "rigw:tracks:stuff99_rotateX.o" "rigwRN.phl[875]";
connectAttr "rigw:tracks:stuff99_rotateY.o" "rigwRN.phl[876]";
connectAttr "rigw:tracks:stuff99_rotateZ.o" "rigwRN.phl[877]";
connectAttr "rigw:tracks:stuff99_scaleX.o" "rigwRN.phl[878]";
connectAttr "rigw:tracks:stuff99_scaleY.o" "rigwRN.phl[879]";
connectAttr "rigw:tracks:stuff99_scaleZ.o" "rigwRN.phl[880]";
connectAttr "rigw:tracks:stuff112_visibility.o" "rigwRN.phl[881]";
connectAttr "rigw:tracks:stuff112_translateX.o" "rigwRN.phl[882]";
connectAttr "rigw:tracks:stuff112_translateY.o" "rigwRN.phl[883]";
connectAttr "rigw:tracks:stuff112_translateZ.o" "rigwRN.phl[884]";
connectAttr "rigw:tracks:stuff112_rotateX.o" "rigwRN.phl[885]";
connectAttr "rigw:tracks:stuff112_rotateY.o" "rigwRN.phl[886]";
connectAttr "rigw:tracks:stuff112_rotateZ.o" "rigwRN.phl[887]";
connectAttr "rigw:tracks:stuff112_scaleX.o" "rigwRN.phl[888]";
connectAttr "rigw:tracks:stuff112_scaleY.o" "rigwRN.phl[889]";
connectAttr "rigw:tracks:stuff112_scaleZ.o" "rigwRN.phl[890]";
connectAttr "rigw:rig:Main_translateX.o" "rigwRN.phl[186]";
connectAttr "rigw:rig:Main_translateY.o" "rigwRN.phl[187]";
connectAttr "rigw:rig:Main_translateZ.o" "rigwRN.phl[188]";
connectAttr "rigw:rig:Main_visibility.o" "rigwRN.phl[189]";
connectAttr "rigw:rig:Main_rotateX.o" "rigwRN.phl[190]";
connectAttr "rigw:rig:Main_rotateY.o" "rigwRN.phl[191]";
connectAttr "rigw:rig:Main_rotateZ.o" "rigwRN.phl[192]";
connectAttr "rigw:rig:Main_scaleX.o" "rigwRN.phl[193]";
connectAttr "rigw:rig:Main_scaleY.o" "rigwRN.phl[194]";
connectAttr "rigw:rig:Main_scaleZ.o" "rigwRN.phl[195]";
connectAttr "rigw:rig:FKWheel1_R_rotateX.o" "rigwRN.phl[196]";
connectAttr "rigw:rig:FKWheel1_R_rotateY.o" "rigwRN.phl[197]";
connectAttr "rigw:rig:FKWheel1_R_rotateZ.o" "rigwRN.phl[198]";
connectAttr "rigw:rig:FKWheel2_R_rotateX.o" "rigwRN.phl[199]";
connectAttr "rigw:rig:FKWheel2_R_rotateY.o" "rigwRN.phl[200]";
connectAttr "rigw:rig:FKWheel2_R_rotateZ.o" "rigwRN.phl[201]";
connectAttr "rigw:rig:FKWheel3_R_rotateX.o" "rigwRN.phl[202]";
connectAttr "rigw:rig:FKWheel3_R_rotateY.o" "rigwRN.phl[203]";
connectAttr "rigw:rig:FKWheel3_R_rotateZ.o" "rigwRN.phl[204]";
connectAttr "rigw:rig:FKWheel4_R_rotateX.o" "rigwRN.phl[205]";
connectAttr "rigw:rig:FKWheel4_R_rotateY.o" "rigwRN.phl[206]";
connectAttr "rigw:rig:FKWheel4_R_rotateZ.o" "rigwRN.phl[207]";
connectAttr "rigw:rig:FKWheel5_R_rotateX.o" "rigwRN.phl[208]";
connectAttr "rigw:rig:FKWheel5_R_rotateY.o" "rigwRN.phl[209]";
connectAttr "rigw:rig:FKWheel5_R_rotateZ.o" "rigwRN.phl[210]";
connectAttr "rigw:rig:FKWheel6_R_rotateX.o" "rigwRN.phl[211]";
connectAttr "rigw:rig:FKWheel6_R_rotateY.o" "rigwRN.phl[212]";
connectAttr "rigw:rig:FKWheel6_R_rotateZ.o" "rigwRN.phl[213]";
connectAttr "rigw:rig:FKWheel1_L_rotateX.o" "rigwRN.phl[214]";
connectAttr "rigw:rig:FKWheel1_L_rotateY.o" "rigwRN.phl[215]";
connectAttr "rigw:rig:FKWheel1_L_rotateZ.o" "rigwRN.phl[216]";
connectAttr "rigw:rig:FKWheel2_L_rotateX.o" "rigwRN.phl[217]";
connectAttr "rigw:rig:FKWheel2_L_rotateY.o" "rigwRN.phl[218]";
connectAttr "rigw:rig:FKWheel2_L_rotateZ.o" "rigwRN.phl[219]";
connectAttr "rigw:rig:FKWheel3_L_rotateX.o" "rigwRN.phl[220]";
connectAttr "rigw:rig:FKWheel3_L_rotateY.o" "rigwRN.phl[221]";
connectAttr "rigw:rig:FKWheel3_L_rotateZ.o" "rigwRN.phl[222]";
connectAttr "rigw:rig:FKWheel4_L_rotateX.o" "rigwRN.phl[223]";
connectAttr "rigw:rig:FKWheel4_L_rotateY.o" "rigwRN.phl[224]";
connectAttr "rigw:rig:FKWheel4_L_rotateZ.o" "rigwRN.phl[225]";
connectAttr "rigw:rig:FKWheel5_L_rotateX.o" "rigwRN.phl[226]";
connectAttr "rigw:rig:FKWheel5_L_rotateY.o" "rigwRN.phl[227]";
connectAttr "rigw:rig:FKWheel5_L_rotateZ.o" "rigwRN.phl[228]";
connectAttr "rigw:rig:FKWheel6_L_rotateX.o" "rigwRN.phl[229]";
connectAttr "rigw:rig:FKWheel6_L_rotateY.o" "rigwRN.phl[230]";
connectAttr "rigw:rig:FKWheel6_L_rotateZ.o" "rigwRN.phl[231]";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[232]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[233]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[234]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[235]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[236]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[237]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[238]";
connectAttr "rigw:rig:FKBody_M_rotateX.o" "rigwRN.phl[239]";
connectAttr "rigw:rig:FKBody_M_rotateY.o" "rigwRN.phl[240]";
connectAttr "rigw:rig:FKBody_M_rotateZ.o" "rigwRN.phl[241]";
connectAttr "rigw:rig:FKWeapon_M_rotateX.o" "rigwRN.phl[242]";
connectAttr "rigw:rig:FKWeapon_M_rotateY.o" "rigwRN.phl[243]";
connectAttr "rigw:rig:FKWeapon_M_rotateZ.o" "rigwRN.phl[244]";
connectAttr "rigw:rig:FKShoulder_R_Global.o" "rigwRN.phl[245]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[246]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[247]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[248]";
connectAttr "rigw:rig:FKElbow_R_rotateX.o" "rigwRN.phl[249]";
connectAttr "rigw:rig:FKElbow_R_rotateY.o" "rigwRN.phl[250]";
connectAttr "rigw:rig:FKElbow_R_rotateZ.o" "rigwRN.phl[251]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateX.o" "rigwRN.phl[252]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateY.o" "rigwRN.phl[253]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateZ.o" "rigwRN.phl[254]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateX.o" "rigwRN.phl[255]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateY.o" "rigwRN.phl[256]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateZ.o" "rigwRN.phl[257]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateX.o" "rigwRN.phl[258]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateY.o" "rigwRN.phl[259]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateZ.o" "rigwRN.phl[260]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateX.o" "rigwRN.phl[261]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateY.o" "rigwRN.phl[262]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateZ.o" "rigwRN.phl[263]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateX.o" "rigwRN.phl[264]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateY.o" "rigwRN.phl[265]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateZ.o" "rigwRN.phl[266]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateX.o" "rigwRN.phl[267]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateY.o" "rigwRN.phl[268]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateZ.o" "rigwRN.phl[269]";
connectAttr "rigw:rig:FKShoulder_L_Global.o" "rigwRN.phl[270]";
connectAttr "rigw:rig:FKShoulder_L_rotateX.o" "rigwRN.phl[271]";
connectAttr "rigw:rig:FKShoulder_L_rotateY.o" "rigwRN.phl[272]";
connectAttr "rigw:rig:FKShoulder_L_rotateZ.o" "rigwRN.phl[273]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[274]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[275]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[276]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateX.o" "rigwRN.phl[277]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateY.o" "rigwRN.phl[278]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateZ.o" "rigwRN.phl[279]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateX.o" "rigwRN.phl[280]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateY.o" "rigwRN.phl[281]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateZ.o" "rigwRN.phl[282]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateX.o" "rigwRN.phl[283]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateY.o" "rigwRN.phl[284]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateZ.o" "rigwRN.phl[285]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateX.o" "rigwRN.phl[286]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateY.o" "rigwRN.phl[287]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateZ.o" "rigwRN.phl[288]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateX.o" "rigwRN.phl[289]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateY.o" "rigwRN.phl[290]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateZ.o" "rigwRN.phl[291]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateX.o" "rigwRN.phl[292]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateY.o" "rigwRN.phl[293]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateZ.o" "rigwRN.phl[294]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[295]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[296]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[297]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[298]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[299]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[300]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[301]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[302]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[303]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[304]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[305]";
connectAttr "rigw:rig:PoleLeg_R_translateX.o" "rigwRN.phl[306]";
connectAttr "rigw:rig:PoleLeg_R_translateY.o" "rigwRN.phl[307]";
connectAttr "rigw:rig:PoleLeg_R_translateZ.o" "rigwRN.phl[308]";
connectAttr "rigw:rig:PoleLeg_R_follow.o" "rigwRN.phl[309]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[310]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[311]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[312]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[313]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[314]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[315]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[316]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[317]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[318]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[319]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[320]";
connectAttr "rigw:rig:PoleLeg_L_translateX.o" "rigwRN.phl[321]";
connectAttr "rigw:rig:PoleLeg_L_translateY.o" "rigwRN.phl[322]";
connectAttr "rigw:rig:PoleLeg_L_translateZ.o" "rigwRN.phl[323]";
connectAttr "rigw:rig:PoleLeg_L_follow.o" "rigwRN.phl[324]";
connectAttr "rigw:rig:FKIKLeg_R_FKIKBlend.o" "rigwRN.phl[325]";
connectAttr "rigw:rig:FKIKLeg_R_IKVis.o" "rigwRN.phl[326]";
connectAttr "rigw:rig:FKIKLeg_R_FKVis.o" "rigwRN.phl[327]";
connectAttr "rigw:rig:FKIKLeg_L_FKIKBlend.o" "rigwRN.phl[328]";
connectAttr "rigw:rig:FKIKLeg_L_IKVis.o" "rigwRN.phl[329]";
connectAttr "rigw:rig:FKIKLeg_L_FKVis.o" "rigwRN.phl[330]";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "rigw:TrackR_translateX.o" "rigwRN.phl[146]";
connectAttr "rigw:TrackR_translateY.o" "rigwRN.phl[147]";
connectAttr "rigw:TrackR_translateZ.o" "rigwRN.phl[148]";
connectAttr "rigw:TrackR_visibility.o" "rigwRN.phl[149]";
connectAttr "rigw:TrackR_rotateX.o" "rigwRN.phl[150]";
connectAttr "rigw:TrackR_rotateY.o" "rigwRN.phl[151]";
connectAttr "rigw:TrackR_rotateZ.o" "rigwRN.phl[152]";
connectAttr "rigw:TrackR_scaleX.o" "rigwRN.phl[153]";
connectAttr "rigw:TrackR_scaleY.o" "rigwRN.phl[154]";
connectAttr "rigw:TrackR_scaleZ.o" "rigwRN.phl[155]";
connectAttr "rigw:TrackL_translateX.o" "rigwRN.phl[156]";
connectAttr "rigw:TrackL_translateY.o" "rigwRN.phl[157]";
connectAttr "rigw:TrackL_translateZ.o" "rigwRN.phl[158]";
connectAttr "rigw:TrackL_visibility.o" "rigwRN.phl[159]";
connectAttr "rigw:TrackL_rotateX.o" "rigwRN.phl[160]";
connectAttr "rigw:TrackL_rotateY.o" "rigwRN.phl[161]";
connectAttr "rigw:TrackL_rotateZ.o" "rigwRN.phl[162]";
connectAttr "rigw:TrackL_scaleX.o" "rigwRN.phl[163]";
connectAttr "rigw:TrackL_scaleY.o" "rigwRN.phl[164]";
connectAttr "rigw:TrackL_scaleZ.o" "rigwRN.phl[165]";
connectAttr "rigw:nurbsCircle2_scaleZ.o" "rigwRN.phl[166]";
connectAttr "rigw:nurbsCircle2_scaleY.o" "rigwRN.phl[167]";
connectAttr "rigw:nurbsCircle2_scaleX.o" "rigwRN.phl[168]";
connectAttr "rigw:nurbsCircle2_rotateZ.o" "rigwRN.phl[169]";
connectAttr "rigw:nurbsCircle2_rotateY.o" "rigwRN.phl[170]";
connectAttr "rigw:nurbsCircle2_rotateX.o" "rigwRN.phl[171]";
connectAttr "rigw:nurbsCircle2_translateZ.o" "rigwRN.phl[172]";
connectAttr "rigw:nurbsCircle2_translateY.o" "rigwRN.phl[173]";
connectAttr "rigw:nurbsCircle2_translateX.o" "rigwRN.phl[174]";
connectAttr "rigw:nurbsCircle2_visibility.o" "rigwRN.phl[175]";
connectAttr "rigw:nurbsCircle1_scaleZ.o" "rigwRN.phl[176]";
connectAttr "rigw:nurbsCircle1_scaleY.o" "rigwRN.phl[177]";
connectAttr "rigw:nurbsCircle1_scaleX.o" "rigwRN.phl[178]";
connectAttr "rigw:nurbsCircle1_rotateZ.o" "rigwRN.phl[179]";
connectAttr "rigw:nurbsCircle1_rotateY.o" "rigwRN.phl[180]";
connectAttr "rigw:nurbsCircle1_rotateX.o" "rigwRN.phl[181]";
connectAttr "rigw:nurbsCircle1_translateZ.o" "rigwRN.phl[182]";
connectAttr "rigw:nurbsCircle1_translateY.o" "rigwRN.phl[183]";
connectAttr "rigw:nurbsCircle1_translateX.o" "rigwRN.phl[184]";
connectAttr "rigw:nurbsCircle1_visibility.o" "rigwRN.phl[185]";
connectAttr "mia_exposure_photographic1.msg" ":defaultTextureList1.tx" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Artillery__mc-rigw@Art_hitreact.ma
