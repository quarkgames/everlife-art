//Maya ASCII 2013 scene
//Name: Artillery__mc-rigw@Art_run.ma
//Last modified: Wed, Jun 04, 2014 01:06:54 PM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Artillery/Artillery__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Artillery__rig.ma";
file -rdi 1 -rpr "everlife" -rfn "everlifeRN" "/Users/jmiller/Art/everlife//Assets/Art/LightingResources/everlife_LightRig_Characters.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Artillery/Artillery__mc-rigw.ma";
file -r -rpr "everlife" -dr 1 -rfn "everlifeRN" "/Users/jmiller/Art/everlife//Assets/Art/LightingResources/everlife_LightRig_Characters.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 35.516313907106181 21.879501564452255 36.700750414601693 ;
	setAttr ".r" -type "double3" -23.138352729606023 -677.79999999997676 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 50.324355576263422;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" 0 0 500 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".ow" 72;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 17 ".lnk";
	setAttr -s 17 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 662 ".phl";
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".phl[440]" 0;
	setAttr ".phl[441]" 0;
	setAttr ".phl[442]" 0;
	setAttr ".phl[443]" 0;
	setAttr ".phl[444]" 0;
	setAttr ".phl[445]" 0;
	setAttr ".phl[446]" 0;
	setAttr ".phl[447]" 0;
	setAttr ".phl[448]" 0;
	setAttr ".phl[449]" 0;
	setAttr ".phl[450]" 0;
	setAttr ".phl[451]" 0;
	setAttr ".phl[452]" 0;
	setAttr ".phl[453]" 0;
	setAttr ".phl[454]" 0;
	setAttr ".phl[455]" 0;
	setAttr ".phl[456]" 0;
	setAttr ".phl[457]" 0;
	setAttr ".phl[458]" 0;
	setAttr ".phl[459]" 0;
	setAttr ".phl[460]" 0;
	setAttr ".phl[461]" 0;
	setAttr ".phl[462]" 0;
	setAttr ".phl[463]" 0;
	setAttr ".phl[464]" 0;
	setAttr ".phl[465]" 0;
	setAttr ".phl[466]" 0;
	setAttr ".phl[467]" 0;
	setAttr ".phl[468]" 0;
	setAttr ".phl[469]" 0;
	setAttr ".phl[470]" 0;
	setAttr ".phl[471]" 0;
	setAttr ".phl[472]" 0;
	setAttr ".phl[473]" 0;
	setAttr ".phl[474]" 0;
	setAttr ".phl[475]" 0;
	setAttr ".phl[476]" 0;
	setAttr ".phl[477]" 0;
	setAttr ".phl[478]" 0;
	setAttr ".phl[479]" 0;
	setAttr ".phl[480]" 0;
	setAttr ".phl[481]" 0;
	setAttr ".phl[482]" 0;
	setAttr ".phl[483]" 0;
	setAttr ".phl[484]" 0;
	setAttr ".phl[485]" 0;
	setAttr ".phl[486]" 0;
	setAttr ".phl[487]" 0;
	setAttr ".phl[488]" 0;
	setAttr ".phl[489]" 0;
	setAttr ".phl[490]" 0;
	setAttr ".phl[491]" 0;
	setAttr ".phl[492]" 0;
	setAttr ".phl[493]" 0;
	setAttr ".phl[494]" 0;
	setAttr ".phl[495]" 0;
	setAttr ".phl[496]" 0;
	setAttr ".phl[497]" 0;
	setAttr ".phl[498]" 0;
	setAttr ".phl[499]" 0;
	setAttr ".phl[500]" 0;
	setAttr ".phl[501]" 0;
	setAttr ".phl[502]" 0;
	setAttr ".phl[503]" 0;
	setAttr ".phl[504]" 0;
	setAttr ".phl[505]" 0;
	setAttr ".phl[506]" 0;
	setAttr ".phl[507]" 0;
	setAttr ".phl[508]" 0;
	setAttr ".phl[509]" 0;
	setAttr ".phl[510]" 0;
	setAttr ".phl[511]" 0;
	setAttr ".phl[512]" 0;
	setAttr ".phl[513]" 0;
	setAttr ".phl[514]" 0;
	setAttr ".phl[515]" 0;
	setAttr ".phl[516]" 0;
	setAttr ".phl[517]" 0;
	setAttr ".phl[518]" 0;
	setAttr ".phl[519]" 0;
	setAttr ".phl[520]" 0;
	setAttr ".phl[521]" 0;
	setAttr ".phl[522]" 0;
	setAttr ".phl[523]" 0;
	setAttr ".phl[524]" 0;
	setAttr ".phl[525]" 0;
	setAttr ".phl[526]" 0;
	setAttr ".phl[527]" 0;
	setAttr ".phl[528]" 0;
	setAttr ".phl[529]" 0;
	setAttr ".phl[530]" 0;
	setAttr ".phl[531]" 0;
	setAttr ".phl[532]" 0;
	setAttr ".phl[533]" 0;
	setAttr ".phl[534]" 0;
	setAttr ".phl[535]" 0;
	setAttr ".phl[536]" 0;
	setAttr ".phl[537]" 0;
	setAttr ".phl[538]" 0;
	setAttr ".phl[539]" 0;
	setAttr ".phl[540]" 0;
	setAttr ".phl[541]" 0;
	setAttr ".phl[542]" 0;
	setAttr ".phl[543]" 0;
	setAttr ".phl[544]" 0;
	setAttr ".phl[545]" 0;
	setAttr ".phl[546]" 0;
	setAttr ".phl[547]" 0;
	setAttr ".phl[548]" 0;
	setAttr ".phl[549]" 0;
	setAttr ".phl[550]" 0;
	setAttr ".phl[551]" 0;
	setAttr ".phl[552]" 0;
	setAttr ".phl[553]" 0;
	setAttr ".phl[554]" 0;
	setAttr ".phl[555]" 0;
	setAttr ".phl[556]" 0;
	setAttr ".phl[557]" 0;
	setAttr ".phl[558]" 0;
	setAttr ".phl[559]" 0;
	setAttr ".phl[560]" 0;
	setAttr ".phl[561]" 0;
	setAttr ".phl[562]" 0;
	setAttr ".phl[563]" 0;
	setAttr ".phl[564]" 0;
	setAttr ".phl[565]" 0;
	setAttr ".phl[566]" 0;
	setAttr ".phl[567]" 0;
	setAttr ".phl[568]" 0;
	setAttr ".phl[569]" 0;
	setAttr ".phl[570]" 0;
	setAttr ".phl[571]" 0;
	setAttr ".phl[572]" 0;
	setAttr ".phl[573]" 0;
	setAttr ".phl[574]" 0;
	setAttr ".phl[575]" 0;
	setAttr ".phl[576]" 0;
	setAttr ".phl[577]" 0;
	setAttr ".phl[578]" 0;
	setAttr ".phl[579]" 0;
	setAttr ".phl[580]" 0;
	setAttr ".phl[581]" 0;
	setAttr ".phl[582]" 0;
	setAttr ".phl[583]" 0;
	setAttr ".phl[584]" 0;
	setAttr ".phl[585]" 0;
	setAttr ".phl[586]" 0;
	setAttr ".phl[587]" 0;
	setAttr ".phl[588]" 0;
	setAttr ".phl[589]" 0;
	setAttr ".phl[590]" 0;
	setAttr ".phl[591]" 0;
	setAttr ".phl[592]" 0;
	setAttr ".phl[593]" 0;
	setAttr ".phl[594]" 0;
	setAttr ".phl[595]" 0;
	setAttr ".phl[596]" 0;
	setAttr ".phl[597]" 0;
	setAttr ".phl[598]" 0;
	setAttr ".phl[599]" 0;
	setAttr ".phl[600]" 0;
	setAttr ".phl[601]" 0;
	setAttr ".phl[602]" 0;
	setAttr ".phl[603]" 0;
	setAttr ".phl[604]" 0;
	setAttr ".phl[605]" 0;
	setAttr ".phl[606]" 0;
	setAttr ".phl[607]" 0;
	setAttr ".phl[608]" 0;
	setAttr ".phl[609]" 0;
	setAttr ".phl[610]" 0;
	setAttr ".phl[611]" 0;
	setAttr ".phl[612]" 0;
	setAttr ".phl[613]" 0;
	setAttr ".phl[614]" 0;
	setAttr ".phl[615]" 0;
	setAttr ".phl[616]" 0;
	setAttr ".phl[617]" 0;
	setAttr ".phl[618]" 0;
	setAttr ".phl[619]" 0;
	setAttr ".phl[620]" 0;
	setAttr ".phl[621]" 0;
	setAttr ".phl[622]" 0;
	setAttr ".phl[623]" 0;
	setAttr ".phl[624]" 0;
	setAttr ".phl[625]" 0;
	setAttr ".phl[626]" 0;
	setAttr ".phl[627]" 0;
	setAttr ".phl[628]" 0;
	setAttr ".phl[629]" 0;
	setAttr ".phl[630]" 0;
	setAttr ".phl[631]" 0;
	setAttr ".phl[632]" 0;
	setAttr ".phl[633]" 0;
	setAttr ".phl[634]" 0;
	setAttr ".phl[635]" 0;
	setAttr ".phl[636]" 0;
	setAttr ".phl[637]" 0;
	setAttr ".phl[638]" 0;
	setAttr ".phl[639]" 0;
	setAttr ".phl[640]" 0;
	setAttr ".phl[641]" 0;
	setAttr ".phl[642]" 0;
	setAttr ".phl[643]" 0;
	setAttr ".phl[644]" 0;
	setAttr ".phl[645]" 0;
	setAttr ".phl[646]" 0;
	setAttr ".phl[647]" 0;
	setAttr ".phl[648]" 0;
	setAttr ".phl[649]" 0;
	setAttr ".phl[650]" 0;
	setAttr ".phl[651]" 0;
	setAttr ".phl[652]" 0;
	setAttr ".phl[653]" 0;
	setAttr ".phl[654]" 0;
	setAttr ".phl[655]" 0;
	setAttr ".phl[656]" 0;
	setAttr ".phl[657]" 0;
	setAttr ".phl[658]" 0;
	setAttr ".phl[659]" 0;
	setAttr ".phl[660]" 0;
	setAttr ".phl[661]" 0;
	setAttr ".phl[662]" 0;
	setAttr ".phl[663]" 0;
	setAttr ".phl[664]" 0;
	setAttr ".phl[665]" 0;
	setAttr ".phl[666]" 0;
	setAttr ".phl[667]" 0;
	setAttr ".phl[668]" 0;
	setAttr ".phl[669]" 0;
	setAttr ".phl[670]" 0;
	setAttr ".phl[671]" 0;
	setAttr ".phl[672]" 0;
	setAttr ".phl[673]" 0;
	setAttr ".phl[674]" 0;
	setAttr ".phl[675]" 0;
	setAttr ".phl[676]" 0;
	setAttr ".phl[677]" 0;
	setAttr ".phl[678]" 0;
	setAttr ".phl[679]" 0;
	setAttr ".phl[680]" 0;
	setAttr ".phl[681]" 0;
	setAttr ".phl[682]" 0;
	setAttr ".phl[683]" 0;
	setAttr ".phl[684]" 0;
	setAttr ".phl[685]" 0;
	setAttr ".phl[686]" 0;
	setAttr ".phl[687]" 0;
	setAttr ".phl[688]" 0;
	setAttr ".phl[689]" 0;
	setAttr ".phl[690]" 0;
	setAttr ".phl[691]" 0;
	setAttr ".phl[692]" 0;
	setAttr ".phl[693]" 0;
	setAttr ".phl[694]" 0;
	setAttr ".phl[695]" 0;
	setAttr ".phl[696]" 0;
	setAttr ".phl[697]" 0;
	setAttr ".phl[698]" 0;
	setAttr ".phl[699]" 0;
	setAttr ".phl[700]" 0;
	setAttr ".phl[701]" 0;
	setAttr ".phl[702]" 0;
	setAttr ".phl[703]" 0;
	setAttr ".phl[704]" 0;
	setAttr ".phl[705]" 0;
	setAttr ".phl[706]" 0;
	setAttr ".phl[707]" 0;
	setAttr ".phl[708]" 0;
	setAttr ".phl[709]" 0;
	setAttr ".phl[710]" 0;
	setAttr ".phl[711]" 0;
	setAttr ".phl[712]" 0;
	setAttr ".phl[713]" 0;
	setAttr ".phl[714]" 0;
	setAttr ".phl[715]" 0;
	setAttr ".phl[716]" 0;
	setAttr ".phl[717]" 0;
	setAttr ".phl[718]" 0;
	setAttr ".phl[719]" 0;
	setAttr ".phl[720]" 0;
	setAttr ".phl[721]" 0;
	setAttr ".phl[722]" 0;
	setAttr ".phl[723]" 0;
	setAttr ".phl[724]" 0;
	setAttr ".phl[725]" 0;
	setAttr ".phl[726]" 0;
	setAttr ".phl[727]" 0;
	setAttr ".phl[728]" 0;
	setAttr ".phl[729]" 0;
	setAttr ".phl[730]" 0;
	setAttr ".phl[731]" 0;
	setAttr ".phl[732]" 0;
	setAttr ".phl[733]" 0;
	setAttr ".phl[734]" 0;
	setAttr ".phl[735]" 0;
	setAttr ".phl[736]" 0;
	setAttr ".phl[737]" 0;
	setAttr ".phl[738]" 0;
	setAttr ".phl[739]" 0;
	setAttr ".phl[740]" 0;
	setAttr ".phl[741]" 0;
	setAttr ".phl[742]" 0;
	setAttr ".phl[743]" 0;
	setAttr ".phl[744]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 24
		2 "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR" "rotate" " -type \"double3\" 116.015625 0 0"
		
		2 "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR" "rotateX" " -av"
		
		2 "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL" "rotate" " -type \"double3\" 116.015625 0 0"
		
		2 "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL" "rotateX" " -av"
		
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.rotateX" 
		"rigwRN.placeHolderList[83]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.rotateY" 
		"rigwRN.placeHolderList[84]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.rotateZ" 
		"rigwRN.placeHolderList[85]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.visibility" 
		"rigwRN.placeHolderList[86]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.translateX" 
		"rigwRN.placeHolderList[87]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.translateY" 
		"rigwRN.placeHolderList[88]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.translateZ" 
		"rigwRN.placeHolderList[89]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.scaleX" 
		"rigwRN.placeHolderList[90]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.scaleY" 
		"rigwRN.placeHolderList[91]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackR.scaleZ" 
		"rigwRN.placeHolderList[92]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.rotateX" 
		"rigwRN.placeHolderList[93]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.rotateY" 
		"rigwRN.placeHolderList[94]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.rotateZ" 
		"rigwRN.placeHolderList[95]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.visibility" 
		"rigwRN.placeHolderList[96]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.translateX" 
		"rigwRN.placeHolderList[97]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.translateY" 
		"rigwRN.placeHolderList[98]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.translateZ" 
		"rigwRN.placeHolderList[99]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.scaleX" 
		"rigwRN.placeHolderList[100]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.scaleY" 
		"rigwRN.placeHolderList[101]" ""
		5 4 "rigwRN" "|rigw:rigGame|rigw:model|rigw:mesh:Artillery|rigw:TrackL.scaleZ" 
		"rigwRN.placeHolderList[102]" ""
		"rigw:rigRN" 0
		"rigwRN" 946
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "translate" 
		" -type \"double3\" 3.936553 0.8362 6.035284"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "rotate" 
		" -type \"double3\" 94.752968 0.460223 0.626802"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51" "rotateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "translate" 
		" -type \"double3\" 3.92481 1.51982 5.504459"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "rotate" 
		" -type \"double3\" 24.554215 0.460223 0.626802"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50" "rotateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49" "translate" 
		" -type \"double3\" 3.912503 1.951248 4.559938"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48" "translate" 
		" -type \"double3\" 3.900196 2.382676 3.615418"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42" "translate" 
		" -type \"double3\" 3.88959 2.698813 2.682868"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42" "rotate" 
		" -type \"double3\" 9.388794 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43" "translate" 
		" -type \"double3\" 3.88959 2.754467 1.698309"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43" "rotate" 
		" -type \"double3\" 0 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44" "translate" 
		" -type \"double3\" 3.88959 2.754467 0.6702"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45" "translate" 
		" -type \"double3\" 3.88959 2.754467 -0.357909"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46" "translate" 
		" -type \"double3\" 3.88959 2.754467 -1.386019"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47" "translate" 
		" -type \"double3\" 3.88959 2.754467 -2.414128"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52" "translate" 
		" -type \"double3\" 3.88959 2.630392 -3.498546"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53" "translate" 
		" -type \"double3\" 3.88959 2.322473 -4.454273"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54" "translate" 
		" -type \"double3\" 3.88959 1.887221 -5.435571"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55" "translate" 
		" -type \"double3\" 3.88959 1.451968 -6.41687"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "translate" 
		" -type \"double3\" 3.88959 0.786892 -6.874434"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "rotate" 
		" -type \"double3\" -90.735333 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "translate" 
		" -type \"double3\" 3.88959 0.0771255 -6.485033"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "rotate" 
		" -type \"double3\" -151.216669 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -5.448837"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66" "rotate" 
		" -type \"double3\" 180 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -4.429445"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -3.410054"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -2.390662"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -1.371271"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61" "translate" 
		" -type \"double3\" 3.88959 0.00360929 -0.351879"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60" "translate" 
		" -type \"double3\" 3.88959 0.00360929 0.667512"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59" "translate" 
		" -type \"double3\" 3.88959 0.00360929 1.686904"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58" "translate" 
		" -type \"double3\" 3.88959 0.00360929 2.706296"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57" "translate" 
		" -type \"double3\" 3.88959 0.00360929 3.725687"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56" "translate" 
		" -type \"double3\" 3.88959 0.00360929 4.745079"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "translate" 
		" -type \"double3\" 3.88959 0.117146 5.720589"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "rotate" 
		" -type \"double3\" 164.498227 0 0"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "translate" 
		" -type \"double3\" -3.936553 0.8362 6.035284"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "rotate" 
		" -type \"double3\" 85.247032 0.460223 179.373198"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "translate" 
		" -type \"double3\" -3.92481 1.51982 5.504459"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "rotate" 
		" -type \"double3\" 155.445785 0.460223 179.373198"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92" "translate" 
		" -type \"double3\" -3.912503 1.951248 4.559938"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91" "translate" 
		" -type \"double3\" -3.900196 2.382676 3.615418"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91" "translateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85" "translate" 
		" -type \"double3\" -3.88959 2.698813 2.682868"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86" "translate" 
		" -type \"double3\" -3.88959 2.754467 1.698309"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87" "translate" 
		" -type \"double3\" -3.88959 2.754467 0.6702"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88" "translate" 
		" -type \"double3\" -3.88959 2.754467 -0.357909"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89" "translate" 
		" -type \"double3\" -3.88959 2.754467 -1.386019"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90" "translate" 
		" -type \"double3\" -3.88959 2.754467 -2.414128"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95" "translate" 
		" -type \"double3\" -3.88959 2.630392 -3.498546"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96" "translate" 
		" -type \"double3\" -3.88959 2.322473 -4.454273"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97" "translate" 
		" -type \"double3\" -3.88959 1.887221 -5.435571"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "translate" 
		" -type \"double3\" -3.88959 1.451968 -6.41687"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "translate" 
		" -type \"double3\" -3.88959 0.786892 -6.874434"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "rotate" 
		" -type \"double3\" -89.264667 0 180"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "translate" 
		" -type \"double3\" -3.88959 0.0771255 -6.485033"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "rotate" 
		" -type \"double3\" -28.783331 0 180"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110" "rotateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -5.448837"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -4.429445"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -3.410054"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -2.390662"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -1.371271"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104" "translate" 
		" -type \"double3\" -3.88959 0.00360929 -0.351879"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103" "translate" 
		" -type \"double3\" -3.88959 0.00360929 0.667512"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102" "translate" 
		" -type \"double3\" -3.88959 0.00360929 1.686904"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101" "translate" 
		" -type \"double3\" -3.88959 0.00360929 2.706296"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100" "translate" 
		" -type \"double3\" -3.88959 0.00360929 3.725687"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99" "translate" 
		" -type \"double3\" -3.88959 0.00360929 4.745079"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "translate" 
		" -type \"double3\" -3.88959 0.117146 5.720589"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "translateY" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "translateZ" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "rotate" 
		" -type \"double3\" 15.501773 0 180"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "rotateX" 
		" -av"
		2 "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112" "rotateY" 
		" -av"
		2 "rigw:skinCluster1" "nodeState" " 0"
		2 "rigw:skinCluster1" "matrix" " -s 38"
		2 "rigw:skinCluster1" "lw[0:37]" " -s 38 0 0 0 0 0 0 0 0"
		2 "rigw:skinCluster1" "lockWeights" " -s 30"
		2 "rigw:bindPose1" "g[0:38]" " -s 39 1 1 1 1 1 1 1 1"
		2 "rigw:skinCluster2" "nodeState" " 0"
		2 "rigw:skinCluster2" "matrix" " -s 38"
		2 "rigw:skinCluster2" "lw[0:37]" " -s 38 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
		
		2 "rigw:skinCluster3" "nodeState" " 0"
		2 "rigw:skinCluster3" "matrix" " -s 38"
		2 "rigw:skinCluster3" "lw[0:37]" " -s 38 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
		
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel6_R.worldMatrix" 
		"rigw:skinCluster3.matrix[0]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel6_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[0]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel6_L.worldMatrix" 
		"rigw:skinCluster3.matrix[1]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel6_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[1]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel5_R.worldMatrix" 
		"rigw:skinCluster3.matrix[2]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel5_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[2]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel5_L.worldMatrix" 
		"rigw:skinCluster3.matrix[3]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel5_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[3]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel4_R.worldMatrix" 
		"rigw:skinCluster3.matrix[4]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel4_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[4]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel4_L.worldMatrix" 
		"rigw:skinCluster3.matrix[5]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel4_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[5]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel3_R.worldMatrix" 
		"rigw:skinCluster3.matrix[6]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel3_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[6]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel3_L.worldMatrix" 
		"rigw:skinCluster3.matrix[7]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel3_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[7]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel2_R.worldMatrix" 
		"rigw:skinCluster3.matrix[8]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel2_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[8]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel2_L.worldMatrix" 
		"rigw:skinCluster3.matrix[9]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel2_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[9]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel1_R.worldMatrix" 
		"rigw:skinCluster3.matrix[10]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel1_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[10]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel1_L.worldMatrix" 
		"rigw:skinCluster3.matrix[11]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel1_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[11]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Weapon_M.worldMatrix" 
		"rigw:skinCluster3.matrix[12]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Weapon_M.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[12]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R.worldMatrix" 
		"rigw:skinCluster3.matrix[13]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[13]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L.worldMatrix" 
		"rigw:skinCluster3.matrix[14]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[14]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R.worldMatrix" 
		"rigw:skinCluster3.matrix[15]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[15]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L.worldMatrix" 
		"rigw:skinCluster3.matrix[16]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[16]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R.worldMatrix" 
		"rigw:skinCluster3.matrix[17]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[17]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L.worldMatrix" 
		"rigw:skinCluster3.matrix[18]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[18]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_ThumbFinger1_R|rigw:rigGame_ThumbFinger2_R.worldMatrix" 
		"rigw:skinCluster3.matrix[19]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_ThumbFinger1_R|rigw:rigGame_ThumbFinger2_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[19]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_ThumbFinger1_L|rigw:rigGame_ThumbFinger2_L.worldMatrix" 
		"rigw:skinCluster3.matrix[20]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_ThumbFinger1_L|rigw:rigGame_ThumbFinger2_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[20]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_ThumbFinger1_R.worldMatrix" 
		"rigw:skinCluster3.matrix[21]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_ThumbFinger1_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[21]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_ThumbFinger1_L.worldMatrix" 
		"rigw:skinCluster3.matrix[22]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_ThumbFinger1_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[22]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R.worldMatrix" 
		"rigw:skinCluster3.matrix[23]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[23]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M.worldMatrix" 
		"rigw:skinCluster3.matrix[25]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[25]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M.worldMatrix" 
		"rigw:skinCluster3.matrix[26]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[26]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_MiddleFinger1_R|rigw:rigGame_MiddleFinger2_R.worldMatrix" 
		"rigw:skinCluster3.matrix[27]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_MiddleFinger1_R|rigw:rigGame_MiddleFinger2_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[27]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_MiddleFinger1_L|rigw:rigGame_MiddleFinger2_L.worldMatrix" 
		"rigw:skinCluster3.matrix[28]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_MiddleFinger1_L|rigw:rigGame_MiddleFinger2_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[28]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_MiddleFinger1_R.worldMatrix" 
		"rigw:skinCluster3.matrix[29]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_MiddleFinger1_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[29]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_MiddleFinger1_L.worldMatrix" 
		"rigw:skinCluster3.matrix[30]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_MiddleFinger1_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[30]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_IndexFinger1_R|rigw:rigGame_IndexFinger2_R.worldMatrix" 
		"rigw:skinCluster3.matrix[31]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_IndexFinger1_R|rigw:rigGame_IndexFinger2_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[31]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_IndexFinger1_L|rigw:rigGame_IndexFinger2_L.worldMatrix" 
		"rigw:skinCluster3.matrix[32]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_IndexFinger1_L|rigw:rigGame_IndexFinger2_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[32]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_IndexFinger1_R.worldMatrix" 
		"rigw:skinCluster3.matrix[33]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_IndexFinger1_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[33]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_IndexFinger1_L.worldMatrix" 
		"rigw:skinCluster3.matrix[34]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_IndexFinger1_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[34]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R.worldMatrix" 
		"rigw:skinCluster3.matrix[35]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[35]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L.worldMatrix" 
		"rigw:skinCluster3.matrix[36]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[36]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M.worldMatrix" 
		"rigw:skinCluster3.matrix[37]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M.lockInfluenceWeights" 
		"rigw:skinCluster3.lockWeights[37]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R.worldMatrix" 
		"rigw:skinCluster1.matrix[13]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R.lockInfluenceWeights" 
		"rigw:skinCluster1.lockWeights[13]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L.worldMatrix" 
		"rigw:skinCluster1.matrix[14]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L.lockInfluenceWeights" 
		"rigw:skinCluster1.lockWeights[14]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R.worldMatrix" 
		"rigw:skinCluster1.matrix[15]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R.lockInfluenceWeights" 
		"rigw:skinCluster1.lockWeights[15]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L.worldMatrix" 
		"rigw:skinCluster1.matrix[16]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L.lockInfluenceWeights" 
		"rigw:skinCluster1.lockWeights[16]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R.worldMatrix" 
		"rigw:skinCluster1.matrix[17]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R.lockInfluenceWeights" 
		"rigw:skinCluster1.lockWeights[17]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L.worldMatrix" 
		"rigw:skinCluster1.matrix[18]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L.lockInfluenceWeights" 
		"rigw:skinCluster1.lockWeights[18]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M.worldMatrix" 
		"rigw:skinCluster1.matrix[25]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M.lockInfluenceWeights" 
		"rigw:skinCluster1.lockWeights[25]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M.worldMatrix" 
		"rigw:skinCluster1.matrix[26]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M.lockInfluenceWeights" 
		"rigw:skinCluster1.lockWeights[26]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel6_R.worldMatrix" 
		"rigw:skinCluster2.matrix[0]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel6_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[0]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel6_L.worldMatrix" 
		"rigw:skinCluster2.matrix[1]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel6_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[1]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel5_R.worldMatrix" 
		"rigw:skinCluster2.matrix[2]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel5_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[2]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel5_L.worldMatrix" 
		"rigw:skinCluster2.matrix[3]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel5_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[3]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel4_R.worldMatrix" 
		"rigw:skinCluster2.matrix[4]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel4_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[4]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel4_L.worldMatrix" 
		"rigw:skinCluster2.matrix[5]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel4_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[5]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel3_R.worldMatrix" 
		"rigw:skinCluster2.matrix[6]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel3_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[6]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel3_L.worldMatrix" 
		"rigw:skinCluster2.matrix[7]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel3_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[7]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel2_R.worldMatrix" 
		"rigw:skinCluster2.matrix[8]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel2_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[8]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel2_L.worldMatrix" 
		"rigw:skinCluster2.matrix[9]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel2_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[9]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel1_R.worldMatrix" 
		"rigw:skinCluster2.matrix[10]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R|rigw:rigGame_Wheel1_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[10]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel1_L.worldMatrix" 
		"rigw:skinCluster2.matrix[11]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L|rigw:rigGame_Wheel1_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[11]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Weapon_M.worldMatrix" 
		"rigw:skinCluster2.matrix[12]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Weapon_M.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[12]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R.worldMatrix" 
		"rigw:skinCluster2.matrix[13]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[13]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L.worldMatrix" 
		"rigw:skinCluster2.matrix[14]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[14]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R.worldMatrix" 
		"rigw:skinCluster2.matrix[15]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R|rigw:rigGame_TrackBottom_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[15]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L.worldMatrix" 
		"rigw:skinCluster2.matrix[16]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L|rigw:rigGame_TrackBottom_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[16]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R.worldMatrix" 
		"rigw:skinCluster2.matrix[17]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_R|rigw:rigGame_TrackBend_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[17]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L.worldMatrix" 
		"rigw:skinCluster2.matrix[18]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Track_L|rigw:rigGame_TrackBend_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[18]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_ThumbFinger1_R|rigw:rigGame_ThumbFinger2_R.worldMatrix" 
		"rigw:skinCluster2.matrix[19]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_ThumbFinger1_R|rigw:rigGame_ThumbFinger2_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[19]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_ThumbFinger1_L|rigw:rigGame_ThumbFinger2_L.worldMatrix" 
		"rigw:skinCluster2.matrix[20]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_ThumbFinger1_L|rigw:rigGame_ThumbFinger2_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[20]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_ThumbFinger1_R.worldMatrix" 
		"rigw:skinCluster2.matrix[21]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_ThumbFinger1_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[21]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_ThumbFinger1_L.worldMatrix" 
		"rigw:skinCluster2.matrix[22]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_ThumbFinger1_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[22]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L.worldMatrix" 
		"rigw:skinCluster2.matrix[24]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[24]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M.worldMatrix" 
		"rigw:skinCluster2.matrix[25]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[25]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M.worldMatrix" 
		"rigw:skinCluster2.matrix[26]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[26]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_MiddleFinger1_R|rigw:rigGame_MiddleFinger2_R.worldMatrix" 
		"rigw:skinCluster2.matrix[27]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_MiddleFinger1_R|rigw:rigGame_MiddleFinger2_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[27]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_MiddleFinger1_L|rigw:rigGame_MiddleFinger2_L.worldMatrix" 
		"rigw:skinCluster2.matrix[28]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_MiddleFinger1_L|rigw:rigGame_MiddleFinger2_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[28]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_MiddleFinger1_R.worldMatrix" 
		"rigw:skinCluster2.matrix[29]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_MiddleFinger1_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[29]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_MiddleFinger1_L.worldMatrix" 
		"rigw:skinCluster2.matrix[30]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_MiddleFinger1_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[30]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_IndexFinger1_R|rigw:rigGame_IndexFinger2_R.worldMatrix" 
		"rigw:skinCluster2.matrix[31]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_IndexFinger1_R|rigw:rigGame_IndexFinger2_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[31]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_IndexFinger1_L|rigw:rigGame_IndexFinger2_L.worldMatrix" 
		"rigw:skinCluster2.matrix[32]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_IndexFinger1_L|rigw:rigGame_IndexFinger2_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[32]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_IndexFinger1_R.worldMatrix" 
		"rigw:skinCluster2.matrix[33]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R|rigw:rigGame_IndexFinger1_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[33]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_IndexFinger1_L.worldMatrix" 
		"rigw:skinCluster2.matrix[34]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L|rigw:rigGame_IndexFinger1_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[34]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R.worldMatrix" 
		"rigw:skinCluster2.matrix[35]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_R|rigw:rigGame_Elbow_R.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[35]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L.worldMatrix" 
		"rigw:skinCluster2.matrix[36]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M|rigw:rigGame_Shoulder_L|rigw:rigGame_Elbow_L.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[36]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M.worldMatrix" 
		"rigw:skinCluster2.matrix[37]" ""
		3 "|rigw:rigRNfosterParent1|rigw:rigGame|rigw:rigGame_Root_M|rigw:rigGame_Pelvis_M|rigw:rigGame_Body_M.lockInfluenceWeights" 
		"rigw:skinCluster2.lockWeights[37]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.translateX" 
		"rigwRN.placeHolderList[185]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.translateY" 
		"rigwRN.placeHolderList[186]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.translateZ" 
		"rigwRN.placeHolderList[187]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.rotateX" 
		"rigwRN.placeHolderList[188]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.rotateY" 
		"rigwRN.placeHolderList[189]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.rotateZ" 
		"rigwRN.placeHolderList[190]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.visibility" 
		"rigwRN.placeHolderList[191]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.scaleX" 
		"rigwRN.placeHolderList[192]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.scaleY" 
		"rigwRN.placeHolderList[193]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff51.scaleZ" 
		"rigwRN.placeHolderList[194]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.translateX" 
		"rigwRN.placeHolderList[195]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.translateY" 
		"rigwRN.placeHolderList[196]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.translateZ" 
		"rigwRN.placeHolderList[197]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.rotateX" 
		"rigwRN.placeHolderList[198]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.rotateY" 
		"rigwRN.placeHolderList[199]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.rotateZ" 
		"rigwRN.placeHolderList[200]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.visibility" 
		"rigwRN.placeHolderList[201]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.scaleX" 
		"rigwRN.placeHolderList[202]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.scaleY" 
		"rigwRN.placeHolderList[203]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff50.scaleZ" 
		"rigwRN.placeHolderList[204]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.translateX" 
		"rigwRN.placeHolderList[205]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.translateY" 
		"rigwRN.placeHolderList[206]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.translateZ" 
		"rigwRN.placeHolderList[207]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.visibility" 
		"rigwRN.placeHolderList[208]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.rotateX" 
		"rigwRN.placeHolderList[209]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.rotateY" 
		"rigwRN.placeHolderList[210]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.rotateZ" 
		"rigwRN.placeHolderList[211]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.scaleX" 
		"rigwRN.placeHolderList[212]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.scaleY" 
		"rigwRN.placeHolderList[213]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff49.scaleZ" 
		"rigwRN.placeHolderList[214]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.translateX" 
		"rigwRN.placeHolderList[215]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.translateY" 
		"rigwRN.placeHolderList[216]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.translateZ" 
		"rigwRN.placeHolderList[217]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.visibility" 
		"rigwRN.placeHolderList[218]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.rotateX" 
		"rigwRN.placeHolderList[219]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.rotateY" 
		"rigwRN.placeHolderList[220]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.rotateZ" 
		"rigwRN.placeHolderList[221]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.scaleX" 
		"rigwRN.placeHolderList[222]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.scaleY" 
		"rigwRN.placeHolderList[223]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff48.scaleZ" 
		"rigwRN.placeHolderList[224]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.translateX" 
		"rigwRN.placeHolderList[225]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.translateY" 
		"rigwRN.placeHolderList[226]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.translateZ" 
		"rigwRN.placeHolderList[227]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.rotateX" 
		"rigwRN.placeHolderList[228]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.rotateY" 
		"rigwRN.placeHolderList[229]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.rotateZ" 
		"rigwRN.placeHolderList[230]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.visibility" 
		"rigwRN.placeHolderList[231]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.scaleX" 
		"rigwRN.placeHolderList[232]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.scaleY" 
		"rigwRN.placeHolderList[233]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff42.scaleZ" 
		"rigwRN.placeHolderList[234]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.translateX" 
		"rigwRN.placeHolderList[235]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.translateY" 
		"rigwRN.placeHolderList[236]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.translateZ" 
		"rigwRN.placeHolderList[237]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.rotateX" 
		"rigwRN.placeHolderList[238]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.rotateY" 
		"rigwRN.placeHolderList[239]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.rotateZ" 
		"rigwRN.placeHolderList[240]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.visibility" 
		"rigwRN.placeHolderList[241]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.scaleX" 
		"rigwRN.placeHolderList[242]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.scaleY" 
		"rigwRN.placeHolderList[243]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff43.scaleZ" 
		"rigwRN.placeHolderList[244]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.translateX" 
		"rigwRN.placeHolderList[245]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.translateY" 
		"rigwRN.placeHolderList[246]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.translateZ" 
		"rigwRN.placeHolderList[247]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.visibility" 
		"rigwRN.placeHolderList[248]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.rotateX" 
		"rigwRN.placeHolderList[249]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.rotateY" 
		"rigwRN.placeHolderList[250]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.rotateZ" 
		"rigwRN.placeHolderList[251]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.scaleX" 
		"rigwRN.placeHolderList[252]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.scaleY" 
		"rigwRN.placeHolderList[253]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff44.scaleZ" 
		"rigwRN.placeHolderList[254]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.translateX" 
		"rigwRN.placeHolderList[255]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.translateY" 
		"rigwRN.placeHolderList[256]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.translateZ" 
		"rigwRN.placeHolderList[257]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.visibility" 
		"rigwRN.placeHolderList[258]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.rotateX" 
		"rigwRN.placeHolderList[259]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.rotateY" 
		"rigwRN.placeHolderList[260]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.rotateZ" 
		"rigwRN.placeHolderList[261]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.scaleX" 
		"rigwRN.placeHolderList[262]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.scaleY" 
		"rigwRN.placeHolderList[263]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff45.scaleZ" 
		"rigwRN.placeHolderList[264]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.translateX" 
		"rigwRN.placeHolderList[265]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.translateY" 
		"rigwRN.placeHolderList[266]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.translateZ" 
		"rigwRN.placeHolderList[267]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.visibility" 
		"rigwRN.placeHolderList[268]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.rotateX" 
		"rigwRN.placeHolderList[269]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.rotateY" 
		"rigwRN.placeHolderList[270]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.rotateZ" 
		"rigwRN.placeHolderList[271]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.scaleX" 
		"rigwRN.placeHolderList[272]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.scaleY" 
		"rigwRN.placeHolderList[273]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff46.scaleZ" 
		"rigwRN.placeHolderList[274]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.translateX" 
		"rigwRN.placeHolderList[275]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.translateY" 
		"rigwRN.placeHolderList[276]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.translateZ" 
		"rigwRN.placeHolderList[277]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.visibility" 
		"rigwRN.placeHolderList[278]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.rotateX" 
		"rigwRN.placeHolderList[279]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.rotateY" 
		"rigwRN.placeHolderList[280]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.rotateZ" 
		"rigwRN.placeHolderList[281]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.scaleX" 
		"rigwRN.placeHolderList[282]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.scaleY" 
		"rigwRN.placeHolderList[283]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff47.scaleZ" 
		"rigwRN.placeHolderList[284]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.translateX" 
		"rigwRN.placeHolderList[285]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.translateY" 
		"rigwRN.placeHolderList[286]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.translateZ" 
		"rigwRN.placeHolderList[287]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.visibility" 
		"rigwRN.placeHolderList[288]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.rotateX" 
		"rigwRN.placeHolderList[289]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.rotateY" 
		"rigwRN.placeHolderList[290]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.rotateZ" 
		"rigwRN.placeHolderList[291]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.scaleX" 
		"rigwRN.placeHolderList[292]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.scaleY" 
		"rigwRN.placeHolderList[293]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff52.scaleZ" 
		"rigwRN.placeHolderList[294]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.translateX" 
		"rigwRN.placeHolderList[295]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.translateY" 
		"rigwRN.placeHolderList[296]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.translateZ" 
		"rigwRN.placeHolderList[297]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.visibility" 
		"rigwRN.placeHolderList[298]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.rotateX" 
		"rigwRN.placeHolderList[299]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.rotateY" 
		"rigwRN.placeHolderList[300]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.rotateZ" 
		"rigwRN.placeHolderList[301]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.scaleX" 
		"rigwRN.placeHolderList[302]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.scaleY" 
		"rigwRN.placeHolderList[303]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff53.scaleZ" 
		"rigwRN.placeHolderList[304]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.translateX" 
		"rigwRN.placeHolderList[305]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.translateY" 
		"rigwRN.placeHolderList[306]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.translateZ" 
		"rigwRN.placeHolderList[307]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.visibility" 
		"rigwRN.placeHolderList[308]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.rotateX" 
		"rigwRN.placeHolderList[309]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.rotateY" 
		"rigwRN.placeHolderList[310]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.rotateZ" 
		"rigwRN.placeHolderList[311]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.scaleX" 
		"rigwRN.placeHolderList[312]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.scaleY" 
		"rigwRN.placeHolderList[313]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff54.scaleZ" 
		"rigwRN.placeHolderList[314]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.translateX" 
		"rigwRN.placeHolderList[315]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.translateY" 
		"rigwRN.placeHolderList[316]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.translateZ" 
		"rigwRN.placeHolderList[317]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.visibility" 
		"rigwRN.placeHolderList[318]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.rotateX" 
		"rigwRN.placeHolderList[319]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.rotateY" 
		"rigwRN.placeHolderList[320]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.rotateZ" 
		"rigwRN.placeHolderList[321]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.scaleX" 
		"rigwRN.placeHolderList[322]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.scaleY" 
		"rigwRN.placeHolderList[323]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff55.scaleZ" 
		"rigwRN.placeHolderList[324]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.translateX" 
		"rigwRN.placeHolderList[325]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.translateY" 
		"rigwRN.placeHolderList[326]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.translateZ" 
		"rigwRN.placeHolderList[327]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.rotateX" 
		"rigwRN.placeHolderList[328]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.rotateY" 
		"rigwRN.placeHolderList[329]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.rotateZ" 
		"rigwRN.placeHolderList[330]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.visibility" 
		"rigwRN.placeHolderList[331]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.scaleX" 
		"rigwRN.placeHolderList[332]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.scaleY" 
		"rigwRN.placeHolderList[333]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff68.scaleZ" 
		"rigwRN.placeHolderList[334]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.translateX" 
		"rigwRN.placeHolderList[335]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.translateY" 
		"rigwRN.placeHolderList[336]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.translateZ" 
		"rigwRN.placeHolderList[337]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.rotateX" 
		"rigwRN.placeHolderList[338]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.rotateY" 
		"rigwRN.placeHolderList[339]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.rotateZ" 
		"rigwRN.placeHolderList[340]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.visibility" 
		"rigwRN.placeHolderList[341]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.scaleX" 
		"rigwRN.placeHolderList[342]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.scaleY" 
		"rigwRN.placeHolderList[343]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff67.scaleZ" 
		"rigwRN.placeHolderList[344]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.translateX" 
		"rigwRN.placeHolderList[345]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.translateY" 
		"rigwRN.placeHolderList[346]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.translateZ" 
		"rigwRN.placeHolderList[347]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.rotateX" 
		"rigwRN.placeHolderList[348]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.rotateY" 
		"rigwRN.placeHolderList[349]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.rotateZ" 
		"rigwRN.placeHolderList[350]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.visibility" 
		"rigwRN.placeHolderList[351]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.scaleX" 
		"rigwRN.placeHolderList[352]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.scaleY" 
		"rigwRN.placeHolderList[353]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff66.scaleZ" 
		"rigwRN.placeHolderList[354]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.translateX" 
		"rigwRN.placeHolderList[355]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.translateY" 
		"rigwRN.placeHolderList[356]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.translateZ" 
		"rigwRN.placeHolderList[357]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.visibility" 
		"rigwRN.placeHolderList[358]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.rotateX" 
		"rigwRN.placeHolderList[359]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.rotateY" 
		"rigwRN.placeHolderList[360]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.rotateZ" 
		"rigwRN.placeHolderList[361]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.scaleX" 
		"rigwRN.placeHolderList[362]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.scaleY" 
		"rigwRN.placeHolderList[363]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff65.scaleZ" 
		"rigwRN.placeHolderList[364]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.translateX" 
		"rigwRN.placeHolderList[365]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.translateY" 
		"rigwRN.placeHolderList[366]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.translateZ" 
		"rigwRN.placeHolderList[367]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.visibility" 
		"rigwRN.placeHolderList[368]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.rotateX" 
		"rigwRN.placeHolderList[369]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.rotateY" 
		"rigwRN.placeHolderList[370]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.rotateZ" 
		"rigwRN.placeHolderList[371]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.scaleX" 
		"rigwRN.placeHolderList[372]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.scaleY" 
		"rigwRN.placeHolderList[373]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff64.scaleZ" 
		"rigwRN.placeHolderList[374]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.translateX" 
		"rigwRN.placeHolderList[375]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.translateY" 
		"rigwRN.placeHolderList[376]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.translateZ" 
		"rigwRN.placeHolderList[377]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.visibility" 
		"rigwRN.placeHolderList[378]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.rotateX" 
		"rigwRN.placeHolderList[379]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.rotateY" 
		"rigwRN.placeHolderList[380]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.rotateZ" 
		"rigwRN.placeHolderList[381]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.scaleX" 
		"rigwRN.placeHolderList[382]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.scaleY" 
		"rigwRN.placeHolderList[383]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff63.scaleZ" 
		"rigwRN.placeHolderList[384]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.translateX" 
		"rigwRN.placeHolderList[385]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.translateY" 
		"rigwRN.placeHolderList[386]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.translateZ" 
		"rigwRN.placeHolderList[387]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.visibility" 
		"rigwRN.placeHolderList[388]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.rotateX" 
		"rigwRN.placeHolderList[389]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.rotateY" 
		"rigwRN.placeHolderList[390]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.rotateZ" 
		"rigwRN.placeHolderList[391]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.scaleX" 
		"rigwRN.placeHolderList[392]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.scaleY" 
		"rigwRN.placeHolderList[393]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff62.scaleZ" 
		"rigwRN.placeHolderList[394]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.translateX" 
		"rigwRN.placeHolderList[395]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.translateY" 
		"rigwRN.placeHolderList[396]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.translateZ" 
		"rigwRN.placeHolderList[397]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.visibility" 
		"rigwRN.placeHolderList[398]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.rotateX" 
		"rigwRN.placeHolderList[399]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.rotateY" 
		"rigwRN.placeHolderList[400]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.rotateZ" 
		"rigwRN.placeHolderList[401]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.scaleX" 
		"rigwRN.placeHolderList[402]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.scaleY" 
		"rigwRN.placeHolderList[403]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff61.scaleZ" 
		"rigwRN.placeHolderList[404]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.translateX" 
		"rigwRN.placeHolderList[405]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.translateY" 
		"rigwRN.placeHolderList[406]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.translateZ" 
		"rigwRN.placeHolderList[407]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.visibility" 
		"rigwRN.placeHolderList[408]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.rotateX" 
		"rigwRN.placeHolderList[409]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.rotateY" 
		"rigwRN.placeHolderList[410]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.rotateZ" 
		"rigwRN.placeHolderList[411]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.scaleX" 
		"rigwRN.placeHolderList[412]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.scaleY" 
		"rigwRN.placeHolderList[413]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff60.scaleZ" 
		"rigwRN.placeHolderList[414]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.translateX" 
		"rigwRN.placeHolderList[415]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.translateY" 
		"rigwRN.placeHolderList[416]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.translateZ" 
		"rigwRN.placeHolderList[417]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.visibility" 
		"rigwRN.placeHolderList[418]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.rotateX" 
		"rigwRN.placeHolderList[419]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.rotateY" 
		"rigwRN.placeHolderList[420]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.rotateZ" 
		"rigwRN.placeHolderList[421]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.scaleX" 
		"rigwRN.placeHolderList[422]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.scaleY" 
		"rigwRN.placeHolderList[423]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff59.scaleZ" 
		"rigwRN.placeHolderList[424]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.translateX" 
		"rigwRN.placeHolderList[425]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.translateY" 
		"rigwRN.placeHolderList[426]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.translateZ" 
		"rigwRN.placeHolderList[427]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.visibility" 
		"rigwRN.placeHolderList[428]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.rotateX" 
		"rigwRN.placeHolderList[429]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.rotateY" 
		"rigwRN.placeHolderList[430]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.rotateZ" 
		"rigwRN.placeHolderList[431]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.scaleX" 
		"rigwRN.placeHolderList[432]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.scaleY" 
		"rigwRN.placeHolderList[433]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff58.scaleZ" 
		"rigwRN.placeHolderList[434]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.translateX" 
		"rigwRN.placeHolderList[435]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.translateY" 
		"rigwRN.placeHolderList[436]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.translateZ" 
		"rigwRN.placeHolderList[437]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.visibility" 
		"rigwRN.placeHolderList[438]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.rotateX" 
		"rigwRN.placeHolderList[439]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.rotateY" 
		"rigwRN.placeHolderList[440]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.rotateZ" 
		"rigwRN.placeHolderList[441]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.scaleX" 
		"rigwRN.placeHolderList[442]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.scaleY" 
		"rigwRN.placeHolderList[443]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff57.scaleZ" 
		"rigwRN.placeHolderList[444]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.translateX" 
		"rigwRN.placeHolderList[445]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.translateY" 
		"rigwRN.placeHolderList[446]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.translateZ" 
		"rigwRN.placeHolderList[447]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.visibility" 
		"rigwRN.placeHolderList[448]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.rotateX" 
		"rigwRN.placeHolderList[449]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.rotateY" 
		"rigwRN.placeHolderList[450]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.rotateZ" 
		"rigwRN.placeHolderList[451]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.scaleX" 
		"rigwRN.placeHolderList[452]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.scaleY" 
		"rigwRN.placeHolderList[453]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff56.scaleZ" 
		"rigwRN.placeHolderList[454]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.translateX" 
		"rigwRN.placeHolderList[455]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.translateY" 
		"rigwRN.placeHolderList[456]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.translateZ" 
		"rigwRN.placeHolderList[457]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.rotateX" 
		"rigwRN.placeHolderList[458]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.rotateY" 
		"rigwRN.placeHolderList[459]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.rotateZ" 
		"rigwRN.placeHolderList[460]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.visibility" 
		"rigwRN.placeHolderList[461]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.scaleX" 
		"rigwRN.placeHolderList[462]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.scaleY" 
		"rigwRN.placeHolderList[463]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackL|rigw:tracks:stuff69.scaleZ" 
		"rigwRN.placeHolderList[464]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.translateX" 
		"rigwRN.placeHolderList[465]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.translateY" 
		"rigwRN.placeHolderList[466]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.translateZ" 
		"rigwRN.placeHolderList[467]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.rotateX" 
		"rigwRN.placeHolderList[468]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.rotateY" 
		"rigwRN.placeHolderList[469]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.rotateZ" 
		"rigwRN.placeHolderList[470]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.visibility" 
		"rigwRN.placeHolderList[471]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.scaleX" 
		"rigwRN.placeHolderList[472]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.scaleY" 
		"rigwRN.placeHolderList[473]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff94.scaleZ" 
		"rigwRN.placeHolderList[474]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.translateX" 
		"rigwRN.placeHolderList[475]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.translateY" 
		"rigwRN.placeHolderList[476]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.translateZ" 
		"rigwRN.placeHolderList[477]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.rotateX" 
		"rigwRN.placeHolderList[478]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.rotateY" 
		"rigwRN.placeHolderList[479]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.rotateZ" 
		"rigwRN.placeHolderList[480]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.visibility" 
		"rigwRN.placeHolderList[481]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.scaleX" 
		"rigwRN.placeHolderList[482]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.scaleY" 
		"rigwRN.placeHolderList[483]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff93.scaleZ" 
		"rigwRN.placeHolderList[484]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.translateX" 
		"rigwRN.placeHolderList[485]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.translateY" 
		"rigwRN.placeHolderList[486]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.translateZ" 
		"rigwRN.placeHolderList[487]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.visibility" 
		"rigwRN.placeHolderList[488]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.rotateX" 
		"rigwRN.placeHolderList[489]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.rotateY" 
		"rigwRN.placeHolderList[490]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.rotateZ" 
		"rigwRN.placeHolderList[491]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.scaleX" 
		"rigwRN.placeHolderList[492]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.scaleY" 
		"rigwRN.placeHolderList[493]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff92.scaleZ" 
		"rigwRN.placeHolderList[494]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.translateX" 
		"rigwRN.placeHolderList[495]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.translateY" 
		"rigwRN.placeHolderList[496]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.translateZ" 
		"rigwRN.placeHolderList[497]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.visibility" 
		"rigwRN.placeHolderList[498]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.rotateX" 
		"rigwRN.placeHolderList[499]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.rotateY" 
		"rigwRN.placeHolderList[500]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.rotateZ" 
		"rigwRN.placeHolderList[501]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.scaleX" 
		"rigwRN.placeHolderList[502]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.scaleY" 
		"rigwRN.placeHolderList[503]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff91.scaleZ" 
		"rigwRN.placeHolderList[504]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.translateX" 
		"rigwRN.placeHolderList[505]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.translateY" 
		"rigwRN.placeHolderList[506]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.translateZ" 
		"rigwRN.placeHolderList[507]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.visibility" 
		"rigwRN.placeHolderList[508]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.rotateX" 
		"rigwRN.placeHolderList[509]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.rotateY" 
		"rigwRN.placeHolderList[510]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.rotateZ" 
		"rigwRN.placeHolderList[511]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.scaleX" 
		"rigwRN.placeHolderList[512]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.scaleY" 
		"rigwRN.placeHolderList[513]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff85.scaleZ" 
		"rigwRN.placeHolderList[514]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.translateX" 
		"rigwRN.placeHolderList[515]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.translateY" 
		"rigwRN.placeHolderList[516]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.translateZ" 
		"rigwRN.placeHolderList[517]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.visibility" 
		"rigwRN.placeHolderList[518]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.rotateX" 
		"rigwRN.placeHolderList[519]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.rotateY" 
		"rigwRN.placeHolderList[520]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.rotateZ" 
		"rigwRN.placeHolderList[521]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.scaleX" 
		"rigwRN.placeHolderList[522]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.scaleY" 
		"rigwRN.placeHolderList[523]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff86.scaleZ" 
		"rigwRN.placeHolderList[524]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.translateX" 
		"rigwRN.placeHolderList[525]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.translateY" 
		"rigwRN.placeHolderList[526]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.translateZ" 
		"rigwRN.placeHolderList[527]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.visibility" 
		"rigwRN.placeHolderList[528]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.rotateX" 
		"rigwRN.placeHolderList[529]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.rotateY" 
		"rigwRN.placeHolderList[530]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.rotateZ" 
		"rigwRN.placeHolderList[531]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.scaleX" 
		"rigwRN.placeHolderList[532]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.scaleY" 
		"rigwRN.placeHolderList[533]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff87.scaleZ" 
		"rigwRN.placeHolderList[534]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.translateX" 
		"rigwRN.placeHolderList[535]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.translateY" 
		"rigwRN.placeHolderList[536]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.translateZ" 
		"rigwRN.placeHolderList[537]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.visibility" 
		"rigwRN.placeHolderList[538]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.rotateX" 
		"rigwRN.placeHolderList[539]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.rotateY" 
		"rigwRN.placeHolderList[540]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.rotateZ" 
		"rigwRN.placeHolderList[541]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.scaleX" 
		"rigwRN.placeHolderList[542]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.scaleY" 
		"rigwRN.placeHolderList[543]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff88.scaleZ" 
		"rigwRN.placeHolderList[544]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.translateX" 
		"rigwRN.placeHolderList[545]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.translateY" 
		"rigwRN.placeHolderList[546]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.translateZ" 
		"rigwRN.placeHolderList[547]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.visibility" 
		"rigwRN.placeHolderList[548]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.rotateX" 
		"rigwRN.placeHolderList[549]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.rotateY" 
		"rigwRN.placeHolderList[550]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.rotateZ" 
		"rigwRN.placeHolderList[551]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.scaleX" 
		"rigwRN.placeHolderList[552]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.scaleY" 
		"rigwRN.placeHolderList[553]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff89.scaleZ" 
		"rigwRN.placeHolderList[554]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.translateX" 
		"rigwRN.placeHolderList[555]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.translateY" 
		"rigwRN.placeHolderList[556]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.translateZ" 
		"rigwRN.placeHolderList[557]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.visibility" 
		"rigwRN.placeHolderList[558]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.rotateX" 
		"rigwRN.placeHolderList[559]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.rotateY" 
		"rigwRN.placeHolderList[560]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.rotateZ" 
		"rigwRN.placeHolderList[561]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.scaleX" 
		"rigwRN.placeHolderList[562]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.scaleY" 
		"rigwRN.placeHolderList[563]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff90.scaleZ" 
		"rigwRN.placeHolderList[564]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.translateX" 
		"rigwRN.placeHolderList[565]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.translateY" 
		"rigwRN.placeHolderList[566]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.translateZ" 
		"rigwRN.placeHolderList[567]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.visibility" 
		"rigwRN.placeHolderList[568]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.rotateX" 
		"rigwRN.placeHolderList[569]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.rotateY" 
		"rigwRN.placeHolderList[570]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.rotateZ" 
		"rigwRN.placeHolderList[571]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.scaleX" 
		"rigwRN.placeHolderList[572]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.scaleY" 
		"rigwRN.placeHolderList[573]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff95.scaleZ" 
		"rigwRN.placeHolderList[574]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.translateX" 
		"rigwRN.placeHolderList[575]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.translateY" 
		"rigwRN.placeHolderList[576]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.translateZ" 
		"rigwRN.placeHolderList[577]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.visibility" 
		"rigwRN.placeHolderList[578]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.rotateX" 
		"rigwRN.placeHolderList[579]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.rotateY" 
		"rigwRN.placeHolderList[580]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.rotateZ" 
		"rigwRN.placeHolderList[581]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.scaleX" 
		"rigwRN.placeHolderList[582]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.scaleY" 
		"rigwRN.placeHolderList[583]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff96.scaleZ" 
		"rigwRN.placeHolderList[584]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.translateX" 
		"rigwRN.placeHolderList[585]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.translateY" 
		"rigwRN.placeHolderList[586]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.translateZ" 
		"rigwRN.placeHolderList[587]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.visibility" 
		"rigwRN.placeHolderList[588]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.rotateX" 
		"rigwRN.placeHolderList[589]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.rotateY" 
		"rigwRN.placeHolderList[590]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.rotateZ" 
		"rigwRN.placeHolderList[591]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.scaleX" 
		"rigwRN.placeHolderList[592]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.scaleY" 
		"rigwRN.placeHolderList[593]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff97.scaleZ" 
		"rigwRN.placeHolderList[594]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.translateX" 
		"rigwRN.placeHolderList[595]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.translateY" 
		"rigwRN.placeHolderList[596]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.translateZ" 
		"rigwRN.placeHolderList[597]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.visibility" 
		"rigwRN.placeHolderList[598]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.rotateX" 
		"rigwRN.placeHolderList[599]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.rotateY" 
		"rigwRN.placeHolderList[600]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.rotateZ" 
		"rigwRN.placeHolderList[601]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.scaleX" 
		"rigwRN.placeHolderList[602]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.scaleY" 
		"rigwRN.placeHolderList[603]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff98.scaleZ" 
		"rigwRN.placeHolderList[604]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.translateX" 
		"rigwRN.placeHolderList[605]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.translateY" 
		"rigwRN.placeHolderList[606]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.translateZ" 
		"rigwRN.placeHolderList[607]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.rotateX" 
		"rigwRN.placeHolderList[608]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.rotateY" 
		"rigwRN.placeHolderList[609]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.rotateZ" 
		"rigwRN.placeHolderList[610]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.visibility" 
		"rigwRN.placeHolderList[611]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.scaleX" 
		"rigwRN.placeHolderList[612]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.scaleY" 
		"rigwRN.placeHolderList[613]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff111.scaleZ" 
		"rigwRN.placeHolderList[614]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.translateX" 
		"rigwRN.placeHolderList[615]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.translateY" 
		"rigwRN.placeHolderList[616]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.translateZ" 
		"rigwRN.placeHolderList[617]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.rotateX" 
		"rigwRN.placeHolderList[618]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.rotateY" 
		"rigwRN.placeHolderList[619]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.rotateZ" 
		"rigwRN.placeHolderList[620]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.visibility" 
		"rigwRN.placeHolderList[621]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.scaleX" 
		"rigwRN.placeHolderList[622]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.scaleY" 
		"rigwRN.placeHolderList[623]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff110.scaleZ" 
		"rigwRN.placeHolderList[624]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.translateX" 
		"rigwRN.placeHolderList[625]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.translateY" 
		"rigwRN.placeHolderList[626]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.translateZ" 
		"rigwRN.placeHolderList[627]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.visibility" 
		"rigwRN.placeHolderList[628]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.rotateX" 
		"rigwRN.placeHolderList[629]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.rotateY" 
		"rigwRN.placeHolderList[630]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.rotateZ" 
		"rigwRN.placeHolderList[631]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.scaleX" 
		"rigwRN.placeHolderList[632]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.scaleY" 
		"rigwRN.placeHolderList[633]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff109.scaleZ" 
		"rigwRN.placeHolderList[634]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.translateX" 
		"rigwRN.placeHolderList[635]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.translateY" 
		"rigwRN.placeHolderList[636]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.translateZ" 
		"rigwRN.placeHolderList[637]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.visibility" 
		"rigwRN.placeHolderList[638]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.rotateX" 
		"rigwRN.placeHolderList[639]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.rotateY" 
		"rigwRN.placeHolderList[640]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.rotateZ" 
		"rigwRN.placeHolderList[641]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.scaleX" 
		"rigwRN.placeHolderList[642]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.scaleY" 
		"rigwRN.placeHolderList[643]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff108.scaleZ" 
		"rigwRN.placeHolderList[644]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.translateX" 
		"rigwRN.placeHolderList[645]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.translateY" 
		"rigwRN.placeHolderList[646]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.translateZ" 
		"rigwRN.placeHolderList[647]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.visibility" 
		"rigwRN.placeHolderList[648]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.rotateX" 
		"rigwRN.placeHolderList[649]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.rotateY" 
		"rigwRN.placeHolderList[650]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.rotateZ" 
		"rigwRN.placeHolderList[651]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.scaleX" 
		"rigwRN.placeHolderList[652]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.scaleY" 
		"rigwRN.placeHolderList[653]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff107.scaleZ" 
		"rigwRN.placeHolderList[654]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.translateX" 
		"rigwRN.placeHolderList[655]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.translateY" 
		"rigwRN.placeHolderList[656]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.translateZ" 
		"rigwRN.placeHolderList[657]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.visibility" 
		"rigwRN.placeHolderList[658]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.rotateX" 
		"rigwRN.placeHolderList[659]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.rotateY" 
		"rigwRN.placeHolderList[660]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.rotateZ" 
		"rigwRN.placeHolderList[661]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.scaleX" 
		"rigwRN.placeHolderList[662]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.scaleY" 
		"rigwRN.placeHolderList[663]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff106.scaleZ" 
		"rigwRN.placeHolderList[664]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.translateX" 
		"rigwRN.placeHolderList[665]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.translateY" 
		"rigwRN.placeHolderList[666]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.translateZ" 
		"rigwRN.placeHolderList[667]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.visibility" 
		"rigwRN.placeHolderList[668]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.rotateX" 
		"rigwRN.placeHolderList[669]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.rotateY" 
		"rigwRN.placeHolderList[670]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.rotateZ" 
		"rigwRN.placeHolderList[671]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.scaleX" 
		"rigwRN.placeHolderList[672]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.scaleY" 
		"rigwRN.placeHolderList[673]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff105.scaleZ" 
		"rigwRN.placeHolderList[674]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.translateX" 
		"rigwRN.placeHolderList[675]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.translateY" 
		"rigwRN.placeHolderList[676]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.translateZ" 
		"rigwRN.placeHolderList[677]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.visibility" 
		"rigwRN.placeHolderList[678]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.rotateX" 
		"rigwRN.placeHolderList[679]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.rotateY" 
		"rigwRN.placeHolderList[680]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.rotateZ" 
		"rigwRN.placeHolderList[681]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.scaleX" 
		"rigwRN.placeHolderList[682]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.scaleY" 
		"rigwRN.placeHolderList[683]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff104.scaleZ" 
		"rigwRN.placeHolderList[684]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.translateX" 
		"rigwRN.placeHolderList[685]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.translateY" 
		"rigwRN.placeHolderList[686]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.translateZ" 
		"rigwRN.placeHolderList[687]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.visibility" 
		"rigwRN.placeHolderList[688]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.rotateX" 
		"rigwRN.placeHolderList[689]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.rotateY" 
		"rigwRN.placeHolderList[690]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.rotateZ" 
		"rigwRN.placeHolderList[691]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.scaleX" 
		"rigwRN.placeHolderList[692]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.scaleY" 
		"rigwRN.placeHolderList[693]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff103.scaleZ" 
		"rigwRN.placeHolderList[694]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.translateX" 
		"rigwRN.placeHolderList[695]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.translateY" 
		"rigwRN.placeHolderList[696]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.translateZ" 
		"rigwRN.placeHolderList[697]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.visibility" 
		"rigwRN.placeHolderList[698]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.rotateX" 
		"rigwRN.placeHolderList[699]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.rotateY" 
		"rigwRN.placeHolderList[700]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.rotateZ" 
		"rigwRN.placeHolderList[701]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.scaleX" 
		"rigwRN.placeHolderList[702]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.scaleY" 
		"rigwRN.placeHolderList[703]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff102.scaleZ" 
		"rigwRN.placeHolderList[704]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.translateX" 
		"rigwRN.placeHolderList[705]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.translateY" 
		"rigwRN.placeHolderList[706]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.translateZ" 
		"rigwRN.placeHolderList[707]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.visibility" 
		"rigwRN.placeHolderList[708]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.rotateX" 
		"rigwRN.placeHolderList[709]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.rotateY" 
		"rigwRN.placeHolderList[710]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.rotateZ" 
		"rigwRN.placeHolderList[711]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.scaleX" 
		"rigwRN.placeHolderList[712]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.scaleY" 
		"rigwRN.placeHolderList[713]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff101.scaleZ" 
		"rigwRN.placeHolderList[714]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.translateX" 
		"rigwRN.placeHolderList[715]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.translateY" 
		"rigwRN.placeHolderList[716]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.translateZ" 
		"rigwRN.placeHolderList[717]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.visibility" 
		"rigwRN.placeHolderList[718]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.rotateX" 
		"rigwRN.placeHolderList[719]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.rotateY" 
		"rigwRN.placeHolderList[720]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.rotateZ" 
		"rigwRN.placeHolderList[721]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.scaleX" 
		"rigwRN.placeHolderList[722]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.scaleY" 
		"rigwRN.placeHolderList[723]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff100.scaleZ" 
		"rigwRN.placeHolderList[724]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.translateX" 
		"rigwRN.placeHolderList[725]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.translateY" 
		"rigwRN.placeHolderList[726]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.translateZ" 
		"rigwRN.placeHolderList[727]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.visibility" 
		"rigwRN.placeHolderList[728]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.rotateX" 
		"rigwRN.placeHolderList[729]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.rotateY" 
		"rigwRN.placeHolderList[730]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.rotateZ" 
		"rigwRN.placeHolderList[731]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.scaleX" 
		"rigwRN.placeHolderList[732]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.scaleY" 
		"rigwRN.placeHolderList[733]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff99.scaleZ" 
		"rigwRN.placeHolderList[734]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.translateX" 
		"rigwRN.placeHolderList[735]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.translateY" 
		"rigwRN.placeHolderList[736]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.translateZ" 
		"rigwRN.placeHolderList[737]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.rotateX" 
		"rigwRN.placeHolderList[738]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.rotateY" 
		"rigwRN.placeHolderList[739]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.rotateZ" 
		"rigwRN.placeHolderList[740]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.visibility" 
		"rigwRN.placeHolderList[741]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.scaleX" 
		"rigwRN.placeHolderList[742]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.scaleY" 
		"rigwRN.placeHolderList[743]" ""
		5 4 "rigwRN" "|rigw:rigRNfosterParent1|rigw:tracks:TrackR|rigw:tracks:stuff112.scaleZ" 
		"rigwRN.placeHolderList[744]" ""
		"rigw:rigRN" 138
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translate" " -type \"double3\" -0.00483391 -0.666608 -0.036963"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 3.86163 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M" 
		"rotate" " -type \"double3\" -1.594217 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotate" " -type \"double3\" 0 0 4.574247"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotate" " -type \"double3\" 0 0 11.69405"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 5.254151"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" 0 -0.0244471 0.000509844"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" -1.194727 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0.301832 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateX" 
		"rigwRN.placeHolderList[103]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateY" 
		"rigwRN.placeHolderList[104]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel1_R|rigw:rig:FKExtraWheel1_R|rigw:rig:FKWheel1_R.rotateZ" 
		"rigwRN.placeHolderList[105]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateX" 
		"rigwRN.placeHolderList[106]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateY" 
		"rigwRN.placeHolderList[107]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel2_R|rigw:rig:FKExtraWheel2_R|rigw:rig:FKWheel2_R.rotateZ" 
		"rigwRN.placeHolderList[108]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateX" 
		"rigwRN.placeHolderList[109]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateY" 
		"rigwRN.placeHolderList[110]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel3_R|rigw:rig:FKExtraWheel3_R|rigw:rig:FKWheel3_R.rotateZ" 
		"rigwRN.placeHolderList[111]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateX" 
		"rigwRN.placeHolderList[112]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateY" 
		"rigwRN.placeHolderList[113]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel4_R|rigw:rig:FKExtraWheel4_R|rigw:rig:FKWheel4_R.rotateZ" 
		"rigwRN.placeHolderList[114]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R.rotateX" 
		"rigwRN.placeHolderList[115]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R.rotateY" 
		"rigwRN.placeHolderList[116]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel5_R|rigw:rig:FKExtraWheel5_R|rigw:rig:FKWheel5_R.rotateZ" 
		"rigwRN.placeHolderList[117]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R.rotateX" 
		"rigwRN.placeHolderList[118]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R.rotateY" 
		"rigwRN.placeHolderList[119]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_R|rigw:rig:FKOffsetWheel6_R|rigw:rig:FKExtraWheel6_R|rigw:rig:FKWheel6_R.rotateZ" 
		"rigwRN.placeHolderList[120]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateX" 
		"rigwRN.placeHolderList[121]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateY" 
		"rigwRN.placeHolderList[122]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel1_L|rigw:rig:FKExtraWheel1_L|rigw:rig:FKWheel1_L.rotateZ" 
		"rigwRN.placeHolderList[123]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateX" 
		"rigwRN.placeHolderList[124]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateY" 
		"rigwRN.placeHolderList[125]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel2_L|rigw:rig:FKExtraWheel2_L|rigw:rig:FKWheel2_L.rotateZ" 
		"rigwRN.placeHolderList[126]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateX" 
		"rigwRN.placeHolderList[127]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateY" 
		"rigwRN.placeHolderList[128]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel3_L|rigw:rig:FKExtraWheel3_L|rigw:rig:FKWheel3_L.rotateZ" 
		"rigwRN.placeHolderList[129]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateX" 
		"rigwRN.placeHolderList[130]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateY" 
		"rigwRN.placeHolderList[131]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel4_L|rigw:rig:FKExtraWheel4_L|rigw:rig:FKWheel4_L.rotateZ" 
		"rigwRN.placeHolderList[132]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L.rotateX" 
		"rigwRN.placeHolderList[133]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L.rotateY" 
		"rigwRN.placeHolderList[134]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel5_L|rigw:rig:FKExtraWheel5_L|rigw:rig:FKWheel5_L.rotateZ" 
		"rigwRN.placeHolderList[135]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L.rotateX" 
		"rigwRN.placeHolderList[136]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L.rotateY" 
		"rigwRN.placeHolderList[137]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToTrackBottom_L|rigw:rig:FKOffsetWheel6_L|rigw:rig:FKExtraWheel6_L|rigw:rig:FKWheel6_L.rotateZ" 
		"rigwRN.placeHolderList[138]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[139]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[140]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[141]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[142]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[143]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[144]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[145]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M.rotateX" 
		"rigwRN.placeHolderList[146]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M.rotateY" 
		"rigwRN.placeHolderList[147]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetWeapon_M|rigw:rig:FKExtraWeapon_M|rigw:rig:FKWeapon_M.rotateZ" 
		"rigwRN.placeHolderList[148]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.Global" 
		"rigwRN.placeHolderList[149]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[150]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[151]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[152]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateX" 
		"rigwRN.placeHolderList[153]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateY" 
		"rigwRN.placeHolderList[154]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKGlobalStaticShoulder_R|rigw:rig:FKGlobalShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateZ" 
		"rigwRN.placeHolderList[155]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.Global" 
		"rigwRN.placeHolderList[156]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateX" 
		"rigwRN.placeHolderList[157]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateY" 
		"rigwRN.placeHolderList[158]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateZ" 
		"rigwRN.placeHolderList[159]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[160]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[161]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetBody_M|rigw:rig:FKExtraBody_M|rigw:rig:FKBody_M|rigw:rig:FKXBody_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKGlobalStaticShoulder_L|rigw:rig:FKGlobalShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[162]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[163]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[164]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[165]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[166]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[167]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[168]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[169]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[170]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[171]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[172]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[173]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[174]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[175]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[176]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[177]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[178]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[179]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[180]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[181]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[182]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[183]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[184]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
	setAttr -s 3 ".opt";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -sn "imrVerbosity" -ln "imrVerbosity" -dv 3 -at "short";
	addAttr -ci true -sn "imrThreads" -ln "imrThreads" -dv 1 -at "short";
	addAttr -ci true -sn "imrThreadsAdjust" -ln "imrThreadsAdjust" -dv 1 -min 0 -max 
		1 -at "bool";
	addAttr -ci true -sn "imrTaskOrder" -ln "imrTaskOrder" -dv 1 -at "short";
	addAttr -ci true -sn "imrTaskSize" -ln "imrTaskSize" -at "short";
	addAttr -ci true -sn "imrTaskAdjust" -ln "imrTaskAdjust" -dv 1 -at "short";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
	setAttr ".imrThreads" 4;
	setAttr ".imrThreadsAdjust" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".fil" 2;
	setAttr ".filw" 0.83333331346511841;
	setAttr ".filh" 0.83333331346511841;
	setAttr ".maxr" 2;
	setAttr ".gi" yes;
	setAttr ".fg" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 8 -ast 1 -aet 8 ";
	setAttr ".st" 6;
createNode animCurveTU -n "rigw:TrackR_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:TrackR_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 8.9064141376358492;
createNode animCurveTL -n "rigw:TrackR_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:TrackR_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:TrackR_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 360;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  0.026516497135162354;
	setAttr -s 2 ".kiy[1]"  0.99964839220046997;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "rigw:TrackR_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:TrackR_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:TrackR_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:TrackR_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:TrackR_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:TrackL_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "rigw:TrackL_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 16.706767680580423;
createNode animCurveTL -n "rigw:TrackL_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:TrackL_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:TrackL_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 360;
	setAttr -s 2 ".kit[1]"  1;
	setAttr -s 2 ".kot[1]"  1;
	setAttr -s 2 ".kix[1]"  0.026516497135162354;
	setAttr -s 2 ".kiy[1]"  0.99964839220046997;
	setAttr -s 2 ".kox[1]"  1;
	setAttr -s 2 ".koy[1]"  0;
createNode animCurveTA -n "rigw:TrackL_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:TrackL_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:TrackL_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:TrackL_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:TrackL_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -0.0048339080945804507 3 -0.01557710422566121
		 5 0.0060956785825503398 7 -0.0048339080945804507 9 -0.0048339080945804507;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -0.66660761577746763 3 -0.48298337553670206
		 5 -0.53616676969926935 7 -0.5972145202999215 9 -0.66660761577746763;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -0.036963017773342301 3 0.021630684187047729
		 5 0.0029217579849616171 7 -0.03227895614069573 9 -0.036963017773342301;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 3.8616301670575943 3 2.718597508627103
		 5 0.01509857856062566 7 3.8620940719902226 9 3.8616301670575943;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 3 -0.065482134508763459 5 0.96460016136629545
		 7 1.3911227457979602 9 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 3 -1.0451107590942004 5 0.83387017189323331
		 7 -1.5598716848806895 9 0;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 9 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0.30183191868897669 3 -0.93601856698271646
		 5 0.30174057555540429 7 -1.8366447549374298 9 0.30183191868897669;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 9 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 9 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 9 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 3 0.045003697096098527 5 -0.022472899136542154
		 7 0.044714201669422859 9 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 3 -0.00073527307362204817 5 -0.00060449223243072027
		 7 -0.0014338271551963947 9 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 9 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 9 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 9 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 7 1 9 1;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 7 1 9 1;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -1.1947266344028675 3 0.10405552709388066
		 5 -1.0980817748773417 7 0.21422273527644703 9 -1.1947266344028675;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -0.02444714083295985 3 0.055308048871566477
		 5 -0.012089008165317772 7 0.019340697247046908 9 -0.02444714083295985;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0.00050984356015753435 3 -0.0011534458256169018
		 5 0.00016949633803691909 7 0.00017966113906603135 9 0.00050984356015753435;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 9 1;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 9 1;
createNode animCurveTA -n "rigw:rig:FKWeapon_M_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  -6 0 -4 -3.6596030893819216 -2 -0.72810455383507666
		 0 -3.1884341585173268 2 0 4 -3.6596030893819216 6 -0.72810455383507666 8 -3.1884341585173268
		 10 0;
createNode animCurveTA -n "rigw:rig:FKWeapon_M_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -6 0 2 0 10 0;
createNode animCurveTA -n "rigw:rig:FKWeapon_M_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -6 0 2 0 10 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 3 7.702822287429318 5 -8.0946073412194846
		 7 9.148494250876956 9 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -6 0 2 0 10 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -6 0 2 0 10 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  -6 0 -4 7.702822287429318 -2 -8.0946073412194846
		 0 9.148494250876956 2 0 4 7.702822287429318 6 -8.0946073412194846 8 9.148494250876956
		 10 0;
createNode animCurveTU -n "rigw:rig:FKShoulder_R_Global";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 10 9 10;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 11.694050301955642 3 0.29437399502064776
		 5 6.3072339787111877 7 -1.1857482642003505 9 11.694050301955642;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -6 0 2 0 10 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -6 0 2 0 10 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  -6 11.694050301955642 -4 0.29437399502064776
		 -2 6.3072339787111877 0 -1.1857482642003505 2 11.694050301955642 4 0.29437399502064776
		 6 6.3072339787111877 8 -1.1857482642003505 10 11.694050301955642;
createNode animCurveTU -n "rigw:rig:FKShoulder_L_Global";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 10 9 10;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel1_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel2_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel3_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel4_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel5_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel5_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel5_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel6_R_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel6_R_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel6_R_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel1_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel2_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel3_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel4_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel5_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel5_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel5_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel6_L_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWheel6_L_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 9 -360;
createNode animCurveTA -n "rigw:rig:FKWheel6_L_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:tracks:stuff94_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff94_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.9365533838983904 2 -3.9411067212681754
		 3 -3.9365533838983904 4 -3.9411067212681754 5 -3.9365533838983904 6 -3.9411067212681754
		 7 -3.9365533838983904 8 -3.9411067212681754;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.99079877138137817 0.99079877138137817 
		0.99079877138137817 0.99079877138137817 0.99079877138137817;
	setAttr -s 8 ".kiy[3:7]"  -0.13534322381019592 0.13534323871135712 
		-0.13534322381019592 0.13534323871135712 -0.13534322381019592;
	setAttr -s 8 ".kox[3:7]"  0.99079877138137817 0.99079877138137817 
		0.99079877138137817 0.99079877138137817 0.99079877138137817;
	setAttr -s 8 ".koy[3:7]"  0.13534323871135712 -0.13534323871135712 
		0.13534323871135712 -0.13534317910671234 0.13534323871135712;
createNode animCurveTL -n "rigw:tracks:stuff94_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.8362001562078627 2 0.39293475407910183
		 3 0.8362001562078627 4 0.39293475407910183 5 0.8362001562078627 6 0.39293475407910183
		 7 0.8362001562078627 8 0.39293475407910183;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.07498776912689209 0.074987761676311493 
		0.07498776912689209 0.074987761676311493 0.07498776912689209;
	setAttr -s 8 ".kiy[3:7]"  -0.99718445539474487 0.99718445539474487 
		-0.99718445539474487 0.99718445539474487 -0.99718445539474487;
	setAttr -s 8 ".kox[3:7]"  0.074987761676311493 0.074987761676311493 
		0.074987761676311493 0.074987791478633881 0.074987761676311493;
	setAttr -s 8 ".koy[3:7]"  0.99718445539474487 -0.99718445539474487 
		0.99718445539474487 -0.99718445539474487 0.99718445539474487;
createNode animCurveTL -n "rigw:tracks:stuff94_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 6.0352838681597607 2 5.9984274887857776
		 3 6.0352838681597607 4 5.9984274887857776 5 6.0352838681597607 6 5.9984274887857776
		 7 6.0352838681597607 8 5.9984274887857776;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.67077046632766724 0.67077034711837769 
		0.67077046632766724 0.67077034711837769 0.67077046632766724;
	setAttr -s 8 ".kiy[3:7]"  -0.7416650652885437 0.7416650652885437 
		-0.7416650652885437 0.7416650652885437 -0.7416650652885437;
	setAttr -s 8 ".kox[3:7]"  0.67077034711837769 0.67077034711837769 
		0.67077034711837769 0.67077058553695679 0.67077034711837769;
	setAttr -s 8 ".koy[3:7]"  0.7416650652885437 -0.7416650652885437 
		0.7416650652885437 -0.74166494607925415 0.7416650652885437;
createNode animCurveTA -n "rigw:tracks:stuff94_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 85.247032360988754 2 58.569810591330373
		 3 85.247032360988754 4 58.569810591330373 5 85.247032360988754 6 58.569810591330373
		 7 85.247032360988754 8 58.569810591330373;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.071408629417419434 0.071408621966838837 
		0.071408629417419434 0.071408621966838837 0.071408629417419434;
	setAttr -s 8 ".kiy[3:7]"  -0.99744719266891479 0.99744719266891479 
		-0.99744719266891479 0.99744719266891479 -0.99744719266891479;
	setAttr -s 8 ".kox[3:7]"  0.071408621966838837 0.071408621966838837 
		0.071408621966838837 0.071408659219741821 0.071408621966838837;
	setAttr -s 8 ".koy[3:7]"  0.99744719266891479 -0.99744719266891479 
		0.99744719266891479 -0.99744719266891479 0.99744719266891479;
createNode animCurveTA -n "rigw:tracks:stuff94_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.46022316200377822 3 0.46022316200377822
		 5 0.46022316200377822 7 0.46022316200377822;
createNode animCurveTA -n "rigw:tracks:stuff94_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 179.37319838705099 3 179.37319838705099
		 5 179.37319838705099 7 179.37319838705099;
createNode animCurveTU -n "rigw:tracks:stuff94_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff94_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff94_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1.0000000000000002 3 -1.0000000000000002
		 5 -1.0000000000000002 7 -1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff93_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff93_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.9248102972057626 2 -3.9300451157807599
		 3 -3.9248102972057626 4 -3.9300451157807599 5 -3.9248102972057626 6 -3.9300451157807599
		 7 -3.9248102972057626 8 -3.9300451157807599;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.98789191246032715 0.98789191246032715 
		0.98789191246032715 0.98789191246032715 0.98789191246032715;
	setAttr -s 8 ".kiy[3:7]"  -0.15514303743839264 0.15514305233955383 
		-0.15514303743839264 0.15514305233955383 -0.15514303743839264;
	setAttr -s 8 ".kox[3:7]"  0.98789191246032715 0.98789191246032715 
		0.98789191246032715 0.9878920316696167 0.98789191246032715;
	setAttr -s 8 ".koy[3:7]"  0.15514305233955383 -0.15514305233955383 
		0.15514305233955383 -0.15514300763607025 0.15514305233955383;
createNode animCurveTL -n "rigw:tracks:stuff93_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.5198204208337878 2 1.2700090059012052
		 3 1.5198204208337878 4 1.2700090059012052 5 1.5198204208337878 6 1.2700090059012052
		 7 1.5198204208337878 8 1.2700090059012052;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.13226175308227539 0.1322617381811142 
		0.13226175308227539 0.1322617381811142 0.13226175308227539;
	setAttr -s 8 ".kiy[3:7]"  -0.99121475219726562 0.99121475219726562 
		-0.99121475219726562 0.99121475219726562 -0.99121475219726562;
	setAttr -s 8 ".kox[3:7]"  0.1322617381811142 0.1322617381811142 0.1322617381811142 
		0.13226179778575897 0.1322617381811142;
	setAttr -s 8 ".koy[3:7]"  0.99121475219726562 -0.99121475219726562 
		0.99121475219726562 -0.99121475219726562 0.99121475219726562;
createNode animCurveTL -n "rigw:tracks:stuff93_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 5.5044586659036074 2 5.8159008485221833
		 3 5.5044586659036074 4 5.8159008485221833 5 5.5044586659036074 6 5.8159008485221833
		 7 5.5044586659036074 8 5.8159008485221833;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.10642116516828537 0.10642115026712418 
		0.10642116516828537 0.10642115026712418 0.10642116516828537;
	setAttr -s 8 ".kiy[3:7]"  0.99432116746902466 -0.99432116746902466 
		0.99432116746902466 -0.99432116746902466 0.99432116746902466;
	setAttr -s 8 ".kox[3:7]"  0.10642115026712418 0.10642115026712418 
		0.10642115026712418 0.10642120242118835 0.10642115026712418;
	setAttr -s 8 ".koy[3:7]"  -0.99432116746902466 0.99432116746902466 
		-0.99432116746902466 0.99432116746902466 -0.99432116746902466;
createNode animCurveTA -n "rigw:tracks:stuff93_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 155.4457850056454 2 124.91321876672632
		 3 155.4457850056454 4 124.91321876672632 5 155.4457850056454 6 124.91321876672632
		 7 155.4457850056454 8 124.91321876672632;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.062429532408714294 0.062429524958133698 
		0.062429532408714294 0.062429524958133698 0.062429532408714294;
	setAttr -s 8 ".kiy[3:7]"  -0.99804931879043579 0.99804931879043579 
		-0.99804931879043579 0.99804931879043579 -0.99804931879043579;
	setAttr -s 8 ".kox[3:7]"  0.062429524958133698 0.062429524958133698 
		0.062429524958133698 0.062429551035165787 0.062429524958133698;
	setAttr -s 8 ".koy[3:7]"  0.99804931879043579 -0.99804931879043579 
		0.99804931879043579 -0.99804931879043579 0.99804931879043579;
createNode animCurveTA -n "rigw:tracks:stuff93_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.46022316200377744 3 0.46022316200377744
		 5 0.46022316200377744 7 0.46022316200377744;
createNode animCurveTA -n "rigw:tracks:stuff93_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 179.37319838705099 3 179.37319838705099
		 5 179.37319838705099 7 179.37319838705099;
createNode animCurveTU -n "rigw:tracks:stuff93_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff93_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff93_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff92_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff92_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.9125030062156698 2 -3.9173046540857479
		 3 -3.9125030062156698 4 -3.9173046540857479 5 -3.9125030062156698 6 -3.9173046540857479
		 7 -3.9125030062156698 8 -3.9173046540857479;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.9897836446762085 0.98978352546691895 
		0.9897836446762085 0.98978352546691895 0.9897836446762085;
	setAttr -s 8 ".kiy[3:7]"  -0.14257776737213135 0.14257776737213135 
		-0.14257776737213135 0.14257776737213135 -0.14257776737213135;
	setAttr -s 8 ".kox[3:7]"  0.98978352546691895 0.98978352546691895 
		0.98978352546691895 0.9897836446762085 0.98978352546691895;
	setAttr -s 8 ".koy[3:7]"  0.14257776737213135 -0.14257776737213135 
		0.14257776737213135 -0.14257772266864777 0.14257776737213135;
createNode animCurveTL -n "rigw:tracks:stuff92_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.9512480591474912 2 1.8311110188233113
		 3 1.9512480591474912 4 1.8311110188233113 5 1.9512480591474912 6 1.8311110188233113
		 7 1.9512480591474912 8 1.8311110188233113;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.2673603892326355 0.26736035943031311 
		0.2673603892326355 0.26736035943031311 0.2673603892326355;
	setAttr -s 8 ".kiy[3:7]"  -0.96359658241271973 0.96359658241271973 
		-0.96359658241271973 0.96359658241271973 -0.96359658241271973;
	setAttr -s 8 ".kox[3:7]"  0.26736035943031311 0.26736035943031311 
		0.26736035943031311 0.26736047863960266 0.26736035943031311;
	setAttr -s 8 ".koy[3:7]"  0.96359658241271973 -0.96359658241271973 
		0.96359658241271973 -0.96359658241271973 0.96359658241271973;
createNode animCurveTL -n "rigw:tracks:stuff92_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 4.5599383184532076 2 4.9940599058154813
		 3 4.5599383184532076 4 4.9940599058154813 5 4.5599383184532076 6 4.9940599058154813
		 7 4.5599383184532076 8 4.9940599058154813;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.076558060944080353 0.076558053493499756 
		0.076558060944080353 0.076558053493499756 0.076558060944080353;
	setAttr -s 8 ".kiy[3:7]"  0.99706512689590454 -0.99706512689590454 
		0.99706512689590454 -0.99706512689590454 0.99706512689590454;
	setAttr -s 8 ".kox[3:7]"  0.076558053493499756 0.076558053493499756 
		0.076558053493499756 0.07655809074640274 0.076558053493499756;
	setAttr -s 8 ".koy[3:7]"  -0.99706512689590454 0.99706512689590454 
		-0.99706512689590454 0.99706512689590454 -0.99706512689590454;
createNode animCurveTA -n "rigw:tracks:stuff92_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 155.44578500564543 3 155.44578500564543
		 5 155.44578500564543 7 155.44578500564543;
createNode animCurveTA -n "rigw:tracks:stuff92_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.46022316200377594 3 0.46022316200377594
		 5 0.46022316200377594 7 0.46022316200377594;
createNode animCurveTA -n "rigw:tracks:stuff92_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 179.37319838705096 3 179.37319838705096
		 5 179.37319838705096 7 179.37319838705096;
createNode animCurveTU -n "rigw:tracks:stuff92_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff92_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff92_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff91_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff91_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.9001957152255784 2 -3.9048746461492279
		 3 -3.9001957152255784 4 -3.9048746461492279 5 -3.9001957152255784 6 -3.9048746461492279
		 7 -3.9001957152255784 8 -3.9048746461492279;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.99029171466827393 0.99029159545898438 
		0.99029171466827393 0.99029159545898438 0.99029171466827393;
	setAttr -s 8 ".kiy[3:7]"  -0.13900518417358398 0.13900518417358398 
		-0.13900518417358398 0.13900518417358398 -0.13900518417358398;
	setAttr -s 8 ".kox[3:7]"  0.99029159545898438 0.99029159545898438 
		0.99029159545898438 0.99029171466827393 0.99029159545898438;
	setAttr -s 8 ".koy[3:7]"  0.13900518417358398 -0.13900518417358398 
		0.13900518417358398 -0.1390051394701004 0.13900518417358398;
createNode animCurveTL -n "rigw:tracks:stuff91_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.3826756974611949 2 2.2794181654751364
		 3 2.3826756974611949 4 2.2794181654751364 5 2.3826756974611949 6 2.2794181654751364
		 7 2.3826756974611949 8 2.2794181654751364;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.3072068989276886 0.3072068989276886 0.3072068989276886 
		0.3072068989276886 0.3072068989276886;
	setAttr -s 8 ".kiy[3:7]"  -0.95164275169372559 0.95164281129837036 
		-0.95164275169372559 0.95164281129837036 -0.95164275169372559;
	setAttr -s 8 ".kox[3:7]"  0.3072068989276886 0.3072068989276886 0.3072068989276886 
		0.30720701813697815 0.3072068989276886;
	setAttr -s 8 ".koy[3:7]"  0.95164281129837036 -0.95164281129837036 
		0.95164281129837036 -0.95164275169372559 0.95164281129837036;
createNode animCurveTL -n "rigw:tracks:stuff91_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.6154179710028074 2 4.0572511939598543
		 3 3.6154179710028074 4 4.0572511939598543 5 3.6154179710028074 6 4.0572511939598543
		 7 3.6154179710028074 8 4.0572511939598543;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.075229465961456299 0.075229458510875702 
		0.075229465961456299 0.075229458510875702 0.075229465961456299;
	setAttr -s 8 ".kiy[3:7]"  0.99716627597808838 -0.99716627597808838 
		0.99716627597808838 -0.99716627597808838 0.99716627597808838;
	setAttr -s 8 ".kox[3:7]"  0.075229458510875702 0.075229458510875702 
		0.075229458510875702 0.07522948831319809 0.075229458510875702;
	setAttr -s 8 ".koy[3:7]"  -0.99716627597808838 0.99716627597808838 
		-0.99716627597808838 0.99716627597808838 -0.99716627597808838;
createNode animCurveTA -n "rigw:tracks:stuff91_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 155.44578500564543 3 155.44578500564543
		 5 155.44578500564543 7 155.44578500564543;
createNode animCurveTA -n "rigw:tracks:stuff91_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.46022316200377594 3 0.46022316200377594
		 5 0.46022316200377594 7 0.46022316200377594;
createNode animCurveTA -n "rigw:tracks:stuff91_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 179.37319838705096 3 179.37319838705096
		 5 179.37319838705096 7 179.37319838705096;
createNode animCurveTU -n "rigw:tracks:stuff91_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff91_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff91_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff85_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff85_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373498 3 -3.8895896675373498
		 5 -3.8895896675373498 7 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff85_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.6988127858634878 2 2.6262482354755936
		 3 2.6988127858634878 4 2.622051275038964 5 2.6988127858634878 6 2.6202038870631852
		 7 2.6988127858634878 8 2.6124712058201718;
createNode animCurveTL -n "rigw:tracks:stuff85_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.6828680972306351 2 3.1217276742403404
		 3 2.6828680972306351 4 3.1471102649420004 5 2.6828680972306351 6 3.1582829918629161
		 7 2.6828680972306351 8 3.2050490946130896;
createNode animCurveTA -n "rigw:tracks:stuff85_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 170.61120588335984 3 170.61120588335984
		 5 170.61120588335984 7 170.61120588335984;
createNode animCurveTA -n "rigw:tracks:stuff85_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff85_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff85_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff85_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff85_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff86_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff86_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373489 3 -3.8895896675373489
		 5 -3.8895896675373489 7 -3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff86_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.7544668665867889 3 2.7544668665867889
		 5 2.7544668665867889 7 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff86_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.6983091210659895 2 2.1431274461225613
		 3 1.6983091210659895 4 2.1688546765921375 5 1.6983091210659895 6 2.1801791045769132
		 7 1.6983091210659895 8 2.2275801881683877;
createNode animCurveTA -n "rigw:tracks:stuff86_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff86_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff86_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff86_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff86_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff86_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff87_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff87_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373489 3 -3.8895896675373489
		 5 -3.8895896675373489 7 -3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff87_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.7544668665867889 3 2.7544668665867889
		 5 2.7544668665867889 7 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff87_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.67019983678636175 2 1.1150181618429338
		 3 0.67019983678636175 4 1.140745392312509 5 0.67019983678636175 6 1.1520698202972863
		 7 0.67019983678636175 8 1.1994709038887601;
createNode animCurveTA -n "rigw:tracks:stuff87_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff87_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff87_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff87_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff87_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff87_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff88_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff88_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373498 3 -3.8895896675373498
		 5 -3.8895896675373498 7 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff88_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.7544668665867889 3 2.7544668665867889
		 5 2.7544668665867889 7 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff88_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -0.35790944749326581 2 0.086908877563306036
		 3 -0.35790944749326581 4 0.11263610803288177 5 -0.35790944749326581 6 0.12396053601765857
		 7 -0.35790944749326581 8 0.17136161960913265;
createNode animCurveTA -n "rigw:tracks:stuff88_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff88_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff88_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff88_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff88_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff88_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff89_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff89_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373502 3 -3.8895896675373502
		 5 -3.8895896675373502 7 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff89_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.7544668665867889 3 2.7544668665867889
		 5 2.7544668665867889 7 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff89_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -1.3860187317728931 2 -0.94120040671632133
		 3 -1.3860187317728931 4 -0.9154731762467454 5 -1.3860187317728931 6 -0.90414874826196867
		 7 -1.3860187317728931 8 -0.85674766467049435;
createNode animCurveTA -n "rigw:tracks:stuff89_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff89_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff89_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff89_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff89_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff89_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff90_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff90_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373502 3 -3.8895896675373502
		 5 -3.8895896675373502 7 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff90_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.7544668665867889 3 2.7544668665867889
		 5 2.7544668665867889 7 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff90_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -2.4141280160525209 2 -1.9693096909959504
		 3 -2.4141280160525209 4 -1.9435824605263741 5 -2.4141280160525209 6 -1.9322580325415968
		 7 -2.4141280160525209 8 -1.884856948950123;
createNode animCurveTA -n "rigw:tracks:stuff90_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff90_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff90_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff90_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff90_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff90_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff95_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff95_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373502 3 -3.8895896675373502
		 5 -3.8895896675373502 7 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff95_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.6303919141490808 2 2.7419387649413962
		 3 2.6303919141490808 4 2.7483903686115547 5 2.6303919141490808 6 2.7512301892924267
		 7 2.6303919141490808 8 2.7631169333875567;
createNode animCurveTL -n "rigw:tracks:stuff95_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.4985456653335549 2 -3.0679406927839441
		 3 -3.4985456653335549 4 -3.043035528706632 5 -3.4985456653335549 6 -3.0320729520166885
		 7 -3.4985456653335549 8 -2.9861864829820934;
createNode animCurveTA -n "rigw:tracks:stuff95_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 14.523049474597975 3 14.523049474597975
		 5 14.523049474597975 7 14.523049474597975;
createNode animCurveTA -n "rigw:tracks:stuff95_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff95_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff95_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff95_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff95_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff96_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff96_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373502 3 -3.8895896675373502
		 5 -3.8895896675373502 7 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff96_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.3224732280064515 2 2.5028265244236567
		 3 2.3224732280064515 4 2.5132577287187656 5 2.3224732280064515 6 2.5178492614907895
		 7 2.3224732280064515 8 2.5370682126848134;
createNode animCurveTL -n "rigw:tracks:stuff96_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -4.4542731959710018 2 -4.0476578608229268
		 3 -4.4542731959710018 4 -4.0241402001274329 5 -4.4542731959710018 6 -4.0137883647352988
		 7 -4.4542731959710018 8 -3.9704582984920087;
createNode animCurveTA -n "rigw:tracks:stuff96_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 23.919569992035171 3 23.919569992035171
		 5 23.919569992035171 7 23.919569992035171;
createNode animCurveTA -n "rigw:tracks:stuff96_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff96_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff96_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff96_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff96_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff97_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff97_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373502 3 -3.8895896675373502
		 5 -3.8895896675373502 7 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff97_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.8872206549298032 2 2.0675739513470082
		 3 1.8872206549298032 4 2.0780051556421171 5 1.8872206549298032 6 2.0825966884141409
		 7 1.8872206549298032 8 2.1018156396081653;
createNode animCurveTL -n "rigw:tracks:stuff97_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -5.4355714277827456 2 -5.0289560926346706
		 3 -5.4355714277827456 4 -5.0054384319391767 5 -5.4355714277827456 6 -4.9950865965470426
		 7 -5.4355714277827456 8 -4.9517565303037534;
createNode animCurveTA -n "rigw:tracks:stuff97_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 23.919569992035171 3 23.919569992035171
		 5 23.919569992035171 7 23.919569992035171;
createNode animCurveTA -n "rigw:tracks:stuff97_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff97_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff97_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff97_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff97_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff98_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff98_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373498 3 -3.8895896675373498
		 5 -3.8895896675373498 7 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff98_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.4519680818531553 2 1.6323213782703605
		 3 1.4519680818531553 4 1.6427525825654692 5 1.4519680818531553 6 1.6473441153374926
		 7 1.4519680818531553 8 1.6665630665315172;
createNode animCurveTL -n "rigw:tracks:stuff98_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -6.4168696595944894 2 -6.0102543244464135
		 3 -6.4168696595944894 4 -5.9867366637509205 5 -6.4168696595944894 6 -5.9763848283587864
		 7 -6.4168696595944894 8 -5.9330547621154972;
createNode animCurveTA -n "rigw:tracks:stuff98_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 23.919569992035171 3 23.919569992035171
		 5 23.919569992035171 7 23.919569992035171;
createNode animCurveTA -n "rigw:tracks:stuff98_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff98_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff98_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff98_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff98_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -0.99999999999999989 3 -0.99999999999999989
		 5 -0.99999999999999989 7 -0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff111_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff111_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373529 3 -3.8895896675373529
		 5 -3.8895896675373529 7 -3.8895896675373529;
createNode animCurveTL -n "rigw:tracks:stuff111_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.78689185839297315 2 1.1427580908019821
		 3 0.78689185839297315 4 1.1427580908019821 5 0.78689185839297315 6 1.1427580908019821
		 7 0.78689185839297315 8 1.1427580908019821;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.093259945511817932 0.093259930610656738 
		0.093259945511817932 0.093259930610656738 0.093259945511817932;
	setAttr -s 8 ".kiy[3:7]"  0.99564182758331299 -0.99564182758331299 
		0.99564182758331299 -0.99564182758331299 0.99564182758331299;
	setAttr -s 8 ".kox[3:7]"  0.093259930610656738 0.093259930610656738 
		0.093259930610656738 0.09325997531414032 0.093259930610656738;
	setAttr -s 8 ".koy[3:7]"  -0.99564182758331299 0.99564182758331299 
		-0.99564182758331299 0.99564182758331299 -0.99564182758331299;
createNode animCurveTL -n "rigw:tracks:stuff111_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -6.8744340999844455 2 -6.7569788736319545
		 3 -6.8744340999844455 4 -6.7569788736319545 5 -6.8744340999844455 6 -6.7569788736319545
		 7 -6.8744340999844455 8 -6.7569788736319545;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.27301463484764099 0.27301463484764099 
		0.27301463484764099 0.27301463484764099 0.27301463484764099;
	setAttr -s 8 ".kiy[3:7]"  0.96200984716415405 -0.96200990676879883 
		0.96200984716415405 -0.96200990676879883 0.96200984716415405;
	setAttr -s 8 ".kox[3:7]"  0.27301463484764099 0.27301463484764099 
		0.27301463484764099 0.27301472425460815 0.27301463484764099;
	setAttr -s 8 ".koy[3:7]"  -0.96200990676879883 0.96200990676879883 
		-0.96200990676879883 0.96200984716415405 -0.96200990676879883;
createNode animCurveTA -n "rigw:tracks:stuff111_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -89.264667126223671 2 -125.82657909301058
		 3 -89.264667126223671 4 -125.82657909301058 5 -89.264667126223671 6 -125.82657909301058
		 7 -89.264667126223671 8 -125.82657909301058;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.052165195345878601 0.052165187895298004 
		0.052165195345878601 0.052165187895298004 0.052165195345878601;
	setAttr -s 8 ".kiy[3:7]"  -0.99863851070404053 0.99863851070404053 
		-0.99863851070404053 0.99863851070404053 -0.99863851070404053;
	setAttr -s 8 ".kox[3:7]"  0.052165187895298004 0.052165187895298004 
		0.052165187895298004 0.052165210247039795 0.052165187895298004;
	setAttr -s 8 ".koy[3:7]"  0.99863851070404053 -0.99863851070404053 
		0.99863851070404053 -0.99863851070404053 0.99863851070404053;
createNode animCurveTA -n "rigw:tracks:stuff111_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff111_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff111_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff111_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff111_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff110_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff110_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373529 3 -3.8895896675373529
		 5 -3.8895896675373529 7 -3.8895896675373529;
createNode animCurveTL -n "rigw:tracks:stuff110_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.077125482214109045 2 0.34601101768358178
		 3 0.077125482214109045 4 0.34601101768358178 5 0.077125482214109045 6 0.34601101768358178
		 7 0.077125482214109045 8 0.34601101768358178;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.12302675098180771 0.12302674353122711 
		0.12302675098180771 0.12302674353122711 0.12302675098180771;
	setAttr -s 8 ".kiy[3:7]"  0.99240332841873169 -0.99240332841873169 
		0.99240332841873169 -0.99240332841873169 0.99240332841873169;
	setAttr -s 8 ".kox[3:7]"  0.12302674353122711 0.12302674353122711 
		0.12302674353122711 0.12302679568529129 0.12302674353122711;
	setAttr -s 8 ".koy[3:7]"  -0.99240332841873169 0.99240332841873169 
		-0.99240332841873169 0.99240332841873169 -0.99240332841873169;
createNode animCurveTL -n "rigw:tracks:stuff110_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -6.4850333431961644 2 -6.7994120848023174
		 3 -6.4850333431961644 4 -6.7994120848023174 5 -6.4850333431961644 6 -6.7994120848023174
		 7 -6.4850333431961644 8 -6.7994120848023174;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.10543821007013321 0.10543819516897202 
		0.10543821007013321 0.10543819516897202 0.10543821007013321;
	setAttr -s 8 ".kiy[3:7]"  -0.99442589282989502 0.99442589282989502 
		-0.99442589282989502 0.99442589282989502 -0.99442589282989502;
	setAttr -s 8 ".kox[3:7]"  0.10543819516897202 0.10543819516897202 
		0.10543819516897202 0.1054382398724556 0.10543819516897202;
	setAttr -s 8 ".koy[3:7]"  0.99442589282989502 -0.99442589282989502 
		0.99442589282989502 -0.99442589282989502 0.99442589282989502;
createNode animCurveTA -n "rigw:tracks:stuff110_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -28.783331426910969 2 -54.066614443647808
		 3 -28.783331426910969 4 -54.066614443647808 5 -28.783331426910969 6 -54.066614443647808
		 7 -28.783331426910969 8 -54.066614443647808;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.075323835015296936 0.075323827564716339 
		0.075323835015296936 0.075323827564716339 0.075323835015296936;
	setAttr -s 8 ".kiy[3:7]"  -0.99715912342071533 0.99715912342071533 
		-0.99715912342071533 0.99715912342071533 -0.99715912342071533;
	setAttr -s 8 ".kox[3:7]"  0.075323827564716339 0.075323827564716339 
		0.075323827564716339 0.075323857367038727 0.075323827564716339;
	setAttr -s 8 ".koy[3:7]"  0.99715912342071533 -0.99715912342071533 
		0.99715912342071533 -0.99715912342071533 0.99715912342071533;
createNode animCurveTA -n "rigw:tracks:stuff110_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff110_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff110_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff110_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1.0000000000000002 3 1.0000000000000002
		 5 1.0000000000000002 7 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff110_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1.0000000000000002 3 -1.0000000000000002
		 5 -1.0000000000000002 7 -1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff109_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff109_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.889589667537352 3 -3.889589667537352
		 5 -3.889589667537352 7 -3.889589667537352;
createNode animCurveTL -n "rigw:tracks:stuff109_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590271651 3 0.0036092918590271651
		 5 0.0036092918590271651 7 0.0036092918590271651;
createNode animCurveTL -n "rigw:tracks:stuff109_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -5.4488368443595645 2 -5.8936551694161388
		 3 -5.4488368443595645 4 -5.9193823998857118 5 -5.4488368443595645 6 -5.9307068278704884
		 7 -5.4488368443595645 8 -5.9781079114619624;
createNode animCurveTA -n "rigw:tracks:stuff109_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff109_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff109_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff109_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff109_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff109_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff108_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff108_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.889589667537352 3 -3.889589667537352
		 5 -3.889589667537352 7 -3.889589667537352;
createNode animCurveTL -n "rigw:tracks:stuff108_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590272896 3 0.0036092918590272896
		 5 0.0036092918590272896 7 0.0036092918590272896;
createNode animCurveTL -n "rigw:tracks:stuff108_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -4.4294452888197569 2 -4.8742636138763311
		 3 -4.4294452888197569 4 -4.8999908443459033 5 -4.4294452888197569 6 -4.9113152723306808
		 7 -4.4294452888197569 8 -4.9587163559221548;
createNode animCurveTA -n "rigw:tracks:stuff108_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff108_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff108_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff108_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff108_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff108_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff107_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff107_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.889589667537352 3 -3.889589667537352
		 5 -3.889589667537352 7 -3.889589667537352;
createNode animCurveTL -n "rigw:tracks:stuff107_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.003609291859027414 3 0.003609291859027414
		 5 0.003609291859027414 7 0.003609291859027414;
createNode animCurveTL -n "rigw:tracks:stuff107_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.4100537332799488 2 -3.854872058336519
		 3 -3.4100537332799488 4 -3.8805992888060961 5 -3.4100537332799488 6 -3.8919237167908727
		 7 -3.4100537332799488 8 -3.9393248003823489;
createNode animCurveTA -n "rigw:tracks:stuff107_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff107_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff107_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff107_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff107_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff107_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff106_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff106_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373502 3 -3.8895896675373502
		 5 -3.8895896675373502 7 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff106_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590275385 3 0.0036092918590275385
		 5 0.0036092918590275385 7 0.0036092918590275385;
createNode animCurveTL -n "rigw:tracks:stuff106_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -2.3906621777401411 2 -2.8354805027967118
		 3 -2.3906621777401411 4 -2.861207733266288 5 -2.3906621777401411 6 -2.8725321612510655
		 7 -2.3906621777401411 8 -2.9199332448425408;
createNode animCurveTA -n "rigw:tracks:stuff106_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff106_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff106_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff106_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff106_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff106_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff105_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff105_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373511 3 -3.8895896675373511
		 5 -3.8895896675373511 7 -3.8895896675373511;
createNode animCurveTL -n "rigw:tracks:stuff105_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.003609291859027663 3 0.003609291859027663
		 5 0.003609291859027663 7 0.003609291859027663;
createNode animCurveTL -n "rigw:tracks:stuff105_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -1.3712706222003337 2 -1.8160889472569055
		 3 -1.3712706222003337 4 -1.841816177726481 5 -1.3712706222003337 6 -1.8531406057112585
		 7 -1.3712706222003337 8 -1.9005416893027325;
createNode animCurveTA -n "rigw:tracks:stuff105_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff105_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff105_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff105_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff105_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff105_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff104_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff104_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373502 3 -3.8895896675373502
		 5 -3.8895896675373502 7 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff104_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590277874 3 0.0036092918590277874
		 5 0.0036092918590277874 7 0.0036092918590277874;
createNode animCurveTL -n "rigw:tracks:stuff104_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -0.35187906666052582 2 -0.79669739171709764
		 3 -0.35187906666052582 4 -0.82242462218667345 5 -0.35187906666052582 6 -0.83374905017145029
		 7 -0.35187906666052582 8 -0.88115013376292428;
createNode animCurveTA -n "rigw:tracks:stuff104_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff104_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff104_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff104_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff104_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff104_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff103_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff103_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373502 3 -3.8895896675373502
		 5 -3.8895896675373502 7 -3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff103_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590279119 3 0.0036092918590279119
		 5 0.0036092918590279119 7 0.0036092918590279119;
createNode animCurveTL -n "rigw:tracks:stuff103_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.66751248887928183 2 0.22269416382271001
		 3 0.66751248887928183 4 0.1969669333531342 5 0.66751248887928183 6 0.18564250536835747
		 7 0.66751248887928183 8 0.13824142177688337;
createNode animCurveTA -n "rigw:tracks:stuff103_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff103_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff103_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff103_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff103_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff103_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff102_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff102_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373498 3 -3.8895896675373498
		 5 -3.8895896675373498 7 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff102_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590280364 3 0.0036092918590280364
		 5 0.0036092918590280364 7 0.0036092918590280364;
createNode animCurveTL -n "rigw:tracks:stuff102_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.6869040444190897 2 1.2420857193625181
		 3 1.6869040444190897 4 1.2163584888929424 5 1.6869040444190897 6 1.2050340609081649
		 7 1.6869040444190897 8 1.1576329773166911;
createNode animCurveTA -n "rigw:tracks:stuff102_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff102_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff102_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff102_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff102_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff102_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff101_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff101_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373498 3 -3.8895896675373498
		 5 -3.8895896675373498 7 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff101_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590281608 3 0.0036092918590281608
		 5 0.0036092918590281608 7 0.0036092918590281608;
createNode animCurveTL -n "rigw:tracks:stuff101_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.7062955999588971 2 2.2614772749023264
		 3 2.7062955999588971 4 2.2357500444327503 5 2.7062955999588971 6 2.2244256164479728
		 7 2.7062955999588971 8 2.1770245328564974;
createNode animCurveTA -n "rigw:tracks:stuff101_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff101_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff101_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff101_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff101_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff101_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff100_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff100_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373498 3 -3.8895896675373498
		 5 -3.8895896675373498 7 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff100_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590282853 3 0.0036092918590282853
		 5 0.0036092918590282853 7 0.0036092918590282853;
createNode animCurveTL -n "rigw:tracks:stuff100_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.7256871554987057 2 3.280868830442135
		 3 3.7256871554987057 4 3.2551415999725588 5 3.7256871554987057 6 3.2438171719877813
		 7 3.7256871554987057 8 3.196416088396306;
createNode animCurveTA -n "rigw:tracks:stuff100_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff100_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff100_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff100_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff100_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff100_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff99_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff99_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373498 3 -3.8895896675373498
		 5 -3.8895896675373498 7 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff99_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590284093 3 0.0036092918590284093
		 5 0.0036092918590284093 7 0.0036092918590284093;
createNode animCurveTL -n "rigw:tracks:stuff99_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 4.745078711038512 2 4.3002603859819377
		 3 4.745078711038512 4 4.2745331555123647 5 4.745078711038512 6 4.2632087275275881
		 7 4.745078711038512 8 4.2158076439361141;
createNode animCurveTA -n "rigw:tracks:stuff99_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff99_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff99_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff99_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff99_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff99_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1 3 -1 5 -1 7 -1;
createNode animCurveTU -n "rigw:tracks:stuff112_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff112_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -3.8895896675373498 3 -3.8895896675373498
		 5 -3.8895896675373498 7 -3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff112_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.11714647165146076 2 -0.0017393210588157694
		 3 0.11714647165146076 4 -0.0017393210588157694 5 0.11714647165146076 6 -0.0017393210588157694
		 7 0.11714647165146076 8 -0.0017393210588157694;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.26997023820877075 0.26997020840644836 
		0.26997023820877075 0.26997020840644836 0.26997023820877075;
	setAttr -s 8 ".kiy[3:7]"  -0.96286869049072266 0.96286869049072266 
		-0.96286869049072266 0.96286869049072266 -0.96286869049072266;
	setAttr -s 8 ".kox[3:7]"  0.26997020840644836 0.26997020840644836 
		0.26997020840644836 0.26997032761573792 0.26997020840644836;
	setAttr -s 8 ".koy[3:7]"  0.96286869049072266 -0.96286869049072266 
		0.96286869049072266 -0.96286869049072266 0.96286869049072266;
createNode animCurveTL -n "rigw:tracks:stuff112_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 5.7205885340857714 2 5.2919517290890674
		 3 5.7205885340857714 4 5.2919517290890674 5 5.7205885340857714 6 5.2919517290890674
		 7 5.7205885340857714 8 5.2919517290890674;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.077531829476356506 0.077531822025775909 
		0.077531829476356506 0.077531822025775909 0.077531829476356506;
	setAttr -s 8 ".kiy[3:7]"  -0.996989905834198 0.996989905834198 -0.996989905834198 
		0.996989905834198 -0.996989905834198;
	setAttr -s 8 ".kox[3:7]"  0.077531822025775909 0.077531822025775909 
		0.077531822025775909 0.077531859278678894 0.077531822025775909;
	setAttr -s 8 ".koy[3:7]"  0.996989905834198 -0.996989905834198 0.996989905834198 
		-0.996989905834198 0.996989905834198;
createNode animCurveTA -n "rigw:tracks:stuff112_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 15.501773238377783 2 4.7321869922132729
		 3 15.501773238377783 4 4.7321869922132729 5 15.501773238377783 6 4.7321869922132729
		 7 15.501773238377783 8 4.7321869922132729;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.17461378872394562 0.17461377382278442 
		0.17461378872394562 0.17461377382278442 0.17461378872394562;
	setAttr -s 8 ".kiy[3:7]"  -0.98463702201843262 0.98463702201843262 
		-0.98463702201843262 0.98463702201843262 -0.98463702201843262;
	setAttr -s 8 ".kox[3:7]"  0.17461377382278442 0.17461377382278442 
		0.17461377382278442 0.17461384832859039 0.17461377382278442;
	setAttr -s 8 ".koy[3:7]"  0.98463702201843262 -0.98463702201843262 
		0.98463702201843262 -0.98463702201843262 0.98463702201843262;
createNode animCurveTA -n "rigw:tracks:stuff112_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 7.016709298534876e-15 3 7.016709298534876e-15
		 5 7.016709298534876e-15 7 7.016709298534876e-15;
createNode animCurveTA -n "rigw:tracks:stuff112_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTU -n "rigw:tracks:stuff112_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff112_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1.0000000000000002 3 1.0000000000000002
		 5 1.0000000000000002 7 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff112_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -1.0000000000000002 3 -1.0000000000000002
		 5 -1.0000000000000002 7 -1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff51_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff51_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.9365533838983895 2 3.9404432029030847
		 3 3.9365533838983895 4 3.9404432029030847 5 3.9365533838983895 6 3.9404432029030847
		 7 3.9365533838983895 8 3.9404432029030847;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.9932599663734436 0.99325984716415405 
		0.9932599663734436 0.99325984716415405 0.9932599663734436;
	setAttr -s 8 ".kiy[3:7]"  0.11590803414583206 -0.11590803414583206 
		0.11590803414583206 -0.11590803414583206 0.11590803414583206;
	setAttr -s 8 ".kox[3:7]"  0.99325984716415405 0.99325984716415405 
		0.99325984716415405 0.9932599663734436 0.99325984716415405;
	setAttr -s 8 ".koy[3:7]"  -0.11590803414583206 0.11590803414583206 
		-0.11590803414583206 0.11590799689292908 -0.11590803414583206;
createNode animCurveTL -n "rigw:tracks:stuff51_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.8362001562078627 2 0.41699735478794464
		 3 0.8362001562078627 4 0.41699735478794464 5 0.8362001562078627 6 0.41699735478794464
		 7 0.8362001562078627 8 0.41699735478794464;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.079265817999839783 0.079265810549259186 
		0.079265817999839783 0.079265810549259186 0.079265817999839783;
	setAttr -s 8 ".kiy[3:7]"  -0.9968535304069519 0.9968535304069519 
		-0.9968535304069519 0.9968535304069519 -0.9968535304069519;
	setAttr -s 8 ".kox[3:7]"  0.079265810549259186 0.079265810549259186 
		0.079265810549259186 0.07926584780216217 0.079265810549259186;
	setAttr -s 8 ".koy[3:7]"  0.9968535304069519 -0.9968535304069519 
		0.9968535304069519 -0.9968535304069519 0.9968535304069519;
createNode animCurveTL -n "rigw:tracks:stuff51_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 6.0352838681597616 2 5.9485997971798898
		 3 6.0352838681597616 4 5.9485997971798898 5 6.0352838681597616 6 5.9485997971798898
		 7 6.0352838681597616 8 5.9485997971798898;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.35891631245613098 0.35891628265380859 
		0.35891631245613098 0.35891628265380859 0.35891631245613098;
	setAttr -s 8 ".kiy[3:7]"  -0.93336975574493408 0.93336975574493408 
		-0.93336975574493408 0.93336975574493408 -0.93336975574493408;
	setAttr -s 8 ".kox[3:7]"  0.35891628265380859 0.35891628265380859 
		0.35891628265380859 0.35891640186309814 0.35891628265380859;
	setAttr -s 8 ".koy[3:7]"  0.93336975574493408 -0.93336975574493408 
		0.93336975574493408 -0.93336969614028931 0.93336975574493408;
createNode animCurveTA -n "rigw:tracks:stuff51_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 94.75296763901126 2 115.78135184407158
		 3 94.75296763901126 4 115.78135184407158 5 94.75296763901126 6 115.78135184407158
		 7 94.75296763901126 8 115.78135184407158;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.090450644493103027 0.09045063704252243 
		0.090450644493103027 0.09045063704252243 0.090450644493103027;
	setAttr -s 8 ".kiy[3:7]"  0.99590098857879639 -0.99590098857879639 
		0.99590098857879639 -0.99590098857879639 0.99590098857879639;
	setAttr -s 8 ".kox[3:7]"  0.09045063704252243 0.09045063704252243 
		0.09045063704252243 0.090450674295425415 0.09045063704252243;
	setAttr -s 8 ".koy[3:7]"  -0.99590098857879639 0.99590098857879639 
		-0.99590098857879639 0.99590098857879639 -0.99590098857879639;
createNode animCurveTA -n "rigw:tracks:stuff51_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.46022316200377128 3 0.46022316200377128
		 5 0.46022316200377128 7 0.46022316200377128;
createNode animCurveTA -n "rigw:tracks:stuff51_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.62680161294900905 3 0.62680161294900905
		 5 0.62680161294900905 7 0.62680161294900905;
createNode animCurveTU -n "rigw:tracks:stuff51_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff51_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff51_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1.0000000000000002 3 1.0000000000000002
		 5 1.0000000000000002 7 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff50_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff50_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.9248102972057617 2 3.9301340761988985
		 3 3.9248102972057617 4 3.9301340761988985 5 3.9248102972057617 6 3.9301340761988985
		 7 3.9248102972057617 8 3.9301340761988985;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.98748475313186646 0.98748475313186646 
		0.98748475313186646 0.98748475313186646 0.98748475313186646;
	setAttr -s 8 ".kiy[3:7]"  0.15771450102329254 -0.15771451592445374 
		0.15771450102329254 -0.15771451592445374 0.15771450102329254;
	setAttr -s 8 ".kox[3:7]"  0.98748475313186646 0.98748475313186646 
		0.98748475313186646 0.98748475313186646 0.98748475313186646;
	setAttr -s 8 ".koy[3:7]"  -0.15771451592445374 0.15771451592445374 
		-0.15771451592445374 0.15771445631980896 -0.15771451592445374;
createNode animCurveTL -n "rigw:tracks:stuff50_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.5198204208337878 2 1.2385073296965541
		 3 1.5198204208337878 4 1.2385073296965541 5 1.5198204208337878 6 1.2385073296965541
		 7 1.5198204208337878 8 1.2385073296965541;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.1176687628030777 0.1176687479019165 0.1176687628030777 
		0.1176687479019165 0.1176687628030777;
	setAttr -s 8 ".kiy[3:7]"  -0.99305284023284912 0.99305284023284912 
		-0.99305284023284912 0.99305284023284912 -0.99305284023284912;
	setAttr -s 8 ".kox[3:7]"  0.1176687479019165 0.1176687479019165 0.1176687479019165 
		0.11766880005598068 0.1176687479019165;
	setAttr -s 8 ".koy[3:7]"  0.99305284023284912 -0.99305284023284912 
		0.99305284023284912 -0.99305284023284912 0.99305284023284912;
createNode animCurveTL -n "rigw:tracks:stuff50_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 5.5044586659036083 2 5.7840731564694332
		 3 5.5044586659036083 4 5.7840731564694332 5 5.5044586659036083 6 5.7840731564694332
		 7 5.5044586659036083 8 5.7840731564694332;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.11837360262870789 0.11837358772754669 
		0.11837360262870789 0.11837358772754669 0.11837360262870789;
	setAttr -s 8 ".kiy[3:7]"  0.9929690957069397 -0.9929690957069397 
		0.9929690957069397 -0.9929690957069397 0.9929690957069397;
	setAttr -s 8 ".kox[3:7]"  0.11837358772754669 0.11837358772754669 
		0.11837358772754669 0.11837363988161087 0.11837358772754669;
	setAttr -s 8 ".koy[3:7]"  -0.9929690957069397 0.9929690957069397 
		-0.9929690957069397 0.9929690957069397 -0.9929690957069397;
createNode animCurveTA -n "rigw:tracks:stuff50_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 24.554214994354588 2 52.322612747336763
		 3 24.554214994354588 4 52.322612747336763 5 24.554214994354588 6 52.322612747336763
		 7 24.554214994354588 8 52.322612747336763;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.06861606240272522 0.068616054952144623 
		0.06861606240272522 0.068616054952144623 0.06861606240272522;
	setAttr -s 8 ".kiy[3:7]"  0.99764317274093628 -0.99764317274093628 
		0.99764317274093628 -0.99764317274093628 0.99764317274093628;
	setAttr -s 8 ".kox[3:7]"  0.068616054952144623 0.068616054952144623 
		0.068616054952144623 0.068616092205047607 0.068616054952144623;
	setAttr -s 8 ".koy[3:7]"  -0.99764317274093628 0.99764317274093628 
		-0.99764317274093628 0.99764317274093628 -0.99764317274093628;
createNode animCurveTA -n "rigw:tracks:stuff50_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.46022316200377039 3 0.46022316200377039
		 5 0.46022316200377039 7 0.46022316200377039;
createNode animCurveTA -n "rigw:tracks:stuff50_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.62680161294901959 3 0.62680161294901959
		 5 0.62680161294901959 7 0.62680161294901959;
createNode animCurveTU -n "rigw:tracks:stuff50_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff50_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff50_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff49_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff49_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.9125030062156698 2 3.9173652131211454
		 3 3.9125030062156698 4 3.9173652131211454 5 3.9125030062156698 6 3.9173652131211454
		 7 3.9125030062156698 8 3.9173652131211454;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.9895283579826355 0.98952829837799072 
		0.9895283579826355 0.98952829837799072 0.9895283579826355;
	setAttr -s 8 ".kiy[3:7]"  0.14433874189853668 -0.14433874189853668 
		0.14433874189853668 -0.14433874189853668 0.14433874189853668;
	setAttr -s 8 ".kox[3:7]"  0.98952829837799072 0.98952829837799072 
		0.98952829837799072 0.9895283579826355 0.98952829837799072;
	setAttr -s 8 ".koy[3:7]"  -0.14433874189853668 0.14433874189853668 
		-0.14433874189853668 0.1443386971950531 -0.14433874189853668;
createNode animCurveTL -n "rigw:tracks:stuff49_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.9512480591474912 2 1.8227812259733931
		 3 1.9512480591474912 4 1.8227812259733931 5 1.9512480591474912 6 1.8227812259733931
		 7 1.9512480591474912 8 1.8227812259733931;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.2511536180973053 0.25115358829498291 
		0.2511536180973053 0.25115358829498291 0.2511536180973053;
	setAttr -s 8 ".kiy[3:7]"  -0.96794724464416504 0.96794724464416504 
		-0.96794724464416504 0.96794724464416504 -0.96794724464416504;
	setAttr -s 8 ".kox[3:7]"  0.25115358829498291 0.25115358829498291 
		0.25115358829498291 0.25115370750427246 0.25115358829498291;
	setAttr -s 8 ".koy[3:7]"  0.96794724464416504 -0.96794724464416504 
		0.96794724464416504 -0.96794724464416504 0.96794724464416504;
createNode animCurveTL -n "rigw:tracks:stuff49_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 4.5599383184532085 2 4.9902543253594809
		 3 4.5599383184532085 4 4.9902543253594809 5 4.5599383184532085 6 4.9902543253594809
		 7 4.5599383184532085 8 4.9902543253594809;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.077231094241142273 0.077231086790561676 
		0.077231094241142273 0.077231086790561676 0.077231094241142273;
	setAttr -s 8 ".kiy[3:7]"  0.99701327085494995 -0.99701327085494995 
		0.99701327085494995 -0.99701327085494995 0.99701327085494995;
	setAttr -s 8 ".kox[3:7]"  0.077231086790561676 0.077231086790561676 
		0.077231086790561676 0.077231124043464661 0.077231086790561676;
	setAttr -s 8 ".koy[3:7]"  -0.99701327085494995 0.99701327085494995 
		-0.99701327085494995 0.99701327085494995 -0.99701327085494995;
createNode animCurveTA -n "rigw:tracks:stuff49_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 24.554214994354584 3 24.554214994354584
		 5 24.554214994354584 7 24.554214994354584;
createNode animCurveTA -n "rigw:tracks:stuff49_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.46022316200376889 3 0.46022316200376889
		 5 0.46022316200376889 7 0.46022316200376889;
createNode animCurveTA -n "rigw:tracks:stuff49_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.62680161294903081 3 0.62680161294903081
		 5 0.62680161294903081 7 0.62680161294903081;
createNode animCurveTU -n "rigw:tracks:stuff49_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff49_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff49_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff48_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff48_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.900195715225578 2 3.9049387400246554
		 3 3.900195715225578 4 3.9049387400246554 5 3.900195715225578 6 3.9049387400246554
		 7 3.900195715225578 8 3.9049387400246554;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.99002784490585327 0.99002784490585327 
		0.99002784490585327 0.99002784490585327 0.99002784490585327;
	setAttr -s 8 ".kiy[3:7]"  0.1408717930316925 -0.1408718079328537 
		0.1408717930316925 -0.1408718079328537 0.1408717930316925;
	setAttr -s 8 ".kox[3:7]"  0.99002784490585327 0.99002784490585327 
		0.99002784490585327 0.99002784490585327 0.99002784490585327;
	setAttr -s 8 ".koy[3:7]"  -0.1408718079328537 0.1408718079328537 
		-0.1408718079328537 0.14087174832820892 -0.1408718079328537;
createNode animCurveTL -n "rigw:tracks:stuff48_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.3826756974611949 2 2.2706021613599932
		 3 2.3826756974611949 4 2.2706021613599932 5 2.3826756974611949 6 2.2706021613599932
		 7 2.3826756974611949 8 2.2706021613599932;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.2850816547870636 0.2850816547870636 0.2850816547870636 
		0.2850816547870636 0.2850816547870636;
	setAttr -s 8 ".kiy[3:7]"  -0.95850324630737305 0.95850330591201782 
		-0.95850324630737305 0.95850330591201782 -0.95850324630737305;
	setAttr -s 8 ".kox[3:7]"  0.2850816547870636 0.2850816547870636 0.2850816547870636 
		0.28508174419403076 0.2850816547870636;
	setAttr -s 8 ".koy[3:7]"  0.95850330591201782 -0.95850330591201782 
		0.95850330591201782 -0.95850324630737305 0.95850330591201782;
createNode animCurveTL -n "rigw:tracks:stuff48_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.6154179710028078 2 4.0532234811985495
		 3 3.6154179710028078 4 4.0532234811985495 5 3.6154179710028078 6 4.0532234811985495
		 7 3.6154179710028078 8 4.0532234811985495;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.07591758668422699 0.075917579233646393 
		0.07591758668422699 0.075917579233646393 0.07591758668422699;
	setAttr -s 8 ".kiy[3:7]"  0.99711406230926514 -0.99711406230926514 
		0.99711406230926514 -0.99711406230926514 0.99711406230926514;
	setAttr -s 8 ".kox[3:7]"  0.075917579233646393 0.075917579233646393 
		0.075917579233646393 0.075917609035968781 0.075917579233646393;
	setAttr -s 8 ".koy[3:7]"  -0.99711406230926514 0.99711406230926514 
		-0.99711406230926514 0.99711406230926514 -0.99711406230926514;
createNode animCurveTA -n "rigw:tracks:stuff48_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 24.554214994354584 3 24.554214994354584
		 5 24.554214994354584 7 24.554214994354584;
createNode animCurveTA -n "rigw:tracks:stuff48_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.46022316200376889 3 0.46022316200376889
		 5 0.46022316200376889 7 0.46022316200376889;
createNode animCurveTA -n "rigw:tracks:stuff48_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.62680161294903081 3 0.62680161294903081
		 5 0.62680161294903081 7 0.62680161294903081;
createNode animCurveTU -n "rigw:tracks:stuff48_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff48_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff48_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff42_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff42_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373489 3 3.8895896675373489
		 5 3.8895896675373489 7 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff42_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.6988127858634878 2 2.6262482354755932
		 3 2.6988127858634878 4 2.6262482354755932 5 2.6988127858634878 6 2.6262482354755932
		 7 2.6988127858634878 8 2.6262482354755932;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.41742640733718872 0.41742637753486633 
		0.41742640733718872 0.41742637753486633 0.41742640733718872;
	setAttr -s 8 ".kiy[3:7]"  -0.90871071815490723 0.90871071815490723 
		-0.90871071815490723 0.90871071815490723 -0.90871071815490723;
	setAttr -s 8 ".kox[3:7]"  0.41742637753486633 0.41742637753486633 
		0.41742637753486633 0.41742649674415588 0.41742637753486633;
	setAttr -s 8 ".koy[3:7]"  0.90871071815490723 -0.90871071815490723 
		0.90871071815490723 -0.90871065855026245 0.90871071815490723;
createNode animCurveTL -n "rigw:tracks:stuff42_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.6828680972306356 2 3.1217276742403408
		 3 2.6828680972306356 4 3.1217276742403408 5 2.6828680972306356 6 3.1217276742403408
		 7 2.6828680972306356 8 3.1217276742403408;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.075736299157142639 0.075736291706562042 
		0.075736299157142639 0.075736291706562042 0.075736299157142639;
	setAttr -s 8 ".kiy[3:7]"  0.9971279501914978 -0.9971279501914978 
		0.9971279501914978 -0.9971279501914978 0.9971279501914978;
	setAttr -s 8 ".kox[3:7]"  0.075736291706562042 0.075736291706562042 
		0.075736291706562042 0.07573632150888443 0.075736291706562042;
	setAttr -s 8 ".koy[3:7]"  -0.9971279501914978 0.9971279501914978 
		-0.9971279501914978 0.9971279501914978 -0.9971279501914978;
createNode animCurveTA -n "rigw:tracks:stuff42_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 9.3887941166401472 2 13.371629029222948
		 3 9.3887941166401472 4 13.371629029222948 5 9.3887941166401472 6 13.371629029222948
		 7 9.3887941166401472 8 13.371629029222948;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.43238121271133423 0.43238115310668945 
		0.43238121271133423 0.43238115310668945 0.43238121271133423;
	setAttr -s 8 ".kiy[3:7]"  0.90169095993041992 -0.90169095993041992 
		0.90169095993041992 -0.90169095993041992 0.90169095993041992;
	setAttr -s 8 ".kox[3:7]"  0.43238115310668945 0.43238115310668945 
		0.43238115310668945 0.43238130211830139 0.43238115310668945;
	setAttr -s 8 ".koy[3:7]"  -0.90169095993041992 0.90169095993041992 
		-0.90169095993041992 0.90169084072113037 -0.90169095993041992;
createNode animCurveTA -n "rigw:tracks:stuff42_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff42_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff42_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff42_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff42_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff43_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff43_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373489 3 3.8895896675373489
		 5 3.8895896675373489 7 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff43_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.7544668665867889 3 2.7544668665867889
		 5 2.7544668665867889 7 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff43_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.69830912106599 2 2.1431274461225622
		 3 1.69830912106599 4 2.1688546765921375 5 1.69830912106599 6 2.1801791045769146 7 1.69830912106599
		 8 2.2275801881683877;
createNode animCurveTA -n "rigw:tracks:stuff43_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 2 1.0964080595694388 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff43_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff43_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff43_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff43_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff43_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff44_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff44_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373489 3 3.8895896675373489
		 5 3.8895896675373489 7 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff44_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.7544668665867889 3 2.7544668665867889
		 5 2.7544668665867889 7 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff44_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.67019983678636219 2 1.1150181618429342
		 3 0.67019983678636219 4 1.1407453923125097 5 0.67019983678636219 6 1.1520698202972868
		 7 0.67019983678636219 8 1.1994709038887601;
createNode animCurveTA -n "rigw:tracks:stuff44_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff44_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff44_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff44_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff44_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff44_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff45_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff45_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373498 3 3.8895896675373498
		 5 3.8895896675373498 7 3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff45_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.7544668665867889 3 2.7544668665867889
		 5 2.7544668665867889 7 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff45_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -0.35790944749326536 2 0.086908877563306453
		 3 -0.35790944749326536 4 0.11263610803288213 5 -0.35790944749326536 6 0.12396053601765904
		 7 -0.35790944749326536 8 0.17136161960913307;
createNode animCurveTA -n "rigw:tracks:stuff45_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff45_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff45_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff45_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff45_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff45_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff46_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff46_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373502 3 3.8895896675373502
		 5 3.8895896675373502 7 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff46_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.7544668665867889 3 2.7544668665867889
		 5 2.7544668665867889 7 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff46_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -1.3860187317728927 2 -0.94120040671632044
		 3 -1.3860187317728927 4 -0.91547317624674507 5 -1.3860187317728927 6 -0.904148748261968
		 7 -1.3860187317728927 8 -0.85674766467049457;
createNode animCurveTA -n "rigw:tracks:stuff46_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff46_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff46_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff46_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff46_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff46_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff47_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff47_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373502 3 3.8895896675373502
		 5 3.8895896675373502 7 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff47_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.7544668665867889 3 2.7544668665867889
		 5 2.7544668665867889 7 2.7544668665867889;
createNode animCurveTL -n "rigw:tracks:stuff47_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -2.4141280160525205 2 -1.9693096909959495
		 3 -2.4141280160525205 4 -1.9435824605263736 5 -2.4141280160525205 6 -1.9322580325415952
		 7 -2.4141280160525205 8 -1.8848569489501219;
createNode animCurveTA -n "rigw:tracks:stuff47_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff47_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff47_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff47_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff47_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff47_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff52_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff52_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373502 3 3.8895896675373502
		 5 3.8895896675373502 7 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff52_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.6303919141490808 2 2.7419387649413962
		 3 2.6303919141490808 4 2.7483903686115547 5 2.6303919141490808 6 2.7512301892924267
		 7 2.6303919141490808 8 2.7631169333875567;
createNode animCurveTL -n "rigw:tracks:stuff52_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.4985456653335545 2 -3.0679406927839437
		 3 -3.4985456653335545 4 -3.0430355287066315 5 -3.4985456653335545 6 -3.032072952016688
		 7 -3.4985456653335545 8 -2.986186482982093;
createNode animCurveTA -n "rigw:tracks:stuff52_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -14.523049474597972 3 -14.523049474597972
		 5 -14.523049474597972 7 -14.523049474597972;
createNode animCurveTA -n "rigw:tracks:stuff52_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff52_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff52_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff52_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff52_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff53_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff53_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373502 3 3.8895896675373502
		 5 3.8895896675373502 7 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff53_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.3224732280064515 2 2.5028265244236576
		 3 2.3224732280064515 4 2.5132577287187656 5 2.3224732280064515 6 2.5178492614907895
		 7 2.3224732280064515 8 2.5370682126848134;
createNode animCurveTL -n "rigw:tracks:stuff53_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -4.4542731959710009 2 -4.0476578608229259
		 3 -4.4542731959710009 4 -4.024140200127432 5 -4.4542731959710009 6 -4.0137883647352979
		 7 -4.4542731959710009 8 -3.9704582984920078;
createNode animCurveTA -n "rigw:tracks:stuff53_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -23.919569992035186 3 -23.919569992035186
		 5 -23.919569992035186 7 -23.919569992035186;
createNode animCurveTA -n "rigw:tracks:stuff53_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff53_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff53_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff53_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff53_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff54_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff54_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373502 3 3.8895896675373502
		 5 3.8895896675373502 7 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff54_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.8872206549298032 2 2.0675739513470086
		 3 1.8872206549298032 4 2.0780051556421175 5 1.8872206549298032 6 2.0825966884141409
		 7 1.8872206549298032 8 2.1018156396081653;
createNode animCurveTL -n "rigw:tracks:stuff54_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -5.4355714277827447 2 -5.0289560926346697
		 3 -5.4355714277827447 4 -5.0054384319391758 5 -5.4355714277827447 6 -4.9950865965470417
		 7 -5.4355714277827447 8 -4.9517565303037525;
createNode animCurveTA -n "rigw:tracks:stuff54_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -23.919569992035186 3 -23.919569992035186
		 5 -23.919569992035186 7 -23.919569992035186;
createNode animCurveTA -n "rigw:tracks:stuff54_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff54_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff54_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff54_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff54_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff55_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff55_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373502 3 3.8895896675373502
		 5 3.8895896675373502 7 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff55_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.4519680818531553 2 1.6323213782703605
		 3 1.4519680818531553 4 1.6427525825654694 5 1.4519680818531553 6 1.6473441153374926
		 7 1.4519680818531553 8 1.6665630665315172;
createNode animCurveTL -n "rigw:tracks:stuff55_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -6.4168696595944885 2 -6.0102543244464135
		 3 -6.4168696595944885 4 -5.9867366637509196 5 -6.4168696595944885 6 -5.9763848283587855
		 7 -6.4168696595944885 8 -5.9330547621154963;
createNode animCurveTA -n "rigw:tracks:stuff55_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -23.919569992035186 3 -23.919569992035186
		 5 -23.919569992035186 7 -23.919569992035186;
createNode animCurveTA -n "rigw:tracks:stuff55_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff55_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff55_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff55_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff55_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.99999999999999989 3 0.99999999999999989
		 5 0.99999999999999989 7 0.99999999999999989;
createNode animCurveTU -n "rigw:tracks:stuff68_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff68_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373538 3 3.8895896675373538
		 5 3.8895896675373538 7 3.8895896675373538;
createNode animCurveTL -n "rigw:tracks:stuff68_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.78689185839297315 2 1.0479088894650594
		 3 0.78689185839297315 4 1.0479088894650594 5 0.78689185839297315 6 1.0479088894650594
		 7 0.78689185839297315 8 1.0479088894650594;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.12667679786682129 0.1266767829656601 
		0.12667679786682129 0.1266767829656601 0.12667679786682129;
	setAttr -s 8 ".kiy[3:7]"  0.99194395542144775 -0.99194395542144775 
		0.99194395542144775 -0.99194395542144775 0.99194395542144775;
	setAttr -s 8 ".kox[3:7]"  0.1266767829656601 0.1266767829656601 0.1266767829656601 
		0.12667684257030487 0.1266767829656601;
	setAttr -s 8 ".koy[3:7]"  -0.99194395542144775 0.99194395542144775 
		-0.99194395542144775 0.99194395542144775 -0.99194395542144775;
createNode animCurveTL -n "rigw:tracks:stuff68_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -6.8744340999844447 2 -6.6511228121307573
		 3 -6.8744340999844447 4 -6.6511228121307573 5 -6.8744340999844447 6 -6.6511228121307573
		 7 -6.8744340999844447 8 -6.6511228121307573;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.14763282239437103 0.14763280749320984 
		0.14763282239437103 0.14763280749320984 0.14763282239437103;
	setAttr -s 8 ".kiy[3:7]"  0.98904222249984741 -0.98904222249984741 
		0.98904222249984741 -0.98904222249984741 0.98904222249984741;
	setAttr -s 8 ".kox[3:7]"  0.14763280749320984 0.14763280749320984 
		0.14763280749320984 0.14763288199901581 0.14763280749320984;
	setAttr -s 8 ".koy[3:7]"  -0.98904222249984741 0.98904222249984741 
		-0.98904222249984741 0.98904222249984741 -0.98904222249984741;
createNode animCurveTA -n "rigw:tracks:stuff68_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -90.735332873776329 2 -51.256582840080377
		 3 -90.735332873776329 4 -51.256582840080377 5 -90.735332873776329 6 -51.256582840080377
		 7 -90.735332873776329 8 -51.256582840080377;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.048320390284061432 0.048320382833480835 
		0.048320390284061432 0.048320382833480835 0.048320390284061432;
	setAttr -s 8 ".kiy[3:7]"  0.99883192777633667 -0.99883192777633667 
		0.99883192777633667 -0.99883192777633667 0.99883192777633667;
	setAttr -s 8 ".kox[3:7]"  0.048320382833480835 0.048320382833480835 
		0.048320382833480835 0.048320405185222626 0.048320382833480835;
	setAttr -s 8 ".koy[3:7]"  -0.99883192777633667 0.99883192777633667 
		-0.99883192777633667 0.99883192777633667 -0.99883192777633667;
createNode animCurveTA -n "rigw:tracks:stuff68_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff68_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff68_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff68_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff68_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff67_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff67_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373538 3 3.8895896675373538
		 5 3.8895896675373538 7 3.8895896675373538;
createNode animCurveTL -n "rigw:tracks:stuff67_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.077125482214109045 2 0.38111706749476215
		 3 0.077125482214109045 4 0.38111706749476215 5 0.077125482214109045 6 0.38111706749476215
		 7 0.077125482214109045 8 0.38111706749476215;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.10899884253740311 0.10899882763624191 
		0.10899884253740311 0.10899882763624191 0.10899884253740311;
	setAttr -s 8 ".kiy[3:7]"  0.99404186010360718 -0.99404186010360718 
		0.99404186010360718 -0.99404186010360718 0.99404186010360718;
	setAttr -s 8 ".kox[3:7]"  0.10899882763624191 0.10899882763624191 
		0.10899882763624191 0.10899887233972549 0.10899882763624191;
	setAttr -s 8 ".koy[3:7]"  -0.99404186010360718 0.99404186010360718 
		-0.99404186010360718 0.99404186010360718 -0.99404186010360718;
createNode animCurveTL -n "rigw:tracks:stuff67_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -6.4850333431961635 2 -6.6986860096231791
		 3 -6.4850333431961635 4 -6.6986860096231791 5 -6.4850333431961635 6 -6.6986860096231791
		 7 -6.4850333431961635 8 -6.6986860096231791;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.15415163338184357 0.15415160357952118 
		0.15415163338184357 0.15415160357952118 0.15415163338184357;
	setAttr -s 8 ".kiy[3:7]"  -0.98804718255996704 0.98804718255996704 
		-0.98804718255996704 0.98804718255996704 -0.98804718255996704;
	setAttr -s 8 ".kox[3:7]"  0.15415160357952118 0.15415160357952118 
		0.15415160357952118 0.15415167808532715 0.15415160357952118;
	setAttr -s 8 ".koy[3:7]"  0.98804718255996704 -0.98804718255996704 
		0.98804718255996704 -0.98804718255996704 0.98804718255996704;
createNode animCurveTA -n "rigw:tracks:stuff67_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -151.21666857308904 2 -117.00779993362832
		 3 -151.21666857308904 4 -117.00779993362832 5 -151.21666857308904 6 -117.00779993362832
		 7 -151.21666857308904 8 -117.00779993362832;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.055742558091878891 0.055742550641298294 
		0.055742558091878891 0.055742550641298294 0.055742558091878891;
	setAttr -s 8 ".kiy[3:7]"  0.99844515323638916 -0.99844515323638916 
		0.99844515323638916 -0.99844515323638916 0.99844515323638916;
	setAttr -s 8 ".kox[3:7]"  0.055742550641298294 0.055742550641298294 
		0.055742550641298294 0.055742576718330383 0.055742550641298294;
	setAttr -s 8 ".koy[3:7]"  -0.99844515323638916 0.99844515323638916 
		-0.99844515323638916 0.99844515323638916 -0.99844515323638916;
createNode animCurveTA -n "rigw:tracks:stuff67_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff67_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff67_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff67_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1.0000000000000002 3 1.0000000000000002
		 5 1.0000000000000002 7 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff67_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1.0000000000000002 3 1.0000000000000002
		 5 1.0000000000000002 7 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff66_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff66_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373529 3 3.8895896675373529
		 5 3.8895896675373529 7 3.8895896675373529;
createNode animCurveTL -n "rigw:tracks:stuff66_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590271651 3 0.0036092918590271651
		 5 0.0036092918590271651 7 0.0036092918590271651;
createNode animCurveTL -n "rigw:tracks:stuff66_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -5.4488368443595636 2 -5.8936551694161379
		 3 -5.4488368443595636 4 -5.8936551694161379 5 -5.4488368443595636 6 -5.8936551694161379
		 7 -5.4488368443595636 8 -5.8936551694161379;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.074727445840835571 0.074727430939674377 
		0.074727445840835571 0.074727430939674377 0.074727445840835571;
	setAttr -s 8 ".kiy[3:7]"  -0.9972040057182312 0.9972040057182312 
		-0.9972040057182312 0.9972040057182312 -0.9972040057182312;
	setAttr -s 8 ".kox[3:7]"  0.074727430939674377 0.074727430939674377 
		0.074727430939674377 0.074727468192577362 0.074727430939674377;
	setAttr -s 8 ".koy[3:7]"  0.9972040057182312 -0.9972040057182312 
		0.9972040057182312 -0.9972040057182312 0.9972040057182312;
createNode animCurveTA -n "rigw:tracks:stuff66_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 180 2 191.85923375188665 3 180 4 191.85923375188665
		 5 180 6 191.85923375188665 7 180 8 191.85923375188665;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.15899549424648285 0.15899547934532166 
		0.15899549424648285 0.15899547934532166 0.15899549424648285;
	setAttr -s 8 ".kiy[3:7]"  0.98727929592132568 -0.98727929592132568 
		0.98727929592132568 -0.98727929592132568 0.98727929592132568;
	setAttr -s 8 ".kox[3:7]"  0.15899547934532166 0.15899547934532166 
		0.15899547934532166 0.15899553894996643 0.15899547934532166;
	setAttr -s 8 ".koy[3:7]"  -0.98727929592132568 0.98727929592132568 
		-0.98727929592132568 0.98727929592132568 -0.98727929592132568;
createNode animCurveTA -n "rigw:tracks:stuff66_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff66_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff66_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff66_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff66_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff65_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff65_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.889589667537352 3 3.889589667537352
		 5 3.889589667537352 7 3.889589667537352;
createNode animCurveTL -n "rigw:tracks:stuff65_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590272896 3 0.0036092918590272896
		 5 0.0036092918590272896 7 0.0036092918590272896;
createNode animCurveTL -n "rigw:tracks:stuff65_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -4.429445288819756 2 -4.8742636138763302
		 3 -4.429445288819756 4 -4.8999908443459033 5 -4.429445288819756 6 -4.9113152723306799
		 7 -4.429445288819756 8 -4.958716355922153;
createNode animCurveTA -n "rigw:tracks:stuff65_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff65_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff65_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff65_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff65_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff65_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff64_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff64_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.889589667537352 3 3.889589667537352
		 5 3.889589667537352 7 3.889589667537352;
createNode animCurveTL -n "rigw:tracks:stuff64_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.003609291859027414 3 0.003609291859027414
		 5 0.003609291859027414 7 0.003609291859027414;
createNode animCurveTL -n "rigw:tracks:stuff64_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -3.4100537332799483 2 -3.8548720583365186
		 3 -3.4100537332799483 4 -3.8805992888060952 5 -3.4100537332799483 6 -3.8919237167908727
		 7 -3.4100537332799483 8 -3.9393248003823458;
createNode animCurveTA -n "rigw:tracks:stuff64_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff64_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff64_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff64_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff64_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff64_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff63_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff63_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373511 3 3.8895896675373511
		 5 3.8895896675373511 7 3.8895896675373511;
createNode animCurveTL -n "rigw:tracks:stuff63_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590275385 3 0.0036092918590275385
		 5 0.0036092918590275385 7 0.0036092918590275385;
createNode animCurveTL -n "rigw:tracks:stuff63_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -2.3906621777401407 2 -2.8354805027967109
		 3 -2.3906621777401407 4 -2.8612077332662875 5 -2.3906621777401407 6 -2.8725321612510655
		 7 -2.3906621777401407 8 -2.9199332448425381;
createNode animCurveTA -n "rigw:tracks:stuff63_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff63_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff63_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff63_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff63_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff63_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff62_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff62_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373511 3 3.8895896675373511
		 5 3.8895896675373511 7 3.8895896675373511;
createNode animCurveTL -n "rigw:tracks:stuff62_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.003609291859027663 3 0.003609291859027663
		 5 0.003609291859027663 7 0.003609291859027663;
createNode animCurveTL -n "rigw:tracks:stuff62_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -1.371270622200333 2 -1.8160889472569053
		 3 -1.371270622200333 4 -1.8418161777264803 5 -1.371270622200333 6 -1.8531406057112576
		 7 -1.371270622200333 8 -1.9005416893027312;
createNode animCurveTA -n "rigw:tracks:stuff62_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff62_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff62_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff62_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff62_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff62_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff61_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff61_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373502 3 3.8895896675373502
		 5 3.8895896675373502 7 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff61_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590277874 3 0.0036092918590277874
		 5 0.0036092918590277874 7 0.0036092918590277874;
createNode animCurveTL -n "rigw:tracks:stuff61_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 -0.35187906666052537 2 -0.79669739171709708
		 3 -0.35187906666052537 4 -0.822424622186673 5 -0.35187906666052537 6 -0.83374905017144996
		 7 -0.35187906666052537 8 -0.88115013376292384;
createNode animCurveTA -n "rigw:tracks:stuff61_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff61_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff61_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff61_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff61_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff61_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff60_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff60_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373502 3 3.8895896675373502
		 5 3.8895896675373502 7 3.8895896675373502;
createNode animCurveTL -n "rigw:tracks:stuff60_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590279119 3 0.0036092918590279119
		 5 0.0036092918590279119 7 0.0036092918590279119;
createNode animCurveTL -n "rigw:tracks:stuff60_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.66751248887928227 2 0.2226941638227104
		 3 0.66751248887928227 4 0.1969669333531347 5 0.66751248887928227 6 0.18564250536835791
		 7 0.66751248887928227 8 0.13824142177688375;
createNode animCurveTA -n "rigw:tracks:stuff60_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff60_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff60_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff60_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff60_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff60_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff59_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff59_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373498 3 3.8895896675373498
		 5 3.8895896675373498 7 3.8895896675373498;
createNode animCurveTL -n "rigw:tracks:stuff59_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590280364 3 0.0036092918590280364
		 5 0.0036092918590280364 7 0.0036092918590280364;
createNode animCurveTL -n "rigw:tracks:stuff59_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 1.6869040444190899 2 1.2420857193625177
		 3 1.6869040444190899 4 1.2163584888929424 5 1.6869040444190899 6 1.2050340609081651
		 7 1.6869040444190899 8 1.1576329773166916;
createNode animCurveTA -n "rigw:tracks:stuff59_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff59_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff59_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff59_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff59_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff59_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff58_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff58_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373489 3 3.8895896675373489
		 5 3.8895896675373489 7 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff58_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590281608 3 0.0036092918590281608
		 5 0.0036092918590281608 7 0.0036092918590281608;
createNode animCurveTL -n "rigw:tracks:stuff58_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 2.7062955999588976 2 2.2614772749023273
		 3 2.7062955999588976 4 2.2357500444327507 5 2.7062955999588976 6 2.2244256164479728
		 7 2.7062955999588976 8 2.1770245328565001;
createNode animCurveTA -n "rigw:tracks:stuff58_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff58_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff58_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff58_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff58_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff58_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff57_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff57_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373489 3 3.8895896675373489
		 5 3.8895896675373489 7 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff57_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590282853 3 0.0036092918590282853
		 5 0.0036092918590282853 7 0.0036092918590282853;
createNode animCurveTL -n "rigw:tracks:stuff57_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 3.7256871554987057 2 3.280868830442135
		 3 3.7256871554987057 4 3.2551415999725588 5 3.7256871554987057 6 3.2438171719877809
		 7 3.7256871554987057 8 3.1964160883963082;
createNode animCurveTA -n "rigw:tracks:stuff57_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff57_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff57_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff57_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff57_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff57_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff56_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff56_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373489 3 3.8895896675373489
		 5 3.8895896675373489 7 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff56_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0.0036092918590284093 3 0.0036092918590284093
		 5 0.0036092918590284093 7 0.0036092918590284093;
createNode animCurveTL -n "rigw:tracks:stuff56_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 4.7450787110385129 2 4.3002603859819386
		 3 4.7450787110385129 4 4.2745331555123656 5 4.7450787110385129 6 4.2632087275275889
		 7 4.7450787110385129 8 4.215807643936115;
createNode animCurveTA -n "rigw:tracks:stuff56_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 180 3 180 5 180 7 180;
createNode animCurveTA -n "rigw:tracks:stuff56_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff56_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff56_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff56_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff56_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff69_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTL -n "rigw:tracks:stuff69_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 3.8895896675373489 3 3.8895896675373489
		 5 3.8895896675373489 7 3.8895896675373489;
createNode animCurveTL -n "rigw:tracks:stuff69_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0.11714647165146076 2 -0.0017393210588157468
		 3 0.11714647165146076 4 -0.0017393210588157468 5 0.11714647165146076 6 -0.0017393210588157468
		 7 0.11714647165146076 8 -0.0017393210588157468;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.26997023820877075 0.26997020840644836 
		0.26997023820877075 0.26997020840644836 0.26997023820877075;
	setAttr -s 8 ".kiy[3:7]"  -0.96286869049072266 0.96286869049072266 
		-0.96286869049072266 0.96286869049072266 -0.96286869049072266;
	setAttr -s 8 ".kox[3:7]"  0.26997020840644836 0.26997020840644836 
		0.26997020840644836 0.26997032761573792 0.26997020840644836;
	setAttr -s 8 ".koy[3:7]"  0.96286869049072266 -0.96286869049072266 
		0.96286869049072266 -0.96286869049072266 0.96286869049072266;
createNode animCurveTL -n "rigw:tracks:stuff69_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 5.7205885340857723 2 5.2919517290890683
		 3 5.7205885340857723 4 5.2919517290890683 5 5.7205885340857723 6 5.2919517290890683
		 7 5.7205885340857723 8 5.2919517290890683;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.077531829476356506 0.077531822025775909 
		0.077531829476356506 0.077531822025775909 0.077531829476356506;
	setAttr -s 8 ".kiy[3:7]"  -0.996989905834198 0.996989905834198 -0.996989905834198 
		0.996989905834198 -0.996989905834198;
	setAttr -s 8 ".kox[3:7]"  0.077531822025775909 0.077531822025775909 
		0.077531822025775909 0.077531859278678894 0.077531822025775909;
	setAttr -s 8 ".koy[3:7]"  0.996989905834198 -0.996989905834198 0.996989905834198 
		-0.996989905834198 0.996989905834198;
createNode animCurveTA -n "rigw:tracks:stuff69_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 164.49822676162222 2 173.26586020557659
		 3 164.49822676162222 4 173.26586020557659 5 164.49822676162222 6 173.26586020557659
		 7 164.49822676162222 8 173.26586020557659;
	setAttr -s 8 ".kit[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kot[3:7]"  1 2 1 2 1;
	setAttr -s 8 ".kix[3:7]"  0.2128395289182663 0.2128395140171051 0.2128395289182663 
		0.2128395140171051 0.2128395289182663;
	setAttr -s 8 ".kiy[3:7]"  0.97708714008331299 -0.97708714008331299 
		0.97708714008331299 -0.97708714008331299 0.97708714008331299;
	setAttr -s 8 ".kox[3:7]"  0.2128395140171051 0.2128395140171051 0.2128395140171051 
		0.21283960342407227 0.2128395140171051;
	setAttr -s 8 ".koy[3:7]"  -0.97708714008331299 0.97708714008331299 
		-0.97708714008331299 0.97708714008331299 -0.97708714008331299;
createNode animCurveTA -n "rigw:tracks:stuff69_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTA -n "rigw:tracks:stuff69_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 3 0 5 0 7 0;
createNode animCurveTU -n "rigw:tracks:stuff69_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 3 1 5 1 7 1;
createNode animCurveTU -n "rigw:tracks:stuff69_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1.0000000000000002 3 1.0000000000000002
		 5 1.0000000000000002 7 1.0000000000000002;
createNode animCurveTU -n "rigw:tracks:stuff69_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1.0000000000000002 3 1.0000000000000002
		 5 1.0000000000000002 7 1.0000000000000002;
createNode reference -n "everlifeRN";
	setAttr ".ed" -type "dataReferenceEdits" 
		"everlifeRN"
		"everlifeRN" 0;
lockNode -l 1 ;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 17 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -s 64 ".gn";
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 24 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 36 ".tx";
select -ne :lightList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 6 ".l";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 48 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 4 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren" -type "string" "mentalRay";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 32;
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar" 0;
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn" no;
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff" yes;
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultRenderQuality;
	setAttr ".ert" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w" 1024;
	setAttr -av ".h" 1024;
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al" yes;
	setAttr -av ".dar" 1;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -s 6 ".dsm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
connectAttr "rigw:tracks:stuff51_translateX.o" "rigwRN.phl[185]";
connectAttr "rigw:tracks:stuff51_translateY.o" "rigwRN.phl[186]";
connectAttr "rigw:tracks:stuff51_translateZ.o" "rigwRN.phl[187]";
connectAttr "rigw:tracks:stuff51_rotateX.o" "rigwRN.phl[188]";
connectAttr "rigw:tracks:stuff51_rotateY.o" "rigwRN.phl[189]";
connectAttr "rigw:tracks:stuff51_rotateZ.o" "rigwRN.phl[190]";
connectAttr "rigw:tracks:stuff51_visibility.o" "rigwRN.phl[191]";
connectAttr "rigw:tracks:stuff51_scaleX.o" "rigwRN.phl[192]";
connectAttr "rigw:tracks:stuff51_scaleY.o" "rigwRN.phl[193]";
connectAttr "rigw:tracks:stuff51_scaleZ.o" "rigwRN.phl[194]";
connectAttr "rigw:tracks:stuff50_translateX.o" "rigwRN.phl[195]";
connectAttr "rigw:tracks:stuff50_translateY.o" "rigwRN.phl[196]";
connectAttr "rigw:tracks:stuff50_translateZ.o" "rigwRN.phl[197]";
connectAttr "rigw:tracks:stuff50_rotateX.o" "rigwRN.phl[198]";
connectAttr "rigw:tracks:stuff50_rotateY.o" "rigwRN.phl[199]";
connectAttr "rigw:tracks:stuff50_rotateZ.o" "rigwRN.phl[200]";
connectAttr "rigw:tracks:stuff50_visibility.o" "rigwRN.phl[201]";
connectAttr "rigw:tracks:stuff50_scaleX.o" "rigwRN.phl[202]";
connectAttr "rigw:tracks:stuff50_scaleY.o" "rigwRN.phl[203]";
connectAttr "rigw:tracks:stuff50_scaleZ.o" "rigwRN.phl[204]";
connectAttr "rigw:tracks:stuff49_translateX.o" "rigwRN.phl[205]";
connectAttr "rigw:tracks:stuff49_translateY.o" "rigwRN.phl[206]";
connectAttr "rigw:tracks:stuff49_translateZ.o" "rigwRN.phl[207]";
connectAttr "rigw:tracks:stuff49_visibility.o" "rigwRN.phl[208]";
connectAttr "rigw:tracks:stuff49_rotateX.o" "rigwRN.phl[209]";
connectAttr "rigw:tracks:stuff49_rotateY.o" "rigwRN.phl[210]";
connectAttr "rigw:tracks:stuff49_rotateZ.o" "rigwRN.phl[211]";
connectAttr "rigw:tracks:stuff49_scaleX.o" "rigwRN.phl[212]";
connectAttr "rigw:tracks:stuff49_scaleY.o" "rigwRN.phl[213]";
connectAttr "rigw:tracks:stuff49_scaleZ.o" "rigwRN.phl[214]";
connectAttr "rigw:tracks:stuff48_translateX.o" "rigwRN.phl[215]";
connectAttr "rigw:tracks:stuff48_translateY.o" "rigwRN.phl[216]";
connectAttr "rigw:tracks:stuff48_translateZ.o" "rigwRN.phl[217]";
connectAttr "rigw:tracks:stuff48_visibility.o" "rigwRN.phl[218]";
connectAttr "rigw:tracks:stuff48_rotateX.o" "rigwRN.phl[219]";
connectAttr "rigw:tracks:stuff48_rotateY.o" "rigwRN.phl[220]";
connectAttr "rigw:tracks:stuff48_rotateZ.o" "rigwRN.phl[221]";
connectAttr "rigw:tracks:stuff48_scaleX.o" "rigwRN.phl[222]";
connectAttr "rigw:tracks:stuff48_scaleY.o" "rigwRN.phl[223]";
connectAttr "rigw:tracks:stuff48_scaleZ.o" "rigwRN.phl[224]";
connectAttr "rigw:tracks:stuff42_translateX.o" "rigwRN.phl[225]";
connectAttr "rigw:tracks:stuff42_translateY.o" "rigwRN.phl[226]";
connectAttr "rigw:tracks:stuff42_translateZ.o" "rigwRN.phl[227]";
connectAttr "rigw:tracks:stuff42_rotateX.o" "rigwRN.phl[228]";
connectAttr "rigw:tracks:stuff42_rotateY.o" "rigwRN.phl[229]";
connectAttr "rigw:tracks:stuff42_rotateZ.o" "rigwRN.phl[230]";
connectAttr "rigw:tracks:stuff42_visibility.o" "rigwRN.phl[231]";
connectAttr "rigw:tracks:stuff42_scaleX.o" "rigwRN.phl[232]";
connectAttr "rigw:tracks:stuff42_scaleY.o" "rigwRN.phl[233]";
connectAttr "rigw:tracks:stuff42_scaleZ.o" "rigwRN.phl[234]";
connectAttr "rigw:tracks:stuff43_translateX.o" "rigwRN.phl[235]";
connectAttr "rigw:tracks:stuff43_translateY.o" "rigwRN.phl[236]";
connectAttr "rigw:tracks:stuff43_translateZ.o" "rigwRN.phl[237]";
connectAttr "rigw:tracks:stuff43_rotateX.o" "rigwRN.phl[238]";
connectAttr "rigw:tracks:stuff43_rotateY.o" "rigwRN.phl[239]";
connectAttr "rigw:tracks:stuff43_rotateZ.o" "rigwRN.phl[240]";
connectAttr "rigw:tracks:stuff43_visibility.o" "rigwRN.phl[241]";
connectAttr "rigw:tracks:stuff43_scaleX.o" "rigwRN.phl[242]";
connectAttr "rigw:tracks:stuff43_scaleY.o" "rigwRN.phl[243]";
connectAttr "rigw:tracks:stuff43_scaleZ.o" "rigwRN.phl[244]";
connectAttr "rigw:tracks:stuff44_translateX.o" "rigwRN.phl[245]";
connectAttr "rigw:tracks:stuff44_translateY.o" "rigwRN.phl[246]";
connectAttr "rigw:tracks:stuff44_translateZ.o" "rigwRN.phl[247]";
connectAttr "rigw:tracks:stuff44_visibility.o" "rigwRN.phl[248]";
connectAttr "rigw:tracks:stuff44_rotateX.o" "rigwRN.phl[249]";
connectAttr "rigw:tracks:stuff44_rotateY.o" "rigwRN.phl[250]";
connectAttr "rigw:tracks:stuff44_rotateZ.o" "rigwRN.phl[251]";
connectAttr "rigw:tracks:stuff44_scaleX.o" "rigwRN.phl[252]";
connectAttr "rigw:tracks:stuff44_scaleY.o" "rigwRN.phl[253]";
connectAttr "rigw:tracks:stuff44_scaleZ.o" "rigwRN.phl[254]";
connectAttr "rigw:tracks:stuff45_translateX.o" "rigwRN.phl[255]";
connectAttr "rigw:tracks:stuff45_translateY.o" "rigwRN.phl[256]";
connectAttr "rigw:tracks:stuff45_translateZ.o" "rigwRN.phl[257]";
connectAttr "rigw:tracks:stuff45_visibility.o" "rigwRN.phl[258]";
connectAttr "rigw:tracks:stuff45_rotateX.o" "rigwRN.phl[259]";
connectAttr "rigw:tracks:stuff45_rotateY.o" "rigwRN.phl[260]";
connectAttr "rigw:tracks:stuff45_rotateZ.o" "rigwRN.phl[261]";
connectAttr "rigw:tracks:stuff45_scaleX.o" "rigwRN.phl[262]";
connectAttr "rigw:tracks:stuff45_scaleY.o" "rigwRN.phl[263]";
connectAttr "rigw:tracks:stuff45_scaleZ.o" "rigwRN.phl[264]";
connectAttr "rigw:tracks:stuff46_translateX.o" "rigwRN.phl[265]";
connectAttr "rigw:tracks:stuff46_translateY.o" "rigwRN.phl[266]";
connectAttr "rigw:tracks:stuff46_translateZ.o" "rigwRN.phl[267]";
connectAttr "rigw:tracks:stuff46_visibility.o" "rigwRN.phl[268]";
connectAttr "rigw:tracks:stuff46_rotateX.o" "rigwRN.phl[269]";
connectAttr "rigw:tracks:stuff46_rotateY.o" "rigwRN.phl[270]";
connectAttr "rigw:tracks:stuff46_rotateZ.o" "rigwRN.phl[271]";
connectAttr "rigw:tracks:stuff46_scaleX.o" "rigwRN.phl[272]";
connectAttr "rigw:tracks:stuff46_scaleY.o" "rigwRN.phl[273]";
connectAttr "rigw:tracks:stuff46_scaleZ.o" "rigwRN.phl[274]";
connectAttr "rigw:tracks:stuff47_translateX.o" "rigwRN.phl[275]";
connectAttr "rigw:tracks:stuff47_translateY.o" "rigwRN.phl[276]";
connectAttr "rigw:tracks:stuff47_translateZ.o" "rigwRN.phl[277]";
connectAttr "rigw:tracks:stuff47_visibility.o" "rigwRN.phl[278]";
connectAttr "rigw:tracks:stuff47_rotateX.o" "rigwRN.phl[279]";
connectAttr "rigw:tracks:stuff47_rotateY.o" "rigwRN.phl[280]";
connectAttr "rigw:tracks:stuff47_rotateZ.o" "rigwRN.phl[281]";
connectAttr "rigw:tracks:stuff47_scaleX.o" "rigwRN.phl[282]";
connectAttr "rigw:tracks:stuff47_scaleY.o" "rigwRN.phl[283]";
connectAttr "rigw:tracks:stuff47_scaleZ.o" "rigwRN.phl[284]";
connectAttr "rigw:tracks:stuff52_translateX.o" "rigwRN.phl[285]";
connectAttr "rigw:tracks:stuff52_translateY.o" "rigwRN.phl[286]";
connectAttr "rigw:tracks:stuff52_translateZ.o" "rigwRN.phl[287]";
connectAttr "rigw:tracks:stuff52_visibility.o" "rigwRN.phl[288]";
connectAttr "rigw:tracks:stuff52_rotateX.o" "rigwRN.phl[289]";
connectAttr "rigw:tracks:stuff52_rotateY.o" "rigwRN.phl[290]";
connectAttr "rigw:tracks:stuff52_rotateZ.o" "rigwRN.phl[291]";
connectAttr "rigw:tracks:stuff52_scaleX.o" "rigwRN.phl[292]";
connectAttr "rigw:tracks:stuff52_scaleY.o" "rigwRN.phl[293]";
connectAttr "rigw:tracks:stuff52_scaleZ.o" "rigwRN.phl[294]";
connectAttr "rigw:tracks:stuff53_translateX.o" "rigwRN.phl[295]";
connectAttr "rigw:tracks:stuff53_translateY.o" "rigwRN.phl[296]";
connectAttr "rigw:tracks:stuff53_translateZ.o" "rigwRN.phl[297]";
connectAttr "rigw:tracks:stuff53_visibility.o" "rigwRN.phl[298]";
connectAttr "rigw:tracks:stuff53_rotateX.o" "rigwRN.phl[299]";
connectAttr "rigw:tracks:stuff53_rotateY.o" "rigwRN.phl[300]";
connectAttr "rigw:tracks:stuff53_rotateZ.o" "rigwRN.phl[301]";
connectAttr "rigw:tracks:stuff53_scaleX.o" "rigwRN.phl[302]";
connectAttr "rigw:tracks:stuff53_scaleY.o" "rigwRN.phl[303]";
connectAttr "rigw:tracks:stuff53_scaleZ.o" "rigwRN.phl[304]";
connectAttr "rigw:tracks:stuff54_translateX.o" "rigwRN.phl[305]";
connectAttr "rigw:tracks:stuff54_translateY.o" "rigwRN.phl[306]";
connectAttr "rigw:tracks:stuff54_translateZ.o" "rigwRN.phl[307]";
connectAttr "rigw:tracks:stuff54_visibility.o" "rigwRN.phl[308]";
connectAttr "rigw:tracks:stuff54_rotateX.o" "rigwRN.phl[309]";
connectAttr "rigw:tracks:stuff54_rotateY.o" "rigwRN.phl[310]";
connectAttr "rigw:tracks:stuff54_rotateZ.o" "rigwRN.phl[311]";
connectAttr "rigw:tracks:stuff54_scaleX.o" "rigwRN.phl[312]";
connectAttr "rigw:tracks:stuff54_scaleY.o" "rigwRN.phl[313]";
connectAttr "rigw:tracks:stuff54_scaleZ.o" "rigwRN.phl[314]";
connectAttr "rigw:tracks:stuff55_translateX.o" "rigwRN.phl[315]";
connectAttr "rigw:tracks:stuff55_translateY.o" "rigwRN.phl[316]";
connectAttr "rigw:tracks:stuff55_translateZ.o" "rigwRN.phl[317]";
connectAttr "rigw:tracks:stuff55_visibility.o" "rigwRN.phl[318]";
connectAttr "rigw:tracks:stuff55_rotateX.o" "rigwRN.phl[319]";
connectAttr "rigw:tracks:stuff55_rotateY.o" "rigwRN.phl[320]";
connectAttr "rigw:tracks:stuff55_rotateZ.o" "rigwRN.phl[321]";
connectAttr "rigw:tracks:stuff55_scaleX.o" "rigwRN.phl[322]";
connectAttr "rigw:tracks:stuff55_scaleY.o" "rigwRN.phl[323]";
connectAttr "rigw:tracks:stuff55_scaleZ.o" "rigwRN.phl[324]";
connectAttr "rigw:tracks:stuff68_translateX.o" "rigwRN.phl[325]";
connectAttr "rigw:tracks:stuff68_translateY.o" "rigwRN.phl[326]";
connectAttr "rigw:tracks:stuff68_translateZ.o" "rigwRN.phl[327]";
connectAttr "rigw:tracks:stuff68_rotateX.o" "rigwRN.phl[328]";
connectAttr "rigw:tracks:stuff68_rotateY.o" "rigwRN.phl[329]";
connectAttr "rigw:tracks:stuff68_rotateZ.o" "rigwRN.phl[330]";
connectAttr "rigw:tracks:stuff68_visibility.o" "rigwRN.phl[331]";
connectAttr "rigw:tracks:stuff68_scaleX.o" "rigwRN.phl[332]";
connectAttr "rigw:tracks:stuff68_scaleY.o" "rigwRN.phl[333]";
connectAttr "rigw:tracks:stuff68_scaleZ.o" "rigwRN.phl[334]";
connectAttr "rigw:tracks:stuff67_translateX.o" "rigwRN.phl[335]";
connectAttr "rigw:tracks:stuff67_translateY.o" "rigwRN.phl[336]";
connectAttr "rigw:tracks:stuff67_translateZ.o" "rigwRN.phl[337]";
connectAttr "rigw:tracks:stuff67_rotateX.o" "rigwRN.phl[338]";
connectAttr "rigw:tracks:stuff67_rotateY.o" "rigwRN.phl[339]";
connectAttr "rigw:tracks:stuff67_rotateZ.o" "rigwRN.phl[340]";
connectAttr "rigw:tracks:stuff67_visibility.o" "rigwRN.phl[341]";
connectAttr "rigw:tracks:stuff67_scaleX.o" "rigwRN.phl[342]";
connectAttr "rigw:tracks:stuff67_scaleY.o" "rigwRN.phl[343]";
connectAttr "rigw:tracks:stuff67_scaleZ.o" "rigwRN.phl[344]";
connectAttr "rigw:tracks:stuff66_translateX.o" "rigwRN.phl[345]";
connectAttr "rigw:tracks:stuff66_translateY.o" "rigwRN.phl[346]";
connectAttr "rigw:tracks:stuff66_translateZ.o" "rigwRN.phl[347]";
connectAttr "rigw:tracks:stuff66_rotateX.o" "rigwRN.phl[348]";
connectAttr "rigw:tracks:stuff66_rotateY.o" "rigwRN.phl[349]";
connectAttr "rigw:tracks:stuff66_rotateZ.o" "rigwRN.phl[350]";
connectAttr "rigw:tracks:stuff66_visibility.o" "rigwRN.phl[351]";
connectAttr "rigw:tracks:stuff66_scaleX.o" "rigwRN.phl[352]";
connectAttr "rigw:tracks:stuff66_scaleY.o" "rigwRN.phl[353]";
connectAttr "rigw:tracks:stuff66_scaleZ.o" "rigwRN.phl[354]";
connectAttr "rigw:tracks:stuff65_translateX.o" "rigwRN.phl[355]";
connectAttr "rigw:tracks:stuff65_translateY.o" "rigwRN.phl[356]";
connectAttr "rigw:tracks:stuff65_translateZ.o" "rigwRN.phl[357]";
connectAttr "rigw:tracks:stuff65_visibility.o" "rigwRN.phl[358]";
connectAttr "rigw:tracks:stuff65_rotateX.o" "rigwRN.phl[359]";
connectAttr "rigw:tracks:stuff65_rotateY.o" "rigwRN.phl[360]";
connectAttr "rigw:tracks:stuff65_rotateZ.o" "rigwRN.phl[361]";
connectAttr "rigw:tracks:stuff65_scaleX.o" "rigwRN.phl[362]";
connectAttr "rigw:tracks:stuff65_scaleY.o" "rigwRN.phl[363]";
connectAttr "rigw:tracks:stuff65_scaleZ.o" "rigwRN.phl[364]";
connectAttr "rigw:tracks:stuff64_translateX.o" "rigwRN.phl[365]";
connectAttr "rigw:tracks:stuff64_translateY.o" "rigwRN.phl[366]";
connectAttr "rigw:tracks:stuff64_translateZ.o" "rigwRN.phl[367]";
connectAttr "rigw:tracks:stuff64_visibility.o" "rigwRN.phl[368]";
connectAttr "rigw:tracks:stuff64_rotateX.o" "rigwRN.phl[369]";
connectAttr "rigw:tracks:stuff64_rotateY.o" "rigwRN.phl[370]";
connectAttr "rigw:tracks:stuff64_rotateZ.o" "rigwRN.phl[371]";
connectAttr "rigw:tracks:stuff64_scaleX.o" "rigwRN.phl[372]";
connectAttr "rigw:tracks:stuff64_scaleY.o" "rigwRN.phl[373]";
connectAttr "rigw:tracks:stuff64_scaleZ.o" "rigwRN.phl[374]";
connectAttr "rigw:tracks:stuff63_translateX.o" "rigwRN.phl[375]";
connectAttr "rigw:tracks:stuff63_translateY.o" "rigwRN.phl[376]";
connectAttr "rigw:tracks:stuff63_translateZ.o" "rigwRN.phl[377]";
connectAttr "rigw:tracks:stuff63_visibility.o" "rigwRN.phl[378]";
connectAttr "rigw:tracks:stuff63_rotateX.o" "rigwRN.phl[379]";
connectAttr "rigw:tracks:stuff63_rotateY.o" "rigwRN.phl[380]";
connectAttr "rigw:tracks:stuff63_rotateZ.o" "rigwRN.phl[381]";
connectAttr "rigw:tracks:stuff63_scaleX.o" "rigwRN.phl[382]";
connectAttr "rigw:tracks:stuff63_scaleY.o" "rigwRN.phl[383]";
connectAttr "rigw:tracks:stuff63_scaleZ.o" "rigwRN.phl[384]";
connectAttr "rigw:tracks:stuff62_translateX.o" "rigwRN.phl[385]";
connectAttr "rigw:tracks:stuff62_translateY.o" "rigwRN.phl[386]";
connectAttr "rigw:tracks:stuff62_translateZ.o" "rigwRN.phl[387]";
connectAttr "rigw:tracks:stuff62_visibility.o" "rigwRN.phl[388]";
connectAttr "rigw:tracks:stuff62_rotateX.o" "rigwRN.phl[389]";
connectAttr "rigw:tracks:stuff62_rotateY.o" "rigwRN.phl[390]";
connectAttr "rigw:tracks:stuff62_rotateZ.o" "rigwRN.phl[391]";
connectAttr "rigw:tracks:stuff62_scaleX.o" "rigwRN.phl[392]";
connectAttr "rigw:tracks:stuff62_scaleY.o" "rigwRN.phl[393]";
connectAttr "rigw:tracks:stuff62_scaleZ.o" "rigwRN.phl[394]";
connectAttr "rigw:tracks:stuff61_translateX.o" "rigwRN.phl[395]";
connectAttr "rigw:tracks:stuff61_translateY.o" "rigwRN.phl[396]";
connectAttr "rigw:tracks:stuff61_translateZ.o" "rigwRN.phl[397]";
connectAttr "rigw:tracks:stuff61_visibility.o" "rigwRN.phl[398]";
connectAttr "rigw:tracks:stuff61_rotateX.o" "rigwRN.phl[399]";
connectAttr "rigw:tracks:stuff61_rotateY.o" "rigwRN.phl[400]";
connectAttr "rigw:tracks:stuff61_rotateZ.o" "rigwRN.phl[401]";
connectAttr "rigw:tracks:stuff61_scaleX.o" "rigwRN.phl[402]";
connectAttr "rigw:tracks:stuff61_scaleY.o" "rigwRN.phl[403]";
connectAttr "rigw:tracks:stuff61_scaleZ.o" "rigwRN.phl[404]";
connectAttr "rigw:tracks:stuff60_translateX.o" "rigwRN.phl[405]";
connectAttr "rigw:tracks:stuff60_translateY.o" "rigwRN.phl[406]";
connectAttr "rigw:tracks:stuff60_translateZ.o" "rigwRN.phl[407]";
connectAttr "rigw:tracks:stuff60_visibility.o" "rigwRN.phl[408]";
connectAttr "rigw:tracks:stuff60_rotateX.o" "rigwRN.phl[409]";
connectAttr "rigw:tracks:stuff60_rotateY.o" "rigwRN.phl[410]";
connectAttr "rigw:tracks:stuff60_rotateZ.o" "rigwRN.phl[411]";
connectAttr "rigw:tracks:stuff60_scaleX.o" "rigwRN.phl[412]";
connectAttr "rigw:tracks:stuff60_scaleY.o" "rigwRN.phl[413]";
connectAttr "rigw:tracks:stuff60_scaleZ.o" "rigwRN.phl[414]";
connectAttr "rigw:tracks:stuff59_translateX.o" "rigwRN.phl[415]";
connectAttr "rigw:tracks:stuff59_translateY.o" "rigwRN.phl[416]";
connectAttr "rigw:tracks:stuff59_translateZ.o" "rigwRN.phl[417]";
connectAttr "rigw:tracks:stuff59_visibility.o" "rigwRN.phl[418]";
connectAttr "rigw:tracks:stuff59_rotateX.o" "rigwRN.phl[419]";
connectAttr "rigw:tracks:stuff59_rotateY.o" "rigwRN.phl[420]";
connectAttr "rigw:tracks:stuff59_rotateZ.o" "rigwRN.phl[421]";
connectAttr "rigw:tracks:stuff59_scaleX.o" "rigwRN.phl[422]";
connectAttr "rigw:tracks:stuff59_scaleY.o" "rigwRN.phl[423]";
connectAttr "rigw:tracks:stuff59_scaleZ.o" "rigwRN.phl[424]";
connectAttr "rigw:tracks:stuff58_translateX.o" "rigwRN.phl[425]";
connectAttr "rigw:tracks:stuff58_translateY.o" "rigwRN.phl[426]";
connectAttr "rigw:tracks:stuff58_translateZ.o" "rigwRN.phl[427]";
connectAttr "rigw:tracks:stuff58_visibility.o" "rigwRN.phl[428]";
connectAttr "rigw:tracks:stuff58_rotateX.o" "rigwRN.phl[429]";
connectAttr "rigw:tracks:stuff58_rotateY.o" "rigwRN.phl[430]";
connectAttr "rigw:tracks:stuff58_rotateZ.o" "rigwRN.phl[431]";
connectAttr "rigw:tracks:stuff58_scaleX.o" "rigwRN.phl[432]";
connectAttr "rigw:tracks:stuff58_scaleY.o" "rigwRN.phl[433]";
connectAttr "rigw:tracks:stuff58_scaleZ.o" "rigwRN.phl[434]";
connectAttr "rigw:tracks:stuff57_translateX.o" "rigwRN.phl[435]";
connectAttr "rigw:tracks:stuff57_translateY.o" "rigwRN.phl[436]";
connectAttr "rigw:tracks:stuff57_translateZ.o" "rigwRN.phl[437]";
connectAttr "rigw:tracks:stuff57_visibility.o" "rigwRN.phl[438]";
connectAttr "rigw:tracks:stuff57_rotateX.o" "rigwRN.phl[439]";
connectAttr "rigw:tracks:stuff57_rotateY.o" "rigwRN.phl[440]";
connectAttr "rigw:tracks:stuff57_rotateZ.o" "rigwRN.phl[441]";
connectAttr "rigw:tracks:stuff57_scaleX.o" "rigwRN.phl[442]";
connectAttr "rigw:tracks:stuff57_scaleY.o" "rigwRN.phl[443]";
connectAttr "rigw:tracks:stuff57_scaleZ.o" "rigwRN.phl[444]";
connectAttr "rigw:tracks:stuff56_translateX.o" "rigwRN.phl[445]";
connectAttr "rigw:tracks:stuff56_translateY.o" "rigwRN.phl[446]";
connectAttr "rigw:tracks:stuff56_translateZ.o" "rigwRN.phl[447]";
connectAttr "rigw:tracks:stuff56_visibility.o" "rigwRN.phl[448]";
connectAttr "rigw:tracks:stuff56_rotateX.o" "rigwRN.phl[449]";
connectAttr "rigw:tracks:stuff56_rotateY.o" "rigwRN.phl[450]";
connectAttr "rigw:tracks:stuff56_rotateZ.o" "rigwRN.phl[451]";
connectAttr "rigw:tracks:stuff56_scaleX.o" "rigwRN.phl[452]";
connectAttr "rigw:tracks:stuff56_scaleY.o" "rigwRN.phl[453]";
connectAttr "rigw:tracks:stuff56_scaleZ.o" "rigwRN.phl[454]";
connectAttr "rigw:tracks:stuff69_translateX.o" "rigwRN.phl[455]";
connectAttr "rigw:tracks:stuff69_translateY.o" "rigwRN.phl[456]";
connectAttr "rigw:tracks:stuff69_translateZ.o" "rigwRN.phl[457]";
connectAttr "rigw:tracks:stuff69_rotateX.o" "rigwRN.phl[458]";
connectAttr "rigw:tracks:stuff69_rotateY.o" "rigwRN.phl[459]";
connectAttr "rigw:tracks:stuff69_rotateZ.o" "rigwRN.phl[460]";
connectAttr "rigw:tracks:stuff69_visibility.o" "rigwRN.phl[461]";
connectAttr "rigw:tracks:stuff69_scaleX.o" "rigwRN.phl[462]";
connectAttr "rigw:tracks:stuff69_scaleY.o" "rigwRN.phl[463]";
connectAttr "rigw:tracks:stuff69_scaleZ.o" "rigwRN.phl[464]";
connectAttr "rigw:tracks:stuff94_translateX.o" "rigwRN.phl[465]";
connectAttr "rigw:tracks:stuff94_translateY.o" "rigwRN.phl[466]";
connectAttr "rigw:tracks:stuff94_translateZ.o" "rigwRN.phl[467]";
connectAttr "rigw:tracks:stuff94_rotateX.o" "rigwRN.phl[468]";
connectAttr "rigw:tracks:stuff94_rotateY.o" "rigwRN.phl[469]";
connectAttr "rigw:tracks:stuff94_rotateZ.o" "rigwRN.phl[470]";
connectAttr "rigw:tracks:stuff94_visibility.o" "rigwRN.phl[471]";
connectAttr "rigw:tracks:stuff94_scaleX.o" "rigwRN.phl[472]";
connectAttr "rigw:tracks:stuff94_scaleY.o" "rigwRN.phl[473]";
connectAttr "rigw:tracks:stuff94_scaleZ.o" "rigwRN.phl[474]";
connectAttr "rigw:tracks:stuff93_translateX.o" "rigwRN.phl[475]";
connectAttr "rigw:tracks:stuff93_translateY.o" "rigwRN.phl[476]";
connectAttr "rigw:tracks:stuff93_translateZ.o" "rigwRN.phl[477]";
connectAttr "rigw:tracks:stuff93_rotateX.o" "rigwRN.phl[478]";
connectAttr "rigw:tracks:stuff93_rotateY.o" "rigwRN.phl[479]";
connectAttr "rigw:tracks:stuff93_rotateZ.o" "rigwRN.phl[480]";
connectAttr "rigw:tracks:stuff93_visibility.o" "rigwRN.phl[481]";
connectAttr "rigw:tracks:stuff93_scaleX.o" "rigwRN.phl[482]";
connectAttr "rigw:tracks:stuff93_scaleY.o" "rigwRN.phl[483]";
connectAttr "rigw:tracks:stuff93_scaleZ.o" "rigwRN.phl[484]";
connectAttr "rigw:tracks:stuff92_translateX.o" "rigwRN.phl[485]";
connectAttr "rigw:tracks:stuff92_translateY.o" "rigwRN.phl[486]";
connectAttr "rigw:tracks:stuff92_translateZ.o" "rigwRN.phl[487]";
connectAttr "rigw:tracks:stuff92_visibility.o" "rigwRN.phl[488]";
connectAttr "rigw:tracks:stuff92_rotateX.o" "rigwRN.phl[489]";
connectAttr "rigw:tracks:stuff92_rotateY.o" "rigwRN.phl[490]";
connectAttr "rigw:tracks:stuff92_rotateZ.o" "rigwRN.phl[491]";
connectAttr "rigw:tracks:stuff92_scaleX.o" "rigwRN.phl[492]";
connectAttr "rigw:tracks:stuff92_scaleY.o" "rigwRN.phl[493]";
connectAttr "rigw:tracks:stuff92_scaleZ.o" "rigwRN.phl[494]";
connectAttr "rigw:tracks:stuff91_translateX.o" "rigwRN.phl[495]";
connectAttr "rigw:tracks:stuff91_translateY.o" "rigwRN.phl[496]";
connectAttr "rigw:tracks:stuff91_translateZ.o" "rigwRN.phl[497]";
connectAttr "rigw:tracks:stuff91_visibility.o" "rigwRN.phl[498]";
connectAttr "rigw:tracks:stuff91_rotateX.o" "rigwRN.phl[499]";
connectAttr "rigw:tracks:stuff91_rotateY.o" "rigwRN.phl[500]";
connectAttr "rigw:tracks:stuff91_rotateZ.o" "rigwRN.phl[501]";
connectAttr "rigw:tracks:stuff91_scaleX.o" "rigwRN.phl[502]";
connectAttr "rigw:tracks:stuff91_scaleY.o" "rigwRN.phl[503]";
connectAttr "rigw:tracks:stuff91_scaleZ.o" "rigwRN.phl[504]";
connectAttr "rigw:tracks:stuff85_translateX.o" "rigwRN.phl[505]";
connectAttr "rigw:tracks:stuff85_translateY.o" "rigwRN.phl[506]";
connectAttr "rigw:tracks:stuff85_translateZ.o" "rigwRN.phl[507]";
connectAttr "rigw:tracks:stuff85_visibility.o" "rigwRN.phl[508]";
connectAttr "rigw:tracks:stuff85_rotateX.o" "rigwRN.phl[509]";
connectAttr "rigw:tracks:stuff85_rotateY.o" "rigwRN.phl[510]";
connectAttr "rigw:tracks:stuff85_rotateZ.o" "rigwRN.phl[511]";
connectAttr "rigw:tracks:stuff85_scaleX.o" "rigwRN.phl[512]";
connectAttr "rigw:tracks:stuff85_scaleY.o" "rigwRN.phl[513]";
connectAttr "rigw:tracks:stuff85_scaleZ.o" "rigwRN.phl[514]";
connectAttr "rigw:tracks:stuff86_translateX.o" "rigwRN.phl[515]";
connectAttr "rigw:tracks:stuff86_translateY.o" "rigwRN.phl[516]";
connectAttr "rigw:tracks:stuff86_translateZ.o" "rigwRN.phl[517]";
connectAttr "rigw:tracks:stuff86_visibility.o" "rigwRN.phl[518]";
connectAttr "rigw:tracks:stuff86_rotateX.o" "rigwRN.phl[519]";
connectAttr "rigw:tracks:stuff86_rotateY.o" "rigwRN.phl[520]";
connectAttr "rigw:tracks:stuff86_rotateZ.o" "rigwRN.phl[521]";
connectAttr "rigw:tracks:stuff86_scaleX.o" "rigwRN.phl[522]";
connectAttr "rigw:tracks:stuff86_scaleY.o" "rigwRN.phl[523]";
connectAttr "rigw:tracks:stuff86_scaleZ.o" "rigwRN.phl[524]";
connectAttr "rigw:tracks:stuff87_translateX.o" "rigwRN.phl[525]";
connectAttr "rigw:tracks:stuff87_translateY.o" "rigwRN.phl[526]";
connectAttr "rigw:tracks:stuff87_translateZ.o" "rigwRN.phl[527]";
connectAttr "rigw:tracks:stuff87_visibility.o" "rigwRN.phl[528]";
connectAttr "rigw:tracks:stuff87_rotateX.o" "rigwRN.phl[529]";
connectAttr "rigw:tracks:stuff87_rotateY.o" "rigwRN.phl[530]";
connectAttr "rigw:tracks:stuff87_rotateZ.o" "rigwRN.phl[531]";
connectAttr "rigw:tracks:stuff87_scaleX.o" "rigwRN.phl[532]";
connectAttr "rigw:tracks:stuff87_scaleY.o" "rigwRN.phl[533]";
connectAttr "rigw:tracks:stuff87_scaleZ.o" "rigwRN.phl[534]";
connectAttr "rigw:tracks:stuff88_translateX.o" "rigwRN.phl[535]";
connectAttr "rigw:tracks:stuff88_translateY.o" "rigwRN.phl[536]";
connectAttr "rigw:tracks:stuff88_translateZ.o" "rigwRN.phl[537]";
connectAttr "rigw:tracks:stuff88_visibility.o" "rigwRN.phl[538]";
connectAttr "rigw:tracks:stuff88_rotateX.o" "rigwRN.phl[539]";
connectAttr "rigw:tracks:stuff88_rotateY.o" "rigwRN.phl[540]";
connectAttr "rigw:tracks:stuff88_rotateZ.o" "rigwRN.phl[541]";
connectAttr "rigw:tracks:stuff88_scaleX.o" "rigwRN.phl[542]";
connectAttr "rigw:tracks:stuff88_scaleY.o" "rigwRN.phl[543]";
connectAttr "rigw:tracks:stuff88_scaleZ.o" "rigwRN.phl[544]";
connectAttr "rigw:tracks:stuff89_translateX.o" "rigwRN.phl[545]";
connectAttr "rigw:tracks:stuff89_translateY.o" "rigwRN.phl[546]";
connectAttr "rigw:tracks:stuff89_translateZ.o" "rigwRN.phl[547]";
connectAttr "rigw:tracks:stuff89_visibility.o" "rigwRN.phl[548]";
connectAttr "rigw:tracks:stuff89_rotateX.o" "rigwRN.phl[549]";
connectAttr "rigw:tracks:stuff89_rotateY.o" "rigwRN.phl[550]";
connectAttr "rigw:tracks:stuff89_rotateZ.o" "rigwRN.phl[551]";
connectAttr "rigw:tracks:stuff89_scaleX.o" "rigwRN.phl[552]";
connectAttr "rigw:tracks:stuff89_scaleY.o" "rigwRN.phl[553]";
connectAttr "rigw:tracks:stuff89_scaleZ.o" "rigwRN.phl[554]";
connectAttr "rigw:tracks:stuff90_translateX.o" "rigwRN.phl[555]";
connectAttr "rigw:tracks:stuff90_translateY.o" "rigwRN.phl[556]";
connectAttr "rigw:tracks:stuff90_translateZ.o" "rigwRN.phl[557]";
connectAttr "rigw:tracks:stuff90_visibility.o" "rigwRN.phl[558]";
connectAttr "rigw:tracks:stuff90_rotateX.o" "rigwRN.phl[559]";
connectAttr "rigw:tracks:stuff90_rotateY.o" "rigwRN.phl[560]";
connectAttr "rigw:tracks:stuff90_rotateZ.o" "rigwRN.phl[561]";
connectAttr "rigw:tracks:stuff90_scaleX.o" "rigwRN.phl[562]";
connectAttr "rigw:tracks:stuff90_scaleY.o" "rigwRN.phl[563]";
connectAttr "rigw:tracks:stuff90_scaleZ.o" "rigwRN.phl[564]";
connectAttr "rigw:tracks:stuff95_translateX.o" "rigwRN.phl[565]";
connectAttr "rigw:tracks:stuff95_translateY.o" "rigwRN.phl[566]";
connectAttr "rigw:tracks:stuff95_translateZ.o" "rigwRN.phl[567]";
connectAttr "rigw:tracks:stuff95_visibility.o" "rigwRN.phl[568]";
connectAttr "rigw:tracks:stuff95_rotateX.o" "rigwRN.phl[569]";
connectAttr "rigw:tracks:stuff95_rotateY.o" "rigwRN.phl[570]";
connectAttr "rigw:tracks:stuff95_rotateZ.o" "rigwRN.phl[571]";
connectAttr "rigw:tracks:stuff95_scaleX.o" "rigwRN.phl[572]";
connectAttr "rigw:tracks:stuff95_scaleY.o" "rigwRN.phl[573]";
connectAttr "rigw:tracks:stuff95_scaleZ.o" "rigwRN.phl[574]";
connectAttr "rigw:tracks:stuff96_translateX.o" "rigwRN.phl[575]";
connectAttr "rigw:tracks:stuff96_translateY.o" "rigwRN.phl[576]";
connectAttr "rigw:tracks:stuff96_translateZ.o" "rigwRN.phl[577]";
connectAttr "rigw:tracks:stuff96_visibility.o" "rigwRN.phl[578]";
connectAttr "rigw:tracks:stuff96_rotateX.o" "rigwRN.phl[579]";
connectAttr "rigw:tracks:stuff96_rotateY.o" "rigwRN.phl[580]";
connectAttr "rigw:tracks:stuff96_rotateZ.o" "rigwRN.phl[581]";
connectAttr "rigw:tracks:stuff96_scaleX.o" "rigwRN.phl[582]";
connectAttr "rigw:tracks:stuff96_scaleY.o" "rigwRN.phl[583]";
connectAttr "rigw:tracks:stuff96_scaleZ.o" "rigwRN.phl[584]";
connectAttr "rigw:tracks:stuff97_translateX.o" "rigwRN.phl[585]";
connectAttr "rigw:tracks:stuff97_translateY.o" "rigwRN.phl[586]";
connectAttr "rigw:tracks:stuff97_translateZ.o" "rigwRN.phl[587]";
connectAttr "rigw:tracks:stuff97_visibility.o" "rigwRN.phl[588]";
connectAttr "rigw:tracks:stuff97_rotateX.o" "rigwRN.phl[589]";
connectAttr "rigw:tracks:stuff97_rotateY.o" "rigwRN.phl[590]";
connectAttr "rigw:tracks:stuff97_rotateZ.o" "rigwRN.phl[591]";
connectAttr "rigw:tracks:stuff97_scaleX.o" "rigwRN.phl[592]";
connectAttr "rigw:tracks:stuff97_scaleY.o" "rigwRN.phl[593]";
connectAttr "rigw:tracks:stuff97_scaleZ.o" "rigwRN.phl[594]";
connectAttr "rigw:tracks:stuff98_translateX.o" "rigwRN.phl[595]";
connectAttr "rigw:tracks:stuff98_translateY.o" "rigwRN.phl[596]";
connectAttr "rigw:tracks:stuff98_translateZ.o" "rigwRN.phl[597]";
connectAttr "rigw:tracks:stuff98_visibility.o" "rigwRN.phl[598]";
connectAttr "rigw:tracks:stuff98_rotateX.o" "rigwRN.phl[599]";
connectAttr "rigw:tracks:stuff98_rotateY.o" "rigwRN.phl[600]";
connectAttr "rigw:tracks:stuff98_rotateZ.o" "rigwRN.phl[601]";
connectAttr "rigw:tracks:stuff98_scaleX.o" "rigwRN.phl[602]";
connectAttr "rigw:tracks:stuff98_scaleY.o" "rigwRN.phl[603]";
connectAttr "rigw:tracks:stuff98_scaleZ.o" "rigwRN.phl[604]";
connectAttr "rigw:tracks:stuff111_translateX.o" "rigwRN.phl[605]";
connectAttr "rigw:tracks:stuff111_translateY.o" "rigwRN.phl[606]";
connectAttr "rigw:tracks:stuff111_translateZ.o" "rigwRN.phl[607]";
connectAttr "rigw:tracks:stuff111_rotateX.o" "rigwRN.phl[608]";
connectAttr "rigw:tracks:stuff111_rotateY.o" "rigwRN.phl[609]";
connectAttr "rigw:tracks:stuff111_rotateZ.o" "rigwRN.phl[610]";
connectAttr "rigw:tracks:stuff111_visibility.o" "rigwRN.phl[611]";
connectAttr "rigw:tracks:stuff111_scaleX.o" "rigwRN.phl[612]";
connectAttr "rigw:tracks:stuff111_scaleY.o" "rigwRN.phl[613]";
connectAttr "rigw:tracks:stuff111_scaleZ.o" "rigwRN.phl[614]";
connectAttr "rigw:tracks:stuff110_translateX.o" "rigwRN.phl[615]";
connectAttr "rigw:tracks:stuff110_translateY.o" "rigwRN.phl[616]";
connectAttr "rigw:tracks:stuff110_translateZ.o" "rigwRN.phl[617]";
connectAttr "rigw:tracks:stuff110_rotateX.o" "rigwRN.phl[618]";
connectAttr "rigw:tracks:stuff110_rotateY.o" "rigwRN.phl[619]";
connectAttr "rigw:tracks:stuff110_rotateZ.o" "rigwRN.phl[620]";
connectAttr "rigw:tracks:stuff110_visibility.o" "rigwRN.phl[621]";
connectAttr "rigw:tracks:stuff110_scaleX.o" "rigwRN.phl[622]";
connectAttr "rigw:tracks:stuff110_scaleY.o" "rigwRN.phl[623]";
connectAttr "rigw:tracks:stuff110_scaleZ.o" "rigwRN.phl[624]";
connectAttr "rigw:tracks:stuff109_translateX.o" "rigwRN.phl[625]";
connectAttr "rigw:tracks:stuff109_translateY.o" "rigwRN.phl[626]";
connectAttr "rigw:tracks:stuff109_translateZ.o" "rigwRN.phl[627]";
connectAttr "rigw:tracks:stuff109_visibility.o" "rigwRN.phl[628]";
connectAttr "rigw:tracks:stuff109_rotateX.o" "rigwRN.phl[629]";
connectAttr "rigw:tracks:stuff109_rotateY.o" "rigwRN.phl[630]";
connectAttr "rigw:tracks:stuff109_rotateZ.o" "rigwRN.phl[631]";
connectAttr "rigw:tracks:stuff109_scaleX.o" "rigwRN.phl[632]";
connectAttr "rigw:tracks:stuff109_scaleY.o" "rigwRN.phl[633]";
connectAttr "rigw:tracks:stuff109_scaleZ.o" "rigwRN.phl[634]";
connectAttr "rigw:tracks:stuff108_translateX.o" "rigwRN.phl[635]";
connectAttr "rigw:tracks:stuff108_translateY.o" "rigwRN.phl[636]";
connectAttr "rigw:tracks:stuff108_translateZ.o" "rigwRN.phl[637]";
connectAttr "rigw:tracks:stuff108_visibility.o" "rigwRN.phl[638]";
connectAttr "rigw:tracks:stuff108_rotateX.o" "rigwRN.phl[639]";
connectAttr "rigw:tracks:stuff108_rotateY.o" "rigwRN.phl[640]";
connectAttr "rigw:tracks:stuff108_rotateZ.o" "rigwRN.phl[641]";
connectAttr "rigw:tracks:stuff108_scaleX.o" "rigwRN.phl[642]";
connectAttr "rigw:tracks:stuff108_scaleY.o" "rigwRN.phl[643]";
connectAttr "rigw:tracks:stuff108_scaleZ.o" "rigwRN.phl[644]";
connectAttr "rigw:tracks:stuff107_translateX.o" "rigwRN.phl[645]";
connectAttr "rigw:tracks:stuff107_translateY.o" "rigwRN.phl[646]";
connectAttr "rigw:tracks:stuff107_translateZ.o" "rigwRN.phl[647]";
connectAttr "rigw:tracks:stuff107_visibility.o" "rigwRN.phl[648]";
connectAttr "rigw:tracks:stuff107_rotateX.o" "rigwRN.phl[649]";
connectAttr "rigw:tracks:stuff107_rotateY.o" "rigwRN.phl[650]";
connectAttr "rigw:tracks:stuff107_rotateZ.o" "rigwRN.phl[651]";
connectAttr "rigw:tracks:stuff107_scaleX.o" "rigwRN.phl[652]";
connectAttr "rigw:tracks:stuff107_scaleY.o" "rigwRN.phl[653]";
connectAttr "rigw:tracks:stuff107_scaleZ.o" "rigwRN.phl[654]";
connectAttr "rigw:tracks:stuff106_translateX.o" "rigwRN.phl[655]";
connectAttr "rigw:tracks:stuff106_translateY.o" "rigwRN.phl[656]";
connectAttr "rigw:tracks:stuff106_translateZ.o" "rigwRN.phl[657]";
connectAttr "rigw:tracks:stuff106_visibility.o" "rigwRN.phl[658]";
connectAttr "rigw:tracks:stuff106_rotateX.o" "rigwRN.phl[659]";
connectAttr "rigw:tracks:stuff106_rotateY.o" "rigwRN.phl[660]";
connectAttr "rigw:tracks:stuff106_rotateZ.o" "rigwRN.phl[661]";
connectAttr "rigw:tracks:stuff106_scaleX.o" "rigwRN.phl[662]";
connectAttr "rigw:tracks:stuff106_scaleY.o" "rigwRN.phl[663]";
connectAttr "rigw:tracks:stuff106_scaleZ.o" "rigwRN.phl[664]";
connectAttr "rigw:tracks:stuff105_translateX.o" "rigwRN.phl[665]";
connectAttr "rigw:tracks:stuff105_translateY.o" "rigwRN.phl[666]";
connectAttr "rigw:tracks:stuff105_translateZ.o" "rigwRN.phl[667]";
connectAttr "rigw:tracks:stuff105_visibility.o" "rigwRN.phl[668]";
connectAttr "rigw:tracks:stuff105_rotateX.o" "rigwRN.phl[669]";
connectAttr "rigw:tracks:stuff105_rotateY.o" "rigwRN.phl[670]";
connectAttr "rigw:tracks:stuff105_rotateZ.o" "rigwRN.phl[671]";
connectAttr "rigw:tracks:stuff105_scaleX.o" "rigwRN.phl[672]";
connectAttr "rigw:tracks:stuff105_scaleY.o" "rigwRN.phl[673]";
connectAttr "rigw:tracks:stuff105_scaleZ.o" "rigwRN.phl[674]";
connectAttr "rigw:tracks:stuff104_translateX.o" "rigwRN.phl[675]";
connectAttr "rigw:tracks:stuff104_translateY.o" "rigwRN.phl[676]";
connectAttr "rigw:tracks:stuff104_translateZ.o" "rigwRN.phl[677]";
connectAttr "rigw:tracks:stuff104_visibility.o" "rigwRN.phl[678]";
connectAttr "rigw:tracks:stuff104_rotateX.o" "rigwRN.phl[679]";
connectAttr "rigw:tracks:stuff104_rotateY.o" "rigwRN.phl[680]";
connectAttr "rigw:tracks:stuff104_rotateZ.o" "rigwRN.phl[681]";
connectAttr "rigw:tracks:stuff104_scaleX.o" "rigwRN.phl[682]";
connectAttr "rigw:tracks:stuff104_scaleY.o" "rigwRN.phl[683]";
connectAttr "rigw:tracks:stuff104_scaleZ.o" "rigwRN.phl[684]";
connectAttr "rigw:tracks:stuff103_translateX.o" "rigwRN.phl[685]";
connectAttr "rigw:tracks:stuff103_translateY.o" "rigwRN.phl[686]";
connectAttr "rigw:tracks:stuff103_translateZ.o" "rigwRN.phl[687]";
connectAttr "rigw:tracks:stuff103_visibility.o" "rigwRN.phl[688]";
connectAttr "rigw:tracks:stuff103_rotateX.o" "rigwRN.phl[689]";
connectAttr "rigw:tracks:stuff103_rotateY.o" "rigwRN.phl[690]";
connectAttr "rigw:tracks:stuff103_rotateZ.o" "rigwRN.phl[691]";
connectAttr "rigw:tracks:stuff103_scaleX.o" "rigwRN.phl[692]";
connectAttr "rigw:tracks:stuff103_scaleY.o" "rigwRN.phl[693]";
connectAttr "rigw:tracks:stuff103_scaleZ.o" "rigwRN.phl[694]";
connectAttr "rigw:tracks:stuff102_translateX.o" "rigwRN.phl[695]";
connectAttr "rigw:tracks:stuff102_translateY.o" "rigwRN.phl[696]";
connectAttr "rigw:tracks:stuff102_translateZ.o" "rigwRN.phl[697]";
connectAttr "rigw:tracks:stuff102_visibility.o" "rigwRN.phl[698]";
connectAttr "rigw:tracks:stuff102_rotateX.o" "rigwRN.phl[699]";
connectAttr "rigw:tracks:stuff102_rotateY.o" "rigwRN.phl[700]";
connectAttr "rigw:tracks:stuff102_rotateZ.o" "rigwRN.phl[701]";
connectAttr "rigw:tracks:stuff102_scaleX.o" "rigwRN.phl[702]";
connectAttr "rigw:tracks:stuff102_scaleY.o" "rigwRN.phl[703]";
connectAttr "rigw:tracks:stuff102_scaleZ.o" "rigwRN.phl[704]";
connectAttr "rigw:tracks:stuff101_translateX.o" "rigwRN.phl[705]";
connectAttr "rigw:tracks:stuff101_translateY.o" "rigwRN.phl[706]";
connectAttr "rigw:tracks:stuff101_translateZ.o" "rigwRN.phl[707]";
connectAttr "rigw:tracks:stuff101_visibility.o" "rigwRN.phl[708]";
connectAttr "rigw:tracks:stuff101_rotateX.o" "rigwRN.phl[709]";
connectAttr "rigw:tracks:stuff101_rotateY.o" "rigwRN.phl[710]";
connectAttr "rigw:tracks:stuff101_rotateZ.o" "rigwRN.phl[711]";
connectAttr "rigw:tracks:stuff101_scaleX.o" "rigwRN.phl[712]";
connectAttr "rigw:tracks:stuff101_scaleY.o" "rigwRN.phl[713]";
connectAttr "rigw:tracks:stuff101_scaleZ.o" "rigwRN.phl[714]";
connectAttr "rigw:tracks:stuff100_translateX.o" "rigwRN.phl[715]";
connectAttr "rigw:tracks:stuff100_translateY.o" "rigwRN.phl[716]";
connectAttr "rigw:tracks:stuff100_translateZ.o" "rigwRN.phl[717]";
connectAttr "rigw:tracks:stuff100_visibility.o" "rigwRN.phl[718]";
connectAttr "rigw:tracks:stuff100_rotateX.o" "rigwRN.phl[719]";
connectAttr "rigw:tracks:stuff100_rotateY.o" "rigwRN.phl[720]";
connectAttr "rigw:tracks:stuff100_rotateZ.o" "rigwRN.phl[721]";
connectAttr "rigw:tracks:stuff100_scaleX.o" "rigwRN.phl[722]";
connectAttr "rigw:tracks:stuff100_scaleY.o" "rigwRN.phl[723]";
connectAttr "rigw:tracks:stuff100_scaleZ.o" "rigwRN.phl[724]";
connectAttr "rigw:tracks:stuff99_translateX.o" "rigwRN.phl[725]";
connectAttr "rigw:tracks:stuff99_translateY.o" "rigwRN.phl[726]";
connectAttr "rigw:tracks:stuff99_translateZ.o" "rigwRN.phl[727]";
connectAttr "rigw:tracks:stuff99_visibility.o" "rigwRN.phl[728]";
connectAttr "rigw:tracks:stuff99_rotateX.o" "rigwRN.phl[729]";
connectAttr "rigw:tracks:stuff99_rotateY.o" "rigwRN.phl[730]";
connectAttr "rigw:tracks:stuff99_rotateZ.o" "rigwRN.phl[731]";
connectAttr "rigw:tracks:stuff99_scaleX.o" "rigwRN.phl[732]";
connectAttr "rigw:tracks:stuff99_scaleY.o" "rigwRN.phl[733]";
connectAttr "rigw:tracks:stuff99_scaleZ.o" "rigwRN.phl[734]";
connectAttr "rigw:tracks:stuff112_translateX.o" "rigwRN.phl[735]";
connectAttr "rigw:tracks:stuff112_translateY.o" "rigwRN.phl[736]";
connectAttr "rigw:tracks:stuff112_translateZ.o" "rigwRN.phl[737]";
connectAttr "rigw:tracks:stuff112_rotateX.o" "rigwRN.phl[738]";
connectAttr "rigw:tracks:stuff112_rotateY.o" "rigwRN.phl[739]";
connectAttr "rigw:tracks:stuff112_rotateZ.o" "rigwRN.phl[740]";
connectAttr "rigw:tracks:stuff112_visibility.o" "rigwRN.phl[741]";
connectAttr "rigw:tracks:stuff112_scaleX.o" "rigwRN.phl[742]";
connectAttr "rigw:tracks:stuff112_scaleY.o" "rigwRN.phl[743]";
connectAttr "rigw:tracks:stuff112_scaleZ.o" "rigwRN.phl[744]";
connectAttr "rigw:rig:FKWheel1_R_rotateX.o" "rigwRN.phl[103]";
connectAttr "rigw:rig:FKWheel1_R_rotateY.o" "rigwRN.phl[104]";
connectAttr "rigw:rig:FKWheel1_R_rotateZ.o" "rigwRN.phl[105]";
connectAttr "rigw:rig:FKWheel2_R_rotateX.o" "rigwRN.phl[106]";
connectAttr "rigw:rig:FKWheel2_R_rotateY.o" "rigwRN.phl[107]";
connectAttr "rigw:rig:FKWheel2_R_rotateZ.o" "rigwRN.phl[108]";
connectAttr "rigw:rig:FKWheel3_R_rotateX.o" "rigwRN.phl[109]";
connectAttr "rigw:rig:FKWheel3_R_rotateY.o" "rigwRN.phl[110]";
connectAttr "rigw:rig:FKWheel3_R_rotateZ.o" "rigwRN.phl[111]";
connectAttr "rigw:rig:FKWheel4_R_rotateX.o" "rigwRN.phl[112]";
connectAttr "rigw:rig:FKWheel4_R_rotateY.o" "rigwRN.phl[113]";
connectAttr "rigw:rig:FKWheel4_R_rotateZ.o" "rigwRN.phl[114]";
connectAttr "rigw:rig:FKWheel5_R_rotateX.o" "rigwRN.phl[115]";
connectAttr "rigw:rig:FKWheel5_R_rotateY.o" "rigwRN.phl[116]";
connectAttr "rigw:rig:FKWheel5_R_rotateZ.o" "rigwRN.phl[117]";
connectAttr "rigw:rig:FKWheel6_R_rotateX.o" "rigwRN.phl[118]";
connectAttr "rigw:rig:FKWheel6_R_rotateY.o" "rigwRN.phl[119]";
connectAttr "rigw:rig:FKWheel6_R_rotateZ.o" "rigwRN.phl[120]";
connectAttr "rigw:rig:FKWheel1_L_rotateX.o" "rigwRN.phl[121]";
connectAttr "rigw:rig:FKWheel1_L_rotateY.o" "rigwRN.phl[122]";
connectAttr "rigw:rig:FKWheel1_L_rotateZ.o" "rigwRN.phl[123]";
connectAttr "rigw:rig:FKWheel2_L_rotateX.o" "rigwRN.phl[124]";
connectAttr "rigw:rig:FKWheel2_L_rotateY.o" "rigwRN.phl[125]";
connectAttr "rigw:rig:FKWheel2_L_rotateZ.o" "rigwRN.phl[126]";
connectAttr "rigw:rig:FKWheel3_L_rotateX.o" "rigwRN.phl[127]";
connectAttr "rigw:rig:FKWheel3_L_rotateY.o" "rigwRN.phl[128]";
connectAttr "rigw:rig:FKWheel3_L_rotateZ.o" "rigwRN.phl[129]";
connectAttr "rigw:rig:FKWheel4_L_rotateX.o" "rigwRN.phl[130]";
connectAttr "rigw:rig:FKWheel4_L_rotateY.o" "rigwRN.phl[131]";
connectAttr "rigw:rig:FKWheel4_L_rotateZ.o" "rigwRN.phl[132]";
connectAttr "rigw:rig:FKWheel5_L_rotateX.o" "rigwRN.phl[133]";
connectAttr "rigw:rig:FKWheel5_L_rotateY.o" "rigwRN.phl[134]";
connectAttr "rigw:rig:FKWheel5_L_rotateZ.o" "rigwRN.phl[135]";
connectAttr "rigw:rig:FKWheel6_L_rotateX.o" "rigwRN.phl[136]";
connectAttr "rigw:rig:FKWheel6_L_rotateY.o" "rigwRN.phl[137]";
connectAttr "rigw:rig:FKWheel6_L_rotateZ.o" "rigwRN.phl[138]";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[139]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[140]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[141]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[142]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[143]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[144]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[145]";
connectAttr "rigw:rig:FKWeapon_M_rotateX.o" "rigwRN.phl[146]";
connectAttr "rigw:rig:FKWeapon_M_rotateY.o" "rigwRN.phl[147]";
connectAttr "rigw:rig:FKWeapon_M_rotateZ.o" "rigwRN.phl[148]";
connectAttr "rigw:rig:FKShoulder_R_Global.o" "rigwRN.phl[149]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[150]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[151]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[152]";
connectAttr "rigw:rig:FKElbow_R_rotateX.o" "rigwRN.phl[153]";
connectAttr "rigw:rig:FKElbow_R_rotateY.o" "rigwRN.phl[154]";
connectAttr "rigw:rig:FKElbow_R_rotateZ.o" "rigwRN.phl[155]";
connectAttr "rigw:rig:FKShoulder_L_Global.o" "rigwRN.phl[156]";
connectAttr "rigw:rig:FKShoulder_L_rotateX.o" "rigwRN.phl[157]";
connectAttr "rigw:rig:FKShoulder_L_rotateY.o" "rigwRN.phl[158]";
connectAttr "rigw:rig:FKShoulder_L_rotateZ.o" "rigwRN.phl[159]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[160]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[161]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[162]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[163]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[164]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[165]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[166]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[167]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[168]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[169]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[170]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[171]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[172]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[173]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[174]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[175]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[176]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[177]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[178]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[179]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[180]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[181]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[182]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[183]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[184]";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "rigw:TrackR_rotateX.o" "rigwRN.phl[83]";
connectAttr "rigw:TrackR_rotateY.o" "rigwRN.phl[84]";
connectAttr "rigw:TrackR_rotateZ.o" "rigwRN.phl[85]";
connectAttr "rigw:TrackR_visibility.o" "rigwRN.phl[86]";
connectAttr "rigw:TrackR_translateX.o" "rigwRN.phl[87]";
connectAttr "rigw:TrackR_translateY.o" "rigwRN.phl[88]";
connectAttr "rigw:TrackR_translateZ.o" "rigwRN.phl[89]";
connectAttr "rigw:TrackR_scaleX.o" "rigwRN.phl[90]";
connectAttr "rigw:TrackR_scaleY.o" "rigwRN.phl[91]";
connectAttr "rigw:TrackR_scaleZ.o" "rigwRN.phl[92]";
connectAttr "rigw:TrackL_rotateX.o" "rigwRN.phl[93]";
connectAttr "rigw:TrackL_rotateY.o" "rigwRN.phl[94]";
connectAttr "rigw:TrackL_rotateZ.o" "rigwRN.phl[95]";
connectAttr "rigw:TrackL_visibility.o" "rigwRN.phl[96]";
connectAttr "rigw:TrackL_translateX.o" "rigwRN.phl[97]";
connectAttr "rigw:TrackL_translateY.o" "rigwRN.phl[98]";
connectAttr "rigw:TrackL_translateZ.o" "rigwRN.phl[99]";
connectAttr "rigw:TrackL_scaleX.o" "rigwRN.phl[100]";
connectAttr "rigw:TrackL_scaleY.o" "rigwRN.phl[101]";
connectAttr "rigw:TrackL_scaleZ.o" "rigwRN.phl[102]";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Artillery__mc-rigw@Art_run.ma
