//Maya ASCII 2013 scene
//Name: Juggernaut__mc-rigw@Jug_idle.ma
//Last modified: Mon, Oct 27, 2014 11:27:45 AM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Juggernaut/Juggernaut__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Juggernaut__rig.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Juggernaut/Juggernaut__mc-rigw.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.4";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 52.12463311784046 31.06799563413891 78.280171150220525 ;
	setAttr ".r" -type "double3" -11.138352729601833 33.399999999998236 -4.7621770705483938e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 97.104220570429788;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -2.985325879607569 6.2106915861186067 3.5809729249191236 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" 0 0 500 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".ow" 327.55510615170698;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 11 ".lnk";
	setAttr -s 11 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 163 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 0
		"rigw:rigRN" 0
		"rigw:rigRN" 370
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "visibility" " -av 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scale" " -type \"double3\" 2.2 2.2 2.2"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotate" " -type \"double3\" 0 0 13.13857"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotate" " -type \"double3\" 0 0 8.671456"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotate" " -type \"double3\" 0 0 8.671456"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotate" " -type \"double3\" 0 0 8.671456"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotate" " -type \"double3\" 0 0 8.671456"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotate" " -type \"double3\" 0 0 8.671456"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotate" " -type \"double3\" -3.935081 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" -2.602427 -24.350197 -0.0222506"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotate" " -type \"double3\" 5.965211 -6.749883 -29.071287"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotate" " -type \"double3\" 0 75.860719 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotate" " -type \"double3\" 2.499691 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotate" " -type \"double3\" 2.499691 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotate" " -type \"double3\" -3.935081 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotate" " -type \"double3\" -10.062119 0.740671 -7.386709"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 -4.869769"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotate" " -type \"double3\" 2.499691 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotate" " -type \"double3\" 2.499691 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotate" " -type \"double3\" 0 0 18.86924"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotate" " -type \"double3\" 0 0 18.86924"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotate" " -type \"double3\" 0 0 18.86924"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotate" " -type \"double3\" 0 0 18.86924"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotate" " -type \"double3\" 0 0 18.86924"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotate" " -type \"double3\" 0 0 18.86924"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translate" " -type \"double3\" 0 -0.0730678 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"CenterBtwFeet" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" -0.350318 0 -0.0401067"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 -6.531151 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"toe" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M|rigw:rig:IKXSpineHandle_M" 
		"translate" " -type \"double3\" -2.24645e-08 0.00443195 -0.000534086"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M|rigw:rig:IKXSpineHandle_M" 
		"rotate" " -type \"double3\" 0.190416 -1.07353e-05 -1.6939e-06"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0.237274 0 -1.38742"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 7.576037 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"toe" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R" 
		"FKIKBlend" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R" 
		"FKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R" 
		"IKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"IKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M" 
		"FKIKBlend" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M" 
		"FKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M" 
		"IKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L" 
		"FKIKBlend" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L" 
		"FKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L" 
		"IKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"IKVis" " -av -k 1 1"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleY" "rigwRN.placeHolderList[1]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleX" "rigwRN.placeHolderList[2]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleZ" "rigwRN.placeHolderList[3]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateX" "rigwRN.placeHolderList[4]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateY" "rigwRN.placeHolderList[5]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateZ" "rigwRN.placeHolderList[6]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.visibility" "rigwRN.placeHolderList[7]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateX" "rigwRN.placeHolderList[8]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateY" "rigwRN.placeHolderList[9]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateZ" "rigwRN.placeHolderList[10]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateX" 
		"rigwRN.placeHolderList[11]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateY" 
		"rigwRN.placeHolderList[12]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[13]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateX" 
		"rigwRN.placeHolderList[14]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateY" 
		"rigwRN.placeHolderList[15]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[16]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateX" 
		"rigwRN.placeHolderList[17]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateY" 
		"rigwRN.placeHolderList[18]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[19]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateX" 
		"rigwRN.placeHolderList[20]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateY" 
		"rigwRN.placeHolderList[21]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[22]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateX" 
		"rigwRN.placeHolderList[23]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateY" 
		"rigwRN.placeHolderList[24]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[25]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateX" 
		"rigwRN.placeHolderList[26]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateY" 
		"rigwRN.placeHolderList[27]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[28]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateX" 
		"rigwRN.placeHolderList[29]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateY" 
		"rigwRN.placeHolderList[30]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateZ" 
		"rigwRN.placeHolderList[31]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[32]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[33]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[34]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateX" 
		"rigwRN.placeHolderList[35]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateY" 
		"rigwRN.placeHolderList[36]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateZ" 
		"rigwRN.placeHolderList[37]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateX" 
		"rigwRN.placeHolderList[38]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateY" 
		"rigwRN.placeHolderList[39]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateZ" 
		"rigwRN.placeHolderList[40]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R.rotateX" 
		"rigwRN.placeHolderList[41]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R.rotateY" 
		"rigwRN.placeHolderList[42]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R.rotateZ" 
		"rigwRN.placeHolderList[43]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R.rotateX" 
		"rigwRN.placeHolderList[44]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R.rotateY" 
		"rigwRN.placeHolderList[45]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R.rotateZ" 
		"rigwRN.placeHolderList[46]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L.rotateX" 
		"rigwRN.placeHolderList[47]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L.rotateY" 
		"rigwRN.placeHolderList[48]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L.rotateZ" 
		"rigwRN.placeHolderList[49]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateX" 
		"rigwRN.placeHolderList[50]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateY" 
		"rigwRN.placeHolderList[51]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateZ" 
		"rigwRN.placeHolderList[52]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[53]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[54]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[55]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateX" 
		"rigwRN.placeHolderList[56]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateY" 
		"rigwRN.placeHolderList[57]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateZ" 
		"rigwRN.placeHolderList[58]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L.rotateX" 
		"rigwRN.placeHolderList[59]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L.rotateY" 
		"rigwRN.placeHolderList[60]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L.rotateZ" 
		"rigwRN.placeHolderList[61]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L.rotateX" 
		"rigwRN.placeHolderList[62]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L.rotateY" 
		"rigwRN.placeHolderList[63]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L.rotateZ" 
		"rigwRN.placeHolderList[64]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateX" 
		"rigwRN.placeHolderList[65]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateY" 
		"rigwRN.placeHolderList[66]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateZ" 
		"rigwRN.placeHolderList[67]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateX" 
		"rigwRN.placeHolderList[68]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateY" 
		"rigwRN.placeHolderList[69]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateZ" 
		"rigwRN.placeHolderList[70]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateX" 
		"rigwRN.placeHolderList[71]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateY" 
		"rigwRN.placeHolderList[72]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[73]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateX" 
		"rigwRN.placeHolderList[74]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateY" 
		"rigwRN.placeHolderList[75]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[76]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateX" 
		"rigwRN.placeHolderList[77]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateY" 
		"rigwRN.placeHolderList[78]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[79]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateX" 
		"rigwRN.placeHolderList[80]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateY" 
		"rigwRN.placeHolderList[81]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[82]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateX" 
		"rigwRN.placeHolderList[83]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateY" 
		"rigwRN.placeHolderList[84]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[85]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateX" 
		"rigwRN.placeHolderList[86]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateY" 
		"rigwRN.placeHolderList[87]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[88]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[89]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[90]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[91]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[92]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[93]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[94]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[95]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateX" 
		"rigwRN.placeHolderList[96]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateY" 
		"rigwRN.placeHolderList[97]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateZ" 
		"rigwRN.placeHolderList[98]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateX" 
		"rigwRN.placeHolderList[99]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateY" 
		"rigwRN.placeHolderList[100]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateZ" 
		"rigwRN.placeHolderList[101]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateX" 
		"rigwRN.placeHolderList[102]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateY" 
		"rigwRN.placeHolderList[103]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateZ" 
		"rigwRN.placeHolderList[104]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[105]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[106]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[107]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[108]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[109]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[110]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[111]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rollAngle" 
		"rigwRN.placeHolderList[112]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.roll" 
		"rigwRN.placeHolderList[113]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.toe" 
		"rigwRN.placeHolderList[114]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[115]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[116]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[117]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[118]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateX" 
		"rigwRN.placeHolderList[119]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateY" 
		"rigwRN.placeHolderList[120]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateZ" 
		"rigwRN.placeHolderList[121]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R.rotateX" 
		"rigwRN.placeHolderList[122]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateX" 
		"rigwRN.placeHolderList[123]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateY" 
		"rigwRN.placeHolderList[124]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateZ" 
		"rigwRN.placeHolderList[125]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.follow" 
		"rigwRN.placeHolderList[126]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[127]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[128]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[129]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[130]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[131]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[132]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[133]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rollAngle" 
		"rigwRN.placeHolderList[134]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.roll" 
		"rigwRN.placeHolderList[135]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.toe" 
		"rigwRN.placeHolderList[136]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[137]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[138]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[139]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[140]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateX" 
		"rigwRN.placeHolderList[141]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateY" 
		"rigwRN.placeHolderList[142]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateZ" 
		"rigwRN.placeHolderList[143]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L.rotateX" 
		"rigwRN.placeHolderList[144]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateX" 
		"rigwRN.placeHolderList[145]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateY" 
		"rigwRN.placeHolderList[146]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateZ" 
		"rigwRN.placeHolderList[147]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.follow" 
		"rigwRN.placeHolderList[148]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R.FKIKBlend" 
		"rigwRN.placeHolderList[149]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R.IKVis" 
		"rigwRN.placeHolderList[150]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R.FKVis" 
		"rigwRN.placeHolderList[151]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKIKBlend" 
		"rigwRN.placeHolderList[152]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.IKVis" 
		"rigwRN.placeHolderList[153]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKVis" 
		"rigwRN.placeHolderList[154]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.FKIKBlend" 
		"rigwRN.placeHolderList[155]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.IKVis" 
		"rigwRN.placeHolderList[156]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.FKVis" 
		"rigwRN.placeHolderList[157]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.FKIKBlend" 
		"rigwRN.placeHolderList[158]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.IKVis" 
		"rigwRN.placeHolderList[159]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.FKVis" 
		"rigwRN.placeHolderList[160]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKIKBlend" 
		"rigwRN.placeHolderList[161]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.IKVis" 
		"rigwRN.placeHolderList[162]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKVis" 
		"rigwRN.placeHolderList[163]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr ".cm" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".cs" 3;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 7 -ast 1 -aet 121 ";
	setAttr ".st" 6;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -0.17224075274600581 2 -0.23627759439451521
		 3 -0.30031443881395647 4 -0.36435128323339799 5 -0.56089304231485704 6 -0.4283881276528389
		 7 -0.07306777956345463;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 0.4617253839969635 0.46172526478767395 
		0.4617253839969635 0.16721145808696747 0.24396194517612457 0.0934019535779953;
	setAttr -s 7 ".kiy[0:6]"  0 -0.88702297210693359 -0.88702297210693359 
		-0.88702297210693359 -0.98592102527618408 0.96978473663330078 0.9956284761428833;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -8.457404975444792 2 -8.7715129927673061
		 3 -9.39897206654258 4 -9.2106586483230259 5 -9.7664149861813527 6 -10.062119424009827;
	setAttr -s 6 ".kit[0:5]"  1 9 9 1 1 9;
	setAttr -s 6 ".kix[0:5]"  1 0.97093641757965088 0.99345594644546509 
		1 1 0.98822504281997681;
	setAttr -s 6 ".kiy[0:5]"  0 -0.23933747410774231 -0.11421570926904678 
		0 0 -0.15300737321376801;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 3.5950918666486422 2 3.6447677706308843
		 3 2.8398869808156131 4 2.9909279579822181 5 1.5328911044202982 6 0.7406706794464103;
	setAttr -s 6 ".kit[0:5]"  1 9 9 1 1 9;
	setAttr -s 6 ".kix[0:5]"  1 0.98100990056991577 0.98566389083862305 
		1 1 0.92368602752685547;
	setAttr -s 6 ".kiy[0:5]"  0 -0.1939576119184494 -0.16872087121009827 
		0 0 -0.38315019011497498;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -7.4921688033583393 2 -7.4506465616669475
		 3 -7.3674850334232698 4 -7.392421787410286 5 -7.3852379068543437 6 -7.3867086347622237;
	setAttr -s 6 ".kit[0:5]"  1 9 9 1 1 9;
	setAttr -s 6 ".kix[0:5]"  0.95329487323760986 0.99946767091751099 
		0.99988389015197754 1 1 0.99999964237213135;
	setAttr -s 6 ".kiy[0:5]"  0.30204126238822937 0.032624758780002594 
		0.015241440385580063 0 0 -0.00077007111394777894;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 1.2415313847348244 3 2.6485961489963428
		 4 4.4364013430144897 5 6.0321838438812412 6 6.4811181163134766 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 0.83841902017593384 0.80509603023529053 
		0.730049729347229 0.76738417148590088 0.97346764802932739 0.28266319632530212;
	setAttr -s 7 ".kiy[0:6]"  0 0.54502624273300171 0.59314441680908203 
		0.68339401483535767 0.64118766784667969 0.22882470488548279 -0.95921921730041504;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 1.959012056743543 3 4.1792191910127023
		 4 7.0001965527234038 5 9.5181813556658099 6 10.22655463015052 7 0;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 0.69806689023971558 0.65213370323181152 
		0.56062173843383789 0.60431897640228271 0.93758589029312134 0.1835809201002121;
	setAttr -s 7 ".kiy[0:6]"  0 0.71603256464004517 0.75810402631759644 
		0.82807201147079468 0.79674249887466431 0.34775379300117493 -0.98300457000732422;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -4.8697690470150734 2 -6.1381871693771863
		 3 -7.5757234799938429 4 -9.4022455761952877 5 -11.032586515083477 6 -11.491242956558873
		 7 -4.8697690470150734;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  0.99206048250198364 0.8330196738243103 
		0.79896628856658936 0.72269856929779053 0.76056975126266479 0.97235375642776489 0.27713635563850403;
	setAttr -s 7 ".kiy[0:6]"  0.1257617324590683 -0.55324345827102661 
		-0.60137581825256348 -0.69116330146789551 -0.64925622940063477 -0.23351265490055084 
		0.96083062887191772;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -1.9043928141465971 2 -2.075645779858994
		 3 -2.3657245051808324 4 -2.2734581669739859 5 -2.4884186767548919 6 -2.602427124480871;
	setAttr -s 6 ".kit[0:5]"  1 9 9 1 1 9;
	setAttr -s 6 ".kix[0:5]"  1 0.99278545379638672 0.99866175651550293 
		1 1 0.9982229471206665;
	setAttr -s 6 ".kiy[0:5]"  0 -0.1199050098657608 -0.051717855036258698 
		0 0 -0.059588607400655746;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -22.969544335884148 2 -22.969544335884148
		 3 -23.371106716411276 4 -23.290794240305843 5 -23.977561699684564 6 -24.350196632134345;
	setAttr -s 6 ".kit[0:5]"  1 9 9 1 1 9;
	setAttr -s 6 ".kix[0:5]"  1 0.99451941251754761 0.99648201465606689 
		1 1 0.98149251937866211;
	setAttr -s 6 ".kiy[0:5]"  0 -0.10455262660980225 -0.08380715548992157 
		0 0 -0.19150018692016602;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 0 3 0 4 0 5 -0.014535476284531163
		 6 -0.022250581011584121;
	setAttr -s 6 ".kit[0:5]"  1 9 9 1 1 9;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.99999183416366577;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 -0.0040395865216851234;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 5.9652109157717845 2 5.4153761300809471
		 3 4.7922318724139501 4 4.0004697810191461 5 3.2937484365879959 6 3.0949297176195878
		 7 5.9652109157717845;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 0.96096903085708618 0.95067638158798218 
		0.92376452684402466 0.93785011768341064 0.99462509155273438 0.55396467447280884;
	setAttr -s 7 ".kiy[0:6]"  0 -0.27665606141090393 -0.31018438935279846 
		-0.3829609751701355 -0.34704059362411499 -0.10354170948266983 0.83254009485244751;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -6.7498829697065776 2 -6.1933773758524051
		 3 -5.5626728911696564 4 -4.7613048360196482 5 -4.0460092750486885 6 -3.8447784105266822
		 7 -6.7498829697065776;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 0.96007239818572998 0.94956189393997192 
		0.92211532592773438 0.93647438287734985 0.9944949746131897 0.54933667182922363;
	setAttr -s 7 ".kiy[0:6]"  0 0.27975133061408997 0.31357961893081665 
		0.38691520690917969 0.35073578357696533 0.10478420555591583 -0.8356010913848877;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -29.071287445800365 2 -29.822025080611173
		 3 -30.672858564271294 4 -31.753920758398849 5 -32.718869365500765 6 -32.990333992182961
		 7 -29.071287445800365;
	setAttr -s 7 ".kit[0:6]"  1 2 2 2 2 2 2;
	setAttr -s 7 ".kix[0:6]"  1 0.93067914247512817 0.91345489025115967 
		0.87025505304336548 0.89254635572433472 0.99004888534545898 0.43807694315910339;
	setAttr -s 7 ".kiy[0:6]"  0 -0.36583626270294189 -0.40693995356559753 
		-0.49260154366493225 -0.45095545053482056 -0.14072410762310028 0.89893746376037598;
createNode animCurveTA -n "rigw:rig:FKClavicle_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -3.9350810706885611;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKClavicle_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKClavicle_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -3.9350810706885611;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 6.5699629208729471 5 11.370884124043949
		 6 8.6714558131184809;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 0.57756704092025757;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 -0.81634330749511719;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 6.5699629208729471 5 11.370884124043949
		 6 8.6714558131184809;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 0.57756704092025757;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 -0.81634330749511719;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 9.9544894101963752 5 17.228612544791332
		 6 13.138569593510642;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 0.42309874296188354;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 -0.90608352422714233;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 6.5699629208729471 5 11.370884124043949
		 6 8.6714558131184809;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 0.57756704092025757;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 -0.81634330749511719;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 6.5699629208729471 5 11.370884124043949
		 6 8.6714558131184809;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 0.57756704092025757;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 -0.81634330749511719;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 6.5699629208729471 5 11.370884124043949
		 6 8.6714558131184809;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  1 1 1 0.57756704092025757;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 -0.81634330749511719;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891518372467 4 17.228458688379426
		 5 25.350716810264466 6 18.86923989098208;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  0.78625392913818359 0.97567397356033325 
		0.99646902084350586 0.28264880180358887;
	setAttr -s 4 ".kiy[0:3]"  -0.61790353059768677 0.21922639012336731 
		-0.083962082862854004 -0.95922344923019409;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891518372467 4 17.228458688379426
		 5 25.350716810264466 6 18.86923989098208;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  0.78625392913818359 0.97567397356033325 
		0.99646902084350586 0.28264880180358887;
	setAttr -s 4 ".kiy[0:3]"  -0.61790353059768677 0.21922639012336731 
		-0.083962082862854004 -0.95922344923019409;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891518372467 4 17.228458688379426
		 5 25.350716810264466 6 18.86923989098208;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  0.78625392913818359 0.97567397356033325 
		0.99646902084350586 0.28264880180358887;
	setAttr -s 4 ".kiy[0:3]"  -0.61790353059768677 0.21922639012336731 
		-0.083962082862854004 -0.95922344923019409;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891518372467 4 17.228458688379426
		 5 25.350716810264466 6 18.86923989098208;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  0.78625392913818359 0.97567397356033325 
		0.99646902084350586 0.28264880180358887;
	setAttr -s 4 ".kiy[0:3]"  -0.61790353059768677 0.21922639012336731 
		-0.083962082862854004 -0.95922344923019409;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891518372467 4 17.228458688379426
		 5 25.350716810264466 6 18.86923989098208;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  0.78625392913818359 0.97567397356033325 
		0.99646902084350586 0.28264880180358887;
	setAttr -s 4 ".kiy[0:3]"  -0.61790353059768677 0.21922639012336731 
		-0.083962082862854004 -0.95922344923019409;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 5 0 6 0;
	setAttr -s 3 ".kit[0:2]"  1 1 9;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891518372467 4 17.228458688379426
		 5 25.350716810264466 6 18.86923989098208;
	setAttr -s 4 ".kit[0:3]"  1 1 1 9;
	setAttr -s 4 ".kix[0:3]"  0.78625392913818359 0.97567397356033325 
		0.99646902084350586 0.28264880180358887;
	setAttr -s 4 ".kiy[0:3]"  -0.61790353059768677 0.21922639012336731 
		-0.083962082862854004 -0.95922344923019409;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 -3.435927406321158 6 0 7 2.499690964537959;
	setAttr -s 5 ".kit[0:4]"  1 1 1 9 2;
	setAttr -s 5 ".kix[0:4]"  1 1 1 0.54115486145019531 0.60711526870727539;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0.84092289209365845 0.79461377859115601;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 0 6 0 7 0;
	setAttr -s 5 ".kit[0:4]"  1 1 1 9 2;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 0 6 0 7 0;
	setAttr -s 5 ".kit[0:4]"  1 1 1 9 2;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 -3.435927406321158 6 0 7 2.499690964537959;
	setAttr -s 5 ".kit[0:4]"  1 9 9 9 2;
	setAttr -s 5 ".kix[0:4]"  1 0.9120028018951416 1 0.54115486145019531 
		0.60711526870727539;
	setAttr -s 5 ".kiy[0:4]"  0 -0.41018402576446533 0 0.84092289209365845 
		0.79461377859115601;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 0 6 0 7 0;
	setAttr -s 5 ".kit[0:4]"  1 9 9 9 2;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 0 6 0 7 0;
	setAttr -s 5 ".kit[0:4]"  1 9 9 9 2;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 -3.435927406321158 6 0 7 2.499690964537959;
	setAttr -s 5 ".kit[0:4]"  1 1 1 9 2;
	setAttr -s 5 ".kix[0:4]"  1 1 1 0.54115486145019531 0.60711526870727539;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0.84092289209365845 0.79461377859115601;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 0 6 0 7 0;
	setAttr -s 5 ".kit[0:4]"  1 1 1 9 2;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 0 6 0 7 0;
	setAttr -s 5 ".kit[0:4]"  1 1 1 9 2;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 -3.435927406321158 6 0 7 2.499690964537959;
	setAttr -s 5 ".kit[0:4]"  1 9 9 9 2;
	setAttr -s 5 ".kix[0:4]"  1 0.9120028018951416 1 0.54115486145019531 
		0.60711526870727539;
	setAttr -s 5 ".kiy[0:4]"  0 -0.41018402576446533 0 0.84092289209365845 
		0.79461377859115601;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 0 6 0 7 0;
	setAttr -s 5 ".kit[0:4]"  1 9 9 9 2;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 5 0 6 0 7 0;
	setAttr -s 5 ".kit[0:4]"  1 9 9 9 2;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 75.860718914179515;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -0.350318;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -0.040106700000000002;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -6.531151;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_roll";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_rollAngle";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 25;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_toe";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLegBall_R_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:PoleLeg_R_follow";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 10;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.237274;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.38742;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 7.576037;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_roll";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_rollAngle";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 25;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_toe";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:IKLegBall_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:PoleLeg_L_follow";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 10;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKArm_R_FKIKBlend";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKArm_R_FKVis";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKArm_R_IKVis";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKIKBlend";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 10;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKVis";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_IKVis";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_FKIKBlend";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_FKVis";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_IKVis";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_FKIKBlend";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_FKVis";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_IKVis";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKIKBlend";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 10;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKVis";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_IKVis";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:Main_visibility";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:Main_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:Main_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTL -n "rigw:rig:Main_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:Main_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:Main_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTA -n "rigw:rig:Main_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:Main_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2.2;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:Main_scaleY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2.2;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode animCurveTU -n "rigw:rig:Main_scaleZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2.2;
	setAttr ".kot[0]"  5;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
createNode mia_exposure_simple -n "mia_exposure_simple1";
	setAttr ".S02" 1;
	setAttr ".S04" 1.1499999761581421;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 18;
	setAttr -av ".unw" 18;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 11 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 13 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 20 ".tx";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 3 ".sol";
connectAttr "rigw:rig:Main_scaleY.o" "rigwRN.phl[1]";
connectAttr "rigw:rig:Main_scaleX.o" "rigwRN.phl[2]";
connectAttr "rigw:rig:Main_scaleZ.o" "rigwRN.phl[3]";
connectAttr "rigw:rig:Main_translateX.o" "rigwRN.phl[4]";
connectAttr "rigw:rig:Main_translateY.o" "rigwRN.phl[5]";
connectAttr "rigw:rig:Main_translateZ.o" "rigwRN.phl[6]";
connectAttr "rigw:rig:Main_visibility.o" "rigwRN.phl[7]";
connectAttr "rigw:rig:Main_rotateX.o" "rigwRN.phl[8]";
connectAttr "rigw:rig:Main_rotateY.o" "rigwRN.phl[9]";
connectAttr "rigw:rig:Main_rotateZ.o" "rigwRN.phl[10]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateX.o" "rigwRN.phl[11]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateY.o" "rigwRN.phl[12]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateZ.o" "rigwRN.phl[13]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateX.o" "rigwRN.phl[14]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateY.o" "rigwRN.phl[15]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateZ.o" "rigwRN.phl[16]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateX.o" "rigwRN.phl[17]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateY.o" "rigwRN.phl[18]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateZ.o" "rigwRN.phl[19]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateX.o" "rigwRN.phl[20]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateY.o" "rigwRN.phl[21]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateZ.o" "rigwRN.phl[22]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateX.o" "rigwRN.phl[23]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateY.o" "rigwRN.phl[24]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateZ.o" "rigwRN.phl[25]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateX.o" "rigwRN.phl[26]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateY.o" "rigwRN.phl[27]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateZ.o" "rigwRN.phl[28]";
connectAttr "rigw:rig:FKClavicle_R_rotateX.o" "rigwRN.phl[29]";
connectAttr "rigw:rig:FKClavicle_R_rotateY.o" "rigwRN.phl[30]";
connectAttr "rigw:rig:FKClavicle_R_rotateZ.o" "rigwRN.phl[31]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[32]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[33]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[34]";
connectAttr "rigw:rig:FKElbow_R_rotateX.o" "rigwRN.phl[35]";
connectAttr "rigw:rig:FKElbow_R_rotateY.o" "rigwRN.phl[36]";
connectAttr "rigw:rig:FKElbow_R_rotateZ.o" "rigwRN.phl[37]";
connectAttr "rigw:rig:FKWrist_R_rotateX.o" "rigwRN.phl[38]";
connectAttr "rigw:rig:FKWrist_R_rotateY.o" "rigwRN.phl[39]";
connectAttr "rigw:rig:FKWrist_R_rotateZ.o" "rigwRN.phl[40]";
connectAttr "rigw:rig:FKShoulderPad1_R_rotateX.o" "rigwRN.phl[41]";
connectAttr "rigw:rig:FKShoulderPad1_R_rotateY.o" "rigwRN.phl[42]";
connectAttr "rigw:rig:FKShoulderPad1_R_rotateZ.o" "rigwRN.phl[43]";
connectAttr "rigw:rig:FKShoulderPad2_R_rotateX.o" "rigwRN.phl[44]";
connectAttr "rigw:rig:FKShoulderPad2_R_rotateY.o" "rigwRN.phl[45]";
connectAttr "rigw:rig:FKShoulderPad2_R_rotateZ.o" "rigwRN.phl[46]";
connectAttr "rigw:rig:FKClavicle_L_rotateX.o" "rigwRN.phl[47]";
connectAttr "rigw:rig:FKClavicle_L_rotateY.o" "rigwRN.phl[48]";
connectAttr "rigw:rig:FKClavicle_L_rotateZ.o" "rigwRN.phl[49]";
connectAttr "rigw:rig:FKShoulder_L_rotateX.o" "rigwRN.phl[50]";
connectAttr "rigw:rig:FKShoulder_L_rotateY.o" "rigwRN.phl[51]";
connectAttr "rigw:rig:FKShoulder_L_rotateZ.o" "rigwRN.phl[52]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[53]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[54]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[55]";
connectAttr "rigw:rig:FKWrist_L_rotateX.o" "rigwRN.phl[56]";
connectAttr "rigw:rig:FKWrist_L_rotateY.o" "rigwRN.phl[57]";
connectAttr "rigw:rig:FKWrist_L_rotateZ.o" "rigwRN.phl[58]";
connectAttr "rigw:rig:FKShoulderPad1_L_rotateX.o" "rigwRN.phl[59]";
connectAttr "rigw:rig:FKShoulderPad1_L_rotateY.o" "rigwRN.phl[60]";
connectAttr "rigw:rig:FKShoulderPad1_L_rotateZ.o" "rigwRN.phl[61]";
connectAttr "rigw:rig:FKShoulderPad2_L_rotateX.o" "rigwRN.phl[62]";
connectAttr "rigw:rig:FKShoulderPad2_L_rotateY.o" "rigwRN.phl[63]";
connectAttr "rigw:rig:FKShoulderPad2_L_rotateZ.o" "rigwRN.phl[64]";
connectAttr "rigw:rig:FKHipTwist_R_rotateX.o" "rigwRN.phl[65]";
connectAttr "rigw:rig:FKHipTwist_R_rotateY.o" "rigwRN.phl[66]";
connectAttr "rigw:rig:FKHipTwist_R_rotateZ.o" "rigwRN.phl[67]";
connectAttr "rigw:rig:FKHipTwist_L_rotateX.o" "rigwRN.phl[68]";
connectAttr "rigw:rig:FKHipTwist_L_rotateY.o" "rigwRN.phl[69]";
connectAttr "rigw:rig:FKHipTwist_L_rotateZ.o" "rigwRN.phl[70]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateX.o" "rigwRN.phl[71]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateY.o" "rigwRN.phl[72]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateZ.o" "rigwRN.phl[73]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateX.o" "rigwRN.phl[74]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateY.o" "rigwRN.phl[75]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateZ.o" "rigwRN.phl[76]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateX.o" "rigwRN.phl[77]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateY.o" "rigwRN.phl[78]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateZ.o" "rigwRN.phl[79]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateX.o" "rigwRN.phl[80]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateY.o" "rigwRN.phl[81]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateZ.o" "rigwRN.phl[82]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateX.o" "rigwRN.phl[83]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateY.o" "rigwRN.phl[84]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateZ.o" "rigwRN.phl[85]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateX.o" "rigwRN.phl[86]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateY.o" "rigwRN.phl[87]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateZ.o" "rigwRN.phl[88]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[89]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[90]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[91]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[92]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[93]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[94]";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[95]";
connectAttr "rigw:rig:FKSpineA_M_rotateX.o" "rigwRN.phl[96]";
connectAttr "rigw:rig:FKSpineA_M_rotateY.o" "rigwRN.phl[97]";
connectAttr "rigw:rig:FKSpineA_M_rotateZ.o" "rigwRN.phl[98]";
connectAttr "rigw:rig:FKChest_M_rotateX.o" "rigwRN.phl[99]";
connectAttr "rigw:rig:FKChest_M_rotateY.o" "rigwRN.phl[100]";
connectAttr "rigw:rig:FKChest_M_rotateZ.o" "rigwRN.phl[101]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateX.o" "rigwRN.phl[102]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateY.o" "rigwRN.phl[103]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateZ.o" "rigwRN.phl[104]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[105]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[106]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[107]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[108]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[109]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[110]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[111]";
connectAttr "rigw:rig:IKLeg_R_rollAngle.o" "rigwRN.phl[112]";
connectAttr "rigw:rig:IKLeg_R_roll.o" "rigwRN.phl[113]";
connectAttr "rigw:rig:IKLeg_R_toe.o" "rigwRN.phl[114]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[115]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[116]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[117]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[118]";
connectAttr "rigw:rig:IKLegHeel_R_rotateX.o" "rigwRN.phl[119]";
connectAttr "rigw:rig:IKLegHeel_R_rotateY.o" "rigwRN.phl[120]";
connectAttr "rigw:rig:IKLegHeel_R_rotateZ.o" "rigwRN.phl[121]";
connectAttr "rigw:rig:IKLegBall_R_rotateX.o" "rigwRN.phl[122]";
connectAttr "rigw:rig:PoleLeg_R_translateX.o" "rigwRN.phl[123]";
connectAttr "rigw:rig:PoleLeg_R_translateY.o" "rigwRN.phl[124]";
connectAttr "rigw:rig:PoleLeg_R_translateZ.o" "rigwRN.phl[125]";
connectAttr "rigw:rig:PoleLeg_R_follow.o" "rigwRN.phl[126]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[127]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[128]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[129]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[130]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[131]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[132]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[133]";
connectAttr "rigw:rig:IKLeg_L_rollAngle.o" "rigwRN.phl[134]";
connectAttr "rigw:rig:IKLeg_L_roll.o" "rigwRN.phl[135]";
connectAttr "rigw:rig:IKLeg_L_toe.o" "rigwRN.phl[136]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[137]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[138]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[139]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[140]";
connectAttr "rigw:rig:IKLegHeel_L_rotateX.o" "rigwRN.phl[141]";
connectAttr "rigw:rig:IKLegHeel_L_rotateY.o" "rigwRN.phl[142]";
connectAttr "rigw:rig:IKLegHeel_L_rotateZ.o" "rigwRN.phl[143]";
connectAttr "rigw:rig:IKLegBall_L_rotateX.o" "rigwRN.phl[144]";
connectAttr "rigw:rig:PoleLeg_L_translateX.o" "rigwRN.phl[145]";
connectAttr "rigw:rig:PoleLeg_L_translateY.o" "rigwRN.phl[146]";
connectAttr "rigw:rig:PoleLeg_L_translateZ.o" "rigwRN.phl[147]";
connectAttr "rigw:rig:PoleLeg_L_follow.o" "rigwRN.phl[148]";
connectAttr "rigw:rig:FKIKArm_R_FKIKBlend.o" "rigwRN.phl[149]";
connectAttr "rigw:rig:FKIKArm_R_IKVis.o" "rigwRN.phl[150]";
connectAttr "rigw:rig:FKIKArm_R_FKVis.o" "rigwRN.phl[151]";
connectAttr "rigw:rig:FKIKLeg_R_FKIKBlend.o" "rigwRN.phl[152]";
connectAttr "rigw:rig:FKIKLeg_R_IKVis.o" "rigwRN.phl[153]";
connectAttr "rigw:rig:FKIKLeg_R_FKVis.o" "rigwRN.phl[154]";
connectAttr "rigw:rig:FKIKSpine_M_FKIKBlend.o" "rigwRN.phl[155]";
connectAttr "rigw:rig:FKIKSpine_M_IKVis.o" "rigwRN.phl[156]";
connectAttr "rigw:rig:FKIKSpine_M_FKVis.o" "rigwRN.phl[157]";
connectAttr "rigw:rig:FKIKArm_L_FKIKBlend.o" "rigwRN.phl[158]";
connectAttr "rigw:rig:FKIKArm_L_IKVis.o" "rigwRN.phl[159]";
connectAttr "rigw:rig:FKIKArm_L_FKVis.o" "rigwRN.phl[160]";
connectAttr "rigw:rig:FKIKLeg_L_FKIKBlend.o" "rigwRN.phl[161]";
connectAttr "rigw:rig:FKIKLeg_L_IKVis.o" "rigwRN.phl[162]";
connectAttr "rigw:rig:FKIKLeg_L_FKVis.o" "rigwRN.phl[163]";
connectAttr "mia_exposure_simple1.msg" "orthCamShape.mils";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "mia_exposure_simple1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Juggernaut__mc-rigw@Jug_idle.ma
