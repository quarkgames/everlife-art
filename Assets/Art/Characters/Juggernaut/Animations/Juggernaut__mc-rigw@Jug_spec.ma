//Maya ASCII 2013 scene
//Name: Juggernaut__mc-rigw@Jug_spec.ma
//Last modified: Thu, Jun 05, 2014 03:25:24 PM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Juggernaut/Juggernaut__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Juggernaut__rig.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Juggernaut/Juggernaut__mc-rigw.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 9.6541023741209102 32.53606379725074 -62.236795303412613 ;
	setAttr ".r" -type "double3" -18.938352729590786 -188.1999999999959 0 ;
	setAttr ".rp" -type "double3" 3.5527136788005009e-15 -1.7763568394002505e-15 0 ;
	setAttr ".rpt" -type "double3" 2.0686315357737022e-18 3.3002374587562451e-17 3.185216484922183e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 72.661735328112741;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" 0 0 500 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".ow" 72;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 11 ".lnk";
	setAttr -s 11 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 203 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 0
		"rigw:rigRN" 0
		"rigw:rigRN" 1413
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "visibility" " -av 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scale" " -type \"double3\" 2.2 2.2 2.2"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateOrder" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateOrder" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotate" " -type \"double3\" -3.935081 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateOrder" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateOrder" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateOrder" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotate" " -type \"double3\" -3.935081 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotate" " -type \"double3\" 0 0 -1.704419"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 -4.869769"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotate" " -type \"double3\" 0 0 12.078892"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotate" " -type \"double3\" 0 0 12.078892"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotate" " -type \"double3\" 0 0 12.078892"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotate" " -type \"double3\" 0 0 12.078892"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotate" " -type \"double3\" 0 0 12.078892"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotate" " -type \"double3\" 0 0 12.078892"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translate" " -type \"double3\" 0 0.0288533 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"CenterBtwFeet" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"translate" " -type \"double3\" 0 -0.611751 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R" 
		"follow" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_R|rigw:rig:PoleExtraArm_R|rigw:rig:PoleArm_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_R|rigw:rig:PoleExtraArm_R|rigw:rig:PoleArm_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_R|rigw:rig:PoleExtraArm_R|rigw:rig:PoleArm_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_R|rigw:rig:PoleExtraArm_R|rigw:rig:PoleArm_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_R|rigw:rig:PoleExtraArm_R|rigw:rig:PoleArm_R" 
		"follow" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" -0.350318 0 -0.0401067"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 -6.531151 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"toe" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M|rigw:rig:IKXSpineHandle_M" 
		"translate" " -type \"double3\" -2.24645e-08 0.00443195 -0.000534086"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M|rigw:rig:IKXSpineHandle_M" 
		"rotate" " -type \"double3\" 0.190416 -1.07353e-05 -1.6939e-06"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L" 
		"translate" " -type \"double3\" 0 -0.611751 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L" 
		"follow" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L" 
		"follow" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0.237274 0 -1.38742"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 7.576037 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"toe" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R" 
		"FKIKBlend" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R" 
		"FKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R" 
		"IKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"IKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M" 
		"FKIKBlend" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M" 
		"FKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M" 
		"IKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L" 
		"FKIKBlend" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L" 
		"FKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L" 
		"IKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"IKVis" " -av -k 1 1"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleY" "rigwRN.placeHolderList[1]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleX" "rigwRN.placeHolderList[2]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleZ" "rigwRN.placeHolderList[3]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateX" "rigwRN.placeHolderList[4]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateY" "rigwRN.placeHolderList[5]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateZ" "rigwRN.placeHolderList[6]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.visibility" "rigwRN.placeHolderList[7]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateX" "rigwRN.placeHolderList[8]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateY" "rigwRN.placeHolderList[9]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateZ" "rigwRN.placeHolderList[10]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateX" 
		"rigwRN.placeHolderList[11]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateY" 
		"rigwRN.placeHolderList[12]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[13]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateX" 
		"rigwRN.placeHolderList[14]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateY" 
		"rigwRN.placeHolderList[15]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[16]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateX" 
		"rigwRN.placeHolderList[17]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateY" 
		"rigwRN.placeHolderList[18]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[19]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateX" 
		"rigwRN.placeHolderList[20]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateY" 
		"rigwRN.placeHolderList[21]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[22]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateX" 
		"rigwRN.placeHolderList[23]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateY" 
		"rigwRN.placeHolderList[24]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[25]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateX" 
		"rigwRN.placeHolderList[26]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateY" 
		"rigwRN.placeHolderList[27]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[28]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateX" 
		"rigwRN.placeHolderList[29]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateY" 
		"rigwRN.placeHolderList[30]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateZ" 
		"rigwRN.placeHolderList[31]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[32]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[33]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[34]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateX" 
		"rigwRN.placeHolderList[35]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateY" 
		"rigwRN.placeHolderList[36]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateZ" 
		"rigwRN.placeHolderList[37]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateX" 
		"rigwRN.placeHolderList[38]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateY" 
		"rigwRN.placeHolderList[39]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateZ" 
		"rigwRN.placeHolderList[40]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R.rotateX" 
		"rigwRN.placeHolderList[41]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R.rotateY" 
		"rigwRN.placeHolderList[42]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R.rotateZ" 
		"rigwRN.placeHolderList[43]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R.rotateX" 
		"rigwRN.placeHolderList[44]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R.rotateY" 
		"rigwRN.placeHolderList[45]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R.rotateZ" 
		"rigwRN.placeHolderList[46]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L.rotateX" 
		"rigwRN.placeHolderList[47]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L.rotateY" 
		"rigwRN.placeHolderList[48]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L.rotateZ" 
		"rigwRN.placeHolderList[49]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateX" 
		"rigwRN.placeHolderList[50]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateY" 
		"rigwRN.placeHolderList[51]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateZ" 
		"rigwRN.placeHolderList[52]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[53]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[54]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[55]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateX" 
		"rigwRN.placeHolderList[56]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateY" 
		"rigwRN.placeHolderList[57]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateZ" 
		"rigwRN.placeHolderList[58]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L.rotateX" 
		"rigwRN.placeHolderList[59]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L.rotateY" 
		"rigwRN.placeHolderList[60]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L.rotateZ" 
		"rigwRN.placeHolderList[61]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L.rotateX" 
		"rigwRN.placeHolderList[62]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L.rotateY" 
		"rigwRN.placeHolderList[63]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L.rotateZ" 
		"rigwRN.placeHolderList[64]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateX" 
		"rigwRN.placeHolderList[65]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateY" 
		"rigwRN.placeHolderList[66]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateZ" 
		"rigwRN.placeHolderList[67]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R.rotateX" 
		"rigwRN.placeHolderList[68]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R.rotateY" 
		"rigwRN.placeHolderList[69]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R.rotateZ" 
		"rigwRN.placeHolderList[70]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R.rotateX" 
		"rigwRN.placeHolderList[71]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R.rotateY" 
		"rigwRN.placeHolderList[72]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R.rotateZ" 
		"rigwRN.placeHolderList[73]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R|rigw:rig:FKXKnee_R|rigw:rig:FKOffsetAnkle_R|rigw:rig:FKExtraAnkle_R|rigw:rig:FKAnkle_R.rotateX" 
		"rigwRN.placeHolderList[74]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R|rigw:rig:FKXKnee_R|rigw:rig:FKOffsetAnkle_R|rigw:rig:FKExtraAnkle_R|rigw:rig:FKAnkle_R.rotateY" 
		"rigwRN.placeHolderList[75]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R|rigw:rig:FKXKnee_R|rigw:rig:FKOffsetAnkle_R|rigw:rig:FKExtraAnkle_R|rigw:rig:FKAnkle_R.rotateZ" 
		"rigwRN.placeHolderList[76]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateX" 
		"rigwRN.placeHolderList[77]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateY" 
		"rigwRN.placeHolderList[78]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateZ" 
		"rigwRN.placeHolderList[79]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L.rotateX" 
		"rigwRN.placeHolderList[80]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L.rotateY" 
		"rigwRN.placeHolderList[81]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L.rotateZ" 
		"rigwRN.placeHolderList[82]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L.rotateX" 
		"rigwRN.placeHolderList[83]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L.rotateY" 
		"rigwRN.placeHolderList[84]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L.rotateZ" 
		"rigwRN.placeHolderList[85]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L|rigw:rig:FKXKnee_L|rigw:rig:FKOffsetAnkle_L|rigw:rig:FKExtraAnkle_L|rigw:rig:FKAnkle_L.rotateX" 
		"rigwRN.placeHolderList[86]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L|rigw:rig:FKXKnee_L|rigw:rig:FKOffsetAnkle_L|rigw:rig:FKExtraAnkle_L|rigw:rig:FKAnkle_L.rotateY" 
		"rigwRN.placeHolderList[87]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L|rigw:rig:FKXKnee_L|rigw:rig:FKOffsetAnkle_L|rigw:rig:FKExtraAnkle_L|rigw:rig:FKAnkle_L.rotateZ" 
		"rigwRN.placeHolderList[88]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateX" 
		"rigwRN.placeHolderList[89]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateY" 
		"rigwRN.placeHolderList[90]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[91]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateX" 
		"rigwRN.placeHolderList[92]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateY" 
		"rigwRN.placeHolderList[93]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[94]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateX" 
		"rigwRN.placeHolderList[95]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateY" 
		"rigwRN.placeHolderList[96]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[97]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateX" 
		"rigwRN.placeHolderList[98]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateY" 
		"rigwRN.placeHolderList[99]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[100]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateX" 
		"rigwRN.placeHolderList[101]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateY" 
		"rigwRN.placeHolderList[102]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[103]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateX" 
		"rigwRN.placeHolderList[104]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateY" 
		"rigwRN.placeHolderList[105]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[106]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[107]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[108]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[109]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[110]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[111]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[112]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[113]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateX" 
		"rigwRN.placeHolderList[114]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateY" 
		"rigwRN.placeHolderList[115]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateZ" 
		"rigwRN.placeHolderList[116]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateX" 
		"rigwRN.placeHolderList[117]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateY" 
		"rigwRN.placeHolderList[118]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateZ" 
		"rigwRN.placeHolderList[119]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateX" 
		"rigwRN.placeHolderList[120]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateY" 
		"rigwRN.placeHolderList[121]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateZ" 
		"rigwRN.placeHolderList[122]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R.rotateX" 
		"rigwRN.placeHolderList[123]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R.rotateY" 
		"rigwRN.placeHolderList[124]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R.rotateZ" 
		"rigwRN.placeHolderList[125]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R.translateX" 
		"rigwRN.placeHolderList[126]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R.translateY" 
		"rigwRN.placeHolderList[127]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R.translateZ" 
		"rigwRN.placeHolderList[128]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_R|rigw:rig:IKExtraArm_R|rigw:rig:IKArm_R.follow" 
		"rigwRN.placeHolderList[129]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_R|rigw:rig:PoleExtraArm_R|rigw:rig:PoleArm_R.translateX" 
		"rigwRN.placeHolderList[130]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_R|rigw:rig:PoleExtraArm_R|rigw:rig:PoleArm_R.translateY" 
		"rigwRN.placeHolderList[131]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_R|rigw:rig:PoleExtraArm_R|rigw:rig:PoleArm_R.translateZ" 
		"rigwRN.placeHolderList[132]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_R|rigw:rig:PoleExtraArm_R|rigw:rig:PoleArm_R.follow" 
		"rigwRN.placeHolderList[133]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[134]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[135]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[136]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[137]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[138]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[139]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[140]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rollAngle" 
		"rigwRN.placeHolderList[141]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.roll" 
		"rigwRN.placeHolderList[142]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.toe" 
		"rigwRN.placeHolderList[143]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[144]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[145]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[146]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[147]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateX" 
		"rigwRN.placeHolderList[148]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateY" 
		"rigwRN.placeHolderList[149]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateZ" 
		"rigwRN.placeHolderList[150]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R.rotateX" 
		"rigwRN.placeHolderList[151]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateX" 
		"rigwRN.placeHolderList[152]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateY" 
		"rigwRN.placeHolderList[153]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateZ" 
		"rigwRN.placeHolderList[154]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.follow" 
		"rigwRN.placeHolderList[155]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.rotateX" 
		"rigwRN.placeHolderList[156]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.rotateY" 
		"rigwRN.placeHolderList[157]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.rotateZ" 
		"rigwRN.placeHolderList[158]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.translateX" 
		"rigwRN.placeHolderList[159]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.translateY" 
		"rigwRN.placeHolderList[160]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.translateZ" 
		"rigwRN.placeHolderList[161]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.follow" 
		"rigwRN.placeHolderList[162]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L.translateX" 
		"rigwRN.placeHolderList[163]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L.translateY" 
		"rigwRN.placeHolderList[164]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L.translateZ" 
		"rigwRN.placeHolderList[165]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L.follow" 
		"rigwRN.placeHolderList[166]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[167]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[168]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[169]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[170]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[171]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[172]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[173]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rollAngle" 
		"rigwRN.placeHolderList[174]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.roll" 
		"rigwRN.placeHolderList[175]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.toe" 
		"rigwRN.placeHolderList[176]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[177]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[178]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[179]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[180]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateX" 
		"rigwRN.placeHolderList[181]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateY" 
		"rigwRN.placeHolderList[182]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateZ" 
		"rigwRN.placeHolderList[183]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L.rotateX" 
		"rigwRN.placeHolderList[184]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateX" 
		"rigwRN.placeHolderList[185]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateY" 
		"rigwRN.placeHolderList[186]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateZ" 
		"rigwRN.placeHolderList[187]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.follow" 
		"rigwRN.placeHolderList[188]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R.FKIKBlend" 
		"rigwRN.placeHolderList[189]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R.IKVis" 
		"rigwRN.placeHolderList[190]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R.FKVis" 
		"rigwRN.placeHolderList[191]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKIKBlend" 
		"rigwRN.placeHolderList[192]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.IKVis" 
		"rigwRN.placeHolderList[193]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKVis" 
		"rigwRN.placeHolderList[194]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.FKIKBlend" 
		"rigwRN.placeHolderList[195]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.IKVis" 
		"rigwRN.placeHolderList[196]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.FKVis" 
		"rigwRN.placeHolderList[197]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.FKIKBlend" 
		"rigwRN.placeHolderList[198]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.IKVis" 
		"rigwRN.placeHolderList[199]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.FKVis" 
		"rigwRN.placeHolderList[200]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKIKBlend" 
		"rigwRN.placeHolderList[201]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.IKVis" 
		"rigwRN.placeHolderList[202]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKVis" 
		"rigwRN.placeHolderList[203]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".fil" 2;
	setAttr ".filw" 0.83333331346511841;
	setAttr ".filh" 0.83333331346511841;
	setAttr ".maxr" 2;
	setAttr ".gi" yes;
	setAttr ".fg" yes;
	setAttr ".cm" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".cs" 3;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 10 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode animCurveTL -n "rigw1:rig:FKAnkle_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKAnkle_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.1102230246251563e-16;
createNode animCurveTL -n "rigw1:rig:FKAnkle_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKAnkle_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKAnkle_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.1102230246251563e-16;
createNode animCurveTL -n "rigw1:rig:FKAnkle_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2.2204460492503131e-16;
createNode animCurveTL -n "rigw1:rig:FKChest_M_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKChest_M_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKChest_M_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKElbow_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKElbow_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKElbow_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKElbow_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2.2204460492503131e-16;
createNode animCurveTL -n "rigw1:rig:FKElbow_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKElbow_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKHip_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKHip_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKHip_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 3.3306690738754696e-16;
createNode animCurveTL -n "rigw1:rig:FKHip_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -4.4408920985006262e-16;
createNode animCurveTL -n "rigw1:rig:FKHip_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKHip_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -4.4408920985006262e-16;
createNode animCurveTL -n "rigw1:rig:FKIndexFinger1_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -8.8817841970012523e-16;
createNode animCurveTL -n "rigw1:rig:FKIndexFinger1_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -2.2204460492503131e-16;
createNode animCurveTL -n "rigw1:rig:FKIndexFinger1_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 4.4408920985006262e-16;
createNode animCurveTA -n "rigw1:rig:FKIndexFinger1_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 58.542128000000005;
createNode animCurveTA -n "rigw1:rig:FKIndexFinger1_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKIndexFinger1_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKKnee_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 8.8817841970012523e-16;
createNode animCurveTL -n "rigw1:rig:FKKnee_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKKnee_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKKnee_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -4.4408920985006262e-16;
createNode animCurveTL -n "rigw1:rig:FKKnee_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -4.4408920985006262e-16;
createNode animCurveTL -n "rigw1:rig:FKKnee_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2.2204460492503131e-16;
createNode animCurveTL -n "rigw1:rig:FKMiddleFinger1_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -3.6082248300317583e-16;
createNode animCurveTL -n "rigw1:rig:FKMiddleFinger1_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKMiddleFinger1_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKMiddleFinger1_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 58.542128000000005;
createNode animCurveTA -n "rigw1:rig:FKMiddleFinger1_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKMiddleFinger1_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKShoulder_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKShoulder_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKShoulder_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 8.8817841970012523e-16;
createNode animCurveTL -n "rigw1:rig:FKShoulder_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 8.8817841970012523e-16;
createNode animCurveTL -n "rigw1:rig:FKShoulder_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.3322676295501878e-15;
createNode animCurveTL -n "rigw1:rig:FKShoulder_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 8.8817841970012523e-16;
createNode animCurveTL -n "rigw1:rig:FKThumbFinger1_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKThumbFinger1_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -2.2204460492503131e-16;
createNode animCurveTL -n "rigw1:rig:FKThumbFinger1_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -2.2204460492503131e-16;
createNode animCurveTA -n "rigw1:rig:FKThumbFinger1_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 58.542128000000005;
createNode animCurveTA -n "rigw1:rig:FKThumbFinger1_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKThumbFinger1_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKWrist_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.1102230246251563e-16;
createNode animCurveTL -n "rigw1:rig:FKWrist_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKWrist_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -4.4408920985006262e-16;
createNode animCurveTL -n "rigw1:rig:FKWrist_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2.2204460492503131e-16;
createNode animCurveTL -n "rigw1:rig:FKWrist_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 5.5511151231257827e-17;
createNode animCurveTL -n "rigw1:rig:FKWrist_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw1:rig:IKLeg_R_toe";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 0 6 0 7 0 10 0;
	setAttr -s 5 ".kit[2:4]"  1 18 18;
	setAttr -s 5 ".kot[2:4]"  1 18 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 4 -0.21338314812660752 6 -0.2682122372898344
		 7 -0.26821223729999999 9 0 10 0;
	setAttr -s 6 ".kit[2:5]"  1 18 18 18;
	setAttr -s 6 ".kot[2:5]"  1 18 18 18;
	setAttr -s 6 ".kix[2:5]"  1 1 1 1;
	setAttr -s 6 ".kiy[2:5]"  0 0 0 0;
	setAttr -s 6 ".kox[2:5]"  1 1 1 1;
	setAttr -s 6 ".koy[2:5]"  0 0 0 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0.028853278069999998 4 -0.13740101058847398
		 6 -0.5399490404286611 7 -0.53994904040000002 9 0 10 0.028853278069999998;
	setAttr -s 6 ".kit[2:5]"  1 18 18 18;
	setAttr -s 6 ".kot[2:5]"  1 18 18 18;
	setAttr -s 6 ".kix[2:5]"  0.26527407765388489 1 0.35936492681503296 
		1;
	setAttr -s 6 ".kiy[2:5]"  -0.96417301893234253 0 0.93319708108901978 
		0;
	setAttr -s 6 ".kox[2:5]"  0.26527410745620728 1 0.35936495661735535 
		1;
	setAttr -s 6 ".koy[2:5]"  -0.9641730785369873 0 0.93319714069366455 
		0;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 4 -0.211262628189964 6 0.73266261714866443
		 7 -0.17897310590000001 9 0 10 0;
	setAttr -s 6 ".kit[2:5]"  1 18 18 18;
	setAttr -s 6 ".kot[2:5]"  1 18 18 18;
	setAttr -s 6 ".kix[2:5]"  0.12554770708084106 1 1 1;
	setAttr -s 6 ".kiy[2:5]"  0.99208760261535645 0 0 0;
	setAttr -s 6 ".kox[2:5]"  0.12554770708084106 1 1 1;
	setAttr -s 6 ".koy[2:5]"  0.99208760261535645 0 0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 -5.5410210053251179 6 13.447069689536816
		 7 13.447069689999999 10 0;
	setAttr -s 5 ".kit[2:4]"  1 18 18;
	setAttr -s 5 ".kot[2:4]"  1 18 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 2.4440131527346542 6 2.4440131527346542
		 7 2.4440131530000002 10 0;
	setAttr -s 5 ".kit[2:4]"  1 18 18;
	setAttr -s 5 ".kot[2:4]"  1 18 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 -0.23724242655477448 6 -0.23724242655477448
		 7 -0.23724242660000003 10 0;
	setAttr -s 5 ".kit[2:4]"  1 18 18;
	setAttr -s 5 ".kot[2:4]"  1 18 18;
	setAttr -s 5 ".kix[2:4]"  0.99237287044525146 1 1;
	setAttr -s 5 ".kiy[2:4]"  -0.12327239662408829 0 0;
	setAttr -s 5 ".kox[2:4]"  0.99237287044525146 1 1;
	setAttr -s 5 ".koy[2:4]"  -0.12327239662408829 0 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 4 -10.440261419970396 6 12.371144440258684
		 7 15.98188745160574 9 -1.1516409439999999 10 0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 4 0 6 6.4500275434232552 7 6.450027543
		 9 1.3557917610000001 10 0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 4 0 6 4.3242345041505139 7 4.3242345039999996
		 9 -0.027252439440000002 10 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -3.9350810709999999 3 -2.0954242501985938
		 4 6.0723240003420402 6 -17.555325752129473 7 -17.555325750000002 9 0 10 -3.9350810709999999;
createNode animCurveTA -n "rigw:rig:FKClavicle_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 7.0233677385596645 4 17.660740358826004
		 6 22.959753182992291 7 22.95975318 9 0 10 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 15.058135158241535 4 20.391890236335417
		 6 14.751303450633694 7 14.751303450000002 9 15.755373329999999 10 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -3.9350810709999999 3 -7.6029591661858964
		 4 -22.753906628499148 5 -5.7415008758937169 7 -4.8382909730000003 10 -3.9350810709999999;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 3 5.5304360509081265 4 8.9170864945436339
		 5 21.638278810142584 7 10.81913941 10 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 15.184490689276059 4 10.78057257980381
		 5 17.706487665180902 7 16.25688568 9 15.755373329999999 10 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 3 95.483971422057678 5 0 6 16.853511008712285
		 9 0 10 0;
	setAttr -s 6 ".kit[3:5]"  1 18 18;
	setAttr -s 6 ".kot[3:5]"  1 18 18;
	setAttr -s 6 ".kix[3:5]"  0.059897910803556442 1 1;
	setAttr -s 6 ".kiy[3:5]"  -0.99820446968078613 0 0;
	setAttr -s 6 ".kox[3:5]"  0.059897910803556442 1 1;
	setAttr -s 6 ".koy[3:5]"  -0.99820446968078613 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 3 75.307128588716893 5 0 6 42.353728539205591
		 10 0;
	setAttr -s 5 ".kit[3:4]"  1 18;
	setAttr -s 5 ".kot[3:4]"  1 18;
	setAttr -s 5 ".kix[3:4]"  0.075863555073738098 1;
	setAttr -s 5 ".kiy[3:4]"  -0.99711823463439941 0;
	setAttr -s 5 ".kox[3:4]"  0.075863555073738098 1;
	setAttr -s 5 ".koy[3:4]"  -0.99711823463439941 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -4.8697690720000013 3 -15.186952517320051
		 5 -26.85116141136735 6 -0.64453945809706825 9 0 10 -4.8697690720000013;
	setAttr -s 6 ".kit[3:5]"  1 18 18;
	setAttr -s 6 ".kot[3:5]"  1 18 18;
	setAttr -s 6 ".kix[3:5]"  0.27944314479827881 1 1;
	setAttr -s 6 ".kiy[3:5]"  0.96016228199005127 0 0;
	setAttr -s 6 ".kox[3:5]"  0.27944314479827881 1 1;
	setAttr -s 6 ".koy[3:5]"  0.96016222238540649 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 80.573187008030274 4 47.741977707655231
		 5 0 7 -2.85331884971683 9 0 10 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 3 0 4 0 5 0 7 0 10 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 3 0 4 0 5 -26.85116141136735 7 0 10 0;
createNode animCurveTA -n "rigw:rig:FKHip_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKHip_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKHip_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKHip_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKHip_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKHip_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_FKIKBlend";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 0 4 0 5 0 6 10 7 10 10 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 7 0 10 0;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 4 1 7 1 10 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKArm_R_FKIKBlend";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 5 0 6 10 7 10 10 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_R_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 5 0 7 0 10 0;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKArm_R_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 5 1 7 1 10 1;
	setAttr -s 4 ".kot[0:3]"  5 5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKIKBlend";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 10 7 10 10 10;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 7 1 10 1;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKIKBlend";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 10 7 10 10 10;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 7 1 10 1;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891690000001 6 -38.138925270989162
		 7 -38.138925270000001 10 12.078891690000001;
	setAttr -s 4 ".kit[1:3]"  1 18 18;
	setAttr -s 4 ".kot[1:3]"  1 18 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 6 0 7 0;
	setAttr -s 3 ".kit[1:2]"  1 18;
	setAttr -s 3 ".kot[1:2]"  1 18;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 3 0 6 0 7 0 10 0;
	setAttr -s 5 ".kit[2:4]"  1 18 18;
	setAttr -s 5 ".kot[2:4]"  1 18 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 -5.5977795570009485 4 -20.462255640324347
		 5 -32.91627295358073 6 -37.462824815433734 7 -37.462824820000002 10 0;
createNode animCurveTA -n "rigw:rig:FKKnee_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKKnee_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKKnee_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKKnee_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKKnee_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKKnee_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891690000001 6 -38.138925270989162
		 7 -38.138925270000001 10 12.078891690000001;
	setAttr -s 4 ".kit[1:3]"  1 18 18;
	setAttr -s 4 ".kot[1:3]"  1 18 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 6 0 7 0;
	setAttr -s 3 ".kit[1:2]"  1 18;
	setAttr -s 3 ".kot[1:2]"  1 18;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 3 0 6 0 7 0 10 0;
	setAttr -s 5 ".kit[2:4]"  1 18 18;
	setAttr -s 5 ".kot[2:4]"  1 18 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 -5.5977795570009485 4 -20.462255640324347
		 5 -32.91627295358073 6 -37.462824815433734 7 -37.462824820000002 10 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 32.462647968851961 4 83.316474110144398
		 5 56.228447212953917 6 73.656861910276618 9 -8.0549708992628268 10 0;
	setAttr -s 7 ".kit[4:6]"  1 18 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[4:6]"  1 1 1;
	setAttr -s 7 ".kiy[4:6]"  0 0 0;
	setAttr -s 7 ".kox[4:6]"  1 1 1;
	setAttr -s 7 ".koy[4:6]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 -17.355295683380429 4 23.132024443419109
		 5 -8.5968064803251316 6 83.818533388961626 9 4.9033729246843683 10 0;
	setAttr -s 7 ".kit[4:6]"  1 18 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[4:6]"  1 0.12875239551067352 1;
	setAttr -s 7 ".kiy[4:6]"  0 -0.99167680740356445 0;
	setAttr -s 7 ".kox[4:6]"  1 0.12875239551067352 1;
	setAttr -s 7 ".koy[4:6]"  0 -0.99167680740356445 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.704419213 3 -63.081776680260582 4 -18.839539067420553
		 5 -28.158101832905114 6 -86.183328813984147 9 -23.701271903701169 10 -1.704419213;
	setAttr -s 7 ".kit[4:6]"  1 18 18;
	setAttr -s 7 ".kot[4:6]"  1 18 18;
	setAttr -s 7 ".kix[4:6]"  1 0.090062618255615234 1;
	setAttr -s 7 ".kiy[4:6]"  0 0.99593609571456909 0;
	setAttr -s 7 ".kox[4:6]"  1 0.090062618255615234 1;
	setAttr -s 7 ".koy[4:6]"  0 0.99593609571456909 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 23.021078710537953 4 83.316474110144398
		 5 56.228447212953917 7 2.9723606094426755 9 2.2162000850000001 10 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 1.7303319676592577 4 23.132024443419109
		 5 -8.5968064803251316 7 2.9809394690883364 9 4.1976308910000002 10 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 -64.386088056852643 4 -58.298820694388475
		 5 -28.158101832905114 7 -10.749478937245753 9 -5.0289727483033291 10 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891690000001 6 -42.403674660453085
		 7 -42.40367466 10 12.078891690000001;
	setAttr -s 4 ".kit[1:3]"  1 18 18;
	setAttr -s 4 ".kot[1:3]"  1 18 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 3 0 6 0 7 0 10 0;
	setAttr -s 5 ".kit[2:4]"  1 18 18;
	setAttr -s 5 ".kot[2:4]"  1 18 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 3 0 6 0 7 0 10 0;
	setAttr -s 5 ".kit[2:4]"  1 18 18;
	setAttr -s 5 ".kot[2:4]"  1 18 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 -7.0989713146387725 4 -23.225813409417839
		 5 -36.737488461447363 6 -39.373432569367075 7 -39.373432569999999 10 0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 1.8459432932393731 3 -4.8740847770938318
		 4 -10.245583047788836 5 0 9 0 10 0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 15.184933017604862 3 53.502187171990002
		 4 1.1248222805817556 5 52.32616276942727 9 0 10 0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 2 -27.458702388245602 3 -12.246564672571765
		 4 -10.311645197793188 5 0 9 0 10 0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 3 32.128358090427753 4 21.978606697114039
		 5 0 6 -39.567332800794595 7 -29.114673133960487 9 0.75007907030000021 10 0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 3 80.119131259131279 4 71.262294382853909
		 5 52.32616276942727 6 43.541083664751497 7 35.380994222233653 9 3.378604165 10 0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 3 -47.327229500233713 4 -33.856975598750367
		 5 0 6 46.853407643713581 7 32.917856633603577 9 -6.8980171260000009 10 0;
createNode animCurveTA -n "rigw:rig:IKArm_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 6 51.892925371841919 7 51.892925371841919;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:IKArm_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 6 40.925763135812765 7 40.925763135812765;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:IKArm_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 6 -43.14635896683626 7 -43.14635896683626;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTL -n "rigw:rig:IKArm_L_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 6 -0.95148009570000003 7 -0.95148009570000003
		 10 -0.95148009570000003;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTL -n "rigw:rig:IKArm_L_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -0.611751 6 -3.2026594968022195 7 -3.2026594968022195
		 10 -3.202659497;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTL -n "rigw:rig:IKArm_L_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 6 1.0638038011763504 7 1.0638038011763504
		 10 1.0638038009999999;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:IKArm_L_follow";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 6 0 7 0;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:IKArm_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 6 76.045027653008418 7 76.045027653008418
		 8 59.265472428273824;
	setAttr -s 4 ".kit[2:3]"  1 18;
	setAttr -s 4 ".kot[2:3]"  1 18;
	setAttr -s 4 ".kix[2:3]"  1 1;
	setAttr -s 4 ".kiy[2:3]"  0 0;
	setAttr -s 4 ".kox[2:3]"  1 1;
	setAttr -s 4 ".koy[2:3]"  0 0;
createNode animCurveTA -n "rigw:rig:IKArm_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 6 -52.866200175910592 7 -52.866200175910592
		 8 -12.553974093369929;
	setAttr -s 4 ".kit[2:3]"  1 18;
	setAttr -s 4 ".kot[2:3]"  1 18;
	setAttr -s 4 ".kix[2:3]"  1 1;
	setAttr -s 4 ".kiy[2:3]"  0 0;
	setAttr -s 4 ".kox[2:3]"  1 1;
	setAttr -s 4 ".koy[2:3]"  0 0;
createNode animCurveTA -n "rigw:rig:IKArm_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 6 74.666198775688144 7 74.666198775688144
		 8 56.271455584972429;
	setAttr -s 4 ".kit[2:3]"  1 18;
	setAttr -s 4 ".kot[2:3]"  1 18;
	setAttr -s 4 ".kix[2:3]"  1 1;
	setAttr -s 4 ".kiy[2:3]"  0 0;
	setAttr -s 4 ".kox[2:3]"  1 1;
	setAttr -s 4 ".koy[2:3]"  0 0;
createNode animCurveTL -n "rigw:rig:IKArm_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 6 1.2363067575784503 7 1.2363067575784503
		 8 1.340946753424697 10 1.236306758;
	setAttr -s 5 ".kit[2:4]"  1 18 18;
	setAttr -s 5 ".kot[2:4]"  1 18 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTL -n "rigw:rig:IKArm_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 -0.611751 6 -3.1720805244981447 7 -3.1720805244981447
		 10 -3.1720805240000001;
	setAttr -s 4 ".kit[2:3]"  1 18;
	setAttr -s 4 ".kot[2:3]"  1 18;
	setAttr -s 4 ".kix[2:3]"  1 1;
	setAttr -s 4 ".kiy[2:3]"  0 0;
	setAttr -s 4 ".kox[2:3]"  1 1;
	setAttr -s 4 ".koy[2:3]"  0 0;
createNode animCurveTL -n "rigw:rig:IKArm_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 6 1.447874246491953 7 1.447874246491953
		 10 1.447874246;
	setAttr -s 4 ".kit[2:3]"  1 18;
	setAttr -s 4 ".kot[2:3]"  1 18;
	setAttr -s 4 ".kix[2:3]"  1 1;
	setAttr -s 4 ".kiy[2:3]"  0 0;
	setAttr -s 4 ".kox[2:3]"  1 1;
	setAttr -s 4 ".koy[2:3]"  0 0;
createNode animCurveTU -n "rigw:rig:IKArm_R_follow";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLegBall_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTA -n "rigw:rig:IKLegBall_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 4 0 6 0 7 0 9 0 10 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 0 4 0 6 0 7 0 9 0 10 0;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 7.576037 3 7.576037 4 7.576037 6 7.576037
		 7 7.576037 9 7.576037 10 7.576037;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 0 4 0 6 0 7 0 9 0 10 0;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0.237274 3 0.237274 4 0.237274 6 0.237274
		 7 0.237274 9 0.237274 10 0.237274;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 0 4 0 6 0 7 0 9 0 10 0;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 -1.38742 3 -1.38742 4 -1.38742 6 -1.38742
		 7 -1.38742 9 -1.38742 10 -1.38742;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 0 4 0 6 0 7 0 9 0 10 0;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_toe";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  4 0 6 0 7 0 9 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 1;
	setAttr -s 4 ".kot[0:3]"  18 1 1 1;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_rollAngle";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 25 3 25 4 25 6 25 7 25 9 25 10 25;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_roll";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 0 4 0 6 0 7 0 9 0 10 0;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 0 4 0 6 0 7 0 9 0 10 0;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 0 4 0 6 0 7 0 9 0 10 0;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 3 1 4 1 6 1 7 1 9 1 10 1;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 1 3 1 4 1 6 1 7 1 9 1 10 1;
	setAttr -s 7 ".kit[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kot[0:6]"  18 1 1 1 1 1 18;
	setAttr -s 7 ".kix[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".kiy[1:6]"  0 0 0 0 0 0;
	setAttr -s 7 ".kox[1:6]"  1 1 1 1 1 1;
	setAttr -s 7 ".koy[1:6]"  0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 7 0 8 0 10 0;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -6.531151 5 -6.531151 6 -6.531151 7 -6.531151
		 8 -6.531151 10 -6.531151;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 7 0 8 0 10 0;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -0.350318 5 -0.350318 6 -0.350318 7 -0.350318
		 8 -0.350318 10 -0.350318;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 7 0 8 0 10 0;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -0.040106700000000002 5 -0.040106700000000002
		 6 -0.040106700000000002 7 -0.040106700000000002 8 -0.040106700000000002 10 -0.040106700000000002;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 7 0 8 0 10 0;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_toe";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 5 0 6 0 7 0 8 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_rollAngle";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 25 5 25 6 25 7 25 8 25 10 25;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_roll";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 7 0 8 0 10 0;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 7 0 8 0 10 0;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 5 0 6 0 7 0 8 0 10 0;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 6 1 7 1 8 1 10 1;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 1 5 1 6 1 7 1 8 1 10 1;
	setAttr -s 6 ".kit[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kot[0:5]"  18 1 1 1 1 18;
	setAttr -s 6 ".kix[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".kiy[1:5]"  0 0 0 0 0;
	setAttr -s 6 ".kox[1:5]"  1 1 1 1 1;
	setAttr -s 6 ".koy[1:5]"  0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:Main_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTL -n "rigw:rig:Main_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTL -n "rigw:rig:Main_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTU -n "rigw:rig:Main_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 1 7 1 10 1;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
createNode animCurveTA -n "rigw:rig:Main_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTA -n "rigw:rig:Main_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTA -n "rigw:rig:Main_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTU -n "rigw:rig:Main_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.2 7 2.2 10 2.2;
createNode animCurveTU -n "rigw:rig:Main_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.2 7 2.2 10 2.2;
createNode animCurveTU -n "rigw:rig:Main_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.2 7 2.2 10 2.2;
createNode animCurveTL -n "rigw:rig:PoleArm_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTL -n "rigw:rig:PoleArm_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTL -n "rigw:rig:PoleArm_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:rig:PoleArm_L_follow";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTL -n "rigw:rig:PoleArm_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:PoleArm_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:PoleArm_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:PoleArm_R_follow";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_L_follow";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 10 7 10 10 10;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 10 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_R_follow";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 10 7 10 10 10;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 7 0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 -10.440261419970396 6 16.26859489531061
		 7 19.879337911605745 10 0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 6 -6.5194100583460486 7 -6.5194100580000001
		 10 0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 6 -6.2452318981677069 7 -6.2452318980000001
		 10 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 3 5.653602219075645 4 10.089247113039386
		 5 12.741617487034054 7 2.9657312650000001 10 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 3 5.653602219075645 4 10.089247113039386
		 5 12.741617487034054 7 8.3297050850000005 10 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 3 8.7561058544018717 4 26.421262005058868
		 5 23.551550456203827 7 6.8744604700000007 10 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 3 8.7561058544018717 4 26.421262005058868
		 5 23.551550456203827 7 15.576610860000002 10 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891690000001 6 -38.138925270989162
		 7 -38.138925270000001 10 12.078891690000001;
	setAttr -s 4 ".kit[1:3]"  1 18 18;
	setAttr -s 4 ".kot[1:3]"  1 18 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891690000001 6 -38.138925270989162
		 7 -38.138925270000001 10 12.078891690000001;
	setAttr -s 4 ".kit[1:3]"  1 18 18;
	setAttr -s 4 ".kot[1:3]"  1 18 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 12.078891690000001 6 -38.138925270989162
		 7 -38.138925270000001 10 12.078891690000001;
	setAttr -s 4 ".kit[1:3]"  1 18 18;
	setAttr -s 4 ".kot[1:3]"  1 18 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 3 -5.5977795570009485 4 -20.462255640324347
		 5 -32.91627295358073 6 -37.462824815433734 7 -37.462824820000002 8 -53.004617463586271
		 10 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  1 0 3 -5.5977795570009485 4 -20.462255640324347
		 5 -32.91627295358073 6 -37.462824815433734 7 -37.462824820000002 8 -53.004617463586271
		 10 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  1 0 3 -5.5977795570009485 4 -20.462255640324347
		 5 -32.91627295358073 6 -37.462824815433734 7 -37.462824820000002 10 0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_FKIKBlend";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_FKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 1;
	setAttr ".kot[0]"  5;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_IKVis";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  7 0;
	setAttr ".kot[0]"  5;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 11 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 13 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 20 ".tx";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 30 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren" -type "string" "mentalRay";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 32;
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar" 0;
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn" no;
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff" yes;
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultRenderQuality;
	setAttr ".ert" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w" 512;
	setAttr -av ".h" 512;
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al" yes;
	setAttr -av ".dar" 1;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 3 ".sol";
connectAttr "rigw:rig:Main_scaleY.o" "rigwRN.phl[1]";
connectAttr "rigw:rig:Main_scaleX.o" "rigwRN.phl[2]";
connectAttr "rigw:rig:Main_scaleZ.o" "rigwRN.phl[3]";
connectAttr "rigw:rig:Main_translateX.o" "rigwRN.phl[4]";
connectAttr "rigw:rig:Main_translateY.o" "rigwRN.phl[5]";
connectAttr "rigw:rig:Main_translateZ.o" "rigwRN.phl[6]";
connectAttr "rigw:rig:Main_visibility.o" "rigwRN.phl[7]";
connectAttr "rigw:rig:Main_rotateX.o" "rigwRN.phl[8]";
connectAttr "rigw:rig:Main_rotateY.o" "rigwRN.phl[9]";
connectAttr "rigw:rig:Main_rotateZ.o" "rigwRN.phl[10]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateX.o" "rigwRN.phl[11]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateY.o" "rigwRN.phl[12]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateZ.o" "rigwRN.phl[13]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateX.o" "rigwRN.phl[14]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateY.o" "rigwRN.phl[15]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateZ.o" "rigwRN.phl[16]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateX.o" "rigwRN.phl[17]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateY.o" "rigwRN.phl[18]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateZ.o" "rigwRN.phl[19]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateX.o" "rigwRN.phl[20]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateY.o" "rigwRN.phl[21]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateZ.o" "rigwRN.phl[22]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateX.o" "rigwRN.phl[23]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateY.o" "rigwRN.phl[24]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateZ.o" "rigwRN.phl[25]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateX.o" "rigwRN.phl[26]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateY.o" "rigwRN.phl[27]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateZ.o" "rigwRN.phl[28]";
connectAttr "rigw:rig:FKClavicle_R_rotateX.o" "rigwRN.phl[29]";
connectAttr "rigw:rig:FKClavicle_R_rotateY.o" "rigwRN.phl[30]";
connectAttr "rigw:rig:FKClavicle_R_rotateZ.o" "rigwRN.phl[31]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[32]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[33]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[34]";
connectAttr "rigw:rig:FKElbow_R_rotateX.o" "rigwRN.phl[35]";
connectAttr "rigw:rig:FKElbow_R_rotateY.o" "rigwRN.phl[36]";
connectAttr "rigw:rig:FKElbow_R_rotateZ.o" "rigwRN.phl[37]";
connectAttr "rigw:rig:FKWrist_R_rotateX.o" "rigwRN.phl[38]";
connectAttr "rigw:rig:FKWrist_R_rotateY.o" "rigwRN.phl[39]";
connectAttr "rigw:rig:FKWrist_R_rotateZ.o" "rigwRN.phl[40]";
connectAttr "rigw:rig:FKShoulderPad1_R_rotateX.o" "rigwRN.phl[41]";
connectAttr "rigw:rig:FKShoulderPad1_R_rotateY.o" "rigwRN.phl[42]";
connectAttr "rigw:rig:FKShoulderPad1_R_rotateZ.o" "rigwRN.phl[43]";
connectAttr "rigw:rig:FKShoulderPad2_R_rotateX.o" "rigwRN.phl[44]";
connectAttr "rigw:rig:FKShoulderPad2_R_rotateY.o" "rigwRN.phl[45]";
connectAttr "rigw:rig:FKShoulderPad2_R_rotateZ.o" "rigwRN.phl[46]";
connectAttr "rigw:rig:FKClavicle_L_rotateX.o" "rigwRN.phl[47]";
connectAttr "rigw:rig:FKClavicle_L_rotateY.o" "rigwRN.phl[48]";
connectAttr "rigw:rig:FKClavicle_L_rotateZ.o" "rigwRN.phl[49]";
connectAttr "rigw:rig:FKShoulder_L_rotateX.o" "rigwRN.phl[50]";
connectAttr "rigw:rig:FKShoulder_L_rotateY.o" "rigwRN.phl[51]";
connectAttr "rigw:rig:FKShoulder_L_rotateZ.o" "rigwRN.phl[52]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[53]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[54]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[55]";
connectAttr "rigw:rig:FKWrist_L_rotateX.o" "rigwRN.phl[56]";
connectAttr "rigw:rig:FKWrist_L_rotateY.o" "rigwRN.phl[57]";
connectAttr "rigw:rig:FKWrist_L_rotateZ.o" "rigwRN.phl[58]";
connectAttr "rigw:rig:FKShoulderPad1_L_rotateX.o" "rigwRN.phl[59]";
connectAttr "rigw:rig:FKShoulderPad1_L_rotateY.o" "rigwRN.phl[60]";
connectAttr "rigw:rig:FKShoulderPad1_L_rotateZ.o" "rigwRN.phl[61]";
connectAttr "rigw:rig:FKShoulderPad2_L_rotateX.o" "rigwRN.phl[62]";
connectAttr "rigw:rig:FKShoulderPad2_L_rotateY.o" "rigwRN.phl[63]";
connectAttr "rigw:rig:FKShoulderPad2_L_rotateZ.o" "rigwRN.phl[64]";
connectAttr "rigw:rig:FKHipTwist_R_rotateX.o" "rigwRN.phl[65]";
connectAttr "rigw:rig:FKHipTwist_R_rotateY.o" "rigwRN.phl[66]";
connectAttr "rigw:rig:FKHipTwist_R_rotateZ.o" "rigwRN.phl[67]";
connectAttr "rigw:rig:FKHip_R_rotateX.o" "rigwRN.phl[68]";
connectAttr "rigw:rig:FKHip_R_rotateY.o" "rigwRN.phl[69]";
connectAttr "rigw:rig:FKHip_R_rotateZ.o" "rigwRN.phl[70]";
connectAttr "rigw:rig:FKKnee_R_rotateX.o" "rigwRN.phl[71]";
connectAttr "rigw:rig:FKKnee_R_rotateY.o" "rigwRN.phl[72]";
connectAttr "rigw:rig:FKKnee_R_rotateZ.o" "rigwRN.phl[73]";
connectAttr "rigw:rig:FKAnkle_R_rotateX.o" "rigwRN.phl[74]";
connectAttr "rigw:rig:FKAnkle_R_rotateY.o" "rigwRN.phl[75]";
connectAttr "rigw:rig:FKAnkle_R_rotateZ.o" "rigwRN.phl[76]";
connectAttr "rigw:rig:FKHipTwist_L_rotateX.o" "rigwRN.phl[77]";
connectAttr "rigw:rig:FKHipTwist_L_rotateY.o" "rigwRN.phl[78]";
connectAttr "rigw:rig:FKHipTwist_L_rotateZ.o" "rigwRN.phl[79]";
connectAttr "rigw:rig:FKHip_L_rotateX.o" "rigwRN.phl[80]";
connectAttr "rigw:rig:FKHip_L_rotateY.o" "rigwRN.phl[81]";
connectAttr "rigw:rig:FKHip_L_rotateZ.o" "rigwRN.phl[82]";
connectAttr "rigw:rig:FKKnee_L_rotateX.o" "rigwRN.phl[83]";
connectAttr "rigw:rig:FKKnee_L_rotateY.o" "rigwRN.phl[84]";
connectAttr "rigw:rig:FKKnee_L_rotateZ.o" "rigwRN.phl[85]";
connectAttr "rigw:rig:FKAnkle_L_rotateX.o" "rigwRN.phl[86]";
connectAttr "rigw:rig:FKAnkle_L_rotateY.o" "rigwRN.phl[87]";
connectAttr "rigw:rig:FKAnkle_L_rotateZ.o" "rigwRN.phl[88]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateX.o" "rigwRN.phl[89]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateY.o" "rigwRN.phl[90]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateZ.o" "rigwRN.phl[91]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateX.o" "rigwRN.phl[92]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateY.o" "rigwRN.phl[93]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateZ.o" "rigwRN.phl[94]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateX.o" "rigwRN.phl[95]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateY.o" "rigwRN.phl[96]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateZ.o" "rigwRN.phl[97]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateX.o" "rigwRN.phl[98]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateY.o" "rigwRN.phl[99]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateZ.o" "rigwRN.phl[100]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateX.o" "rigwRN.phl[101]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateY.o" "rigwRN.phl[102]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateZ.o" "rigwRN.phl[103]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateX.o" "rigwRN.phl[104]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateY.o" "rigwRN.phl[105]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateZ.o" "rigwRN.phl[106]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[107]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[108]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[109]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[110]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[111]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[112]";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[113]";
connectAttr "rigw:rig:FKSpineA_M_rotateX.o" "rigwRN.phl[114]";
connectAttr "rigw:rig:FKSpineA_M_rotateY.o" "rigwRN.phl[115]";
connectAttr "rigw:rig:FKSpineA_M_rotateZ.o" "rigwRN.phl[116]";
connectAttr "rigw:rig:FKChest_M_rotateX.o" "rigwRN.phl[117]";
connectAttr "rigw:rig:FKChest_M_rotateY.o" "rigwRN.phl[118]";
connectAttr "rigw:rig:FKChest_M_rotateZ.o" "rigwRN.phl[119]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateX.o" "rigwRN.phl[120]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateY.o" "rigwRN.phl[121]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateZ.o" "rigwRN.phl[122]";
connectAttr "rigw:rig:IKArm_R_rotateX.o" "rigwRN.phl[123]";
connectAttr "rigw:rig:IKArm_R_rotateY.o" "rigwRN.phl[124]";
connectAttr "rigw:rig:IKArm_R_rotateZ.o" "rigwRN.phl[125]";
connectAttr "rigw:rig:IKArm_R_translateX.o" "rigwRN.phl[126]";
connectAttr "rigw:rig:IKArm_R_translateY.o" "rigwRN.phl[127]";
connectAttr "rigw:rig:IKArm_R_translateZ.o" "rigwRN.phl[128]";
connectAttr "rigw:rig:IKArm_R_follow.o" "rigwRN.phl[129]";
connectAttr "rigw:rig:PoleArm_R_translateX.o" "rigwRN.phl[130]";
connectAttr "rigw:rig:PoleArm_R_translateY.o" "rigwRN.phl[131]";
connectAttr "rigw:rig:PoleArm_R_translateZ.o" "rigwRN.phl[132]";
connectAttr "rigw:rig:PoleArm_R_follow.o" "rigwRN.phl[133]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[134]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[135]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[136]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[137]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[138]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[139]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[140]";
connectAttr "rigw:rig:IKLeg_R_rollAngle.o" "rigwRN.phl[141]";
connectAttr "rigw:rig:IKLeg_R_roll.o" "rigwRN.phl[142]";
connectAttr "rigw:rig:IKLeg_R_toe.o" "rigwRN.phl[143]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[144]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[145]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[146]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[147]";
connectAttr "rigw:rig:IKLegHeel_R_rotateX.o" "rigwRN.phl[148]";
connectAttr "rigw:rig:IKLegHeel_R_rotateY.o" "rigwRN.phl[149]";
connectAttr "rigw:rig:IKLegHeel_R_rotateZ.o" "rigwRN.phl[150]";
connectAttr "rigw:rig:IKLegBall_R_rotateX.o" "rigwRN.phl[151]";
connectAttr "rigw:rig:PoleLeg_R_translateX.o" "rigwRN.phl[152]";
connectAttr "rigw:rig:PoleLeg_R_translateY.o" "rigwRN.phl[153]";
connectAttr "rigw:rig:PoleLeg_R_translateZ.o" "rigwRN.phl[154]";
connectAttr "rigw:rig:PoleLeg_R_follow.o" "rigwRN.phl[155]";
connectAttr "rigw:rig:IKArm_L_rotateX.o" "rigwRN.phl[156]";
connectAttr "rigw:rig:IKArm_L_rotateY.o" "rigwRN.phl[157]";
connectAttr "rigw:rig:IKArm_L_rotateZ.o" "rigwRN.phl[158]";
connectAttr "rigw:rig:IKArm_L_translateX.o" "rigwRN.phl[159]";
connectAttr "rigw:rig:IKArm_L_translateY.o" "rigwRN.phl[160]";
connectAttr "rigw:rig:IKArm_L_translateZ.o" "rigwRN.phl[161]";
connectAttr "rigw:rig:IKArm_L_follow.o" "rigwRN.phl[162]";
connectAttr "rigw:rig:PoleArm_L_translateX.o" "rigwRN.phl[163]";
connectAttr "rigw:rig:PoleArm_L_translateY.o" "rigwRN.phl[164]";
connectAttr "rigw:rig:PoleArm_L_translateZ.o" "rigwRN.phl[165]";
connectAttr "rigw:rig:PoleArm_L_follow.o" "rigwRN.phl[166]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[167]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[168]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[169]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[170]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[171]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[172]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[173]";
connectAttr "rigw:rig:IKLeg_L_rollAngle.o" "rigwRN.phl[174]";
connectAttr "rigw:rig:IKLeg_L_roll.o" "rigwRN.phl[175]";
connectAttr "rigw:rig:IKLeg_L_toe.o" "rigwRN.phl[176]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[177]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[178]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[179]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[180]";
connectAttr "rigw:rig:IKLegHeel_L_rotateX.o" "rigwRN.phl[181]";
connectAttr "rigw:rig:IKLegHeel_L_rotateY.o" "rigwRN.phl[182]";
connectAttr "rigw:rig:IKLegHeel_L_rotateZ.o" "rigwRN.phl[183]";
connectAttr "rigw:rig:IKLegBall_L_rotateX.o" "rigwRN.phl[184]";
connectAttr "rigw:rig:PoleLeg_L_translateX.o" "rigwRN.phl[185]";
connectAttr "rigw:rig:PoleLeg_L_translateY.o" "rigwRN.phl[186]";
connectAttr "rigw:rig:PoleLeg_L_translateZ.o" "rigwRN.phl[187]";
connectAttr "rigw:rig:PoleLeg_L_follow.o" "rigwRN.phl[188]";
connectAttr "rigw:rig:FKIKArm_R_FKIKBlend.o" "rigwRN.phl[189]";
connectAttr "rigw:rig:FKIKArm_R_IKVis.o" "rigwRN.phl[190]";
connectAttr "rigw:rig:FKIKArm_R_FKVis.o" "rigwRN.phl[191]";
connectAttr "rigw:rig:FKIKLeg_R_FKIKBlend.o" "rigwRN.phl[192]";
connectAttr "rigw:rig:FKIKLeg_R_IKVis.o" "rigwRN.phl[193]";
connectAttr "rigw:rig:FKIKLeg_R_FKVis.o" "rigwRN.phl[194]";
connectAttr "rigw:rig:FKIKSpine_M_FKIKBlend.o" "rigwRN.phl[195]";
connectAttr "rigw:rig:FKIKSpine_M_IKVis.o" "rigwRN.phl[196]";
connectAttr "rigw:rig:FKIKSpine_M_FKVis.o" "rigwRN.phl[197]";
connectAttr "rigw:rig:FKIKArm_L_FKIKBlend.o" "rigwRN.phl[198]";
connectAttr "rigw:rig:FKIKArm_L_IKVis.o" "rigwRN.phl[199]";
connectAttr "rigw:rig:FKIKArm_L_FKVis.o" "rigwRN.phl[200]";
connectAttr "rigw:rig:FKIKLeg_L_FKIKBlend.o" "rigwRN.phl[201]";
connectAttr "rigw:rig:FKIKLeg_L_IKVis.o" "rigwRN.phl[202]";
connectAttr "rigw:rig:FKIKLeg_L_FKVis.o" "rigwRN.phl[203]";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Juggernaut__mc-rigw@Jug_spec.ma
