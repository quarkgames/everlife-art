//Maya ASCII 2013 scene
//Name: Juggernaut__mc-rigw@Jug_run.ma
//Last modified: Tue, Oct 21, 2014 01:56:50 PM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Juggernaut/Juggernaut__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Juggernaut__rig.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Juggernaut/Juggernaut__mc-rigw.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.4";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -17.890856058474768 62.441102821479348 161.78986759894252 ;
	setAttr ".r" -type "double3" -20.138352729665129 -5.4000000000011399 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 158.05523389363285;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 8.3165179940113312 7.704628717514943 10.820647893104699 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" -2.9961962568028269 9.21380837716492 500.22264033209439 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".ow" 122.17970678167829;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 11 ".lnk";
	setAttr -s 11 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 163 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 0
		"rigw:rigRN" 0
		"rigwRN" 1
		2 "rigw:_mesh" "displayType" " 2"
		"rigw:rigRN" 370
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "visibility" " -av 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translate" " -type \"double3\" 0 0 0"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scale" " -type \"double3\" 2.2 2.2 2.2"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotate" " -type \"double3\" -12.258318 -6.447323 27.330513"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotate" " -type \"double3\" 9.675087 5.111402 27.5907"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotate" " -type \"double3\" 24.174184 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotate" " -type \"double3\" -14.226793 21.112765 -3.41185"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotate" " -type \"double3\" 0 44.120253 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotate" " -type \"double3\" 0 0 -0.50966"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotate" " -type \"double3\" 0 0 -0.50966"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotate" " -type \"double3\" -2.79917 -32.33506 21.685281"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotate" " -type \"double3\" -2.578596 35.659335 5.801334"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 -28.867237"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotate" " -type \"double3\" 0 66.694455 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotate" " -type \"double3\" -0.138916 -4.276543 -2.846375"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotate" " -type \"double3\" -0.142821 -4.357709 -2.866017"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotate" " -type \"double3\" 73.441319 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translate" " -type \"double3\" -0.811144 -0.158455 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 5.989244"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"CenterBtwFeet" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotate" " -type \"double3\" 0 0.707806 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotate" " -type \"double3\" 0 0.429269 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotate" " -type \"double3\" -28.318406 -17.950315 8.278361"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" 0 0 -4.106434"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"toe" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M|rigw:rig:IKXSpineHandle_M" 
		"translate" " -type \"double3\" -2.2464e-08 0.00443195 -0.000534086"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M|rigw:rig:IKXSpineHandle_M" 
		"rotate" " -type \"double3\" 0.190416 -1.07397e-05 -1.69397e-06"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0 1.079148 0.669683"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"swivel" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"roll" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rollAngle" " -av -k 1 25"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"toe" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"stretchy" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"antiPop" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length1" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"Length2" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotate" " -type \"double3\" -8.924884 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L" 
		"rotateX" " -av 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L" 
		"follow" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R" 
		"FKIKBlend" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R" 
		"FKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R" 
		"IKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R" 
		"IKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M" 
		"FKIKBlend" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M" 
		"FKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M" 
		"IKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L" 
		"FKIKBlend" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L" 
		"FKVis" " -av -k 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L" 
		"IKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKIKBlend" " -av -k 1 10"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"FKVis" " -av -k 1 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L" 
		"IKVis" " -av -k 1 1"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleY" "rigwRN.placeHolderList[1]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleX" "rigwRN.placeHolderList[2]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleZ" "rigwRN.placeHolderList[3]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateX" "rigwRN.placeHolderList[4]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateY" "rigwRN.placeHolderList[5]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateZ" "rigwRN.placeHolderList[6]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.visibility" "rigwRN.placeHolderList[7]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateX" "rigwRN.placeHolderList[8]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateY" "rigwRN.placeHolderList[9]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateZ" "rigwRN.placeHolderList[10]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateX" 
		"rigwRN.placeHolderList[11]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateY" 
		"rigwRN.placeHolderList[12]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[13]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateX" 
		"rigwRN.placeHolderList[14]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateY" 
		"rigwRN.placeHolderList[15]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetMiddleFinger1_R|rigw:rig:FKExtraMiddleFinger1_R|rigw:rig:FKMiddleFinger1_R|rigw:rig:FKXMiddleFinger1_R|rigw:rig:FKOffsetMiddleFinger2_R|rigw:rig:FKExtraMiddleFinger2_R|rigw:rig:FKMiddleFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[16]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateX" 
		"rigwRN.placeHolderList[17]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateY" 
		"rigwRN.placeHolderList[18]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[19]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateX" 
		"rigwRN.placeHolderList[20]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateY" 
		"rigwRN.placeHolderList[21]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetIndexFinger1_R|rigw:rig:FKExtraIndexFinger1_R|rigw:rig:FKIndexFinger1_R|rigw:rig:FKXIndexFinger1_R|rigw:rig:FKOffsetIndexFinger2_R|rigw:rig:FKExtraIndexFinger2_R|rigw:rig:FKIndexFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[22]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateX" 
		"rigwRN.placeHolderList[23]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateY" 
		"rigwRN.placeHolderList[24]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R.rotateZ" 
		"rigwRN.placeHolderList[25]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateX" 
		"rigwRN.placeHolderList[26]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateY" 
		"rigwRN.placeHolderList[27]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_R|rigw:rig:FKOffsetThumbFinger1_R|rigw:rig:FKExtraThumbFinger1_R|rigw:rig:FKThumbFinger1_R|rigw:rig:FKXThumbFinger1_R|rigw:rig:FKOffsetThumbFinger2_R|rigw:rig:FKExtraThumbFinger2_R|rigw:rig:FKThumbFinger2_R.rotateZ" 
		"rigwRN.placeHolderList[28]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateX" 
		"rigwRN.placeHolderList[29]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateY" 
		"rigwRN.placeHolderList[30]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateZ" 
		"rigwRN.placeHolderList[31]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[32]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[33]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[34]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateX" 
		"rigwRN.placeHolderList[35]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateY" 
		"rigwRN.placeHolderList[36]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateZ" 
		"rigwRN.placeHolderList[37]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateX" 
		"rigwRN.placeHolderList[38]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateY" 
		"rigwRN.placeHolderList[39]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateZ" 
		"rigwRN.placeHolderList[40]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R.rotateX" 
		"rigwRN.placeHolderList[41]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R.rotateY" 
		"rigwRN.placeHolderList[42]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_R|rigw:rig:FKExtraShoulderPad1_R|rigw:rig:FKShoulderPad1_R.rotateZ" 
		"rigwRN.placeHolderList[43]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R.rotateX" 
		"rigwRN.placeHolderList[44]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R.rotateY" 
		"rigwRN.placeHolderList[45]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_R|rigw:rig:FKExtraShoulderPad2_R|rigw:rig:FKShoulderPad2_R.rotateZ" 
		"rigwRN.placeHolderList[46]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L.rotateX" 
		"rigwRN.placeHolderList[47]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L.rotateY" 
		"rigwRN.placeHolderList[48]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L.rotateZ" 
		"rigwRN.placeHolderList[49]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateX" 
		"rigwRN.placeHolderList[50]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateY" 
		"rigwRN.placeHolderList[51]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateZ" 
		"rigwRN.placeHolderList[52]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[53]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[54]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[55]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateX" 
		"rigwRN.placeHolderList[56]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateY" 
		"rigwRN.placeHolderList[57]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateZ" 
		"rigwRN.placeHolderList[58]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L.rotateX" 
		"rigwRN.placeHolderList[59]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L.rotateY" 
		"rigwRN.placeHolderList[60]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad1_L|rigw:rig:FKExtraShoulderPad1_L|rigw:rig:FKShoulderPad1_L.rotateZ" 
		"rigwRN.placeHolderList[61]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L.rotateX" 
		"rigwRN.placeHolderList[62]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L.rotateY" 
		"rigwRN.placeHolderList[63]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetShoulderPad2_L|rigw:rig:FKExtraShoulderPad2_L|rigw:rig:FKShoulderPad2_L.rotateZ" 
		"rigwRN.placeHolderList[64]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateX" 
		"rigwRN.placeHolderList[65]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateY" 
		"rigwRN.placeHolderList[66]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateZ" 
		"rigwRN.placeHolderList[67]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateX" 
		"rigwRN.placeHolderList[68]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateY" 
		"rigwRN.placeHolderList[69]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateZ" 
		"rigwRN.placeHolderList[70]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateX" 
		"rigwRN.placeHolderList[71]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateY" 
		"rigwRN.placeHolderList[72]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[73]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateX" 
		"rigwRN.placeHolderList[74]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateY" 
		"rigwRN.placeHolderList[75]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[76]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateX" 
		"rigwRN.placeHolderList[77]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateY" 
		"rigwRN.placeHolderList[78]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[79]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateX" 
		"rigwRN.placeHolderList[80]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateY" 
		"rigwRN.placeHolderList[81]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetIndexFinger1_L|rigw:rig:FKExtraIndexFinger1_L|rigw:rig:FKIndexFinger1_L|rigw:rig:FKXIndexFinger1_L|rigw:rig:FKOffsetIndexFinger2_L|rigw:rig:FKExtraIndexFinger2_L|rigw:rig:FKIndexFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[82]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateX" 
		"rigwRN.placeHolderList[83]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateY" 
		"rigwRN.placeHolderList[84]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[85]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateX" 
		"rigwRN.placeHolderList[86]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateY" 
		"rigwRN.placeHolderList[87]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[88]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[89]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[90]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[91]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[92]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[93]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[94]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[95]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateX" 
		"rigwRN.placeHolderList[96]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateY" 
		"rigwRN.placeHolderList[97]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateZ" 
		"rigwRN.placeHolderList[98]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateX" 
		"rigwRN.placeHolderList[99]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateY" 
		"rigwRN.placeHolderList[100]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateZ" 
		"rigwRN.placeHolderList[101]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateX" 
		"rigwRN.placeHolderList[102]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateY" 
		"rigwRN.placeHolderList[103]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateZ" 
		"rigwRN.placeHolderList[104]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[105]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[106]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[107]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[108]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[109]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[110]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[111]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rollAngle" 
		"rigwRN.placeHolderList[112]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.roll" 
		"rigwRN.placeHolderList[113]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.toe" 
		"rigwRN.placeHolderList[114]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[115]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[116]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[117]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[118]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateX" 
		"rigwRN.placeHolderList[119]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateY" 
		"rigwRN.placeHolderList[120]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateZ" 
		"rigwRN.placeHolderList[121]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R.rotateX" 
		"rigwRN.placeHolderList[122]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateX" 
		"rigwRN.placeHolderList[123]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateY" 
		"rigwRN.placeHolderList[124]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateZ" 
		"rigwRN.placeHolderList[125]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.follow" 
		"rigwRN.placeHolderList[126]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[127]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[128]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[129]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[130]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[131]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[132]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[133]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rollAngle" 
		"rigwRN.placeHolderList[134]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.roll" 
		"rigwRN.placeHolderList[135]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.toe" 
		"rigwRN.placeHolderList[136]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[137]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[138]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[139]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[140]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateX" 
		"rigwRN.placeHolderList[141]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateY" 
		"rigwRN.placeHolderList[142]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateZ" 
		"rigwRN.placeHolderList[143]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L.rotateX" 
		"rigwRN.placeHolderList[144]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateX" 
		"rigwRN.placeHolderList[145]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateY" 
		"rigwRN.placeHolderList[146]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateZ" 
		"rigwRN.placeHolderList[147]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.follow" 
		"rigwRN.placeHolderList[148]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R.FKIKBlend" 
		"rigwRN.placeHolderList[149]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R.IKVis" 
		"rigwRN.placeHolderList[150]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_R|rigw:rig:FKIKArm_R.FKVis" 
		"rigwRN.placeHolderList[151]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKIKBlend" 
		"rigwRN.placeHolderList[152]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.IKVis" 
		"rigwRN.placeHolderList[153]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKVis" 
		"rigwRN.placeHolderList[154]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.FKIKBlend" 
		"rigwRN.placeHolderList[155]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.IKVis" 
		"rigwRN.placeHolderList[156]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.FKVis" 
		"rigwRN.placeHolderList[157]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.FKIKBlend" 
		"rigwRN.placeHolderList[158]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.IKVis" 
		"rigwRN.placeHolderList[159]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.FKVis" 
		"rigwRN.placeHolderList[160]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKIKBlend" 
		"rigwRN.placeHolderList[161]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.IKVis" 
		"rigwRN.placeHolderList[162]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKVis" 
		"rigwRN.placeHolderList[163]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr ".cm" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".cs" 3;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 17 -ast 1 -aet 120 ";
	setAttr ".st" 6;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -12.25831785244085 2 -12.25831785244085
		 3 -12.25831785244085 4 -12.25831785244085 5 -12.25831785244085 6 -12.25831785244085
		 7 -12.25831785244085 9 -12.25831785244085 10 -12.25831785244085 11 -12.25831785244085
		 12 -12.25831785244085 13 -12.25831785244085 14 -12.25831785244085 15 -12.25831785244085
		 16 -12.25831785244085 17 -12.25831785244085 18 -12.25831785244085;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -6.4473230018286305 2 -6.4473230018286305
		 3 -6.4473230018286305 4 -6.4473230018286305 5 -6.4473230018286305 6 -6.4473230018286305
		 7 -6.4473230018286305 9 -6.4473230018286305 10 -6.4473230018286305 11 -6.4473230018286305
		 12 -6.4473230018286305 13 -6.4473230018286305 14 -6.4473230018286305 15 -6.4473230018286305
		 16 -6.4473230018286305 17 -6.4473230018286305 18 -6.4473230018286305;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 27.330512758547886 2 27.330512758547886
		 3 27.330512758547886 4 27.330512758547886 5 27.330512758547886 6 27.330512758547886
		 7 27.330512758547886 9 27.330512758547886 10 27.330512758547886 11 27.330512758547886
		 12 27.330512758547886 13 27.330512758547886 14 27.330512758547886 15 27.330512758547886
		 16 27.330512758547886 17 27.330512758547886 18 27.330512758547886;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 9.6750867002038188 2 9.6750867002038188
		 3 9.6750867002038188 4 9.6750867002038188 5 9.6750867002038188 6 9.6750867002038188
		 7 9.6750867002038188 9 9.6750867002038188 10 9.6750867002038188 11 9.6750867002038188
		 12 9.6750867002038188 13 9.6750867002038188 14 9.6750867002038188 15 9.6750867002038188
		 16 9.6750867002038188 17 9.6750867002038188 18 9.6750867002038188;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 5.1114018178811511 2 5.1114018178811511
		 3 5.1114018178811511 4 5.1114018178811511 5 5.1114018178811511 6 5.1114018178811511
		 7 5.1114018178811511 9 5.1114018178811511 10 5.1114018178811511 11 5.1114018178811511
		 12 5.1114018178811511 13 5.1114018178811511 14 5.1114018178811511 15 5.1114018178811511
		 16 5.1114018178811511 17 5.1114018178811511 18 5.1114018178811511;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 27.590700122758125 2 27.590700122758125
		 3 27.590700122758125 4 27.590700122758125 5 27.590700122758125 6 27.590700122758125
		 7 27.590700122758125 9 27.590700122758125 10 27.590700122758125 11 27.590700122758125
		 12 27.590700122758125 13 27.590700122758125 14 27.590700122758125 15 27.590700122758125
		 16 27.590700122758125 17 27.590700122758125 18 27.590700122758125;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 24.174184491936419 2 24.174184491936419
		 3 24.174184491936419 4 24.174184491936419 5 24.174184491936419 6 24.174184491936419
		 7 24.174184491936419 9 24.174184491936419 10 24.174184491936419 11 24.174184491936419
		 12 24.174184491936419 13 24.174184491936419 14 24.174184491936419 15 24.174184491936419
		 16 24.174184491936419 17 24.174184491936419 18 24.174184491936419;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -1.124765700080445 2 -2.242250784519888
		 3 -4.5570806453638912 4 -7.3258366686830829 5 -8.760821279581144 6 -10.942202121082117
		 7 -13.123582962583084 9 -14.250726030661028 10 -14.226792741318391 11 -13.293258331554062
		 12 -11.490116985186589 13 -9.1906329319640587 14 -7.6665504794702741 15 -5.567059197920063
		 16 -4.5209241887694125 17 -2.5080665081555011 18 -1.124765700080445;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.961556077003479 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.97483682632446289 1 0.30000007152557373 
		0.066666662693023682 0.99624389410018921 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  -0.27460870146751404 -0.019890971481800079 
		-0.01010033767670393 -0.017974460497498512 -0.22291968762874603 0 -0.063184723258018494 
		-0.0047313682734966278 0.086591236293315887 0.005165348295122385 0.0072916923090815544 
		0.0086304787546396255 0.027167269960045815 0.0091817397624254227 0.018111493438482285 
		0.0084257172420620918 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -6.1991309677382898 2 -3.8696718496414961
		 3 0.95571826715127406 4 6.7273426557397515 5 9.7186465916118703 6 14.265854876548172
		 7 18.813063161484475 9 21.162655841464641 10 21.112764609792691 11 19.166760565483905
		 12 15.408012382298081 13 10.614611426087754 14 7.437577837605744 15 3.0610725435246797
		 16 0.88034692426522687 17 -3.315564692574835 18 -6.1991309677382898;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.8592606782913208 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.90268790721893311 0.38723796606063843 
		0.30000007152557373 0.066666662693023682 0.98397928476333618 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 0.55219227075576782;
	setAttr -s 17 ".kiy[0:16]"  0.51153808832168579 0.04146382212638855 
		0.021054733544588089 0.037468757480382919 0.43029579520225525 0.92197978496551514 
		0.13171203434467316 0.0098627209663391113 -0.17828281223773956 -0.010767469182610512 
		-0.01519996952265501 -0.017990708351135254 -0.056631691753864288 -0.019139857962727547 
		-0.037754420191049576 -0.017563877627253532 -0.83371680974960327;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -18.284554373520095 2 -17.016046423733606
		 3 -14.388378598618559 4 -11.245437323334682 5 -9.6165218116951809 6 -7.1403376183672531
		 7 -4.6641534250393253 9 -3.3846817710997357 10 -3.4118499535920126 11 -4.4715472639254372
		 12 -6.5183751319807541 13 -9.1286231425077062 14 -10.858677943839618 15 -13.241905564685098
		 16 -14.429421031630278 17 -16.714306712667362 18 -18.284554373520095;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.95126211643218994 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.96792203187942505 1 0.30000007152557373 
		0.066666662693023682 0.99516785144805908 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0.30838364362716675 0.022579126060009003 
		0.011465400457382202 0.020403631031513214 0.25125095248222351 0 0.071723833680152893 
		0.005370760802179575 -0.098187908530235291 -0.0058634206652641296 -0.008277134969830513 
		-0.0097968671470880508 -0.030838813632726669 -0.010422622784972191 -0.020559182390570641 
		-0.0095644276589155197 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 0.066666662693023682 
		1 0.033333420753479004 0.033333420753479004 0.033333420753479004 0.099999904632568359 
		0.033333420753479004 0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 0.066666662693023682 
		1 0.033333420753479004 0.033333420753479004 0.033333420753479004 0.099999904632568359 
		0.033333420753479004 0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -46.508691687438855 2 -17.461826650532917
		 3 -1.9433241461158144 4 0 5 0 6 0 7 0 9 0 10 0 11 -2.3274538469762849 12 -10.291255094174776
		 13 -21.293260714370497 14 -28.203813770299259 15 -36.541401435193237 16 -39.936827932673651
		 17 -44.29712428245476 18 -46.508691687438855;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  0.31210625171661377 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 0.066666662693023682 
		1 0.033333420753479004 0.033333420753479004 0.033333420753479004 0.099999904632568359 
		0.033333420753479004 0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[1:16]"  0.95004719495773315 0.033903781324625015 
		0 0 0 0 0 0 -0.019282449036836624 -0.034708540886640549 -0.040493175387382507 -0.11800882965326309 
		-0.032394576817750931 -0.053219575434923172 -0.010412552393972874 0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 70.267037969211231 2 66.793092561212561
		 3 59.27875130611082 4 50.770905810194179 5 47.271141767284824 6 45.585792140605811
		 7 43.900442513926798 9 44.012704141509424 10 44.120252795879146 11 50.418473744667274
		 12 66.62569623201739 13 76.155247132713313 14 76.644063062144937 15 74.64709265002567
		 16 73.16557999591673 17 70.685711003251484 18 70.267037969211231;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.75816851854324341 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.90974730253219604 1 0.30000007152557373 
		0.066666662693023682 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  -0.65205866098403931 -0.063377924263477325 
		-0.032787472009658813 -0.049784168601036072 -0.41516244411468506 0 -0.0044973879121243954 
		0.0019165227422490716 0 0.048495132476091385 0.054547615349292755 0.010010209865868092 
		-0.010121747851371765 -0.012366613373160362 -0.026137979701161385 -0.006885698065161705 
		0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 -2.8842710734356167 4 -7.4075131454630956
		 5 0 6 0 7 0 9 0 10 0 11 0 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		1 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 -0.050270061939954758 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 1.1175102458264663 4 0.65093717060958101
		 5 0 6 0 7 0 9 0 10 0 11 0 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		1 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0.0083546973764896393 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -11.258712132780785 2 -10.81032315131406
		 3 -9.6145000507925165 4 -5.0361426540471985 5 -4.994005127079455 6 -3.2043754311802148
		 7 -1.4147457352809747 9 -0.49002436825270512 10 -0.50965984779907947 11 -1.2755422969263672
		 12 -2.754860286192411 13 -4.6413832029512783 14 -5.8917577589190495 15 -7.6142042684915294
		 16 -8.4724649753470835 17 -10.123836503640511 18 -11.258712132780785;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.97363263368606567 0.066666662693023682 
		0.033333331346511841 1 0.9828532338142395 1 0.30000007152557373 0.066666662693023682 
		0.99746721982955933 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0.22812163829803467 0.0040161586366593838 
		0.0041446946561336517 0 0.18438954651355743 0 0.051837470382452011 0.0038816526066511869 
		-0.071128033101558685 -0.00423771096393466 -0.0059821959584951401 -0.0070805554278194904 
		-0.02228834480047226 -0.0075327991507947445 -0.014858875423669815 -0.0069125620648264885 
		0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 -6.7995931926337576 4 0 5 0 6 0
		 7 0 9 0 10 0 11 0 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0.034556087106466293 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 18 ".ktv[0:17]"  1 0 2 0 3 2.8858469067105017 4 0 5 0 6 0
		 7 0 8 -0.02226993712339553 9 0 10 0 11 0 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 18 ".kit[0:17]"  1 1 1 1 1 10 1 10 
		1 1 1 1 1 1 1 1 1 10;
	setAttr -s 18 ".kix[0:17]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 1 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 18 ".kiy[0:17]"  0 0 -0.0057475920766592026 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 18 ".ktv[0:17]"  1 -11.258712132780785 2 -10.341914634064295
		 3 -8.9954062875705372 4 -5.1659899859923311 5 -4.994005127079455 6 -3.2043754311802148
		 7 -1.4147457352809747 8 -1.4149209676863881 9 -0.49002436825270512 10 -0.50965984779907947
		 11 -1.2755422969263672 12 -2.754860286192411 13 -4.6413832029512783 14 -5.8917577589190495
		 15 -7.6142042684915294 16 -8.4724649753470835 17 -10.123836503640511 18 -11.258712132780785;
	setAttr -s 18 ".kit[0:17]"  1 1 1 1 1 10 1 10 
		1 1 1 1 1 1 1 1 1 10;
	setAttr -s 18 ".kix[0:17]"  0.97363263368606567 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.9828532338142395 1 0.30000007152557373 
		1 0.066666662693023682 0.99746721982955933 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 18 ".kiy[0:17]"  0.22812163829803467 0.016318786889314651 
		0.021961778402328491 -0.0040022744797170162 0.18438959121704102 0 0.051837470382452011 
		0 0.0038816526066511869 -0.071128033101558685 -0.00423771096393466 -0.0059821959584951401 
		-0.0070805554278194904 -0.02228834480047226 -0.0075327991507947445 -0.014858875423669815 
		-0.0069125620648264885 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -5.4238778735191744 2 -5.3584407030705412
		 3 -4.9589296567096097 4 -4.3820493007496299 5 -4.2425647438412062 6 -3.8144091530628832
		 7 -3.3862535622845611 9 -2.9323734081031465 10 -2.799169636084585 11 -2.9267110111993038
		 12 -3.3661069393650207 13 -3.980483003661214 14 -4.3724397108701636 15 -4.8569415577291597
		 16 -5.06186304469409 17 -5.3503863548144155 18 -5.4238778735191744;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0.0021639731712639332 0.0021639757324010134 
		0.0029754566494375467 0 0 0.02092362754046917 0.0028783853631466627 0 -0.0010587198194116354 
		-0.0019240137189626694 -0.0022803181782364845 -0.0067493198439478874 -0.0019240163965150714 
		-0.0032779516186565161 -0.00082457443932071328 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 4.0174708596375588 2 0.91692673929610258
		 3 -5.5057376116273211 4 -13.18785412921312 5 -17.169322629871196 6 -23.221723087911528
		 7 -29.274123545951859 9 -32.401465436200041 10 -32.335059781180405 11 -29.744899571570258
		 12 -24.741950692836006 13 -18.361864881630993 14 -14.133187120642036 15 -8.3079956554206227
		 16 -5.4054180835609111 17 0.17940184110792293 18 4.0174708596375588;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.7837720513343811 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.84438431262969971 0.30092716217041016 
		0.30000007152557373 0.066666662693023682 0.97213071584701538 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 0.44550088047981262;
	setAttr -s 17 ".kiy[0:16]"  -0.6210486888885498 -0.055188938975334167 
		-0.028024157509207726 -0.049871485680341721 -0.53573805093765259 -0.95364707708358765 
		-0.17531067132949829 -0.013127489015460014 0.23443938791751862 0.014331607148051262 
		0.020231353119015694 0.02394590713083744 0.075377628207206726 0.025475427508354187 
		0.050251677632331848 0.023377778008580208 0.89528149366378784;
createNode animCurveTA -n "rigw:rig:FKClavicle_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 29.407792300618148 2 28.749131549085089
		 3 27.384739184441138 4 25.752796159845715 5 24.906998293725067 6 23.621263046625259
		 7 22.335527799525448 9 21.671173999895554 10 21.68528107298615 11 22.235519086179018
		 12 23.29831545694773 13 24.653662026258225 14 25.551976104653772 15 26.789444582387521
		 16 27.406050487551351 17 28.592456443086576 18 29.407792300618148;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.98612678050994873 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.99103868007659912 1 0.30000007152557373 
		0.066666662693023682 0.99869024753570557 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  -0.16599403321743011 -0.011724045500159264 
		-0.0059533044695854187 -0.010594372637569904 -0.13357511162757874 0 -0.037241943180561066 
		-0.0027887055184692144 0.051163550466299057 0.0030445100273936987 0.0042978250421583652 
		0.005086914636194706 0.016012754291296005 0.0054118814878165722 0.010675197467207909 
		0.0049662254750728607 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -2.5785961113858922 2 -2.5785961113858922
		 3 -2.5785961113858922 4 -2.5785961113858922 5 -2.5785961113858922 6 -2.5785961113858922
		 7 -2.5785961113858922 9 -2.5785961113858922 10 -2.5785961113858922 11 -2.5785961113858922
		 12 -2.5785961113858922 13 -2.5785961113858922 14 -2.5785961113858922 15 -2.5785961113858922
		 16 -2.5785961113858922 17 -2.5785961113858922 18 -2.5785961113858922;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 35.659335291883437 2 35.659335291883437
		 3 35.659335291883437 4 35.659335291883437 5 35.659335291883437 6 35.659335291883437
		 7 35.659335291883437 9 35.659335291883437 10 35.659335291883437 11 35.659335291883437
		 12 35.659335291883437 13 35.659335291883437 14 35.659335291883437 15 35.659335291883437
		 16 35.659335291883437 17 35.659335291883437 18 35.659335291883437;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 5.8013337452672609 2 5.8013337452672609
		 3 5.8013337452672609 4 5.8013337452672609 5 5.8013337452672609 6 5.8013337452672609
		 7 5.8013337452672609 9 5.8013337452672609 10 5.8013337452672609 11 5.8013337452672609
		 12 5.8013337452672609 13 5.8013337452672609 14 5.8013337452672609 15 5.8013337452672609
		 16 5.8013337452672609 17 5.8013337452672609 18 5.8013337452672609;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		1 0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		1 0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 9.4652155929241371 2 1.5178770334209442
		 3 -12.55801809778748 4 -21.198265706436221 5 -22.825115013238445 6 -22.265565759549737
		 7 -21.706016505861022 9 -25.2225965834531 10 -28.86723713543638 11 -10.142878570367031
		 12 9.4652155929241371 13 9.4652155929241371 14 9.4652155929241371 15 9.4652155929241371
		 16 9.4652155929241371 17 9.4652155929241371 18 9.4652155929241371;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.46157705783843994 0.066666662693023682 
		0.63777971267700195 0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 
		0.65235686302185059 0.033333420753479004 1 0.033333420753479004 0.099999904632568359 
		0.033333420753479004 0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  -0.88710016012191772 -0.14399784803390503 
		-0.77021878957748413 -0.036580685526132584 0 0 0.015625579282641411 -0.097851753234863281 
		0.75791192054748535 0.10295619815587997 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 35.928090340551471 2 35.928090340551471
		 3 35.928090340551471 4 35.928090340551471 5 35.928090340551471 6 45.054344920481313
		 7 54.180599500411162 9 63.855179211796504 10 66.694455410936357 11 68.807912173952587
		 12 72.876245398676005 13 70.713678230307508 14 64.02042403127075 15 51.868126881541514
		 16 45.865960486793881 17 37.248208697442074 18 35.928090340551471;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 0.20483367145061493 0.30000007152557373 0.066666662693023682 
		1 0.033333420753479004 0.033333420753479004 0.033333420753479004 0.099999904632568359 
		0.033333420753479004 0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0.97879678010940552 0.44599294662475586 
		0.061353575438261032 0 0.015328597277402878 0.0082067623734474182 -0.02795761451125145 
		-0.14340686798095703 -0.054478086531162262 -0.098674684762954712 -0.022098317742347717 
		0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 -0.0014406073854741188 3 -0.011907060722928044
		 4 -0.035574183097107401 5 -0.053302472177775305 6 -0.081085614161117425 7 -0.10886875614445957
		 9 -0.12859625997301358 10 -0.1389157062488954 11 -0.14444293120487495 12 -8.627730876910304
		 13 -0.14300232628717119 14 -0.12421562483813535 15 -0.078586591229708458 16 -0.053302491272021607
		 17 -0.011907069698827774 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 0.066666662693023682 
		0.099999904632568359 1 0.033333420753479004 0.033333420753479004 0.099999904632568359 
		0.033333420753479004 0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 -4.926042674924247e-05 -6.4654290326870978e-05 
		-0.00019473256543278694 -0.00032327146618627012 0 -0.00087629671907052398 -0.00014547245518770069 
		-0.00013854503049515188 0 0.031727544963359833 4.92602885060478e-05 0.00048028913442976773 
		0.00022167188581079245 0.00043102845665998757 0.0001293085515499115 0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 -0.27259048650185391 3 -0.88748313414258795
		 4 -1.7315081696130488 5 -2.2459667965476995 6 -2.9331965246385501 7 -3.6204262527294007
		 9 -4.0570179481862745 10 -4.2765425797681162 11 -4.3909411612619413 12 -3.8594366482144422
		 13 -4.3614478135482999 14 -3.9619460828385136 15 -2.9037587021697471 16 -2.245967375139041
		 17 -0.88748353675567582 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  0.99772155284881592 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.099999994039535522 1 0.30000007152557373 
		0.066666662693023682 0.099999904632568359 1 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		0.9909784197807312;
	setAttr -s 17 ".kiy[0:16]"  -0.067466266453266144 -0.0049915816634893417 
		-0.0028389615472406149 -0.0060093961656093597 -0.008891252800822258 0 -0.019882487133145332 
		-0.0031392343807965517 -0.0029013562016189098 0 -0.0019878214225172997 0.0010139087680727243 
		0.010482336394488811 0.0054907370358705521 0.011855000630021095 0.0056779221631586552 
		0.13402174413204193;
createNode animCurveTA -n "rigw:rig:FKShoulderPad1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 12.466709473616586 2 11.507444565631014
		 3 9.2456733409841352 4 6.0471642991476857 5 4.088454076283532 6 1.566238842063393
		 7 -0.95597639215674568 9 -2.3398412742968806 10 -2.8463749134792389 11 -2.6438471084312321
		 12 -1.66473245878627 13 -2.5423515790208859 14 -1.1675455178189569 15 2.4739973942806972
		 16 4.7376566768879522 17 9.4126103335782183 18 12.466709473616586;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  0.97397714853286743 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.099999994039535522 1 0.30000007152557373 
		0.066666662693023682 0.099999904632568359 0.99501329660415649 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 0.9066198468208313;
	setAttr -s 17 ".kiy[0:16]"  -0.22664648294448853 -0.017894407734274864 
		-0.010636895895004272 -0.022905843332409859 -0.033753916621208191 0 -0.067823313176631927 
		-0.0087739359587430954 -0.0042612524703145027 0.099742420017719269 -0.003661896800622344 
		0.0034891853574663401 0.036072861403226852 0.018895279616117477 0.040796652436256409 
		0.019539449363946915 0.42194834351539612;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 -0.0016221617474642966 3 -0.013329938319580242
		 4 -0.039469668946629455 5 -0.058750473012234072 6 -0.087671678595642108 7 -0.11659288417905014
		 9 -0.13496561994401474 10 -0.14282076471433516 11 -4.641325779552318 12 -3.3102466053672366
		 13 -0.13477689603705711 14 -0.11032751042814908 15 -0.066525015706380419 16 -0.04437108906338489
		 17 -0.0096660347131731633 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 0.066666662693023682 
		0.099999904632568359 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 -5.539316771319136e-05 -7.2011134761851281e-05 
		-0.00021326373098418117 -0.00034897690056823194 0 -0.00085997878341004252 -0.00012463478196877986 
		-8.3089893450960517e-05 -0.058864064514636993 0.045207753777503967 0.00010585357813397422 
		0.0005160361179150641 0.00019847507064696401 0.00037048716330900788 0.00010585345444269478 
		0;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 -0.29051392516399932 3 -0.94872629435593381
		 4 -1.8498824971205143 5 -2.3937921730432645 6 -3.093879797928893 7 -3.7939674228145215
		 9 -4.1932898105976957 10 -4.3577088592600308 11 -4.1327278913917347 12 -4.2091591189616686
		 13 -4.1892868556337222 14 -3.6534624417620676 15 -2.5984585980087949 16 -1.9941366336776147
		 17 -0.78549146293667282 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  0.9974290132522583 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.099999994039535522 1 0.30000007152557373 
		0.066666662693023682 0.099999904632568359 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 0.99275827407836914;
	setAttr -s 17 ".kiy[0:16]"  -0.071661889553070068 -0.0053323651663959026 
		-0.0030403845012187958 -0.0063848048448562622 -0.0093456758186221123 0 -0.019133364781737328 
		-0.002642790088430047 -0.0017119609983637929 0.0033799938391894102 -0.002595862839370966 
		0.0022457835730165243 0.011664736084640026 0.0051396265625953674 0.010726178996264935 
		0.0050055515021085739 0.12012950330972672;
createNode animCurveTA -n "rigw:rig:FKShoulderPad2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 12.466709473616586 2 11.444527721513602
		 3 9.0268588075530225 4 5.6256339431541589 5 3.5693447450544733 6 1.0448693218257461
		 7 -1.4796061014029815 9 -2.6451459018813259 10 -2.8660167840987318 11 -1.9979671053114865
		 12 -2.2565378969538488 13 -1.949893586480177 14 -0.10595954221032285 15 3.5246280639880343
		 16 5.6042824173865169 17 9.7635961250780294 18 12.466709473616586;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  0.97076833248138428 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.099999994039535522 1 0.30000007152557373 
		0.066666662693023682 0.099999904632568359 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 0.92315864562988281;
	setAttr -s 17 ".kiy[0:16]"  -0.24001850187778473 -0.019107330590486526 
		-0.011363507248461246 -0.024197489023208618 -0.035176381468772888 0 -0.06246638298034668 
		-0.0060991295613348484 0.0017442496027797461 0.0063661695457994938 -0.0055307853035628796 
		0.0077284569852054119 0.040141891688108444 0.017687015235424042 0.036912042647600174 
		0.017225611954927444 0.38441920280456543;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 2.1910958889935923 3 5.6880170401771846
		 4 11.442403267262266 5 17.403301561807208 6 19.66544597792824 7 11.371688946667945
		 9 11.371688946667945 10 73.441318603340449 11 0.35449051363893008 12 0 13 0 14 0
		 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.83810573816299438 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.61510884761810303 1 0.30000007152557373 
		0.066666662693023682 1 0.033333420753479004 1 0.033333420753479004 0.099999904632568359 
		0.033333420753479004 0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0.54550790786743164 0.033949926495552063 
		0.015258185565471649 0.055194895714521408 0.78844213485717773 0 0.69667202234268188 
		0.088717199862003326 0 -0.21099433302879333 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 -1.7051373818739224 3 -5.6734564874925217
		 4 -9.7619109718375867 5 -10.633856645658987 6 -0.9724990125520554 7 2.3044211455628938
		 9 2.3044211455628938 10 0 11 -3.6623233585064767 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.9292101263999939 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 1 0.28314471244812012 0.30000007152557373 
		0.066666662693023682 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  -0.36955180764198303 -0.032465741038322449 
		-0.017315065488219261 -0.019073620438575745 0 0.95907723903656006 0.15414965152740479 
		0.021205762401223183 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 -3.7099161829088296 3 -12.343902416079288
		 4 -21.239268936593586 5 -23.136385590504368 6 -9.1135062932058677 7 -5.5077950048502613
		 9 -5.5077950048502613 10 0 11 -3.825254649967829 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.75620079040527344 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 1 0.21176344156265259 0.30000007152557373 
		0.066666662693023682 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  -0.65433967113494873 -0.070636637508869171 
		-0.037672851234674454 -0.041499048471450806 0 0.97732096910476685 0.33538779616355896 
		0.046137981116771698 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 3.6222291463792899
		 12 12.944146902232085 13 18.270952473798943 14 17.106713595890426 15 12.806616944805921
		 16 9.9607048204313298 17 3.9477041089683444 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 1 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 0.43550208210945129;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0.027891078963875771 
		0.031377561390399933 0 -0.038537565618753433 -0.023433908820152283 -0.051850449293851852 
		-0.025302378460764885 -0.90018773078918457;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 -1.068151794027393
		 12 -3.8170730626846474 13 -5.3878839509666578 14 -5.0445638348239887 15 -3.7765172003355176
		 16 -2.9372919203880392 17 -1.1641304429347474 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 1 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 -0.0082247406244277954 
		-0.0092528648674488068 0 0.011364252306520939 0.0069103674031794071 0.015290076844394207 
		0.007461372297257185 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 -3.920067404899898
		 12 -14.008481079807057 13 -19.773284343043709 14 -18.513315420257488 15 -13.859642942426056
		 16 -10.779725393751828 17 -4.2723047003702099 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 1 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 0.4081108570098877;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 -0.030184421688318253 
		-0.033957593142986298 0 0.041706353425979614 0.025360787287354469 0.056113876402378082 
		0.027382871136069298 0.91293239593505859;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKIndexFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 -0.31345381183121246 3 -0.49448437883560031
		 4 -0.90728647997610179 5 -1.1825133863032609 6 -1.0205568032709915 7 -0.85860022023872229
		 9 -0.82982196910624773 10 -0.81114380713881507 11 0.10460776869793434 12 0.48240389396343264
		 13 0.73971641561581236 14 0.85634781883309996 15 0.90841874485391683 16 0.89213620515006564
		 17 0.59047232440488107 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.23018315434455872 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.85739541053771973 0.20159099996089935 
		1 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 0.55112677812576294 
		0.099999904632568359 0.99947381019592285 0.066666483879089355 0.033333420753479004 
		0.056362289935350418;
	setAttr -s 17 ".kiy[0:16]"  -0.97314733266830444 -0.28184729814529419 
		-0.0084547223523259163 -0.26748296618461609 -0.51465833187103271 0.97946977615356445 
		0 0.019923374056816101 0 0.098660990595817566 0.057984232902526855 0.83442157506942749 
		0.082512855529785156 -0.03243488073348999 -0.039791498333215714 -0.16654323041439056 
		-0.99841034412384033;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -0.10684541804113556 2 -0.92293521963398673
		 3 -0.9712815877613844 4 -0.41487612749669778 5 -0.15845479151496833 6 -0.15845479151496833
		 7 -0.15845479151496833 9 -0.15845479151496833 10 -0.15845479151496833 11 -1.0848429779376214
		 12 -0.73245028503892529 13 0.30558793539335305 14 0.3421961835351065 15 0.097814131203460608
		 16 -0.053973122788930211 17 -0.26212892385189607 18 -0.10684541804113556;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.099349804222583771 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 
		1 0.033333420753479004 0.033333420753479004 0.36480975151062012 0.099999904632568359 
		0.57279348373413086 0.066666483879089355 0.033333420753479004 0.20988017320632935;
	setAttr -s 17 ".kiy[0:16]"  -0.99505257606506348 -0.66770994663238525 
		0.078218042850494385 0.2780112624168396 0 0 0 0 0 -0.09631078690290451 0.2287324070930481 
		0.93108206987380981 -0.12870441377162933 -0.81969970464706421 -0.18403667211532593 
		0.028234438970685005 0.97772711515426636;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 1 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 1 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 1 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -2.9645802037807929 2 10.267270904440361
		 3 12.56187408153351 4 13.159636644919066 5 14.144510249578516 6 13.461327288583467
		 7 12.778144327588423 9 9.9129959545542032 10 5.9892435153443637 11 -0.68394468802941144
		 12 -3.16526277491525 13 -4.0133442096848997 14 -4.7121548207516453 15 -5.7888400753754201
		 16 -5.3813062110547714 17 -4.1245498090230033 18 -2.9645802037807929;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.33272382616996765 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 1 1 1 0.066666662693023682 0.83112996816635132 
		0.033333420753479004 0.033333420753479004 1 0.099999904632568359 1 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0.94302433729171753 0.18895041942596436 
		-0.005527933593839407 0.01718929223716259 0 0 0 -0.042462226003408432 -0.55607825517654419 
		-0.011402127332985401 -0.005865827202796936 0 -0.019514482468366623 0 0.011639135889708996 
		0.016418043524026871 0;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 1 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -9.6736369436839897 2 -8.7881931671983562
		 3 -6.9540284910302832 4 -4.7601932518731314 5 -3.6231779619470355 6 -1.8947521827807912
		 7 -0.16632640361454662 9 0.72677025147153662 10 0.70780632522427356 11 -0.03188345251584037
		 12 -1.4606099606060596 13 -3.2826151273087247 14 -4.4902278166682752 15 -6.1537681924326453
		 16 -6.9826773245904725 17 -8.5775731339389818 18 -9.6736369436839897;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.97534030675888062 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.98397845029830933 1 0.30000007152557373 
		0.066666662693023682 0.99763685464859009 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0.22070649266242981 0.015760678797960281 
		0.0080030476674437523 0.014242147095501423 0.17828744649887085 0 0.050064671784639359 
		0.0037489023525267839 -0.068707160651683807 -0.0040927855297923088 -0.0057776146568357944 
		-0.0068383901380002499 -0.021526113152503967 -0.0072751999832689762 -0.014350729994475842 
		-0.006676175631582737 0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 8.2377486864530631 2 7.5717556390053185
		 3 6.1921752057180459 4 4.5420654872629473 5 3.6868510659933182 6 2.3868028215090824
		 7 1.086754577024847 9 0.41500538799613623 10 0.4292692890091071 11 0.98563249266417763
		 12 2.0602597570245318 13 3.4306944472138845 14 4.3390093801433967 15 5.590253471631339
		 16 6.2137233894347288 17 7.4133360592937985 18 8.2377486864530631;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.98582279682159424 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.99084079265594482 1 0.30000007152557373 
		0.066666662693023682 0.99866098165512085 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  -0.16779015958309174 -0.011854513548314571 
		-0.0060195485129952431 -0.010712344199419022 -0.13503529131412506 0 -0.037656504660844803 
		-0.0028197644278407097 0.051731664687395096 0.0030784169211983681 0.0043456703424453735 
		0.0051435451023280621 0.016191009432077408 0.0054720966145396233 0.010794001631438732 
		0.0050215343944728374 0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -28.616933717789784 2 -28.609491046727179
		 3 -28.564050516675984 4 -28.49843688362591 5 -28.482573919133241 6 -28.433876501382255
		 7 -28.385179083631268 9 -28.333555612669876 10 -28.318405527884678 11 -28.332911857276148
		 12 -28.382887957302071 13 -28.452765882091239 14 -28.497346292481588 15 -28.552452775658015
		 16 -28.575759367364629 17 -28.608575054176121 18 -28.616933717789784;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0.00024610524997115135 0.00024615702568553388 
		0.00033839468960650265 0 0 0.0023798032198101282 0.0003274240589234978 0 -0.00012041492300340906 
		-0.00021883135195821524 -0.00025933099095709622 -0.00076768675353378057 -0.00021884441957809031 
		-0.00037281782715581357 -9.3780916358809918e-05 0;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 19.571115431450416 2 16.370874914223521
		 3 9.7416922904932903 4 1.8125607120396707 5 -2.2969300944695936 6 -8.5439428601703131
		 7 -14.790955625871032 9 -18.018855268624733 10 -17.95031496253274 11 -15.276869969584489
		 12 -10.113054110715169 13 -3.5278186967305269 14 0.83683063507355682 15 6.8493293005142872
		 16 9.8452381167350964 17 15.609634833035274 18 19.571115431450416;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.77407824993133545 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.83657604455947876 0.29236552119255066 
		0.30000007152557373 0.066666662693023682 0.97038966417312622 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 0.43427357077598572;
	setAttr -s 17 ".kiy[0:16]"  -0.63308995962142944 -0.056963548064231873 
		-0.028925249353051186 -0.051475059241056442 -0.5478508472442627 -0.95630663633346558 
		-0.1809476763010025 -0.01354953832924366 0.24154467880725861 0.014792509377002716 
		0.020881922915577888 0.024715865030884743 0.077801354229450226 0.026294579729437828 
		0.051867496222257614 0.024129459634423256 0.90078097581863403;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -11.508087095121756 2 -9.8204810207866906
		 3 -6.324666013780611 4 -2.1433393847809934 5 0.023748204828407053 6 3.3180307415642205
		 7 6.612313278300034 9 8.3145053976265118 10 8.2783610665869993 11 6.8685537558553857
		 12 4.145481000251876 13 0.67284089801715086 14 -1.6288013858286494 15 -4.799415693831981
		 16 -6.3792699442422798 17 -9.4190514740248972 18 -11.508087095121756;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  0.91823887825012207 0.066666662693023682 
		0.033333331346511841 0.066666662693023682 0.94522398710250854 0.50155603885650635 
		0.30000007152557373 0.066666662693023682 0.99149489402770996 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0.39602690935134888 0.030038995668292046 
		0.015253362245857716 0.027144715189933777 0.32642245292663574 0.86512517929077148 
		0.09542044997215271 0.0071451817639172077 -0.13014602661132812 -0.0078006098046898842 
		-0.011011801660060883 -0.013033600524067879 -0.041027553379535675 -0.013866111636161804 
		-0.027351671829819679 -0.012724372558295727 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0.99325066795376338 2 0 3 0 4 0 5 0 6 0
		 7 0 9 0 10 0 11 0 12 0.21294404176418483 13 1.1882750241464759 14 1.3155401770090056
		 15 1.3559525391918259 16 1.3165244218239207 17 1.1089971306891797 18 0.99325066795376338;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.51521891355514526 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.40947818756103516 0.27673876285552979;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0.18975599110126495 0.85705858469009399 
		0.087123103439807892 -0.0094616468995809555 -0.060246240347623825 -0.91231995820999146 
		-0.96094518899917603;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0.2020674675565548 2 0.2020674675565548
		 3 -0.13656932836498248 4 -0.96665479796715692 5 -1.4679615677073294 6 -2.2945361265461965
		 7 -2.9517057099658577 9 -3.609390203189776 10 -4.1064337179351273 11 -4.1073996291854078
		 12 -3.8233456962995089 13 -2.8086791259621608 14 -2.1998791023938198 15 -1.3881459695641871
		 16 -0.98227969993164788 17 -0.17054643578842249 18 0.2020674675565548;
	setAttr -s 17 ".kit[0:16]"  2 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 2;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		0.19562453031539917 0.044886093586683273 0.30000007152557373 0.066666662693023682 
		0.19430564343929291 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.16208599507808685 
		0.089102357625961304;
	setAttr -s 17 ".kiy[1:16]"  0 -0.10006260126829147 -0.33420455455780029 
		-0.98067891597747803 -0.99899214506149292 -1.5145320892333984 -0.33656272292137146 
		-0.98094111680984497 -0.0098211690783500671 0.20293368399143219 0.20293301343917847 
		0.60880011320114136 0.2029329389333725 0.40586638450622559 0.98677664995193481 0.99602246284484863;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 2.9375927628457825 13 13.339695348729876 14 8.7654029140434666 15 -1.7229424877505095
		 16 -7.1168019783737408 17 -14.356278493137218 18 0;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.9360690712928772 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.91648018360137939 0.13187134265899658;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0.044817786663770676 
		-0.35181623697280884 -0.11453345417976379 -0.048873327672481537 -0.088296093046665192 
		-0.40007996559143066 0.99126684665679932;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_roll";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_rollAngle";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 25 2 25 3 25 4 25 5 25 6 25 7 25 9 25
		 10 25 11 25 12 25 13 25 14 25 15 25 16 25 17 25 18 25;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_toe";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 9 1 10 1 11 1
		 12 1 13 1 14 1 15 1 16 1 17 1 18 1;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 9 1 10 1 11 1
		 12 1 13 1 14 1 15 1 16 1 17 1 18 1;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 1;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -9.8133534021774214 2 0 3 0 4 0 5 0 6 0
		 7 0 9 0 10 0 11 0 12 0 13 0 14 0 15 0 16 0 17 0 18 -9.8133534021774214;
	setAttr -s 17 ".kit[0:16]"  10 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[1:16]"  1 0.033333331346511841 0.066666662693023682 
		1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		1 0.19103436172008514;
	setAttr -s 17 ".kiy[1:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.9815833568572998;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegBall_R_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 11.689967258967004
		 12 18.828230648492177 13 14.664295886389429 14 10.949339562479338 15 5.7933016329283857
		 16 3.519433533725985 17 0.44898146678418904 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0.070625171065330505 
		-0.0075834253802895546 -0.019906410947442055 -0.068250782787799835 -0.02123352512717247 
		-0.036400362849235535 -0.0075834156014025211 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_R_follow";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10 9 10
		 10 10 11 10 12 10 13 10 14 10 15 10 16 10 17 10 18 10;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0.98614102296717088 5 1.7461145183477229
		 6 1.7812153508643382 7 1.8163161833809536 9 1.4848485497109294 10 1.0791478681413711
		 11 0 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		2 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.40959060192108154 1 0.30000007152557373 0.29211735725402832 
		0.081886447966098785 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0.70882022380828857 0.9122694730758667 
		0 -0.50489455461502075 -0.95638251304626465 -0.99664169549942017 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 -2.6317205118894038 2 -2.6324002560121653
		 3 -2.9806304974202642 4 -2.4838585943514304 5 -1.9809482109236616 6 -1.2265823344036773
		 7 -0.47221645788369249 9 0.19833086406464639 10 0.66968329158651629 11 0.66968329158651629
		 12 0.44336077568100823 13 -0.69860725577548799 14 -1.3892246450496957 15 -2.2989830854529565
		 16 -2.7529219667943563 17 -3.6697972275225119 18 -2.6317205118894038;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		2 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  0 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 0.044144146144390106 0.30000007152557373 
		0.19502446055412292 0.070542313158512115 0.033333420753479004 0.23802118003368378 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 0.15568673610687256;
	setAttr -s 17 ".kiy[0:16]"  0 -0.00067943811882287264 -0.17377525568008423 
		0.33527395129203796 0.50291061401367188 0.99902516603469849 1.5087317228317261 0.98079836368560791 
		0.99750882387161255 0 -0.97126001119613647 -0.22765253484249115 -0.68295872211456299 
		-0.21149282157421112 -0.42298537492752075 -0.21149244904518127 -0.98780649900436401;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 13.16059270120576 5 21.011264536234084
		 6 10.641788880013792 7 0.27231322379350326 9 -8.5559619527323338 10 0 11 0 12 0 13 0
		 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.97581952810287476 0.18113422393798828 0.30000007152557373 
		0.82400637865066528 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0.15639449656009674 -0.21857807040214539 
		-0.98345839977264404 -0.44533747434616089 -0.56658047437667847 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_roll";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_rollAngle";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 25 2 25 3 25 4 25 5 25 6 25 7 25 9 25
		 10 25 11 25 12 25 13 25 14 25 15 25 16 25 17 25 18 25;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_toe";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 9 1 10 1 11 1
		 12 1 13 1 14 1 15 1 16 1 17 1 18 1;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 9 1 10 1 11 1
		 12 1 13 1 14 1 15 1 16 1 17 1 18 1;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 0.099999994039535522 1 0.30000007152557373 1 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 -8.9248835028160141
		 11 0 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 1 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 1 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		10 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 1 1 0.033333420753479004 0.033333420753479004 
		0.033333420753479004 0.099999904632568359 0.033333420753479004 0.066666483879089355 
		0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "rigw:rig:IKLegBall_L_rotateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateX";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateY";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateZ";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_L_follow";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10 9 10
		 10 10 11 10 12 10 13 10 14 10 15 10 16 10 17 10 18 10;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_R_FKIKBlend";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_R_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 9 1 10 1 11 1
		 12 1 13 1 14 1 15 1 16 1 17 1 18 1;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 9 1 9 
		1 1 1 1 1 1 1 1 9;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 1 1 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_R_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 9 1 9 
		1 1 1 1 1 1 1 1 9;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 1 1 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKIKBlend";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10 9 10
		 10 10 11 10 12 10 13 10 14 10 15 10 16 10 17 10 18 10;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 9 1 9 
		1 1 1 1 1 1 1 1 9;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 1 1 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 9 1 10 1 11 1
		 12 1 13 1 14 1 15 1 16 1 17 1 18 1;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 9 1 9 
		1 1 1 1 1 1 1 1 9;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 1 1 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_FKIKBlend";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 9 1 10 1 11 1
		 12 1 13 1 14 1 15 1 16 1 17 1 18 1;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 9 1 9 
		1 1 1 1 1 1 1 1 9;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 1 1 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 9 1 9 
		1 1 1 1 1 1 1 1 9;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 1 1 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_FKIKBlend";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 9 1 10 1 11 1
		 12 1 13 1 14 1 15 1 16 1 17 1 18 1;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 9 1 9 
		1 1 1 1 1 1 1 1 9;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 1 1 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 9 1 9 
		1 1 1 1 1 1 1 1 9;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 1 1 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKIKBlend";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 10 2 10 3 10 4 10 5 10 6 10 7 10 9 10
		 10 10 11 10 12 10 13 10 14 10 15 10 16 10 17 10 18 10;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 10 1 1 
		1 1 1 1 1 1 1 1 10;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 0.30000007152557373 0.066666662693023682 1 0.033333420753479004 
		0.033333420753479004 0.033333420753479004 0.099999904632568359 0.033333420753479004 
		0.066666483879089355 0.033333420753479004 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 0 2 0 3 0 4 0 5 0 6 0 7 0 9 0 10 0 11 0
		 12 0 13 0 14 0 15 0 16 0 17 0 18 0;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 9 1 9 
		1 1 1 1 1 1 1 1 9;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 1 1 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  1 1 2 1 3 1 4 1 5 1 6 1 7 1 9 1 10 1 11 1
		 12 1 13 1 14 1 15 1 16 1 17 1 18 1;
	setAttr -s 17 ".kit[0:16]"  1 1 1 1 1 9 1 9 
		1 1 1 1 1 1 1 1 9;
	setAttr -s 17 ".kix[0:16]"  1 0.066666662693023682 0.033333331346511841 
		0.066666662693023682 1 1 1 1 1 0.033333420753479004 0.033333420753479004 0.033333420753479004 
		0.099999904632568359 0.033333420753479004 0.066666483879089355 0.033333420753479004 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "rigw:rig:Main_visibility";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 18 1;
	setAttr -s 2 ".kit[1]"  9;
	setAttr -s 2 ".kot[1]"  5;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "rigw:rig:Main_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 18 0;
	setAttr -s 2 ".kit[1]"  10;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "rigw:rig:Main_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 18 0;
	setAttr -s 2 ".kit[1]"  10;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "rigw:rig:Main_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 18 0;
	setAttr -s 2 ".kit[1]"  10;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  0 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "rigw:rig:Main_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 18 0;
	setAttr -s 2 ".kit[1]"  10;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "rigw:rig:Main_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 18 0;
	setAttr -s 2 ".kit[1]"  10;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "rigw:rig:Main_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 18 0;
	setAttr -s 2 ".kit[1]"  10;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "rigw:rig:Main_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.2 18 2.2;
	setAttr -s 2 ".kit[1]"  10;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "rigw:rig:Main_scaleY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.2 18 2.2;
	setAttr -s 2 ".kit[1]"  10;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "rigw:rig:Main_scaleZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 2.2 18 2.2;
	setAttr -s 2 ".kit[1]"  10;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode mia_exposure_simple -n "mia_exposure_simple1";
	setAttr ".S02" 1;
	setAttr ".S04" 1.1499999761581421;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 10;
	setAttr ".unw" 10;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 11 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 13 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 20 ".tx";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 31 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 3 ".sol";
connectAttr "rigw:rig:Main_scaleY.o" "rigwRN.phl[1]";
connectAttr "rigw:rig:Main_scaleX.o" "rigwRN.phl[2]";
connectAttr "rigw:rig:Main_scaleZ.o" "rigwRN.phl[3]";
connectAttr "rigw:rig:Main_translateX.o" "rigwRN.phl[4]";
connectAttr "rigw:rig:Main_translateY.o" "rigwRN.phl[5]";
connectAttr "rigw:rig:Main_translateZ.o" "rigwRN.phl[6]";
connectAttr "rigw:rig:Main_visibility.o" "rigwRN.phl[7]";
connectAttr "rigw:rig:Main_rotateX.o" "rigwRN.phl[8]";
connectAttr "rigw:rig:Main_rotateY.o" "rigwRN.phl[9]";
connectAttr "rigw:rig:Main_rotateZ.o" "rigwRN.phl[10]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateX.o" "rigwRN.phl[11]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateY.o" "rigwRN.phl[12]";
connectAttr "rigw:rig:FKMiddleFinger1_R_rotateZ.o" "rigwRN.phl[13]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateX.o" "rigwRN.phl[14]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateY.o" "rigwRN.phl[15]";
connectAttr "rigw:rig:FKMiddleFinger2_R_rotateZ.o" "rigwRN.phl[16]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateX.o" "rigwRN.phl[17]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateY.o" "rigwRN.phl[18]";
connectAttr "rigw:rig:FKIndexFinger1_R_rotateZ.o" "rigwRN.phl[19]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateX.o" "rigwRN.phl[20]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateY.o" "rigwRN.phl[21]";
connectAttr "rigw:rig:FKIndexFinger2_R_rotateZ.o" "rigwRN.phl[22]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateX.o" "rigwRN.phl[23]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateY.o" "rigwRN.phl[24]";
connectAttr "rigw:rig:FKThumbFinger1_R_rotateZ.o" "rigwRN.phl[25]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateX.o" "rigwRN.phl[26]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateY.o" "rigwRN.phl[27]";
connectAttr "rigw:rig:FKThumbFinger2_R_rotateZ.o" "rigwRN.phl[28]";
connectAttr "rigw:rig:FKClavicle_R_rotateX.o" "rigwRN.phl[29]";
connectAttr "rigw:rig:FKClavicle_R_rotateY.o" "rigwRN.phl[30]";
connectAttr "rigw:rig:FKClavicle_R_rotateZ.o" "rigwRN.phl[31]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[32]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[33]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[34]";
connectAttr "rigw:rig:FKElbow_R_rotateX.o" "rigwRN.phl[35]";
connectAttr "rigw:rig:FKElbow_R_rotateY.o" "rigwRN.phl[36]";
connectAttr "rigw:rig:FKElbow_R_rotateZ.o" "rigwRN.phl[37]";
connectAttr "rigw:rig:FKWrist_R_rotateX.o" "rigwRN.phl[38]";
connectAttr "rigw:rig:FKWrist_R_rotateY.o" "rigwRN.phl[39]";
connectAttr "rigw:rig:FKWrist_R_rotateZ.o" "rigwRN.phl[40]";
connectAttr "rigw:rig:FKShoulderPad1_R_rotateX.o" "rigwRN.phl[41]";
connectAttr "rigw:rig:FKShoulderPad1_R_rotateY.o" "rigwRN.phl[42]";
connectAttr "rigw:rig:FKShoulderPad1_R_rotateZ.o" "rigwRN.phl[43]";
connectAttr "rigw:rig:FKShoulderPad2_R_rotateX.o" "rigwRN.phl[44]";
connectAttr "rigw:rig:FKShoulderPad2_R_rotateY.o" "rigwRN.phl[45]";
connectAttr "rigw:rig:FKShoulderPad2_R_rotateZ.o" "rigwRN.phl[46]";
connectAttr "rigw:rig:FKClavicle_L_rotateX.o" "rigwRN.phl[47]";
connectAttr "rigw:rig:FKClavicle_L_rotateY.o" "rigwRN.phl[48]";
connectAttr "rigw:rig:FKClavicle_L_rotateZ.o" "rigwRN.phl[49]";
connectAttr "rigw:rig:FKShoulder_L_rotateX.o" "rigwRN.phl[50]";
connectAttr "rigw:rig:FKShoulder_L_rotateY.o" "rigwRN.phl[51]";
connectAttr "rigw:rig:FKShoulder_L_rotateZ.o" "rigwRN.phl[52]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[53]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[54]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[55]";
connectAttr "rigw:rig:FKWrist_L_rotateX.o" "rigwRN.phl[56]";
connectAttr "rigw:rig:FKWrist_L_rotateY.o" "rigwRN.phl[57]";
connectAttr "rigw:rig:FKWrist_L_rotateZ.o" "rigwRN.phl[58]";
connectAttr "rigw:rig:FKShoulderPad1_L_rotateX.o" "rigwRN.phl[59]";
connectAttr "rigw:rig:FKShoulderPad1_L_rotateY.o" "rigwRN.phl[60]";
connectAttr "rigw:rig:FKShoulderPad1_L_rotateZ.o" "rigwRN.phl[61]";
connectAttr "rigw:rig:FKShoulderPad2_L_rotateX.o" "rigwRN.phl[62]";
connectAttr "rigw:rig:FKShoulderPad2_L_rotateY.o" "rigwRN.phl[63]";
connectAttr "rigw:rig:FKShoulderPad2_L_rotateZ.o" "rigwRN.phl[64]";
connectAttr "rigw:rig:FKHipTwist_R_rotateX.o" "rigwRN.phl[65]";
connectAttr "rigw:rig:FKHipTwist_R_rotateY.o" "rigwRN.phl[66]";
connectAttr "rigw:rig:FKHipTwist_R_rotateZ.o" "rigwRN.phl[67]";
connectAttr "rigw:rig:FKHipTwist_L_rotateX.o" "rigwRN.phl[68]";
connectAttr "rigw:rig:FKHipTwist_L_rotateY.o" "rigwRN.phl[69]";
connectAttr "rigw:rig:FKHipTwist_L_rotateZ.o" "rigwRN.phl[70]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateX.o" "rigwRN.phl[71]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateY.o" "rigwRN.phl[72]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateZ.o" "rigwRN.phl[73]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateX.o" "rigwRN.phl[74]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateY.o" "rigwRN.phl[75]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateZ.o" "rigwRN.phl[76]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateX.o" "rigwRN.phl[77]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateY.o" "rigwRN.phl[78]";
connectAttr "rigw:rig:FKIndexFinger1_L_rotateZ.o" "rigwRN.phl[79]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateX.o" "rigwRN.phl[80]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateY.o" "rigwRN.phl[81]";
connectAttr "rigw:rig:FKIndexFinger2_L_rotateZ.o" "rigwRN.phl[82]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateX.o" "rigwRN.phl[83]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateY.o" "rigwRN.phl[84]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateZ.o" "rigwRN.phl[85]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateX.o" "rigwRN.phl[86]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateY.o" "rigwRN.phl[87]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateZ.o" "rigwRN.phl[88]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[89]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[90]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[91]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[92]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[93]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[94]";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[95]";
connectAttr "rigw:rig:FKSpineA_M_rotateX.o" "rigwRN.phl[96]";
connectAttr "rigw:rig:FKSpineA_M_rotateY.o" "rigwRN.phl[97]";
connectAttr "rigw:rig:FKSpineA_M_rotateZ.o" "rigwRN.phl[98]";
connectAttr "rigw:rig:FKChest_M_rotateX.o" "rigwRN.phl[99]";
connectAttr "rigw:rig:FKChest_M_rotateY.o" "rigwRN.phl[100]";
connectAttr "rigw:rig:FKChest_M_rotateZ.o" "rigwRN.phl[101]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateX.o" "rigwRN.phl[102]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateY.o" "rigwRN.phl[103]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateZ.o" "rigwRN.phl[104]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[105]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[106]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[107]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[108]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[109]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[110]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[111]";
connectAttr "rigw:rig:IKLeg_R_rollAngle.o" "rigwRN.phl[112]";
connectAttr "rigw:rig:IKLeg_R_roll.o" "rigwRN.phl[113]";
connectAttr "rigw:rig:IKLeg_R_toe.o" "rigwRN.phl[114]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[115]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[116]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[117]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[118]";
connectAttr "rigw:rig:IKLegHeel_R_rotateX.o" "rigwRN.phl[119]";
connectAttr "rigw:rig:IKLegHeel_R_rotateY.o" "rigwRN.phl[120]";
connectAttr "rigw:rig:IKLegHeel_R_rotateZ.o" "rigwRN.phl[121]";
connectAttr "rigw:rig:IKLegBall_R_rotateX.o" "rigwRN.phl[122]";
connectAttr "rigw:rig:PoleLeg_R_translateX.o" "rigwRN.phl[123]";
connectAttr "rigw:rig:PoleLeg_R_translateY.o" "rigwRN.phl[124]";
connectAttr "rigw:rig:PoleLeg_R_translateZ.o" "rigwRN.phl[125]";
connectAttr "rigw:rig:PoleLeg_R_follow.o" "rigwRN.phl[126]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[127]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[128]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[129]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[130]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[131]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[132]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[133]";
connectAttr "rigw:rig:IKLeg_L_rollAngle.o" "rigwRN.phl[134]";
connectAttr "rigw:rig:IKLeg_L_roll.o" "rigwRN.phl[135]";
connectAttr "rigw:rig:IKLeg_L_toe.o" "rigwRN.phl[136]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[137]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[138]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[139]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[140]";
connectAttr "rigw:rig:IKLegHeel_L_rotateX.o" "rigwRN.phl[141]";
connectAttr "rigw:rig:IKLegHeel_L_rotateY.o" "rigwRN.phl[142]";
connectAttr "rigw:rig:IKLegHeel_L_rotateZ.o" "rigwRN.phl[143]";
connectAttr "rigw:rig:IKLegBall_L_rotateX.o" "rigwRN.phl[144]";
connectAttr "rigw:rig:PoleLeg_L_translateX.o" "rigwRN.phl[145]";
connectAttr "rigw:rig:PoleLeg_L_translateY.o" "rigwRN.phl[146]";
connectAttr "rigw:rig:PoleLeg_L_translateZ.o" "rigwRN.phl[147]";
connectAttr "rigw:rig:PoleLeg_L_follow.o" "rigwRN.phl[148]";
connectAttr "rigw:rig:FKIKArm_R_FKIKBlend.o" "rigwRN.phl[149]";
connectAttr "rigw:rig:FKIKArm_R_IKVis.o" "rigwRN.phl[150]";
connectAttr "rigw:rig:FKIKArm_R_FKVis.o" "rigwRN.phl[151]";
connectAttr "rigw:rig:FKIKLeg_R_FKIKBlend.o" "rigwRN.phl[152]";
connectAttr "rigw:rig:FKIKLeg_R_IKVis.o" "rigwRN.phl[153]";
connectAttr "rigw:rig:FKIKLeg_R_FKVis.o" "rigwRN.phl[154]";
connectAttr "rigw:rig:FKIKSpine_M_FKIKBlend.o" "rigwRN.phl[155]";
connectAttr "rigw:rig:FKIKSpine_M_IKVis.o" "rigwRN.phl[156]";
connectAttr "rigw:rig:FKIKSpine_M_FKVis.o" "rigwRN.phl[157]";
connectAttr "rigw:rig:FKIKArm_L_FKIKBlend.o" "rigwRN.phl[158]";
connectAttr "rigw:rig:FKIKArm_L_IKVis.o" "rigwRN.phl[159]";
connectAttr "rigw:rig:FKIKArm_L_FKVis.o" "rigwRN.phl[160]";
connectAttr "rigw:rig:FKIKLeg_L_FKIKBlend.o" "rigwRN.phl[161]";
connectAttr "rigw:rig:FKIKLeg_L_IKVis.o" "rigwRN.phl[162]";
connectAttr "rigw:rig:FKIKLeg_L_FKVis.o" "rigwRN.phl[163]";
connectAttr "mia_exposure_simple1.msg" "orthCamShape.mils";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "mia_exposure_simple1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Juggernaut__mc-rigw@Jug_run.ma
