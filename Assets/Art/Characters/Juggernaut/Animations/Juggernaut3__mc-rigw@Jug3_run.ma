//Maya ASCII 2013 scene
//Name: Juggernaut3__mc-rigw@Jug3_run.ma
//Last modified: Thu, Jun 05, 2014 03:01:31 PM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Juggernaut/Juggernaut3__mc-rigw.ma";
file -rdi 2 -ns "rig" -rfn "rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Juggernaut__rig.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Juggernaut/Juggernaut3__mc-rigw.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 71.320506300546981 29.32045024793301 41.048866095128965 ;
	setAttr ".r" -type "double3" -13.538352729602623 59.000000000000526 1.5438444439369916e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 85.645651207789911;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" 0 0 500 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".ow" 72;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 11 ".lnk";
	setAttr -s 11 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 2 ".fn";
	setAttr ".fn[0]" -type "string" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Juggernaut/Juggernaut2__mc-rigw.ma";
	setAttr ".fn[1]" -type "string" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Juggernaut/Juggernaut__mc-rigw.ma";
	setAttr -s 170 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 0
		"rigw:rigRN" 0
		"rigw:rigRN" 523
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scale" " -type \"double3\" 2.2 2.2 2.2"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scaleZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" 0 0 -21.678889"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotate" " -type \"double3\" 0 0 -21.678889"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotate" " -type \"double3\" 0 0 -21.678889"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotate" " -type \"double3\" -4.457173 2.367031 8.413501"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateOrder" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotate" " -type \"double3\" 0 0 13.860833"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 0 0 13.860833"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"caching" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"isHistoricallyInteresting" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"nodeState" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"isCollapsed" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"blackBox" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"iconName" " -type \"string\" \"\""
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"viewMode" " 2"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"templateVersion" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"uiTreatment" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"intermediateObject" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"template" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghosting" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"useObjectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"objectColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"overrideDisplayType" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"overrideLevelOfDetail" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"overrideShading" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"overrideTexturing" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"overridePlayback" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"overrideEnabled" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"overrideVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"overrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"lodVisibility" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"identification" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"layerRenderable" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"layerOverrideColor" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghostingControl" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghostPreSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghostPostSteps" " 3"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghostStepSize" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghostColorPreA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghostColorPre" " -type \"float3\" 0.447 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghostColorPostA" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghostColorPost" " -type \"float3\" 0.878 0.678 0.663"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghostRangeStart" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"ghostRangeEnd" " 125"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotate" " -type \"double3\" 0 0 13.860833"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateOrder" " 5"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"shear" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotatePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotatePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"scalePivot" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"scalePivotTranslate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateAxis" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minTransLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxTransLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxTransXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxTransYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxTransZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minRotLimit" " -type \"double3\" -45 -45 -45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxRotLimit" " -type \"double3\" 45 45 45"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxRotXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxRotYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxRotZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minScaleLimit" " -type \"double3\" -1 -1 -1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxScaleLimit" " -type \"double3\" 1 1 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"minScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxScaleXLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxScaleYLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"maxScaleZLimitEnable" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"selectHandle" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"inheritsTransform" " 1"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"displayHandle" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"displayScalePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"displayRotatePivot" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"displayLocalAxis" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"showManipDefault" " 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L" 
		"rotateQuaternion" " -type \"double4\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R" 
		"rotate" " -type \"double3\" -45 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L" 
		"rotate" " -type \"double3\" 45 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotate" " -type \"double3\" 3.215702 -6.297736 2.836222"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotate" " -type \"double3\" 3.215702 -6.297736 2.836222"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotate" " -type \"double3\" -0.250307 18.211086 -3.705674"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M|rigw:rig:IKXSpineHandle_M" 
		"translate" " -type \"double3\" -2.24645e-08 0.00443195 -0.000534086"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M|rigw:rig:IKXSpineHandle_M" 
		"rotate" " -type \"double3\" 0.190416 -1.07353e-05 -1.6939e-06"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleY" "rigwRN.placeHolderList[1]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleX" "rigwRN.placeHolderList[2]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.scaleZ" "rigwRN.placeHolderList[3]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateX" "rigwRN.placeHolderList[4]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateY" "rigwRN.placeHolderList[5]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.translateZ" "rigwRN.placeHolderList[6]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateX" "rigwRN.placeHolderList[7]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateY" "rigwRN.placeHolderList[8]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.rotateZ" "rigwRN.placeHolderList[9]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main.visibility" "rigwRN.placeHolderList[10]" 
		""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateX" 
		"rigwRN.placeHolderList[11]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateY" 
		"rigwRN.placeHolderList[12]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R.rotateZ" 
		"rigwRN.placeHolderList[13]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[14]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[15]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[16]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateX" 
		"rigwRN.placeHolderList[17]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateY" 
		"rigwRN.placeHolderList[18]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateZ" 
		"rigwRN.placeHolderList[19]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateX" 
		"rigwRN.placeHolderList[20]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateY" 
		"rigwRN.placeHolderList[21]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_R|rigw:rig:FKExtraClavicle_R|rigw:rig:FKClavicle_R|rigw:rig:FKXClavicle_R|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetWrist_R|rigw:rig:FKExtraWrist_R|rigw:rig:FKWrist_R.rotateZ" 
		"rigwRN.placeHolderList[22]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateX" 
		"rigwRN.placeHolderList[23]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateY" 
		"rigwRN.placeHolderList[24]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateZ" 
		"rigwRN.placeHolderList[25]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[26]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateX" 
		"rigwRN.placeHolderList[27]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateY" 
		"rigwRN.placeHolderList[28]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToChest_M|rigw:rig:FKOffsetClavicle_L|rigw:rig:FKExtraClavicle_L|rigw:rig:FKClavicle_L|rigw:rig:FKXClavicle_L|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetWrist_L|rigw:rig:FKExtraWrist_L|rigw:rig:FKWrist_L.rotateZ" 
		"rigwRN.placeHolderList[29]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateX" 
		"rigwRN.placeHolderList[30]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateY" 
		"rigwRN.placeHolderList[31]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R.rotateZ" 
		"rigwRN.placeHolderList[32]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R.rotateX" 
		"rigwRN.placeHolderList[33]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R.rotateY" 
		"rigwRN.placeHolderList[34]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R.rotateZ" 
		"rigwRN.placeHolderList[35]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R.rotateX" 
		"rigwRN.placeHolderList[36]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R.rotateY" 
		"rigwRN.placeHolderList[37]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R.rotateZ" 
		"rigwRN.placeHolderList[38]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R|rigw:rig:FKXKnee_R|rigw:rig:FKOffsetAnkle_R|rigw:rig:FKExtraAnkle_R|rigw:rig:FKAnkle_R.rotateX" 
		"rigwRN.placeHolderList[39]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R|rigw:rig:FKXKnee_R|rigw:rig:FKOffsetAnkle_R|rigw:rig:FKExtraAnkle_R|rigw:rig:FKAnkle_R.rotateY" 
		"rigwRN.placeHolderList[40]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_R|rigw:rig:FKExtraHipTwist_R|rigw:rig:FKHipTwist_R|rigw:rig:FKXHipTwist_R|rigw:rig:FKOffsetHip_R|rigw:rig:FKExtraHip_R|rigw:rig:FKHip_R|rigw:rig:FKXHip_R|rigw:rig:FKOffsetKnee_R|rigw:rig:FKExtraKnee_R|rigw:rig:FKKnee_R|rigw:rig:FKXKnee_R|rigw:rig:FKOffsetAnkle_R|rigw:rig:FKExtraAnkle_R|rigw:rig:FKAnkle_R.rotateZ" 
		"rigwRN.placeHolderList[41]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateX" 
		"rigwRN.placeHolderList[42]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateY" 
		"rigwRN.placeHolderList[43]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L.rotateZ" 
		"rigwRN.placeHolderList[44]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L.rotateX" 
		"rigwRN.placeHolderList[45]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L.rotateY" 
		"rigwRN.placeHolderList[46]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L.rotateZ" 
		"rigwRN.placeHolderList[47]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L.rotateX" 
		"rigwRN.placeHolderList[48]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L.rotateY" 
		"rigwRN.placeHolderList[49]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L.rotateZ" 
		"rigwRN.placeHolderList[50]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L|rigw:rig:FKXKnee_L|rigw:rig:FKOffsetAnkle_L|rigw:rig:FKExtraAnkle_L|rigw:rig:FKAnkle_L.rotateX" 
		"rigwRN.placeHolderList[51]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L|rigw:rig:FKXKnee_L|rigw:rig:FKOffsetAnkle_L|rigw:rig:FKExtraAnkle_L|rigw:rig:FKAnkle_L.rotateY" 
		"rigwRN.placeHolderList[52]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToPelvis_M|rigw:rig:FKOffsetHipTwist_L|rigw:rig:FKExtraHipTwist_L|rigw:rig:FKHipTwist_L|rigw:rig:FKXHipTwist_L|rigw:rig:FKOffsetHip_L|rigw:rig:FKExtraHip_L|rigw:rig:FKHip_L|rigw:rig:FKXHip_L|rigw:rig:FKOffsetKnee_L|rigw:rig:FKExtraKnee_L|rigw:rig:FKKnee_L|rigw:rig:FKXKnee_L|rigw:rig:FKOffsetAnkle_L|rigw:rig:FKExtraAnkle_L|rigw:rig:FKAnkle_L.rotateZ" 
		"rigwRN.placeHolderList[53]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateX" 
		"rigwRN.placeHolderList[54]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateY" 
		"rigwRN.placeHolderList[55]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[56]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateX" 
		"rigwRN.placeHolderList[57]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateY" 
		"rigwRN.placeHolderList[58]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetMiddleFinger1_L|rigw:rig:FKExtraMiddleFinger1_L|rigw:rig:FKMiddleFinger1_L|rigw:rig:FKXMiddleFinger1_L|rigw:rig:FKOffsetMiddleFinger2_L|rigw:rig:FKExtraMiddleFinger2_L|rigw:rig:FKMiddleFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[59]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateX" 
		"rigwRN.placeHolderList[60]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateY" 
		"rigwRN.placeHolderList[61]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L.rotateZ" 
		"rigwRN.placeHolderList[62]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateX" 
		"rigwRN.placeHolderList[63]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateY" 
		"rigwRN.placeHolderList[64]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:FKParentConstraintToWrist_L|rigw:rig:FKOffsetThumbFinger1_L|rigw:rig:FKExtraThumbFinger1_L|rigw:rig:FKThumbFinger1_L|rigw:rig:FKXThumbFinger1_L|rigw:rig:FKOffsetThumbFinger2_L|rigw:rig:FKExtraThumbFinger2_L|rigw:rig:FKThumbFinger2_L.rotateZ" 
		"rigwRN.placeHolderList[65]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[66]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[67]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[68]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[69]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[70]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[71]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[72]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateX" 
		"rigwRN.placeHolderList[73]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateY" 
		"rigwRN.placeHolderList[74]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M.rotateZ" 
		"rigwRN.placeHolderList[75]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateX" 
		"rigwRN.placeHolderList[76]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateY" 
		"rigwRN.placeHolderList[77]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:HipSwingerGroupPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:HipSwingerStabalizePelvis_M|rigw:rig:FKOffsetSpineA_M|rigw:rig:FKExtraSpineA_M|rigw:rig:FKSpineA_M|rigw:rig:FKXSpineA_M|rigw:rig:FKOffsetChest_M|rigw:rig:FKExtraChest_M|rigw:rig:FKChest_M.rotateZ" 
		"rigwRN.placeHolderList[78]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateX" 
		"rigwRN.placeHolderList[79]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateY" 
		"rigwRN.placeHolderList[80]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:HipSwingerOffsetPelvis_M|rigw:rig:HipSwingerPelvis_M.rotateZ" 
		"rigwRN.placeHolderList[81]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[82]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[83]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[84]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[85]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[86]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[87]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[88]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rollAngle" 
		"rigwRN.placeHolderList[89]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.roll" 
		"rigwRN.placeHolderList[90]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.toe" 
		"rigwRN.placeHolderList[91]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[92]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[93]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[94]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[95]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateX" 
		"rigwRN.placeHolderList[96]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateY" 
		"rigwRN.placeHolderList[97]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R.rotateZ" 
		"rigwRN.placeHolderList[98]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R|rigw:rig:IKFootRollLeg_R|rigw:rig:IKRollLegHeel_R|rigw:rig:IKExtraLegHeel_R|rigw:rig:IKLegHeel_R|rigw:rig:IKRollLegBall_R|rigw:rig:IKExtraLegBall_R|rigw:rig:IKLegBall_R.rotateX" 
		"rigwRN.placeHolderList[99]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateX" 
		"rigwRN.placeHolderList[100]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateY" 
		"rigwRN.placeHolderList[101]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.translateZ" 
		"rigwRN.placeHolderList[102]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_R|rigw:rig:PoleExtraLeg_R|rigw:rig:PoleLeg_R.follow" 
		"rigwRN.placeHolderList[103]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine0_M|rigw:rig:IKExtraSpine0_M|rigw:rig:IKSpine0_M.translateX" 
		"rigwRN.placeHolderList[104]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine0_M|rigw:rig:IKExtraSpine0_M|rigw:rig:IKSpine0_M.translateY" 
		"rigwRN.placeHolderList[105]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine0_M|rigw:rig:IKExtraSpine0_M|rigw:rig:IKSpine0_M.translateZ" 
		"rigwRN.placeHolderList[106]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine0_M|rigw:rig:IKExtraSpine0_M|rigw:rig:IKSpine0_M.rotateX" 
		"rigwRN.placeHolderList[107]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine0_M|rigw:rig:IKExtraSpine0_M|rigw:rig:IKSpine0_M.rotateY" 
		"rigwRN.placeHolderList[108]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine0_M|rigw:rig:IKExtraSpine0_M|rigw:rig:IKSpine0_M.rotateZ" 
		"rigwRN.placeHolderList[109]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine0_M|rigw:rig:IKExtraSpine0_M|rigw:rig:IKSpine0_M.stiff" 
		"rigwRN.placeHolderList[110]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine2_M|rigw:rig:IKExtraSpine2_M|rigw:rig:IKSpine2_M.stiff" 
		"rigwRN.placeHolderList[111]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine2_M|rigw:rig:IKExtraSpine2_M|rigw:rig:IKSpine2_M.translateX" 
		"rigwRN.placeHolderList[112]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine2_M|rigw:rig:IKExtraSpine2_M|rigw:rig:IKSpine2_M.translateY" 
		"rigwRN.placeHolderList[113]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine2_M|rigw:rig:IKExtraSpine2_M|rigw:rig:IKSpine2_M.translateZ" 
		"rigwRN.placeHolderList[114]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine2_M|rigw:rig:IKExtraSpine2_M|rigw:rig:IKSpine2_M.rotateX" 
		"rigwRN.placeHolderList[115]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine2_M|rigw:rig:IKExtraSpine2_M|rigw:rig:IKSpine2_M.rotateY" 
		"rigwRN.placeHolderList[116]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine2_M|rigw:rig:IKExtraSpine2_M|rigw:rig:IKSpine2_M.rotateZ" 
		"rigwRN.placeHolderList[117]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M.translateX" 
		"rigwRN.placeHolderList[118]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M.translateY" 
		"rigwRN.placeHolderList[119]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M.translateZ" 
		"rigwRN.placeHolderList[120]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M.stiff" 
		"rigwRN.placeHolderList[121]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M.stretchy" 
		"rigwRN.placeHolderList[122]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M.rotateX" 
		"rigwRN.placeHolderList[123]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M.rotateY" 
		"rigwRN.placeHolderList[124]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintSpine4_M|rigw:rig:IKExtraSpine4_M|rigw:rig:IKSpine4_M.rotateZ" 
		"rigwRN.placeHolderList[125]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.rotateX" 
		"rigwRN.placeHolderList[126]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.rotateY" 
		"rigwRN.placeHolderList[127]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.rotateZ" 
		"rigwRN.placeHolderList[128]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.translateX" 
		"rigwRN.placeHolderList[129]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.translateY" 
		"rigwRN.placeHolderList[130]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.translateZ" 
		"rigwRN.placeHolderList[131]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintArm_L|rigw:rig:IKExtraArm_L|rigw:rig:IKArm_L.follow" 
		"rigwRN.placeHolderList[132]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L.translateX" 
		"rigwRN.placeHolderList[133]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L.translateY" 
		"rigwRN.placeHolderList[134]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L.translateZ" 
		"rigwRN.placeHolderList[135]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintArm_L|rigw:rig:PoleExtraArm_L|rigw:rig:PoleArm_L.follow" 
		"rigwRN.placeHolderList[136]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[137]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[138]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[139]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[140]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[141]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[142]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[143]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rollAngle" 
		"rigwRN.placeHolderList[144]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.roll" 
		"rigwRN.placeHolderList[145]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.toe" 
		"rigwRN.placeHolderList[146]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[147]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[148]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[149]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[150]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateX" 
		"rigwRN.placeHolderList[151]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateY" 
		"rigwRN.placeHolderList[152]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L.rotateZ" 
		"rigwRN.placeHolderList[153]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L|rigw:rig:IKFootRollLeg_L|rigw:rig:IKRollLegHeel_L|rigw:rig:IKExtraLegHeel_L|rigw:rig:IKLegHeel_L|rigw:rig:IKRollLegBall_L|rigw:rig:IKExtraLegBall_L|rigw:rig:IKLegBall_L.rotateX" 
		"rigwRN.placeHolderList[154]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateX" 
		"rigwRN.placeHolderList[155]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateY" 
		"rigwRN.placeHolderList[156]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.translateZ" 
		"rigwRN.placeHolderList[157]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:PoleParentConstraintLeg_L|rigw:rig:PoleExtraLeg_L|rigw:rig:PoleLeg_L.follow" 
		"rigwRN.placeHolderList[158]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKIKBlend" 
		"rigwRN.placeHolderList[159]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.IKVis" 
		"rigwRN.placeHolderList[160]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_R|rigw:rig:FKIKLeg_R.FKVis" 
		"rigwRN.placeHolderList[161]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.FKIKBlend" 
		"rigwRN.placeHolderList[162]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.IKVis" 
		"rigwRN.placeHolderList[163]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintSpine_M|rigw:rig:FKIKSpine_M.FKVis" 
		"rigwRN.placeHolderList[164]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.FKIKBlend" 
		"rigwRN.placeHolderList[165]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.IKVis" 
		"rigwRN.placeHolderList[166]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintArm_L|rigw:rig:FKIKArm_L.FKVis" 
		"rigwRN.placeHolderList[167]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKIKBlend" 
		"rigwRN.placeHolderList[168]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.IKVis" 
		"rigwRN.placeHolderList[169]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKIKSystem|rigw:rig:FKIKParentConstraintLeg_L|rigw:rig:FKIKLeg_L.FKVis" 
		"rigwRN.placeHolderList[170]" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".fil" 2;
	setAttr ".filw" 0.83333331346511841;
	setAttr ".filh" 0.83333331346511841;
	setAttr ".maxr" 2;
	setAttr ".gi" yes;
	setAttr ".fg" yes;
	setAttr ".cm" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".cs" 3;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 6 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode animCurveTL -n "rigw1:rig:FKChest_M_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKChest_M_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKChest_M_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKHipTwist_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKHipTwist_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKHipTwist_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKHipTwist_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKHipTwist_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 7.153105;
createNode animCurveTA -n "rigw1:rig:FKHipTwist_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw1:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKSpineA_M_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKSpineA_M_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw1:rig:FKSpineA_M_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw1:rig:IKLeg_L_toe";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw1:rig:IKLeg_R_toe";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 6 0;
	setAttr -s 3 ".kit[1:2]"  1 18;
	setAttr -s 3 ".kot[1:2]"  1 18;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 6 0;
	setAttr -s 3 ".kit[1:2]"  1 18;
	setAttr -s 3 ".kot[1:2]"  1 18;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 6 0;
	setAttr -s 3 ".kit[1:2]"  1 18;
	setAttr -s 3 ".kot[1:2]"  1 18;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0.74553779600987835 2 0 3 -0.0080815132263589162
		 4 1.0965303390871199 5 0 6 -0.056279043132118817;
	setAttr -s 6 ".kit[0:5]"  2 18 18 1 18 2;
	setAttr -s 6 ".kot[0:5]"  2 18 18 1 18 2;
	setAttr -s 6 ".kix[3:5]"  1 0.19369015097618103 0.50960761308670044;
	setAttr -s 6 ".kiy[3:5]"  0 -0.98106271028518677 -0.86040687561035156;
	setAttr -s 6 ".kox[3:5]"  1 0.19369015097618103 1;
	setAttr -s 6 ".koy[3:5]"  0 -0.98106271028518677 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 6 0;
	setAttr -s 3 ".kit[1:2]"  1 18;
	setAttr -s 3 ".kot[1:2]"  1 18;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 6 0;
	setAttr -s 3 ".kit[1:2]"  1 18;
	setAttr -s 3 ".kot[1:2]"  1 18;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 6 0;
	setAttr -s 3 ".kit[1:2]"  1 18;
	setAttr -s 3 ".kot[1:2]"  1 18;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -6.3707948286980933 2 43.816585482302528
		 4 0 5 1.5449998833759206 6 -25.616003708458347;
	setAttr -s 5 ".kit[2:4]"  1 1 18;
	setAttr -s 5 ".kot[2:4]"  1 1 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 -0.76051075188464223 3 -21.361787327893669
		 4 -4.7993545891371694 5 28.262214353790892 6 18.041609548208008;
	setAttr -s 6 ".kit[3:5]"  1 1 18;
	setAttr -s 6 ".kot[3:5]"  1 1 18;
	setAttr -s 6 ".kix[3:5]"  1 1 1;
	setAttr -s 6 ".kiy[3:5]"  0 0 0;
	setAttr -s 6 ".kox[3:5]"  1 1 1;
	setAttr -s 6 ".koy[3:5]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_R_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKAnkle_R_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.2157016730851029 4 4.2872517196467896
		 6 3.3106571070908286;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -6.2977355030573641 4 6.4567286107310151
		 6 -3.7724359562500682;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKChest_M_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.8362216625954426 4 -4.1398202069888619
		 6 1.995591968234796;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 -4.4571725877246751 2 -6.1174287831103573
		 4 -2.1374214629999999 5 -6.1174287831103573 6 -6.1174287830000003;
	setAttr -s 5 ".kit[2:4]"  1 1 18;
	setAttr -s 5 ".kot[2:4]"  1 1 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 2.3670305599030814 2 0 4 13.80707602 5 0
		 6 0;
	setAttr -s 5 ".kit[2:4]"  1 1 18;
	setAttr -s 5 ".kot[2:4]"  1 1 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKClavicle_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 8.4135014554631784 2 5.1682142790306997
		 4 11.55589436 5 5.1682142790306997 6 5.1682142789999999;
	setAttr -s 5 ".kit[2:4]"  1 1 18;
	setAttr -s 5 ".kot[2:4]"  1 1 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 13.860832683757463 4 -25.240266543943545
		 6 3.5903797588967188;
	setAttr -s 3 ".kit[2]"  1;
	setAttr -s 3 ".kot[2]"  1;
	setAttr -s 3 ".kix[2]"  1;
	setAttr -s 3 ".kiy[2]"  0;
	setAttr -s 3 ".kox[2]"  1;
	setAttr -s 3 ".koy[2]"  0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 7.153105;
createNode animCurveTA -n "rigw:rig:FKHipTwist_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKHip_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 45 2 14.904919479467978 3 -29.749421455765034
		 4 -45 5 -4.0269398286876825 6 13.376017447016419;
	setAttr -s 6 ".kit[3:5]"  1 1 1;
	setAttr -s 6 ".kot[3:5]"  1 1 1;
	setAttr -s 6 ".kix[3:5]"  1 0.058663692325353622 0.058663696050643921;
	setAttr -s 6 ".kiy[3:5]"  0 -0.99827778339385986 -0.99827778339385986;
	setAttr -s 6 ".kox[3:5]"  1 0.058663692325353622 0.058663699775934219;
	setAttr -s 6 ".koy[3:5]"  0 -0.99827778339385986 -0.99827778339385986;
createNode animCurveTA -n "rigw:rig:FKHip_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 3.618498967259161 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKHip_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 3.4872805855453981 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKHip_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 -45 2 5.2033167810171053 3 20 4 45 5 27.14921477577845
		 6 -20;
	setAttr -s 6 ".kit[3:5]"  1 1 1;
	setAttr -s 6 ".kot[3:5]"  1 1 1;
	setAttr -s 6 ".kix[3:5]"  1 0.058663692325353622 0.058663696050643921;
	setAttr -s 6 ".kiy[3:5]"  0 0.99827778339385986 0.99827778339385986;
	setAttr -s 6 ".kox[3:5]"  1 0.058663692325353622 0.058663699775934219;
	setAttr -s 6 ".koy[3:5]"  0 0.99827778339385986 0.99827778339385986;
createNode animCurveTA -n "rigw:rig:FKHip_R_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKHip_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 5.9832818895258937 3 -2.5050302311797323
		 4 0 5 0 6 0;
	setAttr -s 6 ".kit[3:5]"  1 1 18;
	setAttr -s 6 ".kot[3:5]"  1 1 18;
	setAttr -s 6 ".kix[3:5]"  1 1 1;
	setAttr -s 6 ".kiy[3:5]"  0 0 0;
	setAttr -s 6 ".kox[3:5]"  1 1 1;
	setAttr -s 6 ".koy[3:5]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_FKIKBlend";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  9 1 1 9;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKArm_L_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 4 1 5 1 6 1;
	setAttr -s 4 ".kit[0:3]"  9 1 1 9;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKIKBlend";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 4 1 5 1 6 1;
	setAttr -s 4 ".kit[0:3]"  9 1 1 9;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_L_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  9 1 1 9;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKIKBlend";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 4 1 5 1 6 1;
	setAttr -s 4 ".kit[0:3]"  9 1 1 9;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKLeg_R_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  9 1 1 9;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_FKIKBlend";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_IKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  9 1 1 9;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:FKIKSpine_M_FKVis";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 4 1 5 1 6 1;
	setAttr -s 4 ".kit[0:3]"  9 1 1 9;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKKnee_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 41.576883796932009 2 68.880729574168143
		 4 0 5 8.0514769436494209 6 20.093103057220823;
	setAttr -s 5 ".kit[2:4]"  1 1 18;
	setAttr -s 5 ".kot[2:4]"  1 1 18;
	setAttr -s 5 ".kix[2:4]"  1 1 1;
	setAttr -s 5 ".kiy[2:4]"  0 0 0;
	setAttr -s 5 ".kox[2:4]"  1 1 1;
	setAttr -s 5 ".koy[2:4]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKKnee_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKKnee_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKKnee_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  1 0 2 6.1819643509625122 3 15.308292469088455
		 4 40.562760181154445 5 65.930571237903536 6 35.735522546620963;
	setAttr -s 6 ".kit[3:5]"  1 1 18;
	setAttr -s 6 ".kot[3:5]"  1 1 18;
	setAttr -s 6 ".kix[3:5]"  1 1 1;
	setAttr -s 6 ".kiy[3:5]"  0 0 0;
	setAttr -s 6 ".kox[3:5]"  1 1 1;
	setAttr -s 6 ".koy[3:5]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKKnee_R_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 1.6170685977771058;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKKnee_R_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 1.4140506092847127;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger1_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 19.962698192013882 4 -22.625680860146911;
	setAttr -s 2 ".kit[0:1]"  18 1;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
	setAttr ".kox[0]"  1;
	setAttr ".koy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKMiddleFinger2_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
	setAttr ".kox[0]"  1;
	setAttr ".koy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 6 0;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[1:2]"  18 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 6 0;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[1:2]"  18 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 13.860832683757463 4 -25.240266543943545
		 6 3.5903797588967188;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[1:2]"  18 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 3.2157016730851029 4 4.2872517196467896
		 6 3.3106571070908286;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -6.2977355030573641 4 6.4567286107310151
		 6 -3.7724359562500682;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKSpineA_M_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 2.8362216625954426 4 -4.1398202069888619
		 6 1.995591968234796;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 19.962698192013882 4 -22.625680860146911;
	setAttr -s 2 ".kit[0:1]"  18 1;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
	setAttr ".kox[0]"  1;
	setAttr ".koy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger1_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
	setAttr ".kox[0]"  1;
	setAttr ".koy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 19.962698192013882 4 -22.625680860146911;
	setAttr -s 2 ".kit[0:1]"  18 1;
	setAttr -s 2 ".kot[1]"  18;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
	setAttr ".kox[0]"  1;
	setAttr ".koy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKThumbFinger2_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kix[0]"  1;
	setAttr ".kiy[0]"  0;
	setAttr ".kox[0]"  1;
	setAttr ".koy[0]"  0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 6 0;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 0 6 0;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:FKWrist_L_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 13.860832683757463 4 -25.240266543943545
		 6 3.5903797588967188;
	setAttr -s 3 ".kit[0:2]"  18 1 1;
	setAttr -s 3 ".kot[0:2]"  18 1 1;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -0.25030659478409062 4 -0.61307619634722788
		 6 -0.78165400007807262;
	setAttr -s 3 ".kit[1:2]"  1 18;
	setAttr -s 3 ".kot[1:2]"  1 18;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 18.211086461875972 4 -17.698745822269828
		 5 -3.8520000000000003 6 3.817827859772335;
	setAttr -s 4 ".kit[1:3]"  1 18 18;
	setAttr -s 4 ".kot[1:3]"  1 18 18;
	setAttr -s 4 ".kix[1:3]"  1 0.17479155957698822 1;
	setAttr -s 4 ".kiy[1:3]"  0 0.98460543155670166 0;
	setAttr -s 4 ".kox[1:3]"  1 0.17479155957698822 1;
	setAttr -s 4 ".koy[1:3]"  0 0.98460543155670166 0;
createNode animCurveTA -n "rigw:rig:HipSwingerPelvis_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -3.7056744253999097 4 3.530327872713666
		 6 -1.9277828523001592;
	setAttr -s 3 ".kit[1:2]"  1 18;
	setAttr -s 3 ".kot[1:2]"  1 18;
	setAttr -s 3 ".kix[1:2]"  1 1;
	setAttr -s 3 ".kiy[1:2]"  0 0;
	setAttr -s 3 ".kox[1:2]"  1 1;
	setAttr -s 3 ".koy[1:2]"  0 0;
createNode animCurveTA -n "rigw:rig:IKArm_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKArm_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKArm_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKArm_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKArm_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKArm_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKArm_L_follow";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLegBall_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLegBall_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLegHeel_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_toe";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_rollAngle";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 25;
createNode animCurveTU -n "rigw:rig:IKLeg_L_roll";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_toe";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_rollAngle";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 25;
createNode animCurveTU -n "rigw:rig:IKLeg_R_roll";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTL -n "rigw:rig:IKSpine0_M_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKSpine0_M_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKSpine0_M_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKSpine0_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKSpine0_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKSpine0_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKSpine0_M_stiff";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKSpine2_M_stiff";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKSpine2_M_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKSpine2_M_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKSpine2_M_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKSpine2_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKSpine2_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKSpine2_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKSpine4_M_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKSpine4_M_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKSpine4_M_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKSpine4_M_stiff";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKSpine4_M_stretchy";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKSpine4_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKSpine4_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKSpine4_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:Main_scaleY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.2 4 2.2 5 2.2 6 2.2;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:Main_scaleX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.2 4 2.2 5 2.2 6 2.2;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:Main_scaleZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 2.2 4 2.2 5 2.2 6 2.2;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTL -n "rigw:rig:Main_translateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTL -n "rigw:rig:Main_translateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTL -n "rigw:rig:Main_translateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:Main_rotateX";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:Main_rotateY";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTA -n "rigw:rig:Main_rotateZ";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 0 4 0 5 0 6 0;
	setAttr -s 4 ".kit[0:3]"  18 1 1 18;
	setAttr -s 4 ".kot[0:3]"  18 1 1 18;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
	setAttr -s 4 ".kox[1:3]"  1 1 1;
	setAttr -s 4 ".koy[1:3]"  0 0 0;
createNode animCurveTU -n "rigw:rig:Main_visibility";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  1 1 4 1 5 1 6 1;
	setAttr -s 4 ".kit[0:3]"  9 1 1 9;
	setAttr -s 4 ".kix[1:3]"  1 1 1;
	setAttr -s 4 ".kiy[1:3]"  0 0 0;
createNode animCurveTL -n "rigw:rig:PoleArm_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:PoleArm_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:PoleArm_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:PoleArm_L_follow";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_L_follow";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 10;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:PoleLeg_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:PoleLeg_R_follow";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 10;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -21.678889038670199 4 12.122434085421366
		 6 -6.1214567086884744;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -21.678889038670199 4 12.122434085421366
		 6 -6.1214567086884744;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:FKWrist_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 -21.678889038670199 4 12.122434085421366
		 6 -6.1214567086884744;
createNode reference -n "sharedReferenceNode";
	setAttr ".ed" -type "dataReferenceEdits" 
		"sharedReferenceNode";
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 11 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 12 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 16 ".tx";
select -ne :lightList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 28 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren" -type "string" "mentalRay";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 32;
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar" 0;
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn" no;
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff" yes;
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultRenderQuality;
	setAttr ".ert" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w" 512;
	setAttr -av ".h" 512;
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al" yes;
	setAttr -av ".dar" 1;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 3 ".sol";
connectAttr "rigw:rig:Main_scaleY.o" "rigwRN.phl[1]";
connectAttr "rigw:rig:Main_scaleX.o" "rigwRN.phl[2]";
connectAttr "rigw:rig:Main_scaleZ.o" "rigwRN.phl[3]";
connectAttr "rigw:rig:Main_translateX.o" "rigwRN.phl[4]";
connectAttr "rigw:rig:Main_translateY.o" "rigwRN.phl[5]";
connectAttr "rigw:rig:Main_translateZ.o" "rigwRN.phl[6]";
connectAttr "rigw:rig:Main_rotateX.o" "rigwRN.phl[7]";
connectAttr "rigw:rig:Main_rotateY.o" "rigwRN.phl[8]";
connectAttr "rigw:rig:Main_rotateZ.o" "rigwRN.phl[9]";
connectAttr "rigw:rig:Main_visibility.o" "rigwRN.phl[10]";
connectAttr "rigw:rig:FKClavicle_R_rotateX.o" "rigwRN.phl[11]";
connectAttr "rigw:rig:FKClavicle_R_rotateY.o" "rigwRN.phl[12]";
connectAttr "rigw:rig:FKClavicle_R_rotateZ.o" "rigwRN.phl[13]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[14]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[15]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[16]";
connectAttr "rigw:rig:FKElbow_R_rotateX.o" "rigwRN.phl[17]";
connectAttr "rigw:rig:FKElbow_R_rotateY.o" "rigwRN.phl[18]";
connectAttr "rigw:rig:FKElbow_R_rotateZ.o" "rigwRN.phl[19]";
connectAttr "rigw:rig:FKWrist_R_rotateX.o" "rigwRN.phl[20]";
connectAttr "rigw:rig:FKWrist_R_rotateY.o" "rigwRN.phl[21]";
connectAttr "rigw:rig:FKWrist_R_rotateZ.o" "rigwRN.phl[22]";
connectAttr "rigw:rig:FKShoulder_L_rotateX.o" "rigwRN.phl[23]";
connectAttr "rigw:rig:FKShoulder_L_rotateY.o" "rigwRN.phl[24]";
connectAttr "rigw:rig:FKShoulder_L_rotateZ.o" "rigwRN.phl[25]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[26]";
connectAttr "rigw:rig:FKWrist_L_rotateX.o" "rigwRN.phl[27]";
connectAttr "rigw:rig:FKWrist_L_rotateY.o" "rigwRN.phl[28]";
connectAttr "rigw:rig:FKWrist_L_rotateZ.o" "rigwRN.phl[29]";
connectAttr "rigw:rig:FKHipTwist_R_rotateX.o" "rigwRN.phl[30]";
connectAttr "rigw:rig:FKHipTwist_R_rotateY.o" "rigwRN.phl[31]";
connectAttr "rigw:rig:FKHipTwist_R_rotateZ.o" "rigwRN.phl[32]";
connectAttr "rigw:rig:FKHip_R_rotateX.o" "rigwRN.phl[33]";
connectAttr "rigw:rig:FKHip_R_rotateY.o" "rigwRN.phl[34]";
connectAttr "rigw:rig:FKHip_R_rotateZ.o" "rigwRN.phl[35]";
connectAttr "rigw:rig:FKKnee_R_rotateX.o" "rigwRN.phl[36]";
connectAttr "rigw:rig:FKKnee_R_rotateY.o" "rigwRN.phl[37]";
connectAttr "rigw:rig:FKKnee_R_rotateZ.o" "rigwRN.phl[38]";
connectAttr "rigw:rig:FKAnkle_R_rotateX.o" "rigwRN.phl[39]";
connectAttr "rigw:rig:FKAnkle_R_rotateY.o" "rigwRN.phl[40]";
connectAttr "rigw:rig:FKAnkle_R_rotateZ.o" "rigwRN.phl[41]";
connectAttr "rigw:rig:FKHipTwist_L_rotateX.o" "rigwRN.phl[42]";
connectAttr "rigw:rig:FKHipTwist_L_rotateY.o" "rigwRN.phl[43]";
connectAttr "rigw:rig:FKHipTwist_L_rotateZ.o" "rigwRN.phl[44]";
connectAttr "rigw:rig:FKHip_L_rotateX.o" "rigwRN.phl[45]";
connectAttr "rigw:rig:FKHip_L_rotateY.o" "rigwRN.phl[46]";
connectAttr "rigw:rig:FKHip_L_rotateZ.o" "rigwRN.phl[47]";
connectAttr "rigw:rig:FKKnee_L_rotateX.o" "rigwRN.phl[48]";
connectAttr "rigw:rig:FKKnee_L_rotateY.o" "rigwRN.phl[49]";
connectAttr "rigw:rig:FKKnee_L_rotateZ.o" "rigwRN.phl[50]";
connectAttr "rigw:rig:FKAnkle_L_rotateX.o" "rigwRN.phl[51]";
connectAttr "rigw:rig:FKAnkle_L_rotateY.o" "rigwRN.phl[52]";
connectAttr "rigw:rig:FKAnkle_L_rotateZ.o" "rigwRN.phl[53]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateX.o" "rigwRN.phl[54]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateY.o" "rigwRN.phl[55]";
connectAttr "rigw:rig:FKMiddleFinger1_L_rotateZ.o" "rigwRN.phl[56]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateX.o" "rigwRN.phl[57]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateY.o" "rigwRN.phl[58]";
connectAttr "rigw:rig:FKMiddleFinger2_L_rotateZ.o" "rigwRN.phl[59]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateX.o" "rigwRN.phl[60]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateY.o" "rigwRN.phl[61]";
connectAttr "rigw:rig:FKThumbFinger1_L_rotateZ.o" "rigwRN.phl[62]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateX.o" "rigwRN.phl[63]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateY.o" "rigwRN.phl[64]";
connectAttr "rigw:rig:FKThumbFinger2_L_rotateZ.o" "rigwRN.phl[65]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[66]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[67]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[68]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[69]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[70]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[71]";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[72]";
connectAttr "rigw:rig:FKSpineA_M_rotateX.o" "rigwRN.phl[73]";
connectAttr "rigw:rig:FKSpineA_M_rotateY.o" "rigwRN.phl[74]";
connectAttr "rigw:rig:FKSpineA_M_rotateZ.o" "rigwRN.phl[75]";
connectAttr "rigw:rig:FKChest_M_rotateX.o" "rigwRN.phl[76]";
connectAttr "rigw:rig:FKChest_M_rotateY.o" "rigwRN.phl[77]";
connectAttr "rigw:rig:FKChest_M_rotateZ.o" "rigwRN.phl[78]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateX.o" "rigwRN.phl[79]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateY.o" "rigwRN.phl[80]";
connectAttr "rigw:rig:HipSwingerPelvis_M_rotateZ.o" "rigwRN.phl[81]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[82]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[83]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[84]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[85]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[86]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[87]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[88]";
connectAttr "rigw:rig:IKLeg_R_rollAngle.o" "rigwRN.phl[89]";
connectAttr "rigw:rig:IKLeg_R_roll.o" "rigwRN.phl[90]";
connectAttr "rigw:rig:IKLeg_R_toe.o" "rigwRN.phl[91]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[92]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[93]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[94]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[95]";
connectAttr "rigw:rig:IKLegHeel_R_rotateX.o" "rigwRN.phl[96]";
connectAttr "rigw:rig:IKLegHeel_R_rotateY.o" "rigwRN.phl[97]";
connectAttr "rigw:rig:IKLegHeel_R_rotateZ.o" "rigwRN.phl[98]";
connectAttr "rigw:rig:IKLegBall_R_rotateX.o" "rigwRN.phl[99]";
connectAttr "rigw:rig:PoleLeg_R_translateX.o" "rigwRN.phl[100]";
connectAttr "rigw:rig:PoleLeg_R_translateY.o" "rigwRN.phl[101]";
connectAttr "rigw:rig:PoleLeg_R_translateZ.o" "rigwRN.phl[102]";
connectAttr "rigw:rig:PoleLeg_R_follow.o" "rigwRN.phl[103]";
connectAttr "rigw:rig:IKSpine0_M_translateX.o" "rigwRN.phl[104]";
connectAttr "rigw:rig:IKSpine0_M_translateY.o" "rigwRN.phl[105]";
connectAttr "rigw:rig:IKSpine0_M_translateZ.o" "rigwRN.phl[106]";
connectAttr "rigw:rig:IKSpine0_M_rotateX.o" "rigwRN.phl[107]";
connectAttr "rigw:rig:IKSpine0_M_rotateY.o" "rigwRN.phl[108]";
connectAttr "rigw:rig:IKSpine0_M_rotateZ.o" "rigwRN.phl[109]";
connectAttr "rigw:rig:IKSpine0_M_stiff.o" "rigwRN.phl[110]";
connectAttr "rigw:rig:IKSpine2_M_stiff.o" "rigwRN.phl[111]";
connectAttr "rigw:rig:IKSpine2_M_translateX.o" "rigwRN.phl[112]";
connectAttr "rigw:rig:IKSpine2_M_translateY.o" "rigwRN.phl[113]";
connectAttr "rigw:rig:IKSpine2_M_translateZ.o" "rigwRN.phl[114]";
connectAttr "rigw:rig:IKSpine2_M_rotateX.o" "rigwRN.phl[115]";
connectAttr "rigw:rig:IKSpine2_M_rotateY.o" "rigwRN.phl[116]";
connectAttr "rigw:rig:IKSpine2_M_rotateZ.o" "rigwRN.phl[117]";
connectAttr "rigw:rig:IKSpine4_M_translateX.o" "rigwRN.phl[118]";
connectAttr "rigw:rig:IKSpine4_M_translateY.o" "rigwRN.phl[119]";
connectAttr "rigw:rig:IKSpine4_M_translateZ.o" "rigwRN.phl[120]";
connectAttr "rigw:rig:IKSpine4_M_stiff.o" "rigwRN.phl[121]";
connectAttr "rigw:rig:IKSpine4_M_stretchy.o" "rigwRN.phl[122]";
connectAttr "rigw:rig:IKSpine4_M_rotateX.o" "rigwRN.phl[123]";
connectAttr "rigw:rig:IKSpine4_M_rotateY.o" "rigwRN.phl[124]";
connectAttr "rigw:rig:IKSpine4_M_rotateZ.o" "rigwRN.phl[125]";
connectAttr "rigw:rig:IKArm_L_rotateX.o" "rigwRN.phl[126]";
connectAttr "rigw:rig:IKArm_L_rotateY.o" "rigwRN.phl[127]";
connectAttr "rigw:rig:IKArm_L_rotateZ.o" "rigwRN.phl[128]";
connectAttr "rigw:rig:IKArm_L_translateX.o" "rigwRN.phl[129]";
connectAttr "rigw:rig:IKArm_L_translateY.o" "rigwRN.phl[130]";
connectAttr "rigw:rig:IKArm_L_translateZ.o" "rigwRN.phl[131]";
connectAttr "rigw:rig:IKArm_L_follow.o" "rigwRN.phl[132]";
connectAttr "rigw:rig:PoleArm_L_translateX.o" "rigwRN.phl[133]";
connectAttr "rigw:rig:PoleArm_L_translateY.o" "rigwRN.phl[134]";
connectAttr "rigw:rig:PoleArm_L_translateZ.o" "rigwRN.phl[135]";
connectAttr "rigw:rig:PoleArm_L_follow.o" "rigwRN.phl[136]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[137]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[138]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[139]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[140]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[141]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[142]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[143]";
connectAttr "rigw:rig:IKLeg_L_rollAngle.o" "rigwRN.phl[144]";
connectAttr "rigw:rig:IKLeg_L_roll.o" "rigwRN.phl[145]";
connectAttr "rigw:rig:IKLeg_L_toe.o" "rigwRN.phl[146]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[147]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[148]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[149]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[150]";
connectAttr "rigw:rig:IKLegHeel_L_rotateX.o" "rigwRN.phl[151]";
connectAttr "rigw:rig:IKLegHeel_L_rotateY.o" "rigwRN.phl[152]";
connectAttr "rigw:rig:IKLegHeel_L_rotateZ.o" "rigwRN.phl[153]";
connectAttr "rigw:rig:IKLegBall_L_rotateX.o" "rigwRN.phl[154]";
connectAttr "rigw:rig:PoleLeg_L_translateX.o" "rigwRN.phl[155]";
connectAttr "rigw:rig:PoleLeg_L_translateY.o" "rigwRN.phl[156]";
connectAttr "rigw:rig:PoleLeg_L_translateZ.o" "rigwRN.phl[157]";
connectAttr "rigw:rig:PoleLeg_L_follow.o" "rigwRN.phl[158]";
connectAttr "rigw:rig:FKIKLeg_R_FKIKBlend.o" "rigwRN.phl[159]";
connectAttr "rigw:rig:FKIKLeg_R_IKVis.o" "rigwRN.phl[160]";
connectAttr "rigw:rig:FKIKLeg_R_FKVis.o" "rigwRN.phl[161]";
connectAttr "rigw:rig:FKIKSpine_M_FKIKBlend.o" "rigwRN.phl[162]";
connectAttr "rigw:rig:FKIKSpine_M_IKVis.o" "rigwRN.phl[163]";
connectAttr "rigw:rig:FKIKSpine_M_FKVis.o" "rigwRN.phl[164]";
connectAttr "rigw:rig:FKIKArm_L_FKIKBlend.o" "rigwRN.phl[165]";
connectAttr "rigw:rig:FKIKArm_L_IKVis.o" "rigwRN.phl[166]";
connectAttr "rigw:rig:FKIKArm_L_FKVis.o" "rigwRN.phl[167]";
connectAttr "rigw:rig:FKIKLeg_L_FKIKBlend.o" "rigwRN.phl[168]";
connectAttr "rigw:rig:FKIKLeg_L_IKVis.o" "rigwRN.phl[169]";
connectAttr "rigw:rig:FKIKLeg_L_FKVis.o" "rigwRN.phl[170]";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "sharedReferenceNode.sr" "rigwRN.sr";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Juggernaut3__mc-rigw@Jug3_run.ma
