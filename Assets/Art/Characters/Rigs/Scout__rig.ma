//Maya ASCII 2013 scene
//Name: Scout__rig.ma
//Last modified: Mon, May 12, 2014 12:43:41 PM
//Codeset: UTF-8
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -33.221139006290322 15.19486381971338 19.102777945183501 ;
	setAttr ".r" -type "double3" -12.338352726998986 656.19999999999447 -1.8009697161162979e-15 ;
	setAttr ".rp" -type "double3" -8.8817841970012523e-16 2.6645352591003757e-15 0 ;
	setAttr ".rpt" -type "double3" -1.2562355331908604e-15 -1.5417647434667315e-15 1.1925639982518871e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 38.552017240793816;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.3268859452274917 7.8217488306440428 2.5863431995646069 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -4.1361804461634772 100.1 -1.3734464006168321 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 27.29039406909008;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.901028054573354 5.1429571324361483 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 23.262695065792553;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0.46503179089690394 1.1394088250574752 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 23.798400774708881;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "ctrl_rig";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Main" -p "ctrl_rig";
	addAttr -ci true -sn "height" -ln "height" -at "double";
	addAttr -ci true -sn "fkVis" -ln "fkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "ikVis" -ln "ikVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fkIkVis" -ln "fkIkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimVis" -ln "aimVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "eyeVis" -ln "eyeVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fingerVis" -ln "fingerVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "bendVis" -ln "bendVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "arrowVis" -ln "arrowVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "jointVis" -ln "jointVis" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -l on ".height" 12.842671381712725;
	setAttr -cb on ".fkVis";
	setAttr -cb on ".ikVis";
	setAttr -cb on ".fkIkVis";
	setAttr -cb on ".aimVis";
	setAttr -cb on ".eyeVis";
	setAttr -cb on ".fingerVis";
	setAttr -cb on ".bendVis";
	setAttr -cb on ".arrowVis";
	setAttr -cb on ".jointVis";
createNode nurbsCurve -n "MainShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		3.644282767 -0.083987242239999996 -3.6525860790000002
		1.4492270940000001e-15 -0.083987242239999996 -5.162097427
		-3.644282767 -0.083987242239999996 -3.6525860790000002
		-5.1537941150000002 -0.083987242239999996 -0.0083033118089999997
		-3.644282767 -0.083987242239999996 3.6359794550000002
		4.8427462940000002e-16 -0.083987242239999996 5.1454908030000004
		3.644282767 -0.083987242239999996 3.6359794550000002
		5.1537941150000002 -0.083987242239999996 -0.0083033118089999997
		3.644282767 -0.083987242239999996 -3.6525860790000002
		1.4492270940000001e-15 -0.083987242239999996 -5.162097427
		-3.644282767 -0.083987242239999996 -3.6525860790000002
		;
createNode transform -n "BaseSkeleton" -p "Main";
	setAttr -l on ".v" no;
createNode transform -n "BaseSkeletonOffset" -p "BaseSkeleton";
createNode joint -n "Pelvis" -p "BaseSkeletonOffset";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 1.4031819443810043e-16 4.7513626008102703 -0.30196761248010473 ;
	setAttr ".r" -type "double3" 9.5763298435263593 0 0 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".dl" yes;
	setAttr ".typ" 1;
createNode joint -n "SpineA" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0 1.4130837058957333 0.018891738117306826 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Mid";
createNode joint -n "SpineB" -p "SpineA";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 4.9303806576313238e-32 0.71494231050174317 0.030376928917300816 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".otp" -type "string" "Mid";
createNode joint -n "Chest" -p "SpineB";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0.97106708107769091 -0.00031724266683363922 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Chest";
createNode joint -n "Head" -p "Chest";
	setAttr ".t" -type "double3" 0 3.3621280567969052 -0.05136910884368695 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "Head_End" -p "Head";
	setAttr ".t" -type "double3" 6.4094948549207209e-31 1.7440267880228095 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "Gun1" -p "Chest";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -2.350906237499248 -0.68096479514476815 0.40968214042113382 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -9.5763298435263629 0 0 ;
	setAttr ".dl" yes;
	setAttr ".sd" 3;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "Gun2" -p "Gun1";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.0017406567944728835 1.1650684250566521 0.081878483299648419 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "Gun3_End" -p "Gun2";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0.85323520097619676 -1.1102230246251563e-16 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "Shoulder1" -p "Chest";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 2.8186117422206967 0.59492979900123899 -0.34712698500417805 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -19.067487875850972 -1.3739223107677414 -135.91978996654333 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Shoulder";
createNode joint -n "Elbow1" -p "Shoulder1";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.0052683376394053738 2.6881635991971731 -0.063794163756092814 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 116.1997640970533 4.5150796727271558 0.22693078970869665 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "Wrist2" -p "Elbow1";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.2632519518955041 2.618887825676202 0.12313942754248219 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 9.9077476711942545 -37.967629035855033 -4.9544430117845222 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Hand";
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "Wrist1" -p "Wrist2";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.016234100558222053 0.66692055569756181 0.08319947863292311 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -1.5663486604252204 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Hand1";
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "MiddleFinger1" -p "Wrist1";
	setAttr ".t" -type "double3" -0.29152620454748313 0.84419957325886341 -0.16371647164843584 ;
	setAttr ".r" -type "double3" -18.27408367519886 6.6060868362829286 14.794837317493444 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -34.462082586865911 -8.7285733235282201 -1.7903981777634761 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "4";
createNode joint -n "MiddleFinger2" -p "MiddleFinger1";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0.77937696384104704 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -29.808792925358407 0.66965351907005877 1.1044222451673171 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "3";
createNode joint -n "MiddleFinger3_End" -p "MiddleFinger2";
	setAttr ".t" -type "double3" 2.6645352591003757e-15 0.75403114661343051 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 1.0813885916975958e-13 5.4988808672490367e-14 -1.930944564816641e-13 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "3";
createNode joint -n "IndexFinger1" -p "Wrist1";
	setAttr ".t" -type "double3" 0.4564735271965894 0.8360215040075929 -0.14399876771940257 ;
	setAttr ".r" -type "double3" -9.9917552374070979 11.317098515901023 -11.999448686173059 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0.065532877363568762 20.527688987272207 -2.5422327562497964 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "8";
createNode joint -n "IndexFinger2" -p "IndexFinger1";
	setAttr ".t" -type "double3" 0 0.77937696384104749 0 ;
	setAttr ".r" -type "double3" -29.808792925356347 0.66965351907016912 1.1044222451669812 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "7";
createNode joint -n "IndexFinger3_End" -p "IndexFinger2";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 0.7455622172995513 -1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" -1.8765272620634735e-13 3.9756933518293959e-13 -7.9513867036587922e-14 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "7";
createNode joint -n "CapeMid1" -p "Chest";
	setAttr ".t" -type "double3" 0.55933212460801518 0.42121368670569143 -2.3549250024825055 ;
	setAttr ".r" -type "double3" -153.18961080159258 -1.5855962108091453e-14 3.7789421682584257e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeMid2" -p "CapeMid1";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 1.6833042369258155 0 ;
	setAttr ".r" -type "double3" -15.968938142864413 0 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeMid3" -p "CapeMid2";
	setAttr ".t" -type "double3" 1.1102230246251565e-16 1.6177712472652495 0 ;
	setAttr ".r" -type "double3" -9.6661502587978578 3.470240232835771e-16 -4.1041842320128437e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeMid4" -p "CapeMid3";
	setAttr ".t" -type "double3" 0 1.5499083382539354 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -11.490374048279023 -4.2804704561659061e-16 4.2545241601337115e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeMid5" -p "CapeMid4";
	setAttr ".t" -type "double3" 0 1.4951400258888459 0 ;
	setAttr ".r" -type "double3" -15.239886048773769 4.4695105096256936e-16 -3.3408715031384448e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeMid6_End" -p "CapeMid5";
	setAttr ".t" -type "double3" 0 1.9040269453498448 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -178.35147795836633 0 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 178.35147795836636 0 0 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeR1" -p "Chest";
	setAttr ".t" -type "double3" -1.2384978380766396 0.49343850131222222 -2.1909061954099762 ;
	setAttr ".r" -type "double3" -163.15317673685627 -0.6530925149464285 5.7287131778935754 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".sd" 3;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeR2" -p "CapeR1";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 1.6370816261950383 0 ;
	setAttr ".r" -type "double3" -7.8160550545472152 5.3960436440882866e-16 -7.8988932937810563e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeR3" -p "CapeR2";
	setAttr ".t" -type "double3" 0 1.610633066263917 0 ;
	setAttr ".r" -type "double3" -13.830213515803063 4.0277743815429421e-15 -3.3210313709481207e-14 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeR4_End" -p "CapeR3";
	setAttr ".t" -type "double3" 1.1102230246251565e-15 1.5323214152261362 0 ;
	setAttr ".r" -type "double3" -188.48009377426871 0 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -174.72217913748679 0 -1.1430118386509511e-15 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeL1" -p "Chest";
	setAttr ".t" -type "double3" 2.7851101326328398 1.1554102572743092 -2.0217223955830868 ;
	setAttr ".r" -type "double3" -161.87814708083627 -0.76856379121006912 -5.1670689035051547 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeL2" -p "CapeL1";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.6425287455094493 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -11.38404435696016 4.7664796991339689e-15 -4.7821342559246521e-14 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeL3" -p "CapeL2";
	setAttr ".t" -type "double3" 1.3322676295501878e-15 1.596218513952298 0 ;
	setAttr ".r" -type "double3" -11.542373997596062 6.7130166689644775e-15 -6.6420627418962553e-14 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "CapeL4_End" -p "CapeL3";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.5323214152261331 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -188.48009377426871 0 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -174.72217913748671 -1.9878466759146985e-16 -1.8139100917721621e-15 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "HipTwist" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.1766292369888238 0.00064981476919445669 -0.00086878601129591893 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 205.13957903290628 89.999999999984979 ;
createNode joint -n "Hip" -p "HipTwist";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.18138768581135345 1.4668210268269899 0.1043039042828824 ;
	setAttr ".r" -type "double3" -33.386391273779928 -1.0313001680124003 0.56252860080088729 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.72356917514651575 1.0610891793703126 -90.162074787503371 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "LegAim";
createNode joint -n "Knee" -p "Hip";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.036337162627452013 2.3836548774653217 -0.051143419208275809 ;
	setAttr ".r" -type "double3" 95.891111388826673 0 0 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 22.919762006828137 0 0 ;
	setAttr ".dl" yes;
	setAttr ".typ" 2;
createNode joint -n "BackKnee" -p "Knee";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.038343975960456245 2.9451069251958168 0.053911437438922238 ;
	setAttr ".r" -type "double3" -23.958969796981204 0 0 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -95.891111388826658 0 0 ;
createNode joint -n "Ankle" -p "BackKnee";
	setAttr ".t" -type "double3" -0.051469342780704135 3.8906130684037232 -0.54457140111455093 ;
	setAttr ".r" -type "double3" 49.265463721840447 0.090816435087590131 -0.19806112377855789 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".pa" -type "double3" 3.1147589914174403 -1.2104724556304991 -11.405913270501992 ;
	setAttr ".dl" yes;
	setAttr ".typ" 4;
createNode joint -n "Foot_End" -p "Ankle";
	setAttr ".t" -type "double3" 0.3171877571462125 0.27738757624720001 -1.6117158190438499 ;
	setAttr ".r" -type "double3" -0.00019030234564052423 0.00053514845282692043 25.864574245063647 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" -125.44414790060311 9.5527524453757771 173.4955857010211 ;
	setAttr ".pa" -type "double3" -0.00019030234564052423 0.00053514845282692043 25.864574245063647 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Ball";
createNode joint -n "Heel_End" -p "Ankle";
	setAttr ".t" -type "double3" 0.0035262855923918757 0.27963451144734086 0.3137258083977279 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" -137.99129230822595 9.5527935702937565 173.49554852167796 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Heel";
createNode transform -n "MotionSystem" -p "Main";
createNode transform -n "FKSystem" -p "MotionSystem";
createNode transform -n "FKParentConstraintToChest_M" -p "FKSystem";
createNode joint -n "FKOffsetHead_M" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 3.362128056796907 -0.05136910884368695 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraHead_M" -p "FKOffsetHead_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -4.4408920985006262e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKHead_M" -p "FKExtraHead_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHead_MShape" -p "FKHead_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.8771756530000001 2.8373094769999998 -1.8771756530000001
		-2.6547272670000002 2.8373094769999998 0
		-1.8771756530000001 2.8373094769999998 1.8771756530000001
		-7.692735459e-16 2.8373094769999998 2.6547272670000002
		1.8771756530000001 2.8373094769999998 1.8771756530000001
		2.6547272670000002 2.8373094769999998 0
		1.8771756530000001 2.8373094769999998 -1.8771756530000001
		1.4258591940000001e-15 2.8373094769999998 -2.6547272670000002
		-1.8771756530000001 2.8373094769999998 -1.8771756530000001
		-2.6547272670000002 2.8373094769999998 0
		-1.8771756530000001 2.8373094769999998 1.8771756530000001
		;
createNode joint -n "FKXHead_M" -p "FKHead_M";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXHead_End_M" -p "FKXHead_M";
	setAttr ".t" -type "double3" 5.9164567891575885e-31 1.7440267880228078 -6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" 9.5763298435263593 0 0 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -9.5763298435263593 0 0 ;
createNode joint -n "FKOffsetGun1_R" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" -2.350906237499248 -0.68096479514476904 0.40968214042113404 ;
	setAttr ".r" -type "double3" 0 0 90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -9.576329905066201 0 0 ;
createNode transform -n "FKExtraGun1_R" -p "FKOffsetGun1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 -1.1102230246251565e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000002 1 ;
createNode transform -n "FKGun1_R" -p "FKExtraGun1_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKGun1_RShape" -p "FKGun1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.89178567939999998 -0.058221635270000002 -0.89502095270000004
		-1.2611462950000001 -0.058221635270000002 -0.0033055460239999998
		-0.89178567939999998 -0.058221635270000002 0.88840986070000005
		-7.0272688419999995e-05 -0.058221635270000002 1.2577704759999999
		0.89164513400000001 -0.058221635270000002 0.88840986070000005
		1.261005749 -0.058221635270000002 -0.0033055460239999998
		0.89164513400000001 -0.058221635270000002 -0.89502095270000004
		-7.0272688419999995e-05 -0.058221635270000002 -1.2643815679999999
		-0.89178567939999998 -0.058221635270000002 -0.89502095270000004
		-1.2611462950000001 -0.058221635270000002 -0.0033055460239999998
		-0.89178567939999998 -0.058221635270000002 0.88840986070000005
		;
createNode joint -n "FKXGun1_R" -p "FKGun1_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetGun2_R" -p "FKXGun1_R";
	setAttr ".t" -type "double3" 0.0017406567944719953 1.1650684250566519 0.081878483299648419 ;
	setAttr ".r" -type "double3" 0 0 90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000002504478161 ;
createNode transform -n "FKExtraGun2_R" -p "FKOffsetGun2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 1.1102230246251565e-16 ;
	setAttr ".ro" 2;
createNode transform -n "FKGun2_R" -p "FKExtraGun2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 1.1102230246251565e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKGun2_RShape" -p "FKGun2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.3492619880000001 0.044626209710000003 -1.3492619880000001
		-1.908144603 0.044626209710000003 -3.2484652139999998e-17
		-1.3492619880000001 0.044626209710000003 1.3492619880000001
		2.2908213079999999e-15 0.044626209710000003 1.908144603
		1.3492619880000001 0.044626209710000003 1.3492619880000001
		1.908144603 0.044626209710000003 -3.2484652139999998e-17
		1.3492619880000001 0.044626209710000003 -1.3492619880000001
		3.8686220649999998e-15 0.044626209710000003 -1.908144603
		-1.3492619880000001 0.044626209710000003 -1.3492619880000001
		-1.908144603 0.044626209710000003 -3.2484652139999998e-17
		-1.3492619880000001 0.044626209710000003 1.3492619880000001
		;
createNode joint -n "FKXGun2_R" -p "FKGun2_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXGun3_End_R" -p "FKXGun2_R";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0.85323520097619721 -2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 0 0 89.999999999999986 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
createNode joint -n "FKOffsetShoulder1_L" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" 2.8186117422206967 0.59492979900124077 -0.34712698500417805 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -19.06748735118234 -1.373922326903978 -135.9197945963916 ;
createNode transform -n "FKExtraShoulder1_L" -p "FKOffsetShoulder1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.3322676295501878e-15 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 0.99999999999999989 ;
createNode transform -n "FKShoulder1_L" -p "FKExtraShoulder1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 -4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulder1_LShape" -p "FKShoulder1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.68908554649999998 1.9520204839999999e-16 -0.68908554649999998
		-0.97451412550000005 2.1638560400000001e-16 0
		-0.68908554649999998 1.108134075e-16 0.68908554649999998
		-2.8238981319999999e-16 -5.9671780229999993e-17 0.97451412550000005
		0.68908554649999998 -1.9520204839999999e-16 0.68908554649999998
		0.97451412550000005 -2.1638560400000001e-16 0
		0.68908554649999998 -1.108134075e-16 -0.68908554649999998
		5.2341343779999999e-16 5.9671780229999993e-17 -0.97451412550000005
		-0.68908554649999998 1.9520204839999999e-16 -0.68908554649999998
		-0.97451412550000005 2.1638560400000001e-16 0
		-0.68908554649999998 1.108134075e-16 0.68908554649999998
		;
createNode joint -n "FKXShoulder1_L" -p "FKShoulder1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow1_L" -p "FKXShoulder1_L";
	setAttr ".t" -type "double3" -0.0052683376394053738 2.6881635991971722 -0.06379416375609237 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 116.19976804737917 4.5150794753640042 0.22693079401785224 ;
createNode transform -n "FKExtraElbow1_L" -p "FKOffsetElbow1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 2.2204460492503131e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode transform -n "FKElbow1_L" -p "FKExtraElbow1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 4.4408920985006262e-16 6.6613381477509392e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999967 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow1_LShape" -p "FKElbow1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.67851442259999994 1.9220749269999998e-16 -0.67851442259999994
		-0.95956429870000004 2.1306607560000001e-16 0
		-0.67851442259999994 1.0911344110000001e-16 0.67851442259999994
		-2.7805772739999999e-16 -5.875636734999999e-17 0.95956429870000004
		0.67851442259999994 -1.9220749269999998e-16 0.67851442259999994
		0.95956429870000004 -2.1306607560000001e-16 0
		0.67851442259999994 -1.0911344110000001e-16 -0.67851442259999994
		5.1538385669999998e-16 5.875636734999999e-17 -0.95956429870000004
		-0.67851442259999994 1.9220749269999998e-16 -0.67851442259999994
		-0.95956429870000004 2.1306607560000001e-16 0
		-0.67851442259999994 1.0911344110000001e-16 0.67851442259999994
		;
createNode joint -n "FKXElbow1_L" -p "FKElbow1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetWrist2_L" -p "FKXElbow1_L";
	setAttr ".t" -type "double3" 0.26325195189550321 2.6188878256762025 0.12313942754248308 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 9.907747759068144 -37.967628237510198 -4.9544429196692983 ;
createNode transform -n "FKExtraWrist2_L" -p "FKOffsetWrist2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -1.1102230246251565e-16 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKWrist2_L" -p "FKExtraWrist2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -2.2204460492503131e-16 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWrist2_LShape" -p "FKWrist2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.55625652449999996 -0.020822975229999999 -0.55834735579999994
		-0.78645556849999998 -0.020822975229999999 -0.0025977017320000001
		-0.55625652449999996 -0.020822975229999999 0.55315195240000004
		-0.00050687037749999999 -0.020822975229999999 0.78335099640000005
		0.55524278370000002 -0.020822975229999999 0.55315195240000004
		0.78544182770000004 -0.020822975229999999 -0.0025977017320000001
		0.55524278370000002 -0.020822975229999999 -0.55834735579999994
		-0.00050687037749999999 -0.020822975229999999 -0.78854639979999996
		-0.55625652449999996 -0.020822975229999999 -0.55834735579999994
		-0.78645556849999998 -0.020822975229999999 -0.0025977017320000001
		-0.55625652449999996 -0.020822975229999999 0.55315195240000004
		;
createNode joint -n "FKXWrist2_L" -p "FKWrist2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToWrist2_L" -p "FKXWrist2_L";
	setAttr ".r" -type "double3" -90.936737917838244 -11.300010672565646 176.80809458509898 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999967 1.0000000000000002 ;
createNode joint -n "FKOffsetWrist1_L" -p "FKXWrist2_L";
	setAttr ".t" -type "double3" 0.016234100558222941 0.66692055569756248 0.083199478632922208 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 0 -1.5663487127822182 ;
createNode transform -n "FKExtraWrist1_L" -p "FKOffsetWrist1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
createNode transform -n "FKWrist1_L" -p "FKExtraWrist1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 2.2204460492503131e-16 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWrist1_LShape" -p "FKWrist1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.55261106130000004 1.565419732e-16 -0.55261106130000004
		-0.78151005760000003 1.73530092e-16 0
		-0.55261106130000004 8.8866636419999998e-17 0.55261106130000004
		-2.2646206290000002e-16 -4.7853689529999999e-17 0.78151005760000003
		0.55261106130000004 -1.565419732e-16 0.55261106130000004
		0.78151005760000003 -1.73530092e-16 0
		0.55261106130000004 -8.8866636419999998e-17 -0.55261106130000004
		4.1975057649999999e-16 4.7853689529999999e-17 -0.78151005760000003
		-0.55261106130000004 1.565419732e-16 -0.55261106130000004
		-0.78151005760000003 1.73530092e-16 0
		-0.55261106130000004 8.8866636419999998e-17 0.55261106130000004
		;
createNode joint -n "FKXWrist1_L" -p "FKWrist1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToWrist1_L" -p "FKXWrist1_L";
	setAttr ".r" -type "double3" -90.942236176776646 -12.866148796568886 176.78183102714553 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999956 0.99999999999999989 ;
createNode joint -n "FKOffsetCapeMid1_L" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" 0.55933212460801518 0.42121368670569126 -2.3549250024825055 ;
	setAttr ".r" -type "double3" -153.18961372406702 -1.5855961826279083e-14 3.778942074611715e-15 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeMid1_L" -p "FKOffsetCapeMid1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.7763568394002505e-15 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKCapeMid1_L" -p "FKExtraCapeMid1_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeMid1_LShape" -p "FKCapeMid1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.49208139509999999 1.393953142e-16 -0.49208139509999999
		-0.6959081828 1.5452265750000001e-16 0
		-0.49208139509999999 7.9132723699999993e-17 0.49208139509999999
		-2.0165678109999999e-16 -4.2612086429999999e-17 0.6959081828
		0.49208139509999999 -1.393953142e-16 0.49208139509999999
		0.6959081828 -1.5452265750000001e-16 0
		0.49208139509999999 -7.9132723699999993e-17 -0.49208139509999999
		3.7377364249999999e-16 4.2612086429999999e-17 -0.6959081828
		-0.49208139509999999 1.393953142e-16 -0.49208139509999999
		-0.6959081828 1.5452265750000001e-16 0
		-0.49208139509999999 7.9132723699999993e-17 0.49208139509999999
		;
createNode joint -n "FKXCapeMid1_L" -p "FKCapeMid1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetCapeMid2_L" -p "FKXCapeMid1_L";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 1.6833042369258129 0 ;
	setAttr ".r" -type "double3" -15.968937883260207 0 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeMid2_L" -p "FKOffsetCapeMid2_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
createNode transform -n "FKCapeMid2_L" -p "FKExtraCapeMid2_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeMid2_LShape" -p "FKCapeMid2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.47924329199999999 1.3575857560000001e-16 -0.47924329199999999
		-0.67775236319999999 1.5049125569999999e-16 0
		-0.47924329199999999 7.7068199259999997e-17 0.47924329199999999
		-1.9639567879999999e-16 -4.150036311e-17 0.67775236319999999
		0.47924329199999999 -1.3575857560000001e-16 0.47924329199999999
		0.67775236319999999 -1.5049125569999999e-16 0
		0.47924329199999999 -7.7068199259999997e-17 -0.47924329199999999
		3.6402211640000004e-16 4.150036311e-17 -0.67775236319999999
		-0.47924329199999999 1.3575857560000001e-16 -0.47924329199999999
		-0.67775236319999999 1.5049125569999999e-16 0
		-0.47924329199999999 7.7068199259999997e-17 0.47924329199999999
		;
createNode joint -n "FKXCapeMid2_L" -p "FKCapeMid2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetCapeMid3_L" -p "FKXCapeMid2_L";
	setAttr ".t" -type "double3" 1.1102230246251565e-16 1.6177712472652503 0 ;
	setAttr ".r" -type "double3" -9.6661503077464523 3.4702403443666667e-16 -4.1041840592269964e-15 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeMid3_L" -p "FKOffsetCapeMid3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.1102230246251565e-16 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKCapeMid3_L" -p "FKExtraCapeMid3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.1102230246251565e-16 8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeMid3_LShape" -p "FKCapeMid3_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.46594875089999999 1.3199253860000001e-16 -0.46594875089999999
		-0.65895104289999995 1.4631652399999999e-16 0
		-0.46594875089999999 7.4930273990000002e-17 0.46594875089999999
		-1.9094752660000001e-16 -4.0349114269999998e-17 0.65895104289999995
		0.46594875089999999 -1.3199253860000001e-16 0.46594875089999999
		0.65895104289999995 -1.4631652399999999e-16 0
		0.46594875089999999 -7.4930273990000002e-17 -0.46594875089999999
		3.5392389059999999e-16 4.0349114269999998e-17 -0.65895104289999995
		-0.46594875089999999 1.3199253860000001e-16 -0.46594875089999999
		-0.65895104289999995 1.4631652399999999e-16 0
		-0.46594875089999999 7.4930273990000002e-17 0.46594875089999999
		;
createNode joint -n "FKXCapeMid3_L" -p "FKCapeMid3_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetCapeMid4_L" -p "FKXCapeMid3_L";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 1.549908338253934 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -11.490374306746384 -4.2804706253827239e-16 4.254524139563313e-15 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeMid4_L" -p "FKOffsetCapeMid4_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
createNode transform -n "FKCapeMid4_L" -p "FKExtraCapeMid4_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeMid4_LShape" -p "FKCapeMid4_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.45521947930000001 1.2895318339999999e-16 -0.45521947930000001
		-0.64377756149999998 1.4294733429999998e-16 0
		-0.45521947930000001 7.3204875530000004e-17 0.45521947930000001
		-1.865506313e-16 -3.9420006500000005e-17 0.64377756149999998
		0.45521947930000001 -1.2895318339999999e-16 0.45521947930000001
		0.64377756149999998 -1.4294733429999998e-16 0
		0.45521947930000001 -7.3204875530000004e-17 -0.45521947930000001
		3.4577418409999996e-16 3.9420006500000005e-17 -0.64377756149999998
		-0.45521947930000001 1.2895318339999999e-16 -0.45521947930000001
		-0.64377756149999998 1.4294733429999998e-16 0
		-0.45521947930000001 7.3204875530000004e-17 0.45521947930000001
		;
createNode joint -n "FKXCapeMid4_L" -p "FKCapeMid4_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetCapeMid5_L" -p "FKXCapeMid4_L";
	setAttr ".t" -type "double3" 0 1.4951400258888454 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -15.239885198800339 4.4695104316951464e-16 -3.3408713795334731e-15 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeMid5_L" -p "FKOffsetCapeMid5_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 4.4408920985006262e-16 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKCapeMid5_L" -p "FKExtraCapeMid5_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 0 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeMid5_LShape" -p "FKCapeMid5_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.53532161519999999 1.5164427170000001e-16 -0.53532161519999999
		-0.75705908840000002 1.6810088620000002e-16 0
		-0.53532161519999999 8.6086281420000007e-17 0.53532161519999999
		-2.1937678370000001e-16 -4.6356499470000003e-17 0.75705908840000002
		0.53532161519999999 -1.5164427170000001e-16 0.53532161519999999
		0.75705908840000002 -1.6810088620000002e-16 0
		0.53532161519999999 -8.6086281420000007e-17 -0.53532161519999999
		4.0661791320000003e-16 4.6356499470000003e-17 -0.75705908840000002
		-0.53532161519999999 1.5164427170000001e-16 -0.53532161519999999
		-0.75705908840000002 1.6810088620000002e-16 0
		-0.53532161519999999 8.6086281420000007e-17 0.53532161519999999
		;
createNode joint -n "FKXCapeMid5_L" -p "FKCapeMid5_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXCapeMid6_End_L" -p "FKXCapeMid5_L";
	setAttr ".t" -type "double3" 1.1102230246251565e-16 1.9040269453498442 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 164.02137054321878 0 0 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -164.02137054321875 0 0 ;
createNode joint -n "FKOffsetCapeR1_R" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" -1.2384978380766396 0.49343850131222311 -2.1909061954099762 ;
	setAttr ".r" -type "double3" -163.15317949657981 -0.65309249651371715 5.7287131639819444 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeR1_R" -p "FKOffsetCapeR1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 1.7763568394002505e-15 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000004 ;
createNode transform -n "FKCapeR1_R" -p "FKExtraCapeR1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999956 0.99999999999999933 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeR1_RShape" -p "FKCapeR1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.96605250269977327 2.7366040156078735e-16 -0.96605250269977538
		-0.22365883685286031 4.96622380669854e-17 0
		-0.96605250269977383 1.5535309103683061e-16 0.96605250269977427
		-6.4810735417657799e-17 -1.3695153932643757e-17 0.22365883685286031
		0.96605250269977361 -2.7366040156078735e-16 0.96605250269977427
		0.22365883685286031 -4.9662238066985413e-17 0
		0.96605250269977383 -1.5535309103683071e-16 -0.96605250269977316
		1.2012759757267298e-16 1.3695153932643745e-17 -0.22365883685286031
		-0.96605250269977327 2.7366040156078735e-16 -0.96605250269977538
		-0.22365883685286031 4.96622380669854e-17 0
		-0.96605250269977383 1.5535309103683061e-16 0.96605250269977427
		;
createNode joint -n "FKXCapeR1_R" -p "FKCapeR1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetCapeR2_R" -p "FKXCapeR1_R";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 1.6370816261950383 0 ;
	setAttr ".r" -type "double3" -7.8160552597178068 5.3960438268407708e-16 -7.8988931557842676e-15 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeR2_R" -p "FKOffsetCapeR2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKCapeR2_R" -p "FKExtraCapeR2_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999944 0.99999999999999944 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeR2_RShape" -p "FKCapeR2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.47784490159397192 1.3536244385117099e-16 -0.47784490159397297
		-0.67577474054503262 1.500521352826373e-16 0
		-0.47784490159397219 7.684332092857701e-17 0.47784490159397242
		-1.9582261326081083e-16 -4.1379268647655315e-17 0.67577474054503262
		0.47784490159397208 -1.3536244385117099e-16 0.47784490159397242
		0.67577474054503262 -1.5005213528263733e-16 0
		0.47784490159397219 -7.684332092857706e-17 -0.47784490159397186
		3.6295993140381476e-16 4.1379268647655278e-17 -0.67577474054503262
		-0.47784490159397192 1.3536244385117099e-16 -0.47784490159397297
		-0.67577474054503262 1.500521352826373e-16 0
		-0.47784490159397219 7.684332092857701e-17 0.47784490159397242
		;
createNode joint -n "FKXCapeR2_R" -p "FKCapeR2_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetCapeR3_R" -p "FKXCapeR2_R";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 1.6106330662639188 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -13.830213555058808 4.0277743826935383e-15 -3.3210312829227182e-14 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeR3_R" -p "FKOffsetCapeR3_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 8.8817841970012523e-16 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKCapeR3_R" -p "FKExtraCapeR3_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999978 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeR3_RShape" -p "FKCapeR3_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.46250342156456437 1.3101655625846607e-16 -0.46250342156456536
		-0.65407861142056845 1.4523462686279319e-16 0
		-0.46250342156456464 7.4376222777091796e-17 0.46250342156456481
		-1.8953561783483509e-16 -4.0050763893347178e-17 0.65407861142056845
		0.46250342156456448 -1.3101655625846607e-16 0.46250342156456481
		0.65407861142056845 -1.4523462686279322e-16 0
		0.46250342156456464 -7.4376222777091845e-17 -0.46250342156456431
		3.5130689812767818e-16 4.0050763893347141e-17 -0.65407861142056845
		-0.46250342156456437 1.3101655625846607e-16 -0.46250342156456536
		-0.65407861142056845 1.4523462686279319e-16 0
		-0.46250342156456464 7.4376222777091796e-17 0.46250342156456481
		;
createNode joint -n "FKXCapeR3_R" -p "FKCapeR3_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXCapeR4_End_R" -p "FKXCapeR3_R";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.5323214152261344 0 ;
	setAttr ".r" -type "double3" -178.3290718317165 -2.9971447353922982 4.9277326482877477 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 175.13505155985732 2.7175657717247024 -5.0869948607038733 ;
createNode joint -n "FKOffsetCapeL1_L" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" 2.7851101326328398 1.1554102572743083 -2.0217223955830868 ;
	setAttr ".r" -type "double3" -161.8781471030785 -0.76856378134365844 -5.1670688429659108 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeL1_L" -p "FKOffsetCapeL1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -8.8817841970012523e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "FKCapeL1_L" -p "FKExtraCapeL1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeL1_LShape" -p "FKCapeL1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.48409335785412233 1.3713248745077769e-16 -0.48409335785412333
		-0.68461139213203237 1.5201426609313281e-16 0
		-0.48409335785412261 7.7848149332323166e-17 0.48409335785412277
		-1.9838325381515583e-16 -4.1920357501715294e-17 0.68461139213203237
		0.48409335785412244 -1.3713248745077769e-16 0.48409335785412277
		0.68461139213203237 -1.5201426609313284e-16 0
		0.48409335785412261 -7.7848149332323215e-17 -0.48409335785412227
		3.6770611420915311e-16 4.1920357501715257e-17 -0.68461139213203237
		-0.48409335785412233 1.3713248745077769e-16 -0.48409335785412333
		-0.68461139213203237 1.5201426609313281e-16 0
		-0.48409335785412261 7.7848149332323166e-17 0.48409335785412277
		;
createNode joint -n "FKXCapeL1_L" -p "FKCapeL1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetCapeL2_L" -p "FKXCapeL1_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 1.6425287455094493 0 ;
	setAttr ".r" -type "double3" -11.384044483068564 4.7664797402447395e-15 -4.7821343816370987e-14 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeL2_L" -p "FKOffsetCapeL2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKCapeL2_L" -p "FKExtraCapeL2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeL2_LShape" -p "FKCapeL2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.47502104890422525 1.345625114884216e-16 -0.47502104890422631
		-0.67178120977304911 1.4916539332011627e-16 0
		-0.47502104890422553 7.6389210781603818e-17 0.47502104890422575
		-1.9466538795334146e-16 -4.1134735413795016e-17 0.67178120977304911
		0.47502104890422542 -1.345625114884216e-16 0.47502104890422575
		0.67178120977304911 -1.491653933201163e-16 0
		0.47502104890422553 -7.6389210781603867e-17 -0.4750210489042252
		3.6081499823586432e-16 4.1134735413794979e-17 -0.67178120977304911
		-0.47502104890422525 1.345625114884216e-16 -0.47502104890422631
		-0.67178120977304911 1.4916539332011627e-16 0
		-0.47502104890422553 7.6389210781603818e-17 0.47502104890422575
		;
createNode joint -n "FKXCapeL2_L" -p "FKCapeL2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetCapeL3_L" -p "FKXCapeL2_L";
	setAttr ".t" -type "double3" 1.3322676295501878e-15 1.5962185139522989 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -11.542374244444982 6.7130167049165837e-15 -6.6420625658454363e-14 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraCapeL3_L" -p "FKOffsetCapeL3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -1.7763568394002505e-15 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKCapeL3_L" -p "FKExtraCapeL3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999978 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKCapeL3_LShape" -p "FKCapeL3_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.46250342156456392 1.3101655625846594e-16 -0.46250342156456492
		-0.65407861142056778 1.4523462686279304e-16 0
		-0.4625034215645642 7.4376222777091734e-17 0.46250342156456442
		-1.8953561783483491e-16 -4.0050763893347141e-17 0.65407861142056778
		0.46250342156456409 -1.3101655625846594e-16 0.46250342156456442
		0.65407861142056778 -1.4523462686279307e-16 0
		0.4625034215645642 -7.4376222777091771e-17 -0.46250342156456387
		3.5130689812767784e-16 4.0050763893347104e-17 -0.65407861142056778
		-0.46250342156456392 1.3101655625846594e-16 -0.46250342156456492
		-0.65407861142056778 1.4523462686279304e-16 0
		-0.4625034215645642 7.4376222777091734e-17 0.46250342156456442
		;
createNode joint -n "FKXCapeL3_L" -p "FKCapeL3_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXCapeL4_End_L" -p "FKXCapeL3_L";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 1.532321415226134 2.2204460492503131e-15 ;
	setAttr ".r" -type "double3" -178.32702550026309 1.5824315033775358 -4.9789463065886101 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 175.13602342243851 -1.3021982328222592 5.0592863700934112 ;
createNode parentConstraint -n "FKParentConstraintToChest_M_parentConstraint1" -p
		 "FKParentConstraintToChest_M";
	addAttr -ci true -k true -sn "w0" -ln "Chest_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 9.5763298435263593 0 0 ;
	setAttr ".rst" -type "double3" 1.4031819443810062e-16 7.7991257400932748 0.26187111942261887 ;
	setAttr ".rsrr" -type "double3" 9.5763298435263593 0 0 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToWrist1_L" -p "FKSystem";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999967 0.99999999999999967 ;
createNode joint -n "FKOffsetMiddleFinger1_L" -p "FKParentConstraintToWrist1_L";
	setAttr ".t" -type "double3" -0.29152620454748401 0.84419957325886319 -0.16371647164843495 ;
	setAttr ".r" -type "double3" -18.274084039457197 6.6060868213632249 14.794836902671477 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger1_L" -p "FKOffsetMiddleFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 -2.6645352591003757e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKMiddleFinger1_L" -p "FKExtraMiddleFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger1_LShape" -p "FKMiddleFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.44340486270000001 0.087030455579999999 0.50538264450000003
		0.64998315579999999 0.08703048612 0.0066584562939999998
		0.44340481230000001 0.087030509290000002 -0.49206571100000002
		-0.055319375890000001 0.087030511529999993 -0.69864400410000005
		-0.55404354320000004 0.087030491520000003 -0.49206566060000001
		-0.76062183630000002 0.087030460990000003 0.0066585276080000002
		-0.55404349269999997 0.08703043781 0.50538269489999998
		-0.055319304579999999 0.087030435569999995 0.71196098799999996
		0.44340486270000001 0.087030455579999999 0.50538264450000003
		0.64998315579999999 0.08703048612 0.0066584562939999998
		0.44340481230000001 0.087030509290000002 -0.49206571100000002
		;
createNode joint -n "FKXMiddleFinger1_L" -p "FKMiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger2_L" -p "FKXMiddleFinger1_L";
	setAttr ".t" -type "double3" 0 0.77937696384104704 0 ;
	setAttr ".r" -type "double3" -29.808793104106133 0.66965351745602952 1.1044222721623154 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger2_L" -p "FKOffsetMiddleFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -3.3306690738754696e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "FKMiddleFinger2_L" -p "FKExtraMiddleFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 4.4408920985006262e-16 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999978 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger2_LShape" -p "FKMiddleFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.60342969069999997 0.013272863609999999 0.60342942759999996
		0.85337843339999997 0.01327291031 -1.8662939460000002e-07
		0.60342971219999997 0.013272951910000001 -0.60342980970000004
		9.8002240989999989e-08 0.01327296404 -0.85337855240000005
		-0.60342952510000003 0.013272939589999999 -0.60342983120000004
		-0.85337826780000003 0.01327289289 -2.1697028089999998e-07
		-0.60342954650000002 0.01327285129 0.60342940609999995
		6.7661355630000002e-08 0.01327283917 0.85337814879999996
		0.60342969069999997 0.013272863609999999 0.60342942759999996
		0.85337843339999997 0.01327291031 -1.8662939460000002e-07
		0.60342971219999997 0.013272951910000001 -0.60342980970000004
		;
createNode joint -n "FKXMiddleFinger2_L" -p "FKMiddleFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleFinger3_End_L" -p "FKXMiddleFinger2_L";
	setAttr ".t" -type "double3" 5.3290705182007514e-15 0.75403114661343096 -2.6645352591003757e-15 ;
	setAttr ".r" -type "double3" -43.949686208167925 -10.646930558522527 -169.40765988416092 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 43.949686208167918 10.646930558522524 169.40765988416095 ;
createNode joint -n "FKOffsetIndexFinger1_L" -p "FKParentConstraintToWrist1_L";
	setAttr ".t" -type "double3" 0.4564735271965894 0.83602150400759334 -0.14399876771940257 ;
	setAttr ".r" -type "double3" -9.9917548169910688 11.317098383918683 -11.999448796154944 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger1_L" -p "FKOffsetIndexFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -4.4408920985006262e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000004 ;
createNode transform -n "FKIndexFinger1_L" -p "FKExtraIndexFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -3.3306690738754696e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999967 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger1_LShape" -p "FKIndexFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.47995336519999998 -8.1083238789999997e-08 0.47995306160000001
		0.6787564787 -5.8121721659999996e-08 -1.7614287980000002e-07
		0.47995331930000001 -2.7515075549999999e-08 -0.47995339479999999
		8.1590195929999994e-08 -7.1922592329999998e-09 -0.67875650830000001
		-0.47995313709999998 -9.0581018239999988e-09 -0.47995334890000002
		-0.67875625049999999 -3.2019619399999999e-08 -1.1119784470000001e-07
		-0.47995309120000001 -6.2626265060000005e-08 0.47995310749999998
		1.4653522750000001e-07 -8.2949081379999997e-08 0.67875622089999998
		0.47995336519999998 -8.1083238789999997e-08 0.47995306160000001
		0.6787564787 -5.8121721659999996e-08 -1.7614287980000002e-07
		0.47995331930000001 -2.7515075549999999e-08 -0.47995339479999999
		;
createNode joint -n "FKXIndexFinger1_L" -p "FKIndexFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetIndexFinger2_L" -p "FKXIndexFinger1_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0.77937696384104815 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -29.808793104106133 0.66965351745602952 1.1044222721623154 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger2_L" -p "FKOffsetIndexFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKIndexFinger2_L" -p "FKExtraIndexFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger2_LShape" -p "FKIndexFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.63046461149999999 -1.640619662e-07 0.63046444960000003
		0.8916116275 -1.2646324569999999e-07 -1.703267802e-07
		0.63046464540000002 -7.8227066600000001e-08 -0.63046480429999996
		2.5455804579999997e-08 -4.7609526539999996e-08 -0.89161182039999998
		-0.63046460859999998 -5.2545966240000005e-08 -0.6304648383
		-0.89161162459999999 -9.0144687710000004e-08 -2.183101948e-07
		-0.63046464250000001 -1.3838086720000001e-07 0.6304644157
		-2.252761355e-08 -1.6899840589999998e-07 0.89161143180000002
		0.63046461149999999 -1.640619662e-07 0.63046444960000003
		0.8916116275 -1.2646324569999999e-07 -1.703267802e-07
		0.63046464540000002 -7.8227066600000001e-08 -0.63046480429999996
		;
createNode joint -n "FKXIndexFinger2_L" -p "FKIndexFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXIndexFinger3_End_L" -p "FKXIndexFinger2_L";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 0.74556221729954908 -3.5527136788005009e-15 ;
	setAttr ".r" -type "double3" -53.561607835631442 -26.878184598538521 167.56640599026656 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 53.561607835631477 26.878184598538528 -167.56640599026656 ;
createNode parentConstraint -n "FKParentConstraintToWrist1_L_parentConstraint1" -p
		 "FKParentConstraintToWrist1_L";
	addAttr -ci true -k true -sn "w0" -ln "Wrist1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -90.919848138792545 -3.0079470435997457 167.08389447093256 ;
	setAttr ".rst" -type "double3" 3.8136797635550126 6.5185540062759477 1.979837885968879 ;
	setAttr ".rsrr" -type "double3" -90.919844742388236 -3.0079523667758208 167.08389656575079 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToPelvis_M" -p "FKSystem";
	setAttr ".ro" 3;
createNode joint -n "FKOffsetHipTwist_R" -p "FKParentConstraintToPelvis_M";
	setAttr ".t" -type "double3" -1.1766292369888238 0.00064981476919534487 -0.00086878601129591893 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 205.1395817586988 90.000002504478161 ;
createNode transform -n "FKExtraHipTwist_R" -p "FKOffsetHipTwist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 2.2204460492503131e-16 ;
	setAttr ".ro" 2;
createNode transform -n "FKHipTwist_R" -p "FKExtraHipTwist_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHipTwist_RShape" -p "FKHipTwist_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.74373236639999996 -0.033731662209999999 -0.64979319440000005
		-1.0044809889999999 -0.033731662209999999 -0.02029033257
		-0.74373236639999996 -0.033731662209999999 0.60921252920000002
		-0.1142295046 -0.033731662209999999 0.86996115220000003
		0.5152733572 -0.033731662209999999 0.60921252920000002
		0.77602198020000002 -0.033731662209999999 -0.02029033257
		0.5152733572 -0.033731662209999999 -0.64979319440000005
		-0.1142295046 -0.033731662209999999 -0.91054181739999995
		-0.74373236639999996 -0.033731662209999999 -0.64979319440000005
		-1.0044809889999999 -0.033731662209999999 -0.02029033257
		-0.74373236639999996 -0.033731662209999999 0.60921252920000002
		;
createNode joint -n "FKXHipTwist_R" -p "FKHipTwist_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "IKXLegAim_R" -p "FKXHipTwist_R";
	setAttr ".t" -type "double3" -0.18138774881721886 1.4668210215861834 0.10430386841496231 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000007 1.0000000000000004 ;
createNode aimConstraint -n "IKXLegAim_R_aimConstraint1" -p "IKXLegAim_R";
	addAttr -ci true -sn "w0" -ln "IKLeg_RW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 0 0 1 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -11.163039581568984 -0.28325817933216613 -88.151040807249416 ;
	setAttr -k on ".w0";
createNode transform -n "IKXLegUnAim_R" -p "FKXHipTwist_R";
	setAttr ".t" -type "double3" -0.18138774881721886 1.4668210215861843 0.10430386841496209 ;
	setAttr ".r" -type "double3" -11.163039581568984 -0.28325817933216529 -88.151040807249416 ;
	setAttr ".s" -type "double3" 1.0000000000000007 1.0000000000000004 1.0000000000000002 ;
createNode transform -n "IKXLegAimBlend_R" -p "FKXHipTwist_R";
	setAttr ".t" -type "double3" -0.18138774881721886 1.4668210215861843 0.10430386841496209 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 0.99999999999999989 ;
createNode orientConstraint -n "IKXLegAimBlend_R_orientConstraint1" -p "IKXLegAimBlend_R";
	addAttr -ci true -k true -sn "w0" -ln "IKXLegAim_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXLegUnAim_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -11.163039581568984 -0.2832581793321311 -88.15104080724943 ;
	setAttr ".rsrr" -type "double3" -11.163039581568984 -0.28325817933216457 -88.151040807249416 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "FKOffsetHip_R" -p "IKXLegAimBlend_R";
	setAttr ".t" -type "double3" -3.0278570761765877e-09 5.5000512588776473e-08 4.7428685423511752e-08 ;
	setAttr ".r" -type "double3" -33.386391549845456 -1.031300187898863 0.56252861707013802 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 11.874723615453277 1.7080662933788762 -1.7130498353335171 ;
createNode transform -n "FKExtraHip_R" -p "FKOffsetHip_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "FKHip_R" -p "FKExtraHip_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHip_RShape" -p "FKHip_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.70946947689999995 -0.025387602030000001 -0.71343500530000004
		-1.0032502780000001 -0.025387602030000001 -0.0041854102749999999
		-0.70946947689999995 -0.025387602030000001 0.70506418479999999
		-0.00021988185730000001 -0.025387602030000001 0.99884498619999995
		0.70902971319999997 -0.025387602030000001 0.70506418479999999
		1.002810515 -0.025387602030000001 -0.0041854102749999999
		0.70902971319999997 -0.025387602030000001 -0.71343500530000004
		-0.00021988185730000001 -0.025387602030000001 -1.0072158069999999
		-0.70946947689999995 -0.025387602030000001 -0.71343500530000004
		-1.0032502780000001 -0.025387602030000001 -0.0041854102749999999
		-0.70946947689999995 -0.025387602030000001 0.70506418479999999
		;
createNode joint -n "FKXHip_R" -p "FKHip_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetKnee_R" -p "FKXHip_R";
	setAttr ".t" -type "double3" -0.036337162627452013 2.3836548774653217 -0.051143419208274921 ;
	setAttr ".r" -type "double3" 95.891108965505865 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 22.919761854393723 0 0 ;
createNode transform -n "FKExtraKnee_R" -p "FKOffsetKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 4.4408920985006262e-16 4.4408920985006262e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "FKKnee_R" -p "FKExtraKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 4.4408920985006262e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKKnee_RShape" -p "FKKnee_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.8852346056 -0.058392523809999999 -0.87639158989999999
		-1.2528021439999999 -0.058392523809999999 0.010994946029999999
		-0.8852346056 -0.058392523809999999 0.89838148200000001
		0.0021519302919999999 -0.058392523809999999 1.2659490200000001
		0.88953846619999999 -0.058392523809999999 0.89838148200000001
		1.2571060039999999 -0.058392523809999999 0.010994946029999999
		0.88953846619999999 -0.058392523809999999 -0.87639158989999999
		0.0021519302919999999 -0.058392523809999999 -1.243959128
		-0.8852346056 -0.058392523809999999 -0.87639158989999999
		-1.2528021439999999 -0.058392523809999999 0.010994946029999999
		-0.8852346056 -0.058392523809999999 0.89838148200000001
		;
createNode joint -n "FKXKnee_R" -p "FKKnee_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetBackKnee_R" -p "FKXKnee_R";
	setAttr ".t" -type "double3" -0.038343975960455356 2.9451069251958142 0.053911437438918242 ;
	setAttr ".r" -type "double3" -23.958970013969285 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -95.891108965505865 0 0 ;
createNode transform -n "FKExtraBackKnee_R" -p "FKOffsetBackKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 4.4408920985006262e-16 2.2204460492503131e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKBackKnee_R" -p "FKExtraBackKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKBackKnee_RShape" -p "FKBackKnee_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.93199569209999999 2.6401289229999999e-16 -0.93199569209999999
		-1.3180409479999999 2.9266388160000003e-16 0
		-0.93199569209999999 1.4987633819999999e-16 0.93199569209999999
		-3.819352919e-16 -8.0706731399999996e-17 1.3180409479999999
		0.93199569209999999 -2.6401289229999999e-16 0.93199569209999999
		1.3180409479999999 -2.9266388160000003e-16 0
		0.93199569209999999 -1.4987633819999999e-16 -0.93199569209999999
		7.0792236429999998e-16 8.0706731399999996e-17 -1.3180409479999999
		-0.93199569209999999 2.6401289229999999e-16 -0.93199569209999999
		-1.3180409479999999 2.9266388160000003e-16 0
		-0.93199569209999999 1.4987633819999999e-16 0.93199569209999999
		;
createNode joint -n "FKXBackKnee_R" -p "FKBackKnee_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetAnkle_R" -p "FKXBackKnee_R";
	setAttr ".t" -type "double3" -0.051469342780706029 3.8906130684037219 -0.54457140111455082 ;
	setAttr ".r" -type "double3" 49.265464634195801 0.090816436360310948 -0.19806111852072894 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraAnkle_R" -p "FKOffsetAnkle_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 5.5511151231257827e-17 -2.2204460492503131e-16 ;
	setAttr ".ro" 3;
createNode transform -n "FKAnkle_R" -p "FKExtraAnkle_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 5.5511151231257827e-17 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKAnkle_RShape" -p "FKAnkle_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.0950922940000001 -2.1512737809999999e-08 -0.53632320479999995
		-1.5284986380000001 -3.8003223789999997e-08 0.01061993174
		-1.078897682 -3.3354769590000004e-08 0.56427109639999995
		-0.0096595675669999999 -1.0290376619999999e-08 0.8003089457
		1.0528705190000001 1.7679146509999999e-08 0.58046570890000004
		1.4862768630000001 3.416963249e-08 0.033522572309999998
		1.0366759059999999 2.9521178290000001e-08 -0.52012859239999998
		-0.03256220814 6.456785324e-09 -0.75616644160000002
		-1.0950922940000001 -2.1512737809999999e-08 -0.53632320479999995
		-1.5284986380000001 -3.8003223789999997e-08 0.01061993174
		-1.078897682 -3.3354769590000004e-08 0.56427109639999995
		;
createNode joint -n "FKXAnkle_R" -p "FKAnkle_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToAnkle_R" -p "FKXAnkle_R";
	setAttr ".r" -type "double3" 179.99999999999997 -0.31050284846213821 1.1034765745125402e-32 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 0.99999999999999978 ;
createNode joint -n "FKXFoot_End_R" -p "FKXAnkle_R";
	setAttr ".t" -type "double3" 0.31718775714621339 0.2773875762471989 -1.6117158190438492 ;
	setAttr ".r" -type "double3" -55.118469924697102 0.012899034538187451 -142.85179831316532 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 179.99999999999997 -0.3105028484621381 1.7591831875147176e-16 ;
createNode joint -n "FKXHeel_End_R" -p "FKXAnkle_R";
	setAttr ".t" -type "double3" 0.0035262855923923198 0.27963451144734097 0.3137258083977279 ;
	setAttr ".r" -type "double3" 42.044344244641664 -9.244287614810208 -173.50136802171019 ;
	setAttr ".jo" -type "double3" 179.99999999999997 -0.3105028484621381 1.7591831875147176e-16 ;
createNode joint -n "FKOffsetHipTwist_L" -p "FKParentConstraintToPelvis_M";
	setAttr ".t" -type "double3" 1.176629236988824 0.00064981476919623304 -0.00086878601129591893 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 4.8296730725799095e-06 25.139577322360399 90.000002504478161 ;
createNode transform -n "FKExtraHipTwist_L" -p "FKOffsetHipTwist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 4.4408920985006262e-16 -3.3306690738754696e-16 ;
	setAttr ".r" -type "double3" 4.0774711016362273e-12 1.5902773407324512e-15 1.9480897423964094e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999989 ;
createNode transform -n "FKHipTwist_L" -p "FKExtraHipTwist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 2.2204460492503131e-16 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHipTwist_LShape" -p "FKHipTwist_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.74373241938222545 0.033731633990305543 0.64979313522316851
		1.0044809932406293 0.033731583670433052 0.020290253203742115
		0.74373232189903771 0.033731574622744454 -0.60921258837682646
		0.11422943990961176 0.033731612147253287 -0.86996116263523304
		-0.51527340170095481 0.033731674262610589 -0.60921249089364193
		-0.77602197595935918 0.033731724582483302 0.020290391065784119
		-0.51527330421776618 0.033731733630171679 0.64979323270635314
		0.11422957777165976 0.033731696105663289 0.91054180696475973
		0.74373241938222545 0.033731633990305543 0.64979313522316851
		1.0044809932406293 0.033731583670433052 0.020290253203742115
		0.74373232189903771 0.033731574622744454 -0.60921258837682646
		;
createNode joint -n "FKXHipTwist_L" -p "FKHipTwist_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "IKXLegAim_L" -p "FKXHipTwist_L";
	setAttr ".t" -type "double3" 0.18138762465438774 -1.4668210408598914 -0.10430381329262151 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 0.99999999999999989 ;
createNode aimConstraint -n "IKXLegAim_L_aimConstraint1" -p "IKXLegAim_L";
	addAttr -ci true -sn "w0" -ln "IKLeg_LW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 0 0 1 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 168.83695589717615 -0.28326073653079115 -88.151045319386398 ;
	setAttr -k on ".w0";
createNode transform -n "IKXLegUnAim_L" -p "FKXHipTwist_L";
	setAttr ".t" -type "double3" 0.18138762465438774 -1.4668210408598918 -0.10430381329262151 ;
	setAttr ".r" -type "double3" 168.83695589717615 -0.28326073653079109 -88.151045319386398 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1 0.99999999999999978 ;
createNode transform -n "IKXLegAimBlend_L" -p "FKXHipTwist_L";
	setAttr ".t" -type "double3" 0.18138762465438774 -1.4668210408598918 -0.10430381329262151 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1 0.99999999999999978 ;
createNode orientConstraint -n "IKXLegAimBlend_L_orientConstraint1" -p "IKXLegAimBlend_L";
	addAttr -ci true -k true -sn "w0" -ln "IKXLegAim_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXLegUnAim_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 168.83695589717615 -0.28326073653085637 -88.151045319386355 ;
	setAttr ".rsrr" -type "double3" 168.83695589717615 -0.28326073653079109 -88.151045319386384 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "FKOffsetHip_L" -p "IKXLegAimBlend_L";
	setAttr ".t" -type "double3" -1.2502058055474663e-08 -7.8016904936362153e-08 7.7287464206321488e-08 ;
	setAttr ".r" -type "double3" -33.386391549845456 -1.031300187898863 0.56252861707013802 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -168.1252718899029 -1.7080680637436352 1.7130447795697865 ;
createNode transform -n "FKExtraHip_L" -p "FKOffsetHip_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -4.4408920985006262e-16 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -6.3612064257530061e-15 -4.9696166897867449e-17 -1.2424041724466862e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "FKHip_L" -p "FKExtraHip_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -4.4408920985006262e-16 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999956 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHip_LShape" -p "FKHip_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.70946950011273646 0.025387457257474377 0.71343498022432383
		1.0032502986622243 0.025387375397948553 0.0041853841428713956
		0.70946949501171419 0.02538734773318696 -0.7050642098756712
		0.00021989891256035676 0.025387390468831761 -0.99884500872516524
		-0.7090296950882804 0.025387478570922273 -0.70506420477466003
		-1.002810494337768 0.025387560430448985 0.0041853913567919676
		-0.70902968998725813 0.025387588095210578 0.713434985325335
		0.00021990612649558372 0.025387545359565333 1.0072157844748282
		0.70946950011273646 0.025387457257474377 0.71343498022432383
		1.0032502986622243 0.025387375397948553 0.0041853841428713956
		0.70946949501171419 0.02538734773318696 -0.7050642098756712
		;
createNode joint -n "FKXHip_L" -p "FKHip_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetKnee_L" -p "FKXHip_L";
	setAttr ".t" -type "double3" 0.036337162627452901 -2.3836548774653208 0.051143419208277585 ;
	setAttr ".r" -type "double3" 95.891108965505865 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 22.919761854393723 0 0 ;
createNode transform -n "FKExtraKnee_L" -p "FKOffsetKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -3.8167141491692061e-14 -4.3484146035634018e-17 1.4483314202998959e-32 ;
	setAttr ".ro" 2;
createNode transform -n "FKKnee_L" -p "FKExtraKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKKnee_LShape" -p "FKKnee_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.88523432946917291 0.058392835658793718 0.87639169700407527
		1.2528019411249298 0.058392782322915782 -0.01099480858239188
		0.88523447598068961 0.058392698626108519 -0.89838137489591352
		-0.0021520295677781576 0.05839263359682656 -1.2659489861516668
		-0.88953859581930295 0.058392625328341552 -0.89838152140742444
		-1.2571062068750603 0.058392678664219488 -0.010995015780956852
		-0.88953874233081986 0.058392762361027195 0.87639155049256479
		-0.0021522367663515674 0.05839282739030871 1.2439591618483179
		0.88523432946917291 0.058392835658793718 0.87639169700407527
		1.2528019411249298 0.058392782322915782 -0.01099480858239188
		0.88523447598068961 0.058392698626108519 -0.89838137489591352
		;
createNode joint -n "FKXKnee_L" -p "FKKnee_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetBackKnee_L" -p "FKXKnee_L";
	setAttr ".t" -type "double3" 0.038343975960455356 -2.9451069251958168 -0.053911437438915577 ;
	setAttr ".r" -type "double3" -23.958970013969285 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -95.891108965505865 0 0 ;
createNode transform -n "FKExtraBackKnee_L" -p "FKOffsetBackKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 4.4408920985006262e-16 -3.3306690738754696e-16 ;
	setAttr ".r" -type "double3" -1.2722024600202122e-14 1.2424041724466862e-16 7.4544250346801162e-17 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000002 ;
createNode transform -n "FKBackKnee_L" -p "FKExtraBackKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 4.4408920985006262e-16 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999956 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKBackKnee_LShape" -p "FKBackKnee_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.93199562172175154 -3.4937984061400584e-07 0.93199573109191314
		1.3180408727116801 -4.569170388712962e-07 3.6958101023998324e-08
		0.93199561190161084 -4.9330119766821667e-07 -0.93199565310808197
		-8.2232202824883416e-08 -4.3721897036164137e-07 -1.318040904098017
		-0.93199577229838204 -3.2152256501660759e-07 -0.93199564328795526
		-1.3180410232883102 -2.1398536675931723e-07 5.0845857302306285e-08
		-0.93199576247824156 -1.7760120796239676e-07 0.93199574091204029
		-6.8344428338917851e-08 -2.3368343526897206e-07 1.3180409919019753
		0.93199562172175154 -3.4937984061400584e-07 0.93199573109191314
		1.3180408727116801 -4.569170388712962e-07 3.6958101023998324e-08
		0.93199561190161084 -4.9330119766821667e-07 -0.93199565310808197
		;
createNode joint -n "FKXBackKnee_L" -p "FKBackKnee_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetAnkle_L" -p "FKXBackKnee_L";
	setAttr ".t" -type "double3" 0.051469342780708249 -3.8906130684037246 0.5445714011145526 ;
	setAttr ".r" -type "double3" 49.265464634195801 0.090816436360310948 -0.19806111852072894 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraAnkle_L" -p "FKOffsetAnkle_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0 0 ;
	setAttr ".r" -type "double3" 6.4922293500486011e-14 -1.4908850069360232e-16 3.8114559738409907e-33 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000004 ;
createNode transform -n "FKAnkle_L" -p "FKExtraAnkle_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 5.5511151231257827e-17 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999944 1 0.99999999999999922 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKAnkle_LShape" -p "FKAnkle_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.0950918277735244 5.7398794550689303e-08 0.53632372889829338
		1.5284982082323597 3.9882221414266894e-09 -0.010619378751148156
		1.0788972891383484 -1.455968190455792e-08 -0.56427057338122277
		0.0096591904394633943 1.2620193112766742e-08 -0.80030849395586712
		-1.0528709107821266 6.9606244934572459e-08 -0.58046532798336137
		-1.4862772912409616 1.2301681728832392e-07 -0.033522220283919379
		-1.0366763711469504 1.4156472133430853e-07 0.52012897439615569
		0.032561727258934248 1.1438484631698387e-07 0.75616689487079936
		1.0950918277735244 5.7398794550689303e-08 0.53632372889829338
		1.5284982082323597 3.9882221414266894e-09 -0.010619378751148156
		1.0788972891383484 -1.455968190455792e-08 -0.56427057338122277
		;
createNode joint -n "FKXAnkle_L" -p "FKAnkle_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToAnkle_L" -p "FKXAnkle_L";
	setAttr ".r" -type "double3" 0 -0.31050284846213849 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1.0000000000000004 ;
createNode joint -n "FKXFoot_End_L" -p "FKXAnkle_L";
	setAttr ".t" -type "double3" -0.31718775714621383 -0.27738757624720001 1.6117158190438503 ;
	setAttr ".r" -type "double3" 124.8815300753029 0.012899034538187451 -142.85179831316532 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 -0.31050284846213844 0 ;
createNode joint -n "FKXHeel_End_L" -p "FKXAnkle_L";
	setAttr ".t" -type "double3" -0.0035262855923927638 -0.27963451144734097 -0.31372580839772812 ;
	setAttr ".r" -type "double3" -137.95565575535835 9.2442876148102098 173.50136802171022 ;
	setAttr ".jo" -type "double3" 0 -0.31050284846213844 0 ;
createNode parentConstraint -n "FKParentConstraintToPelvis_M_parentConstraint1" -p
		 "FKParentConstraintToPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 9.5763298435263593 0 0 ;
	setAttr ".rst" -type "double3" 1.4031819443810048e-16 4.7513626008102703 -0.30196761248010473 ;
	setAttr ".rsrr" -type "double3" 9.5763298435263593 0 0 ;
	setAttr -k on ".w0";
createNode transform -n "PelvisCenterBtwLegsBlended_M" -p "FKSystem";
	setAttr ".ro" 3;
createNode transform -n "CenterOffset_M" -p "PelvisCenterBtwLegsBlended_M";
createNode transform -n "CenterExtra_M" -p "CenterOffset_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Center_M" -p "CenterExtra_M";
	addAttr -ci true -k true -sn "CenterBtwFeet" -ln "CenterBtwFeet" -min 0 -max 10 
		-at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 4;
	setAttr -k on ".CenterBtwFeet";
createNode nurbsCurve -n "Center_MShape" -p "Center_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 46 0 no 3
		47 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46
		47
		-3.5188755710000001 -0.23222724750000001 2.4989613190000001e-05
		-3.7428901240000001 -0.23222724750000001 0.68947241429999995
		-4.3293693830000004 -0.23222724750000001 1.1155759890000001
		-5.054298803 -0.23222724750000001 1.1155759890000001
		-5.640778063 -0.23222724750000001 0.68947241429999995
		-5.8647926129999997 -0.23222724750000001 2.4989613190000001e-05
		-5.640778063 -0.23222724750000001 -0.68942243510000001
		-5.054298803 -0.23222724750000001 -1.1155260090000001
		-4.3293693830000004 -0.23222724750000001 -1.1155260090000001
		-3.7428901240000001 -0.23222724750000001 -0.68942243510000001
		-3.5188755710000001 -0.23222724750000001 2.4989613190000001e-05
		-6.2660851749999999e-09 -0.23222724750000001 2.4989613190000001e-05
		3.518875558 -0.23222724750000001 2.4989613190000001e-05
		3.7428901109999999 -0.23222724750000001 0.68947241429999995
		4.3293693700000002 -0.23222724750000001 1.1155759890000001
		5.0542987899999998 -0.23222724750000001 1.1155759890000001
		5.6407780519999999 -0.23222724750000001 0.68947241429999995
		5.8647926019999996 -0.23222724750000001 2.4989613190000001e-05
		5.6407780519999999 -0.23222724750000001 -0.68942243510000001
		5.0542987899999998 -0.23222724750000001 -1.1155260090000001
		4.3293693700000002 -0.23222724750000001 -1.1155260090000001
		3.7428901109999999 -0.23222724750000001 -0.68942243510000001
		3.518875558 -0.23222724750000001 2.4989613190000001e-05
		-6.2660851749999999e-09 -0.23222724750000001 2.4989613190000001e-05
		-6.2660851749999999e-09 -0.23222724750000001 -3.5188505750000001
		-0.68944743090000005 -0.23222724750000001 -3.742865128
		-1.1155510049999999 -0.23222724750000001 -4.3293443869999999
		-1.1155510049999999 -0.23222724750000001 -5.0542738070000004
		-0.68944743090000005 -0.23222724750000001 -5.6407530689999996
		-6.2660851749999999e-09 -0.23222724750000001 -5.864767617
		0.68944741850000002 -0.23222724750000001 -5.6407530689999996
		1.115550993 -0.23222724750000001 -5.0542738070000004
		1.115550993 -0.23222724750000001 -4.3293443869999999
		0.68944741850000002 -0.23222724750000001 -3.742865128
		-6.2660851749999999e-09 -0.23222724750000001 -3.5188505750000001
		-6.2660851749999999e-09 -0.23222724750000001 2.4989613190000001e-05
		-6.2660851749999999e-09 -0.23222724750000001 3.518900554
		-0.68944743090000005 -0.23222724750000001 3.742915107
		-1.1155510049999999 -0.23222724750000001 4.3293943659999998
		-1.1155510049999999 -0.23222724750000001 5.0543237870000004
		-0.68944743090000005 -0.23222724750000001 5.6408030470000003
		-6.2660851749999999e-09 -0.23222724750000001 5.864817597
		0.68944741850000002 -0.23222724750000001 5.6408030470000003
		1.115550993 -0.23222724750000001 5.0543237870000004
		1.115550993 -0.23222724750000001 4.3293943659999998
		0.68944741850000002 -0.23222724750000001 3.742915107
		-6.2660851749999999e-09 -0.23222724750000001 3.518900554
		;
createNode joint -n "FKOffsetPelvis_M" -p "Center_M";
	setAttr ".t" -type "double3" 1.4031819443810043e-16 0 0 ;
	setAttr ".r" -type "double3" 9.576329905066201 0 0 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraPelvis_M" -p "FKOffsetPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0 0 ;
	setAttr ".ro" 3;
createNode transform -n "FKPelvis_M" -p "FKExtraPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0 0 ;
	setAttr ".ro" 3;
createNode nurbsCurve -n "FKPelvis_MShape" -p "FKPelvis_M";
	setAttr -l on -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.3175074378111462 3.7321948184297888e-16 -1.3175074378111491
		-1.8632368870799514 4.1372169847341298e-16 0
		-1.3175074378111471 2.118713551861538e-16 1.3175074378111475
		-5.3991943536933729e-16 -1.1409035449078691e-16 1.8632368870799514
		1.3175074378111469 -3.7321948184297888e-16 1.3175074378111475
		1.8632368870799514 -4.1372169847341308e-16 0
		1.3175074378111471 -2.118713551861539e-16 -1.317507437811146
		1.0007481667310563e-15 1.1409035449078682e-16 -1.8632368870799514
		-1.3175074378111462 3.7321948184297888e-16 -1.3175074378111491
		-1.8632368870799514 4.1372169847341298e-16 0
		-1.3175074378111471 2.118713551861538e-16 1.3175074378111475
		;
createNode transform -n "HipSwingerGroupPelvis_M" -p "FKPelvis_M";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 1.4130837059160273 0.018891736599552011 ;
createNode orientConstraint -n "HipSwingerGroupPelvis_M_orientConstraint1" -p "HipSwingerGroupPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "HipSwingerPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode joint -n "FKXPelvis_M" -p "HipSwingerGroupPelvis_M";
	setAttr ".t" -type "double3" -2.4651903288156619e-32 -1.4130837059160273 -0.018891736599552011 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "HipSwingerStabalizePelvis_M" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 1.4130837059160273 0.018891736599552011 ;
createNode joint -n "FKOffsetSpineA_M" -p "HipSwingerStabalizePelvis_M";
	setAttr ".t" -type "double3" -4.9303806576313238e-32 -2.7250557366187422e-10 1.4932286518387627e-09 ;
	setAttr ".jo" -type "double3" 9.5763299050662027 0 0 ;
createNode transform -n "FKExtraSpineA_M" -p "FKOffsetSpineA_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0 0 ;
createNode transform -n "FKSpineA_M" -p "FKExtraSpineA_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKSpineA_MShape" -p "FKSpineA_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.0479293679999999 -0.054513331409999997 -1.050402893
		-1.4819962820000001 -0.054513331409999997 -0.0024726600290000001
		-1.0479293679999999 -0.054513331409999997 1.045457573
		8.6543680280000006e-07 -0.054513331409999997 1.4795244869999999
		1.0479310989999999 -0.054513331409999997 1.045457573
		1.4819980129999999 -0.054513331409999997 -0.0024726600290000001
		1.0479310989999999 -0.054513331409999997 -1.050402893
		8.65436804e-07 -0.054513331409999997 -1.484469807
		-1.0479293679999999 -0.054513331409999997 -1.050402893
		-1.4819962820000001 -0.054513331409999997 -0.0024726600290000001
		-1.0479293679999999 -0.054513331409999997 1.045457573
		;
createNode joint -n "FKXSpineA_M" -p "FKSpineA_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetSpineB_M" -p "FKXSpineA_M";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0.71494231050174317 0.030376928917300816 ;
createNode transform -n "FKExtraSpineB_M" -p "FKOffsetSpineB_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0 0 ;
createNode transform -n "FKSpineB_M" -p "FKExtraSpineB_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKSpineB_MShape" -p "FKSpineB_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.4841557409999999 -0.0157966566 -1.4841502849999999
		-2.098913177 -0.0157966566 5.4558415609999998e-06
		-1.4841557409999999 -0.0157966566 1.4841611969999999
		-6.0821252809999993e-16 -0.0157966566 2.0989186329999998
		1.4841557409999999 -0.0157966566 1.4841611969999999
		2.098913177 -0.0157966566 5.4558415609999998e-06
		1.4841557409999999 -0.0157966566 -1.4841502849999999
		1.1273303619999999e-15 -0.0157966566 -2.0989077209999998
		-1.4841557409999999 -0.0157966566 -1.4841502849999999
		-2.098913177 -0.0157966566 5.4558415609999998e-06
		-1.4841557409999999 -0.0157966566 1.4841611969999999
		;
createNode joint -n "FKXSpineB_M" -p "FKSpineB_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetChest_M" -p "FKXSpineB_M";
	setAttr ".t" -type "double3" 0 0.97106708107769091 -0.00031724266683363922 ;
createNode transform -n "FKExtraChest_M" -p "FKOffsetChest_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0 0 ;
createNode transform -n "FKChest_M" -p "FKExtraChest_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKChest_MShape" -p "FKChest_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.1717145649999998 -0.07031978556 -2.1717145649999998
		-3.0712681919999998 -0.07031978556 0
		-2.1717145649999998 -0.07031978556 2.1717145649999998
		-8.8997668449999996e-16 -0.07031978556 3.0712681919999998
		2.1717145649999998 -0.07031978556 2.1717145649999998
		3.0712681919999998 -0.07031978556 0
		2.1717145649999998 -0.07031978556 -2.1717145649999998
		1.649584137e-15 -0.07031978556 -3.0712681919999998
		-2.1717145649999998 -0.07031978556 -2.1717145649999998
		-3.0712681919999998 -0.07031978556 0
		-2.1717145649999998 -0.07031978556 2.1717145649999998
		;
createNode joint -n "FKXChest_M" -p "FKChest_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToChest_M" -p "FKXChest_M";
	setAttr ".r" -type "double3" -9.5763298435263593 0 0 ;
createNode orientConstraint -n "HipSwingerStabalizePelvis_M_orientConstraint1" -p
		 "HipSwingerStabalizePelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -9.5763299050662027 0 0 ;
	setAttr ".rsrr" -type "double3" -9.5763299050662027 0 0 ;
	setAttr -k on ".w0";
createNode transform -n "HipSwingerOffsetPelvis_M" -p "Center_M";
	setAttr ".t" -type "double3" 1.4031819443810053e-16 1.3902493148677681 0.25371105641422892 ;
	setAttr ".r" -type "double3" 9.5763298435263593 0 0 ;
createNode transform -n "HipSwingerPelvis_M" -p "HipSwingerOffsetPelvis_M";
	setAttr -k off ".v";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "HipSwingerPelvis_MShape" -p "HipSwingerPelvis_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-4.783346216 -0.6956564234 -3.6496522329999999
		-3.3823365449999998 -0.73903379000000002 0.050718023379999998
		-4.783346216 -0.78241115650000004 3.7510882790000002
		-9.8011651149999996e-16 -0.76970622020000001 2.667274924
		4.783346216 -0.78241115650000004 3.7510882790000002
		3.3823365449999998 -0.73903379000000002 0.050718023379999998
		4.783346216 -0.6956564234 -3.6496522329999999
		1.8166595569999997e-15 -0.70836135960000002 -2.565838877
		-4.783346216 -0.6956564234 -3.6496522329999999
		-3.3823365449999998 -0.73903379000000002 0.050718023379999998
		-4.783346216 -0.78241115650000004 3.7510882790000002
		;
createNode parentConstraint -n "PelvisCenterBtwLegsBlended_M_parentConstraint1" -p
		 "PelvisCenterBtwLegsBlended_M";
	addAttr -ci true -k true -sn "w0" -ln "PelvisCenter_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PelvisCenterBtwLegsOffset_MW1" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0 4.7513626008102703 -0.30196761248010473 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKSystem" -p "MotionSystem";
createNode transform -n "IKParentConstraint" -p "IKSystem";
createNode transform -n "IKParentConstraintShoulder1_L" -p "IKParentConstraint";
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "IKOffsetShoulder1_L" -p "IKParentConstraintShoulder1_L";
	setAttr ".t" -type "double3" 2.8186117422206962 0.59492979900124077 -0.34712698500417805 ;
	setAttr ".r" -type "double3" 13.044524769364708 14.095584200967815 -137.77015510098204 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode joint -n "IKXShoulder1_L" -p "IKOffsetShoulder1_L";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 8.8817841970012523e-16 0 ;
	setAttr ".r" -type "double3" -2.3360542986509009e-15 -7.5068800163687117e-15 -1.1624389887395462e-14 ;
	setAttr ".ro" 5;
createNode joint -n "IKXElbow1_L" -p "IKXShoulder1_L";
	setAttr ".t" -type "double3" -0.0052683376394053738 2.6881635991971726 -0.063794163756091926 ;
	setAttr ".r" -type "double3" 9.2819207209097999e-08 -9.584297497257202e-09 5.4033267436458703e-09 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 116.19976804737917 4.5150794753640042 0.22693079401785224 ;
createNode joint -n "IKXWrist2_L" -p "IKXElbow1_L";
	setAttr ".t" -type "double3" 0.26325195189550321 2.6188878256762003 0.12313942754248397 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 9.907747759068144 -37.967628237510198 -4.9544429196692983 ;
createNode joint -n "IKXWrist1_L" -p "IKXWrist2_L";
	setAttr ".t" -type "double3" 0.016234100558222053 0.66692055569756181 0.083199478632924873 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 0 -1.5663487127822182 ;
createNode transform -n "IKFKAlignedArm_L" -p "IKXWrist2_L";
	setAttr ".t" -type "double3" 1.5258123919892341e-07 3.4583185759551327e-08 -7.5357864837144461e-08 ;
	setAttr ".r" -type "double3" 1.2606358112053045e-06 -4.4431676204530389e-06 9.7759665112018092e-14 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999967 0.999999999999999 1.0000000000000004 ;
createNode orientConstraint -n "IKXWrist2_L_orientConstraint1" -p "IKXWrist2_L";
	addAttr -ci true -k true -sn "w0" -ln "IKArm_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 89.063258559583801 -168.69998695714327 -3.1919063032999433 ;
	setAttr ".o" -type "double3" -90.919848193956767 -3.0079525238324076 168.65024266083981 ;
	setAttr ".rsrr" -type "double3" -180 -180 180 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector1" -p "IKXElbow1_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationArm_L" -p "IKXElbow1_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 90.000005267415617 42.048854314324274 -170.56272712964739 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999978 1 ;
createNode annotationShape -n "PoleAnnotationArm_LShape" -p "PoleAnnotationArm_L";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintShoulder1_L_parentConstraint1" 
		-p "IKParentConstraintShoulder1_L";
	addAttr -ci true -k true -sn "w0" -ln "Chest_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 9.5763298435263593 0 0 ;
	setAttr ".rst" -type "double3" 1.403181944381005e-16 7.7991257400932739 0.26187111942261876 ;
	setAttr ".rsrr" -type "double3" 9.5763298435263593 0 0 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintKnee_R" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "IKOffsetKnee_R" -p "IKParentConstraintKnee_R";
	setAttr ".t" -type "double3" -0.036337162627452901 2.3836548774653208 -0.051143419208276253 ;
	setAttr ".r" -type "double3" 118.81087339565485 0 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode joint -n "IKXKnee_R" -p "IKOffsetKnee_R";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 8.9254808231700773e-08 -5.3296476495369208e-07 3.0305222440375441e-06 ;
	setAttr ".ro" 2;
createNode joint -n "IKXBackKnee_R" -p "IKXKnee_R";
	setAttr ".t" -type "double3" -0.038343975960455801 2.9451069251958155 0.05391143743891913 ;
	setAttr ".r" -type "double3" -23.95896988483647 -1.0487683228244418e-09 -1.0560878597072078e-09 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -95.891108965505865 0 0 ;
	setAttr ".pa" -type "double3" -23.958970013969285 0 0 ;
createNode joint -n "IKXAnkle_R" -p "IKXBackKnee_R";
	setAttr ".t" -type "double3" -0.051469342780706473 3.8906130684037228 -0.54457140111455071 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" 49.265464634195801 0.090816436360310948 -0.19806111852072894 ;
createNode joint -n "IKXFoot_End_R" -p "IKXAnkle_R";
	setAttr ".t" -type "double3" 0.31718775714621339 0.27738757624719901 -1.6117158190438494 ;
	setAttr ".r" -type "double3" -0.00019030234952083469 0.00053514843766093152 25.864574009382626 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -125.44414222104629 9.5527520920513158 173.4955885415965 ;
	setAttr ".pa" -type "double3" -0.00019030234952083469 0.00053514843766093152 25.864574009382626 ;
createNode transform -n "IKFKAlignedLeg_R" -p "IKXAnkle_R";
	setAttr ".t" -type "double3" 2.8853209865786766e-07 -6.4280547884543182e-08 -3.6000602299246509e-07 ;
	setAttr ".r" -type "double3" -5.5695229832162736e-06 0 -3.7014161053772233e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 0.99999999999999922 0.99999999999999911 ;
createNode orientConstraint -n "IKXAnkle_R_orientConstraint1" -p "IKXAnkle_R";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -130.73565236206551 -0.11180929820186257 -0.43333680441227163 ;
	setAttr ".o" -type "double3" 0 -0.31050286089969953 1.4810611873229576e-24 ;
	setAttr ".rsrr" -type "double3" 49.265464634193059 0.090816435582856486 -0.19806111785120076 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector3" -p "IKXAnkle_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector2" -p "IKXBackKnee_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_R" -p "IKXBackKnee_R";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0 3.3306690738754696e-16 ;
	setAttr ".r" -type "double3" -130.73480932324765 -0.11180587887202922 -0.43334099549799154 ;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999956 ;
createNode annotationShape -n "PoleAnnotationLeg_RShape" -p "PoleAnnotationLeg_R";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintKnee_R_parentConstraint1" -p "IKParentConstraintKnee_R";
	addAttr -ci true -k true -sn "w0" -ln "Hip_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 131.77412456864991 -0.17960135312555045 0.2972973516610366 ;
	setAttr ".rst" -type "double3" -2.6434502638157822 4.898900030559993 -0.45186244838401113 ;
	setAttr ".rsrr" -type "double3" 48.225872704160842 179.82040288160513 -179.70269757576483 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintPelvis_M" -p "IKParentConstraint";
	setAttr ".ro" 3;
createNode joint -n "IKXPelvis_M" -p "IKParentConstraintPelvis_M";
	setAttr ".t" -type "double3" 9.8607613152626476e-32 8.8817841970012523e-16 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 0.18682133802676973 -7.2423876097998037e-18 4.8283198056942271e-16 ;
	setAttr ".ro" 3;
createNode joint -n "IKXSpineA_M" -p "IKXPelvis_M";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 1.413083705895736 0.018891738117306822 ;
	setAttr ".r" -type "double3" 0.99016821986203407 -67.463802340144966 -3.1622790927625828 ;
createNode joint -n "IKXSpineB_M" -p "IKXSpineA_M";
	setAttr ".t" -type "double3" 9.8607613152626476e-32 0.7149423105017414 0.030376928917301038 ;
	setAttr ".r" -type "double3" 5.2475523511986557 -67.432540655405248 -3.8303958785695373 ;
createNode joint -n "IKXChest_M" -p "IKXSpineB_M";
createNode transform -n "IKFKAlignedSpine_M" -p "IKXChest_M";
	setAttr ".t" -type "double3" -7.3955709864469857e-32 -0.00057056416629830409 0.022897766421766663 ;
	setAttr ".r" -type "double3" 0.93483031010652229 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode parentConstraint -n "IKXChest_M_parentConstraint1" -p "IKXChest_M";
	addAttr -ci true -k true -sn "w0" -ln "IKfake2Spine_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKSpine4AlignTo_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0.0187182385219565 0 0 ;
	setAttr ".rst" -type "double3" 4.9303806576313238e-32 0.97106713289847058 1.9984014443252818e-15 ;
	setAttr ".rsrr" -type "double3" 0.018718238522027652 0 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode aimConstraint -n "IKXSpineB_M_aimConstraint1" -p "IKXSpineB_M";
	addAttr -ci true -sn "w0" -ln "IKfake2Spine_MW0" -dv 1 -at "double";
	addAttr -ci true -sn "w1" -ln "IKSpine4_MW1" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 0 0 1 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 1.332663756261335 5.4141492086190446e-31 -4.6552464950295825e-29 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "IKfake1Spine_M" -p "IKXSpineA_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 1.2325951644078309e-31 0.7149423105017414 0.030376928917301038 ;
	setAttr ".r" -type "double3" 5.2662705897191486 -67.432540655398768 -3.8303958785679519 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "IKfake2Spine_M" -p "IKfake1Spine_M";
	setAttr ".t" -type "double3" 2.4651903288156619e-32 0.97106708107769091 -0.00031724266683386126 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode ikEffector -n "effector4" -p "IKfake1Spine_M";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode transform -n "IKFake1UpLocSpine_M" -p "IKfake1Spine_M";
	setAttr ".t" -type "double3" 0 1 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999967 ;
createNode locator -n "IKFake1UpLocSpine_MShape" -p "IKFake1UpLocSpine_M";
	setAttr -k off ".v";
createNode parentConstraint -n "IKParentConstraintPelvis_M_parentConstraint1" -p "IKParentConstraintPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine0AlignTo_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -9.8607613152626476e-32 -8.8817841970012523e-16 
		-2.2204460492503131e-16 ;
	setAttr ".tg[0].tor" -type "double3" -0.18682133802677089 0 0 ;
	setAttr ".lr" -type "double3" 9.5763298435263593 0 0 ;
	setAttr ".rst" -type "double3" 1.4031819443810043e-16 4.7513626008102712 -0.30196761248010462 ;
	setAttr ".rsrr" -type "double3" 9.5763298435263593 0 0 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintKnee_L" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "IKOffsetKnee_L" -p "IKParentConstraintKnee_L";
	setAttr ".t" -type "double3" 0.036337162627452901 -2.3836548774653217 0.051143419208277141 ;
	setAttr ".r" -type "double3" 118.81087339565482 0 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode joint -n "IKXKnee_L" -p "IKOffsetKnee_L";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr ".r" -type "double3" 4.2876619146319563e-08 -2.560279885805549e-07 1.4558157786671621e-06 ;
	setAttr ".ro" 2;
createNode joint -n "IKXBackKnee_L" -p "IKXKnee_L";
	setAttr ".t" -type "double3" 0.038343975960455801 -2.9451069251958168 -0.053911437438919574 ;
	setAttr ".r" -type "double3" -23.9589699719854 4.3745196942907023e-10 4.36872029272403e-10 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -95.891108965505865 0 0 ;
	setAttr ".pa" -type "double3" -23.958970013969285 0 0 ;
createNode joint -n "IKXAnkle_L" -p "IKXBackKnee_L";
	setAttr ".t" -type "double3" 0.051469342780709582 -3.8906130684037232 0.54457140111455171 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" 49.265464634195801 0.090816436360310948 -0.19806111852072894 ;
createNode joint -n "IKXFoot_End_L" -p "IKXAnkle_L";
	setAttr ".t" -type "double3" -0.31718775714621295 -0.2773875762471999 1.6117158190438501 ;
	setAttr ".r" -type "double3" -0.00019030234952083469 0.00053514843766093152 25.864574009382626 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -125.44414222104629 9.5527520920513158 173.4955885415965 ;
	setAttr ".pa" -type "double3" -0.00019030234952083469 0.00053514843766093152 25.864574009382626 ;
createNode transform -n "IKFKAlignedLeg_L" -p "IKXAnkle_L";
	setAttr ".t" -type "double3" 1.4194369102327187e-07 -3.5774044837566521e-10 -9.1094234155875142e-08 ;
	setAttr ".r" -type "double3" 0 3.9025133024620663e-06 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999944 0.99999999999999956 ;
createNode orientConstraint -n "IKXAnkle_L_orientConstraint1" -p "IKXAnkle_L";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 49.264343282608699 -0.11180968927705245 -0.43334342138699505 ;
	setAttr ".o" -type "double3" 180 0.31050286089966439 1.4810611873229576e-24 ;
	setAttr ".rsrr" -type "double3" 49.265464634193094 0.090816435580283628 -0.19806111784898503 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector7" -p "IKXAnkle_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector6" -p "IKXBackKnee_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_L" -p "IKXBackKnee_L";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 -1.1102230246251565e-16 ;
	setAttr ".r" -type "double3" 49.265190676752326 -0.11180587887457921 -0.43334099549574912 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999956 0.99999999999999978 ;
createNode annotationShape -n "PoleAnnotationLeg_LShape" -p "PoleAnnotationLeg_L";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintKnee_L_parentConstraint1" -p "IKParentConstraintKnee_L";
	addAttr -ci true -k true -sn "w0" -ln "Hip_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -48.2258709973027 0.17960003437407537 0.29730419414904602 ;
	setAttr ".rst" -type "double3" 2.6434502638157822 4.898900030559993 -0.45186244838401113 ;
	setAttr ".rsrr" -type "double3" -48.225872704160814 0.17959711839486719 0.29730242423516368 ;
	setAttr -k on ".w0";
createNode transform -n "IKHandle" -p "IKSystem";
createNode transform -n "IKParentConstraintArm_L" -p "IKHandle";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "IKExtraArm_L" -p "IKParentConstraintArm_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKArm_L" -p "IKExtraArm_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 -2.2204460492503131e-16 ;
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow";
createNode nurbsCurve -n "IKArm_LShape" -p "IKArm_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-1.765538193 -1.235624517 -1.4563898550000001
		-1.765538193 -3.2265927090000002 1.8768182870000001
		1.765538193 -3.2265927090000002 1.8768182870000001
		1.765538193 0.1066154341 3.8677864799999999
		-1.765538193 0.1066154341 3.8677864799999999
		-1.765538193 2.097583626 0.53457833659999998
		1.765538193 2.097583626 0.53457833659999998
		1.765538193 -1.235624517 -1.4563898550000001
		-1.765538193 -1.235624517 -1.4563898550000001
		-1.765538193 2.097583626 0.53457833659999998
		1.765538193 2.097583626 0.53457833659999998
		1.765538193 0.1066154341 3.8677864799999999
		-1.765538193 0.1038431537 3.8567898540000001
		-1.765538193 -3.215596084 1.874046007
		1.765538193 -3.2265927090000002 1.8768182870000001
		1.765538193 -1.235624517 -1.4563898550000001
		;
createNode ikHandle -n "IKXArmHandle_L" -p "IKArm_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXArmHandle_L_poleVectorConstraint1" -p "IKXArmHandle_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleArm_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.9169127996714033 -1.7322846713012288 -1.5229038189792272 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IKParentConstraintArm_L_parentConstraint1" -p "IKParentConstraintArm_L";
	addAttr -ci true -k true -sn "w0" -ln "IKParentConstraintArm_LStaticW0" -dv 1 -min 
		0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "Chest_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" -4.4408920985006262e-16 0 -2.2204460492503131e-16 ;
	setAttr ".tg[1].tot" -type "double3" 3.9650077580538889 -1.1695596211397765 1.2809974032897229 ;
	setAttr ".tg[1].tor" -type "double3" -9.5763298435263593 0 -1.7655625192200634e-31 ;
	setAttr ".rst" -type "double3" 3.9650077580538889 6.4327555932456599 1.3304480593007249 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PoleParentConstraintArm_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "PoleExtraArm_L" -p "PoleParentConstraintArm_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleArm_L" -p "PoleExtraArm_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow";
createNode nurbsCurve -n "PoleArm_LShape" -p "PoleArm_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		1.6164847240000001e-13 0.13903589229999999 2.0428103649999997e-14
		1.6164847240000001e-13 0.27807178459999998 2.0428103649999997e-14
		1.6164847240000001e-13 2.0783375020000002e-13 0.41710767700000001
		1.6164847240000001e-13 -0.27807178459999998 2.0428103649999997e-14
		1.6164847240000001e-13 -0.13903589229999999 2.0428103649999997e-14
		1.6164847240000001e-13 -0.13903589229999999 -0.41710767700000001
		1.6164847240000001e-13 0.13903589229999999 -0.41710767700000001
		1.6164847240000001e-13 0.13903589229999999 2.0428103649999997e-14
		;
createNode transform -n "PoleAnnotateTargetArm_L" -p "PoleArm_L";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetArm_LShape" -p "PoleAnnotateTargetArm_L";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintArm_L_parentConstraint1" -p "PoleParentConstraintArm_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintArm_LStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKArm_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" -8.8817841970012523e-16 0 0 ;
	setAttr ".tg[1].tot" -type "double3" 0.7705167838382101 0.27847336754491891 -2.8347971251450019 ;
	setAttr ".tg[1].tor" -type "double3" 0 3.1805546814635176e-15 0 ;
	setAttr ".rst" -type "double3" 4.735524541892099 6.711228960790578 -1.5043490658442766 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintLeg_R" -p "IKHandle";
	setAttr ".t" -type "double3" -2.8017597392016085 0.29519035526101423 1.8667328613131036 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999956 0.99999999999999967 ;
createNode transform -n "IKExtraLeg_R" -p "IKParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -5.5511151231257827e-17 2.2204460492503131e-16 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_R" -p "IKExtraLeg_R";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "legAim" -ln "legAim" -dv 10 -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "toe" -ln "toe" -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
	setAttr -k on ".legAim";
createNode nurbsCurve -n "IKLeg_RShape" -p "IKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-1.1329471149999999 -0.25761911479999999 1.7468097090000001
		1.1329471149999999 -0.25761911479999999 1.7468097090000001
		1.1329471149999999 0.93976454210000004 1.7468097090000001
		1.5293311700000001 0.93976454210000004 -0.85533046680000002
		1.5293311700000001 -0.25761911479999999 -0.85533046680000002
		-1.5293311700000001 -0.25761911479999999 -0.85533046680000002
		-1.5293311700000001 0.93976454210000004 -0.85533046680000002
		-1.1329471149999999 0.93976454210000004 1.7468097090000001
		-1.1329471149999999 -0.25761911479999999 1.7468097090000001
		-1.5293311700000001 -0.25761911479999999 -0.85533046680000002
		-1.5293311700000001 0.93976454210000004 -0.85533046680000002
		1.5293311700000001 0.93976454210000004 -0.85533046680000002
		1.523013768 -0.25761911479999999 -0.85427976090000002
		1.1282671070000001 -0.25761911479999999 1.745759002
		1.1329471149999999 0.93976454210000004 1.7468097090000001
		-1.1329471149999999 0.93976454210000004 1.7468097090000001
		;
createNode transform -n "IKFootRollLeg_R" -p "IKLeg_R";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 5.5511151231257827e-17 -2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 0 10.823097701521274 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999967 0.99999999999999978 ;
createNode transform -n "IKRollLegHeel_R" -p "IKFootRollLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.064039542773719482 -0.27963438423998782 -0.30714036389802146 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -1.5902773407317576e-15 0 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000007 1.0000000000000007 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_R" -p "IKRollLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -3.4694469519536142e-18 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -1.5902773407317588e-15 0 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_R" -p "IKExtraLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_RShape" -p "IKLegHeel_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		-1.002780287e-16 -0.45161209270000002 -1.3086588599999999e-16
		-5.1353462759999998e-17 -0.31933797320000001 0.31933797320000001
		2.7653265189999997e-17 -1.3607953719999999e-16 0.45161209270000002
		9.0461085430000003e-17 0.31933797320000001 0.31933797320000001
		1.002780287e-16 0.45161209270000002 2.4256173589999999e-16
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKRollLegBall_R" -p "IKLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.064039542773719482 0.0022469352001403559 1.9497712471937605 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 1.5902773407317576e-15 0 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000002 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_R" -p "IKRollLegBall_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.3322676295501878e-15 0 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -1.5902773407317606e-15 0 ;
	setAttr ".s" -type "double3" 0.99999999999999878 0.99999999999999911 0.99999999999999889 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_R" -p "IKExtraLegBall_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 1.5902773407317574e-15 0 ;
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_RShape" -p "IKLegBall_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.6479442390000003e-16 0.31319083910000001 -0.31319083910000001
		-3.4228030250000001e-16 -8.4025641839999994e-17 -0.44291873230000001
		-4.038790961e-16 -0.31319083910000001 -0.31319083910000001
		-4.1350706700000001e-16 -0.44291873230000001 -2.0038389209999997e-16
		-3.655242803e-16 -0.31319083910000001 0.31319083910000001
		-2.8803840169999997e-16 -1.0662101479999999e-16 0.44291873230000001
		-2.2643960800000002e-16 0.31319083910000001 0.31319083910000001
		-2.1681163720000001e-16 0.44291873230000001 1.055224368e-16
		-2.6479442390000003e-16 0.31319083910000001 -0.31319083910000001
		-3.4228030250000001e-16 -8.4025641839999994e-17 -0.44291873230000001
		-4.038790961e-16 -0.31319083910000001 -0.31319083910000001
		;
createNode transform -n "IKFootPivotBallReverseLeg_R" -p "IKLegBall_R";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -3.4694469519536142e-18 0 ;
	setAttr ".r" -type "double3" 0 -10.823097701521272 0 ;
	setAttr ".s" -type "double3" 1.0000000000000007 1.0000000000000002 1.0000000000000007 ;
createNode ikHandle -n "IKXLegHandle_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -0.30844877529668402 0.27738744903984341 -1.6134111601742822 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_R_poleVectorConstraint1" -p "IKXLegHandle_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -0.022396104679045514 -0.95872300807511701 -2.9807893731770783 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -1.3486456396094582e-10 -2.1493468405076044e-07 -9.6907433633219853e-08 ;
	setAttr ".r" -type "double3" -179.99999688133241 -0.31050286089969842 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999967 0.99999999999999956 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "PoleAimLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 0.99999999999999978 1.0000000000000004 1.0000000000000004 ;
createNode aimConstraint -n "PoleAimLeg_R_aimConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -sn "w0" -ln "IKLeg_RW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" 0.88575017178539217 -26.717885635110338 -91.969475210614789 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_R_pointConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.1766292369888236 4.7521478927657883 -0.30271618773503134 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode transform -n "PoleExtraLeg_R" -p "PoleParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_R" -p "PoleExtraLeg_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 2.2204460492503131e-16 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_RShape" -p "PoleLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-8.1326532040000003e-33 0.16494980340000001 -0.93421645508339402
		-1.6265306409999998e-32 0.32989960689999998 -0.93421645508339413
		2.1975728359999999e-16 1.0987864179999999e-16 -0.439367044783394
		1.6265306409999998e-32 -0.32989960689999998 -0.9342164550833939
		8.1326532040000003e-33 -0.16494980340000001 -0.93421645508339402
		-2.1975728359999999e-16 -0.16494980340000001 -1.4290658653833941
		-2.1975728359999999e-16 0.16494980340000001 -1.4290658653833941
		-8.1326532040000003e-33 0.16494980340000001 -0.93421645508339402
		;
createNode transform -n "PoleAnnotateTargetLeg_R" -p "PoleLeg_R";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_RShape" -p "PoleAnnotateTargetLeg_R";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_R_parentConstraint1" -p "PoleParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_RStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" 1.9503312877053012 -0.98502301366759903 -2.2191385495358782 ;
	setAttr ".tg[1].tor" -type "double3" 25.954762485280394 -1.8588463089387671e-14 
		108.15180153913228 ;
	setAttr ".lr" -type "double3" -2.8624992133171648e-14 1.2722218725854078e-14 5.0888874903416262e-14 ;
	setAttr ".rst" -type "double3" -2.7202298129302722 2.3904847901554578 -1.6212316495387205 ;
	setAttr ".rsrr" -type "double3" -1.5902773407317576e-15 1.033680271475643e-14 7.7526020360673219e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintSpine0_M" -p "IKHandle";
createNode transform -n "IKExtraSpine0_M" -p "IKParentConstraintSpine0_M";
createNode transform -n "IKSpine0_M" -p "IKExtraSpine0_M";
	addAttr -ci true -k true -sn "stiff" -ln "stiff" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".stiff";
createNode nurbsCurve -n "IKSpine0_MShape" -p "IKSpine0_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.0527618590000001 -0.4195080934 -1.0997260449999999
		-1.0527618590000001 -0.4195080934 -1.0997260449999999
		-1.0527618590000001 0.62807597979999996 -0.99544210160000002
		-1.0527618590000001 0.4195080934 1.0997260449999999
		-1.0527618590000001 -0.62807597979999996 0.99544210160000002
		1.0527618590000001 -0.62807597979999996 0.99544210160000002
		1.0527618590000001 0.4195080934 1.0997260449999999
		1.0527618590000001 0.62807597979999996 -0.99544210160000002
		1.0527618590000001 -0.4195080934 -1.0997260449999999
		1.0527618590000001 -0.62807597979999996 0.99544210160000002
		1.0527618590000001 0.4195080934 1.0997260449999999
		-1.0527618590000001 0.4195080934 1.0997260449999999
		-1.0484130819999999 -0.62764520089999998 0.9911147132
		-1.0484130819999999 -0.41993887229999999 -1.095398656
		-1.0527618590000001 0.62807597979999996 -0.99544210160000002
		1.0527618590000001 0.62807597979999996 -0.99544210160000002
		;
createNode transform -n "IKXSpineLocator0_M" -p "IKSpine0_M";
	setAttr -l on ".v" no;
createNode locator -n "IKXSpineLocator0_MShape" -p "IKXSpineLocator0_M";
	setAttr -k off ".v";
createNode transform -n "IKStiffStartOrientSpine_M" -p "IKSpine0_M";
	setAttr ".t" -type "double3" -4.9303806576313238e-32 -8.8817841970012523e-16 -2.7755575615628914e-16 ;
	setAttr ".r" -type "double3" 170.41024068054352 8.8278125961003194e-32 180 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "IKXSpineLocator1_M" -p "IKStiffStartOrientSpine_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 6.2394289208110343e-17 0.5167086124420166 0 ;
	setAttr ".r" -type "double3" 170.41024068054352 -1.1689312381119849e-15 180 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999978 ;
createNode locator -n "IKXSpineLocator1_MShape" -p "IKXSpineLocator1_M";
	setAttr -k off ".v";
createNode transform -n "IKSpine0AlignTo_M" -p "IKSpine0_M";
	setAttr ".t" -type "double3" 4.9303806576313238e-32 8.8817841970012523e-16 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 9.7631511815531304 0 0 ;
createNode transform -n "IKOrientToSpine_M" -p "IKSpine0_M";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 -2.7755575615628914e-16 ;
	setAttr ".r" -type "double3" 9.5763299050662027 0 0 ;
createNode parentConstraint -n "IKParentConstraintSpine0_M_parentConstraint1" -p "IKParentConstraintSpine0_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.4031819443810048e-16 8.8817841970012523e-16 
		2.7755575615628914e-16 ;
	setAttr ".rst" -type "double3" 1.4031819443810048e-16 4.7513626008102712 -0.30196761248010445 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintSpine2_M" -p "IKHandle";
createNode transform -n "IKExtraSpine2_M" -p "IKParentConstraintSpine2_M";
createNode transform -n "IKSpine2_M" -p "IKExtraSpine2_M";
	addAttr -ci true -k true -sn "stiff" -ln "stiff" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".stiff";
createNode nurbsCurve -n "IKSpine2_MShape" -p "IKSpine2_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.0527618590000001 -0.36610476759999999 -1.11863833
		-1.0527618590000001 -0.36610476759999999 -1.11863833
		-1.0527618590000001 0.67524780340000001 -0.964066812
		-1.0527618590000001 0.36610476759999999 1.11863833
		-1.0527618590000001 -0.67524780340000001 0.964066812
		1.0527618590000001 -0.67524780340000001 0.964066812
		1.0527618590000001 0.36610476759999999 1.11863833
		1.0527618590000001 0.67524780340000001 -0.964066812
		1.0527618590000001 -0.36610476759999999 -1.11863833
		1.0527618590000001 -0.67524780340000001 0.964066812
		1.0527618590000001 0.36610476759999999 1.11863833
		-1.0527618590000001 0.36610476759999999 1.11863833
		-1.0484130819999999 -0.67460929520000001 0.95976516479999996
		-1.0484130819999999 -0.36674327579999999 -1.1143366830000001
		-1.0527618590000001 0.67524780340000001 -0.964066812
		1.0527618590000001 0.67524780340000001 -0.964066812
		;
createNode transform -n "IKXSpineLocator2_M" -p "IKSpine2_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -7.3955709864469857e-32 -8.8817841970012523e-16 -7.6327832942979512e-17 ;
	setAttr ".r" -type "double3" -4.7708320221952752e-15 -1.1689312381119853e-15 -9.8052699113874054e-17 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000004 ;
createNode locator -n "IKXSpineLocator2_MShape" -p "IKXSpineLocator2_M";
	setAttr -k off ".v";
createNode parentConstraint -n "IKParentConstraintSpine2_M_parentConstraint1" -p "IKParentConstraintSpine2_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.4031819443810094e-16 1.5241216135413067 0.28359679088715528 ;
	setAttr ".rst" -type "double3" 1.4031819443810094e-16 6.275484214351577 -0.018370821592949449 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintSpine4_M" -p "IKHandle";
createNode transform -n "IKExtraSpine4_M" -p "IKParentConstraintSpine4_M";
createNode transform -n "IKSpine4_M" -p "IKExtraSpine4_M";
	addAttr -ci true -k true -sn "stiff" -ln "stiff" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".stiff";
	setAttr -k on ".stretchy";
createNode nurbsCurve -n "IKSpine4_MShape" -p "IKSpine4_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.0527618590000001 1.099629594 -0.41976084800000002
		-1.0527618590000001 1.099629594 -0.41976084800000002
		-1.0527618590000001 0.99558643489999998 0.62784716659999995
		-1.0527618590000001 -1.099629594 0.41976084800000002
		-1.0527618590000001 -0.99558643489999998 -0.62784716659999995
		1.0527618590000001 -0.99558643489999998 -0.62784716659999995
		1.0527618590000001 -1.099629594 0.41976084800000002
		1.0527618590000001 0.99558643489999998 0.62784716659999995
		1.0527618590000001 1.099629594 -0.41976084800000002
		1.0527618590000001 -0.99558643489999998 -0.62784716659999995
		1.0527618590000001 -1.099629594 0.41976084800000002
		-1.0527618590000001 -1.099629594 0.41976084800000002
		-1.0484130819999999 -0.9912589476 -0.62741738229999999
		-1.0484130819999999 1.095302107 -0.42019063220000002
		-1.0527618590000001 0.99558643489999998 0.62784716659999995
		1.0527618590000001 0.99558643489999998 0.62784716659999995
		;
createNode transform -n "IKXSpineLocator4_M" -p "IKSpine4_M";
	setAttr -l on ".v" no;
createNode locator -n "IKXSpineLocator4_MShape" -p "IKXSpineLocator4_M";
	setAttr -k off ".v";
createNode ikHandle -n "IKXSpineHandle_M" -p "IKSpine4_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 6.2867462971571797e-16 0.00030445328169470542 4.6820103433264926e-05 ;
	setAttr ".r" -type "double3" 166.0474177963392 -44.161589165235867 -170.16649376330892 ;
	setAttr ".roc" yes;
createNode transform -n "IKStiffEndOrientSpine_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" -1.6023737137301802e-30 -8.7041485130612273e-14 -3.1086244689504383e-15 ;
	setAttr ".r" -type "double3" -8.7426944525009151 0 -179.99999987022866 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "IKXSpineLocator3_M" -p "IKStiffEndOrientSpine_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 1.15796038906589e-09 0.51726508140563965 -2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" -8.7426944525009169 -1.972490227895683e-08 179.99999987173649 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
createNode locator -n "IKXSpineLocator3_MShape" -p "IKXSpineLocator3_M";
	setAttr -k off ".v";
createNode transform -n "IKEndJointOrientToSpine_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" -1.553069907153867e-30 0.0040045093575695745 -0.022552095310158532 ;
	setAttr ".r" -type "double3" 8.6414995949596864 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "IKSpine4AlignTo_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" -1.5284180038657104e-30 0.0003044532816955936 4.6820103431599591e-05 ;
	setAttr ".r" -type "double3" 9.9926862875682918 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 0.99999999999999978 ;
createNode transform -n "IKSpine4AlignUnTwistToOffset_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" -1.5037661005775538e-30 0.0003044532816955936 4.682010343154408e-05 ;
	setAttr ".r" -type "double3" 9.763151181553134 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999967 ;
createNode transform -n "IKSpine4AlignUnTwistTo_M" -p "IKSpine4AlignUnTwistToOffset_M";
	setAttr ".t" -type "double3" 0 0 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 1.590277340731758e-15 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode parentConstraint -n "IKParentConstraintSpine4_M_parentConstraint1" -p "IKParentConstraintSpine4_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.4031819443810223e-16 3.0477631392830915 0.56383873190272671 ;
	setAttr ".rst" -type "double3" 1.4031819443810223e-16 7.7991257400933618 0.26187111942262198 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintLeg_L" -p "IKHandle";
	setAttr ".t" -type "double3" 2.8017597392013371 0.29519035526100712 1.8667328613131038 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "IKExtraLeg_L" -p "IKParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -2.2204460492503131e-16 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_L" -p "IKExtraLeg_L";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "legAim" -ln "legAim" -dv 10 -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "toe" -ln "toe" -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
	setAttr -k on ".legAim";
createNode nurbsCurve -n "IKLeg_LShape" -p "IKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.1329471150002708 -0.25761911479999294 1.7468097090000005
		-1.132947114999729 -0.25761911479999294 1.7468097090000005
		-1.132947114999729 0.93976454210000715 1.7468097090000005
		-1.5293311699997292 0.93976454210000715 -0.85533046679999969
		-1.5293311699997292 -0.25761911479999294 -0.85533046679999969
		1.5293311700002707 -0.25761911479999294 -0.85533046679999969
		1.5293311700002707 0.93976454210000715 -0.85533046679999969
		1.1329471150002708 0.93976454210000715 1.7468097090000005
		1.1329471150002708 -0.25761911479999294 1.7468097090000005
		1.5293311700002707 -0.25761911479999294 -0.85533046679999969
		1.5293311700002707 0.93976454210000715 -0.85533046679999969
		-1.5293311699997292 0.93976454210000715 -0.85533046679999969
		-1.5230137679997291 -0.25761911479999294 -0.85427976089999969
		-1.1282671069997292 -0.25761911479999294 1.7457590020000004
		-1.132947114999729 0.93976454210000715 1.7468097090000005
		1.1329471150002708 0.93976454210000715 1.7468097090000005
		;
createNode transform -n "IKFootRollLeg_L" -p "IKLeg_L";
	setAttr ".t" -type "double3" 0 0 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 0 -10.823097701511927 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "IKRollLegHeel_L" -p "IKFootRollLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.064039542773401958 -0.27963438423998144 -0.30714036389808319 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_L" -p "IKRollLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1 0.99999999999999956 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_L" -p "IKExtraLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_LShape" -p "IKLegHeel_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		4.9737991503207013e-14 0.31933797320000085 -0.31933797320000012
		7.1498362785860081e-14 7.7715611723760958e-16 -0.45161209270000024
		5.0182080713057076e-14 -0.31933797319999918 -0.31933797320000024
		-2.2204460492503131e-15 -0.45161209269999919 -2.2204460492503131e-16
		-5.4178883601707639e-14 -0.31933797319999918 0.3193379731999999
		-7.5939254884360707e-14 6.9388939039072284e-16 0.45161209270000002
		-5.4622972811557702e-14 0.31933797320000085 0.31933797320000012
		-2.2204460492503131e-15 0.45161209270000086 0
		4.9737991503207013e-14 0.31933797320000085 -0.31933797320000012
		7.1498362785860081e-14 7.7715611723760958e-16 -0.45161209270000024
		5.0182080713057076e-14 -0.31933797319999918 -0.31933797320000024
		;
createNode transform -n "IKRollLegBall_L" -p "IKLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.06403954277340107 0.0022469352001408538 1.9497712471937731 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 1.590277340731758e-15 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_L" -p "IKRollLegBall_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -6.9388939039072284e-18 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_L" -p "IKExtraLegBall_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_LShape" -p "IKLegBall_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		4.8405723873656825e-14 0.31319083910000012 -0.31319083910000112
		6.9722005946459831e-14 2.5326962749261384e-16 -0.44291873230000078
		4.8405723873656825e-14 -0.31319083909999945 -0.31319083910000112
		-2.2204460492503131e-15 -0.4429187322999994 -1.3322676295501878e-15
		-5.3734794391857577e-14 -0.31319083909999945 0.31319083909999845
		-7.5051076464660582e-14 2.2898349882893854e-16 0.44291873229999812
		-5.3734794391857577e-14 0.31319083910000012 0.31319083909999845
		-2.6645352591003757e-15 0.44291873230000006 -1.3322676295501878e-15
		4.8405723873656825e-14 0.31319083910000012 -0.31319083910000112
		6.9722005946459831e-14 2.5326962749261384e-16 -0.44291873230000078
		4.8405723873656825e-14 -0.31319083909999945 -0.31319083910000112
		;
createNode transform -n "IKFootPivotBallReverseLeg_L" -p "IKLegBall_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -3.4694469519536142e-18 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 0 10.823097701511928 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode ikHandle -n "IKXLegHandle_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0.30844877529641046 0.27738744903983792 -1.6134111601742824 ;
	setAttr ".ro" 3;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_L_poleVectorConstraint1" -p "IKXLegHandle_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0.022396104678988227 -0.95872300807512012 -2.9807893731770791 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 1.3459056091846833e-10 -2.1493469298111689e-07 -9.6907435409576692e-08 ;
	setAttr ".r" -type "double3" 3.1186676635669789e-06 0.31050286089966661 4.7394653954279982e-23 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".hs" 1;
	setAttr ".pv" -type "double3" -5.5511151231257827e-17 0 1 ;
	setAttr ".roc" yes;
createNode transform -n "PoleAimLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1 ;
createNode aimConstraint -n "PoleAimLeg_L_aimConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -sn "w0" -ln "IKLeg_LW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -0.88575017178388793 -26.71788563511036 -88.030524789388551 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_L_pointConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.1766292369888236 4.7521478927657874 -0.30271618773503151 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "PoleExtraLeg_L" -p "PoleParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_L" -p "PoleExtraLeg_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_LShape" -p "PoleLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		1.4788170688007085e-13 0.16494980340000076 -0.93421645508339402
		1.4788170688007085e-13 0.32989960690000064 -0.93421645508339402
		1.4788170688007085e-13 8.8817841970012523e-16 -0.43936704478339395
		1.4788170688007085e-13 -0.32989960689999887 -0.93421645508339402
		1.4788170688007085e-13 -0.16494980339999898 -0.93421645508339402
		1.4788170688007085e-13 -0.16494980339999898 -1.4290658653833936
		1.4788170688007085e-13 0.16494980340000076 -1.4290658653833936
		1.4788170688007085e-13 0.16494980340000076 -0.93421645508339402
		;
createNode transform -n "PoleAnnotateTargetLeg_L" -p "PoleLeg_L";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_LShape" -p "PoleAnnotateTargetLeg_L";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_L_parentConstraint1" -p "PoleParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_LStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" 1.9503312877052068 0.98502301366755463 -2.2191385495358786 ;
	setAttr ".tg[1].tor" -type "double3" 25.954762485280373 -1.0009172432747212e-14 
		71.84819846087062 ;
	setAttr ".lr" -type "double3" -4.7708320221952752e-15 -1.5902773407317584e-15 3.9756933518293969e-16 ;
	setAttr ".rst" -type "double3" 2.7202298129301226 2.3904847901554547 -1.6212316495387205 ;
	setAttr ".rsrr" -type "double3" 1.5902773407317584e-15 3.975693351829396e-15 -9.9392333795734899e-16 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKStatic" -p "IKSystem";
createNode transform -n "IKParentConstraintArm_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 3.9650077580538889 6.4327555932456599 1.3304480593007251 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "PoleParentConstraintArm_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 4.735524541892099 6.7112289607905771 -1.5043490658442766 ;
createNode transform -n "PoleParentConstraintLeg_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -2.7202298129302727 2.3904847901554582 -1.6212316495387205 ;
createNode transform -n "PoleParentConstraintLeg_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 2.7202298129301226 2.3904847901554551 -1.62123164953872 ;
createNode transform -n "IKCrv" -p "IKSystem";
	setAttr ".it" no;
createNode transform -n "IKXSpineCurve_M" -p "IKCrv";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 2.4651903288156619e-32 8.8817841970012523e-16 2.2204460492503131e-16 ;
createNode nurbsCurve -n "IKXSpineCurve_MShape" -p "IKXSpineCurve_M";
	setAttr -k off ".v";
	setAttr -s 5 ".cp";
	setAttr ".cc" -type "nurbsCurve" 
		3 2 0 no 3
		7 0 0 0 1.5499322366102855 3.0998644732205709 3.0998644732205709 3.0998644732205709
		
		5
		1.4031819443810048e-16 4.7513626008102712 -0.30196761248010445
		1.4031819468430083e-16 5.2608506384857217 -0.21588782601793938
		1.4031819443810087e-16 6.2754842143515761 -0.018370821592949525
		7.7464533891223755e-17 7.2878708138831643 0.18324819103987514
		1.4031819443810223e-16 7.7991257400933618 0.26187111942262198
		;
createNode transform -n "IKMessure" -p "IKSystem";
createNode transform -n "FKIKSystem" -p "MotionSystem";
createNode transform -n "FKIKParentConstraintArm_L" -p "FKIKSystem";
createNode transform -n "FKIKArm_L" -p "FKIKParentConstraintArm_L";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend";
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis";
	setAttr -k on ".IKVis" no;
	setAttr -l on ".startJoint" -type "string" "Shoulder1";
	setAttr -l on ".middleJoint" -type "string" "Elbow1";
	setAttr -l on ".endJoint" -type "string" "Wrist2";
createNode nurbsCurve -n "FKIKArm_LShape" -p "FKIKArm_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		3.8949278524887654 1.0703132849999999 -4.5408989071737986e-14
		3.8949278524887654 1.1005257900000001 -4.5408989071737986e-14
		3.8647153474887652 1.1005257900000001 -4.5186944461737992e-14
		3.8345028434887651 1.1307382939999999 -4.496489986173799e-14
		3.8345028434887651 1.493288347 -4.496489986173799e-14
		3.8647153474887652 1.523500852 -4.5186944461737992e-14
		3.8949278524887654 1.523500852 -4.5408989071737986e-14
		3.8949278524887654 1.553713356 -4.5408989071737986e-14
		3.351102772488765 1.553713356 -4.0968096971737988e-14
		3.351102772488765 1.523500852 -4.0968096971737988e-14
		3.3813152774887651 1.523500852 -4.119014157173799e-14
		3.4115277814887652 1.493288347 -4.1523208481737992e-14
		3.4115277814887652 1.3422258250000001 -4.1523208481737992e-14
		3.2302527544887654 1.493288347 -3.9968896251737987e-14
		3.2604652594887651 1.523500852 -4.0190940851737983e-14
		3.2906777634887652 1.523500852 -4.041298546173799e-14
		3.2906777634887652 1.553713356 -4.041298546173799e-14
		3.1094027374887654 1.553713356 -3.8969695521737988e-14
		3.1094027374887654 1.523500852 -3.8969695521737988e-14
		3.139615241488765 1.523500852 -3.9191740131737989e-14
		3.351102772488765 1.3422258250000001 -4.0968096971737988e-14
		3.1094027374887654 1.1005257900000001 -3.8969695521737988e-14
		3.0791902324887652 1.1005257900000001 -3.8636628621737985e-14
		2.9281277104887651 1.553713356 -3.7526405591737991e-14
		2.4749401444887651 1.553713356 -3.375164731173799e-14
		2.4749401444887651 1.523500852 -3.375164731173799e-14
		2.5051526484887652 1.523500852 -3.4084714221737992e-14
		2.5353651534887653 1.493288347 -3.4195736521737986e-14
		2.5353651534887653 1.3422258250000001 -3.4195736521737986e-14
		2.3540901264887655 1.493288347 -3.2752446591737989e-14
		2.3843026314887652 1.523500852 -3.2974491191737985e-14
		2.4145151354887653 1.523500852 -3.3307558101737987e-14
		2.4145151354887653 1.553713356 -3.3307558101737987e-14
		2.233240108488765 1.553713356 -3.1753245861737983e-14
		2.233240108488765 1.523500852 -3.1753245861737983e-14
		2.2634526134887651 1.523500852 -3.197529047173799e-14
		2.4749401444887651 1.3422258250000001 -3.375164731173799e-14
		2.233240108488765 1.1005257900000001 -3.1753245861737983e-14
		2.2030276044887653 1.1005257900000001 -3.1531201261737987e-14
		2.2030276044887653 1.0703132849999999 -3.1531201261737987e-14
		2.3843026314887652 1.0703132849999999 -3.2974491191737985e-14
		2.3843026314887652 1.1005257900000001 -3.2974491191737985e-14
		2.3540901264887655 1.1005257900000001 -3.2752446591737989e-14
		2.5353651534887653 1.2818008160000001 -3.4195736521737986e-14
		2.5353651534887653 1.1307382939999999 -3.4195736521737986e-14
		2.5051526484887652 1.1005257900000001 -3.4084714221737992e-14
		2.4749401444887651 1.1005257900000001 -3.375164731173799e-14
		2.4749401444887651 1.0703132849999999 -3.375164731173799e-14
		2.6864276754887655 1.0703132849999999 -3.5416981851737988e-14
		2.6864276754887655 1.1005257900000001 -3.5416981851737988e-14
		2.6260026664887652 1.1005257900000001 -3.4972892641737985e-14
		2.5957901624887652 1.1307382939999999 -3.4750848031737991e-14
		2.5957901624887652 1.493288347 -3.4750848031737991e-14
		2.6260026664887652 1.523500852 -3.4972892641737985e-14
		2.7166401794887651 1.523500852 -3.5750048751737991e-14
		2.7468526844887649 1.493288347 -3.5972093361737992e-14
		2.7468526844887649 1.1307382939999999 -3.5972093361737992e-14
		2.7166401794887651 1.1005257900000001 -3.5750048751737991e-14
		2.6864276754887655 1.1005257900000001 -3.5416981851737988e-14
		2.6864276754887655 1.0703132849999999 -3.5416981851737988e-14
		2.8677027014887653 1.0703132849999999 -3.6971294081737987e-14
		2.8677027014887653 1.1005257900000001 -3.6971294081737987e-14
		2.8374901974887652 1.1005257900000001 -3.6749249481737991e-14
		2.807277692488765 1.1307382939999999 -3.652720487173799e-14
		2.807277692488765 1.493288347 -3.652720487173799e-14
		2.8374901974887652 1.523500852 -3.6749249481737991e-14
		2.897915206488765 1.523500852 -3.7304360991737988e-14
		3.0489777284887651 1.1005257900000001 -3.841458401173799e-14
		3.0489777284887651 1.0703132849999999 -3.841458401173799e-14
		3.2604652594887651 1.0703132849999999 -4.0190940851737983e-14
		3.2604652594887651 1.1005257900000001 -4.0190940851737983e-14
		3.2302527544887654 1.1005257900000001 -3.9968896251737987e-14
		3.4115277814887652 1.2818008160000001 -4.1523208481737992e-14
		3.4115277814887652 1.1307382939999999 -4.1523208481737992e-14
		3.3813152774887651 1.1005257900000001 -4.119014157173799e-14
		3.351102772488765 1.1005257900000001 -4.0968096971737988e-14
		3.351102772488765 1.0703132849999999 -4.0968096971737988e-14
		3.5323777994887653 1.0703132849999999 -4.2411386901737986e-14
		3.5323777994887653 1.1005257900000001 -4.2411386901737986e-14
		3.5021652944887651 1.1005257900000001 -4.218934230173799e-14
		3.471952790488765 1.1307382939999999 -4.1856275391737994e-14
		3.471952790488765 1.493288347 -4.1856275391737994e-14
		3.5021652944887651 1.523500852 -4.218934230173799e-14
		3.5625903034887654 1.523500852 -4.2633431511737986e-14
		3.5625903034887654 1.4328633390000001 -4.2633431511737986e-14
		3.5928028084887651 1.4328633390000001 -4.2855476111737989e-14
		3.5928028084887651 1.493288347 -4.2855476111737989e-14
		3.6230153124887652 1.523500852 -4.318854302173799e-14
		3.7438653304887652 1.523500852 -4.4187743741737985e-14
		3.7740778344887653 1.493288347 -4.4409788351737986e-14
		3.7740778344887653 1.3422258250000001 -4.4409788351737986e-14
		3.683440321488765 1.3422258250000001 -4.3632632231737987e-14
		3.6532278164887653 1.37243833 -4.3410587621737993e-14
		3.6532278164887653 1.4026508339999999 -4.3410587621737993e-14
		3.6230153124887652 1.4026508339999999 -4.318854302173799e-14
		3.6230153124887652 1.251588312 -4.318854302173799e-14
		3.6532278164887653 1.251588312 -4.3410587621737993e-14
		3.6532278164887653 1.2818008160000001 -4.3410587621737993e-14
		3.683440321488765 1.312013321 -4.3632632231737987e-14
		3.7740778344887653 1.312013321 -4.4409788351737986e-14
		3.7740778344887653 1.1307382939999999 -4.4409788351737986e-14
		3.7438653304887652 1.1005257900000001 -4.4187743741737985e-14
		3.7136528254887651 1.1005257900000001 -4.3965699141737995e-14
		3.7136528254887651 1.0703132849999999 -4.3965699141737995e-14
		3.8949278524887654 1.0703132849999999 -4.5408989071737986e-14
		;
createNode parentConstraint -n "FKIKParentConstraintArm_L_parentConstraint1" -p "FKIKParentConstraintArm_L";
	addAttr -ci true -k true -sn "w0" -ln "Chest_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 2.137304556457341 -0.66484343655621192 -0.14579087062443463 ;
	setAttr ".tg[0].tor" -type "double3" -9.5763298435263593 0 0 ;
	setAttr ".rst" -type "double3" 2.137304556457341 7.1678009700679359 0.0075075846112058198 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintLeg_R" -p "FKIKSystem";
createNode transform -n "FKIKLeg_R" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Knee";
	setAttr -l on ".middleJoint" -type "string" "BackKnee";
	setAttr -l on ".endJoint" -type "string" "Ankle";
createNode nurbsCurve -n "FKIKLeg_RShape" -p "FKIKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-4.0073235661999282 2.2084346627802951 8.6382808012515656e-16
		-4.0073235661999282 2.2308096286802952 8.6382808012515656e-16
		-3.9849486001999281 2.2308096286802952 8.687963206251565e-16
		-3.9625736341999285 2.2531845946802949 8.7376456112515644e-16
		-3.9625736341999285 2.5216841857802952 8.7376456112515644e-16
		-3.9849486001999281 2.5440591516802953 8.687963206251565e-16
		-4.0073235661999282 2.5440591516802953 8.6382808012515656e-16
		-4.0073235661999282 2.5664341175802945 8.6382808012515656e-16
		-3.6045741793999282 2.5664341175802945 9.5325640862515654e-16
		-3.6045741793999282 2.5440591516802953 9.5325640862515654e-16
		-3.6269491453999283 2.5440591516802953 9.482881681251565e-16
		-3.6493241111999284 2.5216841857802952 9.4331992762515646e-16
		-3.6493241111999284 2.4098093560802951 9.4331992762515646e-16
		-3.5150743156999282 2.5216841857802952 9.7312937052515635e-16
		-3.5374492815999283 2.5440591516802953 9.6816113002515651e-16
		-3.5598242475999284 2.5440591516802953 9.6319288952515647e-16
		-3.5598242475999284 2.5664341175802945 9.6319288952515647e-16
		-3.4255744519999283 2.5664341175802945 9.9300233232515661e-16
		-3.4255744519999283 2.5440591516802953 9.9300233232515661e-16
		-3.4479494178999284 2.5440591516802953 9.8803409192515652e-16
		-3.6045741793999282 2.4098093560802951 9.5325640862515654e-16
		-3.4255744519999283 2.2308096286802952 9.9300233232515661e-16
		-3.4031994860999282 2.2308096286802952 9.9797057282515645e-16
		-3.2913246563999285 2.5664341175802945 1.0228117751751565e-15
		-2.9557001675999284 2.5664341175802945 1.0973353822151564e-15
		-2.9557001675999284 2.5440591516802953 1.0973353822151564e-15
		-2.978075133599928 2.5440591516802953 1.0923671417451565e-15
		-3.0004500993999281 2.5216841857802952 1.0873989012651565e-15
		-3.0004500993999281 2.4098093560802951 1.0873989012651565e-15
		-2.8662003038299284 2.5216841857802952 1.1172083440981566e-15
		-2.8885752698199285 2.5440591516802953 1.1122401036261566e-15
		-2.9109502355999282 2.5440591516802953 1.1072718631551565e-15
		-2.9109502355999282 2.5664341175802945 1.1072718631551565e-15
		-2.776700440079928 2.5664341175802945 1.1370813059651564e-15
		-2.776700440079928 2.5440591516802953 1.1370813059651564e-15
		-2.7990754060639285 2.5440591516802953 1.1321130654951566e-15
		-2.9557001675999284 2.4098093560802951 1.0973353822151564e-15
		-2.776700440079928 2.2308096286802952 1.1370813059651564e-15
		-2.7543254742899284 2.2308096286802952 1.1420495464351565e-15
		-2.7543254742899284 2.2084346627802951 1.1420495464351565e-15
		-2.8885752698199285 2.2084346627802951 1.1122401036261566e-15
		-2.8885752698199285 2.2308096286802952 1.1122401036261566e-15
		-2.8662003038299284 2.2308096286802952 1.1172083440981566e-15
		-3.0004500993999281 2.3650594242802949 1.0873989012651565e-15
		-3.0004500993999281 2.2531845946802949 1.0873989012651565e-15
		-2.978075133599928 2.2308096286802952 1.0923671417451565e-15
		-2.9557001675999284 2.2308096286802952 1.0973353822151564e-15
		-2.9557001675999284 2.2084346627802951 1.0973353822151564e-15
		-3.1123249290999282 2.2084346627802951 1.0625576989351566e-15
		-3.1123249290999282 2.2308096286802952 1.0625576989351566e-15
		-3.0675749970999284 2.2308096286802952 1.0724941798751565e-15
		-3.0452000312999283 2.2531845946802949 1.0774624203251566e-15
		-3.0452000312999283 2.5216841857802952 1.0774624203251566e-15
		-3.0675749970999284 2.5440591516802953 1.0724941798751565e-15
		-3.1346998948999283 2.5440591516802953 1.0575894584651565e-15
		-3.1570748608999284 2.5216841857802952 1.0526212179851565e-15
		-3.1570748608999284 2.2531845946802949 1.0526212179851565e-15
		-3.1346998948999283 2.2308096286802952 1.0575894584651565e-15
		-3.1123249290999282 2.2308096286802952 1.0625576989351566e-15
		-3.1123249290999282 2.2084346627802951 1.0625576989351566e-15
		-3.2465747245999284 2.2084346627802951 1.0327482561051564e-15
		-3.2465747245999284 2.2308096286802952 1.0327482561051564e-15
		-3.2241997586999283 2.2308096286802952 1.0377164965751565e-15
		-3.2018247927999282 2.2531845946802949 1.0426847370451566e-15
		-3.2018247927999282 2.5216841857802952 1.0426847370451566e-15
		-3.2241997586999283 2.5440591516802953 1.0377164965751565e-15
		-3.2689496904999285 2.5440591516802953 1.0277800156351564e-15
		-3.3808245201999281 2.2308096286802952 1.0029388133251565e-15
		-3.3808245201999281 2.2084346627802951 1.0029388133251565e-15
		-3.5374492815999283 2.2084346627802951 9.6816113002515651e-16
		-3.5374492815999283 2.2308096286802952 9.6816113002515651e-16
		-3.5150743156999282 2.2308096286802952 9.7312937052515635e-16
		-3.6493241111999284 2.3650594242802949 9.4331992762515646e-16
		-3.6493241111999284 2.2531845946802949 9.4331992762515646e-16
		-3.6269491453999283 2.2308096286802952 9.482881681251565e-16
		-3.6045741793999282 2.2308096286802952 9.5325640862515654e-16
		-3.6045741793999282 2.2084346627802951 9.5325640862515654e-16
		-3.7388239749999284 2.2084346627802951 9.2344696582515659e-16
		-3.7388239749999284 2.2308096286802952 9.2344696582515659e-16
		-3.7164490089999282 2.2308096286802952 9.2841520622515648e-16
		-3.6940740431999282 2.2531845946802949 9.3338344672515653e-16
		-3.6940740431999282 2.5216841857802952 9.3338344672515653e-16
		-3.7164490089999282 2.5440591516802953 9.2841520622515648e-16
		-3.761198940999928 2.5440591516802953 9.1847872532515636e-16
		-3.761198940999928 2.476934253880295 9.1847872532515636e-16
		-3.7835739066999281 2.476934253880295 9.1351048482515651e-16
		-3.7835739066999281 2.5216841857802952 9.1351048482515651e-16
		-3.8059488731999283 2.5440591516802953 9.0854224442515643e-16
		-3.8954487361999282 2.5440591516802953 8.8866928252515641e-16
		-3.9178237021999283 2.5216841857802952 8.8370104202515647e-16
		-3.9178237021999283 2.4098093560802951 8.8370104202515647e-16
		-3.8506988051999285 2.4098093560802951 8.9860576342515654e-16
		-3.8283238391999284 2.4321843220802952 9.0357400392515638e-16
		-3.8283238391999284 2.4545592879802949 9.0357400392515638e-16
		-3.8059488731999283 2.4545592879802949 9.0854224442515643e-16
		-3.8059488731999283 2.3426844583802948 9.0854224442515643e-16
		-3.8283238391999284 2.3426844583802948 9.0357400392515638e-16
		-3.8283238391999284 2.3650594242802949 9.0357400392515638e-16
		-3.8506988051999285 2.387434390180295 8.9860576342515654e-16
		-3.9178237021999283 2.387434390180295 8.8370104202515647e-16
		-3.9178237021999283 2.2531845946802949 8.8370104202515647e-16
		-3.8954487361999282 2.2308096286802952 8.8866928252515641e-16
		-3.8730737701999285 2.2308096286802952 8.936375229251565e-16
		-3.8730737701999285 2.2084346627802951 8.936375229251565e-16
		-4.0073235661999282 2.2084346627802951 8.6382808012515656e-16
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_R_parentConstraint1" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "Hip_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.2248890343291803 3.0091356459498875 0.46568825739144915 ;
	setAttr ".tg[0].tor" -type "double3" -131.77398744744158 -0.11964673200470199 -0.43124200214438679 ;
	setAttr ".lr" -type "double3" -2.7377686890644741e-06 -4.5172906605737895e-07 3.3707675487032288e-06 ;
	setAttr ".rst" -type "double3" -1.4402744069602544 2.5427198141116385 1.490532640241307 ;
	setAttr ".rsrr" -type "double3" 4.7708320221688033e-15 -3.2886935406332764e-12 9.2236085762441977e-13 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintSpine_M" -p "FKIKSystem";
createNode transform -n "FKIKSpine_M" -p "FKIKParentConstraintSpine_M";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend";
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis";
	setAttr -k on ".IKVis" no;
	setAttr -l on ".startJoint" -type "string" "Pelvis";
	setAttr -l on ".middleJoint" -type "string" "SpineA";
	setAttr -l on ".endJoint" -type "string" "Chest";
createNode nurbsCurve -n "FKIKSpine_MShape" -p "FKIKSpine_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		1.1539535856774554 0.26912301769175384 -1.1468663610000001e-16
		1.1539535856774554 0.28756954269175394 -1.1468663610000001e-16
		1.1724001106774553 0.28756954269175394 -1.1059068480000001e-16
		1.1908466346774551 0.30601606669175396 -1.064947335e-16
		1.1908466346774551 0.52737436369175406 -1.064947335e-16
		1.1724001106774553 0.54582088869175394 -1.1059068480000001e-16
		1.1539535856774554 0.54582088869175394 -1.1468663610000001e-16
		1.1539535856774554 0.56426741269175396 -1.1468663610000001e-16
		1.4859910296774554 0.56426741269175396 -4.0959512880000004e-17
		1.4859910296774554 0.54582088869175394 -4.0959512880000004e-17
		1.4675445056774552 0.54582088869175394 -4.5055464169999994e-17
		1.4490979806774553 0.52737436369175406 -4.9151415460000003e-17
		1.4490979806774553 0.43514173969175385 -4.9151415460000003e-17
		1.5597771286774553 0.52737436369175406 -2.457570772e-17
		1.5413306046774551 0.54582088869175394 -2.8671659009999997e-17
		1.5228840796774552 0.54582088869175394 -3.2767610299999999e-17
		1.5228840796774552 0.56426741269175396 -3.2767610299999999e-17
		1.6335632276774552 0.56426741269175396 -8.191902577000001e-18
		1.6335632276774552 0.54582088869175394 -8.191902577000001e-18
		1.6151167026774553 0.54582088869175394 -1.228785386e-17
		1.4859910296774554 0.43514173969175385 -4.0959512880000004e-17
		1.6335632276774552 0.28756954269175394 -8.191902577000001e-18
		1.6520097526774551 0.28756954269175394 -4.0959512880000006e-18
		1.7442423756774552 0.56426741269175396 1.638380515e-17
		2.0209402466774553 0.56426741269175396 7.7823074480000001e-17
		2.0209402466774553 0.54582088869175394 7.7823074480000001e-17
		2.0024937216774554 0.54582088869175394 7.3727123189999999e-17
		1.9840471966774551 0.52737436369175406 6.9631171899999996e-17
		1.9840471966774551 0.43514173969175385 6.9631171899999996e-17
		2.0947263446774551 0.52737436369175406 9.4206879640000011e-17
		2.0762798206774553 0.54582088869175394 9.0110928349999996e-17
		2.0578332956774554 0.54582088869175394 8.6014977059999994e-17
		2.0578332956774554 0.56426741269175396 8.6014977059999994e-17
		2.1685124436774554 0.56426741269175396 1.1059068480000001e-16
		2.1685124436774554 0.54582088869175394 1.1059068480000001e-16
		2.1500659186774551 0.54582088869175394 1.064947335e-16
		2.0209402466774553 0.43514173969175385 7.7823074480000001e-17
		2.1685124436774554 0.28756954269175394 1.1059068480000001e-16
		2.1869589686774553 0.28756954269175394 1.1468663610000001e-16
		2.1869589686774553 0.26912301769175384 1.1468663610000001e-16
		2.0762798206774553 0.26912301769175384 9.0110928349999996e-17
		2.0762798206774553 0.28756954269175394 9.0110928349999996e-17
		2.0947263446774551 0.28756954269175394 9.4206879640000011e-17
		1.9840471966774551 0.39824869069175395 6.9631171899999996e-17
		1.9840471966774551 0.30601606669175396 6.9631171899999996e-17
		2.0024937216774554 0.28756954269175394 7.3727123189999999e-17
		2.0209402466774553 0.28756954269175394 7.7823074480000001e-17
		2.0209402466774553 0.26912301769175384 7.7823074480000001e-17
		1.8918145736774554 0.26912301769175384 4.9151415460000003e-17
		1.8918145736774554 0.28756954269175394 4.9151415460000003e-17
		1.9287076226774551 0.28756954269175394 5.7343318039999996e-17
		1.9471541476774554 0.30601606669175396 6.1439269320000003e-17
		1.9471541476774554 0.52737436369175406 6.1439269320000003e-17
		1.9287076226774551 0.54582088869175394 5.7343318039999996e-17
		1.8733680486774551 0.54582088869175394 4.5055464169999994e-17
		1.8549215236774552 0.52737436369175406 4.0959512880000004e-17
		1.8549215236774552 0.30601606669175396 4.0959512880000004e-17
		1.8733680486774551 0.28756954269175394 4.5055464169999994e-17
		1.8918145736774554 0.28756954269175394 4.9151415460000003e-17
		1.8918145736774554 0.26912301769175384 4.9151415460000003e-17
		1.7811354256774554 0.26912301769175384 2.457570772e-17
		1.7811354256774554 0.28756954269175394 2.457570772e-17
		1.7995819496774552 0.28756954269175394 2.8671659009999997e-17
		1.8180284746774551 0.30601606669175396 3.2767610299999999e-17
		1.8180284746774551 0.52737436369175406 3.2767610299999999e-17
		1.7995819496774552 0.54582088869175394 2.8671659009999997e-17
		1.7626889006774551 0.54582088869175394 2.0479756430000001e-17
		1.6704562766774553 0.28756954269175394 0
		1.6704562766774553 0.26912301769175384 0
		1.5413306046774551 0.26912301769175384 -2.8671659009999997e-17
		1.5413306046774551 0.28756954269175394 -2.8671659009999997e-17
		1.5597771286774553 0.28756954269175394 -2.457570772e-17
		1.4490979806774553 0.39824869069175395 -4.9151415460000003e-17
		1.4490979806774553 0.30601606669175396 -4.9151415460000003e-17
		1.4675445056774552 0.28756954269175394 -4.5055464169999994e-17
		1.4859910296774554 0.28756954269175394 -4.0959512880000004e-17
		1.4859910296774554 0.26912301769175384 -4.0959512880000004e-17
		1.3753118816774554 0.26912301769175384 -6.5535220609999993e-17
		1.3753118816774554 0.28756954269175394 -6.5535220609999993e-17
		1.3937584066774553 0.28756954269175394 -6.1439269320000003e-17
		1.4122049316774552 0.30601606669175396 -5.7343318039999996e-17
		1.4122049316774552 0.52737436369175406 -5.7343318039999996e-17
		1.3937584066774553 0.54582088869175394 -6.1439269320000003e-17
		1.3568653576774552 0.54582088869175394 -6.9631171899999996e-17
		1.3568653576774552 0.49048131369175385 -6.9631171899999996e-17
		1.3384188326774553 0.49048131369175385 -7.3727123189999999e-17
		1.3384188326774553 0.52737436369175406 -7.3727123189999999e-17
		1.3199723076774554 0.54582088869175394 -7.7823074480000001e-17
		1.2461862086774551 0.54582088869175394 -9.4206879640000011e-17
		1.2277396846774553 0.52737436369175406 -9.8302830930000002e-17
		1.2277396846774553 0.43514173969175385 -9.8302830930000002e-17
		1.2830792586774553 0.43514173969175385 -8.6014977059999994e-17
		1.3015257826774551 0.45358826469175395 -8.1919025770000004e-17
		1.3015257826774551 0.47203478969175405 -8.1919025770000004e-17
		1.3199723076774554 0.47203478969175405 -7.7823074480000001e-17
		1.3199723076774554 0.37980216569175385 -7.7823074480000001e-17
		1.3015257826774551 0.37980216569175385 -8.1919025770000004e-17
		1.3015257826774551 0.39824869069175395 -8.1919025770000004e-17
		1.2830792586774553 0.41669521569175405 -8.6014977059999994e-17
		1.2277396846774553 0.41669521569175405 -9.8302830930000002e-17
		1.2277396846774553 0.30601606669175396 -9.8302830930000002e-17
		1.2461862086774551 0.28756954269175394 -9.4206879640000011e-17
		1.2646327336774554 0.28756954269175394 -9.0110928349999996e-17
		1.2646327336774554 0.26912301769175384 -9.0110928349999996e-17
		1.1539535856774554 0.26912301769175384 -1.1468663610000001e-16
		;
createNode parentConstraint -n "FKIKParentConstraintSpine_M_parentConstraint1" -p
		 "FKIKParentConstraintSpine_M";
	addAttr -ci true -k true -sn "w0" -ln "FKPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.2842671381712725 0.77485027431619091 0.012236734856032827 ;
	setAttr ".tg[0].tor" -type "double3" -9.576329905066201 0 0 ;
	setAttr ".rst" -type "double3" 1.2842671381712727 5.5133794989514682 -0.16099622447856438 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintLeg_L" -p "FKIKSystem";
createNode transform -n "FKIKLeg_L" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Knee";
	setAttr -l on ".middleJoint" -type "string" "BackKnee";
	setAttr -l on ".endJoint" -type "string" "Ankle";
createNode nurbsCurve -n "FKIKLeg_LShape" -p "FKIKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		4.007323400223977 2.2084345684396776 3.1468511596521864e-07
		4.0073234015403205 2.2308095343396777 3.129479744234942e-07
		3.9849484355403195 2.2308095356560185 3.1129376587557545e-07
		3.9625734708566611 2.2531845029723594 3.0790241578593225e-07
		3.9625734866527598 2.5216840940723588 2.8705671706319436e-07
		3.9849484539691007 2.5440590586560172 2.8697378406938867e-07
		4.0073234199691017 2.5440590573396755 2.8862799239526282e-07
		4.0073234212854425 2.5664340232396747 2.8689085085353838e-07
		3.6045740344854442 2.5664340469338209 2.571150983232684e-07
		3.6045740331691025 2.5440590810338217 2.5885223964294823e-07
		3.6269489991691026 2.5440590797174791 2.6050644819086699e-07
		3.649323963652761 2.5216841125011391 2.6389779828051019e-07
		3.6493239570710534 2.4098092828011395 2.7258350598913239e-07
		3.5150741681527622 2.5216841203991867 2.5397254743708686e-07
		3.537449135369104 2.5440590849828451 2.5388961422123657e-07
		3.5598241013691041 2.5440590836665034 2.5554382276915533e-07
		3.5598241026854458 2.5664340495665026 2.5380668122743089e-07
		3.4255743070854452 2.566434057464551 2.4388143038400756e-07
		3.4255743057691044 2.5440590915645518 2.45618571925732e-07
		3.4479492716691036 2.5440590902482101 2.4727278047365076e-07
		3.6045740252710532 2.409809285433822 2.6927508911533948e-07
		3.4255742873403232 2.2308095685645539 2.6993855373191877e-07
		3.4031993214403222 2.2308095698808947 2.6828434518400002e-07
		3.2913245114854464 2.5664340653626003 2.3395617954058423e-07
		2.9557000226854466 2.5664340851077205 2.0914305220998131e-07
		2.9557000213691058 2.5440591192077213 2.1088019375170575e-07
		2.978074987369105 2.5440591178913796 2.125344022996245e-07
		3.0004499518527634 2.5216841506750387 2.159257523892677e-07
		3.0004499452710558 2.40980932097504 2.246114600978899e-07
		2.8662001562827655 2.5216841585730871 2.0600050132379977e-07
		2.8885751235891073 2.5440591231567455 2.0591756832999408e-07
		2.9109500893691056 2.5440591218404047 2.0757177665586823e-07
		2.9109500906854464 2.5664340877404039 2.0583463511414379e-07
		2.7767002951654485 2.5664340956384515 1.9590938427072047e-07
		2.7767002938491068 2.5440591297384523 1.9764652581244491e-07
		2.7990752598331072 2.5440591284221115 1.9930073436036366e-07
		2.9557000134710565 2.4098093236077225 2.21303043224097e-07
		2.7767002754203256 2.2308096067384535 2.2196650784067629e-07
		2.7543253096303264 2.2308096080547952 2.2031229929275753e-07
		2.7543253083139829 2.2084346421547951 2.2204944083448197e-07
		2.8885751038439826 2.2084346342567476 2.319746918999499e-07
		2.8885751051603261 2.2308096001567468 2.3023755013618086e-07
		2.866200139170326 2.2308096014730894 2.2858334181030671e-07
		3.0004499426383733 2.3650593891750398 2.2808574340338339e-07
		3.0004499360566657 2.2531845595750402 2.3677145111200559e-07
		2.9780749689403239 2.2308095948913818 2.3685438410581128e-07
		2.9557000029403246 2.2308095962077226 2.3520017577993713e-07
		2.9557000016239821 2.2084346303077234 2.3693731732166157e-07
		3.1123247631239819 2.2084346210933332 2.4851677671300365e-07
		3.1123247644403245 2.2308095869933333 2.4677963517127921e-07
		3.0675748324403243 2.2308095896260158 2.434712180754417e-07
		3.0451998679566659 2.2531845569423568 2.400798679857985e-07
		3.0451998837527636 2.5216841480423553 2.1923416926306061e-07
		3.0675748508691054 2.5440591126260137 2.1915123626925492e-07
		3.1346997486691048 2.5440591086769895 2.2411386169096659e-07
		3.1570747133527632 2.5216841414606486 2.2750521178060978e-07
		3.1570746975566655 2.2531845503606491 2.4835091050334768e-07
		3.1346997302403237 2.2308095856769916 2.4843384371919797e-07
		3.1123247644403245 2.2308095869933333 2.4677963517127921e-07
		3.1123247631239819 2.2084346210933332 2.4851677671300365e-07
		3.2465745586239816 2.2084346131952848 2.5844202733438237e-07
		3.2465745599403242 2.2308095790952849 2.5670488601470254e-07
		3.2241995940403241 2.2308095804116266 2.5505067746678378e-07
		3.2018246294566657 2.2531845477279675 2.5165932737714058e-07
		3.2018246452527634 2.5216841388279669 2.3081362865440269e-07
		3.2241996124691052 2.5440591034116253 2.3073069566059701e-07
		3.2689495442691046 2.5440591007789419 2.3403911253438991e-07
		3.380824355540323 2.2308095711972364 2.6663013685812587e-07
		3.3808243542239795 2.2084346052972363 2.6836727839985031e-07
		3.5374491156239802 2.2084345960828471 2.7994673779119239e-07
		3.5374491169403228 2.2308095619828463 2.7820959624946795e-07
		3.5150741510403227 2.2308095632991871 2.7655538770154919e-07
		3.64932395443837 2.3650593510011402 2.7605778929462588e-07
		3.6493239478566624 2.2531845214011397 2.8474349700324808e-07
		3.6269489807403215 2.2308095567174813 2.8482643021909837e-07
		3.6045740147403214 2.230809558033823 2.8317222167117961e-07
		3.6045740134239797 2.2084345921338238 2.8490936321290405e-07
		3.7388238090239785 2.2084345842357744 2.9483461427837199e-07
		3.738823810340322 2.2308095501357745 2.9309747251460294e-07
		3.716448844340321 2.2308095514521153 2.9144326418872879e-07
		3.6940738798566626 2.2531845187684572 2.8805191409908559e-07
		3.6940738956527603 2.5216841098684557 2.672062151543031e-07
		3.7164488627691021 2.5440590744521141 2.6712328216049741e-07
		3.7611987947691023 2.5440590718194307 2.7043169903429032e-07
		3.7611987908200781 2.4769341740194322 2.7564312388150825e-07
		3.7835737565200773 2.4769341727030914 2.77297332429427e-07
		3.7835737591527607 2.5216841046030907 2.7382304912393352e-07
		3.8059487269691026 2.5440590691867482 2.7374011613012783e-07
		3.895448589969102 2.5440590639213823 2.8035695009975825e-07
		3.9178235546527604 2.5216840967050413 2.8374830018940145e-07
		3.9178235480710528 2.4098092670050417 2.9243400789802365e-07
		3.8506986510710535 2.4098092709540659 2.8747138247631199e-07
		3.8283236863873951 2.4321842382704078 2.8408003238666879e-07
		3.8283236877037359 2.4545592041704078 2.8234289106698895e-07
		3.8059487217037358 2.4545592054867496 2.806886825190702e-07
		3.805948715122029 2.3426843758867499 2.893743902276924e-07
		3.8283236811220283 2.3426843745704082 2.9102859877561116e-07
		3.82832368243837 2.3650593404704083 2.8929145723388672e-07
		3.8506986497547118 2.3874343050540667 2.8920852401803643e-07
		3.9178235467547111 2.3874343011050425 2.9417114943974809e-07
		3.9178235388566618 2.2531845056050419 3.0459399891213934e-07
		3.8954485715403209 2.2308095409213835 3.0467693190594503e-07
		3.8730736055403217 2.2308095422377261 3.0302272358007087e-07
		3.8730736042239791 2.208434576337726 3.0475986512179531e-07
		4.007323400223977 2.2084345684396776 3.1468511596521864e-07
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_L_parentConstraint1" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "Hip_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.2248890343291929 -3.0091356459498755 -0.46568825739145492 ;
	setAttr ".tg[0].tor" -type "double3" 48.226012552558402 -0.1196467320077315 -0.43124200214243275 ;
	setAttr ".lr" -type "double3" 1.7105458244911751e-06 4.2359466770762899e-06 1.1737512870107947e-06 ;
	setAttr ".rst" -type "double3" 1.4402744069601143 2.5427198141117184 1.4905326402413057 ;
	setAttr ".rsrr" -type "double3" 3.1805546814456333e-15 2.6875687058366721e-12 -7.6253798488087825e-13 ;
	setAttr -k on ".w0";
createNode transform -n "RootSystem" -p "MotionSystem";
createNode transform -n "PelvisCenter_M" -p "RootSystem";
	setAttr ".t" -type "double3" 0 4.7513626008102703 -0.30196761248010473 ;
	setAttr ".ro" 3;
createNode transform -n "PelvisCenterBtwLegs_M" -p "RootSystem";
	setAttr ".t" -type "double3" -1.354472090042691e-13 4.7513626008102703 1.8667328613131038 ;
	setAttr ".ro" 3;
createNode orientConstraint -n "PelvisCenterBtwLegs_M_orientConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "PelvisCenterBtwLegs_M_pointConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -1.354472090042691e-13 0.29519035526101067 1.8667328613131038 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PelvisCenterBtwLegsOffset_M" -p "PelvisCenterBtwLegs_M";
	setAttr ".t" -type "double3" 1.354472090042691e-13 0 -2.1687004737932085 ;
	setAttr ".ro" 3;
createNode transform -n "TwistSystem" -p "MotionSystem";
createNode transform -n "TwistFollowPelvis_M" -p "TwistSystem";
	setAttr -l on ".v" no;
	setAttr ".ro" 3;
createNode parentConstraint -n "TwistFollowPelvis_M_parentConstraint1" -p "TwistFollowPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine0_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.4031819443810048e-16 4.7513626008102712 -0.30196761248010445 ;
	setAttr -k on ".w0";
createNode joint -n "UnTwistPelvis_M" -p "TwistFollowPelvis_M";
	setAttr ".t" -type "double3" -4.9303806576313238e-32 -8.8817841970012523e-16 -2.7755575615628914e-16 ;
	setAttr ".r" -type "double3" -0.90476193421606865 180 -2.6384254224049364e-24 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 9.5763298435263593 0 0 ;
createNode joint -n "UnTwistEndPelvis_M" -p "UnTwistPelvis_M";
	setAttr ".t" -type "double3" -5.9948205165698737e-18 3.0994796770875173 -9.3445006354997417e-06 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode orientConstraint -n "UnTwistEndPelvis_M_orientConstraint1" -p "UnTwistEndPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine4AlignUnTwistTo_MW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -0.71794059618928563 -180 -7.0167092985348752e-15 ;
	setAttr ".rsrr" -type "double3" -0.71794059618928563 -180 -7.0167092985348752e-15 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector5" -p "UnTwistPelvis_M";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode ikHandle -n "UnTwistIKPelvis_M" -p "TwistFollowPelvis_M";
	setAttr ".r" -type "double3" 169.51890822225678 8.8278125961003172e-32 180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".roc" yes;
createNode pointConstraint -n "UnTwistIKPelvis_M_pointConstraint1" -p "UnTwistIKPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine4_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.7502851334591199e-30 3.0477631392830906 0.56383873190272649 ;
	setAttr -k on ".w0";
createNode transform -n "GameSkeleton" -p "Main";
createNode joint -n "Pelvis_M" -p "GameSkeleton";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 9.5763298435263593 0 0 ;
createNode joint -n "SpineA_M" -p "Pelvis_M";
createNode joint -n "SpineB_M" -p "SpineA_M";
createNode joint -n "Chest_M" -p "SpineB_M";
createNode joint -n "Head_M" -p "Chest_M";
	setAttr ".ro" 5;
createNode joint -n "Head_End_M" -p "Head_M";
	setAttr ".t" -type "double3" 6.6560138878022871e-31 1.7440267880227989 -1.9984014443252818e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "Head_M_parentConstraint1" -p "Head_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXHead_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -7.3955709864469857e-32 3.3621280567969061 -0.05136910884368695 ;
	setAttr -k on ".w0";
createNode joint -n "Gun1_R" -p "Chest_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -8.0637563679318644e-16 9.5763298435263629 89.999999999999943 ;
createNode joint -n "Gun2_R" -p "Gun1_R";
	setAttr ".ro" 2;
createNode joint -n "Gun3_End_R" -p "Gun2_R";
	setAttr ".t" -type "double3" -2.6645352591003757e-15 0.85323520097619765 6.6613381477509392e-16 ;
	setAttr ".ro" 2;
createNode parentConstraint -n "Gun2_R_parentConstraint1" -p "Gun2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXGun2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0.0017406059556597242 1.1650684251327372 0.081878483297779026 ;
	setAttr ".rsrr" -type "double3" 0 0 2.5044782117931822e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Gun1_R_parentConstraint1" -p "Gun1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXGun1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -4.2253660569758631e-07 6.1539822106032203e-08 2.5044782222012439e-06 ;
	setAttr ".rst" -type "double3" -2.350906237499248 -0.68096479514476904 0.40968214042113382 ;
	setAttr ".rsrr" -type "double3" -4.2253660569758631e-07 6.1539822106032203e-08 2.5044782222012439e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Shoulder1_L" -p "Chest_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -19.067487875850986 -1.373922310767747 -135.91978996654333 ;
createNode joint -n "Elbow1_L" -p "Shoulder1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 116.1997640970533 4.5150796727271656 0.22693078970867911 ;
createNode joint -n "Wrist2_L" -p "Elbow1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 9.9077476711943646 -37.967629035855019 -4.9544430117845675 ;
createNode joint -n "Wrist1_L" -p "Wrist2_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 0 -1.5663486604252643 ;
createNode joint -n "MiddleFinger1_L" -p "Wrist1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -16.268788276036041 10.701367659309426 12.19722942411037 ;
createNode joint -n "MiddleFinger2_L" -p "MiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -29.796169799587517 1.1299853625553653 0.62554984305827477 ;
createNode joint -n "MiddleFinger3_End_L" -p "MiddleFinger2_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0.75403114661343162 3.5527136788005009e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "MiddleFinger2_L_parentConstraint1" -p "MiddleFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -5.2243891408683315e-07 3.0321154233679317e-07 -3.3673884372937522e-07 ;
	setAttr ".rst" -type "double3" 6.2127201161388257e-09 0.77937696384104793 -4.8106558736549232e-09 ;
	setAttr ".rsrr" -type "double3" -5.224389909404024e-07 3.0321151122699276e-07 -3.3673880755056569e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "MiddleFinger1_L_parentConstraint1" -p "MiddleFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -3.536534655124649e-07 7.7973877662772336e-08 -4.5672723596120808e-07 ;
	setAttr ".rst" -type "double3" -0.2915262045474849 0.84419957325886164 -0.16371647164845093 ;
	setAttr ".rsrr" -type "double3" -3.53653544429978e-07 7.7973864940553779e-08 -4.5672718785531852e-07 ;
	setAttr -k on ".w0";
createNode joint -n "IndexFinger1_L" -p "Wrist1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -12.262717653742216 8.7989148864219224 -13.938994459339435 ;
createNode joint -n "IndexFinger2_L" -p "IndexFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -29.796169799585375 1.129985362555298 0.62554984305803507 ;
createNode joint -n "IndexFinger3_End_L" -p "IndexFinger2_L";
	setAttr ".t" -type "double3" -5.3290705182007514e-15 0.74556221729955618 2.6645352591003757e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "IndexFinger2_L_parentConstraint1" -p "IndexFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.5189666263138655e-07 -3.0465792755470687e-08 -1.5472006766891901e-08 ;
	setAttr ".rst" -type "double3" 3.7380143425025381e-10 0.77937696384104993 5.8583218276453408e-09 ;
	setAttr ".rsrr" -type "double3" 2.5189666280687616e-07 -3.0465781225959991e-08 -1.5472017998225645e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IndexFinger1_L_parentConstraint1" -p "IndexFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 4.30673341845599e-07 -4.3392522508710965e-08 -2.7479874803022645e-08 ;
	setAttr ".rst" -type "double3" 0.45647352719658851 0.83602150400759268 -0.14399876771940168 ;
	setAttr ".rsrr" -type "double3" 4.3067334383344567e-07 -4.3392520322079708e-08 -2.7479897066905421e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Wrist1_L_parentConstraint1" -p "Wrist1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWrist1_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXWrist1_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 0 -6.5568884063445204e-09 ;
	setAttr ".rst" -type "double3" 0.016234304801186816 0.66692058359011119 0.083199633296934472 ;
	setAttr ".rsrr" -type "double3" 4.026681024690307e-06 -1.6959952426281099e-06 -2.1798619547268486e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Wrist2_L_parentConstraint1" -p "Wrist2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWrist2_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXWrist2_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 2.3614979220533163e-07 9.4073553526492628e-07 4.5800066180985941e-08 ;
	setAttr ".rst" -type "double3" 0.26325202696017858 2.6188878344125044 0.123139627690529 ;
	setAttr ".rsrr" -type "double3" 164.68354646474728 4.7512591142625533 145.70652066894812 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Elbow1_L_parentConstraint1" -p "Elbow1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow1_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXElbow1_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 4.3623576867914244e-06 1.2093201123804683e-07 1.6228264392908402e-07 ;
	setAttr ".rst" -type "double3" -0.0052682357274820291 2.6881635996288207 -0.063794153983417701 ;
	setAttr ".rsrr" -type "double3" 4.3315209394008429e-06 -2.1830782656305505e-06 4.6098312769855273e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Shoulder1_L_parentConstraint1" -p "Shoulder1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder1_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXShoulder1_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 4.1365804377667152e-07 1.496800652673308e-06 -4.3798424313182255e-06 ;
	setAttr ".rst" -type "double3" 2.8186117422206967 0.59492979900123988 -0.34712698500417849 ;
	setAttr ".rsrr" -type "double3" 2.0682900440535105e-07 7.4840032949415609e-07 -2.1899212142089148e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "CapeMid1_L" -p "Chest_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -153.18961080159258 0 0 ;
createNode joint -n "CapeMid2_L" -p "CapeMid1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -15.968938142864408 0 0 ;
createNode joint -n "CapeMid3_L" -p "CapeMid2_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -9.6661502587978756 0 0 ;
createNode joint -n "CapeMid4_L" -p "CapeMid3_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -11.490374048279003 0 0 ;
createNode joint -n "CapeMid5_L" -p "CapeMid4_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -15.239886048773762 0 0 ;
createNode joint -n "CapeMid6_End_L" -p "CapeMid5_L";
	setAttr ".t" -type "double3" -9.9920072216264089e-16 1.9040269453498433 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "CapeMid5_L_parentConstraint1" -p "CapeMid5_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeMid5_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 8.0216167224134691e-07 0 0 ;
	setAttr ".rst" -type "double3" 6.6613381477509392e-16 1.4951401194601019 -2.8797419782122802e-07 ;
	setAttr ".rsrr" -type "double3" -2.1203127804404191e-06 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "CapeMid4_L_parentConstraint1" -p "CapeMid4_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeMid4_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -4.7811751860193948e-08 0 0 ;
	setAttr ".rst" -type "double3" 1.1102230246251565e-16 1.5499083880246838 -2.2488584860980154e-07 ;
	setAttr ".rsrr" -type "double3" -2.970286206132237e-06 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "CapeMid3_L_parentConstraint1" -p "CapeMid3_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeMid3_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.1065562516932617e-07 0 0 ;
	setAttr ".rst" -type "double3" 5.5511151231257827e-16 1.6177712708867089 -1.5773401962348998e-07 ;
	setAttr ".rsrr" -type "double3" -2.7118188275124391e-06 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "CapeMid2_L_parentConstraint1" -p "CapeMid2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeMid2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.59604204279593e-07 0 0 ;
	setAttr ".rst" -type "double3" -1.1102230246251565e-16 1.6833042369258129 -8.5859965359702528e-08 ;
	setAttr ".rsrr" -type "double3" -2.6628702579438373e-06 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "CapeMid1_L_parentConstraint1" -p "CapeMid1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeMid1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.9224744463206568e-06 0 0 ;
	setAttr ".rst" -type "double3" 0.55933212460801518 0.42121368670569037 -2.3549250024825064 ;
	setAttr ".rsrr" -type "double3" -2.9224744463206568e-06 0 0 ;
	setAttr -k on ".w0";
createNode joint -n "CapeR1_R" -p "Chest_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -163.29133992476449 2.2800268299498305 -5.2971276377214984 ;
createNode joint -n "CapeR2_R" -p "CapeR1_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -7.816055054547232 0 0 ;
createNode joint -n "CapeR3_R" -p "CapeR2_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -13.830213515803038 0 0 ;
createNode joint -n "CapeR4_End_R" -p "CapeR3_R";
	setAttr ".t" -type "double3" -1.1102230246251565e-15 1.5323214152261326 3.5527136788005009e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.2022729117554984 0 0 ;
createNode parentConstraint -n "CapeR3_R_parentConstraint1" -p "CapeR3_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeR3_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.4442634448555195e-07 0 0 ;
	setAttr ".rst" -type "double3" -2.1130492910970133e-09 1.6106330769258701 -1.6057376672762302e-07 ;
	setAttr ".rsrr" -type "double3" -2.9883486175154796e-06 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "CapeR2_R_parentConstraint1" -p "CapeR2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeR2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.0517057573924816e-07 0 0 ;
	setAttr ".rst" -type "double3" -5.0129544959531813e-10 1.6370816261950383 -7.8400622882668358e-08 ;
	setAttr ".rsrr" -type "double3" -2.9490928471788979e-06 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "CapeR1_R_parentConstraint1" -p "CapeR1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeR1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.7439222738079266e-06 2.9379394117773762e-07 1.7544722713261193e-08 ;
	setAttr ".rst" -type "double3" -1.2384978380766396 0.49343850131222222 -2.1909061954099762 ;
	setAttr ".rsrr" -type "double3" -2.7439222738079266e-06 2.9379394117773762e-07 1.7544722713261193e-08 ;
	setAttr -k on ".w0";
createNode joint -n "CapeL1_L" -p "Chest_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -161.88293257475124 -0.87756559708793547 5.1497225823010702 ;
createNode joint -n "CapeL2_L" -p "CapeL1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -11.38404435696016 0 0 ;
createNode joint -n "CapeL3_L" -p "CapeL2_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -11.542373997596052 0 0 ;
createNode joint -n "CapeL4_End_L" -p "CapeL3_L";
	setAttr ".t" -type "double3" -2.2204460492503131e-15 1.5323214152261242 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.2022729117554452 0 0 ;
createNode parentConstraint -n "CapeL3_L_parentConstraint1" -p "CapeL3_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeL3_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -3.959957573445629e-07 0 0 ;
	setAttr ".rst" -type "double3" -3.4486342670447812e-09 1.596218514082663 -4.8025787791061703e-09 ;
	setAttr ".rsrr" -type "double3" -3.9599575416400824e-07 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "CapeL2_L_parentConstraint1" -p "CapeL2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeL2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.4914682411507364e-07 0 0 ;
	setAttr ".rst" -type "double3" -1.7440622279707441e-09 1.6425287455094466 -6.6045746649479042e-10 ;
	setAttr ".rsrr" -type "double3" -1.4914682411507364e-07 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "CapeL1_L_parentConstraint1" -p "CapeL1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXCapeL1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.3038422804449226e-08 7.8233575005387508e-09 6.0837591108345404e-08 ;
	setAttr ".rst" -type "double3" 2.7851101326328411 1.1554102572743075 -2.0217223955830859 ;
	setAttr ".rsrr" -type "double3" -2.3038422804449226e-08 7.8233575005387508e-09 6.0837591108345404e-08 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "Chest_M_pointConstraint1" -p "Chest_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXChest_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXChest_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 2.5465762899137247e-16 0.97122108091991066 -0.00031948180983842889 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "Chest_M_orientConstraint1" -p "Chest_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXChest_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXChest_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rsrr" -type "double3" 0.51355269392967329 -67.487906900050632 -0.59620161720712683 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "SpineB_M_pointConstraint1" -p "SpineB_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXSpineB_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXSpineB_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -1.3877787807814383e-17 0.71510797723191821 0.02684644544654935 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "SpineB_M_orientConstraint1" -p "SpineB_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXSpineB_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXSpineB_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rsrr" -type "double3" 0.51927774155507833 -67.487829364143252 -0.61253003424299435 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "SpineA_M_pointConstraint1" -p "SpineA_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXSpineA_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXSpineA_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 1.2325951644078309e-31 1.4130491503809646 0.021195468479887847 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "SpineA_M_orientConstraint1" -p "SpineA_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXSpineA_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXSpineA_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rsrr" -type "double3" -0.069307009574944914 -33.732340129832259 -1.1712096920631583 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "HipTwist_R" -p "Pelvis_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 205.13957903290628 89.999999999980218 ;
createNode joint -n "Hip_R" -p "HipTwist_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -32.661444518753804 0.33346118320189072 -89.70152155897317 ;
createNode joint -n "Knee_R" -p "Hip_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 118.81087339565488 0 0 ;
createNode joint -n "BackKnee_R" -p "Knee_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -119.85008118580784 0 0 ;
createNode joint -n "Ankle_R" -p "BackKnee_R";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 49.265149786248998 0.09081589247780894 -0.19806137257821405 ;
createNode joint -n "Foot_End_R" -p "Ankle_R";
	setAttr ".t" -type "double3" 0.31718775714603575 0.27738757624716481 -1.6117158190438856 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -131.31909969045631 29.986674445462729 156.51250164863913 ;
createNode parentConstraint -n "Ankle_R_parentConstraint1" -p "Ankle_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXAnkle_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXAnkle_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 5.4778685667612533e-06 8.4263336048058524e-06 3.808171029753006e-06 ;
	setAttr ".rst" -type "double3" -0.051469198341905553 3.8906132042969532 -0.54457139334076854 ;
	setAttr ".rsrr" -type "double3" 3.339060148383418e-07 5.4051965881934098e-08 -1.8512260318988782e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "BackKnee_R_parentConstraint1" -p "BackKnee_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXBackKnee_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXBackKnee_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 2.3354655143252217e-06 0 0 ;
	setAttr ".rst" -type "double3" -0.038343897409502503 2.9451068948650043 0.053911293073820143 ;
	setAttr ".rsrr" -type "double3" -5.7533147578057681e-07 1.4391289879063881e-06 -1.1717519886990998e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Knee_R_parentConstraint1" -p "Knee_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXKnee_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXKnee_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -3.1805546814635187e-14 2.9631609887173662e-22 3.0305222440375441e-06 ;
	setAttr ".rst" -type "double3" -0.036337116627696453 2.3836548937535742 -0.051143448360413846 ;
	setAttr ".rsrr" -type "double3" -2.7816641666424874e-06 -1.7325974014240483e-06 
		-6.6498200400219088e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Hip_R_parentConstraint1" -p "Hip_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHip_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.7505901532657107e-07 1.5912049792839539e-06 -4.8091237203175782e-07 ;
	setAttr ".rst" -type "double3" -0.1813876228053477 1.4668210320678201 0.10430394015068623 ;
	setAttr ".rsrr" -type "double3" -2.9875729179693791e-06 2.8352813960107519e-06 -2.3953204046533509e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "HipTwist_R_parentConstraint1" -p "HipTwist_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.0639731256721314e-06 2.7257924907597003e-06 -2.2672607202607571e-06 ;
	setAttr ".rst" -type "double3" -1.1766292369888238 0.00064981476919534487 -0.00086878601129569688 ;
	setAttr ".rsrr" -type "double3" 1.0639731256721314e-06 2.7257924907597003e-06 -2.2672607202607571e-06 ;
	setAttr -k on ".w0";
createNode joint -n "HipTwist_L" -p "Pelvis_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.7566776614494359e-15 25.139579032906266 90.000000000016158 ;
createNode joint -n "Hip_L" -p "HipTwist_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -32.661444518753804 0.3334611832018462 -89.701521558973155 ;
createNode joint -n "Knee_L" -p "Hip_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 118.81087339565485 0 0 ;
createNode joint -n "BackKnee_L" -p "Knee_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -119.85008118580794 0 0 ;
createNode joint -n "Ankle_L" -p "BackKnee_L";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 49.265149786248934 0.090815892477818086 -0.19806137257817766 ;
createNode joint -n "Foot_End_L" -p "Ankle_L";
	setAttr ".t" -type "double3" -0.31718775714603886 -0.27738757624718968 1.6117158190438843 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -131.31909969045631 29.986674445462715 156.5125016486391 ;
createNode parentConstraint -n "Ankle_L_parentConstraint1" -p "Ankle_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXAnkle_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXAnkle_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -3.3793947714269986e-06 -1.0087156152784076e-05 -3.4074049543977728e-06 ;
	setAttr ".rst" -type "double3" 0.051469413823856947 -3.8906130552232954 0.5445712213255306 ;
	setAttr ".rsrr" -type "double3" 2.5421706472676009e-06 1.9636993959879974e-06 -1.7555946805004745e-08 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "BackKnee_L_parentConstraint1" -p "BackKnee_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXBackKnee_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXBackKnee_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 2.2483166876091241e-06 0 0 ;
	setAttr ".rst" -type "double3" 0.03834393505365874 -2.945106997386215 -0.053911427621676111 ;
	setAttr ".rsrr" -type "double3" 1.636611931074712e-06 1.2882008622794296e-06 1.4683337155719776e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Knee_L_parentConstraint1" -p "Knee_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXKnee_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXKnee_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -4.4527765540489247e-14 3.0294269509061879e-24 1.4558157786671625e-06 ;
	setAttr ".rst" -type "double3" 0.036337217313283432 -2.383654826842851 0.051143366685184155 ;
	setAttr ".rsrr" -type "double3" -5.697208037286026e-07 6.3235184902351703e-07 -1.8481332246522621e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Hip_L_parentConstraint1" -p "Hip_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHip_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.8060152970223224e-07 6.955030139985021e-07 -1.0551303076656936e-06 ;
	setAttr ".rst" -type "double3" 0.18138774696826854 -1.4668210127940973 -0.10430399527298806 ;
	setAttr ".rsrr" -type "double3" 1.4363135686365575e-06 2.6292391248618516e-06 2.8894572222319389e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "HipTwist_L_parentConstraint1" -p "HipTwist_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 3.7657153792688921e-06 -1.7105457018261068e-06 2.2672282612170556e-06 ;
	setAttr ".rst" -type "double3" 1.1766292369888234 0.00064981476919445669 -0.00086878601129525279 ;
	setAttr ".rsrr" -type "double3" 3.7657153792688921e-06 -1.7105457018261068e-06 2.2672282612170556e-06 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "Pelvis_M_pointConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXPelvis_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 1.403181944381005e-16 4.7513626008102712 -0.30196761248010451 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "Pelvis_M_orientConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKOrientToSpine_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 6.1539841677666287e-08 0 0 ;
	setAttr ".rsrr" -type "double3" 6.1539841677666274e-08 0 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode nurbsCurve -n "forwardArrowShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-0.25152840864878678 5.5850526125841665e-17 2.0713986099536648
		-0.50305681729757357 1.1170105225168333e-16 2.0713986099536648
		-1.6755157837752504e-16 3.3510315675505028e-16 2.8259838359000251
		0.50305681729757357 -1.1170105225168333e-16 2.0713986099536648
		0.25152840864878678 -5.5850526125841665e-17 2.0713986099536648
		0.25152840864878701 -3.9095368288089197e-16 1.3168133840073044
		-0.25152840864878662 -2.7925263062920859e-16 1.3168133840073044
		-0.25152840864878678 5.5850526125841665e-17 2.0713986099536648
		;
createNode transform -n "items" -p "ctrl_rig";
	setAttr -l on ".it" no;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 11 ".lnk";
	setAttr -s 11 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr ".cm" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".cs" 3;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode shadingEngine -n "irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode shadingEngine -n "BPRig_irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "BPRig_materialInfo3";
createNode ikSCsolver -n "ikSCsolver";
createNode ikRPsolver -n "ikRPsolver";
createNode ikSplineSolver -n "ikSplineSolver";
createNode displayLayer -n "jointLayer";
createNode reverse -n "jointVisReverse";
createNode objectSet -n "ControlSet";
	setAttr ".ihi" 0;
	setAttr -s 104 ".dsm";
createNode objectSet -n "GameSet";
	setAttr ".ihi" 0;
	setAttr -s 36 ".dsm";
createNode objectSet -n "AllSet";
	setAttr ".ihi" 0;
	setAttr -s 515 ".dsm";
	setAttr -s 77 ".dnsm";
createNode objectSet -n "Sets";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dnsm";
createNode setRange -n "IKArm_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode setRange -n "PoleArm_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendArmUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendArmReverse_L";
createNode condition -n "FKIKBlendArmCondition_L";
createNode setRange -n "FKIKBlendArmsetRange_L";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode unitConversion -n "unitConversion1";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_R";
createNode condition -n "FKIKBlendLegCondition_R";
createNode setRange -n "FKIKBlendLegsetRange_R";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "Leg_RAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_R";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "IKLegAimLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "IKLegAimLegReverse_R";
createNode multiplyDivide -n "IKLegAimLegMultiplyDivide_R";
createNode setRange -n "IKStiffSpine0_M";
	setAttr ".m" -type "float3" 0 1.0334172 0 ;
	setAttr ".on" -type "float3" 0 -10 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode setRange -n "IKStiffSpine2_M";
	setAttr ".on" -type "float3" 0 -10 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode setRange -n "IKStiffSpine4_M";
	setAttr ".m" -type "float3" 0 1.0345302 0 ;
	setAttr ".on" -type "float3" 0 -10 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode curveInfo -n "IKCurveInfoSpine_M";
createNode multiplyDivide -n "IKCurveInfoNormalizeSpine_M";
	setAttr ".op" 2;
	setAttr ".i2" -type "float3" 1 3.0996561 1 ;
createNode multiplyDivide -n "IKCurveInfoAllMultiplySpine_M";
	setAttr ".op" 2;
createNode unitConversion -n "stretchySpineUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "stretchySpineReverse_M";
createNode multiplyDivide -n "stretchySpineMultiplyDivide0_M";
	setAttr ".i1" -type "float3" 0 0.97106707 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo0_M";
	setAttr -s 2 ".i[0:1]"  0.97106708107769091 0.97106707096099854;
createNode multiplyDivide -n "stretchySpineMultiplyDivide1_M";
	setAttr ".i1" -type "float3" 0 0.71494234 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo1_M";
	setAttr -s 2 ".i[0:1]"  0.7149423105017414 0.7149423360824585;
createNode multiplyDivide -n "stretchySpineMultiplyDivide2_M";
	setAttr ".i1" -type "float3" 0 0.97106707 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo2_M";
	setAttr -s 2 ".i[0:1]"  0.97106708107769091 0.97106707096099854;
createNode multiplyDivide -n "stretchySpineMultiplyDivide3_M";
	setAttr ".i1" -type "float3" 0 0.71494234 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo3_M";
	setAttr -s 2 ".i[0:1]"  0.7149423105017414 0.7149423360824585;
createNode multiplyDivide -n "stretchySpineMultiplyDivide4_M";
	setAttr ".i1" -type "float3" 0 1.4130837 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo4_M";
	setAttr -s 2 ".i[0:1]"  1.413083705895736 1.4130836725234985;
createNode unitConversion -n "IKTwistSpineUnitConversion_M";
	setAttr ".cf" 0.75;
createNode unitConversion -n "FKIKBlendSpineUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendSpineReverse_M";
createNode condition -n "FKIKBlendSpineCondition_M";
createNode setRange -n "FKIKBlendSpinesetRange_M";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode blendColors -n "ScaleBlendWrist1_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendWrist2_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendElbow1_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendShoulder1_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendChest_M";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendSpineB_M";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendSpineA_M";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendAnkle_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendBackKnee_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendKnee_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendPelvis_M";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode unitConversion -n "unitConversion4";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_L";
createNode condition -n "FKIKBlendLegCondition_L";
createNode setRange -n "FKIKBlendLegsetRange_L";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "Leg_LAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_L";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion5";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion6";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "IKLegAimLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "IKLegAimLegReverse_L";
createNode multiplyDivide -n "IKLegAimLegMultiplyDivide_L";
createNode blendColors -n "ScaleBlendAnkle_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendBackKnee_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendKnee_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode setRange -n "CenterBtwFeet_M";
	setAttr ".v" -type "float3" 0 1 0 ;
	setAttr ".n" -type "float3" 2 0 0 ;
	setAttr ".om" -type "float3" 0.1 0 0 ;
createNode unitConversion -n "PelvisCenterBtwFeetUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "PelvisCenterBtwFeetReverse_M";
createNode dagPose -n "buildPose";
	addAttr -ci true -sn "udAttr" -ln "udAttr" -dt "string";
	setAttr ".udAttr" -type "string" (
		"setAttr HipSwingerPelvis_M.rotateX 0;setAttr HipSwingerPelvis_M.rotateY -0;setAttr HipSwingerPelvis_M.rotateZ 0;setAttr IKExtraArm_L.translateX 0;setAttr IKExtraArm_L.translateY -8.881784197e-16;setAttr IKExtraArm_L.translateZ 0;setAttr IKExtraArm_L.rotateX -0;setAttr IKExtraArm_L.rotateY 0;setAttr IKExtraArm_L.rotateZ -0;setAttr IKArm_L.translateX 0;setAttr IKArm_L.translateY 8.881784197e-16;setAttr IKArm_L.translateZ -2.220446049e-16;setAttr IKArm_L.rotateX -0;setAttr IKArm_L.rotateY 0;setAttr IKArm_L.rotateZ -0;setAttr IKArm_L.follow 0;setAttr PoleExtraArm_L.translateX 0;setAttr PoleExtraArm_L.translateY 0;setAttr PoleExtraArm_L.translateZ 0;setAttr PoleArm_L.translateX 0;setAttr PoleArm_L.translateY 0;setAttr PoleArm_L.translateZ 0;setAttr PoleArm_L.follow 0;setAttr FKIKArm_L.FKIKBlend 0;setAttr FKIKArm_L.FKVis 1;setAttr FKIKArm_L.IKVis 0;setAttr IKExtraLeg_R.translateX 4.440892099e-16;setAttr IKExtraLeg_R.translateY -5.551115123e-17;setAttr IKExtraLeg_R.translateZ 2.220446049e-16;setAttr IKExtraLeg_R.rotateX -0;setAttr IKExtraLeg_R.rotateY -0;setAttr IKExtraLeg_R.rotateZ 0;setAttr IKLeg_R.translateX 0;setAttr IKLeg_R.translateY 0;setAttr IKLeg_R.translateZ 0;setAttr IKLeg_R.rotateX -0;setAttr IKLeg_R.rotateY -0;setAttr IKLeg_R.rotateZ 0;setAttr IKLeg_R.swivel 0;setAttr IKLeg_R.roll 0;setAttr IKLeg_R.rollAngle 25;setAttr IKLeg_R.legAim 10;setAttr PoleExtraLeg_R.translateX 0;setAttr PoleExtraLeg_R.translateY 0;setAttr PoleExtraLeg_R.translateZ 0;setAttr PoleLeg_R.translateX -4.440892099e-16;setAttr PoleLeg_R.translateY 0;setAttr PoleLeg_R.translateZ 2.220446049e-16;setAttr PoleLeg_R.follow 10;setAttr FKIKLeg_R.FKIKBlend 10;setAttr FKIKLeg_R.FKVis 0;setAttr FKIKLeg_R.IKVis 1;setAttr IKExtraLegHeel_R.rotateX 0;setAttr IKExtraLegHeel_R.rotateY -1.590277341e-15;setAttr IKExtraLegHeel_R.rotateZ 0;setAttr IKLegHeel_R.rotateX 0;setAttr IKLegHeel_R.rotateY -0;setAttr IKLegHeel_R.rotateZ 0;setAttr IKExtraLegBall_R.rotateX 0;setAttr IKExtraLegBall_R.rotateY -1.590277341e-15;setAttr IKExtraLegBall_R.rotateZ 0;setAttr IKLegBall_R.rotateX 0;setAttr IKExtraSpine0_M.visibility 1;setAttr IKExtraSpine0_M.translateX 0;setAttr IKExtraSpine0_M.translateY 0;setAttr IKExtraSpine0_M.translateZ 0;setAttr IKExtraSpine0_M.rotateX -0;setAttr IKExtraSpine0_M.rotateY -0;setAttr IKExtraSpine0_M.rotateZ 0;setAttr IKExtraSpine0_M.scaleX 1;setAttr IKExtraSpine0_M.scaleY 1;setAttr IKExtraSpine0_M.scaleZ 1;setAttr Main.visibility 1;setAttr Main.translateX 0;setAttr Main.translateY 0;setAttr Main.translateZ 0;setAttr Main.rotateX 0;setAttr Main.rotateY 0;setAttr Main.rotateZ 0;setAttr Main.scaleX 1;setAttr Main.scaleY 1;setAttr Main.scaleZ 1;setAttr FKExtraHead_M.translateX 0;setAttr FKExtraHead_M.translateY 0;setAttr FKExtraHead_M.translateZ -4.440892099e-16;setAttr FKExtraHead_M.rotateX -0;setAttr FKExtraHead_M.rotateY 0;setAttr FKExtraHead_M.rotateZ -0;setAttr FKExtraHead_M.scaleX 1;setAttr FKExtraHead_M.scaleY 1;setAttr FKExtraHead_M.scaleZ 1;setAttr IKSpine0_M.translateX 0;setAttr IKSpine0_M.translateY 0;setAttr IKSpine0_M.translateZ 0;setAttr IKSpine0_M.rotateX -0;setAttr IKSpine0_M.rotateY -0;setAttr IKSpine0_M.rotateZ 0;setAttr IKSpine0_M.stiff 0;setAttr IKExtraSpine2_M.visibility 1;setAttr IKExtraSpine2_M.translateX 0;setAttr IKExtraSpine2_M.translateY 0;setAttr IKExtraSpine2_M.translateZ 0;setAttr IKExtraSpine2_M.rotateX 0;setAttr IKExtraSpine2_M.rotateY -0;setAttr IKExtraSpine2_M.rotateZ 0;setAttr IKExtraSpine2_M.scaleX 1;setAttr IKExtraSpine2_M.scaleY 1;setAttr IKExtraSpine2_M.scaleZ 1;setAttr IKSpine2_M.translateX 0;setAttr IKSpine2_M.translateY 0;setAttr IKSpine2_M.translateZ 0;setAttr IKSpine2_M.rotateX 0;setAttr IKSpine2_M.rotateY -0;setAttr IKSpine2_M.rotateZ 0;setAttr IKSpine2_M.stiff 0;setAttr IKExtraSpine4_M.visibility 1;setAttr IKExtraSpine4_M.translateX 0;setAttr IKExtraSpine4_M.translateY 0;setAttr IKExtraSpine4_M.translateZ 0;setAttr IKExtraSpine4_M.rotateX 0;setAttr IKExtraSpine4_M.rotateY -0;setAttr IKExtraSpine4_M.rotateZ 0;setAttr IKExtraSpine4_M.scaleX 1;setAttr IKExtraSpine4_M.scaleY 1;setAttr IKExtraSpine4_M.scaleZ 1;setAttr IKSpine4_M.translateX 0;setAttr IKSpine4_M.translateY 0;setAttr IKSpine4_M.translateZ 0;setAttr IKSpine4_M.rotateX 0;setAttr IKSpine4_M.rotateY -0;setAttr IKSpine4_M.rotateZ 0;setAttr IKSpine4_M.stiff 0;setAttr IKSpine4_M.stretchy 0;setAttr FKIKSpine_M.FKIKBlend 0;setAttr FKIKSpine_M.FKVis 1;setAttr FKIKSpine_M.IKVis 0;setAttr IKExtraLeg_L.translateX 0;setAttr IKExtraLeg_L.translateY 0;setAttr IKExtraLeg_L.translateZ -2.220446049e-16;setAttr IKExtraLeg_L.rotateX -0;setAttr IKExtraLeg_L.rotateY -0;setAttr IKExtraLeg_L.rotateZ 0;setAttr IKLeg_L.translateX 0;setAttr IKLeg_L.translateY 0;setAttr IKLeg_L.translateZ 0;setAttr IKLeg_L.rotateX -0;setAttr IKLeg_L.rotateY -0;setAttr IKLeg_L.rotateZ 0;setAttr IKLeg_L.swivel 0;setAttr IKLeg_L.roll 0;setAttr IKLeg_L.rollAngle 25;setAttr IKLeg_L.legAim 10;setAttr PoleExtraLeg_L.translateX 0;setAttr PoleExtraLeg_L.translateY 0;setAttr PoleExtraLeg_L.translateZ 0;setAttr PoleLeg_L.translateX 0;setAttr PoleLeg_L.translateY 0;setAttr PoleLeg_L.translateZ 0;setAttr PoleLeg_L.follow 10;setAttr FKIKLeg_L.FKIKBlend 10;setAttr FKIKLeg_L.FKVis 0;setAttr FKIKLeg_L.IKVis 1;setAttr IKExtraLegHeel_L.rotateX 0;setAttr IKExtraLegHeel_L.rotateY -0;setAttr IKExtraLegHeel_L.rotateZ 0;setAttr IKLegHeel_L.rotateX 0;setAttr IKLegHeel_L.rotateY -0;setAttr IKLegHeel_L.rotateZ 0;setAttr IKExtraLegBall_L.rotateX 0;setAttr IKExtraLegBall_L.rotateY -0;setAttr IKExtraLegBall_L.rotateZ 0;setAttr IKLegBall_L.rotateX 0;setAttr CenterExtra_M.translateX 0;setAttr CenterExtra_M.translateY 0;setAttr CenterExtra_M.translateZ 0;setAttr CenterExtra_M.rotateX 0;setAttr CenterExtra_M.rotateY -0;setAttr CenterExtra_M.rotateZ 0;setAttr Center_M.translateX 0;setAttr Center_M.translateY 0;setAttr Center_M.translateZ 0;setAttr Center_M.rotateX 0;setAttr Center_M.rotateY -0;setAttr Center_M.rotateZ 0;setAttr Center_M.CenterBtwFeet 0;setAttr FKHead_M.translateX 2.465190329e-32;setAttr FKHead_M.translateY 0;setAttr FKHead_M.translateZ 0;setAttr FKHead_M.rotateX -0;setAttr FKHead_M.rotateY 0;setAttr FKHead_M.rotateZ -0;setAttr FKHead_M.scaleX 1;setAttr FKHead_M.scaleY 1;setAttr FKHead_M.scaleZ 1;setAttr FKExtraGun2_R.translateX 0;setAttr FKExtraGun2_R.translateY 0;setAttr FKExtraGun2_R.translateZ 1.110223025e-16;setAttr FKExtraGun2_R.rotateX -0;setAttr FKExtraGun2_R.rotateY 0;setAttr FKExtraGun2_R.rotateZ 0;setAttr FKExtraGun2_R.scaleX 1;setAttr FKExtraGun2_R.scaleY 1;setAttr FKExtraGun2_R.scaleZ 1;setAttr FKGun2_R.translateX 0;setAttr FKGun2_R.translateY 0;setAttr FKGun2_R.translateZ 1.110223025e-16;setAttr FKGun2_R.rotateX -0;setAttr FKGun2_R.rotateY 0;setAttr FKGun2_R.rotateZ 0;setAttr FKGun2_R.scaleX 1;setAttr FKGun2_R.scaleY 1;setAttr FKGun2_R.scaleZ 1;setAttr FKExtraGun1_R.translateX 8.881784197e-16;setAttr FKExtraGun1_R.translateY 0;setAttr FKExtraGun1_R.translateZ -1.110223025e-16;setAttr FKExtraGun1_R.rotateX -0;setAttr FKExtraGun1_R.rotateY 0;setAttr FKExtraGun1_R.rotateZ 0;setAttr FKExtraGun1_R.scaleX 1;setAttr FKExtraGun1_R.scaleY 1;setAttr FKExtraGun1_R.scaleZ 1;setAttr FKGun1_R.translateX 0;setAttr FKGun1_R.translateY 0;setAttr FKGun1_R.translateZ 0;setAttr FKGun1_R.rotateX -0;setAttr FKGun1_R.rotateY 0;setAttr FKGun1_R.rotateZ 0;setAttr FKGun1_R.scaleX 1;setAttr FKGun1_R.scaleY 1;setAttr FKGun1_R.scaleZ 1;setAttr FKExtraMiddleFinger2_L.translateX -8.881784197e-16;setAttr FKExtraMiddleFinger2_L.translateY -3.330669074e-16;setAttr FKExtraMiddleFinger2_L.translateZ 0;setAttr FKExtraMiddleFinger2_L.rotateX -0;setAttr FKExtraMiddleFinger2_L.rotateY 0;setAttr FKExtraMiddleFinger2_L.rotateZ -0;setAttr FKExtraMiddleFinger2_L.scaleX 1;setAttr FKExtraMiddleFinger2_L.scaleY 1;setAttr FKExtraMiddleFinger2_L.scaleZ 1;setAttr FKMiddleFinger2_L.translateX -8.881784197e-16;setAttr FKMiddleFinger2_L.translateY 4.440892099e-16;setAttr FKMiddleFinger2_L.translateZ 8.881784197e-16;setAttr FKMiddleFinger2_L.rotateX -0;setAttr FKMiddleFinger2_L.rotateY 0;setAttr FKMiddleFinger2_L.rotateZ -0;setAttr FKMiddleFinger2_L.scaleX 1;setAttr FKMiddleFinger2_L.scaleY 1;setAttr FKMiddleFinger2_L.scaleZ 1;setAttr FKExtraMiddleFinger1_L.translateX 8.881784197e-16;setAttr FKExtraMiddleFinger1_L.translateY 0;setAttr FKExtraMiddleFinger1_L.translateZ -2.664535259e-15;setAttr FKExtraMiddleFinger1_L.rotateX -0;setAttr FKExtraMiddleFinger1_L.rotateY 0;setAttr FKExtraMiddleFinger1_L.rotateZ -0;setAttr FKExtraMiddleFinger1_L.scaleX 1;setAttr FKExtraMiddleFinger1_L.scaleY 1;setAttr FKExtraMiddleFinger1_L.scaleZ 1;setAttr FKMiddleFinger1_L.translateX 0;setAttr FKMiddleFinger1_L.translateY -2.220446049e-16;setAttr FKMiddleFinger1_L.translateZ 0;setAttr FKMiddleFinger1_L.rotateX -0;setAttr FKMiddleFinger1_L.rotateY 0;setAttr FKMiddleFinger1_L.rotateZ -0;setAttr FKMiddleFinger1_L.scaleX 1;setAttr FKMiddleFinger1_L.scaleY 1;setAttr FKMiddleFinger1_L.scaleZ 1;setAttr FKExtraIndexFinger2_L.translateX 0;setAttr FKExtraIndexFinger2_L.translateY 4.440892099e-16;setAttr FKExtraIndexFinger2_L.translateZ 0;setAttr FKExtraIndexFinger2_L.rotateX -0;setAttr FKExtraIndexFinger2_L.rotateY 0;setAttr FKExtraIndexFinger2_L.rotateZ -0;setAttr FKExtraIndexFinger2_L.scaleX 1;setAttr FKExtraIndexFinger2_L.scaleY 1;setAttr FKExtraIndexFinger2_L.scaleZ 1;setAttr FKIndexFinger2_L.translateX 0;setAttr FKIndexFinger2_L.translateY 8.881784197e-16;setAttr FKIndexFinger2_L.translateZ 8.881784197e-16;setAttr FKIndexFinger2_L.rotateX -0;setAttr FKIndexFinger2_L.rotateY 0;setAttr FKIndexFinger2_L.rotateZ -0;setAttr FKIndexFinger2_L.scaleX 1;setAttr FKIndexFinger2_L.scaleY 1;setAttr FKIndexFinger2_L.scaleZ 1;setAttr FKExtraIndexFinger1_L.translateX -8.881784197e-16;setAttr FKExtraIndexFinger1_L.translateY -4.440892099e-16;setAttr FKExtraIndexFinger1_L.translateZ 0;setAttr FKExtraIndexFinger1_L.rotateX -0;setAttr FKExtraIndexFinger1_L.rotateY 0;setAttr FKExtraIndexFinger1_L.rotateZ -0;setAttr FKExtraIndexFinger1_L.scaleX 1;setAttr FKExtraIndexFinger1_L.scaleY 1;setAttr FKExtraIndexFinger1_L.scaleZ 1;setAttr FKIndexFinger1_L.translateX 0;setAttr FKIndexFinger1_L.translateY -3.330669074e-16;setAttr FKIndexFinger1_L.translateZ 0;setAttr FKIndexFinger1_L.rotateX -0;setAttr FKIndexFinger1_L.rotateY 0;setAttr FKIndexFinger1_L.rotateZ -0;setAttr FKIndexFinger1_L.scaleX 1;setAttr FKIndexFinger1_L.scaleY 1;setAttr FKIndexFinger1_L.scaleZ 1;setAttr FKExtraWrist1_L.translateX 0;setAttr FKExtraWrist1_L.translateY 0;setAttr FKExtraWrist1_L.translateZ 8.881784197e-16;setAttr FKExtraWrist1_L.rotateX -0;setAttr FKExtraWrist1_L.rotateY 0;setAttr FKExtraWrist1_L.rotateZ -0;setAttr FKExtraWrist1_L.scaleX 1;setAttr FKExtraWrist1_L.scaleY 1;setAttr FKExtraWrist1_L.scaleZ 1;setAttr FKWrist1_L.translateX -1.776356839e-15;setAttr FKWrist1_L.translateY 2.220446049e-16;setAttr FKWrist1_L.translateZ 8.881784197e-16;setAttr FKWrist1_L.rotateX -0;setAttr FKWrist1_L.rotateY 0;setAttr FKWrist1_L.rotateZ -0;setAttr FKWrist1_L.scaleX 1;setAttr FKWrist1_L.scaleY 1;setAttr FKWrist1_L.scaleZ 1;setAttr FKExtraWrist2_L.translateX 0;setAttr FKExtraWrist2_L.translateY -1.110223025e-16;setAttr FKExtraWrist2_L.translateZ 8.881784197e-16;setAttr FKExtraWrist2_L.rotateX -0;setAttr FKExtraWrist2_L.rotateY 0;setAttr FKExtraWrist2_L.rotateZ -0;setAttr FKExtraWrist2_L.scaleX 1;setAttr FKExtraWrist2_L.scaleY 1;setAttr FKExtraWrist2_L.scaleZ 1;setAttr FKWrist2_L.translateX -8.881784197e-16;setAttr FKWrist2_L.translateY -2.220446049e-16;setAttr FKWrist2_L.translateZ 8.881784197e-16;setAttr FKWrist2_L.rotateX -0;setAttr FKWrist2_L.rotateY 0;setAttr FKWrist2_L.rotateZ -0;setAttr FKWrist2_L.scaleX 1;setAttr FKWrist2_L.scaleY 1;setAttr FKWrist2_L.scaleZ 1;setAttr FKExtraElbow1_L.translateX 8.881784197e-16;setAttr FKExtraElbow1_L.translateY 0;setAttr FKExtraElbow1_L.translateZ 2.220446049e-16;setAttr FKExtraElbow1_L.rotateX -0;setAttr FKExtraElbow1_L.rotateY 0;setAttr FKExtraElbow1_L.rotateZ -0;setAttr FKExtraElbow1_L.scaleX 1;setAttr FKExtraElbow1_L.scaleY 1;setAttr FKExtraElbow1_L.scaleZ 1;setAttr FKElbow1_L.translateX 8.881784197e-16;setAttr FKElbow1_L.translateY 4.440892099e-16;setAttr FKElbow1_L.translateZ 6.661338148e-16;setAttr FKElbow1_L.rotateX -0;setAttr FKElbow1_L.rotateY 0;setAttr FKElbow1_L.rotateZ -0;setAttr FKElbow1_L.scaleX 1;setAttr FKElbow1_L.scaleY 1;setAttr FKElbow1_L.scaleZ 1;setAttr FKExtraShoulder1_L.translateX 0;setAttr FKExtraShoulder1_L.translateY 1.33226763e-15;setAttr FKExtraShoulder1_L.translateZ 4.440892099e-16;setAttr FKExtraShoulder1_L.rotateX -0;setAttr FKExtraShoulder1_L.rotateY 0;setAttr FKExtraShoulder1_L.rotateZ -0;setAttr FKExtraShoulder1_L.scaleX 1;setAttr FKExtraShoulder1_L.scaleY 1;setAttr FKExtraShoulder1_L.scaleZ 1;setAttr FKShoulder1_L.translateX 0;setAttr FKShoulder1_L.translateY 4.440892099e-16;setAttr FKShoulder1_L.translateZ -4.440892099e-16;setAttr FKShoulder1_L.rotateX -0;setAttr FKShoulder1_L.rotateY 0;setAttr FKShoulder1_L.rotateZ -0;setAttr FKShoulder1_L.scaleX 1;setAttr FKShoulder1_L.scaleY 1;setAttr FKShoulder1_L.scaleZ 1;setAttr FKExtraCapeMid5_L.translateX -1.110223025e-16;setAttr FKExtraCapeMid5_L.translateY 4.440892099e-16;setAttr FKExtraCapeMid5_L.translateZ 0;setAttr FKExtraCapeMid5_L.rotateX -0;setAttr FKExtraCapeMid5_L.rotateY 0;setAttr FKExtraCapeMid5_L.rotateZ -0;setAttr FKExtraCapeMid5_L.scaleX 1;setAttr FKExtraCapeMid5_L.scaleY 1;setAttr FKExtraCapeMid5_L.scaleZ 1;setAttr FKCapeMid5_L.translateX -1.110223025e-16;setAttr FKCapeMid5_L.translateY 0;setAttr FKCapeMid5_L.translateZ -4.440892099e-16;setAttr FKCapeMid5_L.rotateX -0;setAttr FKCapeMid5_L.rotateY 0;setAttr FKCapeMid5_L.rotateZ -0;setAttr FKCapeMid5_L.scaleX 1;setAttr FKCapeMid5_L.scaleY 1;setAttr FKCapeMid5_L.scaleZ 1;setAttr FKExtraCapeMid4_L.translateX 0;setAttr FKExtraCapeMid4_L.translateY 0;setAttr FKExtraCapeMid4_L.translateZ 0;setAttr FKExtraCapeMid4_L.rotateX -0;setAttr FKExtraCapeMid4_L.rotateY 0;setAttr FKExtraCapeMid4_L.rotateZ -0;setAttr FKExtraCapeMid4_L.scaleX 1;setAttr FKExtraCapeMid4_L.scaleY 1;setAttr FKExtraCapeMid4_L.scaleZ 1;setAttr FKCapeMid4_L.translateX 0;setAttr FKCapeMid4_L.translateY 0;setAttr FKCapeMid4_L.translateZ 0;setAttr FKCapeMid4_L.rotateX -0;setAttr FKCapeMid4_L.rotateY 0;setAttr FKCapeMid4_L.rotateZ -0;setAttr FKCapeMid4_L.scaleX 1;setAttr FKCapeMid4_L.scaleY 1;setAttr FKCapeMid4_L.scaleZ 1;setAttr FKExtraCapeMid3_L.translateX 1.110223025e-16;setAttr FKExtraCapeMid3_L.translateY 8.881784197e-16;setAttr FKExtraCapeMid3_L.translateZ 0;setAttr FKExtraCapeMid3_L.rotateX -0;setAttr FKExtraCapeMid3_L.rotateY 0;setAttr FKExtraCapeMid3_L.rotateZ -0;setAttr FKExtraCapeMid3_L.scaleX 1;setAttr FKExtraCapeMid3_L.scaleY 1;setAttr FKExtraCapeMid3_L.scaleZ 1;setAttr FKCapeMid3_L.translateX 1.110223025e-16;setAttr FKCapeMid3_L.translateY 8.881784197e-16;setAttr FKCapeMid3_L.translateZ 0;setAttr FKCapeMid3_L.rotateX -0;setAttr FKCapeMid3_L.rotateY 0;setAttr FKCapeMid3_L.rotateZ -0;setAttr FKCapeMid3_L.scaleX 1;setAttr FKCapeMid3_L.scaleY 1;setAttr FKCapeMid3_L.scaleZ 1;setAttr FKExtraCapeMid2_L.translateX 0;setAttr FKExtraCapeMid2_L.translateY 0;setAttr FKExtraCapeMid2_L.translateZ 0;setAttr FKExtraCapeMid2_L.rotateX -0;setAttr FKExtraCapeMid2_L.rotateY 0;setAttr FKExtraCapeMid2_L.rotateZ -0;setAttr FKExtraCapeMid2_L.scaleX 1;setAttr FKExtraCapeMid2_L.scaleY 1;setAttr FKExtraCapeMid2_L.scaleZ 1;setAttr FKCapeMid2_L.translateX 0;setAttr FKCapeMid2_L.translateY 0;setAttr FKCapeMid2_L.translateZ 0;setAttr FKCapeMid2_L.rotateX -0;setAttr FKCapeMid2_L.rotateY 0;setAttr FKCapeMid2_L.rotateZ -0;setAttr FKCapeMid2_L.scaleX 1;setAttr FKCapeMid2_L.scaleY 1;setAttr FKCapeMid2_L.scaleZ 1;setAttr FKExtraCapeMid1_L.translateX 0;setAttr FKExtraCapeMid1_L.translateY 1.776356839e-15;setAttr FKExtraCapeMid1_L.translateZ 0;setAttr FKExtraCapeMid1_L.rotateX -0;setAttr FKExtraCapeMid1_L.rotateY 0;setAttr FKExtraCapeMid1_L.rotateZ -0;setAttr FKExtraCapeMid1_L.scaleX 1;setAttr FKExtraCapeMid1_L.scaleY 1;setAttr FKExtraCapeMid1_L.scaleZ 1;setAttr FKCapeMid1_L.translateX 0;setAttr FKCapeMid1_L.translateY 0;setAttr FKCapeMid1_L.translateZ 0;setAttr FKCapeMid1_L.rotateX -0;setAttr FKCapeMid1_L.rotateY 0;setAttr FKCapeMid1_L.rotateZ -0;setAttr FKCapeMid1_L.scaleX 1;setAttr FKCapeMid1_L.scaleY 1;setAttr FKCapeMid1_L.scaleZ 1;setAttr FKExtraCapeR3_R.translateX 4.440892099e-16;setAttr FKExtraCapeR3_R.translateY 8.881784197e-16;setAttr FKExtraCapeR3_R.translateZ 4.440892099e-16;setAttr FKExtraCapeR3_R.rotateX -0;setAttr FKExtraCapeR3_R.rotateY 0;setAttr FKExtraCapeR3_R.rotateZ -0;setAttr FKExtraCapeR3_R.scaleX 1;setAttr FKExtraCapeR3_R.scaleY 1;setAttr FKExtraCapeR3_R.scaleZ 1;setAttr FKCapeR3_R.translateX 2.220446049e-16;setAttr FKCapeR3_R.translateY 8.881784197e-16;setAttr FKCapeR3_R.translateZ 0;setAttr FKCapeR3_R.rotateX -0;setAttr FKCapeR3_R.rotateY 0;setAttr FKCapeR3_R.rotateZ -0;setAttr FKCapeR3_R.scaleX 1;setAttr FKCapeR3_R.scaleY 1;setAttr FKCapeR3_R.scaleZ 1;setAttr FKExtraCapeR2_R.translateX 4.440892099e-16;setAttr FKExtraCapeR2_R.translateY -8.881784197e-16;setAttr FKExtraCapeR2_R.translateZ 0;setAttr FKExtraCapeR2_R.rotateX -0;setAttr FKExtraCapeR2_R.rotateY 0;setAttr FKExtraCapeR2_R.rotateZ -0;setAttr FKExtraCapeR2_R.scaleX 1;setAttr FKExtraCapeR2_R.scaleY 1;setAttr FKExtraCapeR2_R.scaleZ 1;setAttr FKCapeR2_R.translateX 0;setAttr FKCapeR2_R.translateY 0;setAttr FKCapeR2_R.translateZ 0;setAttr FKCapeR2_R.rotateX -0;setAttr FKCapeR2_R.rotateY 0;setAttr FKCapeR2_R.rotateZ -0;setAttr FKCapeR2_R.scaleX 1;setAttr FKCapeR2_R.scaleY 1;setAttr FKCapeR2_R.scaleZ 1;setAttr FKExtraCapeR1_R.translateX 2.220446049e-16;setAttr FKExtraCapeR1_R.translateY 1.776356839e-15;setAttr FKExtraCapeR1_R.translateZ -8.881784197e-16;setAttr FKExtraCapeR1_R.rotateX -0;setAttr FKExtraCapeR1_R.rotateY 0;setAttr FKExtraCapeR1_R.rotateZ -0;setAttr FKExtraCapeR1_R.scaleX 1;setAttr FKExtraCapeR1_R.scaleY 1;setAttr FKExtraCapeR1_R.scaleZ 1;setAttr FKCapeR1_R.translateX 0;setAttr FKCapeR1_R.translateY 8.881784197e-16;setAttr FKCapeR1_R.translateZ 0;setAttr FKCapeR1_R.rotateX -0;setAttr FKCapeR1_R.rotateY 0;setAttr FKCapeR1_R.rotateZ -0;setAttr FKCapeR1_R.scaleX 1;setAttr FKCapeR1_R.scaleY 1;setAttr FKCapeR1_R.scaleZ 1;setAttr FKExtraCapeL3_L.translateX -4.440892099e-16;setAttr FKExtraCapeL3_L.translateY -1.776356839e-15;setAttr FKExtraCapeL3_L.translateZ 0;setAttr FKExtraCapeL3_L.rotateX -0;setAttr FKExtraCapeL3_L.rotateY 0;setAttr FKExtraCapeL3_L.rotateZ -0;setAttr FKExtraCapeL3_L.scaleX 1;setAttr FKExtraCapeL3_L.scaleY 1;setAttr FKExtraCapeL3_L.scaleZ 1;setAttr FKCapeL3_L.translateX -8.881784197e-16;setAttr FKCapeL3_L.translateY 0;setAttr FKCapeL3_L.translateZ 0;setAttr FKCapeL3_L.rotateX -0;setAttr FKCapeL3_L.rotateY 0;setAttr FKCapeL3_L.rotateZ -0;setAttr FKCapeL3_L.scaleX 1;setAttr FKCapeL3_L.scaleY 1;setAttr FKCapeL3_L.scaleZ 1;setAttr FKExtraCapeL2_L.translateX -4.440892099e-16;setAttr FKExtraCapeL2_L.translateY 0;setAttr FKExtraCapeL2_L.translateZ -8.881784197e-16;setAttr FKExtraCapeL2_L.rotateX -0;setAttr FKExtraCapeL2_L.rotateY 0;setAttr FKExtraCapeL2_L.rotateZ -0;setAttr FKExtraCapeL2_L.scaleX 1;setAttr FKExtraCapeL2_L.scaleY 1;setAttr FKExtraCapeL2_L.scaleZ 1;setAttr FKCapeL2_L.translateX 4.440892099e-16;setAttr FKCapeL2_L.translateY -8.881784197e-16;setAttr FKCapeL2_L.translateZ 0;setAttr FKCapeL2_L.rotateX -0;setAttr FKCapeL2_L.rotateY 0;setAttr FKCapeL2_L.rotateZ -0;setAttr FKCapeL2_L.scaleX 1;setAttr FKCapeL2_L.scaleY 1;setAttr FKCapeL2_L.scaleZ 1;setAttr FKExtraCapeL1_L.translateX 4.440892099e-16;setAttr FKExtraCapeL1_L.translateY -8.881784197e-16;setAttr FKExtraCapeL1_L.translateZ -8.881784197e-16;setAttr FKExtraCapeL1_L.rotateX -0;setAttr FKExtraCapeL1_L.rotateY 0;setAttr FKExtraCapeL1_L.rotateZ -0;setAttr FKExtraCapeL1_L.scaleX 1;setAttr FKExtraCapeL1_L.scaleY 1;setAttr FKExtraCapeL1_L.scaleZ 1;setAttr FKCapeL1_L.translateX 8.881784197e-16;setAttr FKCapeL1_L.translateY -8.881784197e-16;setAttr FKCapeL1_L.translateZ 0;setAttr FKCapeL1_L.rotateX -0;setAttr FKCapeL1_L.rotateY 0;setAttr FKCapeL1_L.rotateZ -0;setAttr FKCapeL1_L.scaleX 1;setAttr FKCapeL1_L.scaleY 1;setAttr FKCapeL1_L.scaleZ 1;setAttr FKExtraChest_M.translateX 2.465190329e-32;setAttr FKExtraChest_M.translateY 0;setAttr FKExtraChest_M.translateZ 0;setAttr FKExtraChest_M.rotateX 0;setAttr FKExtraChest_M.rotateY -0;setAttr FKExtraChest_M.rotateZ 0;setAttr FKExtraChest_M.scaleX 1;setAttr FKExtraChest_M.scaleY 1;setAttr FKExtraChest_M.scaleZ 1;setAttr FKChest_M.translateX 2.465190329e-32;setAttr FKChest_M.translateY 0;setAttr FKChest_M.translateZ 0;setAttr FKChest_M.rotateX 0;setAttr FKChest_M.rotateY -0;setAttr FKChest_M.rotateZ 0;setAttr FKChest_M.scaleX 1;setAttr FKChest_M.scaleY 1;setAttr FKChest_M.scaleZ 1;setAttr FKExtraSpineB_M.translateX 2.465190329e-32;setAttr FKExtraSpineB_M.translateY 0;setAttr FKExtraSpineB_M.translateZ 0;setAttr FKExtraSpineB_M.rotateX 0;setAttr FKExtraSpineB_M.rotateY -0;setAttr FKExtraSpineB_M.rotateZ 0;setAttr FKExtraSpineB_M.scaleX 1;setAttr FKExtraSpineB_M.scaleY 1;setAttr FKExtraSpineB_M.scaleZ 1;setAttr FKSpineB_M.translateX 2.465190329e-32;setAttr FKSpineB_M.translateY 0;setAttr FKSpineB_M.translateZ 0;setAttr FKSpineB_M.rotateX 0;setAttr FKSpineB_M.rotateY -0;setAttr FKSpineB_M.rotateZ 0;setAttr FKSpineB_M.scaleX 1;setAttr FKSpineB_M.scaleY 1;setAttr FKSpineB_M.scaleZ 1;setAttr FKExtraSpineA_M.translateX 2.465190329e-32;setAttr FKExtraSpineA_M.translateY 0;setAttr FKExtraSpineA_M.translateZ 0;setAttr FKExtraSpineA_M.rotateX 0;setAttr FKExtraSpineA_M.rotateY -0;setAttr FKExtraSpineA_M.rotateZ 0;setAttr FKExtraSpineA_M.scaleX 1;setAttr FKExtraSpineA_M.scaleY 1;setAttr FKExtraSpineA_M.scaleZ 1;setAttr FKSpineA_M.translateX 2.465190329e-32;setAttr FKSpineA_M.translateY 0;setAttr FKSpineA_M.translateZ 0;setAttr FKSpineA_M.rotateX 0;setAttr FKSpineA_M.rotateY -0;setAttr FKSpineA_M.rotateZ 0;setAttr FKSpineA_M.scaleX 1;setAttr FKSpineA_M.scaleY 1;setAttr FKSpineA_M.scaleZ 1;setAttr FKExtraAnkle_R.translateX 0;setAttr FKExtraAnkle_R.translateY 5.551115123e-17;setAttr FKExtraAnkle_R.translateZ -2.220446049e-16;setAttr FKExtraAnkle_R.rotateX -0;setAttr FKExtraAnkle_R.rotateY -0;setAttr FKExtraAnkle_R.rotateZ 0;setAttr FKExtraAnkle_R.scaleX 1;setAttr FKExtraAnkle_R.scaleY 1;setAttr FKExtraAnkle_R.scaleZ 1;setAttr FKAnkle_R.translateX 0;setAttr FKAnkle_R.translateY 5.551115123e-17;setAttr FKAnkle_R.translateZ -2.220446049e-16;setAttr FKAnkle_R.rotateX -0;setAttr FKAnkle_R.rotateY -0;setAttr FKAnkle_R.rotateZ 0;setAttr FKAnkle_R.scaleX 1;setAttr FKAnkle_R.scaleY 1;setAttr FKAnkle_R.scaleZ 1;setAttr FKExtraBackKnee_R.translateX -4.440892099e-16;setAttr FKExtraBackKnee_R.translateY 4.440892099e-16;setAttr FKExtraBackKnee_R.translateZ 2.220446049e-16;setAttr FKExtraBackKnee_R.rotateX -0;setAttr FKExtraBackKnee_R.rotateY 0;setAttr FKExtraBackKnee_R.rotateZ 0;setAttr FKExtraBackKnee_R.scaleX 1;setAttr FKExtraBackKnee_R.scaleY 1;setAttr FKExtraBackKnee_R.scaleZ 1;setAttr FKBackKnee_R.translateX -4.440892099e-16;setAttr FKBackKnee_R.translateY 0;setAttr FKBackKnee_R.translateZ 2.220446049e-16;setAttr FKBackKnee_R.rotateX -0;setAttr FKBackKnee_R.rotateY 0;setAttr FKBackKnee_R.rotateZ 0;setAttr FKBackKnee_R.scaleX 1;setAttr FKBackKnee_R.scaleY 1;setAttr FKBackKnee_R.scaleZ 1;setAttr FKExtraKnee_R.translateX -4.440892099e-16;setAttr FKExtraKnee_R.translateY 4.440892099e-16;setAttr FKExtraKnee_R.translateZ 4.440892099e-16;setAttr FKExtraKnee_R.rotateX -0;setAttr FKExtraKnee_R.rotateY 0;setAttr FKExtraKnee_R.rotateZ 0;setAttr FKExtraKnee_R.scaleX 1;setAttr FKExtraKnee_R.scaleY 1;setAttr FKExtraKnee_R.scaleZ 1;setAttr FKKnee_R.translateX -4.440892099e-16;setAttr FKKnee_R.translateY 4.440892099e-16;setAttr FKKnee_R.translateZ 0;setAttr FKKnee_R.rotateX -0;setAttr FKKnee_R.rotateY 0;setAttr FKKnee_R.rotateZ 0;setAttr FKKnee_R.scaleX 1;setAttr FKKnee_R.scaleY 1;setAttr FKKnee_R.scaleZ 1;setAttr FKExtraHip_R.translateX -4.440892099e-16;setAttr FKExtraHip_R.translateY 0;setAttr FKExtraHip_R.translateZ 0;setAttr FKExtraHip_R.rotateX -0;setAttr FKExtraHip_R.rotateY 0;setAttr FKExtraHip_R.rotateZ 0;setAttr FKExtraHip_R.scaleX 1;setAttr FKExtraHip_R.scaleY 1;setAttr FKExtraHip_R.scaleZ 1;setAttr FKHip_R.translateX -4.440892099e-16;setAttr FKHip_R.translateY 0;setAttr FKHip_R.translateZ -8.881784197e-16;setAttr FKHip_R.rotateX -0;setAttr FKHip_R.rotateY 0;setAttr FKHip_R.rotateZ 0;setAttr FKHip_R.scaleX 1;setAttr FKHip_R.scaleY 1;setAttr FKHip_R.scaleZ 1;setAttr FKExtraHipTwist_R.translateX 8.881784197e-16;setAttr FKExtraHipTwist_R.translateY 0;setAttr FKExtraHipTwist_R.translateZ 2.220446049e-16;setAttr FKExtraHipTwist_R.rotateX -0;setAttr FKExtraHipTwist_R.rotateY 0;setAttr FKExtraHipTwist_R.rotateZ 0;setAttr FKExtraHipTwist_R.scaleX 1;setAttr FKExtraHipTwist_R.scaleY 1;setAttr FKExtraHipTwist_R.scaleZ 1;setAttr FKHipTwist_R.translateX 0;setAttr FKHipTwist_R.translateY 0;setAttr FKHipTwist_R.translateZ 0;setAttr FKHipTwist_R.rotateX -0;setAttr FKHipTwist_R.rotateY 0;setAttr FKHipTwist_R.rotateZ 0;setAttr FKHipTwist_R.scaleX 1;setAttr FKHipTwist_R.scaleY 1;setAttr FKHipTwist_R.scaleZ 1;setAttr FKExtraAnkle_L.translateX 4.440892099e-16;setAttr FKExtraAnkle_L.translateY 0;setAttr FKExtraAnkle_L.translateZ 0;setAttr FKExtraAnkle_L.rotateX 6.49222935e-14;setAttr FKExtraAnkle_L.rotateY -1.490885007e-16;setAttr FKExtraAnkle_L.rotateZ 3.811455974e-33;setAttr FKExtraAnkle_L.scaleX 1;setAttr FKExtraAnkle_L.scaleY 1;setAttr FKExtraAnkle_L.scaleZ 1;setAttr FKAnkle_L.translateX 0;setAttr FKAnkle_L.translateY 5.551115123e-17;setAttr FKAnkle_L.translateZ 0;setAttr FKAnkle_L.rotateX -0;setAttr FKAnkle_L.rotateY -0;setAttr FKAnkle_L.rotateZ 0;setAttr FKAnkle_L.scaleX 1;setAttr FKAnkle_L.scaleY 1;setAttr FKAnkle_L.scaleZ 1;setAttr FKExtraBackKnee_L.translateX 4.440892099e-16;setAttr FKExtraBackKnee_L.translateY 4.440892099e-16;setAttr FKExtraBackKnee_L.translateZ -3.330669074e-16;setAttr FKExtraBackKnee_L.rotateX -1.27220246e-14;setAttr FKExtraBackKnee_L.rotateY 1.242404172e-16;setAttr FKExtraBackKnee_L.rotateZ 7.454425035e-17;setAttr FKExtraBackKnee_L.scaleX 1;setAttr FKExtraBackKnee_L.scaleY 1;setAttr FKExtraBackKnee_L.scaleZ 1;setAttr FKBackKnee_L.translateX 4.440892099e-16;setAttr FKBackKnee_L.translateY 4.440892099e-16;setAttr FKBackKnee_L.translateZ -4.440892099e-16;setAttr FKBackKnee_L.rotateX -0;setAttr FKBackKnee_L.rotateY 0;setAttr FKBackKnee_L.rotateZ 0;setAttr FKBackKnee_L.scaleX 1;setAttr FKBackKnee_L.scaleY 1;setAttr FKBackKnee_L.scaleZ 1;setAttr FKExtraKnee_L.translateX 0;setAttr FKExtraKnee_L.translateY 0;setAttr FKExtraKnee_L.translateZ -4.440892099e-16;setAttr FKExtraKnee_L.rotateX -3.816714149e-14;setAttr FKExtraKnee_L.rotateY -4.348414604e-17;setAttr FKExtraKnee_L.rotateZ 1.44833142e-32;setAttr FKExtraKnee_L.scaleX 1;setAttr FKExtraKnee_L.scaleY 1;setAttr FKExtraKnee_L.scaleZ 1;setAttr FKKnee_L.translateX 0;setAttr FKKnee_L.translateY 0;setAttr FKKnee_L.translateZ -4.440892099e-16;setAttr FKKnee_L.rotateX -0;setAttr FKKnee_L.rotateY 0;setAttr FKKnee_L.rotateZ 0;setAttr FKKnee_L.scaleX 1;setAttr FKKnee_L.scaleY 1;setAttr FKKnee_L.scaleZ 1;setAttr FKExtraHip_L.translateX 4.440892099e-16;setAttr FKExtraHip_L.translateY -4.440892099e-16;setAttr FKExtraHip_L.translateZ -4.440892099e-16;setAttr FKExtraHip_L.rotateX -6.361206426e-15;setAttr FKExtraHip_L.rotateY -4.96961669e-17;setAttr FKExtraHip_L.rotateZ -1.242404172e-16;setAttr FKExtraHip_L.scaleX 1;setAttr FKExtraHip_L.scaleY 1;setAttr FKExtraHip_L.scaleZ 1;setAttr FKHip_L.translateX -4.440892099e-16;setAttr FKHip_L.translateY -4.440892099e-16;setAttr FKHip_L.translateZ 8.881784197e-16;setAttr FKHip_L.rotateX -0;setAttr FKHip_L.rotateY 0;setAttr FKHip_L.rotateZ 0;setAttr FKHip_L.scaleX 1;setAttr FKHip_L.scaleY 1;setAttr FKHip_L.scaleZ 1;setAttr FKExtraHipTwist_L.translateX -1.776356839e-15;setAttr FKExtraHipTwist_L.translateY 4.440892099e-16;setAttr FKExtraHipTwist_L.translateZ -3.330669074e-16;setAttr FKExtraHipTwist_L.rotateX 4.077471102e-12;setAttr FKExtraHipTwist_L.rotateY 1.590277341e-15;setAttr FKExtraHipTwist_L.rotateZ 1.948089742e-14;setAttr FKExtraHipTwist_L.scaleX 1;setAttr FKExtraHipTwist_L.scaleY 1;setAttr FKExtraHipTwist_L.scaleZ 1;setAttr FKHipTwist_L.translateX -8.881784197e-16;setAttr FKHipTwist_L.translateY 2.220446049e-16;setAttr FKHipTwist_L.translateZ -2.220446049e-16;setAttr FKHipTwist_L.rotateX -0;setAttr FKHipTwist_L.rotateY 0;setAttr FKHipTwist_L.rotateZ 0;setAttr FKHipTwist_L.scaleX 1;setAttr FKHipTwist_L.scaleY 1;setAttr FKHipTwist_L.scaleZ 1;");
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "jointLayer.di" "Pelvis.do";
connectAttr "Pelvis.s" "SpineA.is";
connectAttr "jointLayer.di" "SpineA.do";
connectAttr "jointLayer.di" "SpineB.do";
connectAttr "jointLayer.di" "Chest.do";
connectAttr "Chest.s" "Head.is";
connectAttr "jointLayer.di" "Head.do";
connectAttr "jointLayer.di" "Head_End.do";
connectAttr "Head.s" "Head_End.is";
connectAttr "Chest.s" "Gun1.is";
connectAttr "jointLayer.di" "Gun1.do";
connectAttr "jointLayer.di" "Gun2.do";
connectAttr "Gun2.s" "Gun3_End.is";
connectAttr "jointLayer.di" "Gun3_End.do";
connectAttr "Chest.s" "Shoulder1.is";
connectAttr "jointLayer.di" "Shoulder1.do";
connectAttr "Shoulder1.s" "Elbow1.is";
connectAttr "jointLayer.di" "Elbow1.do";
connectAttr "Elbow1.s" "Wrist2.is";
connectAttr "jointLayer.di" "Wrist2.do";
connectAttr "jointLayer.di" "Wrist1.do";
connectAttr "Wrist1.s" "MiddleFinger1.is";
connectAttr "jointLayer.di" "MiddleFinger1.do";
connectAttr "MiddleFinger1.s" "MiddleFinger2.is";
connectAttr "jointLayer.di" "MiddleFinger2.do";
connectAttr "jointLayer.di" "MiddleFinger3_End.do";
connectAttr "MiddleFinger2.s" "MiddleFinger3_End.is";
connectAttr "Wrist1.s" "IndexFinger1.is";
connectAttr "jointLayer.di" "IndexFinger1.do";
connectAttr "IndexFinger1.s" "IndexFinger2.is";
connectAttr "jointLayer.di" "IndexFinger2.do";
connectAttr "jointLayer.di" "IndexFinger3_End.do";
connectAttr "IndexFinger2.s" "IndexFinger3_End.is";
connectAttr "Chest.s" "CapeMid1.is";
connectAttr "jointLayer.di" "CapeMid1.do";
connectAttr "jointLayer.di" "CapeMid2.do";
connectAttr "CapeMid1.s" "CapeMid2.is";
connectAttr "CapeMid2.s" "CapeMid3.is";
connectAttr "jointLayer.di" "CapeMid3.do";
connectAttr "jointLayer.di" "CapeMid4.do";
connectAttr "CapeMid3.s" "CapeMid4.is";
connectAttr "CapeMid4.s" "CapeMid5.is";
connectAttr "jointLayer.di" "CapeMid5.do";
connectAttr "jointLayer.di" "CapeMid6_End.do";
connectAttr "CapeMid5.s" "CapeMid6_End.is";
connectAttr "Chest.s" "CapeR1.is";
connectAttr "jointLayer.di" "CapeR1.do";
connectAttr "jointLayer.di" "CapeR2.do";
connectAttr "CapeR1.s" "CapeR2.is";
connectAttr "CapeR2.s" "CapeR3.is";
connectAttr "jointLayer.di" "CapeR3.do";
connectAttr "jointLayer.di" "CapeR4_End.do";
connectAttr "CapeR3.s" "CapeR4_End.is";
connectAttr "Chest.s" "CapeL1.is";
connectAttr "jointLayer.di" "CapeL1.do";
connectAttr "jointLayer.di" "CapeL2.do";
connectAttr "CapeL1.s" "CapeL2.is";
connectAttr "CapeL2.s" "CapeL3.is";
connectAttr "jointLayer.di" "CapeL3.do";
connectAttr "jointLayer.di" "CapeL4_End.do";
connectAttr "CapeL3.s" "CapeL4_End.is";
connectAttr "Pelvis.s" "HipTwist.is";
connectAttr "jointLayer.di" "HipTwist.do";
connectAttr "jointLayer.di" "Hip.do";
connectAttr "Hip.s" "Knee.is";
connectAttr "jointLayer.di" "Knee.do";
connectAttr "jointLayer.di" "BackKnee.do";
connectAttr "jointLayer.di" "Ankle.do";
connectAttr "Ankle.s" "Foot_End.is";
connectAttr "jointLayer.di" "Foot_End.do";
connectAttr "Ankle.s" "Heel_End.is";
connectAttr "jointLayer.di" "Heel_End.do";
connectAttr "Main.fkVis" "FKSystem.v";
connectAttr "FKParentConstraintToChest_M_parentConstraint1.ctx" "FKParentConstraintToChest_M.tx"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.cty" "FKParentConstraintToChest_M.ty"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.ctz" "FKParentConstraintToChest_M.tz"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.crx" "FKParentConstraintToChest_M.rx"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.cry" "FKParentConstraintToChest_M.ry"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.crz" "FKParentConstraintToChest_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetHead_M.do";
connectAttr "jointLayer.di" "FKXHead_M.do";
connectAttr "FKHead_M.s" "FKXHead_End_M.is";
connectAttr "jointLayer.di" "FKXHead_End_M.do";
connectAttr "jointLayer.di" "FKOffsetGun1_R.do";
connectAttr "jointLayer.di" "FKXGun1_R.do";
connectAttr "FKXGun1_R.s" "FKOffsetGun2_R.is";
connectAttr "jointLayer.di" "FKOffsetGun2_R.do";
connectAttr "FKGun1_R.s" "FKXGun2_R.is";
connectAttr "jointLayer.di" "FKXGun2_R.do";
connectAttr "FKGun2_R.s" "FKXGun3_End_R.is";
connectAttr "jointLayer.di" "FKXGun3_End_R.do";
connectAttr "FKIKBlendArmCondition_L.ocg" "FKOffsetShoulder1_L.v" -l on;
connectAttr "jointLayer.di" "FKOffsetShoulder1_L.do";
connectAttr "jointLayer.di" "FKXShoulder1_L.do";
connectAttr "FKXShoulder1_L.s" "FKOffsetElbow1_L.is";
connectAttr "jointLayer.di" "FKOffsetElbow1_L.do";
connectAttr "FKShoulder1_L.s" "FKXElbow1_L.is";
connectAttr "jointLayer.di" "FKXElbow1_L.do";
connectAttr "FKXElbow1_L.s" "FKOffsetWrist2_L.is";
connectAttr "jointLayer.di" "FKOffsetWrist2_L.do";
connectAttr "FKElbow1_L.s" "FKXWrist2_L.is";
connectAttr "jointLayer.di" "FKXWrist2_L.do";
connectAttr "FKXWrist2_L.s" "FKOffsetWrist1_L.is";
connectAttr "jointLayer.di" "FKOffsetWrist1_L.do";
connectAttr "FKWrist2_L.s" "FKXWrist1_L.is";
connectAttr "jointLayer.di" "FKXWrist1_L.do";
connectAttr "jointLayer.di" "FKOffsetCapeMid1_L.do";
connectAttr "jointLayer.di" "FKXCapeMid1_L.do";
connectAttr "FKXCapeMid1_L.s" "FKOffsetCapeMid2_L.is";
connectAttr "jointLayer.di" "FKOffsetCapeMid2_L.do";
connectAttr "FKCapeMid1_L.s" "FKXCapeMid2_L.is";
connectAttr "jointLayer.di" "FKXCapeMid2_L.do";
connectAttr "FKXCapeMid2_L.s" "FKOffsetCapeMid3_L.is";
connectAttr "jointLayer.di" "FKOffsetCapeMid3_L.do";
connectAttr "FKCapeMid2_L.s" "FKXCapeMid3_L.is";
connectAttr "jointLayer.di" "FKXCapeMid3_L.do";
connectAttr "FKXCapeMid3_L.s" "FKOffsetCapeMid4_L.is";
connectAttr "jointLayer.di" "FKOffsetCapeMid4_L.do";
connectAttr "FKCapeMid3_L.s" "FKXCapeMid4_L.is";
connectAttr "jointLayer.di" "FKXCapeMid4_L.do";
connectAttr "FKXCapeMid4_L.s" "FKOffsetCapeMid5_L.is";
connectAttr "jointLayer.di" "FKOffsetCapeMid5_L.do";
connectAttr "FKCapeMid4_L.s" "FKXCapeMid5_L.is";
connectAttr "jointLayer.di" "FKXCapeMid5_L.do";
connectAttr "FKCapeMid5_L.s" "FKXCapeMid6_End_L.is";
connectAttr "jointLayer.di" "FKXCapeMid6_End_L.do";
connectAttr "jointLayer.di" "FKOffsetCapeR1_R.do";
connectAttr "jointLayer.di" "FKXCapeR1_R.do";
connectAttr "FKXCapeR1_R.s" "FKOffsetCapeR2_R.is";
connectAttr "jointLayer.di" "FKOffsetCapeR2_R.do";
connectAttr "FKCapeR1_R.s" "FKXCapeR2_R.is";
connectAttr "jointLayer.di" "FKXCapeR2_R.do";
connectAttr "FKXCapeR2_R.s" "FKOffsetCapeR3_R.is";
connectAttr "jointLayer.di" "FKOffsetCapeR3_R.do";
connectAttr "FKCapeR2_R.s" "FKXCapeR3_R.is";
connectAttr "jointLayer.di" "FKXCapeR3_R.do";
connectAttr "FKCapeR3_R.s" "FKXCapeR4_End_R.is";
connectAttr "jointLayer.di" "FKXCapeR4_End_R.do";
connectAttr "jointLayer.di" "FKOffsetCapeL1_L.do";
connectAttr "jointLayer.di" "FKXCapeL1_L.do";
connectAttr "FKXCapeL1_L.s" "FKOffsetCapeL2_L.is";
connectAttr "jointLayer.di" "FKOffsetCapeL2_L.do";
connectAttr "FKCapeL1_L.s" "FKXCapeL2_L.is";
connectAttr "jointLayer.di" "FKXCapeL2_L.do";
connectAttr "FKXCapeL2_L.s" "FKOffsetCapeL3_L.is";
connectAttr "jointLayer.di" "FKOffsetCapeL3_L.do";
connectAttr "FKCapeL2_L.s" "FKXCapeL3_L.is";
connectAttr "jointLayer.di" "FKXCapeL3_L.do";
connectAttr "FKCapeL3_L.s" "FKXCapeL4_End_L.is";
connectAttr "jointLayer.di" "FKXCapeL4_End_L.do";
connectAttr "FKParentConstraintToChest_M.ro" "FKParentConstraintToChest_M_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToChest_M.pim" "FKParentConstraintToChest_M_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToChest_M.rp" "FKParentConstraintToChest_M_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToChest_M.rpt" "FKParentConstraintToChest_M_parentConstraint1.crt"
		;
connectAttr "Chest_M.t" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Chest_M.rp" "FKParentConstraintToChest_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Chest_M.rpt" "FKParentConstraintToChest_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Chest_M.r" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Chest_M.ro" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Chest_M.s" "FKParentConstraintToChest_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Chest_M.pm" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "Chest_M.jo" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.w0" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tw"
		;
connectAttr "Main.fingerVis" "FKParentConstraintToWrist1_L.v";
connectAttr "FKParentConstraintToWrist1_L_parentConstraint1.ctx" "FKParentConstraintToWrist1_L.tx"
		;
connectAttr "FKParentConstraintToWrist1_L_parentConstraint1.cty" "FKParentConstraintToWrist1_L.ty"
		;
connectAttr "FKParentConstraintToWrist1_L_parentConstraint1.ctz" "FKParentConstraintToWrist1_L.tz"
		;
connectAttr "FKParentConstraintToWrist1_L_parentConstraint1.crx" "FKParentConstraintToWrist1_L.rx"
		;
connectAttr "FKParentConstraintToWrist1_L_parentConstraint1.cry" "FKParentConstraintToWrist1_L.ry"
		;
connectAttr "FKParentConstraintToWrist1_L_parentConstraint1.crz" "FKParentConstraintToWrist1_L.rz"
		;
connectAttr "jointLayer.di" "FKOffsetMiddleFinger1_L.do";
connectAttr "jointLayer.di" "FKXMiddleFinger1_L.do";
connectAttr "FKXMiddleFinger1_L.s" "FKOffsetMiddleFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger2_L.do";
connectAttr "FKMiddleFinger1_L.s" "FKXMiddleFinger2_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger2_L.do";
connectAttr "FKMiddleFinger2_L.s" "FKXMiddleFinger3_End_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger3_End_L.do";
connectAttr "jointLayer.di" "FKOffsetIndexFinger1_L.do";
connectAttr "jointLayer.di" "FKXIndexFinger1_L.do";
connectAttr "FKXIndexFinger1_L.s" "FKOffsetIndexFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetIndexFinger2_L.do";
connectAttr "FKIndexFinger1_L.s" "FKXIndexFinger2_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger2_L.do";
connectAttr "FKIndexFinger2_L.s" "FKXIndexFinger3_End_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger3_End_L.do";
connectAttr "FKParentConstraintToWrist1_L.ro" "FKParentConstraintToWrist1_L_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToWrist1_L.pim" "FKParentConstraintToWrist1_L_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToWrist1_L.rp" "FKParentConstraintToWrist1_L_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToWrist1_L.rpt" "FKParentConstraintToWrist1_L_parentConstraint1.crt"
		;
connectAttr "Wrist1_L.t" "FKParentConstraintToWrist1_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Wrist1_L.rp" "FKParentConstraintToWrist1_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Wrist1_L.rpt" "FKParentConstraintToWrist1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Wrist1_L.r" "FKParentConstraintToWrist1_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Wrist1_L.ro" "FKParentConstraintToWrist1_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Wrist1_L.s" "FKParentConstraintToWrist1_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Wrist1_L.pm" "FKParentConstraintToWrist1_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Wrist1_L.jo" "FKParentConstraintToWrist1_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToWrist1_L_parentConstraint1.w0" "FKParentConstraintToWrist1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.ctx" "FKParentConstraintToPelvis_M.tx"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.cty" "FKParentConstraintToPelvis_M.ty"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.ctz" "FKParentConstraintToPelvis_M.tz"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.crx" "FKParentConstraintToPelvis_M.rx"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.cry" "FKParentConstraintToPelvis_M.ry"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.crz" "FKParentConstraintToPelvis_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetHipTwist_R.do";
connectAttr "jointLayer.di" "FKXHipTwist_R.do";
connectAttr "IKXLegAim_R_aimConstraint1.crx" "IKXLegAim_R.rx";
connectAttr "IKXLegAim_R_aimConstraint1.cry" "IKXLegAim_R.ry";
connectAttr "IKXLegAim_R_aimConstraint1.crz" "IKXLegAim_R.rz";
connectAttr "IKXLegAim_R.pim" "IKXLegAim_R_aimConstraint1.cpim";
connectAttr "IKXLegAim_R.t" "IKXLegAim_R_aimConstraint1.ct";
connectAttr "IKXLegAim_R.rp" "IKXLegAim_R_aimConstraint1.crp";
connectAttr "IKXLegAim_R.rpt" "IKXLegAim_R_aimConstraint1.crt";
connectAttr "IKXLegAim_R.ro" "IKXLegAim_R_aimConstraint1.cro";
connectAttr "IKLeg_R.t" "IKXLegAim_R_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "IKXLegAim_R_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "IKXLegAim_R_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "IKXLegAim_R_aimConstraint1.tg[0].tpm";
connectAttr "IKXLegAim_R_aimConstraint1.w0" "IKXLegAim_R_aimConstraint1.tg[0].tw"
		;
connectAttr "PoleLeg_R.wm" "IKXLegAim_R_aimConstraint1.wum";
connectAttr "IKXLegAimBlend_R_orientConstraint1.crx" "IKXLegAimBlend_R.rx";
connectAttr "IKXLegAimBlend_R_orientConstraint1.cry" "IKXLegAimBlend_R.ry";
connectAttr "IKXLegAimBlend_R_orientConstraint1.crz" "IKXLegAimBlend_R.rz";
connectAttr "IKXLegAimBlend_R.ro" "IKXLegAimBlend_R_orientConstraint1.cro";
connectAttr "IKXLegAimBlend_R.pim" "IKXLegAimBlend_R_orientConstraint1.cpim";
connectAttr "IKXLegAim_R.r" "IKXLegAimBlend_R_orientConstraint1.tg[0].tr";
connectAttr "IKXLegAim_R.ro" "IKXLegAimBlend_R_orientConstraint1.tg[0].tro";
connectAttr "IKXLegAim_R.pm" "IKXLegAimBlend_R_orientConstraint1.tg[0].tpm";
connectAttr "IKXLegAimBlend_R_orientConstraint1.w0" "IKXLegAimBlend_R_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXLegUnAim_R.r" "IKXLegAimBlend_R_orientConstraint1.tg[1].tr";
connectAttr "IKXLegUnAim_R.ro" "IKXLegAimBlend_R_orientConstraint1.tg[1].tro";
connectAttr "IKXLegUnAim_R.pm" "IKXLegAimBlend_R_orientConstraint1.tg[1].tpm";
connectAttr "IKXLegAimBlend_R_orientConstraint1.w1" "IKXLegAimBlend_R_orientConstraint1.tg[1].tw"
		;
connectAttr "IKLegAimLegMultiplyDivide_R.oy" "IKXLegAimBlend_R_orientConstraint1.w0"
		;
connectAttr "IKLegAimLegReverse_R.oy" "IKXLegAimBlend_R_orientConstraint1.w1";
connectAttr "jointLayer.di" "FKOffsetHip_R.do";
connectAttr "FKHipTwist_R.s" "FKXHip_R.is";
connectAttr "jointLayer.di" "FKXHip_R.do";
connectAttr "FKXHip_R.s" "FKOffsetKnee_R.is";
connectAttr "FKIKBlendLegCondition_R.ocg" "FKOffsetKnee_R.v" -l on;
connectAttr "jointLayer.di" "FKOffsetKnee_R.do";
connectAttr "jointLayer.di" "FKXKnee_R.do";
connectAttr "FKXKnee_R.s" "FKOffsetBackKnee_R.is";
connectAttr "jointLayer.di" "FKOffsetBackKnee_R.do";
connectAttr "FKKnee_R.s" "FKXBackKnee_R.is";
connectAttr "jointLayer.di" "FKXBackKnee_R.do";
connectAttr "FKXBackKnee_R.s" "FKOffsetAnkle_R.is";
connectAttr "jointLayer.di" "FKOffsetAnkle_R.do";
connectAttr "FKBackKnee_R.s" "FKXAnkle_R.is";
connectAttr "jointLayer.di" "FKXAnkle_R.do";
connectAttr "FKAnkle_R.s" "FKXFoot_End_R.is";
connectAttr "jointLayer.di" "FKXFoot_End_R.do";
connectAttr "FKAnkle_R.s" "FKXHeel_End_R.is";
connectAttr "jointLayer.di" "FKXHeel_End_R.do";
connectAttr "jointLayer.di" "FKOffsetHipTwist_L.do";
connectAttr "jointLayer.di" "FKXHipTwist_L.do";
connectAttr "IKXLegAim_L_aimConstraint1.crx" "IKXLegAim_L.rx";
connectAttr "IKXLegAim_L_aimConstraint1.cry" "IKXLegAim_L.ry";
connectAttr "IKXLegAim_L_aimConstraint1.crz" "IKXLegAim_L.rz";
connectAttr "IKXLegAim_L.pim" "IKXLegAim_L_aimConstraint1.cpim";
connectAttr "IKXLegAim_L.t" "IKXLegAim_L_aimConstraint1.ct";
connectAttr "IKXLegAim_L.rp" "IKXLegAim_L_aimConstraint1.crp";
connectAttr "IKXLegAim_L.rpt" "IKXLegAim_L_aimConstraint1.crt";
connectAttr "IKXLegAim_L.ro" "IKXLegAim_L_aimConstraint1.cro";
connectAttr "IKLeg_L.t" "IKXLegAim_L_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_L.rp" "IKXLegAim_L_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_L.rpt" "IKXLegAim_L_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_L.pm" "IKXLegAim_L_aimConstraint1.tg[0].tpm";
connectAttr "IKXLegAim_L_aimConstraint1.w0" "IKXLegAim_L_aimConstraint1.tg[0].tw"
		;
connectAttr "PoleLeg_L.wm" "IKXLegAim_L_aimConstraint1.wum";
connectAttr "IKXLegAimBlend_L_orientConstraint1.crx" "IKXLegAimBlend_L.rx";
connectAttr "IKXLegAimBlend_L_orientConstraint1.cry" "IKXLegAimBlend_L.ry";
connectAttr "IKXLegAimBlend_L_orientConstraint1.crz" "IKXLegAimBlend_L.rz";
connectAttr "IKXLegAimBlend_L.ro" "IKXLegAimBlend_L_orientConstraint1.cro";
connectAttr "IKXLegAimBlend_L.pim" "IKXLegAimBlend_L_orientConstraint1.cpim";
connectAttr "IKXLegAim_L.r" "IKXLegAimBlend_L_orientConstraint1.tg[0].tr";
connectAttr "IKXLegAim_L.ro" "IKXLegAimBlend_L_orientConstraint1.tg[0].tro";
connectAttr "IKXLegAim_L.pm" "IKXLegAimBlend_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXLegAimBlend_L_orientConstraint1.w0" "IKXLegAimBlend_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXLegUnAim_L.r" "IKXLegAimBlend_L_orientConstraint1.tg[1].tr";
connectAttr "IKXLegUnAim_L.ro" "IKXLegAimBlend_L_orientConstraint1.tg[1].tro";
connectAttr "IKXLegUnAim_L.pm" "IKXLegAimBlend_L_orientConstraint1.tg[1].tpm";
connectAttr "IKXLegAimBlend_L_orientConstraint1.w1" "IKXLegAimBlend_L_orientConstraint1.tg[1].tw"
		;
connectAttr "IKLegAimLegMultiplyDivide_L.oy" "IKXLegAimBlend_L_orientConstraint1.w0"
		;
connectAttr "IKLegAimLegReverse_L.oy" "IKXLegAimBlend_L_orientConstraint1.w1";
connectAttr "jointLayer.di" "FKOffsetHip_L.do";
connectAttr "FKHipTwist_L.s" "FKXHip_L.is";
connectAttr "jointLayer.di" "FKXHip_L.do";
connectAttr "FKXHip_L.s" "FKOffsetKnee_L.is";
connectAttr "FKIKBlendLegCondition_L.ocg" "FKOffsetKnee_L.v" -l on;
connectAttr "jointLayer.di" "FKOffsetKnee_L.do";
connectAttr "jointLayer.di" "FKXKnee_L.do";
connectAttr "FKXKnee_L.s" "FKOffsetBackKnee_L.is";
connectAttr "jointLayer.di" "FKOffsetBackKnee_L.do";
connectAttr "FKKnee_L.s" "FKXBackKnee_L.is";
connectAttr "jointLayer.di" "FKXBackKnee_L.do";
connectAttr "FKXBackKnee_L.s" "FKOffsetAnkle_L.is";
connectAttr "jointLayer.di" "FKOffsetAnkle_L.do";
connectAttr "FKBackKnee_L.s" "FKXAnkle_L.is";
connectAttr "jointLayer.di" "FKXAnkle_L.do";
connectAttr "FKAnkle_L.s" "FKXFoot_End_L.is";
connectAttr "jointLayer.di" "FKXFoot_End_L.do";
connectAttr "FKAnkle_L.s" "FKXHeel_End_L.is";
connectAttr "jointLayer.di" "FKXHeel_End_L.do";
connectAttr "FKParentConstraintToPelvis_M.ro" "FKParentConstraintToPelvis_M_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToPelvis_M.pim" "FKParentConstraintToPelvis_M_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToPelvis_M.rp" "FKParentConstraintToPelvis_M_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToPelvis_M.rpt" "FKParentConstraintToPelvis_M_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Pelvis_M.rp" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Pelvis_M.ro" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Pelvis_M.pm" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.w0" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctx" "PelvisCenterBtwLegsBlended_M.tx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cty" "PelvisCenterBtwLegsBlended_M.ty"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctz" "PelvisCenterBtwLegsBlended_M.tz"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crx" "PelvisCenterBtwLegsBlended_M.rx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cry" "PelvisCenterBtwLegsBlended_M.ry"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crz" "PelvisCenterBtwLegsBlended_M.rz"
		;
connectAttr "FKIKBlendSpineCondition_M.ocg" "FKOffsetPelvis_M.v" -l on;
connectAttr "jointLayer.di" "FKOffsetPelvis_M.do";
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.crx" "HipSwingerGroupPelvis_M.rx"
		;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.cry" "HipSwingerGroupPelvis_M.ry"
		;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.crz" "HipSwingerGroupPelvis_M.rz"
		;
connectAttr "HipSwingerGroupPelvis_M.ro" "HipSwingerGroupPelvis_M_orientConstraint1.cro"
		;
connectAttr "HipSwingerGroupPelvis_M.pim" "HipSwingerGroupPelvis_M_orientConstraint1.cpim"
		;
connectAttr "HipSwingerPelvis_M.r" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tr"
		;
connectAttr "HipSwingerPelvis_M.ro" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tro"
		;
connectAttr "HipSwingerPelvis_M.pm" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tpm"
		;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.w0" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "jointLayer.di" "FKXPelvis_M.do";
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.crx" "HipSwingerStabalizePelvis_M.rx"
		;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.cry" "HipSwingerStabalizePelvis_M.ry"
		;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.crz" "HipSwingerStabalizePelvis_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetSpineA_M.do";
connectAttr "FKPelvis_M.s" "FKXSpineA_M.is";
connectAttr "jointLayer.di" "FKXSpineA_M.do";
connectAttr "FKXSpineA_M.s" "FKOffsetSpineB_M.is";
connectAttr "jointLayer.di" "FKOffsetSpineB_M.do";
connectAttr "FKSpineA_M.s" "FKXSpineB_M.is";
connectAttr "jointLayer.di" "FKXSpineB_M.do";
connectAttr "FKXSpineB_M.s" "FKOffsetChest_M.is";
connectAttr "jointLayer.di" "FKOffsetChest_M.do";
connectAttr "FKSpineB_M.s" "FKXChest_M.is";
connectAttr "jointLayer.di" "FKXChest_M.do";
connectAttr "HipSwingerStabalizePelvis_M.ro" "HipSwingerStabalizePelvis_M_orientConstraint1.cro"
		;
connectAttr "HipSwingerStabalizePelvis_M.pim" "HipSwingerStabalizePelvis_M_orientConstraint1.cpim"
		;
connectAttr "Center_M.r" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tro"
		;
connectAttr "Center_M.pm" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tpm"
		;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.w0" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocg" "HipSwingerPelvis_M.v" -l on;
connectAttr "PelvisCenterBtwLegsBlended_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.pim" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crt"
		;
connectAttr "PelvisCenter_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tt"
		;
connectAttr "PelvisCenter_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trp"
		;
connectAttr "PelvisCenter_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trt"
		;
connectAttr "PelvisCenter_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tr"
		;
connectAttr "PelvisCenter_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tro"
		;
connectAttr "PelvisCenter_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].ts"
		;
connectAttr "PelvisCenter_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trp"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tr"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tro"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].ts"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tw"
		;
connectAttr "PelvisCenterBtwFeetReverse_M.ox" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0"
		;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1"
		;
connectAttr "Main.ikVis" "IKSystem.v";
connectAttr "IKParentConstraintShoulder1_L_parentConstraint1.ctx" "IKParentConstraintShoulder1_L.tx"
		;
connectAttr "IKParentConstraintShoulder1_L_parentConstraint1.cty" "IKParentConstraintShoulder1_L.ty"
		;
connectAttr "IKParentConstraintShoulder1_L_parentConstraint1.ctz" "IKParentConstraintShoulder1_L.tz"
		;
connectAttr "IKParentConstraintShoulder1_L_parentConstraint1.crx" "IKParentConstraintShoulder1_L.rx"
		;
connectAttr "IKParentConstraintShoulder1_L_parentConstraint1.cry" "IKParentConstraintShoulder1_L.ry"
		;
connectAttr "IKParentConstraintShoulder1_L_parentConstraint1.crz" "IKParentConstraintShoulder1_L.rz"
		;
connectAttr "FKIKBlendArmCondition_L.ocr" "IKParentConstraintShoulder1_L.v";
connectAttr "jointLayer.di" "IKXShoulder1_L.do";
connectAttr "IKXShoulder1_L.s" "IKXElbow1_L.is";
connectAttr "jointLayer.di" "IKXElbow1_L.do";
connectAttr "IKXElbow1_L.s" "IKXWrist2_L.is";
connectAttr "IKXWrist2_L_orientConstraint1.crx" "IKXWrist2_L.rx";
connectAttr "IKXWrist2_L_orientConstraint1.cry" "IKXWrist2_L.ry";
connectAttr "IKXWrist2_L_orientConstraint1.crz" "IKXWrist2_L.rz";
connectAttr "jointLayer.di" "IKXWrist2_L.do";
connectAttr "IKXWrist2_L.s" "IKXWrist1_L.is";
connectAttr "jointLayer.di" "IKXWrist1_L.do";
connectAttr "IKXWrist2_L.ro" "IKXWrist2_L_orientConstraint1.cro";
connectAttr "IKXWrist2_L.pim" "IKXWrist2_L_orientConstraint1.cpim";
connectAttr "IKXWrist2_L.jo" "IKXWrist2_L_orientConstraint1.cjo";
connectAttr "IKArm_L.r" "IKXWrist2_L_orientConstraint1.tg[0].tr";
connectAttr "IKArm_L.ro" "IKXWrist2_L_orientConstraint1.tg[0].tro";
connectAttr "IKArm_L.pm" "IKXWrist2_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXWrist2_L_orientConstraint1.w0" "IKXWrist2_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXWrist2_L.tx" "effector1.tx";
connectAttr "IKXWrist2_L.ty" "effector1.ty";
connectAttr "IKXWrist2_L.tz" "effector1.tz";
connectAttr "Main.arrowVis" "PoleAnnotationArm_L.v";
connectAttr "PoleAnnotateTargetArm_LShape.wm" "PoleAnnotationArm_LShape.dom" -na
		;
connectAttr "IKParentConstraintShoulder1_L.ro" "IKParentConstraintShoulder1_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintShoulder1_L.pim" "IKParentConstraintShoulder1_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintShoulder1_L.rp" "IKParentConstraintShoulder1_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintShoulder1_L.rpt" "IKParentConstraintShoulder1_L_parentConstraint1.crt"
		;
connectAttr "Chest_M.t" "IKParentConstraintShoulder1_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Chest_M.rp" "IKParentConstraintShoulder1_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Chest_M.rpt" "IKParentConstraintShoulder1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Chest_M.r" "IKParentConstraintShoulder1_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Chest_M.ro" "IKParentConstraintShoulder1_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Chest_M.s" "IKParentConstraintShoulder1_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Chest_M.pm" "IKParentConstraintShoulder1_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Chest_M.jo" "IKParentConstraintShoulder1_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintShoulder1_L_parentConstraint1.w0" "IKParentConstraintShoulder1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintKnee_R_parentConstraint1.ctx" "IKParentConstraintKnee_R.tx"
		;
connectAttr "IKParentConstraintKnee_R_parentConstraint1.cty" "IKParentConstraintKnee_R.ty"
		;
connectAttr "IKParentConstraintKnee_R_parentConstraint1.ctz" "IKParentConstraintKnee_R.tz"
		;
connectAttr "IKParentConstraintKnee_R_parentConstraint1.crx" "IKParentConstraintKnee_R.rx"
		;
connectAttr "IKParentConstraintKnee_R_parentConstraint1.cry" "IKParentConstraintKnee_R.ry"
		;
connectAttr "IKParentConstraintKnee_R_parentConstraint1.crz" "IKParentConstraintKnee_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintKnee_R.v";
connectAttr "jointLayer.di" "IKXKnee_R.do";
connectAttr "IKXKnee_R.s" "IKXBackKnee_R.is";
connectAttr "jointLayer.di" "IKXBackKnee_R.do";
connectAttr "IKXBackKnee_R.s" "IKXAnkle_R.is";
connectAttr "IKXAnkle_R_orientConstraint1.crx" "IKXAnkle_R.rx";
connectAttr "IKXAnkle_R_orientConstraint1.cry" "IKXAnkle_R.ry";
connectAttr "IKXAnkle_R_orientConstraint1.crz" "IKXAnkle_R.rz";
connectAttr "jointLayer.di" "IKXAnkle_R.do";
connectAttr "IKXAnkle_R.s" "IKXFoot_End_R.is";
connectAttr "jointLayer.di" "IKXFoot_End_R.do";
connectAttr "IKXAnkle_R.ro" "IKXAnkle_R_orientConstraint1.cro";
connectAttr "IKXAnkle_R.pim" "IKXAnkle_R_orientConstraint1.cpim";
connectAttr "IKXAnkle_R.jo" "IKXAnkle_R_orientConstraint1.cjo";
connectAttr "IKLeg_R.r" "IKXAnkle_R_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "IKXAnkle_R_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "IKXAnkle_R_orientConstraint1.tg[0].tpm";
connectAttr "IKXAnkle_R_orientConstraint1.w0" "IKXAnkle_R_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXFoot_End_R.tx" "effector3.tx";
connectAttr "IKXFoot_End_R.ty" "effector3.ty";
connectAttr "IKXFoot_End_R.tz" "effector3.tz";
connectAttr "IKXAnkle_R.tx" "effector2.tx";
connectAttr "IKXAnkle_R.ty" "effector2.ty";
connectAttr "IKXAnkle_R.tz" "effector2.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_R.v";
connectAttr "PoleAnnotateTargetLeg_RShape.wm" "PoleAnnotationLeg_RShape.dom" -na
		;
connectAttr "IKParentConstraintKnee_R.ro" "IKParentConstraintKnee_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintKnee_R.pim" "IKParentConstraintKnee_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintKnee_R.rp" "IKParentConstraintKnee_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintKnee_R.rpt" "IKParentConstraintKnee_R_parentConstraint1.crt"
		;
connectAttr "Hip_R.t" "IKParentConstraintKnee_R_parentConstraint1.tg[0].tt";
connectAttr "Hip_R.rp" "IKParentConstraintKnee_R_parentConstraint1.tg[0].trp";
connectAttr "Hip_R.rpt" "IKParentConstraintKnee_R_parentConstraint1.tg[0].trt";
connectAttr "Hip_R.r" "IKParentConstraintKnee_R_parentConstraint1.tg[0].tr";
connectAttr "Hip_R.ro" "IKParentConstraintKnee_R_parentConstraint1.tg[0].tro";
connectAttr "Hip_R.s" "IKParentConstraintKnee_R_parentConstraint1.tg[0].ts";
connectAttr "Hip_R.pm" "IKParentConstraintKnee_R_parentConstraint1.tg[0].tpm";
connectAttr "Hip_R.jo" "IKParentConstraintKnee_R_parentConstraint1.tg[0].tjo";
connectAttr "IKParentConstraintKnee_R_parentConstraint1.w0" "IKParentConstraintKnee_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintPelvis_M.v";
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.ctx" "IKParentConstraintPelvis_M.tx"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.cty" "IKParentConstraintPelvis_M.ty"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.ctz" "IKParentConstraintPelvis_M.tz"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.crx" "IKParentConstraintPelvis_M.rx"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.cry" "IKParentConstraintPelvis_M.ry"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.crz" "IKParentConstraintPelvis_M.rz"
		;
connectAttr "jointLayer.di" "IKXPelvis_M.do";
connectAttr "IKXPelvis_M.s" "IKXSpineA_M.is";
connectAttr "stretchySpineBlendTwo4_M.o" "IKXSpineA_M.ty";
connectAttr "jointLayer.di" "IKXSpineA_M.do";
connectAttr "IKXSpineA_M.s" "IKXSpineB_M.is";
connectAttr "stretchySpineBlendTwo1_M.o" "IKXSpineB_M.ty";
connectAttr "jointLayer.di" "IKXSpineB_M.do";
connectAttr "IKXSpineB_M.s" "IKXChest_M.is";
connectAttr "IKXChest_M_parentConstraint1.crx" "IKXChest_M.rx";
connectAttr "IKXChest_M_parentConstraint1.cry" "IKXChest_M.ry";
connectAttr "IKXChest_M_parentConstraint1.crz" "IKXChest_M.rz";
connectAttr "IKXChest_M_parentConstraint1.cty" "IKXChest_M.ty";
connectAttr "IKXChest_M_parentConstraint1.ctx" "IKXChest_M.tx";
connectAttr "IKXChest_M_parentConstraint1.ctz" "IKXChest_M.tz";
connectAttr "jointLayer.di" "IKXChest_M.do";
connectAttr "IKXChest_M.ro" "IKXChest_M_parentConstraint1.cro";
connectAttr "IKXChest_M.pim" "IKXChest_M_parentConstraint1.cpim";
connectAttr "IKXChest_M.rp" "IKXChest_M_parentConstraint1.crp";
connectAttr "IKXChest_M.rpt" "IKXChest_M_parentConstraint1.crt";
connectAttr "IKXChest_M.jo" "IKXChest_M_parentConstraint1.cjo";
connectAttr "IKfake2Spine_M.t" "IKXChest_M_parentConstraint1.tg[0].tt";
connectAttr "IKfake2Spine_M.rp" "IKXChest_M_parentConstraint1.tg[0].trp";
connectAttr "IKfake2Spine_M.rpt" "IKXChest_M_parentConstraint1.tg[0].trt";
connectAttr "IKfake2Spine_M.r" "IKXChest_M_parentConstraint1.tg[0].tr";
connectAttr "IKfake2Spine_M.ro" "IKXChest_M_parentConstraint1.tg[0].tro";
connectAttr "IKfake2Spine_M.s" "IKXChest_M_parentConstraint1.tg[0].ts";
connectAttr "IKfake2Spine_M.pm" "IKXChest_M_parentConstraint1.tg[0].tpm";
connectAttr "IKfake2Spine_M.jo" "IKXChest_M_parentConstraint1.tg[0].tjo";
connectAttr "IKXChest_M_parentConstraint1.w0" "IKXChest_M_parentConstraint1.tg[0].tw"
		;
connectAttr "IKSpine4AlignTo_M.t" "IKXChest_M_parentConstraint1.tg[1].tt";
connectAttr "IKSpine4AlignTo_M.rp" "IKXChest_M_parentConstraint1.tg[1].trp";
connectAttr "IKSpine4AlignTo_M.rpt" "IKXChest_M_parentConstraint1.tg[1].trt";
connectAttr "IKSpine4AlignTo_M.r" "IKXChest_M_parentConstraint1.tg[1].tr";
connectAttr "IKSpine4AlignTo_M.ro" "IKXChest_M_parentConstraint1.tg[1].tro";
connectAttr "IKSpine4AlignTo_M.s" "IKXChest_M_parentConstraint1.tg[1].ts";
connectAttr "IKSpine4AlignTo_M.pm" "IKXChest_M_parentConstraint1.tg[1].tpm";
connectAttr "IKXChest_M_parentConstraint1.w1" "IKXChest_M_parentConstraint1.tg[1].tw"
		;
connectAttr "stretchySpineReverse_M.oy" "IKXChest_M_parentConstraint1.w0";
connectAttr "stretchySpineUnitConversion_M.o" "IKXChest_M_parentConstraint1.w1";
connectAttr "IKXSpineB_M.pim" "IKXSpineB_M_aimConstraint1.cpim";
connectAttr "IKXSpineB_M.t" "IKXSpineB_M_aimConstraint1.ct";
connectAttr "IKXSpineB_M.rp" "IKXSpineB_M_aimConstraint1.crp";
connectAttr "IKXSpineB_M.rpt" "IKXSpineB_M_aimConstraint1.crt";
connectAttr "IKXSpineB_M.ro" "IKXSpineB_M_aimConstraint1.cro";
connectAttr "IKXSpineB_M.jo" "IKXSpineB_M_aimConstraint1.cjo";
connectAttr "IKfake2Spine_M.t" "IKXSpineB_M_aimConstraint1.tg[0].tt";
connectAttr "IKfake2Spine_M.rp" "IKXSpineB_M_aimConstraint1.tg[0].trp";
connectAttr "IKfake2Spine_M.rpt" "IKXSpineB_M_aimConstraint1.tg[0].trt";
connectAttr "IKfake2Spine_M.pm" "IKXSpineB_M_aimConstraint1.tg[0].tpm";
connectAttr "IKXSpineB_M_aimConstraint1.w0" "IKXSpineB_M_aimConstraint1.tg[0].tw"
		;
connectAttr "IKSpine4_M.t" "IKXSpineB_M_aimConstraint1.tg[1].tt";
connectAttr "IKSpine4_M.rp" "IKXSpineB_M_aimConstraint1.tg[1].trp";
connectAttr "IKSpine4_M.rpt" "IKXSpineB_M_aimConstraint1.tg[1].trt";
connectAttr "IKSpine4_M.pm" "IKXSpineB_M_aimConstraint1.tg[1].tpm";
connectAttr "IKXSpineB_M_aimConstraint1.w1" "IKXSpineB_M_aimConstraint1.tg[1].tw"
		;
connectAttr "stretchySpineReverse_M.oy" "IKXSpineB_M_aimConstraint1.w0";
connectAttr "stretchySpineUnitConversion_M.o" "IKXSpineB_M_aimConstraint1.w1";
connectAttr "IKFake1UpLocSpine_M.wm" "IKXSpineB_M_aimConstraint1.wum";
connectAttr "stretchySpineBlendTwo3_M.o" "IKfake1Spine_M.ty";
connectAttr "jointLayer.di" "IKfake1Spine_M.do";
connectAttr "IKfake1Spine_M.s" "IKfake2Spine_M.is";
connectAttr "stretchySpineBlendTwo2_M.o" "IKfake2Spine_M.ty";
connectAttr "jointLayer.di" "IKfake2Spine_M.do";
connectAttr "IKfake2Spine_M.tx" "effector4.tx";
connectAttr "IKfake2Spine_M.ty" "effector4.ty";
connectAttr "IKfake2Spine_M.tz" "effector4.tz";
connectAttr "IKParentConstraintPelvis_M.ro" "IKParentConstraintPelvis_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintPelvis_M.pim" "IKParentConstraintPelvis_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintPelvis_M.rp" "IKParentConstraintPelvis_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintPelvis_M.rpt" "IKParentConstraintPelvis_M_parentConstraint1.crt"
		;
connectAttr "IKSpine0AlignTo_M.t" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tt"
		;
connectAttr "IKSpine0AlignTo_M.rp" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].trp"
		;
connectAttr "IKSpine0AlignTo_M.rpt" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].trt"
		;
connectAttr "IKSpine0AlignTo_M.r" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tr"
		;
connectAttr "IKSpine0AlignTo_M.ro" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tro"
		;
connectAttr "IKSpine0AlignTo_M.s" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].ts"
		;
connectAttr "IKSpine0AlignTo_M.pm" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.w0" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintKnee_L_parentConstraint1.ctx" "IKParentConstraintKnee_L.tx"
		;
connectAttr "IKParentConstraintKnee_L_parentConstraint1.cty" "IKParentConstraintKnee_L.ty"
		;
connectAttr "IKParentConstraintKnee_L_parentConstraint1.ctz" "IKParentConstraintKnee_L.tz"
		;
connectAttr "IKParentConstraintKnee_L_parentConstraint1.crx" "IKParentConstraintKnee_L.rx"
		;
connectAttr "IKParentConstraintKnee_L_parentConstraint1.cry" "IKParentConstraintKnee_L.ry"
		;
connectAttr "IKParentConstraintKnee_L_parentConstraint1.crz" "IKParentConstraintKnee_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintKnee_L.v";
connectAttr "jointLayer.di" "IKXKnee_L.do";
connectAttr "IKXKnee_L.s" "IKXBackKnee_L.is";
connectAttr "jointLayer.di" "IKXBackKnee_L.do";
connectAttr "IKXBackKnee_L.s" "IKXAnkle_L.is";
connectAttr "IKXAnkle_L_orientConstraint1.crx" "IKXAnkle_L.rx";
connectAttr "IKXAnkle_L_orientConstraint1.cry" "IKXAnkle_L.ry";
connectAttr "IKXAnkle_L_orientConstraint1.crz" "IKXAnkle_L.rz";
connectAttr "jointLayer.di" "IKXAnkle_L.do";
connectAttr "IKXAnkle_L.s" "IKXFoot_End_L.is";
connectAttr "jointLayer.di" "IKXFoot_End_L.do";
connectAttr "IKXAnkle_L.ro" "IKXAnkle_L_orientConstraint1.cro";
connectAttr "IKXAnkle_L.pim" "IKXAnkle_L_orientConstraint1.cpim";
connectAttr "IKXAnkle_L.jo" "IKXAnkle_L_orientConstraint1.cjo";
connectAttr "IKLeg_L.r" "IKXAnkle_L_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_L.ro" "IKXAnkle_L_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_L.pm" "IKXAnkle_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXAnkle_L_orientConstraint1.w0" "IKXAnkle_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXFoot_End_L.tx" "effector7.tx";
connectAttr "IKXFoot_End_L.ty" "effector7.ty";
connectAttr "IKXFoot_End_L.tz" "effector7.tz";
connectAttr "IKXAnkle_L.tx" "effector6.tx";
connectAttr "IKXAnkle_L.ty" "effector6.ty";
connectAttr "IKXAnkle_L.tz" "effector6.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_L.v";
connectAttr "PoleAnnotateTargetLeg_LShape.wm" "PoleAnnotationLeg_LShape.dom" -na
		;
connectAttr "IKParentConstraintKnee_L.ro" "IKParentConstraintKnee_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintKnee_L.pim" "IKParentConstraintKnee_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintKnee_L.rp" "IKParentConstraintKnee_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintKnee_L.rpt" "IKParentConstraintKnee_L_parentConstraint1.crt"
		;
connectAttr "Hip_L.t" "IKParentConstraintKnee_L_parentConstraint1.tg[0].tt";
connectAttr "Hip_L.rp" "IKParentConstraintKnee_L_parentConstraint1.tg[0].trp";
connectAttr "Hip_L.rpt" "IKParentConstraintKnee_L_parentConstraint1.tg[0].trt";
connectAttr "Hip_L.r" "IKParentConstraintKnee_L_parentConstraint1.tg[0].tr";
connectAttr "Hip_L.ro" "IKParentConstraintKnee_L_parentConstraint1.tg[0].tro";
connectAttr "Hip_L.s" "IKParentConstraintKnee_L_parentConstraint1.tg[0].ts";
connectAttr "Hip_L.pm" "IKParentConstraintKnee_L_parentConstraint1.tg[0].tpm";
connectAttr "Hip_L.jo" "IKParentConstraintKnee_L_parentConstraint1.tg[0].tjo";
connectAttr "IKParentConstraintKnee_L_parentConstraint1.w0" "IKParentConstraintKnee_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.ctx" "IKParentConstraintArm_L.tx"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.cty" "IKParentConstraintArm_L.ty"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.ctz" "IKParentConstraintArm_L.tz"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.crx" "IKParentConstraintArm_L.rx"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.cry" "IKParentConstraintArm_L.ry"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.crz" "IKParentConstraintArm_L.rz"
		;
connectAttr "FKIKBlendArmCondition_L.ocr" "IKParentConstraintArm_L.v";
connectAttr "IKXShoulder1_L.msg" "IKXArmHandle_L.hsj";
connectAttr "effector1.hp" "IKXArmHandle_L.hee";
connectAttr "ikRPsolver.msg" "IKXArmHandle_L.hsv";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.ctx" "IKXArmHandle_L.pvx";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.cty" "IKXArmHandle_L.pvy";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.ctz" "IKXArmHandle_L.pvz";
connectAttr "IKXArmHandle_L.pim" "IKXArmHandle_L_poleVectorConstraint1.cpim";
connectAttr "IKXShoulder1_L.pm" "IKXArmHandle_L_poleVectorConstraint1.ps";
connectAttr "IKXShoulder1_L.t" "IKXArmHandle_L_poleVectorConstraint1.crp";
connectAttr "PoleArm_L.t" "IKXArmHandle_L_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleArm_L.rp" "IKXArmHandle_L_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleArm_L.rpt" "IKXArmHandle_L_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleArm_L.pm" "IKXArmHandle_L_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.w0" "IKXArmHandle_L_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintArm_L.ro" "IKParentConstraintArm_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintArm_L.pim" "IKParentConstraintArm_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintArm_L.rp" "IKParentConstraintArm_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintArm_L.rpt" "IKParentConstraintArm_L_parentConstraint1.crt"
		;
connectAttr "IKParentConstraintArm_LStatic.t" "IKParentConstraintArm_L_parentConstraint1.tg[0].tt"
		;
connectAttr "IKParentConstraintArm_LStatic.rp" "IKParentConstraintArm_L_parentConstraint1.tg[0].trp"
		;
connectAttr "IKParentConstraintArm_LStatic.rpt" "IKParentConstraintArm_L_parentConstraint1.tg[0].trt"
		;
connectAttr "IKParentConstraintArm_LStatic.r" "IKParentConstraintArm_L_parentConstraint1.tg[0].tr"
		;
connectAttr "IKParentConstraintArm_LStatic.ro" "IKParentConstraintArm_L_parentConstraint1.tg[0].tro"
		;
connectAttr "IKParentConstraintArm_LStatic.s" "IKParentConstraintArm_L_parentConstraint1.tg[0].ts"
		;
connectAttr "IKParentConstraintArm_LStatic.pm" "IKParentConstraintArm_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.w0" "IKParentConstraintArm_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Chest_M.t" "IKParentConstraintArm_L_parentConstraint1.tg[1].tt";
connectAttr "Chest_M.rp" "IKParentConstraintArm_L_parentConstraint1.tg[1].trp";
connectAttr "Chest_M.rpt" "IKParentConstraintArm_L_parentConstraint1.tg[1].trt";
connectAttr "Chest_M.r" "IKParentConstraintArm_L_parentConstraint1.tg[1].tr";
connectAttr "Chest_M.ro" "IKParentConstraintArm_L_parentConstraint1.tg[1].tro";
connectAttr "Chest_M.s" "IKParentConstraintArm_L_parentConstraint1.tg[1].ts";
connectAttr "Chest_M.pm" "IKParentConstraintArm_L_parentConstraint1.tg[1].tpm";
connectAttr "Chest_M.jo" "IKParentConstraintArm_L_parentConstraint1.tg[1].tjo";
connectAttr "IKParentConstraintArm_L_parentConstraint1.w1" "IKParentConstraintArm_L_parentConstraint1.tg[1].tw"
		;
connectAttr "IKArm_LSetRangeFollow.oy" "IKParentConstraintArm_L_parentConstraint1.w0"
		;
connectAttr "IKArm_LSetRangeFollow.ox" "IKParentConstraintArm_L_parentConstraint1.w1"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.ctx" "PoleParentConstraintArm_L.tx"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.cty" "PoleParentConstraintArm_L.ty"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.ctz" "PoleParentConstraintArm_L.tz"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.crx" "PoleParentConstraintArm_L.rx"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.cry" "PoleParentConstraintArm_L.ry"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.crz" "PoleParentConstraintArm_L.rz"
		;
connectAttr "FKIKBlendArmCondition_L.ocr" "PoleParentConstraintArm_L.v";
connectAttr "PoleParentConstraintArm_L.ro" "PoleParentConstraintArm_L_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintArm_L.pim" "PoleParentConstraintArm_L_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintArm_L.rp" "PoleParentConstraintArm_L_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintArm_L.rpt" "PoleParentConstraintArm_L_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintArm_LStatic.t" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintArm_LStatic.rp" "PoleParentConstraintArm_L_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintArm_LStatic.rpt" "PoleParentConstraintArm_L_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintArm_LStatic.r" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintArm_LStatic.ro" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintArm_LStatic.s" "PoleParentConstraintArm_L_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintArm_LStatic.pm" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.w0" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKArm_L.t" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tt";
connectAttr "IKArm_L.rp" "PoleParentConstraintArm_L_parentConstraint1.tg[1].trp"
		;
connectAttr "IKArm_L.rpt" "PoleParentConstraintArm_L_parentConstraint1.tg[1].trt"
		;
connectAttr "IKArm_L.r" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tr";
connectAttr "IKArm_L.ro" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tro"
		;
connectAttr "IKArm_L.s" "PoleParentConstraintArm_L_parentConstraint1.tg[1].ts";
connectAttr "IKArm_L.pm" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.w1" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleArm_LSetRangeFollow.oy" "PoleParentConstraintArm_L_parentConstraint1.w0"
		;
connectAttr "PoleArm_LSetRangeFollow.ox" "PoleParentConstraintArm_L_parentConstraint1.w1"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintLeg_R.v";
connectAttr "unitConversion2.o" "IKRollLegHeel_R.rx";
connectAttr "unitConversion3.o" "IKRollLegBall_R.rx";
connectAttr "IKXKnee_R.msg" "IKXLegHandle_R.hsj";
connectAttr "effector2.hp" "IKXLegHandle_R.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_R.hsv";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctx" "IKXLegHandle_R.pvx";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.cty" "IKXLegHandle_R.pvy";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctz" "IKXLegHandle_R.pvz";
connectAttr "IKXLegHandle_R.pim" "IKXLegHandle_R_poleVectorConstraint1.cpim";
connectAttr "IKXKnee_R.pm" "IKXLegHandle_R_poleVectorConstraint1.ps";
connectAttr "IKXKnee_R.t" "IKXLegHandle_R_poleVectorConstraint1.crp";
connectAttr "PoleLeg_R.t" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_R.rp" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_R.rpt" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_R.pm" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.w0" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXAnkle_R.msg" "IKXLegHandleBall_R.hsj";
connectAttr "effector3.hp" "IKXLegHandleBall_R.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_R.hsv";
connectAttr "PoleAimLeg_R_aimConstraint1.crx" "PoleAimLeg_R.rx";
connectAttr "PoleAimLeg_R_aimConstraint1.cry" "PoleAimLeg_R.ry";
connectAttr "PoleAimLeg_R_aimConstraint1.crz" "PoleAimLeg_R.rz";
connectAttr "PoleAimLeg_R_pointConstraint1.ctx" "PoleAimLeg_R.tx";
connectAttr "PoleAimLeg_R_pointConstraint1.cty" "PoleAimLeg_R.ty";
connectAttr "PoleAimLeg_R_pointConstraint1.ctz" "PoleAimLeg_R.tz";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_aimConstraint1.cpim";
connectAttr "PoleAimLeg_R.t" "PoleAimLeg_R_aimConstraint1.ct";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_aimConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.crt";
connectAttr "PoleAimLeg_R.ro" "PoleAimLeg_R_aimConstraint1.cro";
connectAttr "IKLeg_R.t" "PoleAimLeg_R_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PoleAimLeg_R_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PoleAimLeg_R_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_aimConstraint1.w0" "PoleAimLeg_R_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_R.wm" "PoleAimLeg_R_aimConstraint1.wum";
connectAttr "unitConversion1.o" "PoleAimLeg_R_aimConstraint1.ox";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_pointConstraint1.cpim";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_pointConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_pointConstraint1.crt";
connectAttr "HipTwist_R.t" "PoleAimLeg_R_pointConstraint1.tg[0].tt";
connectAttr "HipTwist_R.rp" "PoleAimLeg_R_pointConstraint1.tg[0].trp";
connectAttr "HipTwist_R.rpt" "PoleAimLeg_R_pointConstraint1.tg[0].trt";
connectAttr "HipTwist_R.pm" "PoleAimLeg_R_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_pointConstraint1.w0" "PoleAimLeg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctx" "PoleParentConstraintLeg_R.tx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cty" "PoleParentConstraintLeg_R.ty"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctz" "PoleParentConstraintLeg_R.tz"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crx" "PoleParentConstraintLeg_R.rx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cry" "PoleParentConstraintLeg_R.ry"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crz" "PoleParentConstraintLeg_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "PoleParentConstraintLeg_R.v";
connectAttr "PoleParentConstraintLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_R.pim" "PoleParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_RStatic.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_RStatic.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_RStatic.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w0" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_R.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_R.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_R.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_R.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w1" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_RSetRangeFollow.oy" "PoleParentConstraintLeg_R_parentConstraint1.w0"
		;
connectAttr "PoleLeg_RSetRangeFollow.ox" "PoleParentConstraintLeg_R_parentConstraint1.w1"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintSpine0_M.v";
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.ctx" "IKParentConstraintSpine0_M.tx"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.cty" "IKParentConstraintSpine0_M.ty"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.ctz" "IKParentConstraintSpine0_M.tz"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.crx" "IKParentConstraintSpine0_M.rx"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.cry" "IKParentConstraintSpine0_M.ry"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.crz" "IKParentConstraintSpine0_M.rz"
		;
connectAttr "IKStiffSpine0_M.oy" "IKXSpineLocator1_M.ty";
connectAttr "IKParentConstraintSpine0_M.ro" "IKParentConstraintSpine0_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintSpine0_M.pim" "IKParentConstraintSpine0_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintSpine0_M.rp" "IKParentConstraintSpine0_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintSpine0_M.rpt" "IKParentConstraintSpine0_M_parentConstraint1.crt"
		;
connectAttr "Center_M.t" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Center_M.rp" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Center_M.rpt" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Center_M.r" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Center_M.s" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Center_M.pm" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.w0" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintSpine2_M.v";
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.ctx" "IKParentConstraintSpine2_M.tx"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.cty" "IKParentConstraintSpine2_M.ty"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.ctz" "IKParentConstraintSpine2_M.tz"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.crx" "IKParentConstraintSpine2_M.rx"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.cry" "IKParentConstraintSpine2_M.ry"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.crz" "IKParentConstraintSpine2_M.rz"
		;
connectAttr "IKParentConstraintSpine2_M.ro" "IKParentConstraintSpine2_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintSpine2_M.pim" "IKParentConstraintSpine2_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintSpine2_M.rp" "IKParentConstraintSpine2_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintSpine2_M.rpt" "IKParentConstraintSpine2_M_parentConstraint1.crt"
		;
connectAttr "Center_M.t" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Center_M.rp" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Center_M.rpt" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Center_M.r" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Center_M.s" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Center_M.pm" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.w0" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintSpine4_M.v";
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.ctx" "IKParentConstraintSpine4_M.tx"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.cty" "IKParentConstraintSpine4_M.ty"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.ctz" "IKParentConstraintSpine4_M.tz"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.crx" "IKParentConstraintSpine4_M.rx"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.cry" "IKParentConstraintSpine4_M.ry"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.crz" "IKParentConstraintSpine4_M.rz"
		;
connectAttr "IKXPelvis_M.msg" "IKXSpineHandle_M.hsj";
connectAttr "effector4.hp" "IKXSpineHandle_M.hee";
connectAttr "ikSplineSolver.msg" "IKXSpineHandle_M.hsv";
connectAttr "IKXSpineCurve_MShape.ws" "IKXSpineHandle_M.ic";
connectAttr "IKTwistSpineUnitConversion_M.o" "IKXSpineHandle_M.twi";
connectAttr "IKStiffSpine4_M.oy" "IKXSpineLocator3_M.ty";
connectAttr "IKParentConstraintSpine4_M.ro" "IKParentConstraintSpine4_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintSpine4_M.pim" "IKParentConstraintSpine4_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintSpine4_M.rp" "IKParentConstraintSpine4_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintSpine4_M.rpt" "IKParentConstraintSpine4_M_parentConstraint1.crt"
		;
connectAttr "Center_M.t" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Center_M.rp" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Center_M.rpt" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Center_M.r" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Center_M.s" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Center_M.pm" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.w0" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintLeg_L.v";
connectAttr "unitConversion5.o" "IKRollLegHeel_L.rx";
connectAttr "unitConversion6.o" "IKRollLegBall_L.rx";
connectAttr "IKXKnee_L.msg" "IKXLegHandle_L.hsj";
connectAttr "effector6.hp" "IKXLegHandle_L.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_L.hsv";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctx" "IKXLegHandle_L.pvx";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.cty" "IKXLegHandle_L.pvy";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctz" "IKXLegHandle_L.pvz";
connectAttr "IKXLegHandle_L.pim" "IKXLegHandle_L_poleVectorConstraint1.cpim";
connectAttr "IKXKnee_L.pm" "IKXLegHandle_L_poleVectorConstraint1.ps";
connectAttr "IKXKnee_L.t" "IKXLegHandle_L_poleVectorConstraint1.crp";
connectAttr "PoleLeg_L.t" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_L.rp" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_L.rpt" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_L.pm" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.w0" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXAnkle_L.msg" "IKXLegHandleBall_L.hsj";
connectAttr "effector7.hp" "IKXLegHandleBall_L.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_L.hsv";
connectAttr "PoleAimLeg_L_aimConstraint1.crx" "PoleAimLeg_L.rx";
connectAttr "PoleAimLeg_L_aimConstraint1.cry" "PoleAimLeg_L.ry";
connectAttr "PoleAimLeg_L_aimConstraint1.crz" "PoleAimLeg_L.rz";
connectAttr "PoleAimLeg_L_pointConstraint1.ctx" "PoleAimLeg_L.tx";
connectAttr "PoleAimLeg_L_pointConstraint1.cty" "PoleAimLeg_L.ty";
connectAttr "PoleAimLeg_L_pointConstraint1.ctz" "PoleAimLeg_L.tz";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_aimConstraint1.cpim";
connectAttr "PoleAimLeg_L.t" "PoleAimLeg_L_aimConstraint1.ct";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_aimConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.crt";
connectAttr "PoleAimLeg_L.ro" "PoleAimLeg_L_aimConstraint1.cro";
connectAttr "IKLeg_L.t" "PoleAimLeg_L_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_L.rp" "PoleAimLeg_L_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_L.pm" "PoleAimLeg_L_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_aimConstraint1.w0" "PoleAimLeg_L_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.wm" "PoleAimLeg_L_aimConstraint1.wum";
connectAttr "unitConversion4.o" "PoleAimLeg_L_aimConstraint1.ox";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_pointConstraint1.cpim";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_pointConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_pointConstraint1.crt";
connectAttr "HipTwist_L.t" "PoleAimLeg_L_pointConstraint1.tg[0].tt";
connectAttr "HipTwist_L.rp" "PoleAimLeg_L_pointConstraint1.tg[0].trp";
connectAttr "HipTwist_L.rpt" "PoleAimLeg_L_pointConstraint1.tg[0].trt";
connectAttr "HipTwist_L.pm" "PoleAimLeg_L_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_pointConstraint1.w0" "PoleAimLeg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctx" "PoleParentConstraintLeg_L.tx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cty" "PoleParentConstraintLeg_L.ty"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctz" "PoleParentConstraintLeg_L.tz"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crx" "PoleParentConstraintLeg_L.rx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cry" "PoleParentConstraintLeg_L.ry"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crz" "PoleParentConstraintLeg_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "PoleParentConstraintLeg_L.v";
connectAttr "PoleParentConstraintLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_L.pim" "PoleParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_LStatic.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_LStatic.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_LStatic.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w0" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_L.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_L.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_L.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_L.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w1" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_LSetRangeFollow.oy" "PoleParentConstraintLeg_L_parentConstraint1.w0"
		;
connectAttr "PoleLeg_LSetRangeFollow.ox" "PoleParentConstraintLeg_L_parentConstraint1.w1"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKXSpineCurve_M.v";
connectAttr "IKXSpineLocator0_MShape.wp" "IKXSpineCurve_MShape.cp[0]";
connectAttr "IKXSpineLocator1_MShape.wp" "IKXSpineCurve_MShape.cp[1]";
connectAttr "IKXSpineLocator2_MShape.wp" "IKXSpineCurve_MShape.cp[2]";
connectAttr "IKXSpineLocator3_MShape.wp" "IKXSpineCurve_MShape.cp[3]";
connectAttr "IKXSpineLocator4_MShape.wp" "IKXSpineCurve_MShape.cp[4]";
connectAttr "Main.fkIkVis" "FKIKSystem.v";
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.ctx" "FKIKParentConstraintArm_L.tx"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.cty" "FKIKParentConstraintArm_L.ty"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.ctz" "FKIKParentConstraintArm_L.tz"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.crx" "FKIKParentConstraintArm_L.rx"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.cry" "FKIKParentConstraintArm_L.ry"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.crz" "FKIKParentConstraintArm_L.rz"
		;
connectAttr "FKIKParentConstraintArm_L.ro" "FKIKParentConstraintArm_L_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintArm_L.pim" "FKIKParentConstraintArm_L_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintArm_L.rp" "FKIKParentConstraintArm_L_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintArm_L.rpt" "FKIKParentConstraintArm_L_parentConstraint1.crt"
		;
connectAttr "Chest_M.t" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tt";
connectAttr "Chest_M.rp" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Chest_M.rpt" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Chest_M.r" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tr";
connectAttr "Chest_M.ro" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Chest_M.s" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].ts";
connectAttr "Chest_M.pm" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Chest_M.jo" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.w0" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctx" "FKIKParentConstraintLeg_R.tx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cty" "FKIKParentConstraintLeg_R.ty"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctz" "FKIKParentConstraintLeg_R.tz"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crx" "FKIKParentConstraintLeg_R.rx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cry" "FKIKParentConstraintLeg_R.ry"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crz" "FKIKParentConstraintLeg_R.rz"
		;
connectAttr "FKIKParentConstraintLeg_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_R.pim" "FKIKParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "Hip_R.t" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tt";
connectAttr "Hip_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trp";
connectAttr "Hip_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trt";
connectAttr "Hip_R.r" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tr";
connectAttr "Hip_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tro";
connectAttr "Hip_R.s" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].ts";
connectAttr "Hip_R.pm" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tpm";
connectAttr "Hip_R.jo" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tjo";
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.w0" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.ctx" "FKIKParentConstraintSpine_M.tx"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.cty" "FKIKParentConstraintSpine_M.ty"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.ctz" "FKIKParentConstraintSpine_M.tz"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.crx" "FKIKParentConstraintSpine_M.rx"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.cry" "FKIKParentConstraintSpine_M.ry"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.crz" "FKIKParentConstraintSpine_M.rz"
		;
connectAttr "FKIKParentConstraintSpine_M.ro" "FKIKParentConstraintSpine_M_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintSpine_M.pim" "FKIKParentConstraintSpine_M_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintSpine_M.rp" "FKIKParentConstraintSpine_M_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintSpine_M.rpt" "FKIKParentConstraintSpine_M_parentConstraint1.crt"
		;
connectAttr "FKPelvis_M.t" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tt"
		;
connectAttr "FKPelvis_M.rp" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].trp"
		;
connectAttr "FKPelvis_M.rpt" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].trt"
		;
connectAttr "FKPelvis_M.r" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tr"
		;
connectAttr "FKPelvis_M.ro" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tro"
		;
connectAttr "FKPelvis_M.s" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].ts"
		;
connectAttr "FKPelvis_M.pm" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.w0" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctx" "FKIKParentConstraintLeg_L.tx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cty" "FKIKParentConstraintLeg_L.ty"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctz" "FKIKParentConstraintLeg_L.tz"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crx" "FKIKParentConstraintLeg_L.rx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cry" "FKIKParentConstraintLeg_L.ry"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crz" "FKIKParentConstraintLeg_L.rz"
		;
connectAttr "FKIKParentConstraintLeg_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_L.pim" "FKIKParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "Hip_L.t" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tt";
connectAttr "Hip_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trp";
connectAttr "Hip_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trt";
connectAttr "Hip_L.r" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tr";
connectAttr "Hip_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tro";
connectAttr "Hip_L.s" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].ts";
connectAttr "Hip_L.pm" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tpm";
connectAttr "Hip_L.jo" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tjo";
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.w0" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.cry" "PelvisCenterBtwLegs_M.ry"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctx" "PelvisCenterBtwLegs_M.tx"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctz" "PelvisCenterBtwLegs_M.tz"
		;
connectAttr "PelvisCenterBtwLegs_M.ro" "PelvisCenterBtwLegs_M_orientConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_orientConstraint1.cpim"
		;
connectAttr "IKLeg_R.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w0" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tr";
connectAttr "IKLeg_L.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tro";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w1" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tw"
		;
connectAttr "CenterBtwFeet_M.ox" "PelvisCenterBtwLegs_M_pointConstraint1.nds";
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_pointConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegs_M.rp" "PelvisCenterBtwLegs_M_pointConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegs_M.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.crt"
		;
connectAttr "IKLeg_R.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w0" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tt";
connectAttr "IKLeg_L.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trp";
connectAttr "IKLeg_L.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trt";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w1" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tw"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.ctx" "TwistFollowPelvis_M.tx"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.cty" "TwistFollowPelvis_M.ty"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.ctz" "TwistFollowPelvis_M.tz"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.crx" "TwistFollowPelvis_M.rx"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.cry" "TwistFollowPelvis_M.ry"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.crz" "TwistFollowPelvis_M.rz"
		;
connectAttr "TwistFollowPelvis_M.ro" "TwistFollowPelvis_M_parentConstraint1.cro"
		;
connectAttr "TwistFollowPelvis_M.pim" "TwistFollowPelvis_M_parentConstraint1.cpim"
		;
connectAttr "TwistFollowPelvis_M.rp" "TwistFollowPelvis_M_parentConstraint1.crp"
		;
connectAttr "TwistFollowPelvis_M.rpt" "TwistFollowPelvis_M_parentConstraint1.crt"
		;
connectAttr "IKSpine0_M.t" "TwistFollowPelvis_M_parentConstraint1.tg[0].tt";
connectAttr "IKSpine0_M.rp" "TwistFollowPelvis_M_parentConstraint1.tg[0].trp";
connectAttr "IKSpine0_M.rpt" "TwistFollowPelvis_M_parentConstraint1.tg[0].trt";
connectAttr "IKSpine0_M.r" "TwistFollowPelvis_M_parentConstraint1.tg[0].tr";
connectAttr "IKSpine0_M.ro" "TwistFollowPelvis_M_parentConstraint1.tg[0].tro";
connectAttr "IKSpine0_M.s" "TwistFollowPelvis_M_parentConstraint1.tg[0].ts";
connectAttr "IKSpine0_M.pm" "TwistFollowPelvis_M_parentConstraint1.tg[0].tpm";
connectAttr "TwistFollowPelvis_M_parentConstraint1.w0" "TwistFollowPelvis_M_parentConstraint1.tg[0].tw"
		;
connectAttr "jointLayer.di" "UnTwistPelvis_M.do";
connectAttr "UnTwistPelvis_M.s" "UnTwistEndPelvis_M.is";
connectAttr "UnTwistEndPelvis_M_orientConstraint1.crx" "UnTwistEndPelvis_M.rx";
connectAttr "UnTwistEndPelvis_M_orientConstraint1.cry" "UnTwistEndPelvis_M.ry";
connectAttr "UnTwistEndPelvis_M_orientConstraint1.crz" "UnTwistEndPelvis_M.rz";
connectAttr "jointLayer.di" "UnTwistEndPelvis_M.do";
connectAttr "UnTwistEndPelvis_M.ro" "UnTwistEndPelvis_M_orientConstraint1.cro";
connectAttr "UnTwistEndPelvis_M.pim" "UnTwistEndPelvis_M_orientConstraint1.cpim"
		;
connectAttr "UnTwistEndPelvis_M.jo" "UnTwistEndPelvis_M_orientConstraint1.cjo";
connectAttr "IKSpine4AlignUnTwistTo_M.r" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tr"
		;
connectAttr "IKSpine4AlignUnTwistTo_M.ro" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tro"
		;
connectAttr "IKSpine4AlignUnTwistTo_M.pm" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tpm"
		;
connectAttr "UnTwistEndPelvis_M_orientConstraint1.w0" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "UnTwistEndPelvis_M.tx" "effector5.tx";
connectAttr "UnTwistEndPelvis_M.ty" "effector5.ty";
connectAttr "UnTwistEndPelvis_M.tz" "effector5.tz";
connectAttr "UnTwistPelvis_M.msg" "UnTwistIKPelvis_M.hsj";
connectAttr "effector5.hp" "UnTwistIKPelvis_M.hee";
connectAttr "ikSCsolver.msg" "UnTwistIKPelvis_M.hsv";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.ctx" "UnTwistIKPelvis_M.tx";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.cty" "UnTwistIKPelvis_M.ty";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.ctz" "UnTwistIKPelvis_M.tz";
connectAttr "UnTwistIKPelvis_M.pim" "UnTwistIKPelvis_M_pointConstraint1.cpim";
connectAttr "UnTwistIKPelvis_M.rp" "UnTwistIKPelvis_M_pointConstraint1.crp";
connectAttr "UnTwistIKPelvis_M.rpt" "UnTwistIKPelvis_M_pointConstraint1.crt";
connectAttr "IKSpine4_M.t" "UnTwistIKPelvis_M_pointConstraint1.tg[0].tt";
connectAttr "IKSpine4_M.rp" "UnTwistIKPelvis_M_pointConstraint1.tg[0].trp";
connectAttr "IKSpine4_M.rpt" "UnTwistIKPelvis_M_pointConstraint1.tg[0].trt";
connectAttr "IKSpine4_M.pm" "UnTwistIKPelvis_M_pointConstraint1.tg[0].tpm";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.w0" "UnTwistIKPelvis_M_pointConstraint1.tg[0].tw"
		;
connectAttr "ScaleBlendPelvis_M.op" "Pelvis_M.s";
connectAttr "Pelvis_M_pointConstraint1.ctx" "Pelvis_M.tx";
connectAttr "Pelvis_M_pointConstraint1.cty" "Pelvis_M.ty";
connectAttr "Pelvis_M_pointConstraint1.ctz" "Pelvis_M.tz";
connectAttr "Pelvis_M_orientConstraint1.crx" "Pelvis_M.rx";
connectAttr "Pelvis_M_orientConstraint1.cry" "Pelvis_M.ry";
connectAttr "Pelvis_M_orientConstraint1.crz" "Pelvis_M.rz";
connectAttr "jointLayer.di" "Pelvis_M.do";
connectAttr "ScaleBlendSpineA_M.op" "SpineA_M.s";
connectAttr "Pelvis_M.s" "SpineA_M.is";
connectAttr "SpineA_M_pointConstraint1.ctx" "SpineA_M.tx";
connectAttr "SpineA_M_pointConstraint1.cty" "SpineA_M.ty";
connectAttr "SpineA_M_pointConstraint1.ctz" "SpineA_M.tz";
connectAttr "SpineA_M_orientConstraint1.crx" "SpineA_M.rx";
connectAttr "SpineA_M_orientConstraint1.cry" "SpineA_M.ry";
connectAttr "SpineA_M_orientConstraint1.crz" "SpineA_M.rz";
connectAttr "jointLayer.di" "SpineA_M.do";
connectAttr "ScaleBlendSpineB_M.op" "SpineB_M.s";
connectAttr "SpineB_M_pointConstraint1.ctx" "SpineB_M.tx";
connectAttr "SpineB_M_pointConstraint1.cty" "SpineB_M.ty";
connectAttr "SpineB_M_pointConstraint1.ctz" "SpineB_M.tz";
connectAttr "SpineB_M_orientConstraint1.crx" "SpineB_M.rx";
connectAttr "SpineB_M_orientConstraint1.cry" "SpineB_M.ry";
connectAttr "SpineB_M_orientConstraint1.crz" "SpineB_M.rz";
connectAttr "jointLayer.di" "SpineB_M.do";
connectAttr "ScaleBlendChest_M.op" "Chest_M.s";
connectAttr "Chest_M_pointConstraint1.ctx" "Chest_M.tx";
connectAttr "Chest_M_pointConstraint1.cty" "Chest_M.ty";
connectAttr "Chest_M_pointConstraint1.ctz" "Chest_M.tz";
connectAttr "Chest_M_orientConstraint1.crx" "Chest_M.rx";
connectAttr "Chest_M_orientConstraint1.cry" "Chest_M.ry";
connectAttr "Chest_M_orientConstraint1.crz" "Chest_M.rz";
connectAttr "jointLayer.di" "Chest_M.do";
connectAttr "FKHead_M.s" "Head_M.s";
connectAttr "Chest_M.s" "Head_M.is";
connectAttr "Head_M_parentConstraint1.ctx" "Head_M.tx";
connectAttr "Head_M_parentConstraint1.cty" "Head_M.ty";
connectAttr "Head_M_parentConstraint1.ctz" "Head_M.tz";
connectAttr "Head_M_parentConstraint1.crx" "Head_M.rx";
connectAttr "Head_M_parentConstraint1.cry" "Head_M.ry";
connectAttr "Head_M_parentConstraint1.crz" "Head_M.rz";
connectAttr "jointLayer.di" "Head_M.do";
connectAttr "Head_M.s" "Head_End_M.is";
connectAttr "jointLayer.di" "Head_End_M.do";
connectAttr "Head_M.ro" "Head_M_parentConstraint1.cro";
connectAttr "Head_M.pim" "Head_M_parentConstraint1.cpim";
connectAttr "Head_M.rp" "Head_M_parentConstraint1.crp";
connectAttr "Head_M.rpt" "Head_M_parentConstraint1.crt";
connectAttr "Head_M.jo" "Head_M_parentConstraint1.cjo";
connectAttr "FKXHead_M.t" "Head_M_parentConstraint1.tg[0].tt";
connectAttr "FKXHead_M.rp" "Head_M_parentConstraint1.tg[0].trp";
connectAttr "FKXHead_M.rpt" "Head_M_parentConstraint1.tg[0].trt";
connectAttr "FKXHead_M.r" "Head_M_parentConstraint1.tg[0].tr";
connectAttr "FKXHead_M.ro" "Head_M_parentConstraint1.tg[0].tro";
connectAttr "FKXHead_M.s" "Head_M_parentConstraint1.tg[0].ts";
connectAttr "FKXHead_M.pm" "Head_M_parentConstraint1.tg[0].tpm";
connectAttr "FKXHead_M.jo" "Head_M_parentConstraint1.tg[0].tjo";
connectAttr "Head_M_parentConstraint1.w0" "Head_M_parentConstraint1.tg[0].tw";
connectAttr "FKGun1_R.s" "Gun1_R.s";
connectAttr "Chest_M.s" "Gun1_R.is";
connectAttr "Gun1_R_parentConstraint1.ctx" "Gun1_R.tx";
connectAttr "Gun1_R_parentConstraint1.cty" "Gun1_R.ty";
connectAttr "Gun1_R_parentConstraint1.ctz" "Gun1_R.tz";
connectAttr "Gun1_R_parentConstraint1.crx" "Gun1_R.rx";
connectAttr "Gun1_R_parentConstraint1.cry" "Gun1_R.ry";
connectAttr "Gun1_R_parentConstraint1.crz" "Gun1_R.rz";
connectAttr "jointLayer.di" "Gun1_R.do";
connectAttr "FKGun2_R.s" "Gun2_R.s";
connectAttr "Gun2_R_parentConstraint1.ctx" "Gun2_R.tx";
connectAttr "Gun2_R_parentConstraint1.cty" "Gun2_R.ty";
connectAttr "Gun2_R_parentConstraint1.ctz" "Gun2_R.tz";
connectAttr "Gun2_R_parentConstraint1.crx" "Gun2_R.rx";
connectAttr "Gun2_R_parentConstraint1.cry" "Gun2_R.ry";
connectAttr "Gun2_R_parentConstraint1.crz" "Gun2_R.rz";
connectAttr "jointLayer.di" "Gun2_R.do";
connectAttr "Gun2_R.s" "Gun3_End_R.is";
connectAttr "jointLayer.di" "Gun3_End_R.do";
connectAttr "Gun2_R.ro" "Gun2_R_parentConstraint1.cro";
connectAttr "Gun2_R.pim" "Gun2_R_parentConstraint1.cpim";
connectAttr "Gun2_R.rp" "Gun2_R_parentConstraint1.crp";
connectAttr "Gun2_R.rpt" "Gun2_R_parentConstraint1.crt";
connectAttr "Gun2_R.jo" "Gun2_R_parentConstraint1.cjo";
connectAttr "FKXGun2_R.t" "Gun2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXGun2_R.rp" "Gun2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXGun2_R.rpt" "Gun2_R_parentConstraint1.tg[0].trt";
connectAttr "FKXGun2_R.r" "Gun2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXGun2_R.ro" "Gun2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXGun2_R.s" "Gun2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXGun2_R.pm" "Gun2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXGun2_R.jo" "Gun2_R_parentConstraint1.tg[0].tjo";
connectAttr "Gun2_R_parentConstraint1.w0" "Gun2_R_parentConstraint1.tg[0].tw";
connectAttr "Gun1_R.ro" "Gun1_R_parentConstraint1.cro";
connectAttr "Gun1_R.pim" "Gun1_R_parentConstraint1.cpim";
connectAttr "Gun1_R.rp" "Gun1_R_parentConstraint1.crp";
connectAttr "Gun1_R.rpt" "Gun1_R_parentConstraint1.crt";
connectAttr "Gun1_R.jo" "Gun1_R_parentConstraint1.cjo";
connectAttr "FKXGun1_R.t" "Gun1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXGun1_R.rp" "Gun1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXGun1_R.rpt" "Gun1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXGun1_R.r" "Gun1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXGun1_R.ro" "Gun1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXGun1_R.s" "Gun1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXGun1_R.pm" "Gun1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXGun1_R.jo" "Gun1_R_parentConstraint1.tg[0].tjo";
connectAttr "Gun1_R_parentConstraint1.w0" "Gun1_R_parentConstraint1.tg[0].tw";
connectAttr "ScaleBlendShoulder1_L.op" "Shoulder1_L.s";
connectAttr "Chest_M.s" "Shoulder1_L.is";
connectAttr "Shoulder1_L_parentConstraint1.ctx" "Shoulder1_L.tx";
connectAttr "Shoulder1_L_parentConstraint1.cty" "Shoulder1_L.ty";
connectAttr "Shoulder1_L_parentConstraint1.ctz" "Shoulder1_L.tz";
connectAttr "Shoulder1_L_parentConstraint1.crx" "Shoulder1_L.rx";
connectAttr "Shoulder1_L_parentConstraint1.cry" "Shoulder1_L.ry";
connectAttr "Shoulder1_L_parentConstraint1.crz" "Shoulder1_L.rz";
connectAttr "jointLayer.di" "Shoulder1_L.do";
connectAttr "ScaleBlendElbow1_L.op" "Elbow1_L.s";
connectAttr "Shoulder1_L.s" "Elbow1_L.is";
connectAttr "Elbow1_L_parentConstraint1.ctx" "Elbow1_L.tx";
connectAttr "Elbow1_L_parentConstraint1.cty" "Elbow1_L.ty";
connectAttr "Elbow1_L_parentConstraint1.ctz" "Elbow1_L.tz";
connectAttr "Elbow1_L_parentConstraint1.crx" "Elbow1_L.rx";
connectAttr "Elbow1_L_parentConstraint1.cry" "Elbow1_L.ry";
connectAttr "Elbow1_L_parentConstraint1.crz" "Elbow1_L.rz";
connectAttr "jointLayer.di" "Elbow1_L.do";
connectAttr "ScaleBlendWrist2_L.op" "Wrist2_L.s";
connectAttr "Elbow1_L.s" "Wrist2_L.is";
connectAttr "Wrist2_L_parentConstraint1.ctx" "Wrist2_L.tx";
connectAttr "Wrist2_L_parentConstraint1.cty" "Wrist2_L.ty";
connectAttr "Wrist2_L_parentConstraint1.ctz" "Wrist2_L.tz";
connectAttr "Wrist2_L_parentConstraint1.crx" "Wrist2_L.rx";
connectAttr "Wrist2_L_parentConstraint1.cry" "Wrist2_L.ry";
connectAttr "Wrist2_L_parentConstraint1.crz" "Wrist2_L.rz";
connectAttr "jointLayer.di" "Wrist2_L.do";
connectAttr "ScaleBlendWrist1_L.op" "Wrist1_L.s";
connectAttr "Wrist1_L_parentConstraint1.ctx" "Wrist1_L.tx";
connectAttr "Wrist1_L_parentConstraint1.cty" "Wrist1_L.ty";
connectAttr "Wrist1_L_parentConstraint1.ctz" "Wrist1_L.tz";
connectAttr "Wrist1_L_parentConstraint1.crx" "Wrist1_L.rx";
connectAttr "Wrist1_L_parentConstraint1.cry" "Wrist1_L.ry";
connectAttr "Wrist1_L_parentConstraint1.crz" "Wrist1_L.rz";
connectAttr "jointLayer.di" "Wrist1_L.do";
connectAttr "FKMiddleFinger1_L.s" "MiddleFinger1_L.s";
connectAttr "Wrist1_L.s" "MiddleFinger1_L.is";
connectAttr "MiddleFinger1_L_parentConstraint1.ctx" "MiddleFinger1_L.tx";
connectAttr "MiddleFinger1_L_parentConstraint1.cty" "MiddleFinger1_L.ty";
connectAttr "MiddleFinger1_L_parentConstraint1.ctz" "MiddleFinger1_L.tz";
connectAttr "MiddleFinger1_L_parentConstraint1.crx" "MiddleFinger1_L.rx";
connectAttr "MiddleFinger1_L_parentConstraint1.cry" "MiddleFinger1_L.ry";
connectAttr "MiddleFinger1_L_parentConstraint1.crz" "MiddleFinger1_L.rz";
connectAttr "jointLayer.di" "MiddleFinger1_L.do";
connectAttr "FKMiddleFinger2_L.s" "MiddleFinger2_L.s";
connectAttr "MiddleFinger1_L.s" "MiddleFinger2_L.is";
connectAttr "MiddleFinger2_L_parentConstraint1.ctx" "MiddleFinger2_L.tx";
connectAttr "MiddleFinger2_L_parentConstraint1.cty" "MiddleFinger2_L.ty";
connectAttr "MiddleFinger2_L_parentConstraint1.ctz" "MiddleFinger2_L.tz";
connectAttr "MiddleFinger2_L_parentConstraint1.crx" "MiddleFinger2_L.rx";
connectAttr "MiddleFinger2_L_parentConstraint1.cry" "MiddleFinger2_L.ry";
connectAttr "MiddleFinger2_L_parentConstraint1.crz" "MiddleFinger2_L.rz";
connectAttr "jointLayer.di" "MiddleFinger2_L.do";
connectAttr "MiddleFinger2_L.s" "MiddleFinger3_End_L.is";
connectAttr "jointLayer.di" "MiddleFinger3_End_L.do";
connectAttr "MiddleFinger2_L.ro" "MiddleFinger2_L_parentConstraint1.cro";
connectAttr "MiddleFinger2_L.pim" "MiddleFinger2_L_parentConstraint1.cpim";
connectAttr "MiddleFinger2_L.rp" "MiddleFinger2_L_parentConstraint1.crp";
connectAttr "MiddleFinger2_L.rpt" "MiddleFinger2_L_parentConstraint1.crt";
connectAttr "MiddleFinger2_L.jo" "MiddleFinger2_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger2_L.t" "MiddleFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger2_L.rp" "MiddleFinger2_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger2_L.rpt" "MiddleFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger2_L.r" "MiddleFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger2_L.ro" "MiddleFinger2_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger2_L.s" "MiddleFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger2_L.pm" "MiddleFinger2_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger2_L.jo" "MiddleFinger2_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger2_L_parentConstraint1.w0" "MiddleFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "MiddleFinger1_L.ro" "MiddleFinger1_L_parentConstraint1.cro";
connectAttr "MiddleFinger1_L.pim" "MiddleFinger1_L_parentConstraint1.cpim";
connectAttr "MiddleFinger1_L.rp" "MiddleFinger1_L_parentConstraint1.crp";
connectAttr "MiddleFinger1_L.rpt" "MiddleFinger1_L_parentConstraint1.crt";
connectAttr "MiddleFinger1_L.jo" "MiddleFinger1_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger1_L.t" "MiddleFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger1_L.rp" "MiddleFinger1_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger1_L.rpt" "MiddleFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger1_L.r" "MiddleFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger1_L.ro" "MiddleFinger1_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger1_L.s" "MiddleFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger1_L.pm" "MiddleFinger1_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger1_L.jo" "MiddleFinger1_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger1_L_parentConstraint1.w0" "MiddleFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIndexFinger1_L.s" "IndexFinger1_L.s";
connectAttr "Wrist1_L.s" "IndexFinger1_L.is";
connectAttr "IndexFinger1_L_parentConstraint1.ctx" "IndexFinger1_L.tx";
connectAttr "IndexFinger1_L_parentConstraint1.cty" "IndexFinger1_L.ty";
connectAttr "IndexFinger1_L_parentConstraint1.ctz" "IndexFinger1_L.tz";
connectAttr "IndexFinger1_L_parentConstraint1.crx" "IndexFinger1_L.rx";
connectAttr "IndexFinger1_L_parentConstraint1.cry" "IndexFinger1_L.ry";
connectAttr "IndexFinger1_L_parentConstraint1.crz" "IndexFinger1_L.rz";
connectAttr "jointLayer.di" "IndexFinger1_L.do";
connectAttr "FKIndexFinger2_L.s" "IndexFinger2_L.s";
connectAttr "IndexFinger1_L.s" "IndexFinger2_L.is";
connectAttr "IndexFinger2_L_parentConstraint1.ctx" "IndexFinger2_L.tx";
connectAttr "IndexFinger2_L_parentConstraint1.cty" "IndexFinger2_L.ty";
connectAttr "IndexFinger2_L_parentConstraint1.ctz" "IndexFinger2_L.tz";
connectAttr "IndexFinger2_L_parentConstraint1.crx" "IndexFinger2_L.rx";
connectAttr "IndexFinger2_L_parentConstraint1.cry" "IndexFinger2_L.ry";
connectAttr "IndexFinger2_L_parentConstraint1.crz" "IndexFinger2_L.rz";
connectAttr "jointLayer.di" "IndexFinger2_L.do";
connectAttr "IndexFinger2_L.s" "IndexFinger3_End_L.is";
connectAttr "jointLayer.di" "IndexFinger3_End_L.do";
connectAttr "IndexFinger2_L.ro" "IndexFinger2_L_parentConstraint1.cro";
connectAttr "IndexFinger2_L.pim" "IndexFinger2_L_parentConstraint1.cpim";
connectAttr "IndexFinger2_L.rp" "IndexFinger2_L_parentConstraint1.crp";
connectAttr "IndexFinger2_L.rpt" "IndexFinger2_L_parentConstraint1.crt";
connectAttr "IndexFinger2_L.jo" "IndexFinger2_L_parentConstraint1.cjo";
connectAttr "FKXIndexFinger2_L.t" "IndexFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger2_L.rp" "IndexFinger2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger2_L.rpt" "IndexFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger2_L.r" "IndexFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger2_L.ro" "IndexFinger2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger2_L.s" "IndexFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger2_L.pm" "IndexFinger2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger2_L.jo" "IndexFinger2_L_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger2_L_parentConstraint1.w0" "IndexFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IndexFinger1_L.ro" "IndexFinger1_L_parentConstraint1.cro";
connectAttr "IndexFinger1_L.pim" "IndexFinger1_L_parentConstraint1.cpim";
connectAttr "IndexFinger1_L.rp" "IndexFinger1_L_parentConstraint1.crp";
connectAttr "IndexFinger1_L.rpt" "IndexFinger1_L_parentConstraint1.crt";
connectAttr "IndexFinger1_L.jo" "IndexFinger1_L_parentConstraint1.cjo";
connectAttr "FKXIndexFinger1_L.t" "IndexFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger1_L.rp" "IndexFinger1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger1_L.rpt" "IndexFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger1_L.r" "IndexFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger1_L.ro" "IndexFinger1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger1_L.s" "IndexFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger1_L.pm" "IndexFinger1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger1_L.jo" "IndexFinger1_L_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger1_L_parentConstraint1.w0" "IndexFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Wrist1_L.ro" "Wrist1_L_parentConstraint1.cro";
connectAttr "Wrist1_L.pim" "Wrist1_L_parentConstraint1.cpim";
connectAttr "Wrist1_L.rp" "Wrist1_L_parentConstraint1.crp";
connectAttr "Wrist1_L.rpt" "Wrist1_L_parentConstraint1.crt";
connectAttr "Wrist1_L.jo" "Wrist1_L_parentConstraint1.cjo";
connectAttr "FKXWrist1_L.t" "Wrist1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWrist1_L.rp" "Wrist1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWrist1_L.rpt" "Wrist1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWrist1_L.r" "Wrist1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWrist1_L.ro" "Wrist1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWrist1_L.s" "Wrist1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWrist1_L.pm" "Wrist1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWrist1_L.jo" "Wrist1_L_parentConstraint1.tg[0].tjo";
connectAttr "Wrist1_L_parentConstraint1.w0" "Wrist1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXWrist1_L.t" "Wrist1_L_parentConstraint1.tg[1].tt";
connectAttr "IKXWrist1_L.rp" "Wrist1_L_parentConstraint1.tg[1].trp";
connectAttr "IKXWrist1_L.rpt" "Wrist1_L_parentConstraint1.tg[1].trt";
connectAttr "IKXWrist1_L.r" "Wrist1_L_parentConstraint1.tg[1].tr";
connectAttr "IKXWrist1_L.ro" "Wrist1_L_parentConstraint1.tg[1].tro";
connectAttr "IKXWrist1_L.s" "Wrist1_L_parentConstraint1.tg[1].ts";
connectAttr "IKXWrist1_L.pm" "Wrist1_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXWrist1_L.jo" "Wrist1_L_parentConstraint1.tg[1].tjo";
connectAttr "Wrist1_L_parentConstraint1.w1" "Wrist1_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArmReverse_L.ox" "Wrist1_L_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_L.o" "Wrist1_L_parentConstraint1.w1";
connectAttr "Wrist2_L.ro" "Wrist2_L_parentConstraint1.cro";
connectAttr "Wrist2_L.pim" "Wrist2_L_parentConstraint1.cpim";
connectAttr "Wrist2_L.rp" "Wrist2_L_parentConstraint1.crp";
connectAttr "Wrist2_L.rpt" "Wrist2_L_parentConstraint1.crt";
connectAttr "Wrist2_L.jo" "Wrist2_L_parentConstraint1.cjo";
connectAttr "FKXWrist2_L.t" "Wrist2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWrist2_L.rp" "Wrist2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWrist2_L.rpt" "Wrist2_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWrist2_L.r" "Wrist2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWrist2_L.ro" "Wrist2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWrist2_L.s" "Wrist2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWrist2_L.pm" "Wrist2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWrist2_L.jo" "Wrist2_L_parentConstraint1.tg[0].tjo";
connectAttr "Wrist2_L_parentConstraint1.w0" "Wrist2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXWrist2_L.t" "Wrist2_L_parentConstraint1.tg[1].tt";
connectAttr "IKXWrist2_L.rp" "Wrist2_L_parentConstraint1.tg[1].trp";
connectAttr "IKXWrist2_L.rpt" "Wrist2_L_parentConstraint1.tg[1].trt";
connectAttr "IKXWrist2_L.r" "Wrist2_L_parentConstraint1.tg[1].tr";
connectAttr "IKXWrist2_L.ro" "Wrist2_L_parentConstraint1.tg[1].tro";
connectAttr "IKXWrist2_L.s" "Wrist2_L_parentConstraint1.tg[1].ts";
connectAttr "IKXWrist2_L.pm" "Wrist2_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXWrist2_L.jo" "Wrist2_L_parentConstraint1.tg[1].tjo";
connectAttr "Wrist2_L_parentConstraint1.w1" "Wrist2_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArmReverse_L.ox" "Wrist2_L_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_L.o" "Wrist2_L_parentConstraint1.w1";
connectAttr "Elbow1_L.ro" "Elbow1_L_parentConstraint1.cro";
connectAttr "Elbow1_L.pim" "Elbow1_L_parentConstraint1.cpim";
connectAttr "Elbow1_L.rp" "Elbow1_L_parentConstraint1.crp";
connectAttr "Elbow1_L.rpt" "Elbow1_L_parentConstraint1.crt";
connectAttr "Elbow1_L.jo" "Elbow1_L_parentConstraint1.cjo";
connectAttr "FKXElbow1_L.t" "Elbow1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow1_L.rp" "Elbow1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow1_L.rpt" "Elbow1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow1_L.r" "Elbow1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow1_L.ro" "Elbow1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow1_L.s" "Elbow1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow1_L.pm" "Elbow1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow1_L.jo" "Elbow1_L_parentConstraint1.tg[0].tjo";
connectAttr "Elbow1_L_parentConstraint1.w0" "Elbow1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXElbow1_L.t" "Elbow1_L_parentConstraint1.tg[1].tt";
connectAttr "IKXElbow1_L.rp" "Elbow1_L_parentConstraint1.tg[1].trp";
connectAttr "IKXElbow1_L.rpt" "Elbow1_L_parentConstraint1.tg[1].trt";
connectAttr "IKXElbow1_L.r" "Elbow1_L_parentConstraint1.tg[1].tr";
connectAttr "IKXElbow1_L.ro" "Elbow1_L_parentConstraint1.tg[1].tro";
connectAttr "IKXElbow1_L.s" "Elbow1_L_parentConstraint1.tg[1].ts";
connectAttr "IKXElbow1_L.pm" "Elbow1_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXElbow1_L.jo" "Elbow1_L_parentConstraint1.tg[1].tjo";
connectAttr "Elbow1_L_parentConstraint1.w1" "Elbow1_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArmReverse_L.ox" "Elbow1_L_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_L.o" "Elbow1_L_parentConstraint1.w1";
connectAttr "Shoulder1_L.ro" "Shoulder1_L_parentConstraint1.cro";
connectAttr "Shoulder1_L.pim" "Shoulder1_L_parentConstraint1.cpim";
connectAttr "Shoulder1_L.rp" "Shoulder1_L_parentConstraint1.crp";
connectAttr "Shoulder1_L.rpt" "Shoulder1_L_parentConstraint1.crt";
connectAttr "Shoulder1_L.jo" "Shoulder1_L_parentConstraint1.cjo";
connectAttr "FKXShoulder1_L.t" "Shoulder1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder1_L.rp" "Shoulder1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder1_L.rpt" "Shoulder1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder1_L.r" "Shoulder1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder1_L.ro" "Shoulder1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder1_L.s" "Shoulder1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder1_L.pm" "Shoulder1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder1_L.jo" "Shoulder1_L_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder1_L_parentConstraint1.w0" "Shoulder1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXShoulder1_L.t" "Shoulder1_L_parentConstraint1.tg[1].tt";
connectAttr "IKXShoulder1_L.rp" "Shoulder1_L_parentConstraint1.tg[1].trp";
connectAttr "IKXShoulder1_L.rpt" "Shoulder1_L_parentConstraint1.tg[1].trt";
connectAttr "IKXShoulder1_L.r" "Shoulder1_L_parentConstraint1.tg[1].tr";
connectAttr "IKXShoulder1_L.ro" "Shoulder1_L_parentConstraint1.tg[1].tro";
connectAttr "IKXShoulder1_L.s" "Shoulder1_L_parentConstraint1.tg[1].ts";
connectAttr "IKXShoulder1_L.pm" "Shoulder1_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXShoulder1_L.jo" "Shoulder1_L_parentConstraint1.tg[1].tjo";
connectAttr "Shoulder1_L_parentConstraint1.w1" "Shoulder1_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArmReverse_L.ox" "Shoulder1_L_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_L.o" "Shoulder1_L_parentConstraint1.w1";
connectAttr "FKCapeMid1_L.s" "CapeMid1_L.s";
connectAttr "Chest_M.s" "CapeMid1_L.is";
connectAttr "CapeMid1_L_parentConstraint1.ctx" "CapeMid1_L.tx";
connectAttr "CapeMid1_L_parentConstraint1.cty" "CapeMid1_L.ty";
connectAttr "CapeMid1_L_parentConstraint1.ctz" "CapeMid1_L.tz";
connectAttr "CapeMid1_L_parentConstraint1.crx" "CapeMid1_L.rx";
connectAttr "CapeMid1_L_parentConstraint1.cry" "CapeMid1_L.ry";
connectAttr "CapeMid1_L_parentConstraint1.crz" "CapeMid1_L.rz";
connectAttr "jointLayer.di" "CapeMid1_L.do";
connectAttr "FKCapeMid2_L.s" "CapeMid2_L.s";
connectAttr "CapeMid1_L.s" "CapeMid2_L.is";
connectAttr "CapeMid2_L_parentConstraint1.ctx" "CapeMid2_L.tx";
connectAttr "CapeMid2_L_parentConstraint1.cty" "CapeMid2_L.ty";
connectAttr "CapeMid2_L_parentConstraint1.ctz" "CapeMid2_L.tz";
connectAttr "CapeMid2_L_parentConstraint1.crx" "CapeMid2_L.rx";
connectAttr "CapeMid2_L_parentConstraint1.cry" "CapeMid2_L.ry";
connectAttr "CapeMid2_L_parentConstraint1.crz" "CapeMid2_L.rz";
connectAttr "jointLayer.di" "CapeMid2_L.do";
connectAttr "FKCapeMid3_L.s" "CapeMid3_L.s";
connectAttr "CapeMid2_L.s" "CapeMid3_L.is";
connectAttr "CapeMid3_L_parentConstraint1.ctx" "CapeMid3_L.tx";
connectAttr "CapeMid3_L_parentConstraint1.cty" "CapeMid3_L.ty";
connectAttr "CapeMid3_L_parentConstraint1.ctz" "CapeMid3_L.tz";
connectAttr "CapeMid3_L_parentConstraint1.crx" "CapeMid3_L.rx";
connectAttr "CapeMid3_L_parentConstraint1.cry" "CapeMid3_L.ry";
connectAttr "CapeMid3_L_parentConstraint1.crz" "CapeMid3_L.rz";
connectAttr "jointLayer.di" "CapeMid3_L.do";
connectAttr "FKCapeMid4_L.s" "CapeMid4_L.s";
connectAttr "CapeMid3_L.s" "CapeMid4_L.is";
connectAttr "CapeMid4_L_parentConstraint1.ctx" "CapeMid4_L.tx";
connectAttr "CapeMid4_L_parentConstraint1.cty" "CapeMid4_L.ty";
connectAttr "CapeMid4_L_parentConstraint1.ctz" "CapeMid4_L.tz";
connectAttr "CapeMid4_L_parentConstraint1.crx" "CapeMid4_L.rx";
connectAttr "CapeMid4_L_parentConstraint1.cry" "CapeMid4_L.ry";
connectAttr "CapeMid4_L_parentConstraint1.crz" "CapeMid4_L.rz";
connectAttr "jointLayer.di" "CapeMid4_L.do";
connectAttr "FKCapeMid5_L.s" "CapeMid5_L.s";
connectAttr "CapeMid4_L.s" "CapeMid5_L.is";
connectAttr "CapeMid5_L_parentConstraint1.ctx" "CapeMid5_L.tx";
connectAttr "CapeMid5_L_parentConstraint1.cty" "CapeMid5_L.ty";
connectAttr "CapeMid5_L_parentConstraint1.ctz" "CapeMid5_L.tz";
connectAttr "CapeMid5_L_parentConstraint1.crx" "CapeMid5_L.rx";
connectAttr "CapeMid5_L_parentConstraint1.cry" "CapeMid5_L.ry";
connectAttr "CapeMid5_L_parentConstraint1.crz" "CapeMid5_L.rz";
connectAttr "jointLayer.di" "CapeMid5_L.do";
connectAttr "CapeMid5_L.s" "CapeMid6_End_L.is";
connectAttr "jointLayer.di" "CapeMid6_End_L.do";
connectAttr "CapeMid5_L.ro" "CapeMid5_L_parentConstraint1.cro";
connectAttr "CapeMid5_L.pim" "CapeMid5_L_parentConstraint1.cpim";
connectAttr "CapeMid5_L.rp" "CapeMid5_L_parentConstraint1.crp";
connectAttr "CapeMid5_L.rpt" "CapeMid5_L_parentConstraint1.crt";
connectAttr "CapeMid5_L.jo" "CapeMid5_L_parentConstraint1.cjo";
connectAttr "FKXCapeMid5_L.t" "CapeMid5_L_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeMid5_L.rp" "CapeMid5_L_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeMid5_L.rpt" "CapeMid5_L_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeMid5_L.r" "CapeMid5_L_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeMid5_L.ro" "CapeMid5_L_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeMid5_L.s" "CapeMid5_L_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeMid5_L.pm" "CapeMid5_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeMid5_L.jo" "CapeMid5_L_parentConstraint1.tg[0].tjo";
connectAttr "CapeMid5_L_parentConstraint1.w0" "CapeMid5_L_parentConstraint1.tg[0].tw"
		;
connectAttr "CapeMid4_L.ro" "CapeMid4_L_parentConstraint1.cro";
connectAttr "CapeMid4_L.pim" "CapeMid4_L_parentConstraint1.cpim";
connectAttr "CapeMid4_L.rp" "CapeMid4_L_parentConstraint1.crp";
connectAttr "CapeMid4_L.rpt" "CapeMid4_L_parentConstraint1.crt";
connectAttr "CapeMid4_L.jo" "CapeMid4_L_parentConstraint1.cjo";
connectAttr "FKXCapeMid4_L.t" "CapeMid4_L_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeMid4_L.rp" "CapeMid4_L_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeMid4_L.rpt" "CapeMid4_L_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeMid4_L.r" "CapeMid4_L_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeMid4_L.ro" "CapeMid4_L_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeMid4_L.s" "CapeMid4_L_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeMid4_L.pm" "CapeMid4_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeMid4_L.jo" "CapeMid4_L_parentConstraint1.tg[0].tjo";
connectAttr "CapeMid4_L_parentConstraint1.w0" "CapeMid4_L_parentConstraint1.tg[0].tw"
		;
connectAttr "CapeMid3_L.ro" "CapeMid3_L_parentConstraint1.cro";
connectAttr "CapeMid3_L.pim" "CapeMid3_L_parentConstraint1.cpim";
connectAttr "CapeMid3_L.rp" "CapeMid3_L_parentConstraint1.crp";
connectAttr "CapeMid3_L.rpt" "CapeMid3_L_parentConstraint1.crt";
connectAttr "CapeMid3_L.jo" "CapeMid3_L_parentConstraint1.cjo";
connectAttr "FKXCapeMid3_L.t" "CapeMid3_L_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeMid3_L.rp" "CapeMid3_L_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeMid3_L.rpt" "CapeMid3_L_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeMid3_L.r" "CapeMid3_L_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeMid3_L.ro" "CapeMid3_L_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeMid3_L.s" "CapeMid3_L_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeMid3_L.pm" "CapeMid3_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeMid3_L.jo" "CapeMid3_L_parentConstraint1.tg[0].tjo";
connectAttr "CapeMid3_L_parentConstraint1.w0" "CapeMid3_L_parentConstraint1.tg[0].tw"
		;
connectAttr "CapeMid2_L.ro" "CapeMid2_L_parentConstraint1.cro";
connectAttr "CapeMid2_L.pim" "CapeMid2_L_parentConstraint1.cpim";
connectAttr "CapeMid2_L.rp" "CapeMid2_L_parentConstraint1.crp";
connectAttr "CapeMid2_L.rpt" "CapeMid2_L_parentConstraint1.crt";
connectAttr "CapeMid2_L.jo" "CapeMid2_L_parentConstraint1.cjo";
connectAttr "FKXCapeMid2_L.t" "CapeMid2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeMid2_L.rp" "CapeMid2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeMid2_L.rpt" "CapeMid2_L_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeMid2_L.r" "CapeMid2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeMid2_L.ro" "CapeMid2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeMid2_L.s" "CapeMid2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeMid2_L.pm" "CapeMid2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeMid2_L.jo" "CapeMid2_L_parentConstraint1.tg[0].tjo";
connectAttr "CapeMid2_L_parentConstraint1.w0" "CapeMid2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "CapeMid1_L.ro" "CapeMid1_L_parentConstraint1.cro";
connectAttr "CapeMid1_L.pim" "CapeMid1_L_parentConstraint1.cpim";
connectAttr "CapeMid1_L.rp" "CapeMid1_L_parentConstraint1.crp";
connectAttr "CapeMid1_L.rpt" "CapeMid1_L_parentConstraint1.crt";
connectAttr "CapeMid1_L.jo" "CapeMid1_L_parentConstraint1.cjo";
connectAttr "FKXCapeMid1_L.t" "CapeMid1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeMid1_L.rp" "CapeMid1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeMid1_L.rpt" "CapeMid1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeMid1_L.r" "CapeMid1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeMid1_L.ro" "CapeMid1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeMid1_L.s" "CapeMid1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeMid1_L.pm" "CapeMid1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeMid1_L.jo" "CapeMid1_L_parentConstraint1.tg[0].tjo";
connectAttr "CapeMid1_L_parentConstraint1.w0" "CapeMid1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKCapeR1_R.s" "CapeR1_R.s";
connectAttr "Chest_M.s" "CapeR1_R.is";
connectAttr "CapeR1_R_parentConstraint1.ctx" "CapeR1_R.tx";
connectAttr "CapeR1_R_parentConstraint1.cty" "CapeR1_R.ty";
connectAttr "CapeR1_R_parentConstraint1.ctz" "CapeR1_R.tz";
connectAttr "CapeR1_R_parentConstraint1.crx" "CapeR1_R.rx";
connectAttr "CapeR1_R_parentConstraint1.cry" "CapeR1_R.ry";
connectAttr "CapeR1_R_parentConstraint1.crz" "CapeR1_R.rz";
connectAttr "jointLayer.di" "CapeR1_R.do";
connectAttr "FKCapeR2_R.s" "CapeR2_R.s";
connectAttr "CapeR1_R.s" "CapeR2_R.is";
connectAttr "CapeR2_R_parentConstraint1.ctx" "CapeR2_R.tx";
connectAttr "CapeR2_R_parentConstraint1.cty" "CapeR2_R.ty";
connectAttr "CapeR2_R_parentConstraint1.ctz" "CapeR2_R.tz";
connectAttr "CapeR2_R_parentConstraint1.crx" "CapeR2_R.rx";
connectAttr "CapeR2_R_parentConstraint1.cry" "CapeR2_R.ry";
connectAttr "CapeR2_R_parentConstraint1.crz" "CapeR2_R.rz";
connectAttr "jointLayer.di" "CapeR2_R.do";
connectAttr "FKCapeR3_R.s" "CapeR3_R.s";
connectAttr "CapeR2_R.s" "CapeR3_R.is";
connectAttr "CapeR3_R_parentConstraint1.ctx" "CapeR3_R.tx";
connectAttr "CapeR3_R_parentConstraint1.cty" "CapeR3_R.ty";
connectAttr "CapeR3_R_parentConstraint1.ctz" "CapeR3_R.tz";
connectAttr "CapeR3_R_parentConstraint1.crx" "CapeR3_R.rx";
connectAttr "CapeR3_R_parentConstraint1.cry" "CapeR3_R.ry";
connectAttr "CapeR3_R_parentConstraint1.crz" "CapeR3_R.rz";
connectAttr "jointLayer.di" "CapeR3_R.do";
connectAttr "CapeR3_R.s" "CapeR4_End_R.is";
connectAttr "jointLayer.di" "CapeR4_End_R.do";
connectAttr "CapeR3_R.ro" "CapeR3_R_parentConstraint1.cro";
connectAttr "CapeR3_R.pim" "CapeR3_R_parentConstraint1.cpim";
connectAttr "CapeR3_R.rp" "CapeR3_R_parentConstraint1.crp";
connectAttr "CapeR3_R.rpt" "CapeR3_R_parentConstraint1.crt";
connectAttr "CapeR3_R.jo" "CapeR3_R_parentConstraint1.cjo";
connectAttr "FKXCapeR3_R.t" "CapeR3_R_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeR3_R.rp" "CapeR3_R_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeR3_R.rpt" "CapeR3_R_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeR3_R.r" "CapeR3_R_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeR3_R.ro" "CapeR3_R_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeR3_R.s" "CapeR3_R_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeR3_R.pm" "CapeR3_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeR3_R.jo" "CapeR3_R_parentConstraint1.tg[0].tjo";
connectAttr "CapeR3_R_parentConstraint1.w0" "CapeR3_R_parentConstraint1.tg[0].tw"
		;
connectAttr "CapeR2_R.ro" "CapeR2_R_parentConstraint1.cro";
connectAttr "CapeR2_R.pim" "CapeR2_R_parentConstraint1.cpim";
connectAttr "CapeR2_R.rp" "CapeR2_R_parentConstraint1.crp";
connectAttr "CapeR2_R.rpt" "CapeR2_R_parentConstraint1.crt";
connectAttr "CapeR2_R.jo" "CapeR2_R_parentConstraint1.cjo";
connectAttr "FKXCapeR2_R.t" "CapeR2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeR2_R.rp" "CapeR2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeR2_R.rpt" "CapeR2_R_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeR2_R.r" "CapeR2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeR2_R.ro" "CapeR2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeR2_R.s" "CapeR2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeR2_R.pm" "CapeR2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeR2_R.jo" "CapeR2_R_parentConstraint1.tg[0].tjo";
connectAttr "CapeR2_R_parentConstraint1.w0" "CapeR2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "CapeR1_R.ro" "CapeR1_R_parentConstraint1.cro";
connectAttr "CapeR1_R.pim" "CapeR1_R_parentConstraint1.cpim";
connectAttr "CapeR1_R.rp" "CapeR1_R_parentConstraint1.crp";
connectAttr "CapeR1_R.rpt" "CapeR1_R_parentConstraint1.crt";
connectAttr "CapeR1_R.jo" "CapeR1_R_parentConstraint1.cjo";
connectAttr "FKXCapeR1_R.t" "CapeR1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeR1_R.rp" "CapeR1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeR1_R.rpt" "CapeR1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeR1_R.r" "CapeR1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeR1_R.ro" "CapeR1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeR1_R.s" "CapeR1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeR1_R.pm" "CapeR1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeR1_R.jo" "CapeR1_R_parentConstraint1.tg[0].tjo";
connectAttr "CapeR1_R_parentConstraint1.w0" "CapeR1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKCapeL1_L.s" "CapeL1_L.s";
connectAttr "Chest_M.s" "CapeL1_L.is";
connectAttr "CapeL1_L_parentConstraint1.ctx" "CapeL1_L.tx";
connectAttr "CapeL1_L_parentConstraint1.cty" "CapeL1_L.ty";
connectAttr "CapeL1_L_parentConstraint1.ctz" "CapeL1_L.tz";
connectAttr "CapeL1_L_parentConstraint1.crx" "CapeL1_L.rx";
connectAttr "CapeL1_L_parentConstraint1.cry" "CapeL1_L.ry";
connectAttr "CapeL1_L_parentConstraint1.crz" "CapeL1_L.rz";
connectAttr "jointLayer.di" "CapeL1_L.do";
connectAttr "FKCapeL2_L.s" "CapeL2_L.s";
connectAttr "CapeL1_L.s" "CapeL2_L.is";
connectAttr "CapeL2_L_parentConstraint1.ctx" "CapeL2_L.tx";
connectAttr "CapeL2_L_parentConstraint1.cty" "CapeL2_L.ty";
connectAttr "CapeL2_L_parentConstraint1.ctz" "CapeL2_L.tz";
connectAttr "CapeL2_L_parentConstraint1.crx" "CapeL2_L.rx";
connectAttr "CapeL2_L_parentConstraint1.cry" "CapeL2_L.ry";
connectAttr "CapeL2_L_parentConstraint1.crz" "CapeL2_L.rz";
connectAttr "jointLayer.di" "CapeL2_L.do";
connectAttr "FKCapeL3_L.s" "CapeL3_L.s";
connectAttr "CapeL2_L.s" "CapeL3_L.is";
connectAttr "CapeL3_L_parentConstraint1.ctx" "CapeL3_L.tx";
connectAttr "CapeL3_L_parentConstraint1.cty" "CapeL3_L.ty";
connectAttr "CapeL3_L_parentConstraint1.ctz" "CapeL3_L.tz";
connectAttr "CapeL3_L_parentConstraint1.crx" "CapeL3_L.rx";
connectAttr "CapeL3_L_parentConstraint1.cry" "CapeL3_L.ry";
connectAttr "CapeL3_L_parentConstraint1.crz" "CapeL3_L.rz";
connectAttr "jointLayer.di" "CapeL3_L.do";
connectAttr "CapeL3_L.s" "CapeL4_End_L.is";
connectAttr "jointLayer.di" "CapeL4_End_L.do";
connectAttr "CapeL3_L.ro" "CapeL3_L_parentConstraint1.cro";
connectAttr "CapeL3_L.pim" "CapeL3_L_parentConstraint1.cpim";
connectAttr "CapeL3_L.rp" "CapeL3_L_parentConstraint1.crp";
connectAttr "CapeL3_L.rpt" "CapeL3_L_parentConstraint1.crt";
connectAttr "CapeL3_L.jo" "CapeL3_L_parentConstraint1.cjo";
connectAttr "FKXCapeL3_L.t" "CapeL3_L_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeL3_L.rp" "CapeL3_L_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeL3_L.rpt" "CapeL3_L_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeL3_L.r" "CapeL3_L_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeL3_L.ro" "CapeL3_L_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeL3_L.s" "CapeL3_L_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeL3_L.pm" "CapeL3_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeL3_L.jo" "CapeL3_L_parentConstraint1.tg[0].tjo";
connectAttr "CapeL3_L_parentConstraint1.w0" "CapeL3_L_parentConstraint1.tg[0].tw"
		;
connectAttr "CapeL2_L.ro" "CapeL2_L_parentConstraint1.cro";
connectAttr "CapeL2_L.pim" "CapeL2_L_parentConstraint1.cpim";
connectAttr "CapeL2_L.rp" "CapeL2_L_parentConstraint1.crp";
connectAttr "CapeL2_L.rpt" "CapeL2_L_parentConstraint1.crt";
connectAttr "CapeL2_L.jo" "CapeL2_L_parentConstraint1.cjo";
connectAttr "FKXCapeL2_L.t" "CapeL2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeL2_L.rp" "CapeL2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeL2_L.rpt" "CapeL2_L_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeL2_L.r" "CapeL2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeL2_L.ro" "CapeL2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeL2_L.s" "CapeL2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeL2_L.pm" "CapeL2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeL2_L.jo" "CapeL2_L_parentConstraint1.tg[0].tjo";
connectAttr "CapeL2_L_parentConstraint1.w0" "CapeL2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "CapeL1_L.ro" "CapeL1_L_parentConstraint1.cro";
connectAttr "CapeL1_L.pim" "CapeL1_L_parentConstraint1.cpim";
connectAttr "CapeL1_L.rp" "CapeL1_L_parentConstraint1.crp";
connectAttr "CapeL1_L.rpt" "CapeL1_L_parentConstraint1.crt";
connectAttr "CapeL1_L.jo" "CapeL1_L_parentConstraint1.cjo";
connectAttr "FKXCapeL1_L.t" "CapeL1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXCapeL1_L.rp" "CapeL1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXCapeL1_L.rpt" "CapeL1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXCapeL1_L.r" "CapeL1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXCapeL1_L.ro" "CapeL1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXCapeL1_L.s" "CapeL1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXCapeL1_L.pm" "CapeL1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXCapeL1_L.jo" "CapeL1_L_parentConstraint1.tg[0].tjo";
connectAttr "CapeL1_L_parentConstraint1.w0" "CapeL1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Chest_M.pim" "Chest_M_pointConstraint1.cpim";
connectAttr "Chest_M.rp" "Chest_M_pointConstraint1.crp";
connectAttr "Chest_M.rpt" "Chest_M_pointConstraint1.crt";
connectAttr "FKXChest_M.t" "Chest_M_pointConstraint1.tg[0].tt";
connectAttr "FKXChest_M.rp" "Chest_M_pointConstraint1.tg[0].trp";
connectAttr "FKXChest_M.rpt" "Chest_M_pointConstraint1.tg[0].trt";
connectAttr "FKXChest_M.pm" "Chest_M_pointConstraint1.tg[0].tpm";
connectAttr "Chest_M_pointConstraint1.w0" "Chest_M_pointConstraint1.tg[0].tw";
connectAttr "IKXChest_M.t" "Chest_M_pointConstraint1.tg[1].tt";
connectAttr "IKXChest_M.rp" "Chest_M_pointConstraint1.tg[1].trp";
connectAttr "IKXChest_M.rpt" "Chest_M_pointConstraint1.tg[1].trt";
connectAttr "IKXChest_M.pm" "Chest_M_pointConstraint1.tg[1].tpm";
connectAttr "Chest_M_pointConstraint1.w1" "Chest_M_pointConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "Chest_M_pointConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Chest_M_pointConstraint1.w1";
connectAttr "Chest_M.ro" "Chest_M_orientConstraint1.cro";
connectAttr "Chest_M.pim" "Chest_M_orientConstraint1.cpim";
connectAttr "Chest_M.jo" "Chest_M_orientConstraint1.cjo";
connectAttr "FKXChest_M.r" "Chest_M_orientConstraint1.tg[0].tr";
connectAttr "FKXChest_M.ro" "Chest_M_orientConstraint1.tg[0].tro";
connectAttr "FKXChest_M.pm" "Chest_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXChest_M.jo" "Chest_M_orientConstraint1.tg[0].tjo";
connectAttr "Chest_M_orientConstraint1.w0" "Chest_M_orientConstraint1.tg[0].tw";
connectAttr "IKXChest_M.r" "Chest_M_orientConstraint1.tg[1].tr";
connectAttr "IKXChest_M.ro" "Chest_M_orientConstraint1.tg[1].tro";
connectAttr "IKXChest_M.pm" "Chest_M_orientConstraint1.tg[1].tpm";
connectAttr "IKXChest_M.jo" "Chest_M_orientConstraint1.tg[1].tjo";
connectAttr "Chest_M_orientConstraint1.w1" "Chest_M_orientConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "Chest_M_orientConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Chest_M_orientConstraint1.w1";
connectAttr "SpineB_M.pim" "SpineB_M_pointConstraint1.cpim";
connectAttr "SpineB_M.rp" "SpineB_M_pointConstraint1.crp";
connectAttr "SpineB_M.rpt" "SpineB_M_pointConstraint1.crt";
connectAttr "FKXSpineB_M.t" "SpineB_M_pointConstraint1.tg[0].tt";
connectAttr "FKXSpineB_M.rp" "SpineB_M_pointConstraint1.tg[0].trp";
connectAttr "FKXSpineB_M.rpt" "SpineB_M_pointConstraint1.tg[0].trt";
connectAttr "FKXSpineB_M.pm" "SpineB_M_pointConstraint1.tg[0].tpm";
connectAttr "SpineB_M_pointConstraint1.w0" "SpineB_M_pointConstraint1.tg[0].tw";
connectAttr "IKXSpineB_M.t" "SpineB_M_pointConstraint1.tg[1].tt";
connectAttr "IKXSpineB_M.rp" "SpineB_M_pointConstraint1.tg[1].trp";
connectAttr "IKXSpineB_M.rpt" "SpineB_M_pointConstraint1.tg[1].trt";
connectAttr "IKXSpineB_M.pm" "SpineB_M_pointConstraint1.tg[1].tpm";
connectAttr "SpineB_M_pointConstraint1.w1" "SpineB_M_pointConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "SpineB_M_pointConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "SpineB_M_pointConstraint1.w1";
connectAttr "SpineB_M.ro" "SpineB_M_orientConstraint1.cro";
connectAttr "SpineB_M.pim" "SpineB_M_orientConstraint1.cpim";
connectAttr "SpineB_M.jo" "SpineB_M_orientConstraint1.cjo";
connectAttr "FKXSpineB_M.r" "SpineB_M_orientConstraint1.tg[0].tr";
connectAttr "FKXSpineB_M.ro" "SpineB_M_orientConstraint1.tg[0].tro";
connectAttr "FKXSpineB_M.pm" "SpineB_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXSpineB_M.jo" "SpineB_M_orientConstraint1.tg[0].tjo";
connectAttr "SpineB_M_orientConstraint1.w0" "SpineB_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXSpineB_M.r" "SpineB_M_orientConstraint1.tg[1].tr";
connectAttr "IKXSpineB_M.ro" "SpineB_M_orientConstraint1.tg[1].tro";
connectAttr "IKXSpineB_M.pm" "SpineB_M_orientConstraint1.tg[1].tpm";
connectAttr "IKXSpineB_M.jo" "SpineB_M_orientConstraint1.tg[1].tjo";
connectAttr "SpineB_M_orientConstraint1.w1" "SpineB_M_orientConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendSpineReverse_M.ox" "SpineB_M_orientConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "SpineB_M_orientConstraint1.w1";
connectAttr "SpineA_M.pim" "SpineA_M_pointConstraint1.cpim";
connectAttr "SpineA_M.rp" "SpineA_M_pointConstraint1.crp";
connectAttr "SpineA_M.rpt" "SpineA_M_pointConstraint1.crt";
connectAttr "FKXSpineA_M.t" "SpineA_M_pointConstraint1.tg[0].tt";
connectAttr "FKXSpineA_M.rp" "SpineA_M_pointConstraint1.tg[0].trp";
connectAttr "FKXSpineA_M.rpt" "SpineA_M_pointConstraint1.tg[0].trt";
connectAttr "FKXSpineA_M.pm" "SpineA_M_pointConstraint1.tg[0].tpm";
connectAttr "SpineA_M_pointConstraint1.w0" "SpineA_M_pointConstraint1.tg[0].tw";
connectAttr "IKXSpineA_M.t" "SpineA_M_pointConstraint1.tg[1].tt";
connectAttr "IKXSpineA_M.rp" "SpineA_M_pointConstraint1.tg[1].trp";
connectAttr "IKXSpineA_M.rpt" "SpineA_M_pointConstraint1.tg[1].trt";
connectAttr "IKXSpineA_M.pm" "SpineA_M_pointConstraint1.tg[1].tpm";
connectAttr "SpineA_M_pointConstraint1.w1" "SpineA_M_pointConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "SpineA_M_pointConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "SpineA_M_pointConstraint1.w1";
connectAttr "SpineA_M.ro" "SpineA_M_orientConstraint1.cro";
connectAttr "SpineA_M.pim" "SpineA_M_orientConstraint1.cpim";
connectAttr "SpineA_M.jo" "SpineA_M_orientConstraint1.cjo";
connectAttr "FKXSpineA_M.r" "SpineA_M_orientConstraint1.tg[0].tr";
connectAttr "FKXSpineA_M.ro" "SpineA_M_orientConstraint1.tg[0].tro";
connectAttr "FKXSpineA_M.pm" "SpineA_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXSpineA_M.jo" "SpineA_M_orientConstraint1.tg[0].tjo";
connectAttr "SpineA_M_orientConstraint1.w0" "SpineA_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXSpineA_M.r" "SpineA_M_orientConstraint1.tg[1].tr";
connectAttr "IKXSpineA_M.ro" "SpineA_M_orientConstraint1.tg[1].tro";
connectAttr "IKXSpineA_M.pm" "SpineA_M_orientConstraint1.tg[1].tpm";
connectAttr "IKXSpineA_M.jo" "SpineA_M_orientConstraint1.tg[1].tjo";
connectAttr "SpineA_M_orientConstraint1.w1" "SpineA_M_orientConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendSpineReverse_M.ox" "SpineA_M_orientConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "SpineA_M_orientConstraint1.w1";
connectAttr "FKHipTwist_R.s" "HipTwist_R.s";
connectAttr "Pelvis_M.s" "HipTwist_R.is";
connectAttr "HipTwist_R_parentConstraint1.ctx" "HipTwist_R.tx";
connectAttr "HipTwist_R_parentConstraint1.cty" "HipTwist_R.ty";
connectAttr "HipTwist_R_parentConstraint1.ctz" "HipTwist_R.tz";
connectAttr "HipTwist_R_parentConstraint1.crx" "HipTwist_R.rx";
connectAttr "HipTwist_R_parentConstraint1.cry" "HipTwist_R.ry";
connectAttr "HipTwist_R_parentConstraint1.crz" "HipTwist_R.rz";
connectAttr "jointLayer.di" "HipTwist_R.do";
connectAttr "FKHip_R.s" "Hip_R.s";
connectAttr "Hip_R_parentConstraint1.ctx" "Hip_R.tx";
connectAttr "Hip_R_parentConstraint1.cty" "Hip_R.ty";
connectAttr "Hip_R_parentConstraint1.ctz" "Hip_R.tz";
connectAttr "Hip_R_parentConstraint1.crx" "Hip_R.rx";
connectAttr "Hip_R_parentConstraint1.cry" "Hip_R.ry";
connectAttr "Hip_R_parentConstraint1.crz" "Hip_R.rz";
connectAttr "jointLayer.di" "Hip_R.do";
connectAttr "ScaleBlendKnee_R.op" "Knee_R.s";
connectAttr "Hip_R.s" "Knee_R.is";
connectAttr "Knee_R_parentConstraint1.ctx" "Knee_R.tx";
connectAttr "Knee_R_parentConstraint1.cty" "Knee_R.ty";
connectAttr "Knee_R_parentConstraint1.ctz" "Knee_R.tz";
connectAttr "Knee_R_parentConstraint1.crx" "Knee_R.rx";
connectAttr "Knee_R_parentConstraint1.cry" "Knee_R.ry";
connectAttr "Knee_R_parentConstraint1.crz" "Knee_R.rz";
connectAttr "jointLayer.di" "Knee_R.do";
connectAttr "ScaleBlendBackKnee_R.op" "BackKnee_R.s";
connectAttr "BackKnee_R_parentConstraint1.ctx" "BackKnee_R.tx";
connectAttr "BackKnee_R_parentConstraint1.cty" "BackKnee_R.ty";
connectAttr "BackKnee_R_parentConstraint1.ctz" "BackKnee_R.tz";
connectAttr "BackKnee_R_parentConstraint1.crx" "BackKnee_R.rx";
connectAttr "BackKnee_R_parentConstraint1.cry" "BackKnee_R.ry";
connectAttr "BackKnee_R_parentConstraint1.crz" "BackKnee_R.rz";
connectAttr "jointLayer.di" "BackKnee_R.do";
connectAttr "ScaleBlendAnkle_R.op" "Ankle_R.s";
connectAttr "Ankle_R_parentConstraint1.ctx" "Ankle_R.tx";
connectAttr "Ankle_R_parentConstraint1.cty" "Ankle_R.ty";
connectAttr "Ankle_R_parentConstraint1.ctz" "Ankle_R.tz";
connectAttr "Ankle_R_parentConstraint1.crx" "Ankle_R.rx";
connectAttr "Ankle_R_parentConstraint1.cry" "Ankle_R.ry";
connectAttr "Ankle_R_parentConstraint1.crz" "Ankle_R.rz";
connectAttr "jointLayer.di" "Ankle_R.do";
connectAttr "Ankle_R.s" "Foot_End_R.is";
connectAttr "jointLayer.di" "Foot_End_R.do";
connectAttr "Ankle_R.ro" "Ankle_R_parentConstraint1.cro";
connectAttr "Ankle_R.pim" "Ankle_R_parentConstraint1.cpim";
connectAttr "Ankle_R.rp" "Ankle_R_parentConstraint1.crp";
connectAttr "Ankle_R.rpt" "Ankle_R_parentConstraint1.crt";
connectAttr "Ankle_R.jo" "Ankle_R_parentConstraint1.cjo";
connectAttr "FKXAnkle_R.t" "Ankle_R_parentConstraint1.tg[0].tt";
connectAttr "FKXAnkle_R.rp" "Ankle_R_parentConstraint1.tg[0].trp";
connectAttr "FKXAnkle_R.rpt" "Ankle_R_parentConstraint1.tg[0].trt";
connectAttr "FKXAnkle_R.r" "Ankle_R_parentConstraint1.tg[0].tr";
connectAttr "FKXAnkle_R.ro" "Ankle_R_parentConstraint1.tg[0].tro";
connectAttr "FKXAnkle_R.s" "Ankle_R_parentConstraint1.tg[0].ts";
connectAttr "FKXAnkle_R.pm" "Ankle_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXAnkle_R.jo" "Ankle_R_parentConstraint1.tg[0].tjo";
connectAttr "Ankle_R_parentConstraint1.w0" "Ankle_R_parentConstraint1.tg[0].tw";
connectAttr "IKXAnkle_R.t" "Ankle_R_parentConstraint1.tg[1].tt";
connectAttr "IKXAnkle_R.rp" "Ankle_R_parentConstraint1.tg[1].trp";
connectAttr "IKXAnkle_R.rpt" "Ankle_R_parentConstraint1.tg[1].trt";
connectAttr "IKXAnkle_R.r" "Ankle_R_parentConstraint1.tg[1].tr";
connectAttr "IKXAnkle_R.ro" "Ankle_R_parentConstraint1.tg[1].tro";
connectAttr "IKXAnkle_R.s" "Ankle_R_parentConstraint1.tg[1].ts";
connectAttr "IKXAnkle_R.pm" "Ankle_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXAnkle_R.jo" "Ankle_R_parentConstraint1.tg[1].tjo";
connectAttr "Ankle_R_parentConstraint1.w1" "Ankle_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Ankle_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Ankle_R_parentConstraint1.w1";
connectAttr "BackKnee_R.ro" "BackKnee_R_parentConstraint1.cro";
connectAttr "BackKnee_R.pim" "BackKnee_R_parentConstraint1.cpim";
connectAttr "BackKnee_R.rp" "BackKnee_R_parentConstraint1.crp";
connectAttr "BackKnee_R.rpt" "BackKnee_R_parentConstraint1.crt";
connectAttr "BackKnee_R.jo" "BackKnee_R_parentConstraint1.cjo";
connectAttr "FKXBackKnee_R.t" "BackKnee_R_parentConstraint1.tg[0].tt";
connectAttr "FKXBackKnee_R.rp" "BackKnee_R_parentConstraint1.tg[0].trp";
connectAttr "FKXBackKnee_R.rpt" "BackKnee_R_parentConstraint1.tg[0].trt";
connectAttr "FKXBackKnee_R.r" "BackKnee_R_parentConstraint1.tg[0].tr";
connectAttr "FKXBackKnee_R.ro" "BackKnee_R_parentConstraint1.tg[0].tro";
connectAttr "FKXBackKnee_R.s" "BackKnee_R_parentConstraint1.tg[0].ts";
connectAttr "FKXBackKnee_R.pm" "BackKnee_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXBackKnee_R.jo" "BackKnee_R_parentConstraint1.tg[0].tjo";
connectAttr "BackKnee_R_parentConstraint1.w0" "BackKnee_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXBackKnee_R.t" "BackKnee_R_parentConstraint1.tg[1].tt";
connectAttr "IKXBackKnee_R.rp" "BackKnee_R_parentConstraint1.tg[1].trp";
connectAttr "IKXBackKnee_R.rpt" "BackKnee_R_parentConstraint1.tg[1].trt";
connectAttr "IKXBackKnee_R.r" "BackKnee_R_parentConstraint1.tg[1].tr";
connectAttr "IKXBackKnee_R.ro" "BackKnee_R_parentConstraint1.tg[1].tro";
connectAttr "IKXBackKnee_R.s" "BackKnee_R_parentConstraint1.tg[1].ts";
connectAttr "IKXBackKnee_R.pm" "BackKnee_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXBackKnee_R.jo" "BackKnee_R_parentConstraint1.tg[1].tjo";
connectAttr "BackKnee_R_parentConstraint1.w1" "BackKnee_R_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendLegReverse_R.ox" "BackKnee_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "BackKnee_R_parentConstraint1.w1";
connectAttr "Knee_R.ro" "Knee_R_parentConstraint1.cro";
connectAttr "Knee_R.pim" "Knee_R_parentConstraint1.cpim";
connectAttr "Knee_R.rp" "Knee_R_parentConstraint1.crp";
connectAttr "Knee_R.rpt" "Knee_R_parentConstraint1.crt";
connectAttr "Knee_R.jo" "Knee_R_parentConstraint1.cjo";
connectAttr "FKXKnee_R.t" "Knee_R_parentConstraint1.tg[0].tt";
connectAttr "FKXKnee_R.rp" "Knee_R_parentConstraint1.tg[0].trp";
connectAttr "FKXKnee_R.rpt" "Knee_R_parentConstraint1.tg[0].trt";
connectAttr "FKXKnee_R.r" "Knee_R_parentConstraint1.tg[0].tr";
connectAttr "FKXKnee_R.ro" "Knee_R_parentConstraint1.tg[0].tro";
connectAttr "FKXKnee_R.s" "Knee_R_parentConstraint1.tg[0].ts";
connectAttr "FKXKnee_R.pm" "Knee_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXKnee_R.jo" "Knee_R_parentConstraint1.tg[0].tjo";
connectAttr "Knee_R_parentConstraint1.w0" "Knee_R_parentConstraint1.tg[0].tw";
connectAttr "IKXKnee_R.t" "Knee_R_parentConstraint1.tg[1].tt";
connectAttr "IKXKnee_R.rp" "Knee_R_parentConstraint1.tg[1].trp";
connectAttr "IKXKnee_R.rpt" "Knee_R_parentConstraint1.tg[1].trt";
connectAttr "IKXKnee_R.r" "Knee_R_parentConstraint1.tg[1].tr";
connectAttr "IKXKnee_R.ro" "Knee_R_parentConstraint1.tg[1].tro";
connectAttr "IKXKnee_R.s" "Knee_R_parentConstraint1.tg[1].ts";
connectAttr "IKXKnee_R.pm" "Knee_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXKnee_R.jo" "Knee_R_parentConstraint1.tg[1].tjo";
connectAttr "Knee_R_parentConstraint1.w1" "Knee_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Knee_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Knee_R_parentConstraint1.w1";
connectAttr "Hip_R.ro" "Hip_R_parentConstraint1.cro";
connectAttr "Hip_R.pim" "Hip_R_parentConstraint1.cpim";
connectAttr "Hip_R.rp" "Hip_R_parentConstraint1.crp";
connectAttr "Hip_R.rpt" "Hip_R_parentConstraint1.crt";
connectAttr "Hip_R.jo" "Hip_R_parentConstraint1.cjo";
connectAttr "FKXHip_R.t" "Hip_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHip_R.rp" "Hip_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHip_R.rpt" "Hip_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHip_R.r" "Hip_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHip_R.ro" "Hip_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHip_R.s" "Hip_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHip_R.pm" "Hip_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHip_R.jo" "Hip_R_parentConstraint1.tg[0].tjo";
connectAttr "Hip_R_parentConstraint1.w0" "Hip_R_parentConstraint1.tg[0].tw";
connectAttr "HipTwist_R.ro" "HipTwist_R_parentConstraint1.cro";
connectAttr "HipTwist_R.pim" "HipTwist_R_parentConstraint1.cpim";
connectAttr "HipTwist_R.rp" "HipTwist_R_parentConstraint1.crp";
connectAttr "HipTwist_R.rpt" "HipTwist_R_parentConstraint1.crt";
connectAttr "HipTwist_R.jo" "HipTwist_R_parentConstraint1.cjo";
connectAttr "FKXHipTwist_R.t" "HipTwist_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHipTwist_R.rp" "HipTwist_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHipTwist_R.rpt" "HipTwist_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHipTwist_R.r" "HipTwist_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHipTwist_R.ro" "HipTwist_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHipTwist_R.s" "HipTwist_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHipTwist_R.pm" "HipTwist_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHipTwist_R.jo" "HipTwist_R_parentConstraint1.tg[0].tjo";
connectAttr "HipTwist_R_parentConstraint1.w0" "HipTwist_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKHipTwist_L.s" "HipTwist_L.s";
connectAttr "Pelvis_M.s" "HipTwist_L.is";
connectAttr "HipTwist_L_parentConstraint1.ctx" "HipTwist_L.tx";
connectAttr "HipTwist_L_parentConstraint1.cty" "HipTwist_L.ty";
connectAttr "HipTwist_L_parentConstraint1.ctz" "HipTwist_L.tz";
connectAttr "HipTwist_L_parentConstraint1.crx" "HipTwist_L.rx";
connectAttr "HipTwist_L_parentConstraint1.cry" "HipTwist_L.ry";
connectAttr "HipTwist_L_parentConstraint1.crz" "HipTwist_L.rz";
connectAttr "jointLayer.di" "HipTwist_L.do";
connectAttr "FKHip_L.s" "Hip_L.s";
connectAttr "Hip_L_parentConstraint1.ctx" "Hip_L.tx";
connectAttr "Hip_L_parentConstraint1.cty" "Hip_L.ty";
connectAttr "Hip_L_parentConstraint1.ctz" "Hip_L.tz";
connectAttr "Hip_L_parentConstraint1.crx" "Hip_L.rx";
connectAttr "Hip_L_parentConstraint1.cry" "Hip_L.ry";
connectAttr "Hip_L_parentConstraint1.crz" "Hip_L.rz";
connectAttr "jointLayer.di" "Hip_L.do";
connectAttr "ScaleBlendKnee_L.op" "Knee_L.s";
connectAttr "Hip_L.s" "Knee_L.is";
connectAttr "Knee_L_parentConstraint1.ctx" "Knee_L.tx";
connectAttr "Knee_L_parentConstraint1.cty" "Knee_L.ty";
connectAttr "Knee_L_parentConstraint1.ctz" "Knee_L.tz";
connectAttr "Knee_L_parentConstraint1.crx" "Knee_L.rx";
connectAttr "Knee_L_parentConstraint1.cry" "Knee_L.ry";
connectAttr "Knee_L_parentConstraint1.crz" "Knee_L.rz";
connectAttr "jointLayer.di" "Knee_L.do";
connectAttr "ScaleBlendBackKnee_L.op" "BackKnee_L.s";
connectAttr "BackKnee_L_parentConstraint1.ctx" "BackKnee_L.tx";
connectAttr "BackKnee_L_parentConstraint1.cty" "BackKnee_L.ty";
connectAttr "BackKnee_L_parentConstraint1.ctz" "BackKnee_L.tz";
connectAttr "BackKnee_L_parentConstraint1.crx" "BackKnee_L.rx";
connectAttr "BackKnee_L_parentConstraint1.cry" "BackKnee_L.ry";
connectAttr "BackKnee_L_parentConstraint1.crz" "BackKnee_L.rz";
connectAttr "jointLayer.di" "BackKnee_L.do";
connectAttr "ScaleBlendAnkle_L.op" "Ankle_L.s";
connectAttr "Ankle_L_parentConstraint1.ctx" "Ankle_L.tx";
connectAttr "Ankle_L_parentConstraint1.cty" "Ankle_L.ty";
connectAttr "Ankle_L_parentConstraint1.ctz" "Ankle_L.tz";
connectAttr "Ankle_L_parentConstraint1.crx" "Ankle_L.rx";
connectAttr "Ankle_L_parentConstraint1.cry" "Ankle_L.ry";
connectAttr "Ankle_L_parentConstraint1.crz" "Ankle_L.rz";
connectAttr "jointLayer.di" "Ankle_L.do";
connectAttr "Ankle_L.s" "Foot_End_L.is";
connectAttr "jointLayer.di" "Foot_End_L.do";
connectAttr "Ankle_L.ro" "Ankle_L_parentConstraint1.cro";
connectAttr "Ankle_L.pim" "Ankle_L_parentConstraint1.cpim";
connectAttr "Ankle_L.rp" "Ankle_L_parentConstraint1.crp";
connectAttr "Ankle_L.rpt" "Ankle_L_parentConstraint1.crt";
connectAttr "Ankle_L.jo" "Ankle_L_parentConstraint1.cjo";
connectAttr "FKXAnkle_L.t" "Ankle_L_parentConstraint1.tg[0].tt";
connectAttr "FKXAnkle_L.rp" "Ankle_L_parentConstraint1.tg[0].trp";
connectAttr "FKXAnkle_L.rpt" "Ankle_L_parentConstraint1.tg[0].trt";
connectAttr "FKXAnkle_L.r" "Ankle_L_parentConstraint1.tg[0].tr";
connectAttr "FKXAnkle_L.ro" "Ankle_L_parentConstraint1.tg[0].tro";
connectAttr "FKXAnkle_L.s" "Ankle_L_parentConstraint1.tg[0].ts";
connectAttr "FKXAnkle_L.pm" "Ankle_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXAnkle_L.jo" "Ankle_L_parentConstraint1.tg[0].tjo";
connectAttr "Ankle_L_parentConstraint1.w0" "Ankle_L_parentConstraint1.tg[0].tw";
connectAttr "IKXAnkle_L.t" "Ankle_L_parentConstraint1.tg[1].tt";
connectAttr "IKXAnkle_L.rp" "Ankle_L_parentConstraint1.tg[1].trp";
connectAttr "IKXAnkle_L.rpt" "Ankle_L_parentConstraint1.tg[1].trt";
connectAttr "IKXAnkle_L.r" "Ankle_L_parentConstraint1.tg[1].tr";
connectAttr "IKXAnkle_L.ro" "Ankle_L_parentConstraint1.tg[1].tro";
connectAttr "IKXAnkle_L.s" "Ankle_L_parentConstraint1.tg[1].ts";
connectAttr "IKXAnkle_L.pm" "Ankle_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXAnkle_L.jo" "Ankle_L_parentConstraint1.tg[1].tjo";
connectAttr "Ankle_L_parentConstraint1.w1" "Ankle_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Ankle_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Ankle_L_parentConstraint1.w1";
connectAttr "BackKnee_L.ro" "BackKnee_L_parentConstraint1.cro";
connectAttr "BackKnee_L.pim" "BackKnee_L_parentConstraint1.cpim";
connectAttr "BackKnee_L.rp" "BackKnee_L_parentConstraint1.crp";
connectAttr "BackKnee_L.rpt" "BackKnee_L_parentConstraint1.crt";
connectAttr "BackKnee_L.jo" "BackKnee_L_parentConstraint1.cjo";
connectAttr "FKXBackKnee_L.t" "BackKnee_L_parentConstraint1.tg[0].tt";
connectAttr "FKXBackKnee_L.rp" "BackKnee_L_parentConstraint1.tg[0].trp";
connectAttr "FKXBackKnee_L.rpt" "BackKnee_L_parentConstraint1.tg[0].trt";
connectAttr "FKXBackKnee_L.r" "BackKnee_L_parentConstraint1.tg[0].tr";
connectAttr "FKXBackKnee_L.ro" "BackKnee_L_parentConstraint1.tg[0].tro";
connectAttr "FKXBackKnee_L.s" "BackKnee_L_parentConstraint1.tg[0].ts";
connectAttr "FKXBackKnee_L.pm" "BackKnee_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXBackKnee_L.jo" "BackKnee_L_parentConstraint1.tg[0].tjo";
connectAttr "BackKnee_L_parentConstraint1.w0" "BackKnee_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXBackKnee_L.t" "BackKnee_L_parentConstraint1.tg[1].tt";
connectAttr "IKXBackKnee_L.rp" "BackKnee_L_parentConstraint1.tg[1].trp";
connectAttr "IKXBackKnee_L.rpt" "BackKnee_L_parentConstraint1.tg[1].trt";
connectAttr "IKXBackKnee_L.r" "BackKnee_L_parentConstraint1.tg[1].tr";
connectAttr "IKXBackKnee_L.ro" "BackKnee_L_parentConstraint1.tg[1].tro";
connectAttr "IKXBackKnee_L.s" "BackKnee_L_parentConstraint1.tg[1].ts";
connectAttr "IKXBackKnee_L.pm" "BackKnee_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXBackKnee_L.jo" "BackKnee_L_parentConstraint1.tg[1].tjo";
connectAttr "BackKnee_L_parentConstraint1.w1" "BackKnee_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendLegReverse_L.ox" "BackKnee_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "BackKnee_L_parentConstraint1.w1";
connectAttr "Knee_L.ro" "Knee_L_parentConstraint1.cro";
connectAttr "Knee_L.pim" "Knee_L_parentConstraint1.cpim";
connectAttr "Knee_L.rp" "Knee_L_parentConstraint1.crp";
connectAttr "Knee_L.rpt" "Knee_L_parentConstraint1.crt";
connectAttr "Knee_L.jo" "Knee_L_parentConstraint1.cjo";
connectAttr "FKXKnee_L.t" "Knee_L_parentConstraint1.tg[0].tt";
connectAttr "FKXKnee_L.rp" "Knee_L_parentConstraint1.tg[0].trp";
connectAttr "FKXKnee_L.rpt" "Knee_L_parentConstraint1.tg[0].trt";
connectAttr "FKXKnee_L.r" "Knee_L_parentConstraint1.tg[0].tr";
connectAttr "FKXKnee_L.ro" "Knee_L_parentConstraint1.tg[0].tro";
connectAttr "FKXKnee_L.s" "Knee_L_parentConstraint1.tg[0].ts";
connectAttr "FKXKnee_L.pm" "Knee_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXKnee_L.jo" "Knee_L_parentConstraint1.tg[0].tjo";
connectAttr "Knee_L_parentConstraint1.w0" "Knee_L_parentConstraint1.tg[0].tw";
connectAttr "IKXKnee_L.t" "Knee_L_parentConstraint1.tg[1].tt";
connectAttr "IKXKnee_L.rp" "Knee_L_parentConstraint1.tg[1].trp";
connectAttr "IKXKnee_L.rpt" "Knee_L_parentConstraint1.tg[1].trt";
connectAttr "IKXKnee_L.r" "Knee_L_parentConstraint1.tg[1].tr";
connectAttr "IKXKnee_L.ro" "Knee_L_parentConstraint1.tg[1].tro";
connectAttr "IKXKnee_L.s" "Knee_L_parentConstraint1.tg[1].ts";
connectAttr "IKXKnee_L.pm" "Knee_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXKnee_L.jo" "Knee_L_parentConstraint1.tg[1].tjo";
connectAttr "Knee_L_parentConstraint1.w1" "Knee_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Knee_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Knee_L_parentConstraint1.w1";
connectAttr "Hip_L.ro" "Hip_L_parentConstraint1.cro";
connectAttr "Hip_L.pim" "Hip_L_parentConstraint1.cpim";
connectAttr "Hip_L.rp" "Hip_L_parentConstraint1.crp";
connectAttr "Hip_L.rpt" "Hip_L_parentConstraint1.crt";
connectAttr "Hip_L.jo" "Hip_L_parentConstraint1.cjo";
connectAttr "FKXHip_L.t" "Hip_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHip_L.rp" "Hip_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHip_L.rpt" "Hip_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHip_L.r" "Hip_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHip_L.ro" "Hip_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHip_L.s" "Hip_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHip_L.pm" "Hip_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHip_L.jo" "Hip_L_parentConstraint1.tg[0].tjo";
connectAttr "Hip_L_parentConstraint1.w0" "Hip_L_parentConstraint1.tg[0].tw";
connectAttr "HipTwist_L.ro" "HipTwist_L_parentConstraint1.cro";
connectAttr "HipTwist_L.pim" "HipTwist_L_parentConstraint1.cpim";
connectAttr "HipTwist_L.rp" "HipTwist_L_parentConstraint1.crp";
connectAttr "HipTwist_L.rpt" "HipTwist_L_parentConstraint1.crt";
connectAttr "HipTwist_L.jo" "HipTwist_L_parentConstraint1.cjo";
connectAttr "FKXHipTwist_L.t" "HipTwist_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHipTwist_L.rp" "HipTwist_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHipTwist_L.rpt" "HipTwist_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHipTwist_L.r" "HipTwist_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHipTwist_L.ro" "HipTwist_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHipTwist_L.s" "HipTwist_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHipTwist_L.pm" "HipTwist_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHipTwist_L.jo" "HipTwist_L_parentConstraint1.tg[0].tjo";
connectAttr "HipTwist_L_parentConstraint1.w0" "HipTwist_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Pelvis_M.pim" "Pelvis_M_pointConstraint1.cpim";
connectAttr "Pelvis_M.rp" "Pelvis_M_pointConstraint1.crp";
connectAttr "Pelvis_M.rpt" "Pelvis_M_pointConstraint1.crt";
connectAttr "FKXPelvis_M.t" "Pelvis_M_pointConstraint1.tg[0].tt";
connectAttr "FKXPelvis_M.rp" "Pelvis_M_pointConstraint1.tg[0].trp";
connectAttr "FKXPelvis_M.rpt" "Pelvis_M_pointConstraint1.tg[0].trt";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_pointConstraint1.tg[0].tpm";
connectAttr "Pelvis_M_pointConstraint1.w0" "Pelvis_M_pointConstraint1.tg[0].tw";
connectAttr "IKXPelvis_M.t" "Pelvis_M_pointConstraint1.tg[1].tt";
connectAttr "IKXPelvis_M.rp" "Pelvis_M_pointConstraint1.tg[1].trp";
connectAttr "IKXPelvis_M.rpt" "Pelvis_M_pointConstraint1.tg[1].trt";
connectAttr "IKXPelvis_M.pm" "Pelvis_M_pointConstraint1.tg[1].tpm";
connectAttr "Pelvis_M_pointConstraint1.w1" "Pelvis_M_pointConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "Pelvis_M_pointConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Pelvis_M_pointConstraint1.w1";
connectAttr "Pelvis_M.ro" "Pelvis_M_orientConstraint1.cro";
connectAttr "Pelvis_M.pim" "Pelvis_M_orientConstraint1.cpim";
connectAttr "Pelvis_M.jo" "Pelvis_M_orientConstraint1.cjo";
connectAttr "FKXPelvis_M.r" "Pelvis_M_orientConstraint1.tg[0].tr";
connectAttr "FKXPelvis_M.ro" "Pelvis_M_orientConstraint1.tg[0].tro";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXPelvis_M.jo" "Pelvis_M_orientConstraint1.tg[0].tjo";
connectAttr "Pelvis_M_orientConstraint1.w0" "Pelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKOrientToSpine_M.r" "Pelvis_M_orientConstraint1.tg[1].tr";
connectAttr "IKOrientToSpine_M.ro" "Pelvis_M_orientConstraint1.tg[1].tro";
connectAttr "IKOrientToSpine_M.pm" "Pelvis_M_orientConstraint1.tg[1].tpm";
connectAttr "Pelvis_M_orientConstraint1.w1" "Pelvis_M_orientConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendSpineReverse_M.ox" "Pelvis_M_orientConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Pelvis_M_orientConstraint1.w1";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "irisPuppetSG.msg" "materialInfo3.sg";
connectAttr "BPRig_irisPuppetSG.msg" "BPRig_materialInfo3.sg";
connectAttr "jointVisReverse.ox" "jointLayer.lod";
connectAttr "Main.jointVis" "jointVisReverse.ix";
connectAttr "Main.iog" "ControlSet.dsm" -na;
connectAttr "FKHead_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHead_M.iog" "ControlSet.dsm" -na;
connectAttr "FKGun2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraGun2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKGun1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraGun1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWrist1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWrist1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWrist2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWrist2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeMid5_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeMid5_L.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeMid4_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeMid4_L.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeMid3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeMid3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeMid2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeMid2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeMid1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeMid1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeR3_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeR3_R.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeR2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeR2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeR1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeR1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeL3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeL3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeL2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeL2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKCapeL1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraCapeL1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKChest_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraChest_M.iog" "ControlSet.dsm" -na;
connectAttr "FKSpineB_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraSpineB_M.iog" "ControlSet.dsm" -na;
connectAttr "FKSpineA_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraSpineA_M.iog" "ControlSet.dsm" -na;
connectAttr "FKAnkle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraAnkle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKBackKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraBackKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHip_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHip_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHipTwist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHipTwist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKAnkle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraAnkle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKBackKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraBackKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHip_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHip_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHipTwist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHipTwist_L.iog" "ControlSet.dsm" -na;
connectAttr "HipSwingerPelvis_M.iog" "ControlSet.dsm" -na;
connectAttr "IKArm_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraArm_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleArm_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraArm_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIKArm_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKSpine0_M.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraSpine0_M.iog" "ControlSet.dsm" -na;
connectAttr "IKSpine2_M.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraSpine2_M.iog" "ControlSet.dsm" -na;
connectAttr "IKSpine4_M.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraSpine4_M.iog" "ControlSet.dsm" -na;
connectAttr "FKIKSpine_M.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "ControlSet.dsm" -na;
connectAttr "Center_M.iog" "ControlSet.dsm" -na;
connectAttr "Head_M.iog" "GameSet.dsm" -na;
connectAttr "Gun2_R.iog" "GameSet.dsm" -na;
connectAttr "Gun1_R.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "Wrist1_L.iog" "GameSet.dsm" -na;
connectAttr "Wrist2_L.iog" "GameSet.dsm" -na;
connectAttr "Elbow1_L.iog" "GameSet.dsm" -na;
connectAttr "Shoulder1_L.iog" "GameSet.dsm" -na;
connectAttr "CapeMid5_L.iog" "GameSet.dsm" -na;
connectAttr "CapeMid4_L.iog" "GameSet.dsm" -na;
connectAttr "CapeMid3_L.iog" "GameSet.dsm" -na;
connectAttr "CapeMid2_L.iog" "GameSet.dsm" -na;
connectAttr "CapeMid1_L.iog" "GameSet.dsm" -na;
connectAttr "CapeR3_R.iog" "GameSet.dsm" -na;
connectAttr "CapeR2_R.iog" "GameSet.dsm" -na;
connectAttr "CapeR1_R.iog" "GameSet.dsm" -na;
connectAttr "CapeL3_L.iog" "GameSet.dsm" -na;
connectAttr "CapeL2_L.iog" "GameSet.dsm" -na;
connectAttr "CapeL1_L.iog" "GameSet.dsm" -na;
connectAttr "Chest_M.iog" "GameSet.dsm" -na;
connectAttr "SpineB_M.iog" "GameSet.dsm" -na;
connectAttr "SpineA_M.iog" "GameSet.dsm" -na;
connectAttr "Ankle_R.iog" "GameSet.dsm" -na;
connectAttr "BackKnee_R.iog" "GameSet.dsm" -na;
connectAttr "Knee_R.iog" "GameSet.dsm" -na;
connectAttr "Hip_R.iog" "GameSet.dsm" -na;
connectAttr "HipTwist_R.iog" "GameSet.dsm" -na;
connectAttr "Pelvis_M.iog" "GameSet.dsm" -na;
connectAttr "Ankle_L.iog" "GameSet.dsm" -na;
connectAttr "BackKnee_L.iog" "GameSet.dsm" -na;
connectAttr "Knee_L.iog" "GameSet.dsm" -na;
connectAttr "Hip_L.iog" "GameSet.dsm" -na;
connectAttr "HipTwist_L.iog" "GameSet.dsm" -na;
connectAttr "buildPose.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "CenterBtwFeet_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendKnee_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendBackKnee_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAnkle_L.msg" "AllSet.dnsm" -na;
connectAttr "IKLegAimLegMultiplyDivide_L.msg" "AllSet.dnsm" -na;
connectAttr "IKLegAimLegReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "IKLegAimLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion6.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion5.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "Leg_LAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion4.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendPelvis_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendKnee_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendBackKnee_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAnkle_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendSpineA_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendSpineB_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendChest_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendShoulder1_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendElbow1_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendWrist2_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendWrist1_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpinesetRange_M.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpineCondition_M.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpineReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpineUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "IKTwistSpineUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo4_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide4_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo3_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide3_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo2_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide2_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo1_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide1_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo0_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide0_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "IKCurveInfoAllMultiplySpine_M.msg" "AllSet.dnsm" -na;
connectAttr "IKCurveInfoNormalizeSpine_M.msg" "AllSet.dnsm" -na;
connectAttr "IKCurveInfoSpine_M.msg" "AllSet.dnsm" -na;
connectAttr "IKStiffSpine4_M.msg" "AllSet.dnsm" -na;
connectAttr "IKStiffSpine2_M.msg" "AllSet.dnsm" -na;
connectAttr "IKStiffSpine0_M.msg" "AllSet.dnsm" -na;
connectAttr "IKLegAimLegMultiplyDivide_R.msg" "AllSet.dnsm" -na;
connectAttr "IKLegAimLegReverse_R.msg" "AllSet.dnsm" -na;
connectAttr "IKLegAimLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion3.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion2.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "Leg_RAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion1.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmsetRange_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmCondition_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "PoleArm_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "IKArm_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "GameSet.msg" "AllSet.dnsm" -na;
connectAttr "ControlSet.msg" "AllSet.dnsm" -na;
connectAttr "jointVisReverse.msg" "AllSet.dnsm" -na;
connectAttr "jointLayer.msg" "AllSet.dnsm" -na;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsOffset_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenter_M.iog" "AllSet.dsm" -na;
connectAttr "Center_MShape.iog" "AllSet.dsm" -na;
connectAttr "Center_M.iog" "AllSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "AllSet.dsm" -na;
connectAttr "CenterOffset_M.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Knee_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "BackKnee_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Ankle_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKXLegAimBlend_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKXLegAimBlend_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegAim_L_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKXLegUnAim_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegAim_L.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_L.iog" "AllSet.dsm" -na;
connectAttr "effector7.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_LStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L.iog" "AllSet.dsm" -na;
connectAttr "effector6.iog" "AllSet.dsm" -na;
connectAttr "IKOrientToSpine_M.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Knee_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "BackKnee_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Ankle_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "SpineA_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "SpineA_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "SpineB_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "SpineB_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Chest_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Chest_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeL1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeL2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeL3_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeR1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeR2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeR3_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeMid1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeMid2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeMid3_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeMid4_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "CapeMid5_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wrist2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wrist1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToWrist1_L_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "MiddleFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Gun1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Gun2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "Head_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "FKIKParentConstraintSpine_M.iog" "AllSet.dsm" -na;
connectAttr "FKIKSpine_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKSpine_M.iog" "AllSet.dsm" -na;
connectAttr "UnTwistEndPelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "UnTwistIKPelvis_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "UnTwistIKPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "effector5.iog" "AllSet.dsm" -na;
connectAttr "UnTwistEndPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "UnTwistPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "TwistFollowPelvis_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TwistFollowPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKXChest_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKSpine0AlignTo_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4AlignUnTwistTo_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4AlignUnTwistToOffset_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4AlignTo_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineB_M_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFake1UpLocSpine_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKFake1UpLocSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKEndJointOrientToSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKStiffEndOrientSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKStiffStartOrientSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4_M.iog" "AllSet.dsm" -na;
connectAttr "IKExtraSpine4_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine4_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator4_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator4_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator3_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator3_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine2_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKSpine2_M.iog" "AllSet.dsm" -na;
connectAttr "IKExtraSpine2_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine2_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator2_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator2_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator1_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator1_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine0_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKSpine0_M.iog" "AllSet.dsm" -na;
connectAttr "IKExtraSpine0_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine0_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator0_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator0_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineCurve_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineCurve_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineHandle_M.iog" "AllSet.dsm" -na;
connectAttr "effector4.iog" "AllSet.dsm" -na;
connectAttr "IKfake2Spine_M.iog" "AllSet.dsm" -na;
connectAttr "IKfake1Spine_M.iog" "AllSet.dsm" -na;
connectAttr "IKXLegAimBlend_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKXLegAimBlend_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegAim_R_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKXLegUnAim_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegAim_R.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_R.iog" "AllSet.dsm" -na;
connectAttr "effector3.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_RStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R.iog" "AllSet.dsm" -na;
connectAttr "effector2.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintArm_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_LStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKXArmHandle_L_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist2_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_LStatic.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKXArmHandle_L.iog" "AllSet.dsm" -na;
connectAttr "effector1.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerStabalizePelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerGroupPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerPelvis_MShape.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerOffsetPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintKnee_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKXHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKHip_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKXKnee_L.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetKnee_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKXKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetKnee_L.iog" "AllSet.dsm" -na;
connectAttr "IKXBackKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKXBackKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKBackKnee_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKBackKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraBackKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetBackKnee_L.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKXAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHeel_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKXFoot_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXFoot_End_L.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "Hip_L.iog" "AllSet.dsm" -na;
connectAttr "Knee_L.iog" "AllSet.dsm" -na;
connectAttr "BackKnee_L.iog" "AllSet.dsm" -na;
connectAttr "Ankle_L.iog" "AllSet.dsm" -na;
connectAttr "Foot_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintKnee_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintShoulder1_L_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "IKXPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKXPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKHip_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKXKnee_R.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetKnee_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKXKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetKnee_R.iog" "AllSet.dsm" -na;
connectAttr "IKXBackKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKXBackKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKBackKnee_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKBackKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraBackKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetBackKnee_R.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKXAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHeel_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXFoot_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXFoot_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKXSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKSpineA_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineB_M.iog" "AllSet.dsm" -na;
connectAttr "FKXSpineB_M.iog" "AllSet.dsm" -na;
connectAttr "FKSpineB_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKSpineB_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraSpineB_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetSpineB_M.iog" "AllSet.dsm" -na;
connectAttr "IKXChest_M.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKXChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKChest_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeL1_L.iog" "AllSet.dsm" -na;
connectAttr "FKCapeL1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeL1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeL1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeL1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeL2_L.iog" "AllSet.dsm" -na;
connectAttr "FKCapeL2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeL2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeL2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeL2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeL3_L.iog" "AllSet.dsm" -na;
connectAttr "FKCapeL3_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeL3_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeL3_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeL3_L.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeL4_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeR1_R.iog" "AllSet.dsm" -na;
connectAttr "FKCapeR1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeR1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeR1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeR1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeR2_R.iog" "AllSet.dsm" -na;
connectAttr "FKCapeR2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeR2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeR2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeR2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeR3_R.iog" "AllSet.dsm" -na;
connectAttr "FKCapeR3_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeR3_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeR3_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeR3_R.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeR4_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeMid1_L.iog" "AllSet.dsm" -na;
connectAttr "FKCapeMid1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeMid1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeMid1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeMid1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeMid2_L.iog" "AllSet.dsm" -na;
connectAttr "FKCapeMid2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeMid2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeMid2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeMid2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeMid3_L.iog" "AllSet.dsm" -na;
connectAttr "FKCapeMid3_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeMid3_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeMid3_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeMid3_L.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeMid4_L.iog" "AllSet.dsm" -na;
connectAttr "FKCapeMid4_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeMid4_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeMid4_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeMid4_L.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeMid5_L.iog" "AllSet.dsm" -na;
connectAttr "FKCapeMid5_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKCapeMid5_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraCapeMid5_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetCapeMid5_L.iog" "AllSet.dsm" -na;
connectAttr "FKXCapeMid6_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKXShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "IKXElbow1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow1_L.iog" "AllSet.dsm" -na;
connectAttr "FKElbow1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow1_L.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist2_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToWrist2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWrist2_L.iog" "AllSet.dsm" -na;
connectAttr "FKWrist2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWrist2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWrist2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWrist2_L.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist1_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToWrist1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWrist1_L.iog" "AllSet.dsm" -na;
connectAttr "FKWrist1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWrist1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWrist1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWrist1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToWrist1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXGun1_R.iog" "AllSet.dsm" -na;
connectAttr "FKGun1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKGun1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraGun1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetGun1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXGun2_R.iog" "AllSet.dsm" -na;
connectAttr "FKGun2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKGun2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraGun2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetGun2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXGun3_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKHead_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHead_End_M.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "Hip_R.iog" "AllSet.dsm" -na;
connectAttr "Knee_R.iog" "AllSet.dsm" -na;
connectAttr "BackKnee_R.iog" "AllSet.dsm" -na;
connectAttr "Ankle_R.iog" "AllSet.dsm" -na;
connectAttr "Foot_End_R.iog" "AllSet.dsm" -na;
connectAttr "SpineA_M.iog" "AllSet.dsm" -na;
connectAttr "SpineB_M.iog" "AllSet.dsm" -na;
connectAttr "Chest_M.iog" "AllSet.dsm" -na;
connectAttr "CapeL1_L.iog" "AllSet.dsm" -na;
connectAttr "CapeL2_L.iog" "AllSet.dsm" -na;
connectAttr "CapeL3_L.iog" "AllSet.dsm" -na;
connectAttr "CapeL4_End_L.iog" "AllSet.dsm" -na;
connectAttr "CapeR1_R.iog" "AllSet.dsm" -na;
connectAttr "CapeR2_R.iog" "AllSet.dsm" -na;
connectAttr "CapeR3_R.iog" "AllSet.dsm" -na;
connectAttr "CapeR4_End_R.iog" "AllSet.dsm" -na;
connectAttr "CapeMid1_L.iog" "AllSet.dsm" -na;
connectAttr "CapeMid2_L.iog" "AllSet.dsm" -na;
connectAttr "CapeMid3_L.iog" "AllSet.dsm" -na;
connectAttr "CapeMid4_L.iog" "AllSet.dsm" -na;
connectAttr "CapeMid5_L.iog" "AllSet.dsm" -na;
connectAttr "CapeMid6_End_L.iog" "AllSet.dsm" -na;
connectAttr "Shoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "Elbow1_L.iog" "AllSet.dsm" -na;
connectAttr "Wrist2_L.iog" "AllSet.dsm" -na;
connectAttr "Wrist1_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "Gun1_R.iog" "AllSet.dsm" -na;
connectAttr "Gun2_R.iog" "AllSet.dsm" -na;
connectAttr "Gun3_End_R.iog" "AllSet.dsm" -na;
connectAttr "Head_M.iog" "AllSet.dsm" -na;
connectAttr "Head_End_M.iog" "AllSet.dsm" -na;
connectAttr "IKMessure.iog" "AllSet.dsm" -na;
connectAttr "IKCrv.iog" "AllSet.dsm" -na;
connectAttr "IKStatic.iog" "AllSet.dsm" -na;
connectAttr "IKHandle.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraint.iog" "AllSet.dsm" -na;
connectAttr "TwistSystem.iog" "AllSet.dsm" -na;
connectAttr "RootSystem.iog" "AllSet.dsm" -na;
connectAttr "FKIKSystem.iog" "AllSet.dsm" -na;
connectAttr "IKSystem.iog" "AllSet.dsm" -na;
connectAttr "FKSystem.iog" "AllSet.dsm" -na;
connectAttr "GameSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MotionSystem.iog" "AllSet.dsm" -na;
connectAttr "BaseSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MainShape.iog" "AllSet.dsm" -na;
connectAttr "Main.iog" "AllSet.dsm" -na;
connectAttr "forwardArrowShape.iog" "AllSet.dsm" -na;
connectAttr "GameSet.msg" "Sets.dnsm" -na;
connectAttr "ControlSet.msg" "Sets.dnsm" -na;
connectAttr "AllSet.msg" "Sets.dnsm" -na;
connectAttr "IKArm_L.follow" "IKArm_LSetRangeFollow.vx";
connectAttr "IKArm_L.follow" "IKArm_LSetRangeFollow.vy";
connectAttr "PoleArm_L.follow" "PoleArm_LSetRangeFollow.vx";
connectAttr "PoleArm_L.follow" "PoleArm_LSetRangeFollow.vy";
connectAttr "FKIKArm_L.FKIKBlend" "FKIKBlendArmUnitConversion_L.i";
connectAttr "FKIKBlendArmUnitConversion_L.o" "FKIKBlendArmReverse_L.ix";
connectAttr "FKIKArm_L.autoVis" "FKIKBlendArmCondition_L.ft";
connectAttr "FKIKArm_L.IKVis" "FKIKBlendArmCondition_L.ctr";
connectAttr "FKIKArm_L.FKVis" "FKIKBlendArmCondition_L.ctg";
connectAttr "FKIKArm_L.FKIKBlend" "FKIKBlendArmCondition_L.cfr";
connectAttr "FKIKBlendArmsetRange_L.ox" "FKIKBlendArmCondition_L.cfg";
connectAttr "FKIKArm_L.FKIKBlend" "FKIKBlendArmsetRange_L.vx";
connectAttr "IKLeg_R.swivel" "unitConversion1.i";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vx";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vy";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegUnitConversion_R.i";
connectAttr "FKIKBlendLegUnitConversion_R.o" "FKIKBlendLegReverse_R.ix";
connectAttr "FKIKLeg_R.autoVis" "FKIKBlendLegCondition_R.ft";
connectAttr "FKIKLeg_R.IKVis" "FKIKBlendLegCondition_R.ctr";
connectAttr "FKIKLeg_R.FKVis" "FKIKBlendLegCondition_R.ctg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegCondition_R.cfr";
connectAttr "FKIKBlendLegsetRange_R.ox" "FKIKBlendLegCondition_R.cfg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegsetRange_R.vx";
connectAttr "IKLeg_R.rollAngle" "Leg_RAngleReverse.i1x";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vx";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vy";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vz";
connectAttr "Leg_RAngleReverse.ox" "IKRollAngleLeg_R.nx";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.my";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.mz";
connectAttr "IKRollAngleLeg_R.ox" "unitConversion2.i";
connectAttr "IKRollAngleLeg_R.oy" "unitConversion3.i";
connectAttr "IKLeg_R.legAim" "IKLegAimLegUnitConversion_R.i";
connectAttr "IKLegAimLegMultiplyDivide_R.oy" "IKLegAimLegReverse_R.iy";
connectAttr "IKLegAimLegUnitConversion_R.o" "IKLegAimLegMultiplyDivide_R.i1y";
connectAttr "FKIKBlendLegUnitConversion_R.o" "IKLegAimLegMultiplyDivide_R.i2y";
connectAttr "IKSpine0_M.stiff" "IKStiffSpine0_M.vy";
connectAttr "IKSpine2_M.stiff" "IKStiffSpine2_M.vy";
connectAttr "IKSpine4_M.stiff" "IKStiffSpine4_M.vy";
connectAttr "IKXSpineCurve_MShape.ws" "IKCurveInfoSpine_M.ic";
connectAttr "IKCurveInfoSpine_M.al" "IKCurveInfoNormalizeSpine_M.i1y";
connectAttr "IKCurveInfoNormalizeSpine_M.oy" "IKCurveInfoAllMultiplySpine_M.i1y"
		;
connectAttr "Main.sy" "IKCurveInfoAllMultiplySpine_M.i2y";
connectAttr "IKSpine4_M.stretchy" "stretchySpineUnitConversion_M.i";
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineReverse_M.iy";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide0_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo0_M.ab";
connectAttr "stretchySpineMultiplyDivide0_M.oy" "stretchySpineBlendTwo0_M.i[1]";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide1_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo1_M.ab";
connectAttr "stretchySpineMultiplyDivide1_M.oy" "stretchySpineBlendTwo1_M.i[1]";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide2_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo2_M.ab";
connectAttr "stretchySpineMultiplyDivide2_M.oy" "stretchySpineBlendTwo2_M.i[1]";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide3_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo3_M.ab";
connectAttr "stretchySpineMultiplyDivide3_M.oy" "stretchySpineBlendTwo3_M.i[1]";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide4_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo4_M.ab";
connectAttr "stretchySpineMultiplyDivide4_M.oy" "stretchySpineBlendTwo4_M.i[1]";
connectAttr "UnTwistEndPelvis_M.ry" "IKTwistSpineUnitConversion_M.i";
connectAttr "FKIKSpine_M.FKIKBlend" "FKIKBlendSpineUnitConversion_M.i";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "FKIKBlendSpineReverse_M.ix";
connectAttr "FKIKSpine_M.autoVis" "FKIKBlendSpineCondition_M.ft";
connectAttr "FKIKSpine_M.IKVis" "FKIKBlendSpineCondition_M.ctr";
connectAttr "FKIKSpine_M.FKVis" "FKIKBlendSpineCondition_M.ctg";
connectAttr "FKIKSpine_M.FKIKBlend" "FKIKBlendSpineCondition_M.cfr";
connectAttr "FKIKBlendSpinesetRange_M.ox" "FKIKBlendSpineCondition_M.cfg";
connectAttr "FKIKSpine_M.FKIKBlend" "FKIKBlendSpinesetRange_M.vx";
connectAttr "FKWrist1_L.s" "ScaleBlendWrist1_L.c2";
connectAttr "FKIKBlendArmUnitConversion_L.o" "ScaleBlendWrist1_L.b";
connectAttr "FKWrist2_L.s" "ScaleBlendWrist2_L.c2";
connectAttr "FKIKBlendArmUnitConversion_L.o" "ScaleBlendWrist2_L.b";
connectAttr "FKElbow1_L.s" "ScaleBlendElbow1_L.c2";
connectAttr "FKIKBlendArmUnitConversion_L.o" "ScaleBlendElbow1_L.b";
connectAttr "FKShoulder1_L.s" "ScaleBlendShoulder1_L.c2";
connectAttr "FKIKBlendArmUnitConversion_L.o" "ScaleBlendShoulder1_L.b";
connectAttr "FKChest_M.s" "ScaleBlendChest_M.c2";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "ScaleBlendChest_M.b";
connectAttr "FKSpineB_M.s" "ScaleBlendSpineB_M.c2";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "ScaleBlendSpineB_M.b";
connectAttr "FKSpineA_M.s" "ScaleBlendSpineA_M.c2";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "ScaleBlendSpineA_M.b";
connectAttr "FKAnkle_R.s" "ScaleBlendAnkle_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendAnkle_R.b";
connectAttr "FKBackKnee_R.s" "ScaleBlendBackKnee_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendBackKnee_R.b";
connectAttr "FKKnee_R.s" "ScaleBlendKnee_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendKnee_R.b";
connectAttr "FKPelvis_M.s" "ScaleBlendPelvis_M.c2";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "ScaleBlendPelvis_M.b";
connectAttr "IKLeg_L.swivel" "unitConversion4.i";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vx";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vy";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegUnitConversion_L.i";
connectAttr "FKIKBlendLegUnitConversion_L.o" "FKIKBlendLegReverse_L.ix";
connectAttr "FKIKLeg_L.autoVis" "FKIKBlendLegCondition_L.ft";
connectAttr "FKIKLeg_L.IKVis" "FKIKBlendLegCondition_L.ctr";
connectAttr "FKIKLeg_L.FKVis" "FKIKBlendLegCondition_L.ctg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegCondition_L.cfr";
connectAttr "FKIKBlendLegsetRange_L.ox" "FKIKBlendLegCondition_L.cfg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegsetRange_L.vx";
connectAttr "IKLeg_L.rollAngle" "Leg_LAngleReverse.i1x";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vx";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vy";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vz";
connectAttr "Leg_LAngleReverse.ox" "IKRollAngleLeg_L.nx";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.my";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.mz";
connectAttr "IKRollAngleLeg_L.ox" "unitConversion5.i";
connectAttr "IKRollAngleLeg_L.oy" "unitConversion6.i";
connectAttr "IKLeg_L.legAim" "IKLegAimLegUnitConversion_L.i";
connectAttr "IKLegAimLegMultiplyDivide_L.oy" "IKLegAimLegReverse_L.iy";
connectAttr "IKLegAimLegUnitConversion_L.o" "IKLegAimLegMultiplyDivide_L.i1y";
connectAttr "FKIKBlendLegUnitConversion_L.o" "IKLegAimLegMultiplyDivide_L.i2y";
connectAttr "FKAnkle_L.s" "ScaleBlendAnkle_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendAnkle_L.b";
connectAttr "FKBackKnee_L.s" "ScaleBlendBackKnee_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendBackKnee_L.b";
connectAttr "FKKnee_L.s" "ScaleBlendKnee_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendKnee_L.b";
connectAttr "Center_M.CenterBtwFeet" "CenterBtwFeet_M.vx";
connectAttr "Center_M.CenterBtwFeet" "PelvisCenterBtwFeetUnitConversion_M.i";
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwFeetReverse_M.ix"
		;
connectAttr "irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "BPRig_irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ikSCsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikSplineSolver.msg" ":ikSystem.sol" -na;
// End of Scout__rig.ma
