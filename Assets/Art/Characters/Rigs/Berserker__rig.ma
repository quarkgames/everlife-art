//Maya ASCII 2013 scene
//Name: Berserker__rig.ma
//Last modified: Fri, Apr 11, 2014 01:23:08 PM
//Codeset: UTF-8
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.1894401895487938 15.642671043110507 36.579643621135766 ;
	setAttr ".r" -type "double3" -16.538352728518177 1075.799999999678 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 44.411290014514719;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 4.1784972405491922 7.7612609843192404 1.6715583172870974 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.5698517566574912 9.2330006461955882 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 20.377162581754032;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 6.4794975166716737 -1.0195760413273467 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 11.1997179230661;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "ctrl_rig";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Main" -p "ctrl_rig";
	addAttr -ci true -sn "height" -ln "height" -at "double";
	addAttr -ci true -sn "fkVis" -ln "fkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "ikVis" -ln "ikVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fkIkVis" -ln "fkIkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimVis" -ln "aimVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "eyeVis" -ln "eyeVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fingerVis" -ln "fingerVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "bendVis" -ln "bendVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "arrowVis" -ln "arrowVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "jointVis" -ln "jointVis" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -l on ".height" 13.811564033360575;
	setAttr -cb on ".fkVis";
	setAttr -cb on ".ikVis";
	setAttr -cb on ".fkIkVis";
	setAttr -cb on ".aimVis";
	setAttr -cb on ".eyeVis";
	setAttr -cb on ".fingerVis";
	setAttr -cb on ".bendVis";
	setAttr -cb on ".arrowVis";
	setAttr -cb on ".jointVis";
createNode nurbsCurve -n "MainShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		-3.1944594689999997e-16 1.7145024419999999e-16 -2.7999949750000002
		-1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		-2.7999949750000002 4.9681991409999996e-32 -8.113684933e-16
		-1.9798954339999999 -1.2123363030000002e-16 1.9798954339999999
		-8.4369313090000001e-16 -1.7145024419999999e-16 2.7999949750000002
		1.9798954339999999 -1.2123363030000002e-16 1.9798954339999999
		2.7999949750000002 -9.2086260590000001e-32 1.503882763e-15
		1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		-3.1944594689999997e-16 1.7145024419999999e-16 -2.7999949750000002
		-1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		;
createNode transform -n "BaseSkeleton" -p "Main";
	setAttr -l on ".v" no;
createNode transform -n "BaseSkeletonOffset" -p "BaseSkeleton";
createNode joint -n "Pelvis" -p "BaseSkeletonOffset";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 5.9604644775390625e-08 5.2490606307983398 -1.2424887120723729 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 1;
createNode joint -n "SpineA" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0 0.92606592178344715 -0.00051128792762744624 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Mid";
createNode joint -n "Chest" -p "SpineA";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0 1.3383469581604004 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Chest";
createNode joint -n "Clavicle" -p "Chest";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.2105141877206975 2.0222060583659243 0.87635690729864191 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -90.000003930293772 84.93129508055091 -4.081938974963629 ;
	setAttr ".dl" yes;
	setAttr ".sd" 3;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "Shoulder1" -p "Clavicle";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 2.3869795029440866e-15 1.438304102456631 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -42.090372616003606 -0.35546573493853373 0.08132585494137079 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Shoulder1";
createNode joint -n "Elbow1" -p "Shoulder1";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.22499009467596209 4.096966861694181 0.21896610692668261 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.443234219938256 -2.9766418362804816 94.68306802031185 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "Wrist2" -p "Elbow1";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.1553423969519212 2.7109514989648074 -0.86113381767890484 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -97.185970603061847 76.527857399514147 -94.999999999999886 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Hand1";
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "Wrist1" -p "Wrist2";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.085558306333425094 0.37731943629396941 0.017482291770029024 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Hand11";
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "Barrel1" -p "Wrist1";
	setAttr ".t" -type "double3" -0.12643525580937798 0.8645464262243443 -0.69426794824468052 ;
	setAttr ".r" -type "double3" 9.1407167783991792 258.32761195818597 7.946928014594552 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -34.462082586865911 -8.7285733235282201 -1.7903981777634761 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "4";
createNode joint -n "Barrel1_End" -p "Barrel1";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 1.5211859788328059 3.5527136788005009e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 9.2236085762442027e-14 -2.3655375443384907e-14 1.2722218725854067e-14 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "3";
createNode joint -n "Barrel3" -p "Wrist1";
	setAttr ".t" -type "double3" 0.59771234485346447 0.8630812155759704 0.2553654503474827 ;
	setAttr ".r" -type "double3" -115.01003099769831 -87.847456983124658 -116.36499169074168 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0.065532877363568762 20.527688987272207 -2.5422327562497964 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "8";
createNode joint -n "Barrel3_End" -p "Barrel3";
	setAttr ".t" -type "double3" 2.4424906541753444e-15 1.6294905799562955 7.1054273576010019e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.272221872585407e-14 -1.8834847254291763e-14 2.4848083448933724e-14 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "7";
createNode joint -n "Barrel2" -p "Wrist1";
	setAttr ".t" -type "double3" -0.60575244809646556 0.86692657736121093 0.40065146316768896 ;
	setAttr ".r" -type "double3" -1.7721985028616831 -89.999999999999318 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "Barrel2_End" -p "Barrel2";
	setAttr ".t" -type "double3" 2.2204460492503131e-15 1.5368634349753663 3.5527136788005009e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -6.6791648310733881e-14 -6.5598940305185073e-14 -7.1562480332929151e-15 ;
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "Head" -p "Chest";
	setAttr ".t" -type "double3" 0 1.3032944042756718 1.030975793432424 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "Head_End" -p "Head";
	setAttr ".t" -type "double3" 3.1589715250000827e-16 1.8033760160193548 1.7486012637846216e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "23";
createNode joint -n "Clavicle1" -p "Chest";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 1.2105099403953554 2.0222064892578122 0.87635700000000116 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999702899345 -89.999995246389517 2.875167631178551e-08 ;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "Shoulder" -p "Clavicle1";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.12707446247097437 -1.4290493987035899 -0.10198145706176652 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -38.023918364922856 0 -4.9999938741906291 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
	setAttr ".dl" yes;
	setAttr ".typ" 10;
createNode joint -n "Elbow" -p "Shoulder";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.24481092820452088 -4.5159835206681702 -0.14617962992843658 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -93.916479527012143 -31.612203259522939 95.454455138366512 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "Wrist" -p "Elbow";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.57694609555033338 -1.730107068039775 -0.012315717006032068 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 17.19742698393474 92.095231137754197 10.897917771998776 ;
	setAttr ".dl" yes;
	setAttr ".typ" 12;
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "MiddleFinger1" -p "Wrist";
	setAttr ".t" -type "double3" 0.40154283045784578 -0.3059332364249423 -0.10969212849629884 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.9285595221306053 -86.765182877390203 -7.6130455398118082 ;
	setAttr ".pa" -type "double3" -2.490303168013669e-17 3.8068719241856415 -4.0949047407001542 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "21";
createNode joint -n "MiddleFinger5" -p "MiddleFinger1";
	setAttr ".t" -type "double3" -0.60580558317900213 -0.98707658579378965 -0.22319654049490817 ;
	setAttr ".r" -type "double3" 32.544060717756359 7.2737883270893411 174.67014830365079 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0 0 2.5199999009299203 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "20";
createNode joint -n "MiddleFinger6" -p "MiddleFinger5";
	setAttr ".t" -type "double3" 0 1.0787334918522269 0 ;
	setAttr ".r" -type "double3" 24.356478443228198 -4.2536987591874231 1.3197451190983127 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0 0 3.6712939054552742 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "19";
createNode joint -n "MiddleFinger7_End" -p "MiddleFinger6";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 1.6026326255252454 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.00017675715840228753 -3.1805546814635168e-14 179.99979030053379 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "18";
createNode joint -n "MiddleFinger2" -p "MiddleFinger1";
	setAttr ".t" -type "double3" 0.56841026477458634 -0.98585602879046075 -0.4854464332702017 ;
	setAttr ".r" -type "double3" 32.450271004110654 -17.589779845254103 -172.50351475401254 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0 0 2.5199999009299203 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "20";
createNode joint -n "MiddleFinger3" -p "MiddleFinger2";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.0793649773753775 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 24.987380460310412 -5.0918936860128694 -0.51202475659202473 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0 0 3.6712939054552742 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "19";
createNode joint -n "MiddleFinger4_End" -p "MiddleFinger3";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.4056735102234916 -5.620504062164855e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.00020152388643063574 -5.7249984266343308e-14 179.99976091802003 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "18";
createNode joint -n "ThumbFinger1" -p "Wrist";
	setAttr ".t" -type "double3" -0.36777760239480417 -0.95234201253672524 0.90711995931591083 ;
	setAttr ".r" -type "double3" 157.6056649408784 4.5037394989151558 2.6409958608682373 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -34.462082586865911 -8.7285733235282201 -1.7903981777634761 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "4";
createNode joint -n "ThumbFinger2" -p "ThumbFinger1";
	setAttr ".t" -type "double3" 0 1.0724462014893907 0 ;
	setAttr ".r" -type "double3" 13.068879256668723 0.28858816588631786 -1.2280945744517413 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "3";
createNode joint -n "ThumbFinger3_End" -p "ThumbFinger2";
	setAttr ".t" -type "double3" 0 1.4566019002008428 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4572258181333257 -2.6135011069317922e-06 -179.99997264373772 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "2";
createNode joint -n "Hand_AP1" -p "Wrist";
	setAttr ".t" -type "double3" 9.9571029194001071e-07 -0.32519415569504861 8.9052131979627802e-07 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.9575588931591462e-06 -89.999999999999773 0 ;
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "Pipe1" -p "Chest";
	setAttr ".t" -type "double3" -4.3963231444358826 -0.79495811462402333 -4.807751447677612 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "Pipe1_End" -p "Pipe1";
	setAttr ".t" -type "double3" 0 1.8033760160193548 1.7763568394002505e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "23";
createNode joint -n "Pipe2" -p "Chest";
	setAttr ".t" -type "double3" -7.3409672379493713 -2.4120712280273442 -2.8420160121917721 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".sd" 3;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "Pipe2_End" -p "Pipe2";
	setAttr ".t" -type "double3" 0 1.8033760160193548 1.7763568394002505e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "23";
createNode joint -n "HipTwist" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.2119851708412173 0.058133125305176669 -0.0001863837242122024 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 205.1395790329064 89.999999999999758 ;
createNode joint -n "Hip" -p "HipTwist";
	setAttr ".t" -type "double3" -0.088520804836134559 0.74529486894607477 -0.041540897564234625 ;
	setAttr ".r" -type "double3" 1.0589532311777121 -0.72669176883990849 -90.175490857600806 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 2;
createNode joint -n "Knee" -p "Hip";
	setAttr ".t" -type "double3" 1.1102230246251565e-15 2.2734843510971179 -2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 22.919762006828137 -6.0894502037902902e-14 -3.0038248579264827e-13 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "Ankle" -p "Knee";
	setAttr ".t" -type "double3" 1.2434497875801753e-14 2.6670582522596749 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 1.490097395720801 -0.007769599634289439 0.59745912492160935 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 3.1147589914174403 -1.2104724556304991 -11.405913270501992 ;
	setAttr ".dl" yes;
	setAttr ".typ" 4;
createNode joint -n "MiddleToe1" -p "Ankle";
	setAttr ".t" -type "double3" 0.020825579278234807 0.15220357689789743 -1.3263746982714686 ;
	setAttr ".r" -type "double3" 108.35550551633652 0.28330165836355059 -179.14629009068284 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.0407002849537961e-13 -5.963540027744092e-16 1.4210240379423903e-15 ;
	setAttr ".pa" -type "double3" -0.00019030234564052423 0.00053514845282692043 25.864574245063647 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Ball";
createNode joint -n "MiddleToe2_End" -p "MiddleToe1";
	setAttr ".t" -type "double3" -5.9952043329758453e-15 1.4371052628296199 -6.6613381477509392e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.517107764305804 -3.9348917493144408e-13 -7.3814052773437456e-14 ;
	setAttr ".dl" yes;
	setAttr ".typ" 5;
createNode joint -n "Heel_End" -p "Ankle";
	setAttr ".t" -type "double3" -0.023211789696358176 0.62094301335498636 1.4783495666826167 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 59.095245418304913 179.10046423217725 -1.2722218725854067e-13 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Heel";
createNode transform -n "MotionSystem" -p "Main";
createNode transform -n "FKSystem" -p "MotionSystem";
createNode transform -n "FKParentConstraintToWrist1_R" -p "FKSystem";
	setAttr ".ro" 5;
createNode joint -n "FKOffsetBarrel1_R" -p "FKParentConstraintToWrist1_R";
	setAttr ".t" -type "double3" -0.12643525580937975 0.8645464262243453 -0.69426794824468008 ;
	setAttr ".r" -type "double3" 9.140716661502065 258.32761720304376 7.9469276606307746 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraBarrel1_R" -p "FKOffsetBarrel1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.3322676295501878e-15 -6.6613381477509392e-16 -1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKBarrel1_R" -p "FKExtraBarrel1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 4.4408920985006262e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKBarrel1_RShape" -p "FKBarrel1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.68983530479999999 0.38366884089999997 -0.68983530479999999
		-0.97557444370000002 0.38366884089999997 1.776356839e-15
		-0.68983530479999999 0.38366884089999997 0.68983530479999999
		-2.794710557e-16 0.38366884089999997 0.97557444370000002
		0.68983530479999999 0.38366884089999997 0.68983530479999999
		0.97557444370000002 0.38366884089999997 1.776356839e-15
		0.68983530479999999 0.38366884089999997 -0.68983530479999999
		5.2720894789999994e-16 0.38366884089999997 -0.97557444370000002
		-0.68983530479999999 0.38366884089999997 -0.68983530479999999
		-0.97557444370000002 0.38366884089999997 1.776356839e-15
		-0.68983530479999999 0.38366884089999997 0.68983530479999999
		;
createNode joint -n "FKXBarrel1_R" -p "FKBarrel1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXBarrel1_End_R" -p "FKXBarrel1_R";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 1.5211859788328059 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -74.941662930976236 -23.285312837255951 -169.71541501167798 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 74.94166293097625 23.285312837255972 169.71541501167798 ;
createNode joint -n "FKOffsetBarrel3_R" -p "FKParentConstraintToWrist1_R";
	setAttr ".t" -type "double3" 0.59771234485346447 0.86308121557597095 0.25536545034748404 ;
	setAttr ".r" -type "double3" -115.01003105623299 -87.847454727170302 -116.36499032340177 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraBarrel3_R" -p "FKOffsetBarrel3_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -4.4408920985006262e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKBarrel3_R" -p "FKExtraBarrel3_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 2.2204460492503131e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999978 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKBarrel3_RShape" -p "FKBarrel3_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.72181521260000003 0.38366884089999997 -0.72181521260000003
		-1.0208008630000001 0.38366884089999997 1.776356839e-15
		-0.72181521260000003 0.38366884089999997 0.72181521260000003
		-5.1784715670000003e-16 0.38366884089999997 1.0208008630000001
		0.72181521260000003 0.38366884089999997 0.72181521260000003
		1.0208008630000001 0.38366884089999997 1.776356839e-15
		0.72181521260000003 0.38366884089999997 -0.72181521260000003
		3.2622953000000001e-16 0.38366884089999997 -1.0208008630000001
		-0.72181521260000003 0.38366884089999997 -0.72181521260000003
		-1.0208008630000001 0.38366884089999997 1.776356839e-15
		-0.72181521260000003 0.38366884089999997 0.72181521260000003
		;
createNode joint -n "FKXBarrel3_R" -p "FKBarrel3_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXBarrel3_End_R" -p "FKXBarrel3_R";
	setAttr ".t" -type "double3" 4.8849813083506888e-15 1.6294905799562958 7.1054273576010019e-15 ;
	setAttr ".r" -type "double3" -72.293738484013289 -33.640693232156224 -168.42735840420639 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 72.293738484013303 33.640693232156224 168.42735840420639 ;
createNode joint -n "FKOffsetBarrel2_R" -p "FKParentConstraintToWrist1_R";
	setAttr ".t" -type "double3" -0.60575244809646733 0.86692657736121193 0.40065146316768985 ;
	setAttr ".r" -type "double3" -1.7721985548136492 -90.000002504478161 0 ;
createNode transform -n "FKExtraBarrel2_R" -p "FKOffsetBarrel2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -6.6613381477509392e-16 -1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" 0 9.9392333795734924e-17 -1.192708005548819e-14 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKBarrel2_R" -p "FKExtraBarrel2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" 1.5902773407317587e-14 1.0436195048552162e-14 -3.975693351829395e-16 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKBarrel2_RShape" -p "FKBarrel2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.69446450429999995 0.34402980059999999 -0.69446450429999995
		-0.9821211205 0.34402980059999999 2.8532622299999997e-15
		-0.69446450429999995 0.34402980059999999 0.69446450429999995
		-2.6671620829999998e-16 0.34402980059999999 0.9821211205
		0.69446450429999995 0.34402980059999999 0.69446450429999995
		0.9821211205 0.34402980059999999 2.8532622299999997e-15
		0.69446450429999995 0.34402980059999999 -0.69446450429999995
		5.4537709130000001e-16 0.34402980059999999 -0.9821211205
		-0.69446450429999995 0.34402980059999999 -0.69446450429999995
		-0.9821211205 0.34402980059999999 2.8532622299999997e-15
		-0.69446450429999995 0.34402980059999999 0.69446450429999995
		;
createNode joint -n "FKXBarrel2_R" -p "FKBarrel2_R";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXBarrel2_End_R" -p "FKXBarrel2_R";
	setAttr ".t" -type "double3" 1.5543122344752192e-15 1.5368634349753676 5.3290705182007514e-15 ;
	setAttr ".r" -type "double3" 75.451855334171029 -2.5871941888059991 -143.44595839072002 ;
	setAttr ".jo" -type "double3" 72.257728084388944 34.567459167593356 167.03813285070973 ;
createNode parentConstraint -n "FKParentConstraintToWrist1_R_parentConstraint1" -p
		 "FKParentConstraintToWrist1_R";
	addAttr -ci true -k true -sn "w0" -ln "Wrist1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 85.661380948787397 -53.367264355696271 -16.259954277165299 ;
	setAttr ".rst" -type "double3" -5.6388832736269654 6.5330846216776814 2.5469149404018703 ;
	setAttr ".rsrr" -type "double3" 85.66137105281247 -53.36726027136136 -16.259964029503418 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToChest_M" -p "FKSystem";
createNode joint -n "FKOffsetClavicle_R" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" -1.2105141877206975 2.0222060583659243 0.87635690729864191 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -90.000002504478161 84.931291800655018 -4.0819391871662374 ;
createNode transform -n "FKExtraClavicle_R" -p "FKOffsetClavicle_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.7755575615628914e-17 0 -1.7763568394002505e-15 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKClavicle_R" -p "FKExtraClavicle_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 5.5511151231257827e-17 0 -1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKClavicle_RShape" -p "FKClavicle_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.066140528 -0.33743607520000002 -2.0208767249999999
		-2.9904095869999998 -0.33743607520000002 0.21050617120000001
		-2.066140528 -0.33743607520000002 2.4418890680000001
		0.16524236819999999 -0.33743607520000002 3.3661581260000002
		2.3966252649999999 -0.33743607520000002 2.4418890680000001
		3.3208943230000001 -0.33743607520000002 0.21050617120000001
		2.3966252649999999 -0.33743607520000002 -2.0208767249999999
		0.16524236819999999 -0.33743607520000002 -2.9451457840000002
		-2.066140528 -0.33743607520000002 -2.0208767249999999
		-2.9904095869999998 -0.33743607520000002 0.21050617120000001
		-2.066140528 -0.33743607520000002 2.4418890680000001
		;
createNode joint -n "FKXClavicle_R" -p "FKClavicle_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetShoulder1_R" -p "FKXClavicle_R";
	setAttr ".t" -type "double3" 8.3266726846886741e-16 1.4383041024566312 7.1054273576010019e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -42.090371401664918 -0.35546572276994209 0.081325855158045177 ;
createNode transform -n "FKExtraShoulder1_R" -p "FKOffsetShoulder1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -5.5511151231257827e-17 4.4408920985006262e-16 3.5527136788005009e-15 ;
	setAttr ".ro" 5;
createNode transform -n "FKShoulder1_R" -p "FKExtraShoulder1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 5.5511151231257827e-17 0 3.5527136788005009e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulder1_RShape" -p "FKShoulder1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.3747129920000001 -1.3747129920000001 -1.8174799139999998e-15
		-1.944137757 2.9756104140000002e-15 -1.5751618389999999e-15
		-1.3747129920000001 1.3747129920000001 -1.2637096510000002e-15
		-5.3976359720000001e-16 1.944137757 -1.303264669e-15
		1.3747129920000001 1.3747129920000001 -8.2860444359999996e-16
		1.944137757 2.9756104140000002e-15 -7.1179123820000001e-16
		1.3747129920000001 -1.3747129920000001 -1.065934556e-15
		1.067799068e-15 -1.944137757 -1.382374707e-15
		-1.3747129920000001 -1.3747129920000001 -1.8174799139999998e-15
		-1.944137757 2.9756104140000002e-15 -1.5751618389999999e-15
		-1.3747129920000001 1.3747129920000001 -1.2637096510000002e-15
		;
createNode joint -n "FKXShoulder1_R" -p "FKShoulder1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow1_R" -p "FKXShoulder1_R";
	setAttr ".t" -type "double3" -0.22499009467595926 4.096966861694181 0.21896610692667196 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -2.4432342250812544 -2.9766419037382197 94.683067067384044 ;
createNode transform -n "FKExtraElbow1_R" -p "FKOffsetElbow1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 5.5511151231257827e-16 0 1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
createNode transform -n "FKElbow1_R" -p "FKExtraElbow1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.1102230246251565e-16 0 -1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999944 0.99999999999999956 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow1_RShape" -p "FKElbow1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.0293275909999999 0.99425410390000002 -0.26640958419999999
		-1.45568904 2.5094872960000001e-15 1.5061167529999999e-15
		-1.0293275909999999 -0.99425410390000002 0.26640958419999999
		-1.3262800789999999e-16 -1.4060876389999999 0.37676004730000001
		1.0293275909999999 -0.99425410390000002 0.26640958419999999
		1.45568904 2.342172224e-15 8.8168840510000008e-16
		1.0293275909999999 0.99425410390000002 -0.26640958419999999
		1.0710476799999999e-15 1.4060876389999999 -0.37676004730000001
		-1.0293275909999999 0.99425410390000002 -0.26640958419999999
		-1.45568904 2.5094872960000001e-15 1.5061167529999999e-15
		-1.0293275909999999 -0.99425410390000002 0.26640958419999999
		;
createNode joint -n "FKXElbow1_R" -p "FKElbow1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetWrist2_R" -p "FKXElbow1_R";
	setAttr ".t" -type "double3" 0.15534239695192054 2.7109514989648096 -0.86113381767890829 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -97.185969398167714 76.527857347789052 -95.000001505250978 ;
createNode transform -n "FKExtraWrist2_R" -p "FKOffsetWrist2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -2.2204460492503131e-16 -5.5511151231257827e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKWrist2_R" -p "FKExtraWrist2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 -2.2204460492503131e-16 -1.1102230246251565e-16 ;
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWrist2_RShape" -p "FKWrist2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.618847672 -0.18723496440000001 -1.669978891
		-2.3069822200000001 -0.18723496440000001 -0.0086751329549999995
		-1.618847672 -0.18723496440000001 1.652628626
		0.042456085999999997 -0.18723496440000001 2.340763173
		1.7037598439999999 -0.18723496440000001 1.652628626
		2.3918943920000002 -0.18723496440000001 -0.0086751329549999995
		1.7037598439999999 -0.18723496440000001 -1.669978891
		0.042456085999999997 -0.18723496440000001 -2.3581134389999998
		-1.618847672 -0.18723496440000001 -1.669978891
		-2.3069822200000001 -0.18723496440000001 -0.0086751329549999995
		-1.618847672 -0.18723496440000001 1.652628626
		;
createNode joint -n "FKXWrist2_R" -p "FKWrist2_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToWrist2_R" -p "FKXWrist2_R";
	setAttr ".r" -type "double3" -87.347219377050109 -12.762703563084598 54.032392559845739 ;
	setAttr ".ro" 5;
createNode joint -n "FKOffsetWrist1_R" -p "FKXWrist2_R";
	setAttr ".t" -type "double3" -0.08555830633342687 0.3773194362939698 0.017482291770028358 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraWrist1_R" -p "FKOffsetWrist1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -4.4408920985006262e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKWrist1_R" -p "FKExtraWrist1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 2.2204460492503131e-16 -7.7715611723760958e-16 ;
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWrist1_RShape" -p "FKWrist1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.5138834249999999 1.0953955980000002e-15 -1.5138834249999999
		-2.1409544710000001 1.1419347160000001e-15 1.6663683159999999e-15
		-1.5138834249999999 9.0999834890000002e-16 1.5138834249999999
		-6.2039504350000001e-16 5.3545167449999999e-16 2.1409544710000001
		1.5138834249999999 2.3769905489999997e-16 1.5138834249999999
		2.1409544710000001 1.9115993689999998e-16 1.6663683159999999e-15
		1.5138834249999999 4.2309630409999997e-16 -1.5138834249999999
		1.149910823e-15 7.9764297849999996e-16 -2.1409544710000001
		-1.5138834249999999 1.0953955980000002e-15 -1.5138834249999999
		-2.1409544710000001 1.1419347160000001e-15 1.6663683159999999e-15
		-1.5138834249999999 9.0999834890000002e-16 1.5138834249999999
		;
createNode joint -n "FKXWrist1_R" -p "FKWrist1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToWrist1_R" -p "FKXWrist1_R";
	setAttr ".r" -type "double3" -87.347219377050109 -12.762703563084598 54.032392559845739 ;
	setAttr ".ro" 5;
createNode joint -n "FKOffsetHead_M" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" 0 1.3032944042756718 1.030975793432424 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraHead_M" -p "FKOffsetHead_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
createNode transform -n "FKHead_M" -p "FKExtraHead_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHead_MShape" -p "FKHead_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.6456111929999999 2.6658598260000002 -3.4844812360000001
		-2.3272456689999999 2.6658598260000002 -0.43688914769999998
		-1.6456111929999999 2.6658598260000002 2.6107029399999999
		-6.7437757149999995e-16 2.6658598260000002 3.873056917
		1.6456111929999999 2.6658598260000002 2.6107029399999999
		2.3272456689999999 2.6658598260000002 -0.43688914769999998
		1.6456111929999999 2.6658598260000002 -3.4844812360000001
		1.249968188e-15 2.6658598260000002 -4.7468352129999998
		-1.6456111929999999 2.6658598260000002 -3.4844812360000001
		-2.3272456689999999 2.6658598260000002 -0.43688914769999998
		-1.6456111929999999 2.6658598260000002 2.6107029399999999
		;
createNode joint -n "FKXHead_M" -p "FKHead_M";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXHead_End_M" -p "FKXHead_M";
	setAttr ".t" -type "double3" 3.1589715250000827e-16 1.8033760160193548 1.7486012637846216e-15 ;
createNode joint -n "FKOffsetClavicle1_L" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" 1.2105099403953554 2.0222064892578118 0.87635700000000116 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 90.000002504478161 -89.999995674288996 2.8751676806689024e-08 ;
createNode transform -n "FKExtraClavicle1_L" -p "FKOffsetClavicle1_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 2;
createNode transform -n "FKClavicle1_L" -p "FKExtraClavicle1_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKClavicle1_LShape" -p "FKClavicle1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.2313869 6.3210046399999995e-16 -2.2313869
		-3.1556576160000001 7.006967488e-16 0
		-2.2313869 3.5883438139999998e-16 2.2313869
		-1.139518182e-15 -1.932283e-16 3.1556576160000001
		2.2313869 -6.3210046399999995e-16 2.2313869
		3.1556576160000001 -7.006967488e-16 0
		2.2313869 -3.5883438139999998e-16 -2.2313869
		1.4698223089999999e-15 1.932283e-16 -3.1556576160000001
		-2.2313869 6.3210046399999995e-16 -2.2313869
		-3.1556576160000001 7.006967488e-16 0
		-2.2313869 3.5883438139999998e-16 2.2313869
		;
createNode joint -n "FKXClavicle1_L" -p "FKClavicle1_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetShoulder_L" -p "FKXClavicle1_L";
	setAttr ".t" -type "double3" -0.12707448618352024 -1.4290493964414877 -0.10198145921310697 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -38.023919241554857 0 -4.9999938781309368 ;
createNode transform -n "FKExtraShoulder_L" -p "FKOffsetShoulder_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 4.4408920985006262e-16 1.7763568394002505e-15 ;
	setAttr ".ro" 5;
createNode transform -n "FKShoulder_L" -p "FKExtraShoulder_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -5.5511151231257827e-17 4.4408920985006262e-16 1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulder_LShape" -p "FKShoulder_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.4908533079999999 -1.513026736 0.01580597924
		-2.1083849670000001 -0.022173427780000001 0.01580597924
		-1.4908533079999999 1.46867988 0.01580597924
		-5.8605260980000005e-16 2.0862115389999998 0.01580597924
		1.4908533079999999 1.46867988 0.01580597924
		2.1083849670000001 -0.022173427780000001 0.01580597924
		1.4908533079999999 -1.513026736 0.01580597924
		1.157322285e-15 -2.130558395 0.01580597924
		-1.4908533079999999 -1.513026736 0.01580597924
		-2.1083849670000001 -0.022173427780000001 0.01580597924
		-1.4908533079999999 1.46867988 0.01580597924
		;
createNode joint -n "FKXShoulder_L" -p "FKShoulder_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow_L" -p "FKXShoulder_L";
	setAttr ".t" -type "double3" 0.24481092699936768 -4.5159835214914352 -0.14617960651331252 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -93.916480785887671 -31.61220210161045 95.454454971866951 ;
createNode transform -n "FKExtraElbow_L" -p "FKOffsetElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKElbow_L" -p "FKExtraElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow_LShape" -p "FKElbow_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.7416580150000001 -1.7026010009999999 0.00022318494509999999
		-2.4588938530000002 0.02895948592 0.00022318494509999999
		-1.7416580150000001 1.7605199730000001 0.00022318494509999999
		-0.01009752794 2.4777558110000002 0.00022318494509999999
		1.7214629589999999 1.7605199730000001 0.00022318494509999999
		2.4386987969999998 0.02895948592 0.00022318494509999999
		1.7214629589999999 -1.7026010009999999 0.00022318494509999999
		-0.01009752794 -2.4198368389999998 0.00022318494509999999
		-1.7416580150000001 -1.7026010009999999 0.00022318494509999999
		-2.4588938530000002 0.02895948592 0.00022318494509999999
		-1.7416580150000001 1.7605199730000001 0.00022318494509999999
		;
createNode joint -n "FKXElbow_L" -p "FKElbow_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetWrist_L" -p "FKXElbow_L";
	setAttr ".t" -type "double3" 0.57694609555033338 -1.7301070680397732 -0.012315717006032179 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 17.197427660209748 92.095231334271276 10.897917721723953 ;
createNode transform -n "FKExtraWrist_L" -p "FKOffsetWrist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 5.5511151231257827e-17 0 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "FKWrist_L" -p "FKExtraWrist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 8.8817841970012523e-16 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWrist_LShape" -p "FKWrist_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.7513468720000001 0.021396425989999999 -1.751346866
		-2.4767784750000001 0.021396425989999999 -5.236851175e-08
		-1.7513468720000001 0.021396425989999999 1.751346761
		-5.8554315859999997e-08 0.021396425989999999 2.4767783639999998
		1.7513467549999999 0.021396425989999999 1.751346761
		2.4767783570000002 0.021396425989999999 -5.236851175e-08
		1.7513467549999999 0.021396425989999999 -1.751346866
		-5.8554313809999998e-08 0.021396425989999999 -2.476778468
		-1.7513468720000001 0.021396425989999999 -1.751346866
		-2.4767784750000001 0.021396425989999999 -5.236851175e-08
		-1.7513468720000001 0.021396425989999999 1.751346761
		;
createNode joint -n "FKXWrist_L" -p "FKWrist_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToWrist_L" -p "FKXWrist_L";
	setAttr ".r" -type "double3" -26.975921985880454 87.451657622770171 26.944821646964211 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999944 0.99999999999999978 ;
createNode joint -n "FKXHand_AP1_L" -p "FKXWrist_L";
	setAttr ".t" -type "double3" 9.9571029210654416e-07 -0.32519415569505039 8.9052132068445644e-07 ;
	setAttr ".r" -type "double3" 178.84342829242735 -2.2709215025522238 -179.94597543039276 ;
	setAttr ".jo" -type "double3" -1.362079439941289 87.728436310297965 -0.20657554852451643 ;
createNode joint -n "FKOffsetPipe1_R" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" -4.3963231444358826 -0.79495811462402344 -4.807751447677612 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraPipe1_R" -p "FKOffsetPipe1_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
createNode transform -n "FKPipe1_R" -p "FKExtraPipe1_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKPipe1_RShape" -p "FKPipe1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.1446761320000001 -0.053241648179999997 -1.1446761320000001
		-1.6188165109999999 -0.053241648179999997 -7.7649495660000002e-17
		-1.1446761320000001 -0.053241648179999997 1.1446761320000001
		-4.6909252520000004e-16 -0.053241648179999997 1.6188165109999999
		1.1446761320000001 -0.053241648179999997 1.1446761320000001
		1.6188165109999999 -0.053241648179999997 -7.7649495660000002e-17
		1.1446761320000001 -0.053241648179999997 -1.1446761320000001
		8.6946950570000006e-16 -0.053241648179999997 -1.6188165109999999
		-1.1446761320000001 -0.053241648179999997 -1.1446761320000001
		-1.6188165109999999 -0.053241648179999997 -7.7649495660000002e-17
		-1.1446761320000001 -0.053241648179999997 1.1446761320000001
		;
createNode joint -n "FKXPipe1_R" -p "FKPipe1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXPipe1_End_R" -p "FKXPipe1_R";
	setAttr ".t" -type "double3" 0 1.8033760160193548 1.7763568394002505e-15 ;
createNode joint -n "FKOffsetPipe2_R" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" -7.3409672379493713 -2.4120712280273438 -2.8420160121917721 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraPipe2_R" -p "FKOffsetPipe2_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
createNode transform -n "FKPipe2_R" -p "FKExtraPipe2_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKPipe2_RShape" -p "FKPipe2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.1446761320000001 3.242603576e-16 -1.1446761320000001
		-1.6188165109999999 3.594494725e-16 0
		-1.1446761320000001 1.840779615e-16 1.1446761320000001
		-4.6909252520000004e-16 -9.9123922909999996e-17 1.6188165109999999
		1.1446761320000001 -3.242603576e-16 1.1446761320000001
		1.6188165109999999 -3.594494725e-16 0
		1.1446761320000001 -1.840779615e-16 -1.1446761320000001
		8.6946950570000006e-16 9.9123922909999996e-17 -1.6188165109999999
		-1.1446761320000001 3.242603576e-16 -1.1446761320000001
		-1.6188165109999999 3.594494725e-16 0
		-1.1446761320000001 1.840779615e-16 1.1446761320000001
		;
createNode joint -n "FKXPipe2_R" -p "FKPipe2_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXPipe2_End_R" -p "FKXPipe2_R";
	setAttr ".t" -type "double3" 0 1.8033760160193548 1.7763568394002505e-15 ;
createNode parentConstraint -n "FKParentConstraintToChest_M_parentConstraint1" -p
		 "FKParentConstraintToChest_M";
	addAttr -ci true -k true -sn "w0" -ln "Chest_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 5.9604644775390625e-08 7.5134735107421875 -1.2430000000000003 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToWrist_L" -p "FKSystem";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999989 0.99999999999999978 ;
createNode joint -n "FKOffsetMiddleFinger1_L" -p "FKParentConstraintToWrist_L";
	setAttr ".t" -type "double3" 0.40154283045784589 -0.30593323642494319 -0.10969212849629884 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 2.9285595064154455 -86.765183932425103 -7.6130458158857488 ;
createNode transform -n "FKExtraMiddleFinger1_L" -p "FKOffsetMiddleFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 8.8817841970012523e-16 1.6653345369377348e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000007 ;
createNode transform -n "FKMiddleFinger1_L" -p "FKExtraMiddleFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999967 1.0000000000000002 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger1_LShape" -p "FKMiddleFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.686158303 0.061447906890000001 -1.3711251470000001
		-2.345349718 0.061468521880000002 -0.017031123530000001
		-1.6289574499999999 0.061435433909999997 1.3607562689999999
		0.043365627509999997 0.061368025460000003 1.9551478630000001
		1.6919953350000001 0.061305783480000001 1.4179571230000001
		2.3511867500000001 0.06128516849 0.063863099569999998
		1.634794482 0.061318256459999998 -1.3139242929999999
		-0.037528595579999997 0.061385664919999999 -1.9083158870000001
		-1.686158303 0.061447906890000001 -1.3711251470000001
		-2.345349718 0.061468521880000002 -0.017031123530000001
		-1.6289574499999999 0.061435433909999997 1.3607562689999999
		;
createNode joint -n "FKXMiddleFinger1_L" -p "FKMiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger5_L" -p "FKXMiddleFinger1_L";
	setAttr ".t" -type "double3" -0.6058055831790039 -0.98707658579378998 -0.22319654049490795 ;
	setAttr ".r" -type "double3" 32.544061885548217 7.2737886122639228 174.67014885240692 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger5_L" -p "FKOffsetMiddleFinger5_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKMiddleFinger5_L" -p "FKExtraMiddleFinger5_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger5_LShape" -p "FKMiddleFinger5_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.58292813239999997 -0.0085259121260000004 -0.5853998037
		-0.82443908840000002 -0.0085259121260000004 -0.0023407780460000002
		-0.58292813239999997 -0.0085259121260000004 0.58071824760000001
		0.0001308931915 -0.0085259121260000004 0.82222920359999996
		0.58318991880000004 -0.0085259121260000004 0.58071824760000001
		0.82470087479999998 -0.0085259121260000004 -0.0023407780460000002
		0.58318991880000004 -0.0085259121260000004 -0.5853998037
		0.0001308931915 -0.0085259121260000004 -0.82691075970000005
		-0.58292813239999997 -0.0085259121260000004 -0.5853998037
		-0.82443908840000002 -0.0085259121260000004 -0.0023407780460000002
		-0.58292813239999997 -0.0085259121260000004 0.58071824760000001
		;
createNode joint -n "FKXMiddleFinger5_L" -p "FKMiddleFinger5_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger6_L" -p "FKXMiddleFinger5_L";
	setAttr ".t" -type "double3" 2.6645352591003757e-15 1.0787334918522262 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 24.356478485927564 -4.2536988079981946 1.319745159685731 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger6_L" -p "FKOffsetMiddleFinger6_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -4.4408920985006262e-16 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "FKMiddleFinger6_L" -p "FKExtraMiddleFinger6_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -4.4408920985006262e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger6_LShape" -p "FKMiddleFinger6_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.62956868420000001 -0.0041132630829999999 -0.62956868420000001
		-0.89034457170000003 -0.0041132630829999999 1.4177372819999999e-16
		-0.62956868420000001 -0.0041132630829999999 0.62956868420000001
		-2.5503979669999997e-16 -0.0041132630829999999 0.89034457170000003
		0.62956868420000001 -0.0041132630829999999 0.62956868420000001
		0.89034457170000003 -0.0041132630829999999 1.4177372819999999e-16
		0.62956868420000001 -0.0041132630829999999 -0.62956868420000001
		4.8116559179999993e-16 -0.0041132630829999999 -0.89034457170000003
		-0.62956868420000001 -0.0041132630829999999 -0.62956868420000001
		-0.89034457170000003 -0.0041132630829999999 1.4177372819999999e-16
		-0.62956868420000001 -0.0041132630829999999 0.62956868420000001
		;
createNode joint -n "FKXMiddleFinger6_L" -p "FKMiddleFinger6_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleFinger7_End_L" -p "FKXMiddleFinger6_L";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.6026326255252459 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 177.49809629382253 -18.512511879828114 178.64769755308919 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -177.4982826445989 18.512516051195025 1.352033578879378 ;
createNode joint -n "FKOffsetMiddleFinger2_L" -p "FKXMiddleFinger1_L";
	setAttr ".t" -type "double3" 0.56841026477458634 -0.98585602879046075 -0.48544643327020143 ;
	setAttr ".r" -type "double3" 32.450269727865603 -17.589779339344666 -172.50351722503049 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger2_L" -p "FKOffsetMiddleFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -4.4408920985006262e-16 1.1102230246251565e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKMiddleFinger2_L" -p "FKExtraMiddleFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 4.4408920985006262e-16 -1.1102230246251565e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger2_LShape" -p "FKMiddleFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.58325344850000005 -8.8621162750000004e-17 -0.58325344850000005
		-0.82484493729999997 -7.0691049590000001e-17 -1.903825633e-16
		-0.58325344850000005 -1.6004910969999999e-16 0.58325344850000005
		-2.3901942690000003e-16 -3.0435060340000001e-16 0.82484493729999997
		0.58325344850000005 -4.1906567280000002e-16 0.58325344850000005
		0.82484493729999997 -4.3699578600000004e-16 -1.903825633e-16
		0.58325344850000005 -3.476377259e-16 -0.58325344850000005
		4.4302582490000003e-16 -2.033362322e-16 -0.82484493729999997
		-0.58325344850000005 -8.8621162750000004e-17 -0.58325344850000005
		-0.82484493729999997 -7.0691049590000001e-17 -1.903825633e-16
		-0.58325344850000005 -1.6004910969999999e-16 0.58325344850000005
		;
createNode joint -n "FKXMiddleFinger2_L" -p "FKMiddleFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger3_L" -p "FKXMiddleFinger2_L";
	setAttr ".t" -type "double3" 3.5527136788005009e-15 1.0793649773753775 1.2212453270876722e-15 ;
	setAttr ".r" -type "double3" 24.987379644467058 -5.0918936465266356 -0.51202475760403565 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger3_L" -p "FKOffsetMiddleFinger3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -4.4408920985006262e-16 1.3877787807814457e-17 ;
	setAttr ".ro" 5;
createNode transform -n "FKMiddleFinger3_L" -p "FKExtraMiddleFinger3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -4.4408920985006262e-16 1.3877787807814457e-17 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger3_LShape" -p "FKMiddleFinger3_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.57828001269999996 1.638133953e-16 -0.57828001269999996
		-0.81781143670000001 1.8159061729999999e-16 -1.370750685e-17
		-0.57828001269999996 9.2994518619999996e-17 0.57828001269999996
		-2.3698129430000001e-16 -5.0076507919999997e-17 0.81781143670000001
		0.57828001269999996 -1.638133953e-16 0.57828001269999996
		0.81781143670000001 -1.8159061729999999e-16 -1.370750685e-17
		0.57828001269999996 -9.2994518619999996e-17 -0.57828001269999996
		4.3924811799999999e-16 5.0076507919999997e-17 -0.81781143670000001
		-0.57828001269999996 1.638133953e-16 -0.57828001269999996
		-0.81781143670000001 1.8159061729999999e-16 -1.370750685e-17
		-0.57828001269999996 9.2994518619999996e-17 0.57828001269999996
		;
createNode joint -n "FKXMiddleFinger3_L" -p "FKMiddleFinger3_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleFinger4_End_L" -p "FKXMiddleFinger3_L";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 1.4056735102234925 -2.1510571102112408e-16 ;
	setAttr ".r" -type "double3" 179.73072560417779 8.4973641586388826 178.01649893173453 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -179.73092924272365 -8.4973571834996768 1.983292076701451 ;
createNode joint -n "FKOffsetThumbFinger1_L" -p "FKParentConstraintToWrist_L";
	setAttr ".t" -type "double3" -0.367777602394804 -0.95234201253672435 0.90711995931591183 ;
	setAttr ".r" -type "double3" 157.60565887156042 4.50373965379449 2.6409959491966264 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraThumbFinger1_L" -p "FKOffsetThumbFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 -8.8817841970012523e-16 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKThumbFinger1_L" -p "FKExtraThumbFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999956 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger1_LShape" -p "FKThumbFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.58112328260000001 1.646188282e-16 -0.58112328260000001
		-0.82183242779999999 1.8248345679999999e-16 0
		-0.58112328260000001 9.3451751319999992e-17 0.58112328260000001
		-4.919898947e-16 -5.0322722610000004e-17 0.82183242779999999
		0.58112328260000001 -1.646188282e-16 0.58112328260000001
		0.82183242779999999 -1.8248345679999999e-16 0
		0.58112328260000001 -9.3451751319999992e-17 -0.58112328260000001
		1.8756438240000001e-16 5.0322722610000004e-17 -0.82183242779999999
		-0.58112328260000001 1.646188282e-16 -0.58112328260000001
		-0.82183242779999999 1.8248345679999999e-16 0
		-0.58112328260000001 9.3451751319999992e-17 0.58112328260000001
		;
createNode joint -n "FKXThumbFinger1_L" -p "FKThumbFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetThumbFinger2_L" -p "FKXThumbFinger1_L";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 1.0724462014893916 -2.2204460492503131e-15 ;
	setAttr ".r" -type "double3" 13.06887883526052 0.2885881656356003 -1.2280945864730322 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraThumbFinger2_L" -p "FKOffsetThumbFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKThumbFinger2_L" -p "FKExtraThumbFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger2_LShape" -p "FKThumbFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.59154189940000002 1.6757018200000001e-16 -0.59154189940000002
		-0.83656657680000002 1.8575509500000001e-16 -1.4621340639999999e-16
		-0.59154189940000002 9.5127192659999991e-17 0.59154189940000002
		-2.4241606480000002e-16 -5.1224929020000005e-17 0.83656657680000002
		0.59154189940000002 -1.6757018200000001e-16 0.59154189940000002
		0.83656657680000002 -1.8575509500000001e-16 -1.4621340639999999e-16
		0.59154189940000002 -9.5127192659999991e-17 -0.59154189940000002
		4.4932154029999995e-16 5.1224929020000005e-17 -0.83656657680000002
		-0.59154189940000002 1.6757018200000001e-16 -0.59154189940000002
		-0.83656657680000002 1.8575509500000001e-16 -1.4621340639999999e-16
		-0.59154189940000002 9.5127192659999991e-17 0.59154189940000002
		;
createNode joint -n "FKXThumbFinger2_L" -p "FKThumbFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXThumbFinger3_End_L" -p "FKXThumbFinger2_L";
	setAttr ".t" -type "double3" 5.773159728050814e-15 1.4566019002008423 -2.6645352591003757e-15 ;
	setAttr ".r" -type "double3" 79.427791816981042 74.973010720244133 -78.416235572632914 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -72.947362477438091 -80.258674977789809 -107.92041348450371 ;
createNode parentConstraint -n "FKParentConstraintToWrist_L_parentConstraint1" -p
		 "FKParentConstraintToWrist_L";
	addAttr -ci true -k true -sn "w0" -ln "Wrist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.3620999641953047 -87.728436419995006 0.20659478415326216 ;
	setAttr ".rst" -type "double3" 5.5319444625044687 5.3142896313366581 -0.56733998656272477 ;
	setAttr ".rsrr" -type "double3" 1.3620697783390436 -87.728436319410562 0.20656587932424683 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToAnkle_R" -p "FKSystem";
	setAttr ".ro" 3;
createNode joint -n "FKOffsetMiddleToe1_R" -p "FKParentConstraintToAnkle_R";
	setAttr ".t" -type "double3" 0.020825579278234807 0.15220357689789743 -1.3263746982714688 ;
	setAttr ".r" -type "double3" 108.35551030576904 0.28330165257886331 -179.1462860042181 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -2.0407002965414474e-13 -5.9635400277440939e-16 1.4210240379423905e-15 ;
createNode transform -n "FKExtraMiddleToe1_R" -p "FKOffsetMiddleToe1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -3.3306690738754696e-16 -1.1102230246251565e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999989 ;
createNode transform -n "FKMiddleToe1_R" -p "FKExtraMiddleToe1_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleToe1_RShape" -p "FKMiddleToe1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.83138740909999997 -0.0099768334289999996 -0.83138740909999997
		-1.1757593500000001 -0.0099768334289999996 -9.4956468399999994e-17
		-0.83138740909999997 -0.0099768334289999996 0.83138740909999997
		-3.6109525879999998e-16 -0.0099768334289999996 1.1757593500000001
		0.83138740909999997 -0.0099768334289999996 0.83138740909999997
		1.1757593500000001 -0.0099768334289999996 -9.4956468399999994e-17
		0.83138740909999997 -0.0099768334289999996 -0.83138740909999997
		6.111130259e-16 -0.0099768334289999996 -1.1757593500000001
		-0.83138740909999997 -0.0099768334289999996 -0.83138740909999997
		-1.1757593500000001 -0.0099768334289999996 -9.4956468399999994e-17
		-0.83138740909999997 -0.0099768334289999996 0.83138740909999997
		;
createNode joint -n "FKXMiddleToe1_R" -p "FKMiddleToe1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleToe2_End_R" -p "FKXMiddleToe1_R";
	setAttr ".t" -type "double3" -5.773159728050814e-15 1.4371052628296204 -6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" -70.125275891486012 4.5685318950170471e-06 179.99994509444107 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 71.642383655791107 -6.0205788377102522e-06 -179.9999452346413 ;
createNode parentConstraint -n "FKParentConstraintToAnkle_R_parentConstraint1" -p
		 "FKParentConstraintToAnkle_R";
	addAttr -ci true -k true -sn "w0" -ln "Ankle_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -180.00000354645681 -0.89954494380165206 -9.9879030189539699e-07 ;
	setAttr ".rst" -type "double3" -1.9235993325710721 0.66881483793257113 -0.23313114047050829 ;
	setAttr ".rsrr" -type "double3" 180.00000000000003 -0.8995357678227105 -1.0456783262843828e-13 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToPelvis_M" -p "FKSystem";
	setAttr ".ro" 3;
createNode joint -n "FKOffsetHipTwist_R" -p "FKParentConstraintToPelvis_M";
	setAttr ".t" -type "double3" -1.2119851708412173 0.058133125305176669 -0.0001863837242122024 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 205.1395817586988 90.000002504478161 ;
createNode transform -n "FKExtraHipTwist_R" -p "FKOffsetHipTwist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 2.2204460492503131e-16 ;
	setAttr ".ro" 2;
createNode transform -n "FKHipTwist_R" -p "FKExtraHipTwist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHipTwist_RShape" -p "FKHipTwist_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.30692569990000002 8.6944974580000007e-17 -0.30692569990000002
		-0.43405848749999998 9.638034535999999e-17 0
		-0.30692569990000002 4.9357416980000005e-17 0.30692569990000002
		-1.2577929040000001e-16 -2.657841687e-17 0.43405848749999998
		0.30692569990000002 -8.6944974580000007e-17 0.30692569990000002
		0.43405848749999998 -9.638034535999999e-17 0
		0.30692569990000002 -4.9357416980000005e-17 -0.30692569990000002
		2.3313366039999999e-16 2.657841687e-17 -0.43405848749999998
		-0.30692569990000002 8.6944974580000007e-17 -0.30692569990000002
		-0.43405848749999998 9.638034535999999e-17 0
		-0.30692569990000002 4.9357416980000005e-17 0.30692569990000002
		;
createNode joint -n "FKXHipTwist_R" -p "FKHipTwist_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHip_R" -p "FKXHipTwist_R";
	setAttr ".t" -type "double3" -0.088520804836134559 0.745294868946075 -0.041540897564236623 ;
	setAttr ".r" -type "double3" 1.0589532759707174 -0.72669178688457137 -90.175490554823298 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraHip_R" -p "FKOffsetHip_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 -2.2204460492503131e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKHip_R" -p "FKExtraHip_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 8.8817841970012523e-16 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHip_RShape" -p "FKHip_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.60505117819999998 1.7139704929999998e-16 -0.60505117819999998
		-0.85567158219999995 1.899972584e-16 0
		-0.60505117819999998 9.729965039e-17 0.60505117819999998
		-2.4795221730000001e-16 -5.239477321e-17 0.85567158219999995
		0.60505117819999998 -1.7139704929999998e-16 0.60505117819999998
		0.85567158219999995 -1.899972584e-16 0
		0.60505117819999998 -9.729965039e-17 -0.60505117819999998
		4.5958287599999996e-16 5.239477321e-17 -0.85567158219999995
		-0.60505117819999998 1.7139704929999998e-16 -0.60505117819999998
		-0.85567158219999995 1.899972584e-16 0
		-0.60505117819999998 9.729965039e-17 0.60505117819999998
		;
createNode joint -n "FKXHip_R" -p "FKHip_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetKnee_R" -p "FKXHip_R";
	setAttr ".t" -type "double3" 2.2204460492503131e-15 2.2734843510971174 -6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" 22.919761854393723 -6.089449925595488e-14 -3.0038248718465602e-13 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraKnee_R" -p "FKOffsetKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 -2.7755575615628914e-17 ;
	setAttr ".ro" 2;
createNode transform -n "FKKnee_R" -p "FKExtraKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 -2.7755575615628914e-17 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKKnee_RShape" -p "FKKnee_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.68215344929999999 1.9323834509999999e-16 -0.68215344929999999
		-0.96471065960000002 2.1420879730000002e-16 0
		-0.68215344929999999 1.0969864119999999e-16 0.68215344929999999
		-2.7954901399999999e-16 -5.9071491070000005e-17 0.96471065960000002
		0.68215344929999999 -1.9323834509999999e-16 0.68215344929999999
		0.96471065960000002 -2.1420879730000002e-16 0
		0.68215344929999999 -1.0969864119999999e-16 -0.68215344929999999
		5.1814797720000006e-16 5.9071491070000005e-17 -0.96471065960000002
		-0.68215344929999999 1.9323834509999999e-16 -0.68215344929999999
		-0.96471065960000002 2.1420879730000002e-16 0
		-0.68215344929999999 1.0969864119999999e-16 0.68215344929999999
		;
createNode joint -n "FKXKnee_R" -p "FKKnee_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetAnkle_R" -p "FKXKnee_R";
	setAttr ".t" -type "double3" 1.2434497875801753e-14 2.6670582522596766 1.6653345369377348e-16 ;
	setAttr ".r" -type "double3" 1.4900974413835479 -0.0077695994812840199 0.59745911162008236 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraAnkle_R" -p "FKOffsetAnkle_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode transform -n "FKAnkle_R" -p "FKExtraAnkle_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKAnkle_RShape" -p "FKAnkle_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.42124664839999998 1.193294636e-16 -0.42124664839999998
		-0.59573272320000004 1.3227923720000001e-16 0
		-0.42124664839999998 6.7741627640000003e-17 0.42124664839999998
		-1.726284391e-16 -3.647810863e-17 0.59573272320000004
		0.42124664839999998 -1.193294636e-16 0.42124664839999998
		0.59573272320000004 -1.3227923720000001e-16 0
		0.42124664839999998 -6.7741627640000003e-17 -0.42124664839999998
		3.199692078e-16 3.647810863e-17 -0.59573272320000004
		-0.42124664839999998 1.193294636e-16 -0.42124664839999998
		-0.59573272320000004 1.3227923720000001e-16 0
		-0.42124664839999998 6.7741627640000003e-17 0.42124664839999998
		;
createNode joint -n "FKXAnkle_R" -p "FKAnkle_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToAnkle_R" -p "FKXAnkle_R";
	setAttr ".r" -type "double3" -180 -0.89953576782276534 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode joint -n "FKXHeel_End_R" -p "FKXAnkle_R";
	setAttr ".t" -type "double3" -0.023211789696358176 0.62094301335498636 1.478349566682617 ;
	setAttr ".r" -type "double3" 59.095245418304927 0 -180 ;
	setAttr ".jo" -type "double3" -180 -0.89953576782276534 -1.1017041183487609e-16 ;
createNode joint -n "FKOffsetHipTwist_L" -p "FKParentConstraintToPelvis_M";
	setAttr ".t" -type "double3" 1.2119850516319279 0.058133125305176669 -0.00018638372421175831 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.0509930133114857e-06 25.139577240952878 90.000002504478161 ;
createNode transform -n "FKExtraHipTwist_L" -p "FKOffsetHipTwist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 1.0495830448829606e-13 1.5294185322743799e-29 1.6697912077683464e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKHipTwist_L" -p "FKExtraHipTwist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 0.99999999999999933 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHipTwist_LShape" -p "FKHipTwist_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.30692569990000002 8.6944974580000007e-17 -0.30692569990000002
		-0.43405848749999998 9.638034535999999e-17 0
		-0.30692569990000002 4.9357416980000005e-17 0.30692569990000002
		-1.2577929040000001e-16 -2.657841687e-17 0.43405848749999998
		0.30692569990000002 -8.6944974580000007e-17 0.30692569990000002
		0.43405848749999998 -9.638034535999999e-17 0
		0.30692569990000002 -4.9357416980000005e-17 -0.30692569990000002
		2.3313366039999999e-16 2.657841687e-17 -0.43405848749999998
		-0.30692569990000002 8.6944974580000007e-17 -0.30692569990000002
		-0.43405848749999998 9.638034535999999e-17 0
		-0.30692569990000002 4.9357416980000005e-17 0.30692569990000002
		;
createNode joint -n "FKXHipTwist_L" -p "FKHipTwist_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHip_L" -p "FKXHipTwist_L";
	setAttr ".t" -type "double3" 0.088520804836134559 -0.74529486894607433 0.041540897564235735 ;
	setAttr ".r" -type "double3" 1.0589532759707174 -0.72669178688457137 -90.175490554823298 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraHip_L" -p "FKOffsetHip_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 3.1824959379829642e-15 -3.727212517340058e-16 -1.2424041724466857e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKHip_L" -p "FKExtraHip_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHip_LShape" -p "FKHip_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.60505117819999998 1.7139704929999998e-16 -0.60505117819999998
		-0.85567158219999995 1.899972584e-16 0
		-0.60505117819999998 9.729965039e-17 0.60505117819999998
		-2.4795221730000001e-16 -5.239477321e-17 0.85567158219999995
		0.60505117819999998 -1.7139704929999998e-16 0.60505117819999998
		0.85567158219999995 -1.899972584e-16 0
		0.60505117819999998 -9.729965039e-17 -0.60505117819999998
		4.5958287599999996e-16 5.239477321e-17 -0.85567158219999995
		-0.60505117819999998 1.7139704929999998e-16 -0.60505117819999998
		-0.85567158219999995 1.899972584e-16 0
		-0.60505117819999998 9.729965039e-17 0.60505117819999998
		;
createNode joint -n "FKXHip_L" -p "FKHip_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetKnee_L" -p "FKXHip_L";
	setAttr ".t" -type "double3" -1.9984014443252818e-15 -2.2734843510971192 -6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" 22.919761854393723 -6.089449925595488e-14 -3.0038248718465602e-13 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraKnee_L" -p "FKOffsetKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 2.7755575615628914e-17 ;
	setAttr ".r" -type "double3" -1.9645515976813228e-16 -3.9912234039849796e-16 -6.2120208622334312e-18 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "FKKnee_L" -p "FKExtraKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 8.3266726846886741e-17 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999944 1 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKKnee_LShape" -p "FKKnee_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.68215344929999999 1.9323834509999999e-16 -0.68215344929999999
		-0.96471065960000002 2.1420879730000002e-16 0
		-0.68215344929999999 1.0969864119999999e-16 0.68215344929999999
		-2.7954901399999999e-16 -5.9071491070000005e-17 0.96471065960000002
		0.68215344929999999 -1.9323834509999999e-16 0.68215344929999999
		0.96471065960000002 -2.1420879730000002e-16 0
		0.68215344929999999 -1.0969864119999999e-16 -0.68215344929999999
		5.1814797720000006e-16 5.9071491070000005e-17 -0.96471065960000002
		-0.68215344929999999 1.9323834509999999e-16 -0.68215344929999999
		-0.96471065960000002 2.1420879730000002e-16 0
		-0.68215344929999999 1.0969864119999999e-16 0.68215344929999999
		;
createNode joint -n "FKXKnee_L" -p "FKKnee_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetAnkle_L" -p "FKXKnee_L";
	setAttr ".t" -type "double3" -1.2434497875801753e-14 -2.6670582522596762 -7.4940054162198066e-16 ;
	setAttr ".r" -type "double3" 1.4900974413835479 -0.0077695994812840199 0.59745911162008236 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraAnkle_L" -p "FKOffsetAnkle_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -1.1102230246251565e-16 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 0.99999999999999978 ;
createNode transform -n "FKAnkle_L" -p "FKExtraAnkle_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKAnkle_LShape" -p "FKAnkle_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.42124664839999998 1.193294636e-16 -0.42124664839999998
		-0.59573272320000004 1.3227923720000001e-16 0
		-0.42124664839999998 6.7741627640000003e-17 0.42124664839999998
		-1.726284391e-16 -3.647810863e-17 0.59573272320000004
		0.42124664839999998 -1.193294636e-16 0.42124664839999998
		0.59573272320000004 -1.3227923720000001e-16 0
		0.42124664839999998 -6.7741627640000003e-17 -0.42124664839999998
		3.199692078e-16 3.647810863e-17 -0.59573272320000004
		-0.42124664839999998 1.193294636e-16 -0.42124664839999998
		-0.59573272320000004 1.3227923720000001e-16 0
		-0.42124664839999998 6.7741627640000003e-17 0.42124664839999998
		;
createNode joint -n "FKXAnkle_L" -p "FKAnkle_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToAnkle_L" -p "FKXAnkle_L";
	setAttr ".r" -type "double3" 0 -0.89953576782276512 0 ;
	setAttr ".ro" 3;
createNode joint -n "FKXHeel_End_L" -p "FKXAnkle_L";
	setAttr ".t" -type "double3" 0.023211789696358176 -0.62094301335498658 -1.4783495666826165 ;
	setAttr ".r" -type "double3" 59.095245418304913 180 0 ;
	setAttr ".jo" -type "double3" 0 -0.89953576782276512 0 ;
createNode parentConstraint -n "FKParentConstraintToPelvis_M_parentConstraint1" -p
		 "FKParentConstraintToPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 5.9604644775390625e-08 5.2490606307983398 -1.2424887120723729 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToAnkle_L" -p "FKSystem";
	setAttr ".ro" 3;
createNode joint -n "FKOffsetMiddleToe1_L" -p "FKParentConstraintToAnkle_L";
	setAttr ".t" -type "double3" -0.020825579278234807 -0.15220357689789743 1.3263746982714686 ;
	setAttr ".r" -type "double3" 108.35551145034458 0.28330165257886331 -179.1462860042181 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -2.0407002965414474e-13 -5.9635400277440939e-16 1.4210240379423905e-15 ;
createNode transform -n "FKExtraMiddleToe1_L" -p "FKOffsetMiddleToe1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 1.1102230246251565e-16 0 ;
	setAttr ".r" -type "double3" 1.2722218725854064e-14 -1.2223212671103346e-14 1.7246426240106738e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
createNode transform -n "FKMiddleToe1_L" -p "FKExtraMiddleToe1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 -1.1102230246251565e-16 1.1102230246251565e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleToe1_LShape" -p "FKMiddleToe1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.83138740909999997 2.3551288529999998e-16 -0.83138740909999997
		-1.1757593500000001 2.6107102029999999e-16 -9.8184652189999999e-17
		-0.83138740909999997 1.336972923e-16 0.83138740909999997
		-5.3707494869999999e-16 -7.1994496209999998e-17 1.1757593500000001
		0.83138740909999997 -2.3551288529999998e-16 0.83138740909999997
		1.1757593500000001 -2.6107102029999999e-16 -9.8184652189999999e-17
		0.83138740909999997 -1.336972923e-16 -0.83138740909999997
		4.3513333599999999e-16 7.1994496209999998e-17 -1.1757593500000001
		-0.83138740909999997 2.3551288529999998e-16 -0.83138740909999997
		-1.1757593500000001 2.6107102029999999e-16 -9.8184652189999999e-17
		-0.83138740909999997 1.336972923e-16 0.83138740909999997
		;
createNode joint -n "FKXMiddleToe1_L" -p "FKMiddleToe1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleToe2_End_L" -p "FKXMiddleToe1_L";
	setAttr ".t" -type "double3" 6.2172489379008766e-15 -1.4371052628296201 -6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" 109.87472410851403 4.568531895017048e-06 179.99994509444107 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 71.642383655791051 180.00000602057884 5.4765358688844145e-05 ;
createNode parentConstraint -n "FKParentConstraintToAnkle_L_parentConstraint1" -p
		 "FKParentConstraintToAnkle_L";
	addAttr -ci true -k true -sn "w0" -ln "Ankle_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.9069042498246937e-06 0.89952744593743506 -1.0635553334776454e-06 ;
	setAttr ".rst" -type "double3" 1.9235993325710723 0.66881483793257068 -0.23313114047050804 ;
	setAttr ".rsrr" -type "double3" 1.3269005234514088e-14 0.89953576782269495 5.140616483593336e-14 ;
	setAttr -k on ".w0";
createNode transform -n "PelvisCenterBtwLegsBlended_M" -p "FKSystem";
	setAttr ".ro" 3;
createNode transform -n "CenterOffset_M" -p "PelvisCenterBtwLegsBlended_M";
createNode transform -n "CenterExtra_M" -p "CenterOffset_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Center_M" -p "CenterExtra_M";
	addAttr -ci true -k true -sn "CenterBtwFeet" -ln "CenterBtwFeet" -min 0 -max 10 
		-at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 4;
	setAttr -k on ".CenterBtwFeet";
createNode nurbsCurve -n "Center_MShape" -p "Center_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 46 0 no 3
		47 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46
		47
		-2.7174872329999999 -0.06143237788 2.0067406049999999e-05
		-2.8904847359999999 -0.06143237788 0.5324528114
		-3.3433992730000002 -0.06143237788 0.86151560459999998
		-3.903233347 -0.06143237788 0.86151560459999998
		-4.3561478850000004 -0.06143237788 0.5324528114
		-4.5291453859999997 -0.06143237788 2.0067406049999999e-05
		-4.3561478850000004 -0.06143237788 -0.53241267660000002
		-3.903233347 -0.06143237788 -0.8614754698
		-3.3433992730000002 -0.06143237788 -0.8614754698
		-2.8904847359999999 -0.06143237788 -0.53241267660000002
		-2.7174872329999999 -0.06143237788 2.0067406049999999e-05
		-2.3394071020000001e-09 -0.06143237788 2.0067406049999999e-05
		2.717487228 -0.06143237788 2.0067406049999999e-05
		2.8904847309999999 -0.06143237788 0.5324528114
		3.3433992680000002 -0.06143237788 0.86151560459999998
		3.9032333420000001 -0.06143237788 0.86151560459999998
		4.3561478810000001 -0.06143237788 0.5324528114
		4.5291453820000003 -0.06143237788 2.0067406049999999e-05
		4.3561478810000001 -0.06143237788 -0.53241267660000002
		3.9032333420000001 -0.06143237788 -0.8614754698
		3.3433992680000002 -0.06143237788 -0.8614754698
		2.8904847309999999 -0.06143237788 -0.53241267660000002
		2.717487228 -0.06143237788 2.0067406049999999e-05
		-2.3394071020000001e-09 -0.06143237788 2.0067406049999999e-05
		-2.3394071020000001e-09 -0.06143237788 -2.7174671629999998
		-0.53243274630000004 -0.06143237788 -2.8904646660000002
		-0.86149553960000003 -0.06143237788 -3.343379203
		-0.86149553960000003 -0.06143237788 -3.9032132769999999
		-0.53243274630000004 -0.06143237788 -4.3561278159999999
		-2.3394071020000001e-09 -0.06143237788 -4.529125316
		0.53243274169999999 -0.06143237788 -4.3561278159999999
		0.86149553489999997 -0.06143237788 -3.9032132769999999
		0.86149553489999997 -0.06143237788 -3.343379203
		0.53243274169999999 -0.06143237788 -2.8904646660000002
		-2.3394071020000001e-09 -0.06143237788 -2.7174671629999998
		-2.3394071020000001e-09 -0.06143237788 2.0067406049999999e-05
		-2.3394071020000001e-09 -0.06143237788 2.7175072980000001
		-0.53243274630000004 -0.06143237788 2.8905048010000001
		-0.86149553960000003 -0.06143237788 3.3434193379999999
		-0.86149553960000003 -0.06143237788 3.9032534120000002
		-0.53243274630000004 -0.06143237788 4.3561679499999997
		-2.3394071020000001e-09 -0.06143237788 4.5291654509999999
		0.53243274169999999 -0.06143237788 4.3561679499999997
		0.86149553489999997 -0.06143237788 3.9032534120000002
		0.86149553489999997 -0.06143237788 3.3434193379999999
		0.53243274169999999 -0.06143237788 2.8905048010000001
		-2.3394071020000001e-09 -0.06143237788 2.7175072980000001
		;
createNode joint -n "FKOffsetPelvis_M" -p "Center_M";
	setAttr ".t" -type "double3" 5.9604644775390625e-08 0 0 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraPelvis_M" -p "FKOffsetPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode transform -n "FKPelvis_M" -p "FKExtraPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode nurbsCurve -n "FKPelvis_MShape" -p "FKPelvis_M";
	setAttr -l on -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.0679459122030384 3.0252445530852347e-16 -1.0679459122030406
		-1.5103035929184445 3.3535476460643132e-16 0
		-1.067945912203039 1.7173880100432833e-16 1.0679459122030392
		-4.3764819641519768e-16 -9.2479423040415912e-17 1.5103035929184445
		1.0679459122030388 -3.0252445530852347e-16 1.0679459122030392
		1.5103035929184445 -3.3535476460643142e-16 0
		1.067945912203039 -1.7173880100432845e-16 -1.0679459122030381
		8.1118700595776965e-16 9.2479423040415825e-17 -1.5103035929184445
		-1.0679459122030384 3.0252445530852347e-16 -1.0679459122030406
		-1.5103035929184445 3.3535476460643132e-16 0
		-1.067945912203039 1.7173880100432833e-16 1.0679459122030392
		;
createNode transform -n "HipSwingerGroupPelvis_M" -p "FKPelvis_M";
	setAttr ".t" -type "double3" 0 0.92606592178344727 -0.00051128792762744624 ;
createNode orientConstraint -n "HipSwingerGroupPelvis_M_orientConstraint1" -p "HipSwingerGroupPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "HipSwingerPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode joint -n "FKXPelvis_M" -p "HipSwingerGroupPelvis_M";
	setAttr ".t" -type "double3" 0 -0.92606592178344727 0.00051128792762744624 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "HipSwingerStabalizePelvis_M" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 0 0.92606592178344727 -0.00051128792762744624 ;
createNode joint -n "FKOffsetSpineA_M" -p "HipSwingerStabalizePelvis_M";
createNode transform -n "FKExtraSpineA_M" -p "FKOffsetSpineA_M";
	setAttr -l on -k off ".v";
createNode transform -n "FKSpineA_M" -p "FKExtraSpineA_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKSpineA_MShape" -p "FKSpineA_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.6327450939999999 -1.6071861970000002e-14 -1.6327450939999999
		-2.3090502549999998 -1.602166886e-14 0
		-1.6327450939999999 -1.6271815580000001e-14 1.6327450939999999
		-6.6910499610000003e-16 -1.6675769559999999e-14 2.3090502549999998
		1.6327450939999999 -1.699690006e-14 1.6327450939999999
		2.3090502549999998 -1.7047093170000002e-14 0
		1.6327450939999999 -1.6796946450000001e-14 -1.6327450939999999
		1.2401953960000001e-15 -1.6392992460000001e-14 -2.3090502549999998
		-1.6327450939999999 -1.6071861970000002e-14 -1.6327450939999999
		-2.3090502549999998 -1.602166886e-14 0
		-1.6327450939999999 -1.6271815580000001e-14 1.6327450939999999
		;
createNode joint -n "FKXSpineA_M" -p "FKSpineA_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetChest_M" -p "FKXSpineA_M";
	setAttr ".t" -type "double3" 0 1.3383469581604004 0 ;
createNode transform -n "FKExtraChest_M" -p "FKOffsetChest_M";
	setAttr -l on -k off ".v";
createNode transform -n "FKChest_M" -p "FKExtraChest_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKChest_MShape" -p "FKChest_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.211824434 6.2655886819999994e-16 -2.211824434
		-3.1279921119999998 6.9455377260000008e-16 0
		-2.211824434 3.5568849689999998e-16 2.211824434
		-9.0641385769999987e-16 -1.915342764e-16 3.1279921119999998
		2.211824434 -6.2655886819999994e-16 2.211824434
		3.1279921119999998 -6.9455377260000008e-16 0
		2.211824434 -3.5568849689999998e-16 -2.211824434
		1.680050665e-15 1.915342764e-16 -3.1279921119999998
		-2.211824434 6.2655886819999994e-16 -2.211824434
		-3.1279921119999998 6.9455377260000008e-16 0
		-2.211824434 3.5568849689999998e-16 2.211824434
		;
createNode joint -n "FKXChest_M" -p "FKChest_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToChest_M" -p "FKXChest_M";
createNode orientConstraint -n "HipSwingerStabalizePelvis_M_orientConstraint1" -p
		 "HipSwingerStabalizePelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "HipSwingerOffsetPelvis_M" -p "Center_M";
	setAttr ".t" -type "double3" 5.9604644775390625e-08 0.92606592178344727 -0.00051128792762744624 ;
createNode transform -n "HipSwingerPelvis_M" -p "HipSwingerOffsetPelvis_M";
	setAttr -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "HipSwingerPelvis_MShape" -p "HipSwingerPelvis_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-3.394147834 -0.52916095929999996 -2.6258744470000002
		-2.4000249490000001 -0.52916095929999996 1.0751535640000002e-15
		-3.394147834 -0.52916095929999996 2.6258744470000002
		-6.9546718639999998e-16 -0.52916095929999996 1.856773628
		3.394147834 -0.52916095929999996 2.6258744470000002
		2.4000249490000001 -0.52916095929999996 1.0751535640000002e-15
		3.394147834 -0.52916095929999996 -2.6258744470000002
		1.2890580819999999e-15 -0.52916095929999996 -1.856773628
		-3.394147834 -0.52916095929999996 -2.6258744470000002
		-2.4000249490000001 -0.52916095929999996 1.0751535640000002e-15
		-3.394147834 -0.52916095929999996 2.6258744470000002
		;
createNode parentConstraint -n "PelvisCenterBtwLegsBlended_M_parentConstraint1" -p
		 "PelvisCenterBtwLegsBlended_M";
	addAttr -ci true -k true -sn "w0" -ln "PelvisCenter_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PelvisCenterBtwLegsOffset_MW1" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0 5.2490606307983398 -1.2424887120723729 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKSystem" -p "MotionSystem";
createNode transform -n "IKParentConstraint" -p "IKSystem";
createNode transform -n "IKParentConstraintShoulder1_R" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 1 ;
createNode transform -n "IKOffsetShoulder1_R" -p "IKParentConstraintShoulder1_R";
	setAttr ".t" -type "double3" 1.1379786002407855e-15 1.4383041024566319 3.5527136788005009e-15 ;
	setAttr ".r" -type "double3" -42.090614547380056 -0.31829956662700554 -0.17791967310599252 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
createNode joint -n "IKXShoulder1_R" -p "IKOffsetShoulder1_R";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 1.3322676295501878e-15 0 ;
	setAttr ".r" -type "double3" 8.0446283892441981e-15 -1.1310794283658302e-14 2.0704405985954376e-15 ;
	setAttr ".ro" 5;
createNode joint -n "IKXElbow1_R" -p "IKXShoulder1_R";
	setAttr ".t" -type "double3" -0.22499009467595976 4.0969668616941837 0.21896610692667728 ;
	setAttr ".r" -type "double3" -8.454365486806076e-08 -5.7701316390939092e-07 -2.9048835949417447e-06 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -2.4432342250812544 -2.9766419037382197 94.683067067384044 ;
createNode joint -n "IKXWrist2_R" -p "IKXElbow1_R";
	setAttr ".t" -type "double3" 0.15534239695192142 2.7109514989648087 -0.86113381767890651 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -97.185969398167714 76.527857347789052 -95.000001505250978 ;
createNode joint -n "IKXWrist1_R" -p "IKXWrist2_R";
	setAttr ".t" -type "double3" -0.08555830633342687 0.37731943629396936 0.017482291770028358 ;
	setAttr ".ro" 5;
createNode transform -n "IKFKAlignedArm1_R" -p "IKXWrist2_R";
	setAttr ".t" -type "double3" -2.6348445913981777e-07 -3.2458732920903621e-07 1.1289069201669832e-07 ;
	setAttr ".r" -type "double3" 3.2087055368768816e-06 -2.2757684066710257e-06 1.8341362808497295e-06 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999978 0.99999999999999967 ;
createNode orientConstraint -n "IKXWrist2_R_orientConstraint1" -p "IKXWrist2_R";
	addAttr -ci true -k true -sn "w0" -ln "IKArm1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -87.347228752337742 -12.762701570497656 54.03239234646967 ;
	setAttr ".o" -type "double3" 85.661377108963038 -53.36726075139466 -16.259959204746536 ;
	setAttr ".rsrr" -type "double3" -2.3854160110976374e-14 -7.2829453917827619e-30 
		3.4986101496098681e-14 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector1" -p "IKXElbow1_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationArm1_R" -p "IKXElbow1_R";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -5.5511151231257827e-17 -1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" 90.815455161867206 34.909745022997335 179.8879613755702 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999956 ;
createNode annotationShape -n "PoleAnnotationArm1_RShape" -p "PoleAnnotationArm1_R";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintShoulder1_R_parentConstraint1" 
		-p "IKParentConstraintShoulder1_R";
	addAttr -ci true -k true -sn "w0" -ln "Clavicle_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -85.918063309174499 90.000002846135587 -5.0687110382607354 ;
	setAttr ".rst" -type "double3" -1.2105141281160527 9.5356795691081118 -0.36664309270135842 ;
	setAttr ".rsrr" -type "double3" -85.918064939960644 90.000004878168184 -5.0687097852427323 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintShoulder_L" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "IKOffsetShoulder_L" -p "IKParentConstraintShoulder_L";
	setAttr ".t" -type "double3" -0.12707448618352024 -1.4290493964414877 -0.10198145921310697 ;
	setAttr ".r" -type "double3" -37.917967667764358 3.0775205889427411 -3.9425589618850587 ;
	setAttr ".ro" 5;
createNode joint -n "IKXShoulder_L" -p "IKOffsetShoulder_L";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" -4.1486450323287716e-08 1.1461049123026046e-06 -3.7759934610320488e-07 ;
	setAttr ".ro" 5;
createNode joint -n "IKXElbow_L" -p "IKXShoulder_L";
	setAttr ".t" -type "double3" 0.24481092855440295 -4.5159835214034949 -0.14617960662576657 ;
	setAttr ".r" -type "double3" -4.2764100601369262e-08 2.0695683859600888e-08 -3.9005829512054682e-06 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -93.916480785887671 -31.61220210161045 95.454454971866951 ;
createNode joint -n "IKXWrist_L" -p "IKXElbow_L";
	setAttr ".t" -type "double3" 0.57694609555033427 -1.730107068039775 -0.012315717006032179 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 17.197427660209748 92.095231334271276 10.897917721723953 ;
createNode transform -n "IKFKAlignedArm_L" -p "IKXWrist_L";
	setAttr ".t" -type "double3" 1.66211187591081e-08 1.5449150403412659e-07 -1.1686869783034126e-07 ;
	setAttr ".r" -type "double3" 1.636672431618113e-06 0 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999933 0.99999999999999933 ;
createNode orientConstraint -n "IKXWrist_L_orientConstraint1" -p "IKXWrist_L";
	addAttr -ci true -k true -sn "w0" -ln "IKArm_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -26.975951489773223 87.451656945888729 26.944844460700569 ;
	setAttr ".o" -type "double3" 1.3620770473986858 -87.728436214478691 0.20657169520007104 ;
	setAttr ".rsrr" -type "double3" 2.2860236773019026e-15 2.251857562559618e-17 -5.9635400277440939e-16 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector2" -p "IKXElbow_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationArm_L" -p "IKXElbow_L";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 0 ;
	setAttr ".r" -type "double3" 179.46373756655299 0.26980894125014204 173.72241485460125 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999967 0.99999999999999978 ;
createNode annotationShape -n "PoleAnnotationArm_LShape" -p "PoleAnnotationArm_L";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintShoulder_L_parentConstraint1" -p
		 "IKParentConstraintShoulder_L";
	addAttr -ci true -k true -sn "w0" -ln "Clavicle1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 90.000002527663426 -89.999996251050433 7.2095185075418087e-08 ;
	setAttr ".rst" -type "double3" 1.2105100000000002 9.53568 -0.36664299999999916 ;
	setAttr ".rsrr" -type "double3" 89.999999616644317 -89.999996197111585 -4.7536104765513104e-07 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintHip_R" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode transform -n "IKOffsetHip_R" -p "IKParentConstraintHip_R";
	setAttr ".t" -type "double3" -0.088520804836132783 0.745294868946075 -0.041540897564235957 ;
	setAttr ".r" -type "double3" 1.0589532311776424 -0.72669176883990994 -90.175490857600821 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000002 ;
createNode joint -n "IKXHip_R" -p "IKOffsetHip_R";
	setAttr ".t" -type "double3" 0 -1.7763568394002505e-15 -2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" -3.9977551076053762e-15 1.4443653435488905e-06 3.1717041390413773e-07 ;
	setAttr ".ro" 2;
createNode joint -n "IKXKnee_R" -p "IKXHip_R";
	setAttr ".t" -type "double3" 2.2204460492503131e-15 2.273484468460083 0 ;
	setAttr ".r" -type "double3" 22.919769686491868 -6.0894506905336978e-14 -3.0038265101492385e-13 ;
	setAttr ".ro" 2;
	setAttr ".pa" -type "double3" 22.919761854393723 -6.089449925595488e-14 -3.0038248718465602e-13 ;
createNode joint -n "IKXAnkle_R" -p "IKXKnee_R";
	setAttr ".t" -type "double3" 1.2434497875801753e-14 2.6670582294464111 1.6653345369377348e-16 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" 1.4900974413835479 -0.0077695994812840199 0.59745911162008236 ;
createNode joint -n "IKXMiddleToe1_R" -p "IKXAnkle_R";
	setAttr ".t" -type "double3" 0.020825579278234807 0.15220357689789743 -1.3263746982714688 ;
	setAttr ".r" -type "double3" 108.35551030576904 0.2833035846914867 -179.1462769787673 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -2.0407002965414474e-13 -5.9635400277440939e-16 1.4210240379423905e-15 ;
	setAttr ".pa" -type "double3" 108.35550752148379 0.28330165257886331 -179.1462860042181 ;
createNode joint -n "IKXMiddleToe2_End_R" -p "IKXMiddleToe1_R";
	setAttr ".t" -type "double3" -5.9952043329758453e-15 1.4371052628296197 -7.7715611723760958e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -1.5171077446214807 -3.9348917796465742e-13 -7.3814052773437456e-14 ;
createNode ikEffector -n "effector4" -p "IKXMiddleToe1_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "IKFKAlignedLeg_R" -p "IKXAnkle_R";
	setAttr ".t" -type "double3" 1.8198378515243974e-07 -1.6354755993930326e-08 -2.1597947558182895e-07 ;
	setAttr ".r" -type "double3" -2.6665377509049473e-06 1.1299600123008416e-29 -2.2894772666090046e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode orientConstraint -n "IKXAnkle_R_orientConstraint1" -p "IKXAnkle_R";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -178.50071434565368 -0.90704662469322661 0.57399757787259431 ;
	setAttr ".o" -type "double3" 0 -0.89953576921938305 -1.8513264841536972e-25 ;
	setAttr ".rsrr" -type "double3" 1.4900974413871593 -0.0077695998277200065 0.59745912493718012 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector5" -p "IKXAnkle_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector3" -p "IKXKnee_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_R" -p "IKXKnee_R";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 5.5511151231257827e-17 ;
	setAttr ".r" -type "double3" -178.50979672665321 -0.90700076186601009 0.57406674412537884 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000002 1.0000000000000002 ;
createNode annotationShape -n "PoleAnnotationLeg_RShape" -p "PoleAnnotationLeg_R";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintHip_R_parentConstraint1" -p "IKParentConstraintHip_R";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 154.86041824130123 1.526666247102488e-13 89.999997732757237 ;
	setAttr ".rst" -type "double3" -1.2119851112365725 5.3071937561035165 -1.2426750957965851 ;
	setAttr ".rsrr" -type "double3" 154.8604209670936 0 90.000000000000242 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintPelvis_M" -p "IKParentConstraint";
	setAttr ".ro" 3;
createNode joint -n "IKXPelvis_M" -p "IKParentConstraintPelvis_M";
	setAttr ".t" -type "double3" 1.0587911840678754e-22 8.8817841970012523e-15 -2.2204460492503131e-15 ;
	setAttr ".ro" 3;
createNode joint -n "IKXSpineA_M" -p "IKXPelvis_M";
	setAttr ".t" -type "double3" 0 0.92606592178344727 -0.00051128792762744624 ;
createNode joint -n "IKXChest_M" -p "IKXSpineA_M";
createNode transform -n "IKFKAlignedSpine_M" -p "IKXChest_M";
	setAttr ".t" -type "double3" -1.0587911840678754e-22 -8.8817841970012523e-15 2.2204460492503131e-15 ;
createNode parentConstraint -n "IKXChest_M_parentConstraint1" -p "IKXChest_M";
	addAttr -ci true -k true -sn "w0" -ln "IKfake2Spine_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKSpine4AlignTo_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0 1.3383469581604004 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode aimConstraint -n "IKXSpineA_M_aimConstraint1" -p "IKXSpineA_M";
	addAttr -ci true -sn "w0" -ln "IKfake2Spine_MW0" -dv 1 -at "double";
	addAttr -ci true -sn "w1" -ln "IKSpine4_MW1" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 0 0 1 ;
	setAttr ".wut" 1;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "IKfake1Spine_M" -p "IKXPelvis_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0 0.92606592178344727 -0.00051128792762744624 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "IKfake2Spine_M" -p "IKfake1Spine_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode ikEffector -n "effector6" -p "IKfake1Spine_M";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode transform -n "IKFake1UpLocSpine_M" -p "IKfake1Spine_M";
	setAttr ".t" -type "double3" 0 1 0 ;
createNode locator -n "IKFake1UpLocSpine_MShape" -p "IKFake1UpLocSpine_M";
	setAttr -k off ".v";
createNode parentConstraint -n "IKParentConstraintPelvis_M_parentConstraint1" -p "IKParentConstraintPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine0AlignTo_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.0587911840678754e-22 -8.8817841970012523e-15 
		2.2204460492503131e-15 ;
	setAttr ".rst" -type "double3" 5.9604644775390625e-08 5.2490606307983398 -1.2424887120723729 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintHip_L" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "IKOffsetHip_L" -p "IKParentConstraintHip_L";
	setAttr ".t" -type "double3" 0.088520804836133671 -0.74529486894607433 0.041540897564236401 ;
	setAttr ".r" -type "double3" 1.0589532311777556 -0.72669176883991526 -90.175490857600835 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999944 0.99999999999999989 0.99999999999999989 ;
createNode joint -n "IKXHip_L" -p "IKOffsetHip_L";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0 -2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" -4.6640598262927299e-14 -4.933446883918503e-06 -1.0833430711163378e-06 ;
	setAttr ".ro" 2;
createNode joint -n "IKXKnee_L" -p "IKXHip_L";
	setAttr ".t" -type "double3" -2.886579864025407e-15 -2.273484468460083 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 22.919766809847236 -6.089450554775608e-14 -3.0038258711354778e-13 ;
	setAttr ".ro" 2;
	setAttr ".pa" -type "double3" 22.919761854393723 -6.089449925595488e-14 -3.0038248718465602e-13 ;
createNode joint -n "IKXAnkle_L" -p "IKXKnee_L";
	setAttr ".t" -type "double3" -1.2434497875801753e-14 -2.6670582294464111 -6.3837823915946501e-16 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" 1.4900974413835479 -0.0077695994812840199 0.59745911162008236 ;
createNode joint -n "IKXMiddleToe1_L" -p "IKXAnkle_L";
	setAttr ".t" -type "double3" -0.020825579278234807 -0.15220357689789743 1.3263746982714686 ;
	setAttr ".r" -type "double3" 108.35551145034458 0.28329999593691257 -179.14629422410513 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -2.0407002965414474e-13 -5.9635400277440939e-16 1.4210240379423905e-15 ;
	setAttr ".pa" -type "double3" 108.35550752148379 0.28330165257886331 -179.1462860042181 ;
createNode joint -n "IKXMiddleToe2_End_L" -p "IKXMiddleToe1_L";
	setAttr ".t" -type "double3" 5.5511151231257827e-15 -1.4371052628296197 -2.2204460492503131e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -1.5171077446214807 -3.9348917796465742e-13 -7.3814052773437456e-14 ;
createNode ikEffector -n "effector9" -p "IKXMiddleToe1_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "IKFKAlignedLeg_L" -p "IKXAnkle_L";
	setAttr ".t" -type "double3" 1.3696637934934586e-07 -2.7686734793697099e-08 -2.0158979124884624e-07 ;
	setAttr ".r" -type "double3" 1.8156088081481249e-06 4.7384478722523305e-06 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000007 1.0000000000000002 1.0000000000000002 ;
createNode orientConstraint -n "IKXAnkle_L_orientConstraint1" -p "IKXAnkle_L";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.4992840285161861 -0.90704624983847326 0.57399249367551941 ;
	setAttr ".o" -type "double3" 180 0.89953576921945344 0 ;
	setAttr ".rsrr" -type "double3" 1.4900975481493579 -0.0077695998277237752 0.59745912493731002 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector10" -p "IKXAnkle_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector8" -p "IKXKnee_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_L" -p "IKXKnee_L";
	setAttr ".r" -type "double3" 1.4902032733472741 -0.90700076186608425 0.57406674412549963 ;
	setAttr ".s" -type "double3" 1.0000000000000007 1.0000000000000002 1.0000000000000002 ;
createNode annotationShape -n "PoleAnnotationLeg_LShape" -p "PoleAnnotationLeg_L";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintHip_L_parentConstraint1" -p "IKParentConstraintHip_L";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -25.139577240952999 4.4042152622383557e-06 90.000000396222816 ;
	setAttr ".rst" -type "double3" 1.2119851112365725 5.3071937561035165 -1.2426750957965851 ;
	setAttr ".rsrr" -type "double3" -25.139579032906394 -1.7566776614494369e-15 90.000000000000242 ;
	setAttr -k on ".w0";
createNode transform -n "IKHandle" -p "IKSystem";
createNode transform -n "IKParentConstraintArm1_R" -p "IKHandle";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "IKExtraArm1_R" -p "IKParentConstraintArm1_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKArm1_R" -p "IKExtraArm1_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow";
createNode nurbsCurve -n "IKArm1_RShape" -p "IKArm1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-0.40752071950000002 -0.40752071950000002 -0.20376035980000001
		0.40752071950000002 -0.40752071950000002 -0.20376035980000001
		0.40752071950000002 -0.40752071950000002 0.20376035980000001
		0.40752071950000002 0.40752071950000002 0.20376035980000001
		0.40752071950000002 0.40752071950000002 -0.20376035980000001
		-0.40752071950000002 0.40752071950000002 -0.20376035980000001
		-0.40752071950000002 0.40752071950000002 0.20376035980000001
		-0.40752071950000002 -0.40752071950000002 0.20376035980000001
		-0.40752071950000002 -0.40752071950000002 -0.20376035980000001
		-0.40752071950000002 0.40752071950000002 -0.20376035980000001
		-0.40752071950000002 0.40752071950000002 0.20376035980000001
		0.40752071950000002 0.40752071950000002 0.20376035980000001
		0.405837322 0.405837322 -0.20376035980000001
		0.405837322 -0.405837322 -0.20376035980000001
		0.40752071950000002 -0.40752071950000002 0.20376035980000001
		-0.40752071950000002 -0.40752071950000002 0.20376035980000001
		;
createNode ikHandle -n "IKXArm1Handle_R" -p "IKArm1_R";
	setAttr -l on ".v" no;
	setAttr ".ro" 5;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXArm1Handle_R_poleVectorConstraint1" -p "IKXArm1Handle_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleArm1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -4.9901354065682124 -2.6721857183697608 -2.2768966964086581 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IKParentConstraintArm1_R_parentConstraint1" -p "IKParentConstraintArm1_R";
	addAttr -ci true -k true -sn "w0" -ln "IKParentConstraintArm1_RStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "Clavicle_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" -2.887757646261321 3.9637683866026303 -3.3102026402737197 ;
	setAttr ".tg[1].tor" -type "double3" 90.000003930293886 -84.931295080550868 4.0819389749637658 ;
	setAttr ".rst" -type "double3" -5.6388833095280528 6.5330845935161346 2.1596220104729853 ;
	setAttr ".rsrr" -type "double3" -6.3611093629270335e-15 -1.6772843932590602e-30 
		3.0215269473903408e-14 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PoleParentConstraintArm1_R" -p "IKHandle";
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
createNode transform -n "PoleExtraArm1_R" -p "PoleParentConstraintArm1_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleArm1_R" -p "PoleExtraArm1_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 4.4408920985006262e-16 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow";
createNode nurbsCurve -n "PoleArm1_RShape" -p "PoleArm1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		-1.6265306409999998e-32 0.32989960689999998 -7.3252427879999994e-17
		2.1975728359999999e-16 1.0987864179999999e-16 0.49484941030000001
		1.6265306409999998e-32 -0.32989960689999998 7.3252427879999994e-17
		8.1326532040000003e-33 -0.16494980340000001 3.6626213939999997e-17
		-2.1975728359999999e-16 -0.16494980340000001 -0.49484941030000001
		-2.1975728359999999e-16 0.16494980340000001 -0.49484941030000001
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		;
createNode transform -n "PoleAnnotateTargetArm1_R" -p "PoleArm1_R";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetArm1_RShape" -p "PoleAnnotateTargetArm1_R";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintArm1_R_parentConstraint1" -p "PoleParentConstraintArm1_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintArm1_RStaticW0" -dv 
		1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKArm1_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" -1.990811485200866 0.43239162201302683 -4.9302363177176698 ;
	setAttr ".rst" -type "double3" -7.6296947947289198 6.9654762155291623 -2.7706143072446849 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintArm_L" -p "IKHandle";
	setAttr ".ro" 5;
createNode transform -n "IKExtraArm_L" -p "IKParentConstraintArm_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKArm_L" -p "IKExtraArm_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow";
createNode nurbsCurve -n "IKArm_LShape" -p "IKArm_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.40752071950000002 0.20376035980000001 -0.40752071950000002
		0.40752071950000002 0.20376035980000001 0.40752071950000002
		0.40752071950000002 -0.20376035980000001 0.40752071950000002
		-0.40752071950000002 -0.20376035980000001 0.40752071950000002
		-0.40752071950000002 0.20376035980000001 0.40752071950000002
		-0.40752071950000002 0.20376035980000001 -0.40752071950000002
		-0.40752071950000002 -0.20376035980000001 -0.40752071950000002
		0.40752071950000002 -0.20376035980000001 -0.40752071950000002
		0.40752071950000002 0.20376035980000001 -0.40752071950000002
		-0.40752071950000002 0.20376035980000001 -0.40752071950000002
		-0.40752071950000002 -0.20376035980000001 -0.40752071950000002
		-0.40752071950000002 -0.20376035980000001 0.40752071950000002
		-0.405837322 0.20376035980000001 0.405837322
		0.405837322 0.20376035980000001 0.405837322
		0.40752071950000002 -0.20376035980000001 0.40752071950000002
		0.40752071950000002 -0.20376035980000001 -0.40752071950000002
		;
createNode ikHandle -n "IKXArmHandle_L" -p "IKArm_L";
	setAttr -l on ".v" no;
	setAttr ".ro" 5;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXArmHandle_L_poleVectorConstraint1" -p "IKXArmHandle_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleArm_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 6.6858788498961914 -0.6383914637164505 -0.10658901685704042 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IKParentConstraintArm_L_parentConstraint1" -p "IKParentConstraintArm_L";
	addAttr -ci true -k true -sn "w0" -ln "IKParentConstraintArm_LStaticW0" -dv 1 -min 
		0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "Clavicle1_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" -0.20069674041593027 -4.3214345076152592 4.2213903087235733 ;
	setAttr ".tg[1].tor" -type "double3" -89.999999664563774 89.999996197111585 0 ;
	setAttr ".rst" -type "double3" 5.5319444660498629 5.3142896623624916 -0.5673400272421707 ;
	setAttr ".rsrr" -type "double3" 3.1805546817960252e-15 1.5902773400667423e-15 -2.3959732262518193e-08 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PoleParentConstraintArm_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000002 ;
createNode transform -n "PoleExtraArm_L" -p "PoleParentConstraintArm_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleArm_L" -p "PoleExtraArm_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 1.7763568394002505e-15 0 -1.1102230246251565e-16 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow";
createNode nurbsCurve -n "PoleArm_LShape" -p "PoleArm_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		-1.6265306409999998e-32 0.32989960689999998 -7.3252427879999994e-17
		2.1975728359999999e-16 1.0987864179999999e-16 0.49484941030000001
		1.6265306409999998e-32 -0.32989960689999998 7.3252427879999994e-17
		8.1326532040000003e-33 -0.16494980340000001 3.6626213939999997e-17
		-2.1975728359999999e-16 -0.16494980340000001 -0.49484941030000001
		-2.1975728359999999e-16 0.16494980340000001 -0.49484941030000001
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		;
createNode transform -n "PoleAnnotateTargetArm_L" -p "PoleArm_L";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetArm_LShape" -p "PoleAnnotateTargetArm_L";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintArm_L_parentConstraint1" -p "PoleParentConstraintArm_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintArm_LStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKArm_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" 0 0 -1.1102230246251565e-16 ;
	setAttr ".tg[1].tot" -type "double3" 3.7934937725358449 3.6849803235726526 -0.032966570648575666 ;
	setAttr ".rst" -type "double3" 9.3254382385857078 8.999269985935145 -0.60030659789074625 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintLeg_R" -p "IKHandle";
	setAttr ".t" -type "double3" -1.9235993326833996 0.66881483811707909 -0.23313113337813718 ;
	setAttr ".ro" 3;
createNode transform -n "IKExtraLeg_R" -p "IKParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_R" -p "IKExtraLeg_R";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "toe" -ln "toe" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".toe";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
	setAttr -k on ".stretchy";
	setAttr -k on ".antiPop";
	setAttr -k on ".Length1";
	setAttr -k on ".Length2";
createNode nurbsCurve -n "IKLeg_RShape" -p "IKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-1.1329471149999999 -0.66881483789999996 2.6905080770000001
		1.1329471149999999 -0.66881483789999996 2.6905080770000001
		1.1329471149999999 0.52856881929999999 2.6905080770000001
		1.5293311700000001 0.52856881929999999 -1.478531781
		1.5293311700000001 -0.66881483789999996 -1.478531781
		-1.5293311700000001 -0.66881483789999996 -1.478531781
		-1.5293311700000001 0.52856881929999999 -1.478531781
		-1.1329471149999999 0.52856881929999999 2.6905080770000001
		-1.1329471149999999 -0.66881483789999996 2.6905080770000001
		-1.5293311700000001 -0.66881483789999996 -1.478531781
		-1.5293311700000001 0.52856881929999999 -1.478531781
		1.5293311700000001 0.52856881929999999 -1.478531781
		1.523013768 -0.66881483789999996 -1.476848384
		1.1282671070000001 -0.66881483789999996 2.6888246790000001
		1.1329471149999999 0.52856881929999999 2.6905080770000001
		-1.1329471149999999 0.52856881929999999 2.6905080770000001
		;
createNode transform -n "IKFootRollLeg_R" -p "IKLeg_R";
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
createNode transform -n "IKRollLegHeel_R" -p "IKFootRollLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.122220094629256e-10 -0.62094301353949466 -1.4785317884523768 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_R" -p "IKRollLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -1.3877787807814457e-17 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1 0.99999999999999978 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_R" -p "IKExtraLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_RShape" -p "IKLegHeel_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		-1.002780287e-16 -0.45161209270000002 -1.3086588599999999e-16
		-5.1353462759999998e-17 -0.31933797320000001 0.31933797320000001
		2.7653265189999997e-17 -1.3607953719999999e-16 0.45161209270000002
		9.0461085430000003e-17 0.31933797320000001 0.31933797320000001
		1.002780287e-16 0.45161209270000002 2.4256173589999999e-16
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKRollLegToe_R" -p "IKLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.4037020956703117e-06 0.016127395699697691 4.1690398580355117 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegToe_R" -p "IKRollLegToe_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.7755575615628914e-17 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegToe_R" -p "IKExtraLegToe_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegToe_RShape" -p "IKLegToe_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		-1.002780287e-16 -0.45161209270000002 -1.3086588599999999e-16
		-5.1353462759999998e-17 -0.31933797320000001 0.31933797320000001
		2.7653265189999997e-17 -1.3607953719999999e-16 0.45161209270000002
		9.0461085430000003e-17 0.31933797320000001 0.31933797320000001
		1.002780287e-16 0.45161209270000002 2.4256173589999999e-16
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKLiftToeLegToe_R" -p "IKLegToe_R";
	setAttr ".t" -type "double3" 1.373487573896881e-06 0.45261204341339123 -1.3639698885572968 ;
createNode ikHandle -n "IKXLegHandleToe_R" -p "IKLiftToeLegToe_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -1.2709369026087103e-06 -0.45261208594233193 1.3639698809394238 ;
	setAttr ".r" -type "double3" 71.642381757719662 4.6206861071762847e-05 179.99997835993693 ;
	setAttr ".s" -type "double3" 1.0000000000000009 1.0000000000000002 1 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "IKRollLegBall_R" -p "IKLegToe_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.3736366890615415e-06 0.45261204075739137 -1.3639698959332396 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_R" -p "IKRollLegBall_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_R" -p "IKExtraLegBall_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_RShape" -p "IKLegBall_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-9.9600445170000006e-17 0.6017504897 -0.6017504897
		-2.4847825250000002e-16 -1.550498299e-16 -0.85100370380000001
		-3.6683134469999999e-16 -0.6017504897 -0.6017504897
		-3.8533008560000004e-16 -0.85100370380000001 -3.850084044e-16
		-2.9313816359999997e-16 -0.6017504897 0.6017504897
		-1.4426035630000001e-16 -1.9846354150000001e-16 0.85100370380000001
		-2.5907264070000001e-17 0.6017504897 0.6017504897
		-7.4085231850000001e-18 0.85100370380000001 2.027459622e-16
		-9.9600445170000006e-17 0.6017504897 -0.6017504897
		-2.4847825250000002e-16 -1.550498299e-16 -0.85100370380000001
		-3.6683134469999999e-16 -0.6017504897 -0.6017504897
		;
createNode transform -n "IKFootPivotBallReverseLeg_R" -p "IKLegBall_R";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0 2.2204460492503131e-16 ;
createNode ikHandle -n "IKXLegHandle_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 2.9953184377262687e-08 0.15220357708240595 -1.3265381736498931 ;
	setAttr ".ro" 3;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_R_poleVectorConstraint1" -p "IKXLegHandle_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -0.068790794733306626 -1.1628908003227467 5.198601449181707 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -1.4911538670503433e-10 2.6559998644870575e-09 7.3759425234243281e-09 ;
	setAttr ".r" -type "double3" 179.99999989323828 -0.89953576921938283 1.8515546702787496e-25 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "IKmessureConstrainToLeg_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr ".t" -type "double3" 2.9953184599307292e-08 0.15220357708240595 -1.3265381736498929 ;
createNode transform -n "PoleAimLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 1 0.99999999999999978 0.99999999999999978 ;
createNode aimConstraint -n "PoleAimLeg_R_aimConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -sn "w0" -ln "IKLeg_RW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -0.084940657943518211 -12.032589519036796 -89.592554831593233 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_R_pointConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "IKXHip_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.9572799801826468 5.4049770832061776 -1.2426749765872953 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 0.99999999999999989 ;
createNode transform -n "PoleExtraLeg_R" -p "PoleParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_R" -p "PoleExtraLeg_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_RShape" -p "PoleLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		-1.6265306409999998e-32 0.32989960689999998 -7.3252427879999994e-17
		2.1975728359999999e-16 1.0987864179999999e-16 0.49484941030000001
		1.6265306409999998e-32 -0.32989960689999998 7.3252427879999994e-17
		8.1326532040000003e-33 -0.16494980340000001 3.6626213939999997e-17
		-2.1975728359999999e-16 -0.16494980340000001 -0.49484941030000001
		-2.1975728359999999e-16 0.16494980340000001 -0.49484941030000001
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		;
createNode transform -n "PoleAnnotateTargetLeg_R" -p "PoleLeg_R";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_RShape" -p "PoleAnnotateTargetLeg_R";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_R_parentConstraint1" -p "PoleParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_RStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" 2.2205761065913867 -0.084236852104972559 4.8419469350595268 ;
	setAttr ".tg[1].tor" -type "double3" 12.032884902140747 6.3428543801072497e-15 89.601506880585802 ;
	setAttr ".lr" -type "double3" 2.529632936039711e-08 9.1157774262992618e-09 4.2765651479463376e-08 ;
	setAttr ".rst" -type "double3" -2.0260707749159534 4.2420862828834291 3.9559264725944105 ;
	setAttr ".rsrr" -type "double3" -2.0586734843249569e-31 -2.3854160110976376e-15 
		9.8895372126756228e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintSpine0_M" -p "IKHandle";
createNode transform -n "IKExtraSpine0_M" -p "IKParentConstraintSpine0_M";
createNode transform -n "IKSpine0_M" -p "IKExtraSpine0_M";
	addAttr -ci true -k true -sn "stiff" -ln "stiff" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".stiff";
createNode nurbsCurve -n "IKSpine0_MShape" -p "IKSpine0_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.0527618590000001 -0.4195080934 -1.0997260449999999
		-1.0527618590000001 -0.4195080934 -1.0997260449999999
		-1.0527618590000001 0.62807597979999996 -0.99544210160000002
		-1.0527618590000001 0.4195080934 1.0997260449999999
		-1.0527618590000001 -0.62807597979999996 0.99544210160000002
		1.0527618590000001 -0.62807597979999996 0.99544210160000002
		1.0527618590000001 0.4195080934 1.0997260449999999
		1.0527618590000001 0.62807597979999996 -0.99544210160000002
		1.0527618590000001 -0.4195080934 -1.0997260449999999
		1.0527618590000001 -0.62807597979999996 0.99544210160000002
		1.0527618590000001 0.4195080934 1.0997260449999999
		-1.0527618590000001 0.4195080934 1.0997260449999999
		-1.0484130819999999 -0.62764520089999998 0.9911147132
		-1.0484130819999999 -0.41993887229999999 -1.095398656
		-1.0527618590000001 0.62807597979999996 -0.99544210160000002
		1.0527618590000001 0.62807597979999996 -0.99544210160000002
		;
createNode transform -n "IKXSpineLocator0_M" -p "IKSpine0_M";
	setAttr -l on ".v" no;
createNode locator -n "IKXSpineLocator0_MShape" -p "IKXSpineLocator0_M";
	setAttr -k off ".v";
createNode transform -n "IKStiffStartOrientSpine_M" -p "IKSpine0_M";
	setAttr ".t" -type "double3" -1.0587911840678754e-22 -8.8817841970012523e-15 2.2204460492503131e-15 ;
	setAttr ".r" -type "double3" -0.03810190966461565 0 0 ;
createNode transform -n "IKXSpineLocator1_M" -p "IKStiffStartOrientSpine_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 1.0587911840678754e-22 0.37740218639373779 -3.907985046680551e-14 ;
	setAttr ".r" -type "double3" 0.03810190966461565 0 0 ;
createNode locator -n "IKXSpineLocator1_MShape" -p "IKXSpineLocator1_M";
	setAttr -k off ".v";
createNode transform -n "IKSpine0AlignTo_M" -p "IKSpine0_M";
createNode transform -n "IKOrientToSpine_M" -p "IKSpine0_M";
	setAttr ".t" -type "double3" -1.0587911840678754e-22 -8.8817841970012523e-15 2.2204460492503131e-15 ;
createNode parentConstraint -n "IKParentConstraintSpine0_M_parentConstraint1" -p "IKParentConstraintSpine0_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 5.9604644775390731e-08 8.8817841970012523e-15 
		-2.2204460492503131e-15 ;
	setAttr ".rst" -type "double3" 5.9604644775390731e-08 5.2490606307983487 -1.2424887120723751 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintSpine2_M" -p "IKHandle";
createNode transform -n "IKExtraSpine2_M" -p "IKParentConstraintSpine2_M";
createNode transform -n "IKSpine2_M" -p "IKExtraSpine2_M";
	addAttr -ci true -k true -sn "stiff" -ln "stiff" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".stiff";
createNode nurbsCurve -n "IKSpine2_MShape" -p "IKSpine2_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.0527618590000001 -0.36610476759999999 -1.11863833
		-1.0527618590000001 -0.36610476759999999 -1.11863833
		-1.0527618590000001 0.67524780340000001 -0.964066812
		-1.0527618590000001 0.36610476759999999 1.11863833
		-1.0527618590000001 -0.67524780340000001 0.964066812
		1.0527618590000001 -0.67524780340000001 0.964066812
		1.0527618590000001 0.36610476759999999 1.11863833
		1.0527618590000001 0.67524780340000001 -0.964066812
		1.0527618590000001 -0.36610476759999999 -1.11863833
		1.0527618590000001 -0.67524780340000001 0.964066812
		1.0527618590000001 0.36610476759999999 1.11863833
		-1.0527618590000001 0.36610476759999999 1.11863833
		-1.0484130819999999 -0.67460929520000001 0.95976516479999996
		-1.0484130819999999 -0.36674327579999999 -1.1143366830000001
		-1.0527618590000001 0.67524780340000001 -0.964066812
		1.0527618590000001 0.67524780340000001 -0.964066812
		;
createNode transform -n "IKXSpineLocator2_M" -p "IKSpine2_M";
	setAttr -l on ".v" no;
createNode locator -n "IKXSpineLocator2_MShape" -p "IKXSpineLocator2_M";
	setAttr -k off ".v";
createNode parentConstraint -n "IKParentConstraintSpine2_M_parentConstraint1" -p "IKParentConstraintSpine2_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 5.960464477539042e-08 1.1322063168508594 -0.00072050106915244605 ;
	setAttr ".rst" -type "double3" 5.960464477539042e-08 6.3812669476491992 -1.2432092131415253 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintSpine4_M" -p "IKHandle";
createNode transform -n "IKExtraSpine4_M" -p "IKParentConstraintSpine4_M";
createNode transform -n "IKSpine4_M" -p "IKExtraSpine4_M";
	addAttr -ci true -k true -sn "stiff" -ln "stiff" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".stiff";
	setAttr -k on ".stretchy";
createNode nurbsCurve -n "IKSpine4_MShape" -p "IKSpine4_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.0527618590000001 1.099629594 -0.41976084800000002
		-1.0527618590000001 1.099629594 -0.41976084800000002
		-1.0527618590000001 0.99558643489999998 0.62784716659999995
		-1.0527618590000001 -1.099629594 0.41976084800000002
		-1.0527618590000001 -0.99558643489999998 -0.62784716659999995
		1.0527618590000001 -0.99558643489999998 -0.62784716659999995
		1.0527618590000001 -1.099629594 0.41976084800000002
		1.0527618590000001 0.99558643489999998 0.62784716659999995
		1.0527618590000001 1.099629594 -0.41976084800000002
		1.0527618590000001 -0.99558643489999998 -0.62784716659999995
		1.0527618590000001 -1.099629594 0.41976084800000002
		-1.0527618590000001 -1.099629594 0.41976084800000002
		-1.0484130819999999 -0.9912589476 -0.62741738229999999
		-1.0484130819999999 1.095302107 -0.42019063220000002
		-1.0527618590000001 0.99558643489999998 0.62784716659999995
		1.0527618590000001 0.99558643489999998 0.62784716659999995
		;
createNode transform -n "IKXSpineLocator4_M" -p "IKSpine4_M";
	setAttr -l on ".v" no;
createNode locator -n "IKXSpineLocator4_MShape" -p "IKXSpineLocator4_M";
	setAttr -k off ".v";
createNode ikHandle -n "IKXSpineHandle_M" -p "IKSpine4_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 1.5881867761018131e-22 1.5987211554602254e-14 -3.3306690738754696e-15 ;
	setAttr ".roc" yes;
createNode transform -n "IKStiffEndOrientSpine_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" 5.2939559203393771e-23 7.1054273576010019e-15 -1.1102230246251565e-15 ;
	setAttr ".r" -type "double3" -179.9906517729099 0 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "IKXSpineLocator3_M" -p "IKStiffEndOrientSpine_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -5.2939559203393771e-23 0.37740215659141541 1.9584334154387761e-13 ;
	setAttr ".r" -type "double3" 179.9906517729099 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999967 ;
createNode locator -n "IKXSpineLocator3_MShape" -p "IKXSpineLocator3_M";
	setAttr -k off ".v";
createNode transform -n "IKEndJointOrientToSpine_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" 1.5881867761018131e-22 1.5987211554602254e-14 -3.3306690738754696e-15 ;
createNode transform -n "IKSpine4AlignTo_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" 1.5881867761018131e-22 1.5987211554602254e-14 -3.3306690738754696e-15 ;
createNode transform -n "IKSpine4AlignUnTwistToOffset_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" 1.5881867761018131e-22 1.5987211554602254e-14 -3.3306690738754696e-15 ;
createNode transform -n "IKSpine4AlignUnTwistTo_M" -p "IKSpine4AlignUnTwistToOffset_M";
createNode parentConstraint -n "IKParentConstraintSpine4_M_parentConstraint1" -p
		 "IKParentConstraintSpine4_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 5.9604644775390572e-08 2.2644128799438406 -0.00051128792762633601 ;
	setAttr ".rst" -type "double3" 5.9604644775390572e-08 7.5134735107421804 -1.2429999999999992 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintLeg_L" -p "IKHandle";
	setAttr ".t" -type "double3" 1.9235993326833907 0.66881483811707776 -0.23313113337811536 ;
	setAttr ".ro" 3;
createNode transform -n "IKExtraLeg_L" -p "IKParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_L" -p "IKExtraLeg_L";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "toe" -ln "toe" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".toe";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
	setAttr -k on ".stretchy";
	setAttr -k on ".antiPop";
	setAttr -k on ".Length1";
	setAttr -k on ".Length2";
createNode nurbsCurve -n "IKLeg_LShape" -p "IKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-1.1329471149999999 -0.66881483789999996 2.6905080770000001
		1.1329471149999999 -0.66881483789999996 2.6905080770000001
		1.1329471149999999 0.52856881929999999 2.6905080770000001
		1.5293311700000001 0.52856881929999999 -1.478531781
		1.5293311700000001 -0.66881483789999996 -1.478531781
		-1.5293311700000001 -0.66881483789999996 -1.478531781
		-1.5293311700000001 0.52856881929999999 -1.478531781
		-1.1329471149999999 0.52856881929999999 2.6905080770000001
		-1.1329471149999999 -0.66881483789999996 2.6905080770000001
		-1.5293311700000001 -0.66881483789999996 -1.478531781
		-1.5293311700000001 0.52856881929999999 -1.478531781
		1.5293311700000001 0.52856881929999999 -1.478531781
		1.523013768 -0.66881483789999996 -1.476848384
		1.1282671070000001 -0.66881483789999996 2.6888246790000001
		1.1329471149999999 0.52856881929999999 2.6905080770000001
		-1.1329471149999999 0.52856881929999999 2.6905080770000001
		;
createNode transform -n "IKFootRollLeg_L" -p "IKLeg_L";
createNode transform -n "IKRollLegHeel_L" -p "IKFootRollLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.122122395003089e-10 -0.62094301353949355 -1.4785317884523974 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1 1 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_L" -p "IKRollLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1 1 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_L" -p "IKExtraLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_LShape" -p "IKLegHeel_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		-1.002780287e-16 -0.45161209270000002 -1.3086588599999999e-16
		-5.1353462759999998e-17 -0.31933797320000001 0.31933797320000001
		2.7653265189999997e-17 -1.3607953719999999e-16 0.45161209270000002
		9.0461085430000003e-17 0.31933797320000001 0.31933797320000001
		1.002780287e-16 0.45161209270000002 2.4256173589999999e-16
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKRollLegToe_L" -p "IKLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.4037020970025793e-06 0.016127395699696456 4.1690398580355099 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegToe_L" -p "IKRollLegToe_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1.0000000000000002 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegToe_L" -p "IKExtraLegToe_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegToe_LShape" -p "IKLegToe_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		-1.002780287e-16 -0.45161209270000002 -1.3086588599999999e-16
		-5.1353462759999998e-17 -0.31933797320000001 0.31933797320000001
		2.7653265189999997e-17 -1.3607953719999999e-16 0.45161209270000002
		9.0461085430000003e-17 0.31933797320000001 0.31933797320000001
		1.002780287e-16 0.45161209270000002 2.4256173589999999e-16
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKLiftToeLegToe_L" -p "IKLegToe_L";
	setAttr ".t" -type "double3" -1.3734920356611724e-06 0.45261204094189977 -1.363969888840846 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode ikHandle -n "IKXLegHandleToe_L" -p "IKLiftToeLegToe_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 1.2709236645314093e-06 -0.45261208848356216 1.3639698800961562 ;
	setAttr ".r" -type "double3" 71.64238165097106 180.0000462068611 2.1638387100590144e-05 ;
	setAttr ".s" -type "double3" 1.0000000000000007 1.0000000000000004 1 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "IKRollLegBall_L" -p "IKLegToe_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.3736366903938091e-06 0.45261204075739203 -1.3639698959332387 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_L" -p "IKRollLegBall_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 2.2204460492503131e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999978 1.0000000000000002 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_L" -p "IKExtraLegBall_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_LShape" -p "IKLegBall_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		2.9313816359999997e-16 0.6017504897 -0.6017504897
		1.4426035630000001e-16 -1.550498299e-16 -0.85100370380000001
		2.5907264070000001e-17 -0.6017504897 -0.6017504897
		7.4085231850000001e-18 -0.85100370380000001 -1.8863909999999998e-16
		9.9600445170000006e-17 -0.6017504897 0.6017504897
		2.4847825250000002e-16 -1.9846354150000001e-16 0.85100370380000001
		3.6683134469999999e-16 0.6017504897 0.6017504897
		3.8533008560000004e-16 0.85100370380000001 3.9911526660000001e-16
		2.9313816359999997e-16 0.6017504897 -0.6017504897
		1.4426035630000001e-16 -1.550498299e-16 -0.85100370380000001
		2.5907264070000001e-17 -0.6017504897 -0.6017504897
		;
createNode transform -n "IKFootPivotBallReverseLeg_L" -p "IKLegBall_L";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 1.1102230246251565e-16 0 ;
createNode ikHandle -n "IKXLegHandle_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -2.9953194369269909e-08 0.15220357708240451 -1.3265381736498938 ;
	setAttr ".ro" 3;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_L_poleVectorConstraint1" -p "IKXLegHandle_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0.068790794733311733 -1.1628908003227143 5.1986014491817105 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 1.446549546813003e-10 1.8450763139554738e-10 7.0923926731580877e-09 ;
	setAttr ".r" -type "double3" 0 0.89953576921945322 0 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "IKmessureConstrainToLeg_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr ".t" -type "double3" -2.9953194591314514e-08 0.15220357708240462 -1.3265381736498936 ;
createNode transform -n "PoleAimLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
createNode aimConstraint -n "PoleAimLeg_L_aimConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -sn "w0" -ln "IKLeg_LW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" 0.084940657943555695 -12.032589519037034 -90.407445168406895 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_L_pointConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "IKXHip_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.9572799801826466 5.4049770832061759 -1.2426749765872944 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 0.99999999999999978 1 1 ;
createNode transform -n "PoleExtraLeg_L" -p "PoleParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_L" -p "PoleExtraLeg_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_LShape" -p "PoleLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		-1.6265306409999998e-32 0.32989960689999998 -7.3252427879999994e-17
		2.1975728359999999e-16 1.0987864179999999e-16 0.49484941030000001
		1.6265306409999998e-32 -0.32989960689999998 7.3252427879999994e-17
		8.1326532040000003e-33 -0.16494980340000001 3.6626213939999997e-17
		-2.1975728359999999e-16 -0.16494980340000001 -0.49484941030000001
		-2.1975728359999999e-16 0.16494980340000001 -0.49484941030000001
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		;
createNode transform -n "PoleAnnotateTargetLeg_L" -p "PoleLeg_L";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_LShape" -p "PoleAnnotateTargetLeg_L";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_L_parentConstraint1" -p "PoleParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_LStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" 2.2205761065913747 0.08423685210498344 4.8419469350595286 ;
	setAttr ".tg[1].tor" -type "double3" 12.032884902140985 7.1083712880512222e-15 90.398493119414312 ;
	setAttr ".lr" -type "double3" -6.4137712857446405e-07 1.7898611696864765e-09 8.3971648888386984e-09 ;
	setAttr ".rst" -type "double3" 2.0260707749159583 4.2420862828834611 3.9559264725944168 ;
	setAttr ".rsrr" -type "double3" -1.192708005548819e-15 -3.5781240166464568e-15 3.5781240166464568e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKStatic" -p "IKSystem";
createNode transform -n "IKParentConstraintArm1_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -5.6388833095280537 6.5330845935161355 2.1596220104729849 ;
createNode transform -n "PoleParentConstraintArm1_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -7.6296947947289198 6.9654762155291623 -2.7706143072446849 ;
createNode transform -n "IKParentConstraintArm_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 5.5319444660498629 5.3142896623624925 -0.56734002724217059 ;
createNode transform -n "PoleParentConstraintArm_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 9.3254382385857078 8.999269985935145 -0.60030659789074625 ;
createNode transform -n "PoleParentConstraintLeg_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -2.0260707749159534 4.24208628288343 3.9559264725944114 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "PoleParentConstraintLeg_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 2.0260707749159583 4.2420862828834611 3.9559264725944168 ;
createNode transform -n "IKCrv" -p "IKSystem";
	setAttr ".it" no;
createNode transform -n "IKXSpineCurve_M" -p "IKCrv";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
createNode nurbsCurve -n "IKXSpineCurve_MShape" -p "IKXSpineCurve_M";
	setAttr -k off ".v";
	setAttr -s 5 ".cp";
	setAttr ".cc" -type "nurbsCurve" 
		3 2 0 no 3
		7 0 0 0 1.1322065105433916 2.2644130210867832 2.2644130210867832 2.2644130210867832
		
		5
		5.9604644775390731e-08 5.2490606307983487 -1.2424887120723751
		5.9604644775390731e-08 5.6264627337427928 -1.2427396859325384
		5.960464477539042e-08 6.3812669476491992 -1.2432092131415253
		5.9604644775390572e-08 7.1360713591740543 -1.2430615759326373
		5.9604644775390572e-08 7.5134735107421804 -1.2429999999999992
		;
createNode transform -n "IKMessure" -p "IKSystem";
createNode transform -n "IKmessureLoc1Leg_R" -p "IKMessure";
	setAttr -l on ".v" no;
createNode locator -n "IKmessureLoc1Leg_RShape" -p "IKmessureLoc1Leg_R";
	setAttr -k off ".v";
createNode pointConstraint -n "IKmessureLoc1Leg_R_pointConstraint1" -p "IKmessureLoc1Leg_R";
	addAttr -ci true -k true -sn "w0" -ln "IKXHip_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.9572799836854902 5.4049770565078363 -1.2426749687103267 ;
	setAttr -k on ".w0";
createNode transform -n "IKmessureLoc2Leg_R" -p "IKmessureLoc1Leg_R";
	setAttr -l on ".v" no;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode locator -n "IKmessureLoc2Leg_RShape" -p "IKmessureLoc2Leg_R";
	setAttr -k off ".v";
createNode pointConstraint -n "IKmessureLoc2Leg_R_pointConstraint1" -p "IKmessureLoc2Leg_R";
	addAttr -ci true -k true -sn "w0" -ln "IKmessureConstrainToLeg_RW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0.033680651002091277 -4.7361622183907564 1.0095438353321937 ;
	setAttr -k on ".w0";
createNode transform -n "IKdistanceLeg_R" -p "IKMessure";
	setAttr -l on ".v" no;
createNode distanceDimShape -n "IKdistanceLeg_RShape" -p "IKdistanceLeg_R";
	addAttr -ci true -sn "antiPop" -ln "antiPop" -at "double";
	setAttr -k off ".v";
createNode transform -n "IKmessureLoc1Leg_L" -p "IKMessure";
	setAttr -l on ".v" no;
createNode locator -n "IKmessureLoc1Leg_LShape" -p "IKmessureLoc1Leg_L";
	setAttr -k off ".v";
createNode pointConstraint -n "IKmessureLoc1Leg_L_pointConstraint1" -p "IKmessureLoc1Leg_L";
	addAttr -ci true -k true -sn "w0" -ln "IKXHip_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.9572799795704985 5.4049770878719654 -1.2426750330079905 ;
	setAttr -k on ".w0";
createNode transform -n "IKmessureLoc2Leg_L" -p "IKmessureLoc1Leg_L";
	setAttr -l on ".v" no;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode locator -n "IKmessureLoc2Leg_LShape" -p "IKmessureLoc2Leg_L";
	setAttr -k off ".v";
createNode pointConstraint -n "IKmessureLoc2Leg_L_pointConstraint1" -p "IKmessureLoc2Leg_L";
	addAttr -ci true -k true -sn "w0" -ln "IKmessureConstrainToLeg_LW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -0.033680646887108701 -4.7361622497548881 1.0095438996298558 ;
	setAttr -k on ".w0";
createNode transform -n "IKdistanceLeg_L" -p "IKMessure";
	setAttr -l on ".v" no;
createNode distanceDimShape -n "IKdistanceLeg_LShape" -p "IKdistanceLeg_L";
	addAttr -ci true -sn "antiPop" -ln "antiPop" -at "double";
	setAttr -k off ".v";
createNode transform -n "FKIKSystem" -p "MotionSystem";
createNode transform -n "FKIKParentConstraintArm1_R" -p "FKIKSystem";
createNode transform -n "FKIKArm1_R" -p "FKIKParentConstraintArm1_R";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend";
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis";
	setAttr -k on ".IKVis" no;
	setAttr -l on ".startJoint" -type "string" "Shoulder1";
	setAttr -l on ".middleJoint" -type "string" "Elbow1";
	setAttr -l on ".endJoint" -type "string" "Wrist2";
createNode nurbsCurve -n "FKIKArm1_RShape" -p "FKIKArm1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		-0.3080633183 -0.07701582958 -6.8403797810000007e-17
		-0.29706105700000002 -0.07701582958 -6.5960805029999992e-17
		-0.28605879560000003 -0.066013568209999998 -6.3517812250000002e-17
		-0.28605879560000003 0.066013568209999998 -6.3517812250000002e-17
		-0.29706105700000002 0.07701582958 -6.5960805029999992e-17
		-0.3080633183 0.07701582958 -6.8403797810000007e-17
		-0.3080633183 0.088018090950000002 -6.8403797810000007e-17
		-0.11002261369999999 0.088018090950000002 -2.4429927789999999e-17
		-0.11002261369999999 0.07701582958 -2.4429927789999999e-17
		-0.1210248751 0.07701582958 -2.6872920570000002e-17
		-0.13202713639999999 0.066013568209999998 -2.9315913349999996e-17
		-0.13202713639999999 0.01100226137 -2.9315913349999996e-17
		-0.066013568209999998 0.066013568209999998 -1.465795667e-17
		-0.07701582958 0.07701582958 -1.710094945e-17
		-0.088018090950000002 0.07701582958 -1.954394223e-17
		-0.088018090950000002 0.088018090950000002 -1.954394223e-17
		-0.022004522740000001 0.088018090950000002 -4.8859855579999998e-18
		-0.022004522740000001 0.07701582958 -4.8859855579999998e-18
		-0.03300678411 0.07701582958 -7.3289783369999998e-18
		-0.11002261369999999 0.01100226137 -2.4429927789999999e-17
		-0.022004522740000001 -0.07701582958 -4.8859855579999998e-18
		-0.01100226137 -0.07701582958 -2.4429927789999999e-18
		0.044009045480000002 0.088018090950000002 9.7719711159999997e-18
		0.209042966 0.088018090950000002 4.6416862800000002e-17
		0.209042966 0.07701582958 4.6416862800000002e-17
		0.19804070460000001 0.07701582958 4.3973870020000005e-17
		0.1870384433 0.066013568209999998 4.1530877239999996e-17
		0.1870384433 0.01100226137 4.1530877239999996e-17
		0.25305201150000001 0.066013568209999998 5.6188833920000001e-17
		0.24204975009999999 0.07701582958 5.3745841140000004e-17
		0.23104748880000001 0.07701582958 5.1302848360000001e-17
		0.23104748880000001 0.088018090950000002 5.1302848360000001e-17
		0.29706105700000002 0.088018090950000002 6.5960805029999992e-17
		0.29706105700000002 0.07701582958 6.5960805029999992e-17
		0.28605879560000003 0.07701582958 6.3517812250000002e-17
		0.209042966 0.01100226137 4.6416862800000002e-17
		0.29706105700000002 -0.07701582958 6.5960805029999992e-17
		0.3080633183 -0.07701582958 6.8403797810000007e-17
		0.3080633183 -0.088018090950000002 6.8403797810000007e-17
		0.24204975009999999 -0.088018090950000002 5.3745841140000004e-17
		0.24204975009999999 -0.07701582958 5.3745841140000004e-17
		0.25305201150000001 -0.07701582958 5.6188833920000001e-17
		0.1870384433 -0.01100226137 4.1530877239999996e-17
		0.1870384433 -0.066013568209999998 4.1530877239999996e-17
		0.19804070460000001 -0.07701582958 4.3973870020000005e-17
		0.209042966 -0.07701582958 4.6416862800000002e-17
		0.209042966 -0.088018090950000002 4.6416862800000002e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.1540316592 -0.07701582958 3.4201898910000001e-17
		0.16503392049999999 -0.066013568209999998 3.6644891679999997e-17
		0.16503392049999999 0.066013568209999998 3.6644891679999997e-17
		0.1540316592 0.07701582958 3.4201898910000001e-17
		0.1210248751 0.07701582958 2.6872920570000002e-17
		0.11002261369999999 0.066013568209999998 2.4429927789999999e-17
		0.11002261369999999 -0.066013568209999998 2.4429927789999999e-17
		0.1210248751 -0.07701582958 2.6872920570000002e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.066013568209999998 -0.088018090950000002 1.465795667e-17
		0.066013568209999998 -0.07701582958 1.465795667e-17
		0.07701582958 -0.07701582958 1.710094945e-17
		0.088018090950000002 -0.066013568209999998 1.954394223e-17
		0.088018090950000002 0.066013568209999998 1.954394223e-17
		0.07701582958 0.07701582958 1.710094945e-17
		0.055011306849999997 0.07701582958 1.2214963889999999e-17
		0 -0.07701582958 0
		0 -0.088018090950000002 0
		-0.07701582958 -0.088018090950000002 -1.710094945e-17
		-0.07701582958 -0.07701582958 -1.710094945e-17
		-0.066013568209999998 -0.07701582958 -1.465795667e-17
		-0.13202713639999999 -0.01100226137 -2.9315913349999996e-17
		-0.13202713639999999 -0.066013568209999998 -2.9315913349999996e-17
		-0.1210248751 -0.07701582958 -2.6872920570000002e-17
		-0.11002261369999999 -0.07701582958 -2.4429927789999999e-17
		-0.11002261369999999 -0.088018090950000002 -2.4429927789999999e-17
		-0.1760361819 -0.088018090950000002 -3.9087884459999999e-17
		-0.1760361819 -0.07701582958 -3.9087884459999999e-17
		-0.16503392049999999 -0.07701582958 -3.6644891679999997e-17
		-0.1540316592 -0.066013568209999998 -3.4201898910000001e-17
		-0.1540316592 0.066013568209999998 -3.4201898910000001e-17
		-0.16503392049999999 0.07701582958 -3.6644891679999997e-17
		-0.1870384433 0.07701582958 -4.1530877239999996e-17
		-0.1870384433 0.044009045480000002 -4.1530877239999996e-17
		-0.19804070460000001 0.044009045480000002 -4.3973870020000005e-17
		-0.19804070460000001 0.066013568209999998 -4.3973870020000005e-17
		-0.209042966 0.07701582958 -4.6416862800000002e-17
		-0.25305201150000001 0.07701582958 -5.6188833920000001e-17
		-0.2640542729 0.066013568209999998 -5.8631826699999991e-17
		-0.2640542729 0.01100226137 -5.8631826699999991e-17
		-0.23104748880000001 0.01100226137 -5.1302848360000001e-17
		-0.22004522739999999 0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 0.03300678411 -4.8859855579999998e-17
		-0.209042966 0.03300678411 -4.6416862800000002e-17
		-0.209042966 -0.022004522740000001 -4.6416862800000002e-17
		-0.22004522739999999 -0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 -0.01100226137 -4.8859855579999998e-17
		-0.23104748880000001 0 -5.1302848360000001e-17
		-0.2640542729 0 -5.8631826699999991e-17
		-0.2640542729 -0.066013568209999998 -5.8631826699999991e-17
		-0.25305201150000001 -0.07701582958 -5.6188833920000001e-17
		-0.24204975009999999 -0.07701582958 -5.3745841140000004e-17
		-0.24204975009999999 -0.088018090950000002 -5.3745841140000004e-17
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		;
createNode parentConstraint -n "FKIKParentConstraintArm1_R_parentConstraint1" -p "FKIKParentConstraintArm1_R";
	addAttr -ci true -k true -sn "w0" -ln "Clavicle_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.1965311514996448 2.8674605780834477 0.1951350987088869 ;
	setAttr ".tg[0].tor" -type "double3" 90.000000348125525 -4.0819350600393633 -84.931295105331486 ;
	setAttr ".lr" -type "double3" -8.9193612684955714e-08 -3.2818719963606738e-06 -1.6307861261972776e-06 ;
	setAttr ".rst" -type "double3" -4.1510712664146743 9.9411608139861656 0.57186814674813269 ;
	setAttr ".rsrr" -type "double3" 3.1805546814635164e-15 3.1805546814635168e-15 -9.5416640443905503e-15 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintArm_L" -p "FKIKSystem";
createNode transform -n "FKIKArm_L" -p "FKIKParentConstraintArm_L";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend";
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis";
	setAttr -k on ".IKVis" no;
	setAttr -l on ".startJoint" -type "string" "Shoulder";
	setAttr -l on ".middleJoint" -type "string" "Elbow";
	setAttr -l on ".endJoint" -type "string" "Wrist";
createNode nurbsCurve -n "FKIKArm_LShape" -p "FKIKArm_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		-0.3080633183 -0.07701582958 -6.8403797810000007e-17
		-0.29706105700000002 -0.07701582958 -6.5960805029999992e-17
		-0.28605879560000003 -0.066013568209999998 -6.3517812250000002e-17
		-0.28605879560000003 0.066013568209999998 -6.3517812250000002e-17
		-0.29706105700000002 0.07701582958 -6.5960805029999992e-17
		-0.3080633183 0.07701582958 -6.8403797810000007e-17
		-0.3080633183 0.088018090950000002 -6.8403797810000007e-17
		-0.11002261369999999 0.088018090950000002 -2.4429927789999999e-17
		-0.11002261369999999 0.07701582958 -2.4429927789999999e-17
		-0.1210248751 0.07701582958 -2.6872920570000002e-17
		-0.13202713639999999 0.066013568209999998 -2.9315913349999996e-17
		-0.13202713639999999 0.01100226137 -2.9315913349999996e-17
		-0.066013568209999998 0.066013568209999998 -1.465795667e-17
		-0.07701582958 0.07701582958 -1.710094945e-17
		-0.088018090950000002 0.07701582958 -1.954394223e-17
		-0.088018090950000002 0.088018090950000002 -1.954394223e-17
		-0.022004522740000001 0.088018090950000002 -4.8859855579999998e-18
		-0.022004522740000001 0.07701582958 -4.8859855579999998e-18
		-0.03300678411 0.07701582958 -7.3289783369999998e-18
		-0.11002261369999999 0.01100226137 -2.4429927789999999e-17
		-0.022004522740000001 -0.07701582958 -4.8859855579999998e-18
		-0.01100226137 -0.07701582958 -2.4429927789999999e-18
		0.044009045480000002 0.088018090950000002 9.7719711159999997e-18
		0.209042966 0.088018090950000002 4.6416862800000002e-17
		0.209042966 0.07701582958 4.6416862800000002e-17
		0.19804070460000001 0.07701582958 4.3973870020000005e-17
		0.1870384433 0.066013568209999998 4.1530877239999996e-17
		0.1870384433 0.01100226137 4.1530877239999996e-17
		0.25305201150000001 0.066013568209999998 5.6188833920000001e-17
		0.24204975009999999 0.07701582958 5.3745841140000004e-17
		0.23104748880000001 0.07701582958 5.1302848360000001e-17
		0.23104748880000001 0.088018090950000002 5.1302848360000001e-17
		0.29706105700000002 0.088018090950000002 6.5960805029999992e-17
		0.29706105700000002 0.07701582958 6.5960805029999992e-17
		0.28605879560000003 0.07701582958 6.3517812250000002e-17
		0.209042966 0.01100226137 4.6416862800000002e-17
		0.29706105700000002 -0.07701582958 6.5960805029999992e-17
		0.3080633183 -0.07701582958 6.8403797810000007e-17
		0.3080633183 -0.088018090950000002 6.8403797810000007e-17
		0.24204975009999999 -0.088018090950000002 5.3745841140000004e-17
		0.24204975009999999 -0.07701582958 5.3745841140000004e-17
		0.25305201150000001 -0.07701582958 5.6188833920000001e-17
		0.1870384433 -0.01100226137 4.1530877239999996e-17
		0.1870384433 -0.066013568209999998 4.1530877239999996e-17
		0.19804070460000001 -0.07701582958 4.3973870020000005e-17
		0.209042966 -0.07701582958 4.6416862800000002e-17
		0.209042966 -0.088018090950000002 4.6416862800000002e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.1540316592 -0.07701582958 3.4201898910000001e-17
		0.16503392049999999 -0.066013568209999998 3.6644891679999997e-17
		0.16503392049999999 0.066013568209999998 3.6644891679999997e-17
		0.1540316592 0.07701582958 3.4201898910000001e-17
		0.1210248751 0.07701582958 2.6872920570000002e-17
		0.11002261369999999 0.066013568209999998 2.4429927789999999e-17
		0.11002261369999999 -0.066013568209999998 2.4429927789999999e-17
		0.1210248751 -0.07701582958 2.6872920570000002e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.066013568209999998 -0.088018090950000002 1.465795667e-17
		0.066013568209999998 -0.07701582958 1.465795667e-17
		0.07701582958 -0.07701582958 1.710094945e-17
		0.088018090950000002 -0.066013568209999998 1.954394223e-17
		0.088018090950000002 0.066013568209999998 1.954394223e-17
		0.07701582958 0.07701582958 1.710094945e-17
		0.055011306849999997 0.07701582958 1.2214963889999999e-17
		0 -0.07701582958 0
		0 -0.088018090950000002 0
		-0.07701582958 -0.088018090950000002 -1.710094945e-17
		-0.07701582958 -0.07701582958 -1.710094945e-17
		-0.066013568209999998 -0.07701582958 -1.465795667e-17
		-0.13202713639999999 -0.01100226137 -2.9315913349999996e-17
		-0.13202713639999999 -0.066013568209999998 -2.9315913349999996e-17
		-0.1210248751 -0.07701582958 -2.6872920570000002e-17
		-0.11002261369999999 -0.07701582958 -2.4429927789999999e-17
		-0.11002261369999999 -0.088018090950000002 -2.4429927789999999e-17
		-0.1760361819 -0.088018090950000002 -3.9087884459999999e-17
		-0.1760361819 -0.07701582958 -3.9087884459999999e-17
		-0.16503392049999999 -0.07701582958 -3.6644891679999997e-17
		-0.1540316592 -0.066013568209999998 -3.4201898910000001e-17
		-0.1540316592 0.066013568209999998 -3.4201898910000001e-17
		-0.16503392049999999 0.07701582958 -3.6644891679999997e-17
		-0.1870384433 0.07701582958 -4.1530877239999996e-17
		-0.1870384433 0.044009045480000002 -4.1530877239999996e-17
		-0.19804070460000001 0.044009045480000002 -4.3973870020000005e-17
		-0.19804070460000001 0.066013568209999998 -4.3973870020000005e-17
		-0.209042966 0.07701582958 -4.6416862800000002e-17
		-0.25305201150000001 0.07701582958 -5.6188833920000001e-17
		-0.2640542729 0.066013568209999998 -5.8631826699999991e-17
		-0.2640542729 0.01100226137 -5.8631826699999991e-17
		-0.23104748880000001 0.01100226137 -5.1302848360000001e-17
		-0.22004522739999999 0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 0.03300678411 -4.8859855579999998e-17
		-0.209042966 0.03300678411 -4.6416862800000002e-17
		-0.209042966 -0.022004522740000001 -4.6416862800000002e-17
		-0.22004522739999999 -0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 -0.01100226137 -4.8859855579999998e-17
		-0.23104748880000001 0 -5.1302848360000001e-17
		-0.2640542729 0 -5.8631826699999991e-17
		-0.2640542729 -0.066013568209999998 -5.8631826699999991e-17
		-0.25305201150000001 -0.07701582958 -5.6188833920000001e-17
		-0.24204975009999999 -0.07701582958 -5.3745841140000004e-17
		-0.24204975009999999 -0.088018090950000002 -5.3745841140000004e-17
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		;
createNode parentConstraint -n "FKIKParentConstraintArm_L_parentConstraint1" -p "FKIKParentConstraintArm_L";
	addAttr -ci true -k true -sn "w0" -ln "Clavicle1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.5264430589988809 -2.1310261835305693 0.9694743986021841 ;
	setAttr ".tg[0].tor" -type "double3" -89.999999999999972 3.8335568439474369e-07 
		-89.999996197111585 ;
	setAttr ".lr" -type "double3" 2.0037494507251504e-13 6.3611049430515734e-15 2.5276634249213337e-06 ;
	setAttr ".rst" -type "double3" 3.3415360757294978 8.5662055871395051 -1.8930862004413149 ;
	setAttr ".rsrr" -type "double3" 6.3611093735672936e-15 -3.1805546601830019e-15 -3.8335569075585376e-07 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintLeg_R" -p "FKIKSystem";
createNode transform -n "FKIKLeg_R" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Hip";
	setAttr -l on ".middleJoint" -type "string" "Knee";
	setAttr -l on ".endJoint" -type "string" "Ankle";
createNode nurbsCurve -n "FKIKLeg_RShape" -p "FKIKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		-0.3080633183 -0.07701582958 -6.8403797810000007e-17
		-0.29706105700000002 -0.07701582958 -6.5960805029999992e-17
		-0.28605879560000003 -0.066013568209999998 -6.3517812250000002e-17
		-0.28605879560000003 0.066013568209999998 -6.3517812250000002e-17
		-0.29706105700000002 0.07701582958 -6.5960805029999992e-17
		-0.3080633183 0.07701582958 -6.8403797810000007e-17
		-0.3080633183 0.088018090950000002 -6.8403797810000007e-17
		-0.11002261369999999 0.088018090950000002 -2.4429927789999999e-17
		-0.11002261369999999 0.07701582958 -2.4429927789999999e-17
		-0.1210248751 0.07701582958 -2.6872920570000002e-17
		-0.13202713639999999 0.066013568209999998 -2.9315913349999996e-17
		-0.13202713639999999 0.01100226137 -2.9315913349999996e-17
		-0.066013568209999998 0.066013568209999998 -1.465795667e-17
		-0.07701582958 0.07701582958 -1.710094945e-17
		-0.088018090950000002 0.07701582958 -1.954394223e-17
		-0.088018090950000002 0.088018090950000002 -1.954394223e-17
		-0.022004522740000001 0.088018090950000002 -4.8859855579999998e-18
		-0.022004522740000001 0.07701582958 -4.8859855579999998e-18
		-0.03300678411 0.07701582958 -7.3289783369999998e-18
		-0.11002261369999999 0.01100226137 -2.4429927789999999e-17
		-0.022004522740000001 -0.07701582958 -4.8859855579999998e-18
		-0.01100226137 -0.07701582958 -2.4429927789999999e-18
		0.044009045480000002 0.088018090950000002 9.7719711159999997e-18
		0.209042966 0.088018090950000002 4.6416862800000002e-17
		0.209042966 0.07701582958 4.6416862800000002e-17
		0.19804070460000001 0.07701582958 4.3973870020000005e-17
		0.1870384433 0.066013568209999998 4.1530877239999996e-17
		0.1870384433 0.01100226137 4.1530877239999996e-17
		0.25305201150000001 0.066013568209999998 5.6188833920000001e-17
		0.24204975009999999 0.07701582958 5.3745841140000004e-17
		0.23104748880000001 0.07701582958 5.1302848360000001e-17
		0.23104748880000001 0.088018090950000002 5.1302848360000001e-17
		0.29706105700000002 0.088018090950000002 6.5960805029999992e-17
		0.29706105700000002 0.07701582958 6.5960805029999992e-17
		0.28605879560000003 0.07701582958 6.3517812250000002e-17
		0.209042966 0.01100226137 4.6416862800000002e-17
		0.29706105700000002 -0.07701582958 6.5960805029999992e-17
		0.3080633183 -0.07701582958 6.8403797810000007e-17
		0.3080633183 -0.088018090950000002 6.8403797810000007e-17
		0.24204975009999999 -0.088018090950000002 5.3745841140000004e-17
		0.24204975009999999 -0.07701582958 5.3745841140000004e-17
		0.25305201150000001 -0.07701582958 5.6188833920000001e-17
		0.1870384433 -0.01100226137 4.1530877239999996e-17
		0.1870384433 -0.066013568209999998 4.1530877239999996e-17
		0.19804070460000001 -0.07701582958 4.3973870020000005e-17
		0.209042966 -0.07701582958 4.6416862800000002e-17
		0.209042966 -0.088018090950000002 4.6416862800000002e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.1540316592 -0.07701582958 3.4201898910000001e-17
		0.16503392049999999 -0.066013568209999998 3.6644891679999997e-17
		0.16503392049999999 0.066013568209999998 3.6644891679999997e-17
		0.1540316592 0.07701582958 3.4201898910000001e-17
		0.1210248751 0.07701582958 2.6872920570000002e-17
		0.11002261369999999 0.066013568209999998 2.4429927789999999e-17
		0.11002261369999999 -0.066013568209999998 2.4429927789999999e-17
		0.1210248751 -0.07701582958 2.6872920570000002e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.066013568209999998 -0.088018090950000002 1.465795667e-17
		0.066013568209999998 -0.07701582958 1.465795667e-17
		0.07701582958 -0.07701582958 1.710094945e-17
		0.088018090950000002 -0.066013568209999998 1.954394223e-17
		0.088018090950000002 0.066013568209999998 1.954394223e-17
		0.07701582958 0.07701582958 1.710094945e-17
		0.055011306849999997 0.07701582958 1.2214963889999999e-17
		0 -0.07701582958 0
		0 -0.088018090950000002 0
		-0.07701582958 -0.088018090950000002 -1.710094945e-17
		-0.07701582958 -0.07701582958 -1.710094945e-17
		-0.066013568209999998 -0.07701582958 -1.465795667e-17
		-0.13202713639999999 -0.01100226137 -2.9315913349999996e-17
		-0.13202713639999999 -0.066013568209999998 -2.9315913349999996e-17
		-0.1210248751 -0.07701582958 -2.6872920570000002e-17
		-0.11002261369999999 -0.07701582958 -2.4429927789999999e-17
		-0.11002261369999999 -0.088018090950000002 -2.4429927789999999e-17
		-0.1760361819 -0.088018090950000002 -3.9087884459999999e-17
		-0.1760361819 -0.07701582958 -3.9087884459999999e-17
		-0.16503392049999999 -0.07701582958 -3.6644891679999997e-17
		-0.1540316592 -0.066013568209999998 -3.4201898910000001e-17
		-0.1540316592 0.066013568209999998 -3.4201898910000001e-17
		-0.16503392049999999 0.07701582958 -3.6644891679999997e-17
		-0.1870384433 0.07701582958 -4.1530877239999996e-17
		-0.1870384433 0.044009045480000002 -4.1530877239999996e-17
		-0.19804070460000001 0.044009045480000002 -4.3973870020000005e-17
		-0.19804070460000001 0.066013568209999998 -4.3973870020000005e-17
		-0.209042966 0.07701582958 -4.6416862800000002e-17
		-0.25305201150000001 0.07701582958 -5.6188833920000001e-17
		-0.2640542729 0.066013568209999998 -5.8631826699999991e-17
		-0.2640542729 0.01100226137 -5.8631826699999991e-17
		-0.23104748880000001 0.01100226137 -5.1302848360000001e-17
		-0.22004522739999999 0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 0.03300678411 -4.8859855579999998e-17
		-0.209042966 0.03300678411 -4.6416862800000002e-17
		-0.209042966 -0.022004522740000001 -4.6416862800000002e-17
		-0.22004522739999999 -0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 -0.01100226137 -4.8859855579999998e-17
		-0.23104748880000001 0 -5.1302848360000001e-17
		-0.2640542729 0 -5.8631826699999991e-17
		-0.2640542729 -0.066013568209999998 -5.8631826699999991e-17
		-0.25305201150000001 -0.07701582958 -5.6188833920000001e-17
		-0.24204975009999999 -0.07701582958 -5.3745841140000004e-17
		-0.24204975009999999 -0.088018090950000002 -5.3745841140000004e-17
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_R_parentConstraint1" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.0944882580262307 2.1177887433849927 0.25856700863520043 ;
	setAttr ".tg[0].tor" -type "double3" -154.86042096709363 1.3115279850922298e-13 
		-90.000000000000284 ;
	setAttr ".lr" -type "double3" -2.7257923593856568e-06 9.6318147152001064e-07 2.052479627046354e-06 ;
	setAttr ".rst" -type "double3" -3.3297738546215703 4.2065345965435128 -1.0117836507656142 ;
	setAttr ".rsrr" -type "double3" -1.2722218725854124e-14 1.1449996853268662e-13 -5.7249984266343308e-14 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintSpine_M" -p "FKIKSystem";
createNode transform -n "FKIKSpine_M" -p "FKIKParentConstraintSpine_M";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend";
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis";
	setAttr -k on ".IKVis" no;
	setAttr -l on ".startJoint" -type "string" "Pelvis";
	setAttr -l on ".middleJoint" -type "string" "SpineA";
	setAttr -l on ".endJoint" -type "string" "Chest";
createNode nurbsCurve -n "FKIKSpine_MShape" -p "FKIKSpine_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		-0.3080633183 -0.07701582958 -6.8403797810000007e-17
		-0.29706105700000002 -0.07701582958 -6.5960805029999992e-17
		-0.28605879560000003 -0.066013568209999998 -6.3517812250000002e-17
		-0.28605879560000003 0.066013568209999998 -6.3517812250000002e-17
		-0.29706105700000002 0.07701582958 -6.5960805029999992e-17
		-0.3080633183 0.07701582958 -6.8403797810000007e-17
		-0.3080633183 0.088018090950000002 -6.8403797810000007e-17
		-0.11002261369999999 0.088018090950000002 -2.4429927789999999e-17
		-0.11002261369999999 0.07701582958 -2.4429927789999999e-17
		-0.1210248751 0.07701582958 -2.6872920570000002e-17
		-0.13202713639999999 0.066013568209999998 -2.9315913349999996e-17
		-0.13202713639999999 0.01100226137 -2.9315913349999996e-17
		-0.066013568209999998 0.066013568209999998 -1.465795667e-17
		-0.07701582958 0.07701582958 -1.710094945e-17
		-0.088018090950000002 0.07701582958 -1.954394223e-17
		-0.088018090950000002 0.088018090950000002 -1.954394223e-17
		-0.022004522740000001 0.088018090950000002 -4.8859855579999998e-18
		-0.022004522740000001 0.07701582958 -4.8859855579999998e-18
		-0.03300678411 0.07701582958 -7.3289783369999998e-18
		-0.11002261369999999 0.01100226137 -2.4429927789999999e-17
		-0.022004522740000001 -0.07701582958 -4.8859855579999998e-18
		-0.01100226137 -0.07701582958 -2.4429927789999999e-18
		0.044009045480000002 0.088018090950000002 9.7719711159999997e-18
		0.209042966 0.088018090950000002 4.6416862800000002e-17
		0.209042966 0.07701582958 4.6416862800000002e-17
		0.19804070460000001 0.07701582958 4.3973870020000005e-17
		0.1870384433 0.066013568209999998 4.1530877239999996e-17
		0.1870384433 0.01100226137 4.1530877239999996e-17
		0.25305201150000001 0.066013568209999998 5.6188833920000001e-17
		0.24204975009999999 0.07701582958 5.3745841140000004e-17
		0.23104748880000001 0.07701582958 5.1302848360000001e-17
		0.23104748880000001 0.088018090950000002 5.1302848360000001e-17
		0.29706105700000002 0.088018090950000002 6.5960805029999992e-17
		0.29706105700000002 0.07701582958 6.5960805029999992e-17
		0.28605879560000003 0.07701582958 6.3517812250000002e-17
		0.209042966 0.01100226137 4.6416862800000002e-17
		0.29706105700000002 -0.07701582958 6.5960805029999992e-17
		0.3080633183 -0.07701582958 6.8403797810000007e-17
		0.3080633183 -0.088018090950000002 6.8403797810000007e-17
		0.24204975009999999 -0.088018090950000002 5.3745841140000004e-17
		0.24204975009999999 -0.07701582958 5.3745841140000004e-17
		0.25305201150000001 -0.07701582958 5.6188833920000001e-17
		0.1870384433 -0.01100226137 4.1530877239999996e-17
		0.1870384433 -0.066013568209999998 4.1530877239999996e-17
		0.19804070460000001 -0.07701582958 4.3973870020000005e-17
		0.209042966 -0.07701582958 4.6416862800000002e-17
		0.209042966 -0.088018090950000002 4.6416862800000002e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.1540316592 -0.07701582958 3.4201898910000001e-17
		0.16503392049999999 -0.066013568209999998 3.6644891679999997e-17
		0.16503392049999999 0.066013568209999998 3.6644891679999997e-17
		0.1540316592 0.07701582958 3.4201898910000001e-17
		0.1210248751 0.07701582958 2.6872920570000002e-17
		0.11002261369999999 0.066013568209999998 2.4429927789999999e-17
		0.11002261369999999 -0.066013568209999998 2.4429927789999999e-17
		0.1210248751 -0.07701582958 2.6872920570000002e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.066013568209999998 -0.088018090950000002 1.465795667e-17
		0.066013568209999998 -0.07701582958 1.465795667e-17
		0.07701582958 -0.07701582958 1.710094945e-17
		0.088018090950000002 -0.066013568209999998 1.954394223e-17
		0.088018090950000002 0.066013568209999998 1.954394223e-17
		0.07701582958 0.07701582958 1.710094945e-17
		0.055011306849999997 0.07701582958 1.2214963889999999e-17
		0 -0.07701582958 0
		0 -0.088018090950000002 0
		-0.07701582958 -0.088018090950000002 -1.710094945e-17
		-0.07701582958 -0.07701582958 -1.710094945e-17
		-0.066013568209999998 -0.07701582958 -1.465795667e-17
		-0.13202713639999999 -0.01100226137 -2.9315913349999996e-17
		-0.13202713639999999 -0.066013568209999998 -2.9315913349999996e-17
		-0.1210248751 -0.07701582958 -2.6872920570000002e-17
		-0.11002261369999999 -0.07701582958 -2.4429927789999999e-17
		-0.11002261369999999 -0.088018090950000002 -2.4429927789999999e-17
		-0.1760361819 -0.088018090950000002 -3.9087884459999999e-17
		-0.1760361819 -0.07701582958 -3.9087884459999999e-17
		-0.16503392049999999 -0.07701582958 -3.6644891679999997e-17
		-0.1540316592 -0.066013568209999998 -3.4201898910000001e-17
		-0.1540316592 0.066013568209999998 -3.4201898910000001e-17
		-0.16503392049999999 0.07701582958 -3.6644891679999997e-17
		-0.1870384433 0.07701582958 -4.1530877239999996e-17
		-0.1870384433 0.044009045480000002 -4.1530877239999996e-17
		-0.19804070460000001 0.044009045480000002 -4.3973870020000005e-17
		-0.19804070460000001 0.066013568209999998 -4.3973870020000005e-17
		-0.209042966 0.07701582958 -4.6416862800000002e-17
		-0.25305201150000001 0.07701582958 -5.6188833920000001e-17
		-0.2640542729 0.066013568209999998 -5.8631826699999991e-17
		-0.2640542729 0.01100226137 -5.8631826699999991e-17
		-0.23104748880000001 0.01100226137 -5.1302848360000001e-17
		-0.22004522739999999 0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 0.03300678411 -4.8859855579999998e-17
		-0.209042966 0.03300678411 -4.6416862800000002e-17
		-0.209042966 -0.022004522740000001 -4.6416862800000002e-17
		-0.22004522739999999 -0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 -0.01100226137 -4.8859855579999998e-17
		-0.23104748880000001 0 -5.1302848360000001e-17
		-0.2640542729 0 -5.8631826699999991e-17
		-0.2640542729 -0.066013568209999998 -5.8631826699999991e-17
		-0.25305201150000001 -0.07701582958 -5.6188833920000001e-17
		-0.24204975009999999 -0.07701582958 -5.3745841140000004e-17
		-0.24204975009999999 -0.088018090950000002 -5.3745841140000004e-17
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		;
createNode parentConstraint -n "FKIKParentConstraintSpine_M_parentConstraint1" -p
		 "FKIKParentConstraintSpine_M";
	addAttr -ci true -k true -sn "w0" -ln "FKPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.3811564033360575 0.5661032199859708 -0.00012782198190919303 ;
	setAttr ".rst" -type "double3" 1.3811564629407023 5.8151638507843106 -1.2426165340542821 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintLeg_L" -p "FKIKSystem";
createNode transform -n "FKIKLeg_L" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Hip";
	setAttr -l on ".middleJoint" -type "string" "Knee";
	setAttr -l on ".endJoint" -type "string" "Ankle";
createNode nurbsCurve -n "FKIKLeg_LShape" -p "FKIKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		-0.3080633183 -0.07701582958 -6.8403797810000007e-17
		-0.29706105700000002 -0.07701582958 -6.5960805029999992e-17
		-0.28605879560000003 -0.066013568209999998 -6.3517812250000002e-17
		-0.28605879560000003 0.066013568209999998 -6.3517812250000002e-17
		-0.29706105700000002 0.07701582958 -6.5960805029999992e-17
		-0.3080633183 0.07701582958 -6.8403797810000007e-17
		-0.3080633183 0.088018090950000002 -6.8403797810000007e-17
		-0.11002261369999999 0.088018090950000002 -2.4429927789999999e-17
		-0.11002261369999999 0.07701582958 -2.4429927789999999e-17
		-0.1210248751 0.07701582958 -2.6872920570000002e-17
		-0.13202713639999999 0.066013568209999998 -2.9315913349999996e-17
		-0.13202713639999999 0.01100226137 -2.9315913349999996e-17
		-0.066013568209999998 0.066013568209999998 -1.465795667e-17
		-0.07701582958 0.07701582958 -1.710094945e-17
		-0.088018090950000002 0.07701582958 -1.954394223e-17
		-0.088018090950000002 0.088018090950000002 -1.954394223e-17
		-0.022004522740000001 0.088018090950000002 -4.8859855579999998e-18
		-0.022004522740000001 0.07701582958 -4.8859855579999998e-18
		-0.03300678411 0.07701582958 -7.3289783369999998e-18
		-0.11002261369999999 0.01100226137 -2.4429927789999999e-17
		-0.022004522740000001 -0.07701582958 -4.8859855579999998e-18
		-0.01100226137 -0.07701582958 -2.4429927789999999e-18
		0.044009045480000002 0.088018090950000002 9.7719711159999997e-18
		0.209042966 0.088018090950000002 4.6416862800000002e-17
		0.209042966 0.07701582958 4.6416862800000002e-17
		0.19804070460000001 0.07701582958 4.3973870020000005e-17
		0.1870384433 0.066013568209999998 4.1530877239999996e-17
		0.1870384433 0.01100226137 4.1530877239999996e-17
		0.25305201150000001 0.066013568209999998 5.6188833920000001e-17
		0.24204975009999999 0.07701582958 5.3745841140000004e-17
		0.23104748880000001 0.07701582958 5.1302848360000001e-17
		0.23104748880000001 0.088018090950000002 5.1302848360000001e-17
		0.29706105700000002 0.088018090950000002 6.5960805029999992e-17
		0.29706105700000002 0.07701582958 6.5960805029999992e-17
		0.28605879560000003 0.07701582958 6.3517812250000002e-17
		0.209042966 0.01100226137 4.6416862800000002e-17
		0.29706105700000002 -0.07701582958 6.5960805029999992e-17
		0.3080633183 -0.07701582958 6.8403797810000007e-17
		0.3080633183 -0.088018090950000002 6.8403797810000007e-17
		0.24204975009999999 -0.088018090950000002 5.3745841140000004e-17
		0.24204975009999999 -0.07701582958 5.3745841140000004e-17
		0.25305201150000001 -0.07701582958 5.6188833920000001e-17
		0.1870384433 -0.01100226137 4.1530877239999996e-17
		0.1870384433 -0.066013568209999998 4.1530877239999996e-17
		0.19804070460000001 -0.07701582958 4.3973870020000005e-17
		0.209042966 -0.07701582958 4.6416862800000002e-17
		0.209042966 -0.088018090950000002 4.6416862800000002e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.1540316592 -0.07701582958 3.4201898910000001e-17
		0.16503392049999999 -0.066013568209999998 3.6644891679999997e-17
		0.16503392049999999 0.066013568209999998 3.6644891679999997e-17
		0.1540316592 0.07701582958 3.4201898910000001e-17
		0.1210248751 0.07701582958 2.6872920570000002e-17
		0.11002261369999999 0.066013568209999998 2.4429927789999999e-17
		0.11002261369999999 -0.066013568209999998 2.4429927789999999e-17
		0.1210248751 -0.07701582958 2.6872920570000002e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.066013568209999998 -0.088018090950000002 1.465795667e-17
		0.066013568209999998 -0.07701582958 1.465795667e-17
		0.07701582958 -0.07701582958 1.710094945e-17
		0.088018090950000002 -0.066013568209999998 1.954394223e-17
		0.088018090950000002 0.066013568209999998 1.954394223e-17
		0.07701582958 0.07701582958 1.710094945e-17
		0.055011306849999997 0.07701582958 1.2214963889999999e-17
		0 -0.07701582958 0
		0 -0.088018090950000002 0
		-0.07701582958 -0.088018090950000002 -1.710094945e-17
		-0.07701582958 -0.07701582958 -1.710094945e-17
		-0.066013568209999998 -0.07701582958 -1.465795667e-17
		-0.13202713639999999 -0.01100226137 -2.9315913349999996e-17
		-0.13202713639999999 -0.066013568209999998 -2.9315913349999996e-17
		-0.1210248751 -0.07701582958 -2.6872920570000002e-17
		-0.11002261369999999 -0.07701582958 -2.4429927789999999e-17
		-0.11002261369999999 -0.088018090950000002 -2.4429927789999999e-17
		-0.1760361819 -0.088018090950000002 -3.9087884459999999e-17
		-0.1760361819 -0.07701582958 -3.9087884459999999e-17
		-0.16503392049999999 -0.07701582958 -3.6644891679999997e-17
		-0.1540316592 -0.066013568209999998 -3.4201898910000001e-17
		-0.1540316592 0.066013568209999998 -3.4201898910000001e-17
		-0.16503392049999999 0.07701582958 -3.6644891679999997e-17
		-0.1870384433 0.07701582958 -4.1530877239999996e-17
		-0.1870384433 0.044009045480000002 -4.1530877239999996e-17
		-0.19804070460000001 0.044009045480000002 -4.3973870020000005e-17
		-0.19804070460000001 0.066013568209999998 -4.3973870020000005e-17
		-0.209042966 0.07701582958 -4.6416862800000002e-17
		-0.25305201150000001 0.07701582958 -5.6188833920000001e-17
		-0.2640542729 0.066013568209999998 -5.8631826699999991e-17
		-0.2640542729 0.01100226137 -5.8631826699999991e-17
		-0.23104748880000001 0.01100226137 -5.1302848360000001e-17
		-0.22004522739999999 0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 0.03300678411 -4.8859855579999998e-17
		-0.209042966 0.03300678411 -4.6416862800000002e-17
		-0.209042966 -0.022004522740000001 -4.6416862800000002e-17
		-0.22004522739999999 -0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 -0.01100226137 -4.8859855579999998e-17
		-0.23104748880000001 0 -5.1302848360000001e-17
		-0.2640542729 0 -5.8631826699999991e-17
		-0.2640542729 -0.066013568209999998 -5.8631826699999991e-17
		-0.25305201150000001 -0.07701582958 -5.6188833920000001e-17
		-0.24204975009999999 -0.07701582958 -5.3745841140000004e-17
		-0.24204975009999999 -0.088018090950000002 -5.3745841140000004e-17
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_L_parentConstraint1" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.0944882580262325 -2.1177887433849918 -0.25856700863519611 ;
	setAttr ".tg[0].tor" -type "double3" 25.139579032906386 1.0412922365280961e-13 -90.000000000000213 ;
	setAttr ".lr" -type "double3" 1.7919534187789179e-06 4.5725404669633423e-06 3.5869063624349362e-07 ;
	setAttr ".rst" -type "double3" 3.3297738546215685 4.2065345965435101 -1.0117836507656095 ;
	setAttr ".rsrr" -type "double3" 1.5902773407317205e-15 -9.2236085762441989e-14 4.6913181551586875e-14 ;
	setAttr -k on ".w0";
createNode transform -n "RootSystem" -p "MotionSystem";
createNode transform -n "PelvisCenter_M" -p "RootSystem";
	setAttr ".t" -type "double3" 0 5.2490606307983398 -1.2424887120723729 ;
	setAttr ".ro" 3;
createNode transform -n "PelvisCenterBtwLegs_M" -p "RootSystem";
	setAttr ".t" -type "double3" -4.4408920985006262e-15 5.2490606307983398 -0.23313113337812627 ;
	setAttr ".ro" 3;
createNode orientConstraint -n "PelvisCenterBtwLegs_M_orientConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "PelvisCenterBtwLegs_M_pointConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -4.4408920985006262e-15 0.66881483811707843 -0.23313113337812627 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PelvisCenterBtwLegsOffset_M" -p "PelvisCenterBtwLegs_M";
	setAttr ".t" -type "double3" 4.4408920985006262e-15 0 -1.0093575786942466 ;
	setAttr ".ro" 3;
createNode transform -n "TwistSystem" -p "MotionSystem";
createNode transform -n "TwistFollowPelvis_M" -p "TwistSystem";
	setAttr -l on ".v" no;
	setAttr ".ro" 3;
createNode parentConstraint -n "TwistFollowPelvis_M_parentConstraint1" -p "TwistFollowPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine0_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 5.9604644775390731e-08 5.2490606307983487 -1.2424887120723751 ;
	setAttr -k on ".w0";
createNode joint -n "UnTwistPelvis_M" -p "TwistFollowPelvis_M";
	setAttr ".t" -type "double3" -1.0587911840678754e-22 -8.8817841970012523e-15 2.2204460492503131e-15 ;
	setAttr ".r" -type "double3" -0.012936969328369496 1.5122627355306081e-25 1.3395142177471998e-21 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "UnTwistEndPelvis_M" -p "UnTwistPelvis_M";
	setAttr ".t" -type "double3" 0 2.26441293766639 -4.2232883856740955e-13 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode orientConstraint -n "UnTwistEndPelvis_M_orientConstraint1" -p "UnTwistEndPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine4AlignUnTwistTo_MW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0.012936969328369503 0 0 ;
	setAttr ".rsrr" -type "double3" 0.012936969328369503 0 0 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector7" -p "UnTwistPelvis_M";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode ikHandle -n "UnTwistIKPelvis_M" -p "TwistFollowPelvis_M";
	setAttr ".r" -type "double3" -0.012936969328369503 0 0 ;
	setAttr ".roc" yes;
createNode pointConstraint -n "UnTwistIKPelvis_M_pointConstraint1" -p "UnTwistIKPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine4_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.5881867761018131e-22 2.2644128799438317 -0.00051128792762411557 ;
	setAttr -k on ".w0";
createNode transform -n "GameSkeleton" -p "Main";
createNode joint -n "Pelvis_M" -p "GameSkeleton";
	setAttr ".ro" 3;
createNode joint -n "SpineA_M" -p "Pelvis_M";
createNode joint -n "Chest_M" -p "SpineA_M";
createNode joint -n "Clavicle_R" -p "Chest_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -90.000003930293929 84.931295080550868 -4.0819389749637471 ;
createNode joint -n "Shoulder1_R" -p "Clavicle_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -42.090372616003478 -0.3554657349384957 0.081325854941446368 ;
createNode joint -n "Elbow1_R" -p "Shoulder1_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -2.4432342199382475 -2.9766418362803719 94.68306802031185 ;
createNode joint -n "Wrist2_R" -p "Elbow1_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -97.185970603061719 76.527857399514133 -94.999999999999758 ;
createNode joint -n "Wrist1_R" -p "Wrist2_R";
	setAttr ".ro" 5;
createNode joint -n "Barrel1_R" -p "Wrist1_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 173.24567741619776 -78.39623995459759 -174.98577840416411 ;
createNode joint -n "Barrel1_End_R" -p "Barrel1_R";
	setAttr ".t" -type "double3" 7.3274719625260332e-15 1.5211859788328059 8.8817841970012523e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "Barrel1_R_parentConstraint1" -p "Barrel1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXBarrel1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 7.4855486796696486e-07 5.1912195416479709e-06 -2.3948408147875637e-07 ;
	setAttr ".rst" -type "double3" -0.12643525580937975 0.86454642622434508 -0.69426794824467974 ;
	setAttr ".rsrr" -type "double3" 7.4855483675777387e-07 5.1912196119998822e-06 -2.3948412088781577e-07 ;
	setAttr -k on ".w0";
createNode joint -n "Barrel3_R" -p "Wrist1_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 123.58568095077455 -88.355054575950263 -125.52566987918071 ;
createNode joint -n "Barrel3_End_R" -p "Barrel3_R";
	setAttr ".t" -type "double3" 7.7715611723760958e-15 1.6294905799562946 7.1054273576010019e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "Barrel3_R_parentConstraint1" -p "Barrel3_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXBarrel3_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.0203204268871206e-06 -1.0038116333232125e-06 1.4258332207266509e-06 ;
	setAttr ".rst" -type "double3" 0.59771234485346625 0.86308121557597117 0.25536545034748126 ;
	setAttr ".rsrr" -type "double3" -2.0203207640259161e-06 -1.0038116629840634e-06 
		1.4258332366791165e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Barrel2_R" -p "Wrist1_R";
	setAttr ".jo" -type "double3" -1.7721985028616756 -89.999999999999289 0 ;
createNode joint -n "Barrel2_End_R" -p "Barrel2_R";
	setAttr ".t" -type "double3" 4.6629367034256575e-15 1.53686343497537 1.7763568394002505e-14 ;
createNode parentConstraint -n "Barrel2_R_parentConstraint1" -p "Barrel2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXBarrel2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7822766879090143e-07 -2.5032809385857833e-06 -7.7452941963485138e-08 ;
	setAttr ".rst" -type "double3" -0.60575244809646378 0.86692657736121126 0.4006514631676904 ;
	setAttr ".rsrr" -type "double3" 7.0345124621185084e-08 -2.5032809451930054e-06 -7.7452930762953197e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Wrist1_R_parentConstraint1" -p "Wrist1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWrist1_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXWrist1_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -0.085558441753423864 0.37731926651868775 0.017482427068370443 ;
	setAttr ".rsrr" -type "double3" 5.2078254500558535e-06 -8.6240312102329e-07 8.9948680036155729e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Wrist2_R_parentConstraint1" -p "Wrist2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWrist2_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXWrist2_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 3.5845762020450315e-06 5.4285022979044808e-07 -1.2075590773532732e-08 ;
	setAttr ".rst" -type "double3" 0.15534250811613859 2.7109513703314732 -0.86113365620572502 ;
	setAttr ".rsrr" -type "double3" 4.740452009320799e-06 -6.8520409409319464e-07 8.957608464540479e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Elbow1_R_parentConstraint1" -p "Elbow1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow1_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXElbow1_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -3.0851775039659699e-08 -2.9526841043995254e-08 -9.3407383658397812e-07 ;
	setAttr ".rst" -type "double3" -0.22498996252910969 4.0969668473937819 0.21896622754063699 ;
	setAttr ".rsrr" -type "double3" 8.7503179363584065e-07 -1.4734506815004335e-06 -2.2775711236940943e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Shoulder1_R_parentConstraint1" -p "Shoulder1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder1_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXShoulder1_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 1.2143665081413817e-06 2.2797935978634791e-08 2.088421841291131e-08 ;
	setAttr ".rst" -type "double3" 4.1167846104306349e-08 1.4383041024566336 2.0549293822114123e-08 ;
	setAttr ".rsrr" -type "double3" 1.4155866654320294e-06 1.1131799704527924e-06 -1.2110684999187835e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Clavicle_R_parentConstraint1" -p "Clavicle_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXClavicle_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.6371884077511045e-06 1.874827658432711e-08 -3.2798958430471213e-06 ;
	setAttr ".rst" -type "double3" -1.210514187720698 2.0222060583659207 0.87635690729864191 ;
	setAttr ".rsrr" -type "double3" 1.6371884077511045e-06 1.874827658432711e-08 -3.2798958430471213e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Head_M" -p "Chest_M";
	setAttr ".ro" 5;
createNode joint -n "Head_End_M" -p "Head_M";
	setAttr ".t" -type "double3" 3.1589715250000827e-16 1.8033760160193548 1.7486012637846216e-15 ;
createNode parentConstraint -n "Head_M_parentConstraint1" -p "Head_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXHead_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 1.3032944042756718 1.030975793432424 ;
	setAttr -k on ".w0";
createNode joint -n "Clavicle1_L" -p "Chest_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 89.999999616644317 -89.999996197111585 -4.7919460118866914e-08 ;
createNode joint -n "Shoulder_L" -p "Clavicle1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -38.023918364923226 0 -4.999994824912684 ;
createNode joint -n "Elbow_L" -p "Shoulder_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -93.916479517820761 -31.612202962954523 95.454455117598243 ;
createNode joint -n "Wrist_L" -p "Elbow_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 17.197426983931944 92.095231137754141 10.897917771995928 ;
createNode joint -n "MiddleFinger1_L" -p "Wrist_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 2.928559522129341 -86.765182877390231 -7.6130455398104839 ;
createNode joint -n "MiddleFinger5_L" -p "MiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -32.157223191004611 -8.9886224950894498 179.39159194778256 ;
createNode joint -n "MiddleFinger6_L" -p "MiddleFinger5_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 24.328749709389609 -4.4191332893711905 -0.55167359184977371 ;
createNode joint -n "MiddleFinger7_End_L" -p "MiddleFinger6_L";
	setAttr ".t" -type "double3" 7.1054273576010019e-15 1.602632625525243 5.773159728050814e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0.00017675715839642742 0 179.99979030053379 ;
createNode parentConstraint -n "MiddleFinger6_L_parentConstraint1" -p "MiddleFinger6_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger6_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -8.0507982044440313e-07 4.3158701716929705e-07 -1.0841869068133619e-07 ;
	setAttr ".rst" -type "double3" 1.4359325284374336e-08 1.0787334795095567 -4.0057464900655759e-08 ;
	setAttr ".rsrr" -type "double3" -8.3038573004129279e-07 9.6071290852492134e-07 6.6807314411548542e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "MiddleFinger5_L_parentConstraint1" -p "MiddleFinger5_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger5_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -8.4420917681766498e-07 5.0685207319239408e-07 1.2544626711896789e-06 ;
	setAttr ".rst" -type "double3" -0.60580561617963369 -0.98707656308396219 -0.2231965477639537 ;
	setAttr ".rsrr" -type "double3" -9.3918648071772884e-07 6.7001928020466507e-07 9.2082023781183309e-07 ;
	setAttr -k on ".w0";
createNode joint -n "MiddleFinger2_L" -p "MiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -31.79794802146575 18.834608271787474 176.9310789564673 ;
createNode joint -n "MiddleFinger3_L" -p "MiddleFinger2_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 25.110775518055277 -4.3970688145584802 -2.6207129746815134 ;
createNode joint -n "MiddleFinger4_End_L" -p "MiddleFinger3_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-15 1.4056735102234843 3.9968028886505635e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0.00020152388640528809 0 179.99976091802003 ;
createNode parentConstraint -n "MiddleFinger3_L_parentConstraint1" -p "MiddleFinger3_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger3_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -8.2155359866553128e-07 2.0332375352420863e-07 -8.0608734554412904e-09 ;
	setAttr ".rst" -type "double3" 5.637860400753425e-08 1.0793649546193933 3.9133989604422936e-08 ;
	setAttr ".rsrr" -type "double3" 4.6991797940381792e-07 -3.847445685237635e-07 -1.795429603843588e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "MiddleFinger2_L_parentConstraint1" -p "MiddleFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.3497164968621524e-06 1.8884702994384529e-07 -1.4271152368628907e-06 ;
	setAttr ".rst" -type "double3" 0.56841023642881527 -0.98585601424079616 -0.4854464197352345 ;
	setAttr ".rsrr" -type "double3" 1.4397241490924301e-06 3.4920035979169608e-07 -1.7634884248603264e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "MiddleFinger1_L_parentConstraint1" -p "MiddleFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.9134912380318959e-07 -1.0544529370181745e-06 3.8344429805396563e-08 ;
	setAttr ".rst" -type "double3" 0.4015428312807896 -0.30593321624092962 -0.10969215951781308 ;
	setAttr ".rsrr" -type "double3" -2.548207898264509e-07 -1.0153951106648691e-06 -3.4126280866354173e-07 ;
	setAttr -k on ".w0";
createNode joint -n "ThumbFinger1_L" -p "Wrist_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 157.74255825472812 -5.1681922016079893 -0.73170866673602875 ;
createNode joint -n "ThumbFinger2_L" -p "ThumbFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 13.060271054529446 0.55873476267479061 -1.131080314900587 ;
createNode joint -n "ThumbFinger3_End_L" -p "ThumbFinger2_L";
	setAttr ".t" -type "double3" 3.9968028886505635e-15 1.4566019002008472 -4.4408920985006262e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 5.4572258181333764 -2.6135011056817596e-06 -179.99997264373806 ;
createNode parentConstraint -n "ThumbFinger2_L_parentConstraint1" -p "ThumbFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -4.2607153651038438e-07 3.2518143812842828e-07 -4.90368059306581e-07 ;
	setAttr ".rst" -type "double3" 7.2673349560403722e-09 1.0724462014893845 -1.1299913493800773e-07 ;
	setAttr ".rsrr" -type "double3" -6.461622333353868e-06 1.9579389314915797e-07 -5.2077524245205675e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ThumbFinger1_L_parentConstraint1" -p "ThumbFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.0370140730224164e-06 4.3351183879445398e-07 -3.8825967692015545e-07 ;
	setAttr ".rst" -type "double3" -0.367777602394805 -0.95234201253672257 0.90711995931591005 ;
	setAttr ".rsrr" -type "double3" -6.0370140984668533e-06 4.3351187298541583e-07 -3.8825965843317941e-07 ;
	setAttr -k on ".w0";
createNode joint -n "Hand_AP1_L" -p "Wrist_L";
	setAttr ".t" -type "double3" 9.957102918289884e-07 -0.32519415569505128 8.9052130736178015e-07 ;
	setAttr ".jo" -type "double3" 2.9575589360866722e-06 -89.999999999999758 0 ;
createNode parentConstraint -n "Wrist_L_parentConstraint1" -p "Wrist_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWrist_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXWrist_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 7.2909188567316634e-07 -1.8210945150340351e-07 -1.5413384275497847e-08 ;
	setAttr ".rst" -type "double3" 0.57694604254196769 -1.730106951808513 -0.012315702886801505 ;
	setAttr ".rsrr" -type "double3" 1.3258403759610543e-06 -4.9366217731333865e-08 -6.6453387755259023e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Elbow_L_parentConstraint1" -p "Elbow_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXElbow_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.3199609621289357e-06 -3.7264806995817201e-07 1.7606330676492613e-06 ;
	setAttr ".rst" -type "double3" 0.24481093856851421 -4.5159834931557432 -0.14617973009852925 ;
	setAttr ".rsrr" -type "double3" -7.289116146738504e-07 2.2887911690689149e-07 -1.8275251730548486e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Shoulder_L_parentConstraint1" -p "Shoulder_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXShoulder_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -8.9972868094371678e-07 -2.611547485790115e-07 3.33975355260632e-07 ;
	setAttr ".rst" -type "double3" -0.12707449270354071 -1.4290493932916641 -0.1019814952267275 ;
	setAttr ".rsrr" -type "double3" 1.1910550202801092e-06 5.7126570489798212e-07 1.6698768891882189e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Clavicle1_L_parentConstraint1" -p "Clavicle1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXClavicle1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.9110191156771869e-06 -1.9642598980061491e-14 -5.2282260642703033e-07 ;
	setAttr ".rst" -type "double3" 1.2105099403953554 2.0222064892578118 0.87635700000000116 ;
	setAttr ".rsrr" -type "double3" 2.9110191156771869e-06 -1.9642598980061491e-14 -5.2282260642703033e-07 ;
	setAttr -k on ".w0";
createNode joint -n "Pipe1_R" -p "Chest_M";
	setAttr ".ro" 5;
createNode joint -n "Pipe1_End_R" -p "Pipe1_R";
	setAttr ".t" -type "double3" 0 1.8033760160193548 1.7763568394002505e-15 ;
createNode parentConstraint -n "Pipe1_R_parentConstraint1" -p "Pipe1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXPipe1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -4.3963231444358826 -0.79495811462402344 -4.807751447677612 ;
	setAttr -k on ".w0";
createNode joint -n "Pipe2_R" -p "Chest_M";
	setAttr ".ro" 5;
createNode joint -n "Pipe2_End_R" -p "Pipe2_R";
	setAttr ".t" -type "double3" 0 1.8033760160193548 1.7763568394002505e-15 ;
createNode parentConstraint -n "Pipe2_R_parentConstraint1" -p "Pipe2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXPipe2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -7.3409672379493713 -2.4120712280273438 -2.8420160121917721 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "Chest_M_pointConstraint1" -p "Chest_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXChest_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXChest_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 5.2939559203393771e-23 1.3383469581604048 -1.1102230246251565e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "Chest_M_orientConstraint1" -p "Chest_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXChest_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXChest_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "SpineA_M_pointConstraint1" -p "SpineA_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXSpineA_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXSpineA_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 5.2939559203393771e-23 0.92606592178345171 -0.00051128792762855646 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "SpineA_M_orientConstraint1" -p "SpineA_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXSpineA_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXSpineA_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "HipTwist_R" -p "Pelvis_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 205.1395790329064 89.999999999999687 ;
createNode joint -n "Hip_R" -p "HipTwist_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0.72356917514655739 1.0610891793702442 -90.162074787503371 ;
createNode joint -n "Knee_R" -p "Hip_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 22.919762006828119 0 0 ;
createNode joint -n "Ankle_R" -p "Knee_R";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 1.4900163786870606 -0.0077691772226723532 0.59745913041447918 ;
createNode joint -n "MiddleToe1_R" -p "Ankle_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 71.642384016319596 179.10051431432231 2.2965204288393681e-05 ;
createNode joint -n "MiddleToe2_End_R" -p "MiddleToe1_R";
	setAttr ".t" -type "double3" -5.773159728050814e-15 1.4371052628296188 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -1.5171077643057804 0 0 ;
createNode parentConstraint -n "MiddleToe1_R_parentConstraint1" -p "MiddleToe1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleToe1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -4.773076763683557e-06 1.0249876052890891e-06 4.4246735423295739e-06 ;
	setAttr ".rst" -type "double3" 0.020825579278232587 0.15220357689789688 -1.3263746982714693 ;
	setAttr ".rsrr" -type "double3" -2.0048139186375513e-06 3.5659466661425838e-08 4.0963792473848766e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Ankle_R_parentConstraint1" -p "Ankle_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXAnkle_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXAnkle_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -8.5462650448106491e-06 8.9040540445561921e-06 3.689665941766985e-06 ;
	setAttr ".rst" -type "double3" 9.1057582363163192e-08 2.6670582478423475 -1.1524917814531577e-07 ;
	setAttr ".rsrr" -type "double3" -1.3638175020663243e-06 8.3555566244441848e-08 -1.1513891809155516e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Knee_R_parentConstraint1" -p "Knee_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXKnee_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXKnee_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 7.6796637495569471e-06 0 0 ;
	setAttr ".rst" -type "double3" 3.7881202841205663e-08 2.2734843649612171 -4.4132063559132462e-08 ;
	setAttr ".rsrr" -type "double3" -1.4866509444413067e-06 -3.9478285213355451e-07 
		-9.3368345268452942e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Hip_R_parentConstraint1" -p "Hip_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHip_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXHip_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -5.7026351892851887e-14 1.4443654739902282e-06 2.9953587376295082e-14 ;
	setAttr ".rst" -type "double3" -0.088520791078289918 0.74529487108319725 -0.041540888538642573 ;
	setAttr ".rsrr" -type "double3" -1.3342165970002897e-06 5.3587466574406934e-07 -1.0137150750391341e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "HipTwist_R_parentConstraint1" -p "HipTwist_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.063964853048983e-06 2.7257923603572866e-06 -2.2672430872658017e-06 ;
	setAttr ".rst" -type "double3" -1.2119851708412168 0.058133125305176669 -0.00018638372421264648 ;
	setAttr ".rsrr" -type "double3" 1.063964853048983e-06 2.7257923603572866e-06 -2.2672430872658017e-06 ;
	setAttr -k on ".w0";
createNode joint -n "HipTwist_L" -p "Pelvis_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.7566776614494373e-15 25.139579032906386 90.000000000000256 ;
createNode joint -n "Hip_L" -p "HipTwist_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0.72356917514652719 1.0610891793702357 -90.162074787503371 ;
createNode joint -n "Knee_L" -p "Hip_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 22.919762006828126 0 0 ;
createNode joint -n "Ankle_L" -p "Knee_L";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 1.4900163786870737 -0.0077691772226841467 0.59745913041447918 ;
createNode joint -n "MiddleToe1_L" -p "Ankle_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 71.642384016319596 179.10051431432231 2.2965204288393681e-05 ;
createNode joint -n "MiddleToe2_End_L" -p "MiddleToe1_L";
	setAttr ".t" -type "double3" 3.5527136788005009e-15 -1.4371052628296188 -2.2204460492503131e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -1.51710776430577 0 0 ;
createNode parentConstraint -n "MiddleToe1_L_parentConstraint1" -p "MiddleToe1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleToe1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -5.9498866269139765e-06 -9.1510912279296364e-07 3.7808833219882118e-06 ;
	setAttr ".rst" -type "double3" -0.02082557927823192 -0.15220357689789654 1.3263746982714695 ;
	setAttr ".rsrr" -type "double3" -2.004813924998659e-06 3.5659426357834957e-08 4.0963792072303726e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Ankle_L_parentConstraint1" -p "Ankle_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXAnkle_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXAnkle_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.3448642735362963e-05 -8.3521286132735963e-06 -3.4897796339664016e-06 ;
	setAttr ".rst" -type "double3" 6.8610131798152452e-08 -2.6670582627625583 -9.4015812274195198e-08 ;
	setAttr ".rsrr" -type "double3" 9.2568309228904381e-07 2.3698258221245236e-06 3.1548147520115494e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Knee_L_parentConstraint1" -p "Knee_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXKnee_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXKnee_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 4.8030191083495001e-06 0 0 ;
	setAttr ".rst" -type "double3" 5.074690245798763e-08 -2.2734843373128086 -5.956973936349641e-08 ;
	setAttr ".rsrr" -type "double3" 7.2570594194361995e-07 2.3693321949245479e-06 3.8375183162011441e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Hip_L_parentConstraint1" -p "Hip_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHip_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXHip_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -9.5416642697277197e-15 -4.9334467594992403e-06 -5.2340154169435882e-15 ;
	setAttr ".rst" -type "double3" 0.088520818932504852 -0.74529486864000027 0.041540873017168467 ;
	setAttr ".rsrr" -type "double3" 8.7814054564277828e-07 2.0328272919510353e-06 1.2761717222231198e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "HipTwist_L_parentConstraint1" -p "HipTwist_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 3.9870285692937789e-06 -1.7919533239907746e-06 2.2672426653791181e-06 ;
	setAttr ".rst" -type "double3" 1.2119850516319282 0.058133125305176669 -0.00018638372421198035 ;
	setAttr ".rsrr" -type "double3" 3.9870285692937789e-06 -1.7919533239907746e-06 2.2672426653791181e-06 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "Pelvis_M_pointConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXPelvis_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 5.9604644775390678e-08 5.2490606307983443 -1.242488712072374 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "Pelvis_M_orientConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKOrientToSpine_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode nurbsCurve -n "forwardArrowShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-0.27050452503275879 6.0064070391332143e-17 2.2276716182839635
		-0.54100905006551758 1.2012814078266429e-16 2.2276716182839635
		-1.8019221117399648e-16 3.6038442234799315e-16 3.0391851933822398
		0.54100905006551758 -1.2012814078266429e-16 2.2276716182839635
		0.27050452503275879 -6.0064070391332143e-17 2.2276716182839635
		0.27050452503275901 -4.204484927393253e-16 1.4161580431856873
		-0.27050452503275862 -3.0032035195666096e-16 1.4161580431856873
		-0.27050452503275879 6.0064070391332143e-17 2.2276716182839635
		;
createNode transform -n "items" -p "ctrl_rig";
	setAttr -l on ".it" no;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr ".cm" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".cs" 3;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode shadingEngine -n "irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode shadingEngine -n "BPRig_irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "BPRig_materialInfo3";
createNode ikSCsolver -n "ikSCsolver";
createNode ikRPsolver -n "ikRPsolver";
createNode ikSplineSolver -n "ikSplineSolver";
createNode displayLayer -n "jointLayer";
createNode reverse -n "jointVisReverse";
createNode objectSet -n "ControlSet";
	setAttr ".ihi" 0;
	setAttr -s 111 ".dsm";
createNode objectSet -n "GameSet";
	setAttr ".ihi" 0;
	setAttr -s 35 ".dsm";
createNode objectSet -n "AllSet";
	setAttr ".ihi" 0;
	setAttr -s 574 ".dsm";
	setAttr -s 113 ".dnsm";
createNode objectSet -n "Sets";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dnsm";
createNode setRange -n "IKArm1_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode setRange -n "PoleArm1_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendArm1UnitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendArm1Reverse_R";
createNode condition -n "FKIKBlendArm1Condition_R";
createNode setRange -n "FKIKBlendArm1setRange_R";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode setRange -n "IKArm_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode setRange -n "PoleArm_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendArmUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendArmReverse_L";
createNode condition -n "FKIKBlendArmCondition_L";
createNode setRange -n "FKIKBlendArmsetRange_L";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode unitConversion -n "unitConversion1";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_R";
createNode condition -n "FKIKBlendLegCondition_R";
createNode setRange -n "FKIKBlendLegsetRange_R";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode unitConversion -n "IKLiftToeLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode multiplyDivide -n "Leg_RAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_R";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion4";
	setAttr ".cf" 0.017453292519943295;
createNode blendTwoAttr -n "IKBallToFKBallMiddleToe1blendTwoAttr_R";
	setAttr -s 2 ".i[0:1]"  108.35550752148379 108.35551030576904;
createNode unitConversion -n "unitConversion5";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion6";
	setAttr ".cf" 0.017453292519943295;
createNode setRange -n "IKStiffSpine0_M";
	setAttr ".m" -type "float3" 0 0.75480437 0 ;
	setAttr ".on" -type "float3" 0 -10 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode setRange -n "IKStiffSpine2_M";
	setAttr ".on" -type "float3" 0 -10 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode setRange -n "IKStiffSpine4_M";
	setAttr ".m" -type "float3" 0 0.75480431 0 ;
	setAttr ".on" -type "float3" 0 -10 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode curveInfo -n "IKCurveInfoSpine_M";
createNode multiplyDivide -n "IKCurveInfoNormalizeSpine_M";
	setAttr ".op" 2;
	setAttr ".i2" -type "float3" 1 2.2644131 1 ;
createNode multiplyDivide -n "IKCurveInfoAllMultiplySpine_M";
	setAttr ".op" 2;
createNode unitConversion -n "stretchySpineUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "stretchySpineReverse_M";
createNode multiplyDivide -n "stretchySpineMultiplyDivide0_M";
	setAttr ".i1" -type "float3" 0 1.338347 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo0_M";
	setAttr -s 2 ".i[0:1]"  1.3383469581604004 1.3383469581604004;
createNode multiplyDivide -n "stretchySpineMultiplyDivide1_M";
	setAttr ".i1" -type "float3" 0 0.92606592 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo1_M";
	setAttr -s 2 ".i[0:1]"  0.92606592178344727 0.92606592178344727;
createNode multiplyDivide -n "stretchySpineMultiplyDivide2_M";
	setAttr ".i1" -type "float3" 0 1.338347 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo2_M";
	setAttr -s 2 ".i[0:1]"  1.3383469581604004 1.3383469581604004;
createNode multiplyDivide -n "stretchySpineMultiplyDivide3_M";
	setAttr ".i1" -type "float3" 0 0.92606592 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo3_M";
	setAttr -s 2 ".i[0:1]"  0.92606592178344727 0.92606592178344727;
createNode unitConversion -n "IKTwistSpineUnitConversion_M";
	setAttr ".cf" 0.75;
createNode unitConversion -n "FKIKBlendSpineUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendSpineReverse_M";
createNode condition -n "FKIKBlendSpineCondition_M";
createNode setRange -n "FKIKBlendSpinesetRange_M";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode blendColors -n "ScaleBlendWrist1_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendWrist2_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendElbow1_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendShoulder1_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendWrist_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendElbow_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendShoulder_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendChest_M";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendSpineA_M";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendAnkle_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendKnee_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendHip_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendPelvis_M";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode setRange -n "IKSetRangeStretchLeg_R";
	setAttr ".m" -type "float3" 0 1 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode setRange -n "IKSetRangeAntiPopLeg_R";
	setAttr ".m" -type "float3" 0 1 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "IKmessureDivLeg_R";
	setAttr ".op" 2;
	setAttr ".i2" -type "float3" 1 4.9405427 1 ;
createNode blendTwoAttr -n "IKmessureBlendAntiPopLeg_R";
	setAttr -s 2 ".i";
	setAttr -s 2 ".i";
createNode animCurveUU -n "IKdistanceLeg_RShape_antiPop";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0.49405425786972046 4.9405426033567945 
		1.2351356744766235 4.9405426033567945 4.9405426979064941 4.9405426033567945 5.9286513328552246 
		5.9286511240281534;
	setAttr -s 4 ".kit[3]"  2;
	setAttr -s 4 ".kot[3]"  2;
	setAttr -s 4 ".ktl[1:3]" no no yes;
	setAttr -s 4 ".kix[0:3]"  1 0.74108147621154785 0.58132672309875488 
		0.7071068286895752;
	setAttr -s 4 ".kiy[0:3]"  0 0 0.81367021799087524 0.70710670948028564;
	setAttr -s 4 ".kox[0:3]"  1 0.61197483539581299 0.7071068286895752 
		1;
	setAttr -s 4 ".koy[0:3]"  0 -0.79087716341018677 0.70710670948028564 
		0;
	setAttr ".pst" 1;
createNode animCurveUU -n "IKdistanceLeg_RShape_normal";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0.49405425786972046 4.9405426033567945 
		1.2351356744766235 4.9405426033567945 4.9405426979064941 4.9405426033567945 5.9286513328552246 
		5.9286511240281534;
	setAttr -s 4 ".kit[3]"  2;
	setAttr -s 4 ".kot[3]"  2;
	setAttr -s 4 ".kix[0:3]"  1 0.74108147621154785 1 0.7071068286895752;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0.70710670948028564;
	setAttr -s 4 ".kox[0:3]"  1 3.7054071426391602 0.7071068286895752 
		1;
	setAttr -s 4 ".koy[0:3]"  0 0 0.70710670948028564 0;
	setAttr ".pst" 1;
createNode clamp -n "IKdistanceClampLeg_R";
	setAttr ".mx" -type "float3" 4.9405427 0 0 ;
createNode blendTwoAttr -n "IKmessureBlendStretchLeg_R";
	setAttr -s 2 ".i";
	setAttr -s 2 ".i";
createNode multiplyDivide -n "IKXKnee_R_IKmessureDiv_R";
createNode multiplyDivide -n "IKXKnee_R_IKLength_R";
	setAttr ".i2" -type "float3" 1 2.2734845 1 ;
createNode multiplyDivide -n "IKXAnkle_R_IKmessureDiv_R";
createNode multiplyDivide -n "IKXAnkle_R_IKLength_R";
	setAttr ".i2" -type "float3" 1 2.6670582 1 ;
createNode plusMinusAverage -n "ScaleBlendAddYKnee_R";
	setAttr -s 3 ".i1[2]"  -1;
	setAttr -s 2 ".i1";
createNode unitConversion -n "unitConversion7";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_L";
createNode condition -n "FKIKBlendLegCondition_L";
createNode setRange -n "FKIKBlendLegsetRange_L";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode unitConversion -n "IKLiftToeLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode multiplyDivide -n "Leg_LAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_L";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion8";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion9";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion10";
	setAttr ".cf" 0.017453292519943295;
createNode blendTwoAttr -n "IKBallToFKBallMiddleToe1blendTwoAttr_L";
	setAttr -s 2 ".i[0:1]"  108.35550752148379 108.35551145034458;
createNode unitConversion -n "unitConversion11";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion12";
	setAttr ".cf" 0.017453292519943295;
createNode blendColors -n "ScaleBlendAnkle_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendKnee_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendHip_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode setRange -n "IKSetRangeStretchLeg_L";
	setAttr ".m" -type "float3" 0 1 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode setRange -n "IKSetRangeAntiPopLeg_L";
	setAttr ".m" -type "float3" 0 1 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "IKmessureDivLeg_L";
	setAttr ".op" 2;
	setAttr ".i2" -type "float3" 1 4.9405427 1 ;
createNode blendTwoAttr -n "IKmessureBlendAntiPopLeg_L";
	setAttr -s 2 ".i";
	setAttr -s 2 ".i";
createNode animCurveUU -n "IKdistanceLeg_LShape_antiPop";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0.49405425786972046 4.9405426033567945 
		1.2351356744766235 4.9405426033567945 4.9405426979064941 4.9405426033567945 5.9286513328552246 
		5.9286511240281534;
	setAttr -s 4 ".kit[3]"  2;
	setAttr -s 4 ".kot[3]"  2;
	setAttr -s 4 ".ktl[1:3]" no no yes;
	setAttr -s 4 ".kix[0:3]"  1 0.74108147621154785 0.58132672309875488 
		0.7071068286895752;
	setAttr -s 4 ".kiy[0:3]"  0 0 0.81367021799087524 0.70710670948028564;
	setAttr -s 4 ".kox[0:3]"  1 0.61197483539581299 0.7071068286895752 
		1;
	setAttr -s 4 ".koy[0:3]"  0 -0.79087716341018677 0.70710670948028564 
		0;
	setAttr ".pst" 1;
createNode animCurveUU -n "IKdistanceLeg_LShape_normal";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0.49405425786972046 4.9405426033567945 
		1.2351356744766235 4.9405426033567945 4.9405426979064941 4.9405426033567945 5.9286513328552246 
		5.9286511240281534;
	setAttr -s 4 ".kit[3]"  2;
	setAttr -s 4 ".kot[3]"  2;
	setAttr -s 4 ".kix[0:3]"  1 0.74108147621154785 1 0.7071068286895752;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0.70710670948028564;
	setAttr -s 4 ".kox[0:3]"  1 3.7054071426391602 0.7071068286895752 
		1;
	setAttr -s 4 ".koy[0:3]"  0 0 0.70710670948028564 0;
	setAttr ".pst" 1;
createNode clamp -n "IKdistanceClampLeg_L";
	setAttr ".mx" -type "float3" 4.9405427 0 0 ;
createNode blendTwoAttr -n "IKmessureBlendStretchLeg_L";
	setAttr -s 2 ".i";
	setAttr -s 2 ".i";
createNode multiplyDivide -n "IKXKnee_L_IKmessureDiv_L";
createNode multiplyDivide -n "IKXKnee_L_IKLength_L";
	setAttr ".i2" -type "float3" 1 -2.2734845 1 ;
createNode multiplyDivide -n "IKXAnkle_L_IKmessureDiv_L";
createNode multiplyDivide -n "IKXAnkle_L_IKLength_L";
	setAttr ".i2" -type "float3" 1 -2.6670582 1 ;
createNode plusMinusAverage -n "ScaleBlendAddYKnee_L";
	setAttr -s 3 ".i1[2]"  -1;
	setAttr -s 2 ".i1";
createNode setRange -n "CenterBtwFeet_M";
	setAttr ".v" -type "float3" 0 1 0 ;
	setAttr ".n" -type "float3" 2 0 0 ;
	setAttr ".om" -type "float3" 0.1 0 0 ;
createNode unitConversion -n "PelvisCenterBtwFeetUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "PelvisCenterBtwFeetReverse_M";
createNode dagPose -n "buildPose";
	addAttr -ci true -sn "udAttr" -ln "udAttr" -dt "string";
	setAttr ".udAttr" -type "string" (
		"setAttr IKExtraLeg_L.translateX 0;setAttr IKExtraLeg_L.translateY 0;setAttr IKExtraLeg_L.translateZ 0;setAttr IKExtraLeg_L.rotateX -0;setAttr IKExtraLeg_L.rotateY -0;setAttr IKExtraLeg_L.rotateZ 0;setAttr IKLeg_L.translateX 0;setAttr IKLeg_L.translateY 0;setAttr IKLeg_L.translateZ 0;setAttr IKLeg_L.rotateX -0;setAttr IKLeg_L.rotateY -0;setAttr IKLeg_L.rotateZ 0;setAttr IKLeg_L.swivel 0;setAttr IKLeg_L.toe 0;setAttr IKLeg_L.roll 0;setAttr IKLeg_L.rollAngle 25;setAttr IKLeg_L.stretchy 0;setAttr IKLeg_L.antiPop 0;setAttr IKLeg_L.Length1 1;setAttr IKLeg_L.Length2 1;setAttr PoleExtraLeg_L.translateX 0;setAttr PoleExtraLeg_L.translateY 0;setAttr PoleExtraLeg_L.translateZ 0;setAttr PoleLeg_L.translateX 0;setAttr PoleLeg_L.translateY 0;setAttr PoleLeg_L.translateZ 0;setAttr PoleLeg_L.follow 10;setAttr FKIKLeg_L.FKIKBlend 10;setAttr FKIKLeg_L.FKVis 0;setAttr FKIKLeg_L.IKVis 1;setAttr IKExtraLegHeel_L.rotateX 0;setAttr IKExtraLegHeel_L.rotateY -0;setAttr IKExtraLegHeel_L.rotateZ 0;setAttr IKLegHeel_L.rotateX 0;setAttr IKLegHeel_L.rotateY -0;setAttr IKLegHeel_L.rotateZ 0;setAttr IKExtraLegToe_L.rotateX 0;setAttr IKExtraLegToe_L.rotateY -0;setAttr IKExtraLegToe_L.rotateZ 0;setAttr IKLegToe_L.rotateX 0;setAttr IKLegToe_L.rotateY -0;setAttr IKLegToe_L.rotateZ 0;setAttr IKExtraLegBall_L.rotateX 0;setAttr IKExtraLegBall_L.rotateY -0;setAttr IKExtraLegBall_L.rotateZ 0;setAttr IKLegBall_L.rotateX 0;setAttr CenterExtra_M.translateX 0;setAttr CenterExtra_M.translateY 0;setAttr CenterExtra_M.translateZ 0;setAttr CenterExtra_M.rotateX 0;setAttr CenterExtra_M.rotateY -0;setAttr CenterExtra_M.rotateZ 0;setAttr Center_M.translateX 0;setAttr Center_M.translateY 0;setAttr Center_M.translateZ 0;setAttr Center_M.rotateX 0;setAttr Center_M.rotateY -0;setAttr Center_M.rotateZ 0;setAttr Center_M.CenterBtwFeet 0;setAttr FKExtraMiddleFinger3_L.translateX -8.881784197e-16;setAttr FKExtraMiddleFinger3_L.translateY -4.440892099e-16;setAttr FKExtraMiddleFinger3_L.translateZ 1.387778781e-17;setAttr FKExtraMiddleFinger3_L.rotateX -0;setAttr FKExtraMiddleFinger3_L.rotateY 0;setAttr FKExtraMiddleFinger3_L.rotateZ -0;setAttr FKExtraMiddleFinger3_L.scaleX 1;setAttr FKExtraMiddleFinger3_L.scaleY 1;setAttr FKExtraMiddleFinger3_L.scaleZ 1;setAttr FKMiddleFinger3_L.translateX -8.881784197e-16;setAttr FKMiddleFinger3_L.translateY -4.440892099e-16;setAttr FKMiddleFinger3_L.translateZ 1.387778781e-17;setAttr FKMiddleFinger3_L.rotateX -0;setAttr FKMiddleFinger3_L.rotateY 0;setAttr FKMiddleFinger3_L.rotateZ -0;setAttr FKMiddleFinger3_L.scaleX 1;setAttr FKMiddleFinger3_L.scaleY 1;setAttr FKMiddleFinger3_L.scaleZ 1;setAttr FKExtraMiddleFinger2_L.translateX -1.776356839e-15;setAttr FKExtraMiddleFinger2_L.translateY -4.440892099e-16;setAttr FKExtraMiddleFinger2_L.translateZ 1.110223025e-16;setAttr FKExtraMiddleFinger2_L.rotateX -0;setAttr FKExtraMiddleFinger2_L.rotateY 0;setAttr FKExtraMiddleFinger2_L.rotateZ -0;setAttr FKExtraMiddleFinger2_L.scaleX 1;setAttr FKExtraMiddleFinger2_L.scaleY 1;setAttr FKExtraMiddleFinger2_L.scaleZ 1;setAttr FKMiddleFinger2_L.translateX -8.881784197e-16;setAttr FKMiddleFinger2_L.translateY 4.440892099e-16;setAttr FKMiddleFinger2_L.translateZ -1.110223025e-16;setAttr FKMiddleFinger2_L.rotateX -0;setAttr FKMiddleFinger2_L.rotateY 0;setAttr FKMiddleFinger2_L.rotateZ -0;setAttr FKMiddleFinger2_L.scaleX 1;setAttr FKMiddleFinger2_L.scaleY 1;setAttr FKMiddleFinger2_L.scaleZ 1;setAttr FKExtraMiddleFinger1_L.translateX 8.881784197e-16;setAttr FKExtraMiddleFinger1_L.translateY 8.881784197e-16;setAttr FKExtraMiddleFinger1_L.translateZ 1.665334537e-16;setAttr FKExtraMiddleFinger1_L.rotateX -0;setAttr FKExtraMiddleFinger1_L.rotateY 0;setAttr FKExtraMiddleFinger1_L.rotateZ -0;setAttr FKExtraMiddleFinger1_L.scaleX 1;setAttr FKExtraMiddleFinger1_L.scaleY 1;setAttr FKExtraMiddleFinger1_L.scaleZ 1;setAttr FKMiddleFinger1_L.translateX 8.881784197e-16;setAttr FKMiddleFinger1_L.translateY 0;setAttr FKMiddleFinger1_L.translateZ 2.220446049e-16;setAttr FKMiddleFinger1_L.rotateX -0;setAttr FKMiddleFinger1_L.rotateY 0;setAttr FKMiddleFinger1_L.rotateZ -0;setAttr FKMiddleFinger1_L.scaleX 1;setAttr FKMiddleFinger1_L.scaleY 1;setAttr FKMiddleFinger1_L.scaleZ 1;setAttr FKExtraThumbFinger2_L.translateX 2.220446049e-16;setAttr FKExtraThumbFinger2_L.translateY 0;setAttr FKExtraThumbFinger2_L.translateZ 8.881784197e-16;setAttr FKExtraThumbFinger2_L.rotateX -0;setAttr FKExtraThumbFinger2_L.rotateY 0;setAttr FKExtraThumbFinger2_L.rotateZ -0;setAttr FKExtraThumbFinger2_L.scaleX 1;setAttr FKExtraThumbFinger2_L.scaleY 1;setAttr FKExtraThumbFinger2_L.scaleZ 1;setAttr FKThumbFinger2_L.translateX 0;setAttr FKThumbFinger2_L.translateY 0;setAttr FKThumbFinger2_L.translateZ 8.881784197e-16;setAttr FKThumbFinger2_L.rotateX -0;setAttr FKThumbFinger2_L.rotateY 0;setAttr FKThumbFinger2_L.rotateZ -0;setAttr FKThumbFinger2_L.scaleX 1;setAttr FKThumbFinger2_L.scaleY 1;setAttr FKThumbFinger2_L.scaleZ 1;setAttr FKExtraThumbFinger1_L.translateX -2.220446049e-16;setAttr FKExtraThumbFinger1_L.translateY -8.881784197e-16;setAttr FKExtraThumbFinger1_L.translateZ 4.440892099e-16;setAttr FKExtraThumbFinger1_L.rotateX -0;setAttr FKExtraThumbFinger1_L.rotateY 0;setAttr FKExtraThumbFinger1_L.rotateZ -0;setAttr FKExtraThumbFinger1_L.scaleX 1;setAttr FKExtraThumbFinger1_L.scaleY 1;setAttr FKExtraThumbFinger1_L.scaleZ 1;setAttr FKThumbFinger1_L.translateX -2.220446049e-16;setAttr FKThumbFinger1_L.translateY 0;setAttr FKThumbFinger1_L.translateZ 0;setAttr FKThumbFinger1_L.rotateX -0;setAttr FKThumbFinger1_L.rotateY 0;setAttr FKThumbFinger1_L.rotateZ -0;setAttr FKThumbFinger1_L.scaleX 1;setAttr FKThumbFinger1_L.scaleY 1;setAttr FKThumbFinger1_L.scaleZ 1;setAttr FKExtraWrist_L.translateX 5.551115123e-17;setAttr FKExtraWrist_L.translateY 0;setAttr FKExtraWrist_L.translateZ -8.881784197e-16;setAttr FKExtraWrist_L.rotateX -0;setAttr FKExtraWrist_L.rotateY 0;setAttr FKExtraWrist_L.rotateZ -0;setAttr FKExtraWrist_L.scaleX 1;setAttr FKExtraWrist_L.scaleY 1;setAttr FKExtraWrist_L.scaleZ 1;setAttr FKWrist_L.translateX -1.110223025e-16;setAttr FKWrist_L.translateY 8.881784197e-16;setAttr FKWrist_L.translateZ -8.881784197e-16;setAttr FKWrist_L.rotateX -0;setAttr FKWrist_L.rotateY 0;setAttr FKWrist_L.rotateZ -0;setAttr FKWrist_L.scaleX 1;setAttr FKWrist_L.scaleY 1;setAttr FKWrist_L.scaleZ 1;setAttr FKExtraElbow_L.translateX 8.881784197e-16;setAttr FKExtraElbow_L.translateY 0;setAttr FKExtraElbow_L.translateZ 0;setAttr FKExtraElbow_L.rotateX -0;setAttr FKExtraElbow_L.rotateY 0;setAttr FKExtraElbow_L.rotateZ -0;setAttr FKExtraElbow_L.scaleX 1;setAttr FKExtraElbow_L.scaleY 1;setAttr FKExtraElbow_L.scaleZ 1;setAttr FKElbow_L.translateX 8.881784197e-16;setAttr FKElbow_L.translateY 0;setAttr FKElbow_L.translateZ 0;setAttr FKElbow_L.rotateX -0;setAttr FKElbow_L.rotateY 0;setAttr FKElbow_L.rotateZ -0;setAttr FKElbow_L.scaleX 1;setAttr FKElbow_L.scaleY 1;setAttr FKElbow_L.scaleZ 1;setAttr FKExtraShoulder_L.translateX -1.110223025e-16;setAttr FKExtraShoulder_L.translateY 4.440892099e-16;setAttr FKExtraShoulder_L.translateZ 1.776356839e-15;setAttr FKExtraShoulder_L.rotateX -0;setAttr FKExtraShoulder_L.rotateY 0;setAttr FKExtraShoulder_L.rotateZ -0;setAttr FKExtraShoulder_L.scaleX 1;setAttr FKExtraShoulder_L.scaleY 1;setAttr FKExtraShoulder_L.scaleZ 1;setAttr FKShoulder_L.translateX -5.551115123e-17;setAttr FKShoulder_L.translateY 4.440892099e-16;setAttr FKShoulder_L.translateZ 1.776356839e-15;setAttr FKShoulder_L.rotateX -0;setAttr FKShoulder_L.rotateY 0;setAttr FKShoulder_L.rotateZ -0;setAttr FKShoulder_L.scaleX 1;setAttr FKShoulder_L.scaleY 1;setAttr FKShoulder_L.scaleZ 1;setAttr FKExtraClavicle1_L.translateX 0;setAttr FKExtraClavicle1_L.translateY 0;setAttr FKExtraClavicle1_L.translateZ 0;setAttr FKExtraClavicle1_L.rotateX -0;setAttr FKExtraClavicle1_L.rotateY 0;setAttr FKExtraClavicle1_L.rotateZ 0;setAttr FKExtraClavicle1_L.scaleX 1;setAttr FKExtraClavicle1_L.scaleY 1;setAttr FKExtraClavicle1_L.scaleZ 1;setAttr FKClavicle1_L.translateX 0;setAttr FKClavicle1_L.translateY 0;setAttr FKClavicle1_L.translateZ 0;setAttr FKClavicle1_L.rotateX -0;setAttr FKClavicle1_L.rotateY 0;setAttr FKClavicle1_L.rotateZ 0;setAttr FKClavicle1_L.scaleX 1;setAttr FKClavicle1_L.scaleY 1;setAttr FKClavicle1_L.scaleZ 1;setAttr FKExtraPipe1_R.translateX 0;setAttr FKExtraPipe1_R.translateY 0;setAttr FKExtraPipe1_R.translateZ 0;setAttr FKExtraPipe1_R.rotateX -0;setAttr FKExtraPipe1_R.rotateY 0;setAttr FKExtraPipe1_R.rotateZ -0;setAttr FKExtraPipe1_R.scaleX 1;setAttr FKExtraPipe1_R.scaleY 1;setAttr FKExtraPipe1_R.scaleZ 1;setAttr FKPipe1_R.translateX 0;setAttr FKPipe1_R.translateY 0;setAttr FKPipe1_R.translateZ 0;setAttr FKPipe1_R.rotateX -0;setAttr FKPipe1_R.rotateY 0;setAttr FKPipe1_R.rotateZ -0;setAttr FKPipe1_R.scaleX 1;setAttr FKPipe1_R.scaleY 1;setAttr FKPipe1_R.scaleZ 1;setAttr FKExtraPipe2_R.translateX 0;setAttr FKExtraPipe2_R.translateY 0;setAttr FKExtraPipe2_R.translateZ 0;setAttr FKExtraPipe2_R.rotateX -0;setAttr FKExtraPipe2_R.rotateY 0;setAttr FKExtraPipe2_R.rotateZ -0;setAttr FKExtraPipe2_R.scaleX 1;setAttr FKExtraPipe2_R.scaleY 1;setAttr FKExtraPipe2_R.scaleZ 1;setAttr FKPipe2_R.translateX 0;setAttr FKPipe2_R.translateY 0;setAttr FKPipe2_R.translateZ 0;setAttr FKPipe2_R.rotateX -0;setAttr FKPipe2_R.rotateY 0;setAttr FKPipe2_R.rotateZ -0;setAttr FKPipe2_R.scaleX 1;setAttr FKPipe2_R.scaleY 1;setAttr FKPipe2_R.scaleZ 1;setAttr FKExtraChest_M.translateX 0;setAttr FKExtraChest_M.translateY 0;setAttr FKExtraChest_M.translateZ 0;setAttr FKExtraChest_M.rotateX 0;setAttr FKExtraChest_M.rotateY -0;setAttr FKExtraChest_M.rotateZ 0;setAttr FKExtraChest_M.scaleX 1;setAttr FKExtraChest_M.scaleY 1;setAttr FKExtraChest_M.scaleZ 1;setAttr FKChest_M.translateX 0;setAttr FKChest_M.translateY 0;setAttr FKChest_M.translateZ 0;setAttr FKChest_M.rotateX 0;setAttr FKChest_M.rotateY -0;setAttr FKChest_M.rotateZ 0;setAttr FKChest_M.scaleX 1;setAttr FKChest_M.scaleY 1;setAttr FKChest_M.scaleZ 1;setAttr FKExtraSpineA_M.translateX 0;setAttr FKExtraSpineA_M.translateY 0;setAttr FKExtraSpineA_M.translateZ 0;setAttr FKExtraSpineA_M.rotateX 0;setAttr FKExtraSpineA_M.rotateY -0;setAttr FKExtraSpineA_M.rotateZ 0;setAttr FKExtraSpineA_M.scaleX 1;setAttr FKExtraSpineA_M.scaleY 1;setAttr FKExtraSpineA_M.scaleZ 1;setAttr FKSpineA_M.translateX 0;setAttr FKSpineA_M.translateY 0;setAttr FKSpineA_M.translateZ 0;setAttr FKSpineA_M.rotateX 0;setAttr FKSpineA_M.rotateY -0;setAttr FKSpineA_M.rotateZ 0;setAttr FKSpineA_M.scaleX 1;setAttr FKSpineA_M.scaleY 1;setAttr FKSpineA_M.scaleZ 1;setAttr FKExtraMiddleToe1_R.translateX 0;setAttr FKExtraMiddleToe1_R.translateY -3.330669074e-16;setAttr FKExtraMiddleToe1_R.translateZ -1.110223025e-16;setAttr FKExtraMiddleToe1_R.rotateX -0;setAttr FKExtraMiddleToe1_R.rotateY 0;setAttr FKExtraMiddleToe1_R.rotateZ -0;setAttr FKExtraMiddleToe1_R.scaleX 1;setAttr FKExtraMiddleToe1_R.scaleY 1;setAttr FKExtraMiddleToe1_R.scaleZ 1;setAttr FKMiddleToe1_R.translateX 0;setAttr FKMiddleToe1_R.translateY 0;setAttr FKMiddleToe1_R.translateZ 0;setAttr FKMiddleToe1_R.rotateX -0;setAttr FKMiddleToe1_R.rotateY 0;setAttr FKMiddleToe1_R.rotateZ -0;setAttr FKMiddleToe1_R.scaleX 1;setAttr FKMiddleToe1_R.scaleY 1;setAttr FKMiddleToe1_R.scaleZ 1;setAttr FKExtraAnkle_R.translateX 0;setAttr FKExtraAnkle_R.translateY 0;setAttr FKExtraAnkle_R.translateZ 0;setAttr FKExtraAnkle_R.rotateX -0;setAttr FKExtraAnkle_R.rotateY -0;setAttr FKExtraAnkle_R.rotateZ 0;setAttr FKExtraAnkle_R.scaleX 1;setAttr FKExtraAnkle_R.scaleY 1;setAttr FKExtraAnkle_R.scaleZ 1;setAttr FKAnkle_R.translateX 0;setAttr FKAnkle_R.translateY 0;setAttr FKAnkle_R.translateZ 0;setAttr FKAnkle_R.rotateX -0;setAttr FKAnkle_R.rotateY -0;setAttr FKAnkle_R.rotateZ 0;setAttr FKAnkle_R.scaleX 1;setAttr FKAnkle_R.scaleY 1;setAttr FKAnkle_R.scaleZ 1;setAttr FKExtraKnee_R.translateX 0;setAttr FKExtraKnee_R.translateY -4.440892099e-16;setAttr FKExtraKnee_R.translateZ -2.775557562e-17;setAttr FKExtraKnee_R.rotateX -0;setAttr FKExtraKnee_R.rotateY 0;setAttr FKExtraKnee_R.rotateZ 0;setAttr FKExtraKnee_R.scaleX 1;setAttr FKExtraKnee_R.scaleY 1;setAttr FKExtraKnee_R.scaleZ 1;setAttr FKKnee_R.translateX 0;setAttr FKKnee_R.translateY -4.440892099e-16;setAttr FKKnee_R.translateZ -2.775557562e-17;setAttr FKKnee_R.rotateX -0;setAttr FKKnee_R.rotateY 0;setAttr FKKnee_R.rotateZ 0;setAttr FKKnee_R.scaleX 1;setAttr FKKnee_R.scaleY 1;setAttr FKKnee_R.scaleZ 1;setAttr FKExtraHip_R.translateX 0;setAttr FKExtraHip_R.translateY 8.881784197e-16;setAttr FKExtraHip_R.translateZ -2.220446049e-16;setAttr FKExtraHip_R.rotateX -0;setAttr FKExtraHip_R.rotateY 0;setAttr FKExtraHip_R.rotateZ 0;setAttr FKExtraHip_R.scaleX 1;setAttr FKExtraHip_R.scaleY 1;setAttr FKExtraHip_R.scaleZ 1;setAttr FKHip_R.translateX -2.220446049e-16;setAttr FKHip_R.translateY 8.881784197e-16;setAttr FKHip_R.translateZ -4.440892099e-16;setAttr FKHip_R.rotateX -0;setAttr FKHip_R.rotateY 0;setAttr FKHip_R.rotateZ 0;setAttr FKHip_R.scaleX 1;setAttr FKHip_R.scaleY 1;setAttr FKHip_R.scaleZ 1;setAttr FKExtraHipTwist_R.translateX 0;setAttr FKExtraHipTwist_R.translateY -2.220446049e-16;setAttr FKExtraHipTwist_R.translateZ 2.220446049e-16;setAttr FKExtraHipTwist_R.rotateX -0;setAttr FKExtraHipTwist_R.rotateY 0;setAttr FKExtraHipTwist_R.rotateZ 0;setAttr FKExtraHipTwist_R.scaleX 1;setAttr FKExtraHipTwist_R.scaleY 1;setAttr FKExtraHipTwist_R.scaleZ 1;setAttr FKHipTwist_R.translateX 0;setAttr FKHipTwist_R.translateY -2.220446049e-16;setAttr FKHipTwist_R.translateZ 2.220446049e-16;setAttr FKHipTwist_R.rotateX -0;setAttr FKHipTwist_R.rotateY 0;setAttr FKHipTwist_R.rotateZ 0;setAttr FKHipTwist_R.scaleX 1;setAttr FKHipTwist_R.scaleY 1;setAttr FKHipTwist_R.scaleZ 1;setAttr FKExtraMiddleToe1_L.translateX -2.220446049e-16;setAttr FKExtraMiddleToe1_L.translateY 1.110223025e-16;setAttr FKExtraMiddleToe1_L.translateZ 0;setAttr FKExtraMiddleToe1_L.rotateX 1.272221873e-14;setAttr FKExtraMiddleToe1_L.rotateY -1.222321267e-14;setAttr FKExtraMiddleToe1_L.rotateZ 1.724642624e-15;setAttr FKExtraMiddleToe1_L.scaleX 1;setAttr FKExtraMiddleToe1_L.scaleY 1;setAttr FKExtraMiddleToe1_L.scaleZ 1;setAttr FKMiddleToe1_L.translateX -2.220446049e-16;setAttr FKMiddleToe1_L.translateY -1.110223025e-16;setAttr FKMiddleToe1_L.translateZ 1.110223025e-16;setAttr FKMiddleToe1_L.rotateX -0;setAttr FKMiddleToe1_L.rotateY 0;setAttr FKMiddleToe1_L.rotateZ -0;setAttr FKMiddleToe1_L.scaleX 1;setAttr FKMiddleToe1_L.scaleY 1;setAttr FKMiddleToe1_L.scaleZ 1;setAttr FKExtraAnkle_L.translateX 0;setAttr FKExtraAnkle_L.translateY -1.110223025e-16;setAttr FKExtraAnkle_L.translateZ 0;setAttr FKExtraAnkle_L.rotateX -0;setAttr FKExtraAnkle_L.rotateY -0;setAttr FKExtraAnkle_L.rotateZ 0;setAttr FKExtraAnkle_L.scaleX 1;setAttr FKExtraAnkle_L.scaleY 1;setAttr FKExtraAnkle_L.scaleZ 1;setAttr FKAnkle_L.translateX 0;setAttr FKAnkle_L.translateY 0;setAttr FKAnkle_L.translateZ 0;setAttr FKAnkle_L.rotateX -0;setAttr FKAnkle_L.rotateY -0;setAttr FKAnkle_L.rotateZ 0;setAttr FKAnkle_L.scaleX 1;setAttr FKAnkle_L.scaleY 1;setAttr FKAnkle_L.scaleZ 1;setAttr FKExtraKnee_L.translateX 0;setAttr FKExtraKnee_L.translateY 4.440892099e-16;setAttr FKExtraKnee_L.translateZ 2.775557562e-17;setAttr FKExtraKnee_L.rotateX -1.964551598e-16;setAttr FKExtraKnee_L.rotateY -3.991223404e-16;setAttr FKExtraKnee_L.rotateZ -6.212020862e-18;setAttr FKExtraKnee_L.scaleX 1;setAttr FKExtraKnee_L.scaleY 1;setAttr FKExtraKnee_L.scaleZ 1;setAttr FKKnee_L.translateX 0;setAttr FKKnee_L.translateY 8.881784197e-16;setAttr FKKnee_L.translateZ 8.326672685e-17;setAttr FKKnee_L.rotateX -0;setAttr FKKnee_L.rotateY 0;setAttr FKKnee_L.rotateZ 0;setAttr FKKnee_L.scaleX 1;setAttr FKKnee_L.scaleY 1;setAttr FKKnee_L.scaleZ 1;setAttr FKExtraHip_L.translateX 0;setAttr FKExtraHip_L.translateY 8.881784197e-16;setAttr FKExtraHip_L.translateZ 4.440892099e-16;setAttr FKExtraHip_L.rotateX 3.182495938e-15;setAttr FKExtraHip_L.rotateY -3.727212517e-16;setAttr FKExtraHip_L.rotateZ -1.242404172e-16;setAttr FKExtraHip_L.scaleX 1;setAttr FKExtraHip_L.scaleY 1;setAttr FKExtraHip_L.scaleZ 1;setAttr FKHip_L.translateX 2.220446049e-16;setAttr FKHip_L.translateY 0;setAttr FKHip_L.translateZ 4.440892099e-16;setAttr FKHip_L.rotateX -0;setAttr FKHip_L.rotateY 0;setAttr FKHip_L.rotateZ 0;setAttr FKHip_L.scaleX 1;setAttr FKHip_L.scaleY 1;setAttr FKHip_L.scaleZ 1;setAttr FKExtraHipTwist_L.translateX 0;setAttr FKExtraHipTwist_L.translateY 0;setAttr FKExtraHipTwist_L.translateZ -2.220446049e-16;setAttr FKExtraHipTwist_L.rotateX 1.049583045e-13;setAttr FKExtraHipTwist_L.rotateY 1.529418532e-29;setAttr FKExtraHipTwist_L.rotateZ 1.669791208e-14;setAttr FKExtraHipTwist_L.scaleX 1;setAttr FKExtraHipTwist_L.scaleY 1;setAttr FKExtraHipTwist_L.scaleZ 1;setAttr FKHipTwist_L.translateX 0;setAttr FKHipTwist_L.translateY -2.220446049e-16;setAttr FKHipTwist_L.translateZ 0;setAttr FKHipTwist_L.rotateX -0;setAttr FKHipTwist_L.rotateY 0;setAttr FKHipTwist_L.rotateZ 0;setAttr FKHipTwist_L.scaleX 1;setAttr FKHipTwist_L.scaleY 1;setAttr FKHipTwist_L.scaleZ 1;setAttr HipSwingerPelvis_M.rotateX 0;setAttr HipSwingerPelvis_M.rotateY -0;setAttr HipSwingerPelvis_M.rotateZ 0;setAttr IKExtraArm1_R.translateX 0;setAttr IKExtraArm1_R.translateY 0;setAttr IKExtraArm1_R.translateZ 0;setAttr IKExtraArm1_R.rotateX -0;setAttr IKExtraArm1_R.rotateY 0;setAttr IKExtraArm1_R.rotateZ -0;setAttr IKArm1_R.translateX 0;setAttr IKArm1_R.translateY 0;setAttr IKArm1_R.translateZ 0;setAttr IKArm1_R.rotateX -0;setAttr IKArm1_R.rotateY 0;setAttr IKArm1_R.rotateZ -0;setAttr IKArm1_R.follow 0;setAttr IKExtraLegBall_R.rotateX 0;setAttr IKExtraLegBall_R.rotateY -0;setAttr IKExtraLegBall_R.rotateZ 0;setAttr IKLegBall_R.rotateX 0;setAttr IKExtraSpine0_M.visibility 1;setAttr IKExtraSpine0_M.translateX 0;setAttr IKExtraSpine0_M.translateY 0;setAttr IKExtraSpine0_M.translateZ 0;setAttr IKExtraSpine0_M.rotateX -0;setAttr IKExtraSpine0_M.rotateY -0;setAttr IKExtraSpine0_M.rotateZ 0;setAttr IKExtraSpine0_M.scaleX 1;setAttr IKExtraSpine0_M.scaleY 1;setAttr IKExtraSpine0_M.scaleZ 1;setAttr IKSpine0_M.translateX 0;setAttr IKSpine0_M.translateY 0;setAttr IKSpine0_M.translateZ 0;setAttr IKSpine0_M.rotateX -0;setAttr IKSpine0_M.rotateY -0;setAttr IKSpine0_M.rotateZ 0;setAttr IKSpine0_M.stiff 0;setAttr IKExtraSpine2_M.visibility 1;setAttr IKExtraSpine2_M.translateX 0;setAttr IKExtraSpine2_M.translateY 0;setAttr IKExtraSpine2_M.translateZ 0;setAttr IKExtraSpine2_M.rotateX 0;setAttr IKExtraSpine2_M.rotateY -0;setAttr IKExtraSpine2_M.rotateZ 0;setAttr IKExtraSpine2_M.scaleX 1;setAttr IKExtraSpine2_M.scaleY 1;setAttr IKExtraSpine2_M.scaleZ 1;setAttr IKSpine2_M.translateX 0;setAttr IKSpine2_M.translateY 0;setAttr IKSpine2_M.translateZ 0;setAttr IKSpine2_M.rotateX 0;setAttr IKSpine2_M.rotateY -0;setAttr IKSpine2_M.rotateZ 0;setAttr IKSpine2_M.stiff 0;setAttr IKExtraSpine4_M.visibility 1;setAttr IKExtraSpine4_M.translateX 0;setAttr IKExtraSpine4_M.translateY 0;setAttr IKExtraSpine4_M.translateZ 0;setAttr IKExtraSpine4_M.rotateX 0;setAttr IKExtraSpine4_M.rotateY -0;setAttr IKExtraSpine4_M.rotateZ 0;setAttr IKExtraSpine4_M.scaleX 1;setAttr IKExtraSpine4_M.scaleY 1;setAttr IKExtraSpine4_M.scaleZ 1;setAttr IKSpine4_M.translateX 0;setAttr IKSpine4_M.translateY 0;setAttr IKSpine4_M.translateZ 0;setAttr IKSpine4_M.rotateX 0;setAttr IKSpine4_M.rotateY -0;setAttr IKSpine4_M.rotateZ 0;setAttr IKSpine4_M.stiff 0;setAttr IKSpine4_M.stretchy 0;setAttr FKIKSpine_M.FKIKBlend 0;setAttr FKIKSpine_M.FKVis 1;setAttr FKIKSpine_M.IKVis 0;setAttr Main.visibility 1;setAttr Main.translateX 0;setAttr Main.translateY 0;setAttr Main.translateZ 0;setAttr Main.rotateX 0;setAttr Main.rotateY 0;setAttr Main.rotateZ 0;setAttr Main.scaleX 1;setAttr Main.scaleY 1;setAttr Main.scaleZ 1;setAttr FKExtraBarrel1_R.translateX -1.33226763e-15;setAttr FKExtraBarrel1_R.translateY -6.661338148e-16;setAttr FKExtraBarrel1_R.translateZ -1.776356839e-15;setAttr FKExtraBarrel1_R.rotateX -0;setAttr FKExtraBarrel1_R.rotateY 0;setAttr FKExtraBarrel1_R.rotateZ -0;setAttr FKExtraBarrel1_R.scaleX 1;setAttr FKExtraBarrel1_R.scaleY 1;setAttr FKExtraBarrel1_R.scaleZ 1;setAttr FKBarrel1_R.translateX -4.440892099e-16;setAttr FKBarrel1_R.translateY 4.440892099e-16;setAttr FKBarrel1_R.translateZ 0;setAttr FKBarrel1_R.rotateX -0;setAttr FKBarrel1_R.rotateY 0;setAttr FKBarrel1_R.rotateZ -0;setAttr FKBarrel1_R.scaleX 1;setAttr FKBarrel1_R.scaleY 1;setAttr FKBarrel1_R.scaleZ 1;setAttr FKExtraBarrel3_R.translateX -8.881784197e-16;setAttr FKExtraBarrel3_R.translateY -4.440892099e-16;setAttr FKExtraBarrel3_R.translateZ 0;setAttr FKExtraBarrel3_R.rotateX -0;setAttr FKExtraBarrel3_R.rotateY 0;setAttr FKExtraBarrel3_R.rotateZ -0;setAttr FKExtraBarrel3_R.scaleX 1;setAttr FKExtraBarrel3_R.scaleY 1;setAttr FKExtraBarrel3_R.scaleZ 1;setAttr FKBarrel3_R.translateX -8.881784197e-16;setAttr FKBarrel3_R.translateY 2.220446049e-16;setAttr FKBarrel3_R.translateZ 0;setAttr FKBarrel3_R.rotateX -0;setAttr FKBarrel3_R.rotateY 0;setAttr FKBarrel3_R.rotateZ -0;setAttr FKBarrel3_R.scaleX 1;setAttr FKBarrel3_R.scaleY 1;setAttr FKBarrel3_R.scaleZ 1;setAttr FKExtraBarrel2_R.translateX -4.440892099e-16;setAttr FKExtraBarrel2_R.translateY -6.661338148e-16;setAttr FKExtraBarrel2_R.translateZ -1.776356839e-15;setAttr FKExtraBarrel2_R.rotateX 0;setAttr FKExtraBarrel2_R.rotateY 9.93923338e-17;setAttr FKExtraBarrel2_R.rotateZ -1.192708006e-14;setAttr FKExtraBarrel2_R.scaleX 1;setAttr FKExtraBarrel2_R.scaleY 1;setAttr FKExtraBarrel2_R.scaleZ 1;setAttr FKBarrel2_R.translateX 8.881784197e-16;setAttr FKBarrel2_R.translateY -4.440892099e-16;setAttr FKBarrel2_R.translateZ 0;setAttr FKBarrel2_R.rotateX 1.590277341e-14;setAttr FKBarrel2_R.rotateY 1.043619505e-14;setAttr FKBarrel2_R.rotateZ -3.975693352e-16;setAttr FKBarrel2_R.scaleX 1;setAttr FKBarrel2_R.scaleY 1;setAttr FKBarrel2_R.scaleZ 1;setAttr FKExtraWrist1_R.translateX -1.776356839e-15;setAttr FKExtraWrist1_R.translateY -4.440892099e-16;setAttr FKExtraWrist1_R.translateZ -8.881784197e-16;setAttr FKExtraWrist1_R.rotateX -0;setAttr FKExtraWrist1_R.rotateY 0;setAttr FKExtraWrist1_R.rotateZ -0;setAttr FKExtraWrist1_R.scaleX 1;setAttr FKExtraWrist1_R.scaleY 1;setAttr FKExtraWrist1_R.scaleZ 1;setAttr FKWrist1_R.translateX 1.776356839e-15;setAttr FKWrist1_R.translateY 2.220446049e-16;setAttr FKWrist1_R.translateZ -7.771561172e-16;setAttr FKWrist1_R.rotateX -0;setAttr FKWrist1_R.rotateY 0;setAttr FKWrist1_R.rotateZ -0;setAttr FKWrist1_R.scaleX 1;setAttr FKWrist1_R.scaleY 1;setAttr FKWrist1_R.scaleZ 1;setAttr FKExtraWrist2_R.translateX -1.776356839e-15;setAttr FKExtraWrist2_R.translateY -2.220446049e-16;setAttr FKExtraWrist2_R.translateZ -5.551115123e-16;setAttr FKExtraWrist2_R.rotateX -0;setAttr FKExtraWrist2_R.rotateY 0;setAttr FKExtraWrist2_R.rotateZ -0;setAttr FKExtraWrist2_R.scaleX 1;setAttr FKExtraWrist2_R.scaleY 1;setAttr FKExtraWrist2_R.scaleZ 1;setAttr FKWrist2_R.translateX 1.776356839e-15;setAttr FKWrist2_R.translateY -2.220446049e-16;setAttr FKWrist2_R.translateZ -1.110223025e-16;setAttr FKWrist2_R.rotateX -0;setAttr FKWrist2_R.rotateY 0;setAttr FKWrist2_R.rotateZ -0;setAttr FKWrist2_R.scaleX 1;setAttr FKWrist2_R.scaleY 1;setAttr FKWrist2_R.scaleZ 1;setAttr FKExtraElbow1_R.translateX 5.551115123e-16;setAttr FKExtraElbow1_R.translateY 0;setAttr FKExtraElbow1_R.translateZ 1.776356839e-15;setAttr FKExtraElbow1_R.rotateX -0;setAttr FKExtraElbow1_R.rotateY 0;setAttr FKExtraElbow1_R.rotateZ -0;setAttr FKExtraElbow1_R.scaleX 1;setAttr FKExtraElbow1_R.scaleY 1;setAttr FKExtraElbow1_R.scaleZ 1;setAttr FKElbow1_R.translateX 1.110223025e-16;setAttr FKElbow1_R.translateY 0;setAttr FKElbow1_R.translateZ -1.776356839e-15;setAttr FKElbow1_R.rotateX -0;setAttr FKElbow1_R.rotateY 0;setAttr FKElbow1_R.rotateZ -0;setAttr FKElbow1_R.scaleX 1;setAttr FKElbow1_R.scaleY 1;setAttr FKElbow1_R.scaleZ 1;setAttr FKExtraShoulder1_R.translateX -5.551115123e-17;setAttr FKExtraShoulder1_R.translateY 4.440892099e-16;setAttr FKExtraShoulder1_R.translateZ 3.552713679e-15;setAttr FKExtraShoulder1_R.rotateX -0;setAttr FKExtraShoulder1_R.rotateY 0;setAttr FKExtraShoulder1_R.rotateZ -0;setAttr FKExtraShoulder1_R.scaleX 1;setAttr FKExtraShoulder1_R.scaleY 1;setAttr FKExtraShoulder1_R.scaleZ 1;setAttr FKShoulder1_R.translateX 5.551115123e-17;setAttr FKShoulder1_R.translateY 0;setAttr FKShoulder1_R.translateZ 3.552713679e-15;setAttr FKShoulder1_R.rotateX -0;setAttr FKShoulder1_R.rotateY 0;setAttr FKShoulder1_R.rotateZ -0;setAttr FKShoulder1_R.scaleX 1;setAttr FKShoulder1_R.scaleY 1;setAttr FKShoulder1_R.scaleZ 1;setAttr FKExtraClavicle_R.translateX 2.775557562e-17;setAttr FKExtraClavicle_R.translateY 0;setAttr FKExtraClavicle_R.translateZ -1.776356839e-15;setAttr FKExtraClavicle_R.rotateX -0;setAttr FKExtraClavicle_R.rotateY 0;setAttr FKExtraClavicle_R.rotateZ 0;setAttr FKExtraClavicle_R.scaleX 1;setAttr FKExtraClavicle_R.scaleY 1;setAttr FKExtraClavicle_R.scaleZ 1;setAttr FKClavicle_R.translateX 5.551115123e-17;setAttr FKClavicle_R.translateY 0;setAttr FKClavicle_R.translateZ -1.776356839e-15;setAttr FKClavicle_R.rotateX -0;setAttr FKClavicle_R.rotateY 0;setAttr FKClavicle_R.rotateZ 0;setAttr FKClavicle_R.scaleX 1;setAttr FKClavicle_R.scaleY 1;setAttr FKClavicle_R.scaleZ 1;setAttr FKExtraHead_M.translateX 0;setAttr FKExtraHead_M.translateY 0;setAttr FKExtraHead_M.translateZ 0;setAttr FKExtraHead_M.rotateX -0;setAttr FKExtraHead_M.rotateY 0;setAttr FKExtraHead_M.rotateZ -0;setAttr FKExtraHead_M.scaleX 1;setAttr FKExtraHead_M.scaleY 1;setAttr FKExtraHead_M.scaleZ 1;setAttr FKHead_M.translateX 0;setAttr FKHead_M.translateY 0;setAttr FKHead_M.translateZ 0;setAttr FKHead_M.rotateX -0;setAttr FKHead_M.rotateY 0;setAttr FKHead_M.rotateZ -0;setAttr FKHead_M.scaleX 1;setAttr FKHead_M.scaleY 1;setAttr FKHead_M.scaleZ 1;setAttr FKExtraMiddleFinger6_L.translateX -8.881784197e-16;setAttr FKExtraMiddleFinger6_L.translateY -4.440892099e-16;setAttr FKExtraMiddleFinger6_L.translateZ 4.440892099e-16;setAttr FKExtraMiddleFinger6_L.rotateX -0;setAttr FKExtraMiddleFinger6_L.rotateY 0;setAttr FKExtraMiddleFinger6_L.rotateZ -0;setAttr FKExtraMiddleFinger6_L.scaleX 1;setAttr FKExtraMiddleFinger6_L.scaleY 1;setAttr FKExtraMiddleFinger6_L.scaleZ 1;setAttr FKMiddleFinger6_L.translateX -1.776356839e-15;setAttr FKMiddleFinger6_L.translateY -4.440892099e-16;setAttr FKMiddleFinger6_L.translateZ 0;setAttr FKMiddleFinger6_L.rotateX -0;setAttr FKMiddleFinger6_L.rotateY 0;setAttr FKMiddleFinger6_L.rotateZ -0;setAttr FKMiddleFinger6_L.scaleX 1;setAttr FKMiddleFinger6_L.scaleY 1;setAttr FKMiddleFinger6_L.scaleZ 1;setAttr FKExtraMiddleFinger5_L.translateX -1.776356839e-15;setAttr FKExtraMiddleFinger5_L.translateY 8.881784197e-16;setAttr FKExtraMiddleFinger5_L.translateZ 0;setAttr FKExtraMiddleFinger5_L.rotateX -0;setAttr FKExtraMiddleFinger5_L.rotateY 0;setAttr FKExtraMiddleFinger5_L.rotateZ -0;setAttr FKExtraMiddleFinger5_L.scaleX 1;setAttr FKExtraMiddleFinger5_L.scaleY 1;setAttr FKExtraMiddleFinger5_L.scaleZ 1;setAttr FKMiddleFinger5_L.translateX -8.881784197e-16;setAttr FKMiddleFinger5_L.translateY 0;setAttr FKMiddleFinger5_L.translateZ 4.440892099e-16;setAttr FKMiddleFinger5_L.rotateX -0;setAttr FKMiddleFinger5_L.rotateY 0;setAttr FKMiddleFinger5_L.rotateZ -0;setAttr FKMiddleFinger5_L.scaleX 1;setAttr FKMiddleFinger5_L.scaleY 1;setAttr FKMiddleFinger5_L.scaleZ 1;setAttr PoleExtraArm1_R.translateX 0;setAttr PoleExtraArm1_R.translateY 0;setAttr PoleExtraArm1_R.translateZ 0;setAttr PoleArm1_R.translateX 0;setAttr PoleArm1_R.translateY 8.881784197e-16;setAttr PoleArm1_R.translateZ 4.440892099e-16;setAttr PoleArm1_R.follow 0;setAttr FKIKArm1_R.FKIKBlend 0;setAttr FKIKArm1_R.FKVis 1;setAttr FKIKArm1_R.IKVis 0;setAttr IKExtraArm_L.translateX 0;setAttr IKExtraArm_L.translateY 0;setAttr IKExtraArm_L.translateZ 0;setAttr IKExtraArm_L.rotateX -0;setAttr IKExtraArm_L.rotateY 0;setAttr IKExtraArm_L.rotateZ -0;setAttr IKArm_L.translateX 0;setAttr IKArm_L.translateY 0;setAttr IKArm_L.translateZ 0;setAttr IKArm_L.rotateX -0;setAttr IKArm_L.rotateY 0;setAttr IKArm_L.rotateZ -0;setAttr IKArm_L.follow 0;setAttr PoleExtraArm_L.translateX 0;setAttr PoleExtraArm_L.translateY 0;setAttr PoleExtraArm_L.translateZ 0;setAttr PoleArm_L.translateX 1.776356839e-15;setAttr PoleArm_L.translateY 0;setAttr PoleArm_L.translateZ -1.110223025e-16;setAttr PoleArm_L.follow 0;setAttr FKIKArm_L.FKIKBlend 0;setAttr FKIKArm_L.FKVis 1;setAttr FKIKArm_L.IKVis 0;setAttr IKExtraLeg_R.translateX 0;setAttr IKExtraLeg_R.translateY 0;setAttr IKExtraLeg_R.translateZ 0;setAttr IKExtraLeg_R.rotateX -0;setAttr IKExtraLeg_R.rotateY -0;setAttr IKExtraLeg_R.rotateZ 0;setAttr IKLeg_R.translateX 0;setAttr IKLeg_R.translateY 0;setAttr IKLeg_R.translateZ 0;setAttr IKLeg_R.rotateX -0;setAttr IKLeg_R.rotateY -0;setAttr IKLeg_R.rotateZ 0;setAttr IKLeg_R.swivel 0;setAttr IKLeg_R.toe 0;setAttr IKLeg_R.roll 0;setAttr IKLeg_R.rollAngle 25;setAttr IKLeg_R.stretchy 0;setAttr IKLeg_R.antiPop 0;setAttr IKLeg_R.Length1 1;setAttr IKLeg_R.Length2 1;setAttr PoleExtraLeg_R.translateX 0;setAttr PoleExtraLeg_R.translateY 0;setAttr PoleExtraLeg_R.translateZ 0;setAttr PoleLeg_R.translateX 0;setAttr PoleLeg_R.translateY 8.881784197e-16;setAttr PoleLeg_R.translateZ -4.440892099e-16;setAttr PoleLeg_R.follow 10;setAttr FKIKLeg_R.FKIKBlend 10;setAttr FKIKLeg_R.FKVis 0;setAttr FKIKLeg_R.IKVis 1;setAttr IKExtraLegHeel_R.rotateX 0;setAttr IKExtraLegHeel_R.rotateY -0;setAttr IKExtraLegHeel_R.rotateZ 0;setAttr IKLegHeel_R.rotateX 0;setAttr IKLegHeel_R.rotateY -0;setAttr IKLegHeel_R.rotateZ 0;setAttr IKExtraLegToe_R.rotateX 0;setAttr IKExtraLegToe_R.rotateY -0;setAttr IKExtraLegToe_R.rotateZ 0;setAttr IKLegToe_R.rotateX 0;setAttr IKLegToe_R.rotateY -0;setAttr IKLegToe_R.rotateZ 0;");
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultRenderGlobals;
	setAttr ".fs" 1;
	setAttr ".ef" 10;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "jointLayer.di" "Pelvis.do";
connectAttr "Pelvis.s" "SpineA.is";
connectAttr "jointLayer.di" "SpineA.do";
connectAttr "SpineA.s" "Chest.is";
connectAttr "jointLayer.di" "Chest.do";
connectAttr "Chest.s" "Clavicle.is";
connectAttr "jointLayer.di" "Clavicle.do";
connectAttr "Clavicle.s" "Shoulder1.is";
connectAttr "jointLayer.di" "Shoulder1.do";
connectAttr "Shoulder1.s" "Elbow1.is";
connectAttr "jointLayer.di" "Elbow1.do";
connectAttr "Elbow1.s" "Wrist2.is";
connectAttr "jointLayer.di" "Wrist2.do";
connectAttr "jointLayer.di" "Wrist1.do";
connectAttr "Wrist1.s" "Barrel1.is";
connectAttr "jointLayer.di" "Barrel1.do";
connectAttr "Barrel1.s" "Barrel1_End.is";
connectAttr "jointLayer.di" "Barrel1_End.do";
connectAttr "Wrist1.s" "Barrel3.is";
connectAttr "jointLayer.di" "Barrel3.do";
connectAttr "Barrel3.s" "Barrel3_End.is";
connectAttr "jointLayer.di" "Barrel3_End.do";
connectAttr "Wrist1.s" "Barrel2.is";
connectAttr "jointLayer.di" "Barrel2.do";
connectAttr "Barrel2.s" "Barrel2_End.is";
connectAttr "jointLayer.di" "Barrel2_End.do";
connectAttr "Chest.s" "Head.is";
connectAttr "jointLayer.di" "Head.do";
connectAttr "Head.s" "Head_End.is";
connectAttr "jointLayer.di" "Head_End.do";
connectAttr "Chest.s" "Clavicle1.is";
connectAttr "jointLayer.di" "Clavicle1.do";
connectAttr "Clavicle1.s" "Shoulder.is";
connectAttr "jointLayer.di" "Shoulder.do";
connectAttr "Shoulder.s" "Elbow.is";
connectAttr "jointLayer.di" "Elbow.do";
connectAttr "Elbow.s" "Wrist.is";
connectAttr "jointLayer.di" "Wrist.do";
connectAttr "Wrist.s" "MiddleFinger1.is";
connectAttr "jointLayer.di" "MiddleFinger1.do";
connectAttr "MiddleFinger1.s" "MiddleFinger5.is";
connectAttr "jointLayer.di" "MiddleFinger5.do";
connectAttr "MiddleFinger5.s" "MiddleFinger6.is";
connectAttr "jointLayer.di" "MiddleFinger6.do";
connectAttr "MiddleFinger6.s" "MiddleFinger7_End.is";
connectAttr "jointLayer.di" "MiddleFinger7_End.do";
connectAttr "MiddleFinger1.s" "MiddleFinger2.is";
connectAttr "jointLayer.di" "MiddleFinger2.do";
connectAttr "MiddleFinger2.s" "MiddleFinger3.is";
connectAttr "jointLayer.di" "MiddleFinger3.do";
connectAttr "MiddleFinger3.s" "MiddleFinger4_End.is";
connectAttr "jointLayer.di" "MiddleFinger4_End.do";
connectAttr "Wrist.s" "ThumbFinger1.is";
connectAttr "jointLayer.di" "ThumbFinger1.do";
connectAttr "ThumbFinger1.s" "ThumbFinger2.is";
connectAttr "jointLayer.di" "ThumbFinger2.do";
connectAttr "ThumbFinger2.s" "ThumbFinger3_End.is";
connectAttr "jointLayer.di" "ThumbFinger3_End.do";
connectAttr "Wrist.s" "Hand_AP1.is";
connectAttr "jointLayer.di" "Hand_AP1.do";
connectAttr "Chest.s" "Pipe1.is";
connectAttr "jointLayer.di" "Pipe1.do";
connectAttr "Pipe1.s" "Pipe1_End.is";
connectAttr "jointLayer.di" "Pipe1_End.do";
connectAttr "Chest.s" "Pipe2.is";
connectAttr "jointLayer.di" "Pipe2.do";
connectAttr "Pipe2.s" "Pipe2_End.is";
connectAttr "jointLayer.di" "Pipe2_End.do";
connectAttr "Pelvis.s" "HipTwist.is";
connectAttr "jointLayer.di" "HipTwist.do";
connectAttr "jointLayer.di" "Hip.do";
connectAttr "Hip.s" "Knee.is";
connectAttr "jointLayer.di" "Knee.do";
connectAttr "Knee.s" "Ankle.is";
connectAttr "jointLayer.di" "Ankle.do";
connectAttr "Ankle.s" "MiddleToe1.is";
connectAttr "jointLayer.di" "MiddleToe1.do";
connectAttr "MiddleToe1.s" "MiddleToe2_End.is";
connectAttr "jointLayer.di" "MiddleToe2_End.do";
connectAttr "Ankle.s" "Heel_End.is";
connectAttr "jointLayer.di" "Heel_End.do";
connectAttr "Main.fkVis" "FKSystem.v";
connectAttr "Main.fingerVis" "FKParentConstraintToWrist1_R.v";
connectAttr "FKParentConstraintToWrist1_R_parentConstraint1.ctx" "FKParentConstraintToWrist1_R.tx"
		;
connectAttr "FKParentConstraintToWrist1_R_parentConstraint1.cty" "FKParentConstraintToWrist1_R.ty"
		;
connectAttr "FKParentConstraintToWrist1_R_parentConstraint1.ctz" "FKParentConstraintToWrist1_R.tz"
		;
connectAttr "FKParentConstraintToWrist1_R_parentConstraint1.crx" "FKParentConstraintToWrist1_R.rx"
		;
connectAttr "FKParentConstraintToWrist1_R_parentConstraint1.cry" "FKParentConstraintToWrist1_R.ry"
		;
connectAttr "FKParentConstraintToWrist1_R_parentConstraint1.crz" "FKParentConstraintToWrist1_R.rz"
		;
connectAttr "jointLayer.di" "FKOffsetBarrel1_R.do";
connectAttr "jointLayer.di" "FKXBarrel1_R.do";
connectAttr "FKBarrel1_R.s" "FKXBarrel1_End_R.is";
connectAttr "jointLayer.di" "FKXBarrel1_End_R.do";
connectAttr "jointLayer.di" "FKOffsetBarrel3_R.do";
connectAttr "jointLayer.di" "FKXBarrel3_R.do";
connectAttr "FKBarrel3_R.s" "FKXBarrel3_End_R.is";
connectAttr "jointLayer.di" "FKXBarrel3_End_R.do";
connectAttr "jointLayer.di" "FKOffsetBarrel2_R.do";
connectAttr "jointLayer.di" "FKXBarrel2_R.do";
connectAttr "FKBarrel2_R.s" "FKXBarrel2_End_R.is";
connectAttr "jointLayer.di" "FKXBarrel2_End_R.do";
connectAttr "FKParentConstraintToWrist1_R.ro" "FKParentConstraintToWrist1_R_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToWrist1_R.pim" "FKParentConstraintToWrist1_R_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToWrist1_R.rp" "FKParentConstraintToWrist1_R_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToWrist1_R.rpt" "FKParentConstraintToWrist1_R_parentConstraint1.crt"
		;
connectAttr "Wrist1_R.t" "FKParentConstraintToWrist1_R_parentConstraint1.tg[0].tt"
		;
connectAttr "Wrist1_R.rp" "FKParentConstraintToWrist1_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Wrist1_R.rpt" "FKParentConstraintToWrist1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Wrist1_R.r" "FKParentConstraintToWrist1_R_parentConstraint1.tg[0].tr"
		;
connectAttr "Wrist1_R.ro" "FKParentConstraintToWrist1_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Wrist1_R.s" "FKParentConstraintToWrist1_R_parentConstraint1.tg[0].ts"
		;
connectAttr "Wrist1_R.pm" "FKParentConstraintToWrist1_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Wrist1_R.jo" "FKParentConstraintToWrist1_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToWrist1_R_parentConstraint1.w0" "FKParentConstraintToWrist1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.ctx" "FKParentConstraintToChest_M.tx"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.cty" "FKParentConstraintToChest_M.ty"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.ctz" "FKParentConstraintToChest_M.tz"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.crx" "FKParentConstraintToChest_M.rx"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.cry" "FKParentConstraintToChest_M.ry"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.crz" "FKParentConstraintToChest_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetClavicle_R.do";
connectAttr "jointLayer.di" "FKXClavicle_R.do";
connectAttr "FKXClavicle_R.s" "FKOffsetShoulder1_R.is";
connectAttr "FKIKBlendArm1Condition_R.ocg" "FKOffsetShoulder1_R.v" -l on;
connectAttr "jointLayer.di" "FKOffsetShoulder1_R.do";
connectAttr "jointLayer.di" "FKXShoulder1_R.do";
connectAttr "FKXShoulder1_R.s" "FKOffsetElbow1_R.is";
connectAttr "jointLayer.di" "FKOffsetElbow1_R.do";
connectAttr "FKShoulder1_R.s" "FKXElbow1_R.is";
connectAttr "jointLayer.di" "FKXElbow1_R.do";
connectAttr "FKXElbow1_R.s" "FKOffsetWrist2_R.is";
connectAttr "jointLayer.di" "FKOffsetWrist2_R.do";
connectAttr "FKElbow1_R.s" "FKXWrist2_R.is";
connectAttr "jointLayer.di" "FKXWrist2_R.do";
connectAttr "FKXWrist2_R.s" "FKOffsetWrist1_R.is";
connectAttr "jointLayer.di" "FKOffsetWrist1_R.do";
connectAttr "FKWrist2_R.s" "FKXWrist1_R.is";
connectAttr "jointLayer.di" "FKXWrist1_R.do";
connectAttr "jointLayer.di" "FKOffsetHead_M.do";
connectAttr "jointLayer.di" "FKXHead_M.do";
connectAttr "FKHead_M.s" "FKXHead_End_M.is";
connectAttr "jointLayer.di" "FKXHead_End_M.do";
connectAttr "jointLayer.di" "FKOffsetClavicle1_L.do";
connectAttr "jointLayer.di" "FKXClavicle1_L.do";
connectAttr "FKXClavicle1_L.s" "FKOffsetShoulder_L.is";
connectAttr "FKIKBlendArmCondition_L.ocg" "FKOffsetShoulder_L.v" -l on;
connectAttr "jointLayer.di" "FKOffsetShoulder_L.do";
connectAttr "jointLayer.di" "FKXShoulder_L.do";
connectAttr "FKXShoulder_L.s" "FKOffsetElbow_L.is";
connectAttr "jointLayer.di" "FKOffsetElbow_L.do";
connectAttr "FKShoulder_L.s" "FKXElbow_L.is";
connectAttr "jointLayer.di" "FKXElbow_L.do";
connectAttr "FKXElbow_L.s" "FKOffsetWrist_L.is";
connectAttr "jointLayer.di" "FKOffsetWrist_L.do";
connectAttr "FKElbow_L.s" "FKXWrist_L.is";
connectAttr "jointLayer.di" "FKXWrist_L.do";
connectAttr "FKWrist_L.s" "FKXHand_AP1_L.is";
connectAttr "jointLayer.di" "FKXHand_AP1_L.do";
connectAttr "jointLayer.di" "FKOffsetPipe1_R.do";
connectAttr "jointLayer.di" "FKXPipe1_R.do";
connectAttr "FKPipe1_R.s" "FKXPipe1_End_R.is";
connectAttr "jointLayer.di" "FKXPipe1_End_R.do";
connectAttr "jointLayer.di" "FKOffsetPipe2_R.do";
connectAttr "jointLayer.di" "FKXPipe2_R.do";
connectAttr "FKPipe2_R.s" "FKXPipe2_End_R.is";
connectAttr "jointLayer.di" "FKXPipe2_End_R.do";
connectAttr "FKParentConstraintToChest_M.ro" "FKParentConstraintToChest_M_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToChest_M.pim" "FKParentConstraintToChest_M_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToChest_M.rp" "FKParentConstraintToChest_M_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToChest_M.rpt" "FKParentConstraintToChest_M_parentConstraint1.crt"
		;
connectAttr "Chest_M.t" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Chest_M.rp" "FKParentConstraintToChest_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Chest_M.rpt" "FKParentConstraintToChest_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Chest_M.r" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Chest_M.ro" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Chest_M.s" "FKParentConstraintToChest_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Chest_M.pm" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "Chest_M.jo" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.w0" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tw"
		;
connectAttr "Main.fingerVis" "FKParentConstraintToWrist_L.v";
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.ctx" "FKParentConstraintToWrist_L.tx"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.cty" "FKParentConstraintToWrist_L.ty"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.ctz" "FKParentConstraintToWrist_L.tz"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.crx" "FKParentConstraintToWrist_L.rx"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.cry" "FKParentConstraintToWrist_L.ry"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.crz" "FKParentConstraintToWrist_L.rz"
		;
connectAttr "jointLayer.di" "FKOffsetMiddleFinger1_L.do";
connectAttr "jointLayer.di" "FKXMiddleFinger1_L.do";
connectAttr "FKXMiddleFinger1_L.s" "FKOffsetMiddleFinger5_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger5_L.do";
connectAttr "FKMiddleFinger1_L.s" "FKXMiddleFinger5_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger5_L.do";
connectAttr "FKXMiddleFinger5_L.s" "FKOffsetMiddleFinger6_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger6_L.do";
connectAttr "FKMiddleFinger5_L.s" "FKXMiddleFinger6_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger6_L.do";
connectAttr "FKMiddleFinger6_L.s" "FKXMiddleFinger7_End_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger7_End_L.do";
connectAttr "FKXMiddleFinger1_L.s" "FKOffsetMiddleFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger2_L.do";
connectAttr "FKMiddleFinger1_L.s" "FKXMiddleFinger2_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger2_L.do";
connectAttr "FKXMiddleFinger2_L.s" "FKOffsetMiddleFinger3_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger3_L.do";
connectAttr "FKMiddleFinger2_L.s" "FKXMiddleFinger3_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger3_L.do";
connectAttr "FKMiddleFinger3_L.s" "FKXMiddleFinger4_End_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger4_End_L.do";
connectAttr "jointLayer.di" "FKOffsetThumbFinger1_L.do";
connectAttr "jointLayer.di" "FKXThumbFinger1_L.do";
connectAttr "FKXThumbFinger1_L.s" "FKOffsetThumbFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetThumbFinger2_L.do";
connectAttr "FKThumbFinger1_L.s" "FKXThumbFinger2_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger2_L.do";
connectAttr "FKThumbFinger2_L.s" "FKXThumbFinger3_End_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger3_End_L.do";
connectAttr "FKParentConstraintToWrist_L.ro" "FKParentConstraintToWrist_L_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToWrist_L.pim" "FKParentConstraintToWrist_L_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToWrist_L.rp" "FKParentConstraintToWrist_L_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToWrist_L.rpt" "FKParentConstraintToWrist_L_parentConstraint1.crt"
		;
connectAttr "Wrist_L.t" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Wrist_L.rp" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Wrist_L.rpt" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Wrist_L.r" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Wrist_L.ro" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Wrist_L.s" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Wrist_L.pm" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Wrist_L.jo" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.w0" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.ctx" "FKParentConstraintToAnkle_R.tx"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.cty" "FKParentConstraintToAnkle_R.ty"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.ctz" "FKParentConstraintToAnkle_R.tz"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.crx" "FKParentConstraintToAnkle_R.rx"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.cry" "FKParentConstraintToAnkle_R.ry"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.crz" "FKParentConstraintToAnkle_R.rz"
		;
connectAttr "unitConversion6.o" "FKOffsetMiddleToe1_R.rx";
connectAttr "jointLayer.di" "FKOffsetMiddleToe1_R.do";
connectAttr "jointLayer.di" "FKXMiddleToe1_R.do";
connectAttr "FKMiddleToe1_R.s" "FKXMiddleToe2_End_R.is";
connectAttr "jointLayer.di" "FKXMiddleToe2_End_R.do";
connectAttr "FKParentConstraintToAnkle_R.ro" "FKParentConstraintToAnkle_R_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToAnkle_R.pim" "FKParentConstraintToAnkle_R_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToAnkle_R.rp" "FKParentConstraintToAnkle_R_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToAnkle_R.rpt" "FKParentConstraintToAnkle_R_parentConstraint1.crt"
		;
connectAttr "Ankle_R.t" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tt"
		;
connectAttr "Ankle_R.rp" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Ankle_R.rpt" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Ankle_R.r" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tr"
		;
connectAttr "Ankle_R.ro" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Ankle_R.s" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].ts"
		;
connectAttr "Ankle_R.pm" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Ankle_R.jo" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.w0" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.ctx" "FKParentConstraintToPelvis_M.tx"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.cty" "FKParentConstraintToPelvis_M.ty"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.ctz" "FKParentConstraintToPelvis_M.tz"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.crx" "FKParentConstraintToPelvis_M.rx"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.cry" "FKParentConstraintToPelvis_M.ry"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.crz" "FKParentConstraintToPelvis_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetHipTwist_R.do";
connectAttr "jointLayer.di" "FKXHipTwist_R.do";
connectAttr "FKXHipTwist_R.s" "FKOffsetHip_R.is";
connectAttr "FKIKBlendLegCondition_R.ocg" "FKOffsetHip_R.v" -l on;
connectAttr "jointLayer.di" "FKOffsetHip_R.do";
connectAttr "jointLayer.di" "FKXHip_R.do";
connectAttr "FKXHip_R.s" "FKOffsetKnee_R.is";
connectAttr "jointLayer.di" "FKOffsetKnee_R.do";
connectAttr "FKHip_R.s" "FKXKnee_R.is";
connectAttr "jointLayer.di" "FKXKnee_R.do";
connectAttr "FKXKnee_R.s" "FKOffsetAnkle_R.is";
connectAttr "jointLayer.di" "FKOffsetAnkle_R.do";
connectAttr "FKKnee_R.s" "FKXAnkle_R.is";
connectAttr "jointLayer.di" "FKXAnkle_R.do";
connectAttr "FKAnkle_R.s" "FKXHeel_End_R.is";
connectAttr "jointLayer.di" "FKXHeel_End_R.do";
connectAttr "jointLayer.di" "FKOffsetHipTwist_L.do";
connectAttr "jointLayer.di" "FKXHipTwist_L.do";
connectAttr "FKXHipTwist_L.s" "FKOffsetHip_L.is";
connectAttr "FKIKBlendLegCondition_L.ocg" "FKOffsetHip_L.v" -l on;
connectAttr "jointLayer.di" "FKOffsetHip_L.do";
connectAttr "jointLayer.di" "FKXHip_L.do";
connectAttr "FKXHip_L.s" "FKOffsetKnee_L.is";
connectAttr "jointLayer.di" "FKOffsetKnee_L.do";
connectAttr "FKHip_L.s" "FKXKnee_L.is";
connectAttr "jointLayer.di" "FKXKnee_L.do";
connectAttr "FKXKnee_L.s" "FKOffsetAnkle_L.is";
connectAttr "jointLayer.di" "FKOffsetAnkle_L.do";
connectAttr "FKKnee_L.s" "FKXAnkle_L.is";
connectAttr "jointLayer.di" "FKXAnkle_L.do";
connectAttr "FKAnkle_L.s" "FKXHeel_End_L.is";
connectAttr "jointLayer.di" "FKXHeel_End_L.do";
connectAttr "FKParentConstraintToPelvis_M.ro" "FKParentConstraintToPelvis_M_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToPelvis_M.pim" "FKParentConstraintToPelvis_M_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToPelvis_M.rp" "FKParentConstraintToPelvis_M_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToPelvis_M.rpt" "FKParentConstraintToPelvis_M_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Pelvis_M.rp" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Pelvis_M.ro" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Pelvis_M.pm" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.w0" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.ctx" "FKParentConstraintToAnkle_L.tx"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.cty" "FKParentConstraintToAnkle_L.ty"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.ctz" "FKParentConstraintToAnkle_L.tz"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.crx" "FKParentConstraintToAnkle_L.rx"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.cry" "FKParentConstraintToAnkle_L.ry"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.crz" "FKParentConstraintToAnkle_L.rz"
		;
connectAttr "unitConversion12.o" "FKOffsetMiddleToe1_L.rx";
connectAttr "jointLayer.di" "FKOffsetMiddleToe1_L.do";
connectAttr "jointLayer.di" "FKXMiddleToe1_L.do";
connectAttr "FKMiddleToe1_L.s" "FKXMiddleToe2_End_L.is";
connectAttr "jointLayer.di" "FKXMiddleToe2_End_L.do";
connectAttr "FKParentConstraintToAnkle_L.ro" "FKParentConstraintToAnkle_L_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToAnkle_L.pim" "FKParentConstraintToAnkle_L_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToAnkle_L.rp" "FKParentConstraintToAnkle_L_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToAnkle_L.rpt" "FKParentConstraintToAnkle_L_parentConstraint1.crt"
		;
connectAttr "Ankle_L.t" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Ankle_L.rp" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Ankle_L.rpt" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Ankle_L.r" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Ankle_L.ro" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Ankle_L.s" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Ankle_L.pm" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Ankle_L.jo" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.w0" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctx" "PelvisCenterBtwLegsBlended_M.tx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cty" "PelvisCenterBtwLegsBlended_M.ty"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctz" "PelvisCenterBtwLegsBlended_M.tz"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crx" "PelvisCenterBtwLegsBlended_M.rx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cry" "PelvisCenterBtwLegsBlended_M.ry"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crz" "PelvisCenterBtwLegsBlended_M.rz"
		;
connectAttr "FKIKBlendSpineCondition_M.ocg" "FKOffsetPelvis_M.v" -l on;
connectAttr "jointLayer.di" "FKOffsetPelvis_M.do";
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.crx" "HipSwingerGroupPelvis_M.rx"
		;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.cry" "HipSwingerGroupPelvis_M.ry"
		;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.crz" "HipSwingerGroupPelvis_M.rz"
		;
connectAttr "HipSwingerGroupPelvis_M.ro" "HipSwingerGroupPelvis_M_orientConstraint1.cro"
		;
connectAttr "HipSwingerGroupPelvis_M.pim" "HipSwingerGroupPelvis_M_orientConstraint1.cpim"
		;
connectAttr "HipSwingerPelvis_M.r" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tr"
		;
connectAttr "HipSwingerPelvis_M.ro" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tro"
		;
connectAttr "HipSwingerPelvis_M.pm" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tpm"
		;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.w0" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "jointLayer.di" "FKXPelvis_M.do";
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.crx" "HipSwingerStabalizePelvis_M.rx"
		;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.cry" "HipSwingerStabalizePelvis_M.ry"
		;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.crz" "HipSwingerStabalizePelvis_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetSpineA_M.do";
connectAttr "FKPelvis_M.s" "FKXSpineA_M.is";
connectAttr "jointLayer.di" "FKXSpineA_M.do";
connectAttr "FKXSpineA_M.s" "FKOffsetChest_M.is";
connectAttr "jointLayer.di" "FKOffsetChest_M.do";
connectAttr "FKSpineA_M.s" "FKXChest_M.is";
connectAttr "jointLayer.di" "FKXChest_M.do";
connectAttr "HipSwingerStabalizePelvis_M.ro" "HipSwingerStabalizePelvis_M_orientConstraint1.cro"
		;
connectAttr "HipSwingerStabalizePelvis_M.pim" "HipSwingerStabalizePelvis_M_orientConstraint1.cpim"
		;
connectAttr "Center_M.r" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tro"
		;
connectAttr "Center_M.pm" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tpm"
		;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.w0" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocg" "HipSwingerPelvis_M.v" -l on;
connectAttr "PelvisCenterBtwLegsBlended_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.pim" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crt"
		;
connectAttr "PelvisCenter_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tt"
		;
connectAttr "PelvisCenter_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trp"
		;
connectAttr "PelvisCenter_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trt"
		;
connectAttr "PelvisCenter_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tr"
		;
connectAttr "PelvisCenter_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tro"
		;
connectAttr "PelvisCenter_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].ts"
		;
connectAttr "PelvisCenter_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trp"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tr"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tro"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].ts"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tw"
		;
connectAttr "PelvisCenterBtwFeetReverse_M.ox" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0"
		;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1"
		;
connectAttr "Main.ikVis" "IKSystem.v";
connectAttr "IKParentConstraintShoulder1_R_parentConstraint1.ctx" "IKParentConstraintShoulder1_R.tx"
		;
connectAttr "IKParentConstraintShoulder1_R_parentConstraint1.cty" "IKParentConstraintShoulder1_R.ty"
		;
connectAttr "IKParentConstraintShoulder1_R_parentConstraint1.ctz" "IKParentConstraintShoulder1_R.tz"
		;
connectAttr "IKParentConstraintShoulder1_R_parentConstraint1.crx" "IKParentConstraintShoulder1_R.rx"
		;
connectAttr "IKParentConstraintShoulder1_R_parentConstraint1.cry" "IKParentConstraintShoulder1_R.ry"
		;
connectAttr "IKParentConstraintShoulder1_R_parentConstraint1.crz" "IKParentConstraintShoulder1_R.rz"
		;
connectAttr "FKIKBlendArm1Condition_R.ocr" "IKParentConstraintShoulder1_R.v";
connectAttr "jointLayer.di" "IKXShoulder1_R.do";
connectAttr "IKXShoulder1_R.s" "IKXElbow1_R.is";
connectAttr "jointLayer.di" "IKXElbow1_R.do";
connectAttr "IKXElbow1_R.s" "IKXWrist2_R.is";
connectAttr "IKXWrist2_R_orientConstraint1.crx" "IKXWrist2_R.rx";
connectAttr "IKXWrist2_R_orientConstraint1.cry" "IKXWrist2_R.ry";
connectAttr "IKXWrist2_R_orientConstraint1.crz" "IKXWrist2_R.rz";
connectAttr "jointLayer.di" "IKXWrist2_R.do";
connectAttr "IKXWrist2_R.s" "IKXWrist1_R.is";
connectAttr "jointLayer.di" "IKXWrist1_R.do";
connectAttr "IKXWrist2_R.ro" "IKXWrist2_R_orientConstraint1.cro";
connectAttr "IKXWrist2_R.pim" "IKXWrist2_R_orientConstraint1.cpim";
connectAttr "IKXWrist2_R.jo" "IKXWrist2_R_orientConstraint1.cjo";
connectAttr "IKArm1_R.r" "IKXWrist2_R_orientConstraint1.tg[0].tr";
connectAttr "IKArm1_R.ro" "IKXWrist2_R_orientConstraint1.tg[0].tro";
connectAttr "IKArm1_R.pm" "IKXWrist2_R_orientConstraint1.tg[0].tpm";
connectAttr "IKXWrist2_R_orientConstraint1.w0" "IKXWrist2_R_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXWrist2_R.tx" "effector1.tx";
connectAttr "IKXWrist2_R.ty" "effector1.ty";
connectAttr "IKXWrist2_R.tz" "effector1.tz";
connectAttr "Main.arrowVis" "PoleAnnotationArm1_R.v";
connectAttr "PoleAnnotateTargetArm1_RShape.wm" "PoleAnnotationArm1_RShape.dom" -na
		;
connectAttr "IKParentConstraintShoulder1_R.ro" "IKParentConstraintShoulder1_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintShoulder1_R.pim" "IKParentConstraintShoulder1_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintShoulder1_R.rp" "IKParentConstraintShoulder1_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintShoulder1_R.rpt" "IKParentConstraintShoulder1_R_parentConstraint1.crt"
		;
connectAttr "Clavicle_R.t" "IKParentConstraintShoulder1_R_parentConstraint1.tg[0].tt"
		;
connectAttr "Clavicle_R.rp" "IKParentConstraintShoulder1_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Clavicle_R.rpt" "IKParentConstraintShoulder1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Clavicle_R.r" "IKParentConstraintShoulder1_R_parentConstraint1.tg[0].tr"
		;
connectAttr "Clavicle_R.ro" "IKParentConstraintShoulder1_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Clavicle_R.s" "IKParentConstraintShoulder1_R_parentConstraint1.tg[0].ts"
		;
connectAttr "Clavicle_R.pm" "IKParentConstraintShoulder1_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Clavicle_R.jo" "IKParentConstraintShoulder1_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintShoulder1_R_parentConstraint1.w0" "IKParentConstraintShoulder1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.ctx" "IKParentConstraintShoulder_L.tx"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.cty" "IKParentConstraintShoulder_L.ty"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.ctz" "IKParentConstraintShoulder_L.tz"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.crx" "IKParentConstraintShoulder_L.rx"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.cry" "IKParentConstraintShoulder_L.ry"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.crz" "IKParentConstraintShoulder_L.rz"
		;
connectAttr "FKIKBlendArmCondition_L.ocr" "IKParentConstraintShoulder_L.v";
connectAttr "jointLayer.di" "IKXShoulder_L.do";
connectAttr "IKXShoulder_L.s" "IKXElbow_L.is";
connectAttr "jointLayer.di" "IKXElbow_L.do";
connectAttr "IKXElbow_L.s" "IKXWrist_L.is";
connectAttr "IKXWrist_L_orientConstraint1.crx" "IKXWrist_L.rx";
connectAttr "IKXWrist_L_orientConstraint1.cry" "IKXWrist_L.ry";
connectAttr "IKXWrist_L_orientConstraint1.crz" "IKXWrist_L.rz";
connectAttr "jointLayer.di" "IKXWrist_L.do";
connectAttr "IKXWrist_L.ro" "IKXWrist_L_orientConstraint1.cro";
connectAttr "IKXWrist_L.pim" "IKXWrist_L_orientConstraint1.cpim";
connectAttr "IKXWrist_L.jo" "IKXWrist_L_orientConstraint1.cjo";
connectAttr "IKArm_L.r" "IKXWrist_L_orientConstraint1.tg[0].tr";
connectAttr "IKArm_L.ro" "IKXWrist_L_orientConstraint1.tg[0].tro";
connectAttr "IKArm_L.pm" "IKXWrist_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXWrist_L_orientConstraint1.w0" "IKXWrist_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXWrist_L.tx" "effector2.tx";
connectAttr "IKXWrist_L.ty" "effector2.ty";
connectAttr "IKXWrist_L.tz" "effector2.tz";
connectAttr "Main.arrowVis" "PoleAnnotationArm_L.v";
connectAttr "PoleAnnotateTargetArm_LShape.wm" "PoleAnnotationArm_LShape.dom" -na
		;
connectAttr "IKParentConstraintShoulder_L.ro" "IKParentConstraintShoulder_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintShoulder_L.pim" "IKParentConstraintShoulder_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintShoulder_L.rp" "IKParentConstraintShoulder_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintShoulder_L.rpt" "IKParentConstraintShoulder_L_parentConstraint1.crt"
		;
connectAttr "Clavicle1_L.t" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Clavicle1_L.rp" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Clavicle1_L.rpt" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Clavicle1_L.r" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Clavicle1_L.ro" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Clavicle1_L.s" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Clavicle1_L.pm" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Clavicle1_L.jo" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.w0" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.ctx" "IKParentConstraintHip_R.tx"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.cty" "IKParentConstraintHip_R.ty"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.ctz" "IKParentConstraintHip_R.tz"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.crx" "IKParentConstraintHip_R.rx"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.cry" "IKParentConstraintHip_R.ry"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.crz" "IKParentConstraintHip_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintHip_R.v";
connectAttr "jointLayer.di" "IKXHip_R.do";
connectAttr "IKXHip_R.s" "IKXKnee_R.is";
connectAttr "IKXKnee_R_IKmessureDiv_R.oy" "IKXKnee_R.ty";
connectAttr "jointLayer.di" "IKXKnee_R.do";
connectAttr "IKXKnee_R.s" "IKXAnkle_R.is";
connectAttr "IKXAnkle_R_IKmessureDiv_R.oy" "IKXAnkle_R.ty";
connectAttr "IKXAnkle_R_orientConstraint1.crx" "IKXAnkle_R.rx";
connectAttr "IKXAnkle_R_orientConstraint1.cry" "IKXAnkle_R.ry";
connectAttr "IKXAnkle_R_orientConstraint1.crz" "IKXAnkle_R.rz";
connectAttr "jointLayer.di" "IKXAnkle_R.do";
connectAttr "IKXAnkle_R.s" "IKXMiddleToe1_R.is";
connectAttr "jointLayer.di" "IKXMiddleToe1_R.do";
connectAttr "IKXMiddleToe1_R.s" "IKXMiddleToe2_End_R.is";
connectAttr "jointLayer.di" "IKXMiddleToe2_End_R.do";
connectAttr "IKXMiddleToe2_End_R.tx" "effector4.tx";
connectAttr "IKXMiddleToe2_End_R.ty" "effector4.ty";
connectAttr "IKXMiddleToe2_End_R.tz" "effector4.tz";
connectAttr "IKXAnkle_R.ro" "IKXAnkle_R_orientConstraint1.cro";
connectAttr "IKXAnkle_R.pim" "IKXAnkle_R_orientConstraint1.cpim";
connectAttr "IKXAnkle_R.jo" "IKXAnkle_R_orientConstraint1.cjo";
connectAttr "IKLeg_R.r" "IKXAnkle_R_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "IKXAnkle_R_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "IKXAnkle_R_orientConstraint1.tg[0].tpm";
connectAttr "IKXAnkle_R_orientConstraint1.w0" "IKXAnkle_R_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXMiddleToe1_R.tx" "effector5.tx";
connectAttr "IKXMiddleToe1_R.ty" "effector5.ty";
connectAttr "IKXMiddleToe1_R.tz" "effector5.tz";
connectAttr "IKXAnkle_R.tx" "effector3.tx";
connectAttr "IKXAnkle_R.ty" "effector3.ty";
connectAttr "IKXAnkle_R.tz" "effector3.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_R.v";
connectAttr "PoleAnnotateTargetLeg_RShape.wm" "PoleAnnotationLeg_RShape.dom" -na
		;
connectAttr "IKParentConstraintHip_R.ro" "IKParentConstraintHip_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintHip_R.pim" "IKParentConstraintHip_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintHip_R.rp" "IKParentConstraintHip_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintHip_R.rpt" "IKParentConstraintHip_R_parentConstraint1.crt"
		;
connectAttr "HipTwist_R.t" "IKParentConstraintHip_R_parentConstraint1.tg[0].tt";
connectAttr "HipTwist_R.rp" "IKParentConstraintHip_R_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_R.rpt" "IKParentConstraintHip_R_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_R.r" "IKParentConstraintHip_R_parentConstraint1.tg[0].tr";
connectAttr "HipTwist_R.ro" "IKParentConstraintHip_R_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_R.s" "IKParentConstraintHip_R_parentConstraint1.tg[0].ts";
connectAttr "HipTwist_R.pm" "IKParentConstraintHip_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_R.jo" "IKParentConstraintHip_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.w0" "IKParentConstraintHip_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintPelvis_M.v";
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.ctx" "IKParentConstraintPelvis_M.tx"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.cty" "IKParentConstraintPelvis_M.ty"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.ctz" "IKParentConstraintPelvis_M.tz"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.crx" "IKParentConstraintPelvis_M.rx"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.cry" "IKParentConstraintPelvis_M.ry"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.crz" "IKParentConstraintPelvis_M.rz"
		;
connectAttr "jointLayer.di" "IKXPelvis_M.do";
connectAttr "IKXPelvis_M.s" "IKXSpineA_M.is";
connectAttr "stretchySpineBlendTwo1_M.o" "IKXSpineA_M.ty";
connectAttr "IKXSpineA_M_aimConstraint1.crx" "IKXSpineA_M.rx";
connectAttr "IKXSpineA_M_aimConstraint1.cry" "IKXSpineA_M.ry";
connectAttr "IKXSpineA_M_aimConstraint1.crz" "IKXSpineA_M.rz";
connectAttr "jointLayer.di" "IKXSpineA_M.do";
connectAttr "IKXSpineA_M.s" "IKXChest_M.is";
connectAttr "IKXChest_M_parentConstraint1.crx" "IKXChest_M.rx";
connectAttr "IKXChest_M_parentConstraint1.cry" "IKXChest_M.ry";
connectAttr "IKXChest_M_parentConstraint1.crz" "IKXChest_M.rz";
connectAttr "IKXChest_M_parentConstraint1.cty" "IKXChest_M.ty";
connectAttr "IKXChest_M_parentConstraint1.ctx" "IKXChest_M.tx";
connectAttr "IKXChest_M_parentConstraint1.ctz" "IKXChest_M.tz";
connectAttr "jointLayer.di" "IKXChest_M.do";
connectAttr "IKXChest_M.ro" "IKXChest_M_parentConstraint1.cro";
connectAttr "IKXChest_M.pim" "IKXChest_M_parentConstraint1.cpim";
connectAttr "IKXChest_M.rp" "IKXChest_M_parentConstraint1.crp";
connectAttr "IKXChest_M.rpt" "IKXChest_M_parentConstraint1.crt";
connectAttr "IKXChest_M.jo" "IKXChest_M_parentConstraint1.cjo";
connectAttr "IKfake2Spine_M.t" "IKXChest_M_parentConstraint1.tg[0].tt";
connectAttr "IKfake2Spine_M.rp" "IKXChest_M_parentConstraint1.tg[0].trp";
connectAttr "IKfake2Spine_M.rpt" "IKXChest_M_parentConstraint1.tg[0].trt";
connectAttr "IKfake2Spine_M.r" "IKXChest_M_parentConstraint1.tg[0].tr";
connectAttr "IKfake2Spine_M.ro" "IKXChest_M_parentConstraint1.tg[0].tro";
connectAttr "IKfake2Spine_M.s" "IKXChest_M_parentConstraint1.tg[0].ts";
connectAttr "IKfake2Spine_M.pm" "IKXChest_M_parentConstraint1.tg[0].tpm";
connectAttr "IKfake2Spine_M.jo" "IKXChest_M_parentConstraint1.tg[0].tjo";
connectAttr "IKXChest_M_parentConstraint1.w0" "IKXChest_M_parentConstraint1.tg[0].tw"
		;
connectAttr "IKSpine4AlignTo_M.t" "IKXChest_M_parentConstraint1.tg[1].tt";
connectAttr "IKSpine4AlignTo_M.rp" "IKXChest_M_parentConstraint1.tg[1].trp";
connectAttr "IKSpine4AlignTo_M.rpt" "IKXChest_M_parentConstraint1.tg[1].trt";
connectAttr "IKSpine4AlignTo_M.r" "IKXChest_M_parentConstraint1.tg[1].tr";
connectAttr "IKSpine4AlignTo_M.ro" "IKXChest_M_parentConstraint1.tg[1].tro";
connectAttr "IKSpine4AlignTo_M.s" "IKXChest_M_parentConstraint1.tg[1].ts";
connectAttr "IKSpine4AlignTo_M.pm" "IKXChest_M_parentConstraint1.tg[1].tpm";
connectAttr "IKXChest_M_parentConstraint1.w1" "IKXChest_M_parentConstraint1.tg[1].tw"
		;
connectAttr "stretchySpineReverse_M.oy" "IKXChest_M_parentConstraint1.w0";
connectAttr "stretchySpineUnitConversion_M.o" "IKXChest_M_parentConstraint1.w1";
connectAttr "IKXSpineA_M.pim" "IKXSpineA_M_aimConstraint1.cpim";
connectAttr "IKXSpineA_M.t" "IKXSpineA_M_aimConstraint1.ct";
connectAttr "IKXSpineA_M.rp" "IKXSpineA_M_aimConstraint1.crp";
connectAttr "IKXSpineA_M.rpt" "IKXSpineA_M_aimConstraint1.crt";
connectAttr "IKXSpineA_M.ro" "IKXSpineA_M_aimConstraint1.cro";
connectAttr "IKXSpineA_M.jo" "IKXSpineA_M_aimConstraint1.cjo";
connectAttr "IKfake2Spine_M.t" "IKXSpineA_M_aimConstraint1.tg[0].tt";
connectAttr "IKfake2Spine_M.rp" "IKXSpineA_M_aimConstraint1.tg[0].trp";
connectAttr "IKfake2Spine_M.rpt" "IKXSpineA_M_aimConstraint1.tg[0].trt";
connectAttr "IKfake2Spine_M.pm" "IKXSpineA_M_aimConstraint1.tg[0].tpm";
connectAttr "IKXSpineA_M_aimConstraint1.w0" "IKXSpineA_M_aimConstraint1.tg[0].tw"
		;
connectAttr "IKSpine4_M.t" "IKXSpineA_M_aimConstraint1.tg[1].tt";
connectAttr "IKSpine4_M.rp" "IKXSpineA_M_aimConstraint1.tg[1].trp";
connectAttr "IKSpine4_M.rpt" "IKXSpineA_M_aimConstraint1.tg[1].trt";
connectAttr "IKSpine4_M.pm" "IKXSpineA_M_aimConstraint1.tg[1].tpm";
connectAttr "IKXSpineA_M_aimConstraint1.w1" "IKXSpineA_M_aimConstraint1.tg[1].tw"
		;
connectAttr "stretchySpineReverse_M.oy" "IKXSpineA_M_aimConstraint1.w0";
connectAttr "stretchySpineUnitConversion_M.o" "IKXSpineA_M_aimConstraint1.w1";
connectAttr "IKFake1UpLocSpine_M.wm" "IKXSpineA_M_aimConstraint1.wum";
connectAttr "stretchySpineBlendTwo3_M.o" "IKfake1Spine_M.ty";
connectAttr "jointLayer.di" "IKfake1Spine_M.do";
connectAttr "IKfake1Spine_M.s" "IKfake2Spine_M.is";
connectAttr "stretchySpineBlendTwo2_M.o" "IKfake2Spine_M.ty";
connectAttr "jointLayer.di" "IKfake2Spine_M.do";
connectAttr "IKfake2Spine_M.tx" "effector6.tx";
connectAttr "IKfake2Spine_M.ty" "effector6.ty";
connectAttr "IKfake2Spine_M.tz" "effector6.tz";
connectAttr "IKParentConstraintPelvis_M.ro" "IKParentConstraintPelvis_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintPelvis_M.pim" "IKParentConstraintPelvis_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintPelvis_M.rp" "IKParentConstraintPelvis_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintPelvis_M.rpt" "IKParentConstraintPelvis_M_parentConstraint1.crt"
		;
connectAttr "IKSpine0AlignTo_M.t" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tt"
		;
connectAttr "IKSpine0AlignTo_M.rp" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].trp"
		;
connectAttr "IKSpine0AlignTo_M.rpt" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].trt"
		;
connectAttr "IKSpine0AlignTo_M.r" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tr"
		;
connectAttr "IKSpine0AlignTo_M.ro" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tro"
		;
connectAttr "IKSpine0AlignTo_M.s" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].ts"
		;
connectAttr "IKSpine0AlignTo_M.pm" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.w0" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.ctx" "IKParentConstraintHip_L.tx"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.cty" "IKParentConstraintHip_L.ty"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.ctz" "IKParentConstraintHip_L.tz"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.crx" "IKParentConstraintHip_L.rx"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.cry" "IKParentConstraintHip_L.ry"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.crz" "IKParentConstraintHip_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintHip_L.v";
connectAttr "jointLayer.di" "IKXHip_L.do";
connectAttr "IKXHip_L.s" "IKXKnee_L.is";
connectAttr "IKXKnee_L_IKmessureDiv_L.oy" "IKXKnee_L.ty";
connectAttr "jointLayer.di" "IKXKnee_L.do";
connectAttr "IKXKnee_L.s" "IKXAnkle_L.is";
connectAttr "IKXAnkle_L_IKmessureDiv_L.oy" "IKXAnkle_L.ty";
connectAttr "IKXAnkle_L_orientConstraint1.crx" "IKXAnkle_L.rx";
connectAttr "IKXAnkle_L_orientConstraint1.cry" "IKXAnkle_L.ry";
connectAttr "IKXAnkle_L_orientConstraint1.crz" "IKXAnkle_L.rz";
connectAttr "jointLayer.di" "IKXAnkle_L.do";
connectAttr "IKXAnkle_L.s" "IKXMiddleToe1_L.is";
connectAttr "jointLayer.di" "IKXMiddleToe1_L.do";
connectAttr "IKXMiddleToe1_L.s" "IKXMiddleToe2_End_L.is";
connectAttr "jointLayer.di" "IKXMiddleToe2_End_L.do";
connectAttr "IKXMiddleToe2_End_L.tx" "effector9.tx";
connectAttr "IKXMiddleToe2_End_L.ty" "effector9.ty";
connectAttr "IKXMiddleToe2_End_L.tz" "effector9.tz";
connectAttr "IKXAnkle_L.ro" "IKXAnkle_L_orientConstraint1.cro";
connectAttr "IKXAnkle_L.pim" "IKXAnkle_L_orientConstraint1.cpim";
connectAttr "IKXAnkle_L.jo" "IKXAnkle_L_orientConstraint1.cjo";
connectAttr "IKLeg_L.r" "IKXAnkle_L_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_L.ro" "IKXAnkle_L_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_L.pm" "IKXAnkle_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXAnkle_L_orientConstraint1.w0" "IKXAnkle_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXMiddleToe1_L.tx" "effector10.tx";
connectAttr "IKXMiddleToe1_L.ty" "effector10.ty";
connectAttr "IKXMiddleToe1_L.tz" "effector10.tz";
connectAttr "IKXAnkle_L.tx" "effector8.tx";
connectAttr "IKXAnkle_L.ty" "effector8.ty";
connectAttr "IKXAnkle_L.tz" "effector8.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_L.v";
connectAttr "PoleAnnotateTargetLeg_LShape.wm" "PoleAnnotationLeg_LShape.dom" -na
		;
connectAttr "IKParentConstraintHip_L.ro" "IKParentConstraintHip_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintHip_L.pim" "IKParentConstraintHip_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintHip_L.rp" "IKParentConstraintHip_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintHip_L.rpt" "IKParentConstraintHip_L_parentConstraint1.crt"
		;
connectAttr "HipTwist_L.t" "IKParentConstraintHip_L_parentConstraint1.tg[0].tt";
connectAttr "HipTwist_L.rp" "IKParentConstraintHip_L_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_L.rpt" "IKParentConstraintHip_L_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_L.r" "IKParentConstraintHip_L_parentConstraint1.tg[0].tr";
connectAttr "HipTwist_L.ro" "IKParentConstraintHip_L_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_L.s" "IKParentConstraintHip_L_parentConstraint1.tg[0].ts";
connectAttr "HipTwist_L.pm" "IKParentConstraintHip_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_L.jo" "IKParentConstraintHip_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.w0" "IKParentConstraintHip_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintArm1_R_parentConstraint1.ctx" "IKParentConstraintArm1_R.tx"
		;
connectAttr "IKParentConstraintArm1_R_parentConstraint1.cty" "IKParentConstraintArm1_R.ty"
		;
connectAttr "IKParentConstraintArm1_R_parentConstraint1.ctz" "IKParentConstraintArm1_R.tz"
		;
connectAttr "IKParentConstraintArm1_R_parentConstraint1.crx" "IKParentConstraintArm1_R.rx"
		;
connectAttr "IKParentConstraintArm1_R_parentConstraint1.cry" "IKParentConstraintArm1_R.ry"
		;
connectAttr "IKParentConstraintArm1_R_parentConstraint1.crz" "IKParentConstraintArm1_R.rz"
		;
connectAttr "FKIKBlendArm1Condition_R.ocr" "IKParentConstraintArm1_R.v";
connectAttr "IKXShoulder1_R.msg" "IKXArm1Handle_R.hsj";
connectAttr "effector1.hp" "IKXArm1Handle_R.hee";
connectAttr "ikRPsolver.msg" "IKXArm1Handle_R.hsv";
connectAttr "IKXArm1Handle_R_poleVectorConstraint1.ctx" "IKXArm1Handle_R.pvx";
connectAttr "IKXArm1Handle_R_poleVectorConstraint1.cty" "IKXArm1Handle_R.pvy";
connectAttr "IKXArm1Handle_R_poleVectorConstraint1.ctz" "IKXArm1Handle_R.pvz";
connectAttr "IKXArm1Handle_R.pim" "IKXArm1Handle_R_poleVectorConstraint1.cpim";
connectAttr "IKXShoulder1_R.pm" "IKXArm1Handle_R_poleVectorConstraint1.ps";
connectAttr "IKXShoulder1_R.t" "IKXArm1Handle_R_poleVectorConstraint1.crp";
connectAttr "PoleArm1_R.t" "IKXArm1Handle_R_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleArm1_R.rp" "IKXArm1Handle_R_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleArm1_R.rpt" "IKXArm1Handle_R_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleArm1_R.pm" "IKXArm1Handle_R_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXArm1Handle_R_poleVectorConstraint1.w0" "IKXArm1Handle_R_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintArm1_R.ro" "IKParentConstraintArm1_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintArm1_R.pim" "IKParentConstraintArm1_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintArm1_R.rp" "IKParentConstraintArm1_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintArm1_R.rpt" "IKParentConstraintArm1_R_parentConstraint1.crt"
		;
connectAttr "IKParentConstraintArm1_RStatic.t" "IKParentConstraintArm1_R_parentConstraint1.tg[0].tt"
		;
connectAttr "IKParentConstraintArm1_RStatic.rp" "IKParentConstraintArm1_R_parentConstraint1.tg[0].trp"
		;
connectAttr "IKParentConstraintArm1_RStatic.rpt" "IKParentConstraintArm1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "IKParentConstraintArm1_RStatic.r" "IKParentConstraintArm1_R_parentConstraint1.tg[0].tr"
		;
connectAttr "IKParentConstraintArm1_RStatic.ro" "IKParentConstraintArm1_R_parentConstraint1.tg[0].tro"
		;
connectAttr "IKParentConstraintArm1_RStatic.s" "IKParentConstraintArm1_R_parentConstraint1.tg[0].ts"
		;
connectAttr "IKParentConstraintArm1_RStatic.pm" "IKParentConstraintArm1_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintArm1_R_parentConstraint1.w0" "IKParentConstraintArm1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Clavicle_R.t" "IKParentConstraintArm1_R_parentConstraint1.tg[1].tt"
		;
connectAttr "Clavicle_R.rp" "IKParentConstraintArm1_R_parentConstraint1.tg[1].trp"
		;
connectAttr "Clavicle_R.rpt" "IKParentConstraintArm1_R_parentConstraint1.tg[1].trt"
		;
connectAttr "Clavicle_R.r" "IKParentConstraintArm1_R_parentConstraint1.tg[1].tr"
		;
connectAttr "Clavicle_R.ro" "IKParentConstraintArm1_R_parentConstraint1.tg[1].tro"
		;
connectAttr "Clavicle_R.s" "IKParentConstraintArm1_R_parentConstraint1.tg[1].ts"
		;
connectAttr "Clavicle_R.pm" "IKParentConstraintArm1_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "Clavicle_R.jo" "IKParentConstraintArm1_R_parentConstraint1.tg[1].tjo"
		;
connectAttr "IKParentConstraintArm1_R_parentConstraint1.w1" "IKParentConstraintArm1_R_parentConstraint1.tg[1].tw"
		;
connectAttr "IKArm1_RSetRangeFollow.oy" "IKParentConstraintArm1_R_parentConstraint1.w0"
		;
connectAttr "IKArm1_RSetRangeFollow.ox" "IKParentConstraintArm1_R_parentConstraint1.w1"
		;
connectAttr "PoleParentConstraintArm1_R_parentConstraint1.ctx" "PoleParentConstraintArm1_R.tx"
		;
connectAttr "PoleParentConstraintArm1_R_parentConstraint1.cty" "PoleParentConstraintArm1_R.ty"
		;
connectAttr "PoleParentConstraintArm1_R_parentConstraint1.ctz" "PoleParentConstraintArm1_R.tz"
		;
connectAttr "PoleParentConstraintArm1_R_parentConstraint1.crx" "PoleParentConstraintArm1_R.rx"
		;
connectAttr "PoleParentConstraintArm1_R_parentConstraint1.cry" "PoleParentConstraintArm1_R.ry"
		;
connectAttr "PoleParentConstraintArm1_R_parentConstraint1.crz" "PoleParentConstraintArm1_R.rz"
		;
connectAttr "FKIKBlendArm1Condition_R.ocr" "PoleParentConstraintArm1_R.v";
connectAttr "PoleParentConstraintArm1_R.ro" "PoleParentConstraintArm1_R_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintArm1_R.pim" "PoleParentConstraintArm1_R_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintArm1_R.rp" "PoleParentConstraintArm1_R_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintArm1_R.rpt" "PoleParentConstraintArm1_R_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintArm1_RStatic.t" "PoleParentConstraintArm1_R_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintArm1_RStatic.rp" "PoleParentConstraintArm1_R_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintArm1_RStatic.rpt" "PoleParentConstraintArm1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintArm1_RStatic.r" "PoleParentConstraintArm1_R_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintArm1_RStatic.ro" "PoleParentConstraintArm1_R_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintArm1_RStatic.s" "PoleParentConstraintArm1_R_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintArm1_RStatic.pm" "PoleParentConstraintArm1_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintArm1_R_parentConstraint1.w0" "PoleParentConstraintArm1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKArm1_R.t" "PoleParentConstraintArm1_R_parentConstraint1.tg[1].tt"
		;
connectAttr "IKArm1_R.rp" "PoleParentConstraintArm1_R_parentConstraint1.tg[1].trp"
		;
connectAttr "IKArm1_R.rpt" "PoleParentConstraintArm1_R_parentConstraint1.tg[1].trt"
		;
connectAttr "IKArm1_R.r" "PoleParentConstraintArm1_R_parentConstraint1.tg[1].tr"
		;
connectAttr "IKArm1_R.ro" "PoleParentConstraintArm1_R_parentConstraint1.tg[1].tro"
		;
connectAttr "IKArm1_R.s" "PoleParentConstraintArm1_R_parentConstraint1.tg[1].ts"
		;
connectAttr "IKArm1_R.pm" "PoleParentConstraintArm1_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintArm1_R_parentConstraint1.w1" "PoleParentConstraintArm1_R_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleArm1_RSetRangeFollow.oy" "PoleParentConstraintArm1_R_parentConstraint1.w0"
		;
connectAttr "PoleArm1_RSetRangeFollow.ox" "PoleParentConstraintArm1_R_parentConstraint1.w1"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.ctx" "IKParentConstraintArm_L.tx"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.cty" "IKParentConstraintArm_L.ty"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.ctz" "IKParentConstraintArm_L.tz"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.crx" "IKParentConstraintArm_L.rx"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.cry" "IKParentConstraintArm_L.ry"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.crz" "IKParentConstraintArm_L.rz"
		;
connectAttr "FKIKBlendArmCondition_L.ocr" "IKParentConstraintArm_L.v";
connectAttr "IKXShoulder_L.msg" "IKXArmHandle_L.hsj";
connectAttr "effector2.hp" "IKXArmHandle_L.hee";
connectAttr "ikRPsolver.msg" "IKXArmHandle_L.hsv";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.ctx" "IKXArmHandle_L.pvx";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.cty" "IKXArmHandle_L.pvy";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.ctz" "IKXArmHandle_L.pvz";
connectAttr "IKXArmHandle_L.pim" "IKXArmHandle_L_poleVectorConstraint1.cpim";
connectAttr "IKXShoulder_L.pm" "IKXArmHandle_L_poleVectorConstraint1.ps";
connectAttr "IKXShoulder_L.t" "IKXArmHandle_L_poleVectorConstraint1.crp";
connectAttr "PoleArm_L.t" "IKXArmHandle_L_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleArm_L.rp" "IKXArmHandle_L_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleArm_L.rpt" "IKXArmHandle_L_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleArm_L.pm" "IKXArmHandle_L_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.w0" "IKXArmHandle_L_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintArm_L.ro" "IKParentConstraintArm_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintArm_L.pim" "IKParentConstraintArm_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintArm_L.rp" "IKParentConstraintArm_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintArm_L.rpt" "IKParentConstraintArm_L_parentConstraint1.crt"
		;
connectAttr "IKParentConstraintArm_LStatic.t" "IKParentConstraintArm_L_parentConstraint1.tg[0].tt"
		;
connectAttr "IKParentConstraintArm_LStatic.rp" "IKParentConstraintArm_L_parentConstraint1.tg[0].trp"
		;
connectAttr "IKParentConstraintArm_LStatic.rpt" "IKParentConstraintArm_L_parentConstraint1.tg[0].trt"
		;
connectAttr "IKParentConstraintArm_LStatic.r" "IKParentConstraintArm_L_parentConstraint1.tg[0].tr"
		;
connectAttr "IKParentConstraintArm_LStatic.ro" "IKParentConstraintArm_L_parentConstraint1.tg[0].tro"
		;
connectAttr "IKParentConstraintArm_LStatic.s" "IKParentConstraintArm_L_parentConstraint1.tg[0].ts"
		;
connectAttr "IKParentConstraintArm_LStatic.pm" "IKParentConstraintArm_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.w0" "IKParentConstraintArm_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Clavicle1_L.t" "IKParentConstraintArm_L_parentConstraint1.tg[1].tt"
		;
connectAttr "Clavicle1_L.rp" "IKParentConstraintArm_L_parentConstraint1.tg[1].trp"
		;
connectAttr "Clavicle1_L.rpt" "IKParentConstraintArm_L_parentConstraint1.tg[1].trt"
		;
connectAttr "Clavicle1_L.r" "IKParentConstraintArm_L_parentConstraint1.tg[1].tr"
		;
connectAttr "Clavicle1_L.ro" "IKParentConstraintArm_L_parentConstraint1.tg[1].tro"
		;
connectAttr "Clavicle1_L.s" "IKParentConstraintArm_L_parentConstraint1.tg[1].ts"
		;
connectAttr "Clavicle1_L.pm" "IKParentConstraintArm_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "Clavicle1_L.jo" "IKParentConstraintArm_L_parentConstraint1.tg[1].tjo"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.w1" "IKParentConstraintArm_L_parentConstraint1.tg[1].tw"
		;
connectAttr "IKArm_LSetRangeFollow.oy" "IKParentConstraintArm_L_parentConstraint1.w0"
		;
connectAttr "IKArm_LSetRangeFollow.ox" "IKParentConstraintArm_L_parentConstraint1.w1"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.ctx" "PoleParentConstraintArm_L.tx"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.cty" "PoleParentConstraintArm_L.ty"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.ctz" "PoleParentConstraintArm_L.tz"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.crx" "PoleParentConstraintArm_L.rx"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.cry" "PoleParentConstraintArm_L.ry"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.crz" "PoleParentConstraintArm_L.rz"
		;
connectAttr "FKIKBlendArmCondition_L.ocr" "PoleParentConstraintArm_L.v";
connectAttr "PoleParentConstraintArm_L.ro" "PoleParentConstraintArm_L_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintArm_L.pim" "PoleParentConstraintArm_L_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintArm_L.rp" "PoleParentConstraintArm_L_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintArm_L.rpt" "PoleParentConstraintArm_L_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintArm_LStatic.t" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintArm_LStatic.rp" "PoleParentConstraintArm_L_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintArm_LStatic.rpt" "PoleParentConstraintArm_L_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintArm_LStatic.r" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintArm_LStatic.ro" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintArm_LStatic.s" "PoleParentConstraintArm_L_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintArm_LStatic.pm" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.w0" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKArm_L.t" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tt";
connectAttr "IKArm_L.rp" "PoleParentConstraintArm_L_parentConstraint1.tg[1].trp"
		;
connectAttr "IKArm_L.rpt" "PoleParentConstraintArm_L_parentConstraint1.tg[1].trt"
		;
connectAttr "IKArm_L.r" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tr";
connectAttr "IKArm_L.ro" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tro"
		;
connectAttr "IKArm_L.s" "PoleParentConstraintArm_L_parentConstraint1.tg[1].ts";
connectAttr "IKArm_L.pm" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.w1" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleArm_LSetRangeFollow.oy" "PoleParentConstraintArm_L_parentConstraint1.w0"
		;
connectAttr "PoleArm_LSetRangeFollow.ox" "PoleParentConstraintArm_L_parentConstraint1.w1"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintLeg_R.v";
connectAttr "unitConversion2.o" "IKRollLegHeel_R.rx";
connectAttr "unitConversion4.o" "IKRollLegToe_R.rx";
connectAttr "IKLiftToeLegUnitConversion_R.o" "IKLiftToeLegToe_R.rx";
connectAttr "IKXMiddleToe1_R.msg" "IKXLegHandleToe_R.hsj";
connectAttr "effector4.hp" "IKXLegHandleToe_R.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleToe_R.hsv";
connectAttr "unitConversion3.o" "IKRollLegBall_R.rx";
connectAttr "IKXHip_R.msg" "IKXLegHandle_R.hsj";
connectAttr "effector3.hp" "IKXLegHandle_R.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_R.hsv";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctx" "IKXLegHandle_R.pvx";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.cty" "IKXLegHandle_R.pvy";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctz" "IKXLegHandle_R.pvz";
connectAttr "IKXLegHandle_R.pim" "IKXLegHandle_R_poleVectorConstraint1.cpim";
connectAttr "IKXHip_R.pm" "IKXLegHandle_R_poleVectorConstraint1.ps";
connectAttr "IKXHip_R.t" "IKXLegHandle_R_poleVectorConstraint1.crp";
connectAttr "PoleLeg_R.t" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_R.rp" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_R.rpt" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_R.pm" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.w0" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXAnkle_R.msg" "IKXLegHandleBall_R.hsj";
connectAttr "effector5.hp" "IKXLegHandleBall_R.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_R.hsv";
connectAttr "PoleAimLeg_R_aimConstraint1.crx" "PoleAimLeg_R.rx";
connectAttr "PoleAimLeg_R_aimConstraint1.cry" "PoleAimLeg_R.ry";
connectAttr "PoleAimLeg_R_aimConstraint1.crz" "PoleAimLeg_R.rz";
connectAttr "PoleAimLeg_R_pointConstraint1.ctx" "PoleAimLeg_R.tx";
connectAttr "PoleAimLeg_R_pointConstraint1.cty" "PoleAimLeg_R.ty";
connectAttr "PoleAimLeg_R_pointConstraint1.ctz" "PoleAimLeg_R.tz";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_aimConstraint1.cpim";
connectAttr "PoleAimLeg_R.t" "PoleAimLeg_R_aimConstraint1.ct";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_aimConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.crt";
connectAttr "PoleAimLeg_R.ro" "PoleAimLeg_R_aimConstraint1.cro";
connectAttr "IKLeg_R.t" "PoleAimLeg_R_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PoleAimLeg_R_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PoleAimLeg_R_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_aimConstraint1.w0" "PoleAimLeg_R_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_R.wm" "PoleAimLeg_R_aimConstraint1.wum";
connectAttr "unitConversion1.o" "PoleAimLeg_R_aimConstraint1.ox";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_pointConstraint1.cpim";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_pointConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_pointConstraint1.crt";
connectAttr "IKXHip_R.t" "PoleAimLeg_R_pointConstraint1.tg[0].tt";
connectAttr "IKXHip_R.rp" "PoleAimLeg_R_pointConstraint1.tg[0].trp";
connectAttr "IKXHip_R.rpt" "PoleAimLeg_R_pointConstraint1.tg[0].trt";
connectAttr "IKXHip_R.pm" "PoleAimLeg_R_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_pointConstraint1.w0" "PoleAimLeg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctx" "PoleParentConstraintLeg_R.tx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cty" "PoleParentConstraintLeg_R.ty"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctz" "PoleParentConstraintLeg_R.tz"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crx" "PoleParentConstraintLeg_R.rx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cry" "PoleParentConstraintLeg_R.ry"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crz" "PoleParentConstraintLeg_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "PoleParentConstraintLeg_R.v";
connectAttr "PoleParentConstraintLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_R.pim" "PoleParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_RStatic.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_RStatic.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_RStatic.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w0" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_R.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_R.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_R.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_R.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w1" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_RSetRangeFollow.oy" "PoleParentConstraintLeg_R_parentConstraint1.w0"
		;
connectAttr "PoleLeg_RSetRangeFollow.ox" "PoleParentConstraintLeg_R_parentConstraint1.w1"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintSpine0_M.v";
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.ctx" "IKParentConstraintSpine0_M.tx"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.cty" "IKParentConstraintSpine0_M.ty"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.ctz" "IKParentConstraintSpine0_M.tz"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.crx" "IKParentConstraintSpine0_M.rx"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.cry" "IKParentConstraintSpine0_M.ry"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.crz" "IKParentConstraintSpine0_M.rz"
		;
connectAttr "IKStiffSpine0_M.oy" "IKXSpineLocator1_M.ty";
connectAttr "IKParentConstraintSpine0_M.ro" "IKParentConstraintSpine0_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintSpine0_M.pim" "IKParentConstraintSpine0_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintSpine0_M.rp" "IKParentConstraintSpine0_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintSpine0_M.rpt" "IKParentConstraintSpine0_M_parentConstraint1.crt"
		;
connectAttr "Center_M.t" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Center_M.rp" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Center_M.rpt" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Center_M.r" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Center_M.s" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Center_M.pm" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.w0" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintSpine2_M.v";
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.ctx" "IKParentConstraintSpine2_M.tx"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.cty" "IKParentConstraintSpine2_M.ty"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.ctz" "IKParentConstraintSpine2_M.tz"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.crx" "IKParentConstraintSpine2_M.rx"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.cry" "IKParentConstraintSpine2_M.ry"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.crz" "IKParentConstraintSpine2_M.rz"
		;
connectAttr "IKParentConstraintSpine2_M.ro" "IKParentConstraintSpine2_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintSpine2_M.pim" "IKParentConstraintSpine2_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintSpine2_M.rp" "IKParentConstraintSpine2_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintSpine2_M.rpt" "IKParentConstraintSpine2_M_parentConstraint1.crt"
		;
connectAttr "Center_M.t" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Center_M.rp" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Center_M.rpt" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Center_M.r" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Center_M.s" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Center_M.pm" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.w0" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintSpine4_M.v";
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.ctx" "IKParentConstraintSpine4_M.tx"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.cty" "IKParentConstraintSpine4_M.ty"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.ctz" "IKParentConstraintSpine4_M.tz"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.crx" "IKParentConstraintSpine4_M.rx"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.cry" "IKParentConstraintSpine4_M.ry"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.crz" "IKParentConstraintSpine4_M.rz"
		;
connectAttr "IKXPelvis_M.msg" "IKXSpineHandle_M.hsj";
connectAttr "effector6.hp" "IKXSpineHandle_M.hee";
connectAttr "ikSplineSolver.msg" "IKXSpineHandle_M.hsv";
connectAttr "IKXSpineCurve_MShape.ws" "IKXSpineHandle_M.ic";
connectAttr "IKTwistSpineUnitConversion_M.o" "IKXSpineHandle_M.twi";
connectAttr "IKStiffSpine4_M.oy" "IKXSpineLocator3_M.ty";
connectAttr "IKParentConstraintSpine4_M.ro" "IKParentConstraintSpine4_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintSpine4_M.pim" "IKParentConstraintSpine4_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintSpine4_M.rp" "IKParentConstraintSpine4_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintSpine4_M.rpt" "IKParentConstraintSpine4_M_parentConstraint1.crt"
		;
connectAttr "Center_M.t" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Center_M.rp" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Center_M.rpt" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Center_M.r" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Center_M.s" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Center_M.pm" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.w0" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintLeg_L.v";
connectAttr "unitConversion8.o" "IKRollLegHeel_L.rx";
connectAttr "unitConversion10.o" "IKRollLegToe_L.rx";
connectAttr "IKLiftToeLegUnitConversion_L.o" "IKLiftToeLegToe_L.rx";
connectAttr "IKXMiddleToe1_L.msg" "IKXLegHandleToe_L.hsj";
connectAttr "effector9.hp" "IKXLegHandleToe_L.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleToe_L.hsv";
connectAttr "unitConversion9.o" "IKRollLegBall_L.rx";
connectAttr "IKXHip_L.msg" "IKXLegHandle_L.hsj";
connectAttr "effector8.hp" "IKXLegHandle_L.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_L.hsv";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctx" "IKXLegHandle_L.pvx";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.cty" "IKXLegHandle_L.pvy";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctz" "IKXLegHandle_L.pvz";
connectAttr "IKXLegHandle_L.pim" "IKXLegHandle_L_poleVectorConstraint1.cpim";
connectAttr "IKXHip_L.pm" "IKXLegHandle_L_poleVectorConstraint1.ps";
connectAttr "IKXHip_L.t" "IKXLegHandle_L_poleVectorConstraint1.crp";
connectAttr "PoleLeg_L.t" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_L.rp" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_L.rpt" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_L.pm" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.w0" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXAnkle_L.msg" "IKXLegHandleBall_L.hsj";
connectAttr "effector10.hp" "IKXLegHandleBall_L.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_L.hsv";
connectAttr "PoleAimLeg_L_aimConstraint1.crx" "PoleAimLeg_L.rx";
connectAttr "PoleAimLeg_L_aimConstraint1.cry" "PoleAimLeg_L.ry";
connectAttr "PoleAimLeg_L_aimConstraint1.crz" "PoleAimLeg_L.rz";
connectAttr "PoleAimLeg_L_pointConstraint1.ctx" "PoleAimLeg_L.tx";
connectAttr "PoleAimLeg_L_pointConstraint1.cty" "PoleAimLeg_L.ty";
connectAttr "PoleAimLeg_L_pointConstraint1.ctz" "PoleAimLeg_L.tz";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_aimConstraint1.cpim";
connectAttr "PoleAimLeg_L.t" "PoleAimLeg_L_aimConstraint1.ct";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_aimConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.crt";
connectAttr "PoleAimLeg_L.ro" "PoleAimLeg_L_aimConstraint1.cro";
connectAttr "IKLeg_L.t" "PoleAimLeg_L_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_L.rp" "PoleAimLeg_L_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_L.pm" "PoleAimLeg_L_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_aimConstraint1.w0" "PoleAimLeg_L_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.wm" "PoleAimLeg_L_aimConstraint1.wum";
connectAttr "unitConversion7.o" "PoleAimLeg_L_aimConstraint1.ox";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_pointConstraint1.cpim";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_pointConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_pointConstraint1.crt";
connectAttr "IKXHip_L.t" "PoleAimLeg_L_pointConstraint1.tg[0].tt";
connectAttr "IKXHip_L.rp" "PoleAimLeg_L_pointConstraint1.tg[0].trp";
connectAttr "IKXHip_L.rpt" "PoleAimLeg_L_pointConstraint1.tg[0].trt";
connectAttr "IKXHip_L.pm" "PoleAimLeg_L_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_pointConstraint1.w0" "PoleAimLeg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctx" "PoleParentConstraintLeg_L.tx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cty" "PoleParentConstraintLeg_L.ty"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctz" "PoleParentConstraintLeg_L.tz"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crx" "PoleParentConstraintLeg_L.rx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cry" "PoleParentConstraintLeg_L.ry"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crz" "PoleParentConstraintLeg_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "PoleParentConstraintLeg_L.v";
connectAttr "PoleParentConstraintLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_L.pim" "PoleParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_LStatic.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_LStatic.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_LStatic.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w0" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_L.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_L.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_L.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_L.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w1" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_LSetRangeFollow.oy" "PoleParentConstraintLeg_L_parentConstraint1.w0"
		;
connectAttr "PoleLeg_LSetRangeFollow.ox" "PoleParentConstraintLeg_L_parentConstraint1.w1"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKXSpineCurve_M.v";
connectAttr "IKXSpineLocator0_MShape.wp" "IKXSpineCurve_MShape.cp[0]";
connectAttr "IKXSpineLocator1_MShape.wp" "IKXSpineCurve_MShape.cp[1]";
connectAttr "IKXSpineLocator2_MShape.wp" "IKXSpineCurve_MShape.cp[2]";
connectAttr "IKXSpineLocator3_MShape.wp" "IKXSpineCurve_MShape.cp[3]";
connectAttr "IKXSpineLocator4_MShape.wp" "IKXSpineCurve_MShape.cp[4]";
connectAttr "IKmessureLoc1Leg_R_pointConstraint1.ctx" "IKmessureLoc1Leg_R.tx";
connectAttr "IKmessureLoc1Leg_R_pointConstraint1.cty" "IKmessureLoc1Leg_R.ty";
connectAttr "IKmessureLoc1Leg_R_pointConstraint1.ctz" "IKmessureLoc1Leg_R.tz";
connectAttr "IKmessureLoc1Leg_R.pim" "IKmessureLoc1Leg_R_pointConstraint1.cpim";
connectAttr "IKmessureLoc1Leg_R.rp" "IKmessureLoc1Leg_R_pointConstraint1.crp";
connectAttr "IKmessureLoc1Leg_R.rpt" "IKmessureLoc1Leg_R_pointConstraint1.crt";
connectAttr "IKXHip_R.t" "IKmessureLoc1Leg_R_pointConstraint1.tg[0].tt";
connectAttr "IKXHip_R.rp" "IKmessureLoc1Leg_R_pointConstraint1.tg[0].trp";
connectAttr "IKXHip_R.rpt" "IKmessureLoc1Leg_R_pointConstraint1.tg[0].trt";
connectAttr "IKXHip_R.pm" "IKmessureLoc1Leg_R_pointConstraint1.tg[0].tpm";
connectAttr "IKmessureLoc1Leg_R_pointConstraint1.w0" "IKmessureLoc1Leg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "IKmessureLoc2Leg_R_pointConstraint1.ctx" "IKmessureLoc2Leg_R.tx";
connectAttr "IKmessureLoc2Leg_R_pointConstraint1.cty" "IKmessureLoc2Leg_R.ty";
connectAttr "IKmessureLoc2Leg_R_pointConstraint1.ctz" "IKmessureLoc2Leg_R.tz";
connectAttr "IKmessureLoc2Leg_R.pim" "IKmessureLoc2Leg_R_pointConstraint1.cpim";
connectAttr "IKmessureLoc2Leg_R.rp" "IKmessureLoc2Leg_R_pointConstraint1.crp";
connectAttr "IKmessureLoc2Leg_R.rpt" "IKmessureLoc2Leg_R_pointConstraint1.crt";
connectAttr "IKmessureConstrainToLeg_R.t" "IKmessureLoc2Leg_R_pointConstraint1.tg[0].tt"
		;
connectAttr "IKmessureConstrainToLeg_R.rp" "IKmessureLoc2Leg_R_pointConstraint1.tg[0].trp"
		;
connectAttr "IKmessureConstrainToLeg_R.rpt" "IKmessureLoc2Leg_R_pointConstraint1.tg[0].trt"
		;
connectAttr "IKmessureConstrainToLeg_R.pm" "IKmessureLoc2Leg_R_pointConstraint1.tg[0].tpm"
		;
connectAttr "IKmessureLoc2Leg_R_pointConstraint1.w0" "IKmessureLoc2Leg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "IKmessureLoc2Leg_R.t" "IKdistanceLeg_RShape.ep";
connectAttr "IKdistanceLeg_RShape_antiPop.o" "IKdistanceLeg_RShape.antiPop";
connectAttr "IKmessureLoc1Leg_L_pointConstraint1.ctx" "IKmessureLoc1Leg_L.tx";
connectAttr "IKmessureLoc1Leg_L_pointConstraint1.cty" "IKmessureLoc1Leg_L.ty";
connectAttr "IKmessureLoc1Leg_L_pointConstraint1.ctz" "IKmessureLoc1Leg_L.tz";
connectAttr "IKmessureLoc1Leg_L.pim" "IKmessureLoc1Leg_L_pointConstraint1.cpim";
connectAttr "IKmessureLoc1Leg_L.rp" "IKmessureLoc1Leg_L_pointConstraint1.crp";
connectAttr "IKmessureLoc1Leg_L.rpt" "IKmessureLoc1Leg_L_pointConstraint1.crt";
connectAttr "IKXHip_L.t" "IKmessureLoc1Leg_L_pointConstraint1.tg[0].tt";
connectAttr "IKXHip_L.rp" "IKmessureLoc1Leg_L_pointConstraint1.tg[0].trp";
connectAttr "IKXHip_L.rpt" "IKmessureLoc1Leg_L_pointConstraint1.tg[0].trt";
connectAttr "IKXHip_L.pm" "IKmessureLoc1Leg_L_pointConstraint1.tg[0].tpm";
connectAttr "IKmessureLoc1Leg_L_pointConstraint1.w0" "IKmessureLoc1Leg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "IKmessureLoc2Leg_L_pointConstraint1.ctx" "IKmessureLoc2Leg_L.tx";
connectAttr "IKmessureLoc2Leg_L_pointConstraint1.cty" "IKmessureLoc2Leg_L.ty";
connectAttr "IKmessureLoc2Leg_L_pointConstraint1.ctz" "IKmessureLoc2Leg_L.tz";
connectAttr "IKmessureLoc2Leg_L.pim" "IKmessureLoc2Leg_L_pointConstraint1.cpim";
connectAttr "IKmessureLoc2Leg_L.rp" "IKmessureLoc2Leg_L_pointConstraint1.crp";
connectAttr "IKmessureLoc2Leg_L.rpt" "IKmessureLoc2Leg_L_pointConstraint1.crt";
connectAttr "IKmessureConstrainToLeg_L.t" "IKmessureLoc2Leg_L_pointConstraint1.tg[0].tt"
		;
connectAttr "IKmessureConstrainToLeg_L.rp" "IKmessureLoc2Leg_L_pointConstraint1.tg[0].trp"
		;
connectAttr "IKmessureConstrainToLeg_L.rpt" "IKmessureLoc2Leg_L_pointConstraint1.tg[0].trt"
		;
connectAttr "IKmessureConstrainToLeg_L.pm" "IKmessureLoc2Leg_L_pointConstraint1.tg[0].tpm"
		;
connectAttr "IKmessureLoc2Leg_L_pointConstraint1.w0" "IKmessureLoc2Leg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "IKmessureLoc2Leg_L.t" "IKdistanceLeg_LShape.ep";
connectAttr "IKdistanceLeg_LShape_antiPop.o" "IKdistanceLeg_LShape.antiPop";
connectAttr "Main.fkIkVis" "FKIKSystem.v";
connectAttr "FKIKParentConstraintArm1_R_parentConstraint1.ctx" "FKIKParentConstraintArm1_R.tx"
		;
connectAttr "FKIKParentConstraintArm1_R_parentConstraint1.cty" "FKIKParentConstraintArm1_R.ty"
		;
connectAttr "FKIKParentConstraintArm1_R_parentConstraint1.ctz" "FKIKParentConstraintArm1_R.tz"
		;
connectAttr "FKIKParentConstraintArm1_R_parentConstraint1.crx" "FKIKParentConstraintArm1_R.rx"
		;
connectAttr "FKIKParentConstraintArm1_R_parentConstraint1.cry" "FKIKParentConstraintArm1_R.ry"
		;
connectAttr "FKIKParentConstraintArm1_R_parentConstraint1.crz" "FKIKParentConstraintArm1_R.rz"
		;
connectAttr "FKIKParentConstraintArm1_R.ro" "FKIKParentConstraintArm1_R_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintArm1_R.pim" "FKIKParentConstraintArm1_R_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintArm1_R.rp" "FKIKParentConstraintArm1_R_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintArm1_R.rpt" "FKIKParentConstraintArm1_R_parentConstraint1.crt"
		;
connectAttr "Clavicle_R.t" "FKIKParentConstraintArm1_R_parentConstraint1.tg[0].tt"
		;
connectAttr "Clavicle_R.rp" "FKIKParentConstraintArm1_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Clavicle_R.rpt" "FKIKParentConstraintArm1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Clavicle_R.r" "FKIKParentConstraintArm1_R_parentConstraint1.tg[0].tr"
		;
connectAttr "Clavicle_R.ro" "FKIKParentConstraintArm1_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Clavicle_R.s" "FKIKParentConstraintArm1_R_parentConstraint1.tg[0].ts"
		;
connectAttr "Clavicle_R.pm" "FKIKParentConstraintArm1_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Clavicle_R.jo" "FKIKParentConstraintArm1_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintArm1_R_parentConstraint1.w0" "FKIKParentConstraintArm1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.ctx" "FKIKParentConstraintArm_L.tx"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.cty" "FKIKParentConstraintArm_L.ty"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.ctz" "FKIKParentConstraintArm_L.tz"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.crx" "FKIKParentConstraintArm_L.rx"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.cry" "FKIKParentConstraintArm_L.ry"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.crz" "FKIKParentConstraintArm_L.rz"
		;
connectAttr "FKIKParentConstraintArm_L.ro" "FKIKParentConstraintArm_L_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintArm_L.pim" "FKIKParentConstraintArm_L_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintArm_L.rp" "FKIKParentConstraintArm_L_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintArm_L.rpt" "FKIKParentConstraintArm_L_parentConstraint1.crt"
		;
connectAttr "Clavicle1_L.t" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Clavicle1_L.rp" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Clavicle1_L.rpt" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Clavicle1_L.r" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Clavicle1_L.ro" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Clavicle1_L.s" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Clavicle1_L.pm" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Clavicle1_L.jo" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.w0" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctx" "FKIKParentConstraintLeg_R.tx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cty" "FKIKParentConstraintLeg_R.ty"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctz" "FKIKParentConstraintLeg_R.tz"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crx" "FKIKParentConstraintLeg_R.rx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cry" "FKIKParentConstraintLeg_R.ry"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crz" "FKIKParentConstraintLeg_R.rz"
		;
connectAttr "FKIKParentConstraintLeg_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_R.pim" "FKIKParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "HipTwist_R.t" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "HipTwist_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_R.r" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "HipTwist_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_R.s" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "HipTwist_R.pm" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_R.jo" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.w0" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.ctx" "FKIKParentConstraintSpine_M.tx"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.cty" "FKIKParentConstraintSpine_M.ty"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.ctz" "FKIKParentConstraintSpine_M.tz"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.crx" "FKIKParentConstraintSpine_M.rx"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.cry" "FKIKParentConstraintSpine_M.ry"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.crz" "FKIKParentConstraintSpine_M.rz"
		;
connectAttr "FKIKParentConstraintSpine_M.ro" "FKIKParentConstraintSpine_M_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintSpine_M.pim" "FKIKParentConstraintSpine_M_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintSpine_M.rp" "FKIKParentConstraintSpine_M_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintSpine_M.rpt" "FKIKParentConstraintSpine_M_parentConstraint1.crt"
		;
connectAttr "FKPelvis_M.t" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tt"
		;
connectAttr "FKPelvis_M.rp" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].trp"
		;
connectAttr "FKPelvis_M.rpt" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].trt"
		;
connectAttr "FKPelvis_M.r" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tr"
		;
connectAttr "FKPelvis_M.ro" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tro"
		;
connectAttr "FKPelvis_M.s" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].ts"
		;
connectAttr "FKPelvis_M.pm" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.w0" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctx" "FKIKParentConstraintLeg_L.tx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cty" "FKIKParentConstraintLeg_L.ty"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctz" "FKIKParentConstraintLeg_L.tz"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crx" "FKIKParentConstraintLeg_L.rx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cry" "FKIKParentConstraintLeg_L.ry"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crz" "FKIKParentConstraintLeg_L.rz"
		;
connectAttr "FKIKParentConstraintLeg_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_L.pim" "FKIKParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "HipTwist_L.t" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "HipTwist_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_L.r" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "HipTwist_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_L.s" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "HipTwist_L.pm" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_L.jo" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.w0" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.cry" "PelvisCenterBtwLegs_M.ry"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctx" "PelvisCenterBtwLegs_M.tx"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctz" "PelvisCenterBtwLegs_M.tz"
		;
connectAttr "PelvisCenterBtwLegs_M.ro" "PelvisCenterBtwLegs_M_orientConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_orientConstraint1.cpim"
		;
connectAttr "IKLeg_R.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w0" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tr";
connectAttr "IKLeg_L.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tro";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w1" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tw"
		;
connectAttr "CenterBtwFeet_M.ox" "PelvisCenterBtwLegs_M_pointConstraint1.nds";
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_pointConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegs_M.rp" "PelvisCenterBtwLegs_M_pointConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegs_M.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.crt"
		;
connectAttr "IKLeg_R.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w0" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tt";
connectAttr "IKLeg_L.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trp";
connectAttr "IKLeg_L.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trt";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w1" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tw"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.ctx" "TwistFollowPelvis_M.tx"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.cty" "TwistFollowPelvis_M.ty"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.ctz" "TwistFollowPelvis_M.tz"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.crx" "TwistFollowPelvis_M.rx"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.cry" "TwistFollowPelvis_M.ry"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.crz" "TwistFollowPelvis_M.rz"
		;
connectAttr "TwistFollowPelvis_M.ro" "TwistFollowPelvis_M_parentConstraint1.cro"
		;
connectAttr "TwistFollowPelvis_M.pim" "TwistFollowPelvis_M_parentConstraint1.cpim"
		;
connectAttr "TwistFollowPelvis_M.rp" "TwistFollowPelvis_M_parentConstraint1.crp"
		;
connectAttr "TwistFollowPelvis_M.rpt" "TwistFollowPelvis_M_parentConstraint1.crt"
		;
connectAttr "IKSpine0_M.t" "TwistFollowPelvis_M_parentConstraint1.tg[0].tt";
connectAttr "IKSpine0_M.rp" "TwistFollowPelvis_M_parentConstraint1.tg[0].trp";
connectAttr "IKSpine0_M.rpt" "TwistFollowPelvis_M_parentConstraint1.tg[0].trt";
connectAttr "IKSpine0_M.r" "TwistFollowPelvis_M_parentConstraint1.tg[0].tr";
connectAttr "IKSpine0_M.ro" "TwistFollowPelvis_M_parentConstraint1.tg[0].tro";
connectAttr "IKSpine0_M.s" "TwistFollowPelvis_M_parentConstraint1.tg[0].ts";
connectAttr "IKSpine0_M.pm" "TwistFollowPelvis_M_parentConstraint1.tg[0].tpm";
connectAttr "TwistFollowPelvis_M_parentConstraint1.w0" "TwistFollowPelvis_M_parentConstraint1.tg[0].tw"
		;
connectAttr "jointLayer.di" "UnTwistPelvis_M.do";
connectAttr "UnTwistPelvis_M.s" "UnTwistEndPelvis_M.is";
connectAttr "UnTwistEndPelvis_M_orientConstraint1.crx" "UnTwistEndPelvis_M.rx";
connectAttr "UnTwistEndPelvis_M_orientConstraint1.cry" "UnTwistEndPelvis_M.ry";
connectAttr "UnTwistEndPelvis_M_orientConstraint1.crz" "UnTwistEndPelvis_M.rz";
connectAttr "jointLayer.di" "UnTwistEndPelvis_M.do";
connectAttr "UnTwistEndPelvis_M.ro" "UnTwistEndPelvis_M_orientConstraint1.cro";
connectAttr "UnTwistEndPelvis_M.pim" "UnTwistEndPelvis_M_orientConstraint1.cpim"
		;
connectAttr "UnTwistEndPelvis_M.jo" "UnTwistEndPelvis_M_orientConstraint1.cjo";
connectAttr "IKSpine4AlignUnTwistTo_M.r" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tr"
		;
connectAttr "IKSpine4AlignUnTwistTo_M.ro" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tro"
		;
connectAttr "IKSpine4AlignUnTwistTo_M.pm" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tpm"
		;
connectAttr "UnTwistEndPelvis_M_orientConstraint1.w0" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "UnTwistEndPelvis_M.tx" "effector7.tx";
connectAttr "UnTwistEndPelvis_M.ty" "effector7.ty";
connectAttr "UnTwistEndPelvis_M.tz" "effector7.tz";
connectAttr "UnTwistPelvis_M.msg" "UnTwistIKPelvis_M.hsj";
connectAttr "effector7.hp" "UnTwistIKPelvis_M.hee";
connectAttr "ikSCsolver.msg" "UnTwistIKPelvis_M.hsv";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.ctx" "UnTwistIKPelvis_M.tx";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.cty" "UnTwistIKPelvis_M.ty";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.ctz" "UnTwistIKPelvis_M.tz";
connectAttr "UnTwistIKPelvis_M.pim" "UnTwistIKPelvis_M_pointConstraint1.cpim";
connectAttr "UnTwistIKPelvis_M.rp" "UnTwistIKPelvis_M_pointConstraint1.crp";
connectAttr "UnTwistIKPelvis_M.rpt" "UnTwistIKPelvis_M_pointConstraint1.crt";
connectAttr "IKSpine4_M.t" "UnTwistIKPelvis_M_pointConstraint1.tg[0].tt";
connectAttr "IKSpine4_M.rp" "UnTwistIKPelvis_M_pointConstraint1.tg[0].trp";
connectAttr "IKSpine4_M.rpt" "UnTwistIKPelvis_M_pointConstraint1.tg[0].trt";
connectAttr "IKSpine4_M.pm" "UnTwistIKPelvis_M_pointConstraint1.tg[0].tpm";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.w0" "UnTwistIKPelvis_M_pointConstraint1.tg[0].tw"
		;
connectAttr "ScaleBlendPelvis_M.op" "Pelvis_M.s";
connectAttr "Pelvis_M_pointConstraint1.ctx" "Pelvis_M.tx";
connectAttr "Pelvis_M_pointConstraint1.cty" "Pelvis_M.ty";
connectAttr "Pelvis_M_pointConstraint1.ctz" "Pelvis_M.tz";
connectAttr "Pelvis_M_orientConstraint1.crx" "Pelvis_M.rx";
connectAttr "Pelvis_M_orientConstraint1.cry" "Pelvis_M.ry";
connectAttr "Pelvis_M_orientConstraint1.crz" "Pelvis_M.rz";
connectAttr "jointLayer.di" "Pelvis_M.do";
connectAttr "ScaleBlendSpineA_M.op" "SpineA_M.s";
connectAttr "Pelvis_M.s" "SpineA_M.is";
connectAttr "SpineA_M_pointConstraint1.ctx" "SpineA_M.tx";
connectAttr "SpineA_M_pointConstraint1.cty" "SpineA_M.ty";
connectAttr "SpineA_M_pointConstraint1.ctz" "SpineA_M.tz";
connectAttr "SpineA_M_orientConstraint1.crx" "SpineA_M.rx";
connectAttr "SpineA_M_orientConstraint1.cry" "SpineA_M.ry";
connectAttr "SpineA_M_orientConstraint1.crz" "SpineA_M.rz";
connectAttr "jointLayer.di" "SpineA_M.do";
connectAttr "ScaleBlendChest_M.op" "Chest_M.s";
connectAttr "SpineA_M.s" "Chest_M.is";
connectAttr "Chest_M_pointConstraint1.ctx" "Chest_M.tx";
connectAttr "Chest_M_pointConstraint1.cty" "Chest_M.ty";
connectAttr "Chest_M_pointConstraint1.ctz" "Chest_M.tz";
connectAttr "Chest_M_orientConstraint1.crx" "Chest_M.rx";
connectAttr "Chest_M_orientConstraint1.cry" "Chest_M.ry";
connectAttr "Chest_M_orientConstraint1.crz" "Chest_M.rz";
connectAttr "jointLayer.di" "Chest_M.do";
connectAttr "FKClavicle_R.s" "Clavicle_R.s";
connectAttr "Clavicle_R_parentConstraint1.ctx" "Clavicle_R.tx";
connectAttr "Clavicle_R_parentConstraint1.cty" "Clavicle_R.ty";
connectAttr "Clavicle_R_parentConstraint1.ctz" "Clavicle_R.tz";
connectAttr "Clavicle_R_parentConstraint1.crx" "Clavicle_R.rx";
connectAttr "Clavicle_R_parentConstraint1.cry" "Clavicle_R.ry";
connectAttr "Clavicle_R_parentConstraint1.crz" "Clavicle_R.rz";
connectAttr "Chest_M.s" "Clavicle_R.is";
connectAttr "jointLayer.di" "Clavicle_R.do";
connectAttr "ScaleBlendShoulder1_R.op" "Shoulder1_R.s";
connectAttr "Clavicle_R.s" "Shoulder1_R.is";
connectAttr "Shoulder1_R_parentConstraint1.ctx" "Shoulder1_R.tx";
connectAttr "Shoulder1_R_parentConstraint1.cty" "Shoulder1_R.ty";
connectAttr "Shoulder1_R_parentConstraint1.ctz" "Shoulder1_R.tz";
connectAttr "Shoulder1_R_parentConstraint1.crx" "Shoulder1_R.rx";
connectAttr "Shoulder1_R_parentConstraint1.cry" "Shoulder1_R.ry";
connectAttr "Shoulder1_R_parentConstraint1.crz" "Shoulder1_R.rz";
connectAttr "jointLayer.di" "Shoulder1_R.do";
connectAttr "ScaleBlendElbow1_R.op" "Elbow1_R.s";
connectAttr "Shoulder1_R.s" "Elbow1_R.is";
connectAttr "Elbow1_R_parentConstraint1.ctx" "Elbow1_R.tx";
connectAttr "Elbow1_R_parentConstraint1.cty" "Elbow1_R.ty";
connectAttr "Elbow1_R_parentConstraint1.ctz" "Elbow1_R.tz";
connectAttr "Elbow1_R_parentConstraint1.crx" "Elbow1_R.rx";
connectAttr "Elbow1_R_parentConstraint1.cry" "Elbow1_R.ry";
connectAttr "Elbow1_R_parentConstraint1.crz" "Elbow1_R.rz";
connectAttr "jointLayer.di" "Elbow1_R.do";
connectAttr "ScaleBlendWrist2_R.op" "Wrist2_R.s";
connectAttr "Elbow1_R.s" "Wrist2_R.is";
connectAttr "Wrist2_R_parentConstraint1.ctx" "Wrist2_R.tx";
connectAttr "Wrist2_R_parentConstraint1.cty" "Wrist2_R.ty";
connectAttr "Wrist2_R_parentConstraint1.ctz" "Wrist2_R.tz";
connectAttr "Wrist2_R_parentConstraint1.crx" "Wrist2_R.rx";
connectAttr "Wrist2_R_parentConstraint1.cry" "Wrist2_R.ry";
connectAttr "Wrist2_R_parentConstraint1.crz" "Wrist2_R.rz";
connectAttr "jointLayer.di" "Wrist2_R.do";
connectAttr "ScaleBlendWrist1_R.op" "Wrist1_R.s";
connectAttr "Wrist1_R_parentConstraint1.ctx" "Wrist1_R.tx";
connectAttr "Wrist1_R_parentConstraint1.cty" "Wrist1_R.ty";
connectAttr "Wrist1_R_parentConstraint1.ctz" "Wrist1_R.tz";
connectAttr "Wrist1_R_parentConstraint1.crx" "Wrist1_R.rx";
connectAttr "Wrist1_R_parentConstraint1.cry" "Wrist1_R.ry";
connectAttr "Wrist1_R_parentConstraint1.crz" "Wrist1_R.rz";
connectAttr "jointLayer.di" "Wrist1_R.do";
connectAttr "FKBarrel1_R.s" "Barrel1_R.s";
connectAttr "Wrist1_R.s" "Barrel1_R.is";
connectAttr "Barrel1_R_parentConstraint1.ctx" "Barrel1_R.tx";
connectAttr "Barrel1_R_parentConstraint1.cty" "Barrel1_R.ty";
connectAttr "Barrel1_R_parentConstraint1.ctz" "Barrel1_R.tz";
connectAttr "Barrel1_R_parentConstraint1.crx" "Barrel1_R.rx";
connectAttr "Barrel1_R_parentConstraint1.cry" "Barrel1_R.ry";
connectAttr "Barrel1_R_parentConstraint1.crz" "Barrel1_R.rz";
connectAttr "jointLayer.di" "Barrel1_R.do";
connectAttr "Barrel1_R.s" "Barrel1_End_R.is";
connectAttr "jointLayer.di" "Barrel1_End_R.do";
connectAttr "Barrel1_R.ro" "Barrel1_R_parentConstraint1.cro";
connectAttr "Barrel1_R.pim" "Barrel1_R_parentConstraint1.cpim";
connectAttr "Barrel1_R.rp" "Barrel1_R_parentConstraint1.crp";
connectAttr "Barrel1_R.rpt" "Barrel1_R_parentConstraint1.crt";
connectAttr "Barrel1_R.jo" "Barrel1_R_parentConstraint1.cjo";
connectAttr "FKXBarrel1_R.t" "Barrel1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXBarrel1_R.rp" "Barrel1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXBarrel1_R.rpt" "Barrel1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXBarrel1_R.r" "Barrel1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXBarrel1_R.ro" "Barrel1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXBarrel1_R.s" "Barrel1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXBarrel1_R.pm" "Barrel1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXBarrel1_R.jo" "Barrel1_R_parentConstraint1.tg[0].tjo";
connectAttr "Barrel1_R_parentConstraint1.w0" "Barrel1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKBarrel3_R.s" "Barrel3_R.s";
connectAttr "Wrist1_R.s" "Barrel3_R.is";
connectAttr "Barrel3_R_parentConstraint1.ctx" "Barrel3_R.tx";
connectAttr "Barrel3_R_parentConstraint1.cty" "Barrel3_R.ty";
connectAttr "Barrel3_R_parentConstraint1.ctz" "Barrel3_R.tz";
connectAttr "Barrel3_R_parentConstraint1.crx" "Barrel3_R.rx";
connectAttr "Barrel3_R_parentConstraint1.cry" "Barrel3_R.ry";
connectAttr "Barrel3_R_parentConstraint1.crz" "Barrel3_R.rz";
connectAttr "jointLayer.di" "Barrel3_R.do";
connectAttr "Barrel3_R.s" "Barrel3_End_R.is";
connectAttr "jointLayer.di" "Barrel3_End_R.do";
connectAttr "Barrel3_R.ro" "Barrel3_R_parentConstraint1.cro";
connectAttr "Barrel3_R.pim" "Barrel3_R_parentConstraint1.cpim";
connectAttr "Barrel3_R.rp" "Barrel3_R_parentConstraint1.crp";
connectAttr "Barrel3_R.rpt" "Barrel3_R_parentConstraint1.crt";
connectAttr "Barrel3_R.jo" "Barrel3_R_parentConstraint1.cjo";
connectAttr "FKXBarrel3_R.t" "Barrel3_R_parentConstraint1.tg[0].tt";
connectAttr "FKXBarrel3_R.rp" "Barrel3_R_parentConstraint1.tg[0].trp";
connectAttr "FKXBarrel3_R.rpt" "Barrel3_R_parentConstraint1.tg[0].trt";
connectAttr "FKXBarrel3_R.r" "Barrel3_R_parentConstraint1.tg[0].tr";
connectAttr "FKXBarrel3_R.ro" "Barrel3_R_parentConstraint1.tg[0].tro";
connectAttr "FKXBarrel3_R.s" "Barrel3_R_parentConstraint1.tg[0].ts";
connectAttr "FKXBarrel3_R.pm" "Barrel3_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXBarrel3_R.jo" "Barrel3_R_parentConstraint1.tg[0].tjo";
connectAttr "Barrel3_R_parentConstraint1.w0" "Barrel3_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKBarrel2_R.s" "Barrel2_R.s";
connectAttr "Wrist1_R.s" "Barrel2_R.is";
connectAttr "Barrel2_R_parentConstraint1.ctx" "Barrel2_R.tx";
connectAttr "Barrel2_R_parentConstraint1.cty" "Barrel2_R.ty";
connectAttr "Barrel2_R_parentConstraint1.ctz" "Barrel2_R.tz";
connectAttr "Barrel2_R_parentConstraint1.crx" "Barrel2_R.rx";
connectAttr "Barrel2_R_parentConstraint1.cry" "Barrel2_R.ry";
connectAttr "Barrel2_R_parentConstraint1.crz" "Barrel2_R.rz";
connectAttr "jointLayer.di" "Barrel2_R.do";
connectAttr "Barrel2_R.s" "Barrel2_End_R.is";
connectAttr "jointLayer.di" "Barrel2_End_R.do";
connectAttr "Barrel2_R.ro" "Barrel2_R_parentConstraint1.cro";
connectAttr "Barrel2_R.pim" "Barrel2_R_parentConstraint1.cpim";
connectAttr "Barrel2_R.rp" "Barrel2_R_parentConstraint1.crp";
connectAttr "Barrel2_R.rpt" "Barrel2_R_parentConstraint1.crt";
connectAttr "Barrel2_R.jo" "Barrel2_R_parentConstraint1.cjo";
connectAttr "FKXBarrel2_R.t" "Barrel2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXBarrel2_R.rp" "Barrel2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXBarrel2_R.rpt" "Barrel2_R_parentConstraint1.tg[0].trt";
connectAttr "FKXBarrel2_R.r" "Barrel2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXBarrel2_R.ro" "Barrel2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXBarrel2_R.s" "Barrel2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXBarrel2_R.pm" "Barrel2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXBarrel2_R.jo" "Barrel2_R_parentConstraint1.tg[0].tjo";
connectAttr "Barrel2_R_parentConstraint1.w0" "Barrel2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Wrist1_R.ro" "Wrist1_R_parentConstraint1.cro";
connectAttr "Wrist1_R.pim" "Wrist1_R_parentConstraint1.cpim";
connectAttr "Wrist1_R.rp" "Wrist1_R_parentConstraint1.crp";
connectAttr "Wrist1_R.rpt" "Wrist1_R_parentConstraint1.crt";
connectAttr "Wrist1_R.jo" "Wrist1_R_parentConstraint1.cjo";
connectAttr "FKXWrist1_R.t" "Wrist1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWrist1_R.rp" "Wrist1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWrist1_R.rpt" "Wrist1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWrist1_R.r" "Wrist1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWrist1_R.ro" "Wrist1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWrist1_R.s" "Wrist1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWrist1_R.pm" "Wrist1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWrist1_R.jo" "Wrist1_R_parentConstraint1.tg[0].tjo";
connectAttr "Wrist1_R_parentConstraint1.w0" "Wrist1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXWrist1_R.t" "Wrist1_R_parentConstraint1.tg[1].tt";
connectAttr "IKXWrist1_R.rp" "Wrist1_R_parentConstraint1.tg[1].trp";
connectAttr "IKXWrist1_R.rpt" "Wrist1_R_parentConstraint1.tg[1].trt";
connectAttr "IKXWrist1_R.r" "Wrist1_R_parentConstraint1.tg[1].tr";
connectAttr "IKXWrist1_R.ro" "Wrist1_R_parentConstraint1.tg[1].tro";
connectAttr "IKXWrist1_R.s" "Wrist1_R_parentConstraint1.tg[1].ts";
connectAttr "IKXWrist1_R.pm" "Wrist1_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXWrist1_R.jo" "Wrist1_R_parentConstraint1.tg[1].tjo";
connectAttr "Wrist1_R_parentConstraint1.w1" "Wrist1_R_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArm1Reverse_R.ox" "Wrist1_R_parentConstraint1.w0";
connectAttr "FKIKBlendArm1UnitConversion_R.o" "Wrist1_R_parentConstraint1.w1";
connectAttr "Wrist2_R.ro" "Wrist2_R_parentConstraint1.cro";
connectAttr "Wrist2_R.pim" "Wrist2_R_parentConstraint1.cpim";
connectAttr "Wrist2_R.rp" "Wrist2_R_parentConstraint1.crp";
connectAttr "Wrist2_R.rpt" "Wrist2_R_parentConstraint1.crt";
connectAttr "Wrist2_R.jo" "Wrist2_R_parentConstraint1.cjo";
connectAttr "FKXWrist2_R.t" "Wrist2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWrist2_R.rp" "Wrist2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWrist2_R.rpt" "Wrist2_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWrist2_R.r" "Wrist2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWrist2_R.ro" "Wrist2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWrist2_R.s" "Wrist2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWrist2_R.pm" "Wrist2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWrist2_R.jo" "Wrist2_R_parentConstraint1.tg[0].tjo";
connectAttr "Wrist2_R_parentConstraint1.w0" "Wrist2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXWrist2_R.t" "Wrist2_R_parentConstraint1.tg[1].tt";
connectAttr "IKXWrist2_R.rp" "Wrist2_R_parentConstraint1.tg[1].trp";
connectAttr "IKXWrist2_R.rpt" "Wrist2_R_parentConstraint1.tg[1].trt";
connectAttr "IKXWrist2_R.r" "Wrist2_R_parentConstraint1.tg[1].tr";
connectAttr "IKXWrist2_R.ro" "Wrist2_R_parentConstraint1.tg[1].tro";
connectAttr "IKXWrist2_R.s" "Wrist2_R_parentConstraint1.tg[1].ts";
connectAttr "IKXWrist2_R.pm" "Wrist2_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXWrist2_R.jo" "Wrist2_R_parentConstraint1.tg[1].tjo";
connectAttr "Wrist2_R_parentConstraint1.w1" "Wrist2_R_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArm1Reverse_R.ox" "Wrist2_R_parentConstraint1.w0";
connectAttr "FKIKBlendArm1UnitConversion_R.o" "Wrist2_R_parentConstraint1.w1";
connectAttr "Elbow1_R.ro" "Elbow1_R_parentConstraint1.cro";
connectAttr "Elbow1_R.pim" "Elbow1_R_parentConstraint1.cpim";
connectAttr "Elbow1_R.rp" "Elbow1_R_parentConstraint1.crp";
connectAttr "Elbow1_R.rpt" "Elbow1_R_parentConstraint1.crt";
connectAttr "Elbow1_R.jo" "Elbow1_R_parentConstraint1.cjo";
connectAttr "FKXElbow1_R.t" "Elbow1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow1_R.rp" "Elbow1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow1_R.rpt" "Elbow1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow1_R.r" "Elbow1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow1_R.ro" "Elbow1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow1_R.s" "Elbow1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow1_R.pm" "Elbow1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow1_R.jo" "Elbow1_R_parentConstraint1.tg[0].tjo";
connectAttr "Elbow1_R_parentConstraint1.w0" "Elbow1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXElbow1_R.t" "Elbow1_R_parentConstraint1.tg[1].tt";
connectAttr "IKXElbow1_R.rp" "Elbow1_R_parentConstraint1.tg[1].trp";
connectAttr "IKXElbow1_R.rpt" "Elbow1_R_parentConstraint1.tg[1].trt";
connectAttr "IKXElbow1_R.r" "Elbow1_R_parentConstraint1.tg[1].tr";
connectAttr "IKXElbow1_R.ro" "Elbow1_R_parentConstraint1.tg[1].tro";
connectAttr "IKXElbow1_R.s" "Elbow1_R_parentConstraint1.tg[1].ts";
connectAttr "IKXElbow1_R.pm" "Elbow1_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXElbow1_R.jo" "Elbow1_R_parentConstraint1.tg[1].tjo";
connectAttr "Elbow1_R_parentConstraint1.w1" "Elbow1_R_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArm1Reverse_R.ox" "Elbow1_R_parentConstraint1.w0";
connectAttr "FKIKBlendArm1UnitConversion_R.o" "Elbow1_R_parentConstraint1.w1";
connectAttr "Shoulder1_R.ro" "Shoulder1_R_parentConstraint1.cro";
connectAttr "Shoulder1_R.pim" "Shoulder1_R_parentConstraint1.cpim";
connectAttr "Shoulder1_R.rp" "Shoulder1_R_parentConstraint1.crp";
connectAttr "Shoulder1_R.rpt" "Shoulder1_R_parentConstraint1.crt";
connectAttr "Shoulder1_R.jo" "Shoulder1_R_parentConstraint1.cjo";
connectAttr "FKXShoulder1_R.t" "Shoulder1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder1_R.rp" "Shoulder1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder1_R.rpt" "Shoulder1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder1_R.r" "Shoulder1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder1_R.ro" "Shoulder1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder1_R.s" "Shoulder1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder1_R.pm" "Shoulder1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder1_R.jo" "Shoulder1_R_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder1_R_parentConstraint1.w0" "Shoulder1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXShoulder1_R.t" "Shoulder1_R_parentConstraint1.tg[1].tt";
connectAttr "IKXShoulder1_R.rp" "Shoulder1_R_parentConstraint1.tg[1].trp";
connectAttr "IKXShoulder1_R.rpt" "Shoulder1_R_parentConstraint1.tg[1].trt";
connectAttr "IKXShoulder1_R.r" "Shoulder1_R_parentConstraint1.tg[1].tr";
connectAttr "IKXShoulder1_R.ro" "Shoulder1_R_parentConstraint1.tg[1].tro";
connectAttr "IKXShoulder1_R.s" "Shoulder1_R_parentConstraint1.tg[1].ts";
connectAttr "IKXShoulder1_R.pm" "Shoulder1_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXShoulder1_R.jo" "Shoulder1_R_parentConstraint1.tg[1].tjo";
connectAttr "Shoulder1_R_parentConstraint1.w1" "Shoulder1_R_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArm1Reverse_R.ox" "Shoulder1_R_parentConstraint1.w0";
connectAttr "FKIKBlendArm1UnitConversion_R.o" "Shoulder1_R_parentConstraint1.w1"
		;
connectAttr "Clavicle_R.ro" "Clavicle_R_parentConstraint1.cro";
connectAttr "Clavicle_R.pim" "Clavicle_R_parentConstraint1.cpim";
connectAttr "Clavicle_R.rp" "Clavicle_R_parentConstraint1.crp";
connectAttr "Clavicle_R.rpt" "Clavicle_R_parentConstraint1.crt";
connectAttr "Clavicle_R.jo" "Clavicle_R_parentConstraint1.cjo";
connectAttr "FKXClavicle_R.t" "Clavicle_R_parentConstraint1.tg[0].tt";
connectAttr "FKXClavicle_R.rp" "Clavicle_R_parentConstraint1.tg[0].trp";
connectAttr "FKXClavicle_R.rpt" "Clavicle_R_parentConstraint1.tg[0].trt";
connectAttr "FKXClavicle_R.r" "Clavicle_R_parentConstraint1.tg[0].tr";
connectAttr "FKXClavicle_R.ro" "Clavicle_R_parentConstraint1.tg[0].tro";
connectAttr "FKXClavicle_R.s" "Clavicle_R_parentConstraint1.tg[0].ts";
connectAttr "FKXClavicle_R.pm" "Clavicle_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXClavicle_R.jo" "Clavicle_R_parentConstraint1.tg[0].tjo";
connectAttr "Clavicle_R_parentConstraint1.w0" "Clavicle_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKHead_M.s" "Head_M.s";
connectAttr "Chest_M.s" "Head_M.is";
connectAttr "Head_M_parentConstraint1.ctx" "Head_M.tx";
connectAttr "Head_M_parentConstraint1.cty" "Head_M.ty";
connectAttr "Head_M_parentConstraint1.ctz" "Head_M.tz";
connectAttr "Head_M_parentConstraint1.crx" "Head_M.rx";
connectAttr "Head_M_parentConstraint1.cry" "Head_M.ry";
connectAttr "Head_M_parentConstraint1.crz" "Head_M.rz";
connectAttr "jointLayer.di" "Head_M.do";
connectAttr "Head_M.s" "Head_End_M.is";
connectAttr "jointLayer.di" "Head_End_M.do";
connectAttr "Head_M.ro" "Head_M_parentConstraint1.cro";
connectAttr "Head_M.pim" "Head_M_parentConstraint1.cpim";
connectAttr "Head_M.rp" "Head_M_parentConstraint1.crp";
connectAttr "Head_M.rpt" "Head_M_parentConstraint1.crt";
connectAttr "Head_M.jo" "Head_M_parentConstraint1.cjo";
connectAttr "FKXHead_M.t" "Head_M_parentConstraint1.tg[0].tt";
connectAttr "FKXHead_M.rp" "Head_M_parentConstraint1.tg[0].trp";
connectAttr "FKXHead_M.rpt" "Head_M_parentConstraint1.tg[0].trt";
connectAttr "FKXHead_M.r" "Head_M_parentConstraint1.tg[0].tr";
connectAttr "FKXHead_M.ro" "Head_M_parentConstraint1.tg[0].tro";
connectAttr "FKXHead_M.s" "Head_M_parentConstraint1.tg[0].ts";
connectAttr "FKXHead_M.pm" "Head_M_parentConstraint1.tg[0].tpm";
connectAttr "FKXHead_M.jo" "Head_M_parentConstraint1.tg[0].tjo";
connectAttr "Head_M_parentConstraint1.w0" "Head_M_parentConstraint1.tg[0].tw";
connectAttr "FKClavicle1_L.s" "Clavicle1_L.s";
connectAttr "Clavicle1_L_parentConstraint1.ctx" "Clavicle1_L.tx";
connectAttr "Clavicle1_L_parentConstraint1.cty" "Clavicle1_L.ty";
connectAttr "Clavicle1_L_parentConstraint1.ctz" "Clavicle1_L.tz";
connectAttr "Clavicle1_L_parentConstraint1.crx" "Clavicle1_L.rx";
connectAttr "Clavicle1_L_parentConstraint1.cry" "Clavicle1_L.ry";
connectAttr "Clavicle1_L_parentConstraint1.crz" "Clavicle1_L.rz";
connectAttr "Chest_M.s" "Clavicle1_L.is";
connectAttr "jointLayer.di" "Clavicle1_L.do";
connectAttr "ScaleBlendShoulder_L.op" "Shoulder_L.s";
connectAttr "Clavicle1_L.s" "Shoulder_L.is";
connectAttr "Shoulder_L_parentConstraint1.ctx" "Shoulder_L.tx";
connectAttr "Shoulder_L_parentConstraint1.cty" "Shoulder_L.ty";
connectAttr "Shoulder_L_parentConstraint1.ctz" "Shoulder_L.tz";
connectAttr "Shoulder_L_parentConstraint1.crx" "Shoulder_L.rx";
connectAttr "Shoulder_L_parentConstraint1.cry" "Shoulder_L.ry";
connectAttr "Shoulder_L_parentConstraint1.crz" "Shoulder_L.rz";
connectAttr "jointLayer.di" "Shoulder_L.do";
connectAttr "ScaleBlendElbow_L.op" "Elbow_L.s";
connectAttr "Shoulder_L.s" "Elbow_L.is";
connectAttr "Elbow_L_parentConstraint1.ctx" "Elbow_L.tx";
connectAttr "Elbow_L_parentConstraint1.cty" "Elbow_L.ty";
connectAttr "Elbow_L_parentConstraint1.ctz" "Elbow_L.tz";
connectAttr "Elbow_L_parentConstraint1.crx" "Elbow_L.rx";
connectAttr "Elbow_L_parentConstraint1.cry" "Elbow_L.ry";
connectAttr "Elbow_L_parentConstraint1.crz" "Elbow_L.rz";
connectAttr "jointLayer.di" "Elbow_L.do";
connectAttr "ScaleBlendWrist_L.op" "Wrist_L.s";
connectAttr "Elbow_L.s" "Wrist_L.is";
connectAttr "Wrist_L_parentConstraint1.ctx" "Wrist_L.tx";
connectAttr "Wrist_L_parentConstraint1.cty" "Wrist_L.ty";
connectAttr "Wrist_L_parentConstraint1.ctz" "Wrist_L.tz";
connectAttr "Wrist_L_parentConstraint1.crx" "Wrist_L.rx";
connectAttr "Wrist_L_parentConstraint1.cry" "Wrist_L.ry";
connectAttr "Wrist_L_parentConstraint1.crz" "Wrist_L.rz";
connectAttr "jointLayer.di" "Wrist_L.do";
connectAttr "FKMiddleFinger1_L.s" "MiddleFinger1_L.s";
connectAttr "Wrist_L.s" "MiddleFinger1_L.is";
connectAttr "MiddleFinger1_L_parentConstraint1.ctx" "MiddleFinger1_L.tx";
connectAttr "MiddleFinger1_L_parentConstraint1.cty" "MiddleFinger1_L.ty";
connectAttr "MiddleFinger1_L_parentConstraint1.ctz" "MiddleFinger1_L.tz";
connectAttr "MiddleFinger1_L_parentConstraint1.crx" "MiddleFinger1_L.rx";
connectAttr "MiddleFinger1_L_parentConstraint1.cry" "MiddleFinger1_L.ry";
connectAttr "MiddleFinger1_L_parentConstraint1.crz" "MiddleFinger1_L.rz";
connectAttr "jointLayer.di" "MiddleFinger1_L.do";
connectAttr "FKMiddleFinger5_L.s" "MiddleFinger5_L.s";
connectAttr "MiddleFinger1_L.s" "MiddleFinger5_L.is";
connectAttr "MiddleFinger5_L_parentConstraint1.ctx" "MiddleFinger5_L.tx";
connectAttr "MiddleFinger5_L_parentConstraint1.cty" "MiddleFinger5_L.ty";
connectAttr "MiddleFinger5_L_parentConstraint1.ctz" "MiddleFinger5_L.tz";
connectAttr "MiddleFinger5_L_parentConstraint1.crx" "MiddleFinger5_L.rx";
connectAttr "MiddleFinger5_L_parentConstraint1.cry" "MiddleFinger5_L.ry";
connectAttr "MiddleFinger5_L_parentConstraint1.crz" "MiddleFinger5_L.rz";
connectAttr "jointLayer.di" "MiddleFinger5_L.do";
connectAttr "FKMiddleFinger6_L.s" "MiddleFinger6_L.s";
connectAttr "MiddleFinger5_L.s" "MiddleFinger6_L.is";
connectAttr "MiddleFinger6_L_parentConstraint1.ctx" "MiddleFinger6_L.tx";
connectAttr "MiddleFinger6_L_parentConstraint1.cty" "MiddleFinger6_L.ty";
connectAttr "MiddleFinger6_L_parentConstraint1.ctz" "MiddleFinger6_L.tz";
connectAttr "MiddleFinger6_L_parentConstraint1.crx" "MiddleFinger6_L.rx";
connectAttr "MiddleFinger6_L_parentConstraint1.cry" "MiddleFinger6_L.ry";
connectAttr "MiddleFinger6_L_parentConstraint1.crz" "MiddleFinger6_L.rz";
connectAttr "jointLayer.di" "MiddleFinger6_L.do";
connectAttr "MiddleFinger6_L.s" "MiddleFinger7_End_L.is";
connectAttr "jointLayer.di" "MiddleFinger7_End_L.do";
connectAttr "MiddleFinger6_L.ro" "MiddleFinger6_L_parentConstraint1.cro";
connectAttr "MiddleFinger6_L.pim" "MiddleFinger6_L_parentConstraint1.cpim";
connectAttr "MiddleFinger6_L.rp" "MiddleFinger6_L_parentConstraint1.crp";
connectAttr "MiddleFinger6_L.rpt" "MiddleFinger6_L_parentConstraint1.crt";
connectAttr "MiddleFinger6_L.jo" "MiddleFinger6_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger6_L.t" "MiddleFinger6_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger6_L.rp" "MiddleFinger6_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger6_L.rpt" "MiddleFinger6_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger6_L.r" "MiddleFinger6_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger6_L.ro" "MiddleFinger6_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger6_L.s" "MiddleFinger6_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger6_L.pm" "MiddleFinger6_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger6_L.jo" "MiddleFinger6_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger6_L_parentConstraint1.w0" "MiddleFinger6_L_parentConstraint1.tg[0].tw"
		;
connectAttr "MiddleFinger5_L.ro" "MiddleFinger5_L_parentConstraint1.cro";
connectAttr "MiddleFinger5_L.pim" "MiddleFinger5_L_parentConstraint1.cpim";
connectAttr "MiddleFinger5_L.rp" "MiddleFinger5_L_parentConstraint1.crp";
connectAttr "MiddleFinger5_L.rpt" "MiddleFinger5_L_parentConstraint1.crt";
connectAttr "MiddleFinger5_L.jo" "MiddleFinger5_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger5_L.t" "MiddleFinger5_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger5_L.rp" "MiddleFinger5_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger5_L.rpt" "MiddleFinger5_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger5_L.r" "MiddleFinger5_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger5_L.ro" "MiddleFinger5_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger5_L.s" "MiddleFinger5_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger5_L.pm" "MiddleFinger5_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger5_L.jo" "MiddleFinger5_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger5_L_parentConstraint1.w0" "MiddleFinger5_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKMiddleFinger2_L.s" "MiddleFinger2_L.s";
connectAttr "MiddleFinger1_L.s" "MiddleFinger2_L.is";
connectAttr "MiddleFinger2_L_parentConstraint1.ctx" "MiddleFinger2_L.tx";
connectAttr "MiddleFinger2_L_parentConstraint1.cty" "MiddleFinger2_L.ty";
connectAttr "MiddleFinger2_L_parentConstraint1.ctz" "MiddleFinger2_L.tz";
connectAttr "MiddleFinger2_L_parentConstraint1.crx" "MiddleFinger2_L.rx";
connectAttr "MiddleFinger2_L_parentConstraint1.cry" "MiddleFinger2_L.ry";
connectAttr "MiddleFinger2_L_parentConstraint1.crz" "MiddleFinger2_L.rz";
connectAttr "jointLayer.di" "MiddleFinger2_L.do";
connectAttr "FKMiddleFinger3_L.s" "MiddleFinger3_L.s";
connectAttr "MiddleFinger2_L.s" "MiddleFinger3_L.is";
connectAttr "MiddleFinger3_L_parentConstraint1.ctx" "MiddleFinger3_L.tx";
connectAttr "MiddleFinger3_L_parentConstraint1.cty" "MiddleFinger3_L.ty";
connectAttr "MiddleFinger3_L_parentConstraint1.ctz" "MiddleFinger3_L.tz";
connectAttr "MiddleFinger3_L_parentConstraint1.crx" "MiddleFinger3_L.rx";
connectAttr "MiddleFinger3_L_parentConstraint1.cry" "MiddleFinger3_L.ry";
connectAttr "MiddleFinger3_L_parentConstraint1.crz" "MiddleFinger3_L.rz";
connectAttr "jointLayer.di" "MiddleFinger3_L.do";
connectAttr "MiddleFinger3_L.s" "MiddleFinger4_End_L.is";
connectAttr "jointLayer.di" "MiddleFinger4_End_L.do";
connectAttr "MiddleFinger3_L.ro" "MiddleFinger3_L_parentConstraint1.cro";
connectAttr "MiddleFinger3_L.pim" "MiddleFinger3_L_parentConstraint1.cpim";
connectAttr "MiddleFinger3_L.rp" "MiddleFinger3_L_parentConstraint1.crp";
connectAttr "MiddleFinger3_L.rpt" "MiddleFinger3_L_parentConstraint1.crt";
connectAttr "MiddleFinger3_L.jo" "MiddleFinger3_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger3_L.t" "MiddleFinger3_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger3_L.rp" "MiddleFinger3_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger3_L.rpt" "MiddleFinger3_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger3_L.r" "MiddleFinger3_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger3_L.ro" "MiddleFinger3_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger3_L.s" "MiddleFinger3_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger3_L.pm" "MiddleFinger3_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger3_L.jo" "MiddleFinger3_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger3_L_parentConstraint1.w0" "MiddleFinger3_L_parentConstraint1.tg[0].tw"
		;
connectAttr "MiddleFinger2_L.ro" "MiddleFinger2_L_parentConstraint1.cro";
connectAttr "MiddleFinger2_L.pim" "MiddleFinger2_L_parentConstraint1.cpim";
connectAttr "MiddleFinger2_L.rp" "MiddleFinger2_L_parentConstraint1.crp";
connectAttr "MiddleFinger2_L.rpt" "MiddleFinger2_L_parentConstraint1.crt";
connectAttr "MiddleFinger2_L.jo" "MiddleFinger2_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger2_L.t" "MiddleFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger2_L.rp" "MiddleFinger2_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger2_L.rpt" "MiddleFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger2_L.r" "MiddleFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger2_L.ro" "MiddleFinger2_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger2_L.s" "MiddleFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger2_L.pm" "MiddleFinger2_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger2_L.jo" "MiddleFinger2_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger2_L_parentConstraint1.w0" "MiddleFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "MiddleFinger1_L.ro" "MiddleFinger1_L_parentConstraint1.cro";
connectAttr "MiddleFinger1_L.pim" "MiddleFinger1_L_parentConstraint1.cpim";
connectAttr "MiddleFinger1_L.rp" "MiddleFinger1_L_parentConstraint1.crp";
connectAttr "MiddleFinger1_L.rpt" "MiddleFinger1_L_parentConstraint1.crt";
connectAttr "MiddleFinger1_L.jo" "MiddleFinger1_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger1_L.t" "MiddleFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger1_L.rp" "MiddleFinger1_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger1_L.rpt" "MiddleFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger1_L.r" "MiddleFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger1_L.ro" "MiddleFinger1_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger1_L.s" "MiddleFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger1_L.pm" "MiddleFinger1_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger1_L.jo" "MiddleFinger1_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger1_L_parentConstraint1.w0" "MiddleFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKThumbFinger1_L.s" "ThumbFinger1_L.s";
connectAttr "Wrist_L.s" "ThumbFinger1_L.is";
connectAttr "ThumbFinger1_L_parentConstraint1.ctx" "ThumbFinger1_L.tx";
connectAttr "ThumbFinger1_L_parentConstraint1.cty" "ThumbFinger1_L.ty";
connectAttr "ThumbFinger1_L_parentConstraint1.ctz" "ThumbFinger1_L.tz";
connectAttr "ThumbFinger1_L_parentConstraint1.crx" "ThumbFinger1_L.rx";
connectAttr "ThumbFinger1_L_parentConstraint1.cry" "ThumbFinger1_L.ry";
connectAttr "ThumbFinger1_L_parentConstraint1.crz" "ThumbFinger1_L.rz";
connectAttr "jointLayer.di" "ThumbFinger1_L.do";
connectAttr "FKThumbFinger2_L.s" "ThumbFinger2_L.s";
connectAttr "ThumbFinger1_L.s" "ThumbFinger2_L.is";
connectAttr "ThumbFinger2_L_parentConstraint1.ctx" "ThumbFinger2_L.tx";
connectAttr "ThumbFinger2_L_parentConstraint1.cty" "ThumbFinger2_L.ty";
connectAttr "ThumbFinger2_L_parentConstraint1.ctz" "ThumbFinger2_L.tz";
connectAttr "ThumbFinger2_L_parentConstraint1.crx" "ThumbFinger2_L.rx";
connectAttr "ThumbFinger2_L_parentConstraint1.cry" "ThumbFinger2_L.ry";
connectAttr "ThumbFinger2_L_parentConstraint1.crz" "ThumbFinger2_L.rz";
connectAttr "jointLayer.di" "ThumbFinger2_L.do";
connectAttr "ThumbFinger2_L.s" "ThumbFinger3_End_L.is";
connectAttr "jointLayer.di" "ThumbFinger3_End_L.do";
connectAttr "ThumbFinger2_L.ro" "ThumbFinger2_L_parentConstraint1.cro";
connectAttr "ThumbFinger2_L.pim" "ThumbFinger2_L_parentConstraint1.cpim";
connectAttr "ThumbFinger2_L.rp" "ThumbFinger2_L_parentConstraint1.crp";
connectAttr "ThumbFinger2_L.rpt" "ThumbFinger2_L_parentConstraint1.crt";
connectAttr "ThumbFinger2_L.jo" "ThumbFinger2_L_parentConstraint1.cjo";
connectAttr "FKXThumbFinger2_L.t" "ThumbFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger2_L.rp" "ThumbFinger2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger2_L.rpt" "ThumbFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger2_L.r" "ThumbFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger2_L.ro" "ThumbFinger2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger2_L.s" "ThumbFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger2_L.pm" "ThumbFinger2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger2_L.jo" "ThumbFinger2_L_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger2_L_parentConstraint1.w0" "ThumbFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "ThumbFinger1_L.ro" "ThumbFinger1_L_parentConstraint1.cro";
connectAttr "ThumbFinger1_L.pim" "ThumbFinger1_L_parentConstraint1.cpim";
connectAttr "ThumbFinger1_L.rp" "ThumbFinger1_L_parentConstraint1.crp";
connectAttr "ThumbFinger1_L.rpt" "ThumbFinger1_L_parentConstraint1.crt";
connectAttr "ThumbFinger1_L.jo" "ThumbFinger1_L_parentConstraint1.cjo";
connectAttr "FKXThumbFinger1_L.t" "ThumbFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger1_L.rp" "ThumbFinger1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger1_L.rpt" "ThumbFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger1_L.r" "ThumbFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger1_L.ro" "ThumbFinger1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger1_L.s" "ThumbFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger1_L.pm" "ThumbFinger1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger1_L.jo" "ThumbFinger1_L_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger1_L_parentConstraint1.w0" "ThumbFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Wrist_L.s" "Hand_AP1_L.is";
connectAttr "jointLayer.di" "Hand_AP1_L.do";
connectAttr "Wrist_L.ro" "Wrist_L_parentConstraint1.cro";
connectAttr "Wrist_L.pim" "Wrist_L_parentConstraint1.cpim";
connectAttr "Wrist_L.rp" "Wrist_L_parentConstraint1.crp";
connectAttr "Wrist_L.rpt" "Wrist_L_parentConstraint1.crt";
connectAttr "Wrist_L.jo" "Wrist_L_parentConstraint1.cjo";
connectAttr "FKXWrist_L.t" "Wrist_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWrist_L.rp" "Wrist_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWrist_L.rpt" "Wrist_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWrist_L.r" "Wrist_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWrist_L.ro" "Wrist_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWrist_L.s" "Wrist_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWrist_L.pm" "Wrist_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWrist_L.jo" "Wrist_L_parentConstraint1.tg[0].tjo";
connectAttr "Wrist_L_parentConstraint1.w0" "Wrist_L_parentConstraint1.tg[0].tw";
connectAttr "IKXWrist_L.t" "Wrist_L_parentConstraint1.tg[1].tt";
connectAttr "IKXWrist_L.rp" "Wrist_L_parentConstraint1.tg[1].trp";
connectAttr "IKXWrist_L.rpt" "Wrist_L_parentConstraint1.tg[1].trt";
connectAttr "IKXWrist_L.r" "Wrist_L_parentConstraint1.tg[1].tr";
connectAttr "IKXWrist_L.ro" "Wrist_L_parentConstraint1.tg[1].tro";
connectAttr "IKXWrist_L.s" "Wrist_L_parentConstraint1.tg[1].ts";
connectAttr "IKXWrist_L.pm" "Wrist_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXWrist_L.jo" "Wrist_L_parentConstraint1.tg[1].tjo";
connectAttr "Wrist_L_parentConstraint1.w1" "Wrist_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendArmReverse_L.ox" "Wrist_L_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_L.o" "Wrist_L_parentConstraint1.w1";
connectAttr "Elbow_L.ro" "Elbow_L_parentConstraint1.cro";
connectAttr "Elbow_L.pim" "Elbow_L_parentConstraint1.cpim";
connectAttr "Elbow_L.rp" "Elbow_L_parentConstraint1.crp";
connectAttr "Elbow_L.rpt" "Elbow_L_parentConstraint1.crt";
connectAttr "Elbow_L.jo" "Elbow_L_parentConstraint1.cjo";
connectAttr "FKXElbow_L.t" "Elbow_L_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow_L.rp" "Elbow_L_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow_L.rpt" "Elbow_L_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow_L.r" "Elbow_L_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow_L.ro" "Elbow_L_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow_L.s" "Elbow_L_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow_L.pm" "Elbow_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow_L.jo" "Elbow_L_parentConstraint1.tg[0].tjo";
connectAttr "Elbow_L_parentConstraint1.w0" "Elbow_L_parentConstraint1.tg[0].tw";
connectAttr "IKXElbow_L.t" "Elbow_L_parentConstraint1.tg[1].tt";
connectAttr "IKXElbow_L.rp" "Elbow_L_parentConstraint1.tg[1].trp";
connectAttr "IKXElbow_L.rpt" "Elbow_L_parentConstraint1.tg[1].trt";
connectAttr "IKXElbow_L.r" "Elbow_L_parentConstraint1.tg[1].tr";
connectAttr "IKXElbow_L.ro" "Elbow_L_parentConstraint1.tg[1].tro";
connectAttr "IKXElbow_L.s" "Elbow_L_parentConstraint1.tg[1].ts";
connectAttr "IKXElbow_L.pm" "Elbow_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXElbow_L.jo" "Elbow_L_parentConstraint1.tg[1].tjo";
connectAttr "Elbow_L_parentConstraint1.w1" "Elbow_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendArmReverse_L.ox" "Elbow_L_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_L.o" "Elbow_L_parentConstraint1.w1";
connectAttr "Shoulder_L.ro" "Shoulder_L_parentConstraint1.cro";
connectAttr "Shoulder_L.pim" "Shoulder_L_parentConstraint1.cpim";
connectAttr "Shoulder_L.rp" "Shoulder_L_parentConstraint1.crp";
connectAttr "Shoulder_L.rpt" "Shoulder_L_parentConstraint1.crt";
connectAttr "Shoulder_L.jo" "Shoulder_L_parentConstraint1.cjo";
connectAttr "FKXShoulder_L.t" "Shoulder_L_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder_L.rp" "Shoulder_L_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder_L.rpt" "Shoulder_L_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder_L.r" "Shoulder_L_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder_L.ro" "Shoulder_L_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder_L.s" "Shoulder_L_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder_L.pm" "Shoulder_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder_L.jo" "Shoulder_L_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder_L_parentConstraint1.w0" "Shoulder_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXShoulder_L.t" "Shoulder_L_parentConstraint1.tg[1].tt";
connectAttr "IKXShoulder_L.rp" "Shoulder_L_parentConstraint1.tg[1].trp";
connectAttr "IKXShoulder_L.rpt" "Shoulder_L_parentConstraint1.tg[1].trt";
connectAttr "IKXShoulder_L.r" "Shoulder_L_parentConstraint1.tg[1].tr";
connectAttr "IKXShoulder_L.ro" "Shoulder_L_parentConstraint1.tg[1].tro";
connectAttr "IKXShoulder_L.s" "Shoulder_L_parentConstraint1.tg[1].ts";
connectAttr "IKXShoulder_L.pm" "Shoulder_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXShoulder_L.jo" "Shoulder_L_parentConstraint1.tg[1].tjo";
connectAttr "Shoulder_L_parentConstraint1.w1" "Shoulder_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArmReverse_L.ox" "Shoulder_L_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_L.o" "Shoulder_L_parentConstraint1.w1";
connectAttr "Clavicle1_L.ro" "Clavicle1_L_parentConstraint1.cro";
connectAttr "Clavicle1_L.pim" "Clavicle1_L_parentConstraint1.cpim";
connectAttr "Clavicle1_L.rp" "Clavicle1_L_parentConstraint1.crp";
connectAttr "Clavicle1_L.rpt" "Clavicle1_L_parentConstraint1.crt";
connectAttr "Clavicle1_L.jo" "Clavicle1_L_parentConstraint1.cjo";
connectAttr "FKXClavicle1_L.t" "Clavicle1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXClavicle1_L.rp" "Clavicle1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXClavicle1_L.rpt" "Clavicle1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXClavicle1_L.r" "Clavicle1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXClavicle1_L.ro" "Clavicle1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXClavicle1_L.s" "Clavicle1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXClavicle1_L.pm" "Clavicle1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXClavicle1_L.jo" "Clavicle1_L_parentConstraint1.tg[0].tjo";
connectAttr "Clavicle1_L_parentConstraint1.w0" "Clavicle1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKPipe1_R.s" "Pipe1_R.s";
connectAttr "Chest_M.s" "Pipe1_R.is";
connectAttr "Pipe1_R_parentConstraint1.ctx" "Pipe1_R.tx";
connectAttr "Pipe1_R_parentConstraint1.cty" "Pipe1_R.ty";
connectAttr "Pipe1_R_parentConstraint1.ctz" "Pipe1_R.tz";
connectAttr "Pipe1_R_parentConstraint1.crx" "Pipe1_R.rx";
connectAttr "Pipe1_R_parentConstraint1.cry" "Pipe1_R.ry";
connectAttr "Pipe1_R_parentConstraint1.crz" "Pipe1_R.rz";
connectAttr "jointLayer.di" "Pipe1_R.do";
connectAttr "Pipe1_R.s" "Pipe1_End_R.is";
connectAttr "jointLayer.di" "Pipe1_End_R.do";
connectAttr "Pipe1_R.ro" "Pipe1_R_parentConstraint1.cro";
connectAttr "Pipe1_R.pim" "Pipe1_R_parentConstraint1.cpim";
connectAttr "Pipe1_R.rp" "Pipe1_R_parentConstraint1.crp";
connectAttr "Pipe1_R.rpt" "Pipe1_R_parentConstraint1.crt";
connectAttr "Pipe1_R.jo" "Pipe1_R_parentConstraint1.cjo";
connectAttr "FKXPipe1_R.t" "Pipe1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXPipe1_R.rp" "Pipe1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXPipe1_R.rpt" "Pipe1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXPipe1_R.r" "Pipe1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXPipe1_R.ro" "Pipe1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXPipe1_R.s" "Pipe1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXPipe1_R.pm" "Pipe1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXPipe1_R.jo" "Pipe1_R_parentConstraint1.tg[0].tjo";
connectAttr "Pipe1_R_parentConstraint1.w0" "Pipe1_R_parentConstraint1.tg[0].tw";
connectAttr "FKPipe2_R.s" "Pipe2_R.s";
connectAttr "Chest_M.s" "Pipe2_R.is";
connectAttr "Pipe2_R_parentConstraint1.ctx" "Pipe2_R.tx";
connectAttr "Pipe2_R_parentConstraint1.cty" "Pipe2_R.ty";
connectAttr "Pipe2_R_parentConstraint1.ctz" "Pipe2_R.tz";
connectAttr "Pipe2_R_parentConstraint1.crx" "Pipe2_R.rx";
connectAttr "Pipe2_R_parentConstraint1.cry" "Pipe2_R.ry";
connectAttr "Pipe2_R_parentConstraint1.crz" "Pipe2_R.rz";
connectAttr "jointLayer.di" "Pipe2_R.do";
connectAttr "Pipe2_R.s" "Pipe2_End_R.is";
connectAttr "jointLayer.di" "Pipe2_End_R.do";
connectAttr "Pipe2_R.ro" "Pipe2_R_parentConstraint1.cro";
connectAttr "Pipe2_R.pim" "Pipe2_R_parentConstraint1.cpim";
connectAttr "Pipe2_R.rp" "Pipe2_R_parentConstraint1.crp";
connectAttr "Pipe2_R.rpt" "Pipe2_R_parentConstraint1.crt";
connectAttr "Pipe2_R.jo" "Pipe2_R_parentConstraint1.cjo";
connectAttr "FKXPipe2_R.t" "Pipe2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXPipe2_R.rp" "Pipe2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXPipe2_R.rpt" "Pipe2_R_parentConstraint1.tg[0].trt";
connectAttr "FKXPipe2_R.r" "Pipe2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXPipe2_R.ro" "Pipe2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXPipe2_R.s" "Pipe2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXPipe2_R.pm" "Pipe2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXPipe2_R.jo" "Pipe2_R_parentConstraint1.tg[0].tjo";
connectAttr "Pipe2_R_parentConstraint1.w0" "Pipe2_R_parentConstraint1.tg[0].tw";
connectAttr "Chest_M.pim" "Chest_M_pointConstraint1.cpim";
connectAttr "Chest_M.rp" "Chest_M_pointConstraint1.crp";
connectAttr "Chest_M.rpt" "Chest_M_pointConstraint1.crt";
connectAttr "FKXChest_M.t" "Chest_M_pointConstraint1.tg[0].tt";
connectAttr "FKXChest_M.rp" "Chest_M_pointConstraint1.tg[0].trp";
connectAttr "FKXChest_M.rpt" "Chest_M_pointConstraint1.tg[0].trt";
connectAttr "FKXChest_M.pm" "Chest_M_pointConstraint1.tg[0].tpm";
connectAttr "Chest_M_pointConstraint1.w0" "Chest_M_pointConstraint1.tg[0].tw";
connectAttr "IKXChest_M.t" "Chest_M_pointConstraint1.tg[1].tt";
connectAttr "IKXChest_M.rp" "Chest_M_pointConstraint1.tg[1].trp";
connectAttr "IKXChest_M.rpt" "Chest_M_pointConstraint1.tg[1].trt";
connectAttr "IKXChest_M.pm" "Chest_M_pointConstraint1.tg[1].tpm";
connectAttr "Chest_M_pointConstraint1.w1" "Chest_M_pointConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "Chest_M_pointConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Chest_M_pointConstraint1.w1";
connectAttr "Chest_M.ro" "Chest_M_orientConstraint1.cro";
connectAttr "Chest_M.pim" "Chest_M_orientConstraint1.cpim";
connectAttr "Chest_M.jo" "Chest_M_orientConstraint1.cjo";
connectAttr "FKXChest_M.r" "Chest_M_orientConstraint1.tg[0].tr";
connectAttr "FKXChest_M.ro" "Chest_M_orientConstraint1.tg[0].tro";
connectAttr "FKXChest_M.pm" "Chest_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXChest_M.jo" "Chest_M_orientConstraint1.tg[0].tjo";
connectAttr "Chest_M_orientConstraint1.w0" "Chest_M_orientConstraint1.tg[0].tw";
connectAttr "IKXChest_M.r" "Chest_M_orientConstraint1.tg[1].tr";
connectAttr "IKXChest_M.ro" "Chest_M_orientConstraint1.tg[1].tro";
connectAttr "IKXChest_M.pm" "Chest_M_orientConstraint1.tg[1].tpm";
connectAttr "IKXChest_M.jo" "Chest_M_orientConstraint1.tg[1].tjo";
connectAttr "Chest_M_orientConstraint1.w1" "Chest_M_orientConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "Chest_M_orientConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Chest_M_orientConstraint1.w1";
connectAttr "SpineA_M.pim" "SpineA_M_pointConstraint1.cpim";
connectAttr "SpineA_M.rp" "SpineA_M_pointConstraint1.crp";
connectAttr "SpineA_M.rpt" "SpineA_M_pointConstraint1.crt";
connectAttr "FKXSpineA_M.t" "SpineA_M_pointConstraint1.tg[0].tt";
connectAttr "FKXSpineA_M.rp" "SpineA_M_pointConstraint1.tg[0].trp";
connectAttr "FKXSpineA_M.rpt" "SpineA_M_pointConstraint1.tg[0].trt";
connectAttr "FKXSpineA_M.pm" "SpineA_M_pointConstraint1.tg[0].tpm";
connectAttr "SpineA_M_pointConstraint1.w0" "SpineA_M_pointConstraint1.tg[0].tw";
connectAttr "IKXSpineA_M.t" "SpineA_M_pointConstraint1.tg[1].tt";
connectAttr "IKXSpineA_M.rp" "SpineA_M_pointConstraint1.tg[1].trp";
connectAttr "IKXSpineA_M.rpt" "SpineA_M_pointConstraint1.tg[1].trt";
connectAttr "IKXSpineA_M.pm" "SpineA_M_pointConstraint1.tg[1].tpm";
connectAttr "SpineA_M_pointConstraint1.w1" "SpineA_M_pointConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "SpineA_M_pointConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "SpineA_M_pointConstraint1.w1";
connectAttr "SpineA_M.ro" "SpineA_M_orientConstraint1.cro";
connectAttr "SpineA_M.pim" "SpineA_M_orientConstraint1.cpim";
connectAttr "SpineA_M.jo" "SpineA_M_orientConstraint1.cjo";
connectAttr "FKXSpineA_M.r" "SpineA_M_orientConstraint1.tg[0].tr";
connectAttr "FKXSpineA_M.ro" "SpineA_M_orientConstraint1.tg[0].tro";
connectAttr "FKXSpineA_M.pm" "SpineA_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXSpineA_M.jo" "SpineA_M_orientConstraint1.tg[0].tjo";
connectAttr "SpineA_M_orientConstraint1.w0" "SpineA_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXSpineA_M.r" "SpineA_M_orientConstraint1.tg[1].tr";
connectAttr "IKXSpineA_M.ro" "SpineA_M_orientConstraint1.tg[1].tro";
connectAttr "IKXSpineA_M.pm" "SpineA_M_orientConstraint1.tg[1].tpm";
connectAttr "IKXSpineA_M.jo" "SpineA_M_orientConstraint1.tg[1].tjo";
connectAttr "SpineA_M_orientConstraint1.w1" "SpineA_M_orientConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendSpineReverse_M.ox" "SpineA_M_orientConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "SpineA_M_orientConstraint1.w1";
connectAttr "FKHipTwist_R.s" "HipTwist_R.s";
connectAttr "HipTwist_R_parentConstraint1.ctx" "HipTwist_R.tx";
connectAttr "HipTwist_R_parentConstraint1.cty" "HipTwist_R.ty";
connectAttr "HipTwist_R_parentConstraint1.ctz" "HipTwist_R.tz";
connectAttr "HipTwist_R_parentConstraint1.crx" "HipTwist_R.rx";
connectAttr "HipTwist_R_parentConstraint1.cry" "HipTwist_R.ry";
connectAttr "HipTwist_R_parentConstraint1.crz" "HipTwist_R.rz";
connectAttr "Pelvis_M.s" "HipTwist_R.is";
connectAttr "jointLayer.di" "HipTwist_R.do";
connectAttr "ScaleBlendHip_R.op" "Hip_R.s";
connectAttr "Hip_R_parentConstraint1.ctx" "Hip_R.tx";
connectAttr "Hip_R_parentConstraint1.cty" "Hip_R.ty";
connectAttr "Hip_R_parentConstraint1.ctz" "Hip_R.tz";
connectAttr "Hip_R_parentConstraint1.crx" "Hip_R.rx";
connectAttr "Hip_R_parentConstraint1.cry" "Hip_R.ry";
connectAttr "Hip_R_parentConstraint1.crz" "Hip_R.rz";
connectAttr "jointLayer.di" "Hip_R.do";
connectAttr "ScaleBlendKnee_R.op" "Knee_R.s";
connectAttr "Hip_R.s" "Knee_R.is";
connectAttr "Knee_R_parentConstraint1.ctx" "Knee_R.tx";
connectAttr "Knee_R_parentConstraint1.cty" "Knee_R.ty";
connectAttr "Knee_R_parentConstraint1.ctz" "Knee_R.tz";
connectAttr "Knee_R_parentConstraint1.crx" "Knee_R.rx";
connectAttr "Knee_R_parentConstraint1.cry" "Knee_R.ry";
connectAttr "Knee_R_parentConstraint1.crz" "Knee_R.rz";
connectAttr "jointLayer.di" "Knee_R.do";
connectAttr "ScaleBlendAnkle_R.op" "Ankle_R.s";
connectAttr "Knee_R.s" "Ankle_R.is";
connectAttr "Ankle_R_parentConstraint1.ctx" "Ankle_R.tx";
connectAttr "Ankle_R_parentConstraint1.cty" "Ankle_R.ty";
connectAttr "Ankle_R_parentConstraint1.ctz" "Ankle_R.tz";
connectAttr "Ankle_R_parentConstraint1.crx" "Ankle_R.rx";
connectAttr "Ankle_R_parentConstraint1.cry" "Ankle_R.ry";
connectAttr "Ankle_R_parentConstraint1.crz" "Ankle_R.rz";
connectAttr "jointLayer.di" "Ankle_R.do";
connectAttr "FKMiddleToe1_R.s" "MiddleToe1_R.s";
connectAttr "Ankle_R.s" "MiddleToe1_R.is";
connectAttr "MiddleToe1_R_parentConstraint1.ctx" "MiddleToe1_R.tx";
connectAttr "MiddleToe1_R_parentConstraint1.cty" "MiddleToe1_R.ty";
connectAttr "MiddleToe1_R_parentConstraint1.ctz" "MiddleToe1_R.tz";
connectAttr "MiddleToe1_R_parentConstraint1.crx" "MiddleToe1_R.rx";
connectAttr "MiddleToe1_R_parentConstraint1.cry" "MiddleToe1_R.ry";
connectAttr "MiddleToe1_R_parentConstraint1.crz" "MiddleToe1_R.rz";
connectAttr "jointLayer.di" "MiddleToe1_R.do";
connectAttr "MiddleToe1_R.s" "MiddleToe2_End_R.is";
connectAttr "jointLayer.di" "MiddleToe2_End_R.do";
connectAttr "MiddleToe1_R.ro" "MiddleToe1_R_parentConstraint1.cro";
connectAttr "MiddleToe1_R.pim" "MiddleToe1_R_parentConstraint1.cpim";
connectAttr "MiddleToe1_R.rp" "MiddleToe1_R_parentConstraint1.crp";
connectAttr "MiddleToe1_R.rpt" "MiddleToe1_R_parentConstraint1.crt";
connectAttr "MiddleToe1_R.jo" "MiddleToe1_R_parentConstraint1.cjo";
connectAttr "FKXMiddleToe1_R.t" "MiddleToe1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleToe1_R.rp" "MiddleToe1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXMiddleToe1_R.rpt" "MiddleToe1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXMiddleToe1_R.r" "MiddleToe1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleToe1_R.ro" "MiddleToe1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXMiddleToe1_R.s" "MiddleToe1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleToe1_R.pm" "MiddleToe1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXMiddleToe1_R.jo" "MiddleToe1_R_parentConstraint1.tg[0].tjo";
connectAttr "MiddleToe1_R_parentConstraint1.w0" "MiddleToe1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Ankle_R.ro" "Ankle_R_parentConstraint1.cro";
connectAttr "Ankle_R.pim" "Ankle_R_parentConstraint1.cpim";
connectAttr "Ankle_R.rp" "Ankle_R_parentConstraint1.crp";
connectAttr "Ankle_R.rpt" "Ankle_R_parentConstraint1.crt";
connectAttr "Ankle_R.jo" "Ankle_R_parentConstraint1.cjo";
connectAttr "FKXAnkle_R.t" "Ankle_R_parentConstraint1.tg[0].tt";
connectAttr "FKXAnkle_R.rp" "Ankle_R_parentConstraint1.tg[0].trp";
connectAttr "FKXAnkle_R.rpt" "Ankle_R_parentConstraint1.tg[0].trt";
connectAttr "FKXAnkle_R.r" "Ankle_R_parentConstraint1.tg[0].tr";
connectAttr "FKXAnkle_R.ro" "Ankle_R_parentConstraint1.tg[0].tro";
connectAttr "FKXAnkle_R.s" "Ankle_R_parentConstraint1.tg[0].ts";
connectAttr "FKXAnkle_R.pm" "Ankle_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXAnkle_R.jo" "Ankle_R_parentConstraint1.tg[0].tjo";
connectAttr "Ankle_R_parentConstraint1.w0" "Ankle_R_parentConstraint1.tg[0].tw";
connectAttr "IKXAnkle_R.t" "Ankle_R_parentConstraint1.tg[1].tt";
connectAttr "IKXAnkle_R.rp" "Ankle_R_parentConstraint1.tg[1].trp";
connectAttr "IKXAnkle_R.rpt" "Ankle_R_parentConstraint1.tg[1].trt";
connectAttr "IKXAnkle_R.r" "Ankle_R_parentConstraint1.tg[1].tr";
connectAttr "IKXAnkle_R.ro" "Ankle_R_parentConstraint1.tg[1].tro";
connectAttr "IKXAnkle_R.s" "Ankle_R_parentConstraint1.tg[1].ts";
connectAttr "IKXAnkle_R.pm" "Ankle_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXAnkle_R.jo" "Ankle_R_parentConstraint1.tg[1].tjo";
connectAttr "Ankle_R_parentConstraint1.w1" "Ankle_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Ankle_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Ankle_R_parentConstraint1.w1";
connectAttr "Knee_R.ro" "Knee_R_parentConstraint1.cro";
connectAttr "Knee_R.pim" "Knee_R_parentConstraint1.cpim";
connectAttr "Knee_R.rp" "Knee_R_parentConstraint1.crp";
connectAttr "Knee_R.rpt" "Knee_R_parentConstraint1.crt";
connectAttr "Knee_R.jo" "Knee_R_parentConstraint1.cjo";
connectAttr "FKXKnee_R.t" "Knee_R_parentConstraint1.tg[0].tt";
connectAttr "FKXKnee_R.rp" "Knee_R_parentConstraint1.tg[0].trp";
connectAttr "FKXKnee_R.rpt" "Knee_R_parentConstraint1.tg[0].trt";
connectAttr "FKXKnee_R.r" "Knee_R_parentConstraint1.tg[0].tr";
connectAttr "FKXKnee_R.ro" "Knee_R_parentConstraint1.tg[0].tro";
connectAttr "FKXKnee_R.s" "Knee_R_parentConstraint1.tg[0].ts";
connectAttr "FKXKnee_R.pm" "Knee_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXKnee_R.jo" "Knee_R_parentConstraint1.tg[0].tjo";
connectAttr "Knee_R_parentConstraint1.w0" "Knee_R_parentConstraint1.tg[0].tw";
connectAttr "IKXKnee_R.t" "Knee_R_parentConstraint1.tg[1].tt";
connectAttr "IKXKnee_R.rp" "Knee_R_parentConstraint1.tg[1].trp";
connectAttr "IKXKnee_R.rpt" "Knee_R_parentConstraint1.tg[1].trt";
connectAttr "IKXKnee_R.r" "Knee_R_parentConstraint1.tg[1].tr";
connectAttr "IKXKnee_R.ro" "Knee_R_parentConstraint1.tg[1].tro";
connectAttr "IKXKnee_R.s" "Knee_R_parentConstraint1.tg[1].ts";
connectAttr "IKXKnee_R.pm" "Knee_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXKnee_R.jo" "Knee_R_parentConstraint1.tg[1].tjo";
connectAttr "Knee_R_parentConstraint1.w1" "Knee_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Knee_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Knee_R_parentConstraint1.w1";
connectAttr "Hip_R.ro" "Hip_R_parentConstraint1.cro";
connectAttr "Hip_R.pim" "Hip_R_parentConstraint1.cpim";
connectAttr "Hip_R.rp" "Hip_R_parentConstraint1.crp";
connectAttr "Hip_R.rpt" "Hip_R_parentConstraint1.crt";
connectAttr "Hip_R.jo" "Hip_R_parentConstraint1.cjo";
connectAttr "FKXHip_R.t" "Hip_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHip_R.rp" "Hip_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHip_R.rpt" "Hip_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHip_R.r" "Hip_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHip_R.ro" "Hip_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHip_R.s" "Hip_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHip_R.pm" "Hip_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHip_R.jo" "Hip_R_parentConstraint1.tg[0].tjo";
connectAttr "Hip_R_parentConstraint1.w0" "Hip_R_parentConstraint1.tg[0].tw";
connectAttr "IKXHip_R.t" "Hip_R_parentConstraint1.tg[1].tt";
connectAttr "IKXHip_R.rp" "Hip_R_parentConstraint1.tg[1].trp";
connectAttr "IKXHip_R.rpt" "Hip_R_parentConstraint1.tg[1].trt";
connectAttr "IKXHip_R.r" "Hip_R_parentConstraint1.tg[1].tr";
connectAttr "IKXHip_R.ro" "Hip_R_parentConstraint1.tg[1].tro";
connectAttr "IKXHip_R.s" "Hip_R_parentConstraint1.tg[1].ts";
connectAttr "IKXHip_R.pm" "Hip_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXHip_R.jo" "Hip_R_parentConstraint1.tg[1].tjo";
connectAttr "Hip_R_parentConstraint1.w1" "Hip_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Hip_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Hip_R_parentConstraint1.w1";
connectAttr "HipTwist_R.ro" "HipTwist_R_parentConstraint1.cro";
connectAttr "HipTwist_R.pim" "HipTwist_R_parentConstraint1.cpim";
connectAttr "HipTwist_R.rp" "HipTwist_R_parentConstraint1.crp";
connectAttr "HipTwist_R.rpt" "HipTwist_R_parentConstraint1.crt";
connectAttr "HipTwist_R.jo" "HipTwist_R_parentConstraint1.cjo";
connectAttr "FKXHipTwist_R.t" "HipTwist_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHipTwist_R.rp" "HipTwist_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHipTwist_R.rpt" "HipTwist_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHipTwist_R.r" "HipTwist_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHipTwist_R.ro" "HipTwist_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHipTwist_R.s" "HipTwist_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHipTwist_R.pm" "HipTwist_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHipTwist_R.jo" "HipTwist_R_parentConstraint1.tg[0].tjo";
connectAttr "HipTwist_R_parentConstraint1.w0" "HipTwist_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKHipTwist_L.s" "HipTwist_L.s";
connectAttr "HipTwist_L_parentConstraint1.ctx" "HipTwist_L.tx";
connectAttr "HipTwist_L_parentConstraint1.cty" "HipTwist_L.ty";
connectAttr "HipTwist_L_parentConstraint1.ctz" "HipTwist_L.tz";
connectAttr "HipTwist_L_parentConstraint1.crx" "HipTwist_L.rx";
connectAttr "HipTwist_L_parentConstraint1.cry" "HipTwist_L.ry";
connectAttr "HipTwist_L_parentConstraint1.crz" "HipTwist_L.rz";
connectAttr "Pelvis_M.s" "HipTwist_L.is";
connectAttr "jointLayer.di" "HipTwist_L.do";
connectAttr "ScaleBlendHip_L.op" "Hip_L.s";
connectAttr "Hip_L_parentConstraint1.ctx" "Hip_L.tx";
connectAttr "Hip_L_parentConstraint1.cty" "Hip_L.ty";
connectAttr "Hip_L_parentConstraint1.ctz" "Hip_L.tz";
connectAttr "Hip_L_parentConstraint1.crx" "Hip_L.rx";
connectAttr "Hip_L_parentConstraint1.cry" "Hip_L.ry";
connectAttr "Hip_L_parentConstraint1.crz" "Hip_L.rz";
connectAttr "jointLayer.di" "Hip_L.do";
connectAttr "ScaleBlendKnee_L.op" "Knee_L.s";
connectAttr "Hip_L.s" "Knee_L.is";
connectAttr "Knee_L_parentConstraint1.ctx" "Knee_L.tx";
connectAttr "Knee_L_parentConstraint1.cty" "Knee_L.ty";
connectAttr "Knee_L_parentConstraint1.ctz" "Knee_L.tz";
connectAttr "Knee_L_parentConstraint1.crx" "Knee_L.rx";
connectAttr "Knee_L_parentConstraint1.cry" "Knee_L.ry";
connectAttr "Knee_L_parentConstraint1.crz" "Knee_L.rz";
connectAttr "jointLayer.di" "Knee_L.do";
connectAttr "ScaleBlendAnkle_L.op" "Ankle_L.s";
connectAttr "Knee_L.s" "Ankle_L.is";
connectAttr "Ankle_L_parentConstraint1.ctx" "Ankle_L.tx";
connectAttr "Ankle_L_parentConstraint1.cty" "Ankle_L.ty";
connectAttr "Ankle_L_parentConstraint1.ctz" "Ankle_L.tz";
connectAttr "Ankle_L_parentConstraint1.crx" "Ankle_L.rx";
connectAttr "Ankle_L_parentConstraint1.cry" "Ankle_L.ry";
connectAttr "Ankle_L_parentConstraint1.crz" "Ankle_L.rz";
connectAttr "jointLayer.di" "Ankle_L.do";
connectAttr "FKMiddleToe1_L.s" "MiddleToe1_L.s";
connectAttr "Ankle_L.s" "MiddleToe1_L.is";
connectAttr "MiddleToe1_L_parentConstraint1.ctx" "MiddleToe1_L.tx";
connectAttr "MiddleToe1_L_parentConstraint1.cty" "MiddleToe1_L.ty";
connectAttr "MiddleToe1_L_parentConstraint1.ctz" "MiddleToe1_L.tz";
connectAttr "MiddleToe1_L_parentConstraint1.crx" "MiddleToe1_L.rx";
connectAttr "MiddleToe1_L_parentConstraint1.cry" "MiddleToe1_L.ry";
connectAttr "MiddleToe1_L_parentConstraint1.crz" "MiddleToe1_L.rz";
connectAttr "jointLayer.di" "MiddleToe1_L.do";
connectAttr "MiddleToe1_L.s" "MiddleToe2_End_L.is";
connectAttr "jointLayer.di" "MiddleToe2_End_L.do";
connectAttr "MiddleToe1_L.ro" "MiddleToe1_L_parentConstraint1.cro";
connectAttr "MiddleToe1_L.pim" "MiddleToe1_L_parentConstraint1.cpim";
connectAttr "MiddleToe1_L.rp" "MiddleToe1_L_parentConstraint1.crp";
connectAttr "MiddleToe1_L.rpt" "MiddleToe1_L_parentConstraint1.crt";
connectAttr "MiddleToe1_L.jo" "MiddleToe1_L_parentConstraint1.cjo";
connectAttr "FKXMiddleToe1_L.t" "MiddleToe1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleToe1_L.rp" "MiddleToe1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXMiddleToe1_L.rpt" "MiddleToe1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXMiddleToe1_L.r" "MiddleToe1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleToe1_L.ro" "MiddleToe1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXMiddleToe1_L.s" "MiddleToe1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleToe1_L.pm" "MiddleToe1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXMiddleToe1_L.jo" "MiddleToe1_L_parentConstraint1.tg[0].tjo";
connectAttr "MiddleToe1_L_parentConstraint1.w0" "MiddleToe1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Ankle_L.ro" "Ankle_L_parentConstraint1.cro";
connectAttr "Ankle_L.pim" "Ankle_L_parentConstraint1.cpim";
connectAttr "Ankle_L.rp" "Ankle_L_parentConstraint1.crp";
connectAttr "Ankle_L.rpt" "Ankle_L_parentConstraint1.crt";
connectAttr "Ankle_L.jo" "Ankle_L_parentConstraint1.cjo";
connectAttr "FKXAnkle_L.t" "Ankle_L_parentConstraint1.tg[0].tt";
connectAttr "FKXAnkle_L.rp" "Ankle_L_parentConstraint1.tg[0].trp";
connectAttr "FKXAnkle_L.rpt" "Ankle_L_parentConstraint1.tg[0].trt";
connectAttr "FKXAnkle_L.r" "Ankle_L_parentConstraint1.tg[0].tr";
connectAttr "FKXAnkle_L.ro" "Ankle_L_parentConstraint1.tg[0].tro";
connectAttr "FKXAnkle_L.s" "Ankle_L_parentConstraint1.tg[0].ts";
connectAttr "FKXAnkle_L.pm" "Ankle_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXAnkle_L.jo" "Ankle_L_parentConstraint1.tg[0].tjo";
connectAttr "Ankle_L_parentConstraint1.w0" "Ankle_L_parentConstraint1.tg[0].tw";
connectAttr "IKXAnkle_L.t" "Ankle_L_parentConstraint1.tg[1].tt";
connectAttr "IKXAnkle_L.rp" "Ankle_L_parentConstraint1.tg[1].trp";
connectAttr "IKXAnkle_L.rpt" "Ankle_L_parentConstraint1.tg[1].trt";
connectAttr "IKXAnkle_L.r" "Ankle_L_parentConstraint1.tg[1].tr";
connectAttr "IKXAnkle_L.ro" "Ankle_L_parentConstraint1.tg[1].tro";
connectAttr "IKXAnkle_L.s" "Ankle_L_parentConstraint1.tg[1].ts";
connectAttr "IKXAnkle_L.pm" "Ankle_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXAnkle_L.jo" "Ankle_L_parentConstraint1.tg[1].tjo";
connectAttr "Ankle_L_parentConstraint1.w1" "Ankle_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Ankle_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Ankle_L_parentConstraint1.w1";
connectAttr "Knee_L.ro" "Knee_L_parentConstraint1.cro";
connectAttr "Knee_L.pim" "Knee_L_parentConstraint1.cpim";
connectAttr "Knee_L.rp" "Knee_L_parentConstraint1.crp";
connectAttr "Knee_L.rpt" "Knee_L_parentConstraint1.crt";
connectAttr "Knee_L.jo" "Knee_L_parentConstraint1.cjo";
connectAttr "FKXKnee_L.t" "Knee_L_parentConstraint1.tg[0].tt";
connectAttr "FKXKnee_L.rp" "Knee_L_parentConstraint1.tg[0].trp";
connectAttr "FKXKnee_L.rpt" "Knee_L_parentConstraint1.tg[0].trt";
connectAttr "FKXKnee_L.r" "Knee_L_parentConstraint1.tg[0].tr";
connectAttr "FKXKnee_L.ro" "Knee_L_parentConstraint1.tg[0].tro";
connectAttr "FKXKnee_L.s" "Knee_L_parentConstraint1.tg[0].ts";
connectAttr "FKXKnee_L.pm" "Knee_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXKnee_L.jo" "Knee_L_parentConstraint1.tg[0].tjo";
connectAttr "Knee_L_parentConstraint1.w0" "Knee_L_parentConstraint1.tg[0].tw";
connectAttr "IKXKnee_L.t" "Knee_L_parentConstraint1.tg[1].tt";
connectAttr "IKXKnee_L.rp" "Knee_L_parentConstraint1.tg[1].trp";
connectAttr "IKXKnee_L.rpt" "Knee_L_parentConstraint1.tg[1].trt";
connectAttr "IKXKnee_L.r" "Knee_L_parentConstraint1.tg[1].tr";
connectAttr "IKXKnee_L.ro" "Knee_L_parentConstraint1.tg[1].tro";
connectAttr "IKXKnee_L.s" "Knee_L_parentConstraint1.tg[1].ts";
connectAttr "IKXKnee_L.pm" "Knee_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXKnee_L.jo" "Knee_L_parentConstraint1.tg[1].tjo";
connectAttr "Knee_L_parentConstraint1.w1" "Knee_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Knee_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Knee_L_parentConstraint1.w1";
connectAttr "Hip_L.ro" "Hip_L_parentConstraint1.cro";
connectAttr "Hip_L.pim" "Hip_L_parentConstraint1.cpim";
connectAttr "Hip_L.rp" "Hip_L_parentConstraint1.crp";
connectAttr "Hip_L.rpt" "Hip_L_parentConstraint1.crt";
connectAttr "Hip_L.jo" "Hip_L_parentConstraint1.cjo";
connectAttr "FKXHip_L.t" "Hip_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHip_L.rp" "Hip_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHip_L.rpt" "Hip_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHip_L.r" "Hip_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHip_L.ro" "Hip_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHip_L.s" "Hip_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHip_L.pm" "Hip_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHip_L.jo" "Hip_L_parentConstraint1.tg[0].tjo";
connectAttr "Hip_L_parentConstraint1.w0" "Hip_L_parentConstraint1.tg[0].tw";
connectAttr "IKXHip_L.t" "Hip_L_parentConstraint1.tg[1].tt";
connectAttr "IKXHip_L.rp" "Hip_L_parentConstraint1.tg[1].trp";
connectAttr "IKXHip_L.rpt" "Hip_L_parentConstraint1.tg[1].trt";
connectAttr "IKXHip_L.r" "Hip_L_parentConstraint1.tg[1].tr";
connectAttr "IKXHip_L.ro" "Hip_L_parentConstraint1.tg[1].tro";
connectAttr "IKXHip_L.s" "Hip_L_parentConstraint1.tg[1].ts";
connectAttr "IKXHip_L.pm" "Hip_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXHip_L.jo" "Hip_L_parentConstraint1.tg[1].tjo";
connectAttr "Hip_L_parentConstraint1.w1" "Hip_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Hip_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Hip_L_parentConstraint1.w1";
connectAttr "HipTwist_L.ro" "HipTwist_L_parentConstraint1.cro";
connectAttr "HipTwist_L.pim" "HipTwist_L_parentConstraint1.cpim";
connectAttr "HipTwist_L.rp" "HipTwist_L_parentConstraint1.crp";
connectAttr "HipTwist_L.rpt" "HipTwist_L_parentConstraint1.crt";
connectAttr "HipTwist_L.jo" "HipTwist_L_parentConstraint1.cjo";
connectAttr "FKXHipTwist_L.t" "HipTwist_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHipTwist_L.rp" "HipTwist_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHipTwist_L.rpt" "HipTwist_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHipTwist_L.r" "HipTwist_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHipTwist_L.ro" "HipTwist_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHipTwist_L.s" "HipTwist_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHipTwist_L.pm" "HipTwist_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHipTwist_L.jo" "HipTwist_L_parentConstraint1.tg[0].tjo";
connectAttr "HipTwist_L_parentConstraint1.w0" "HipTwist_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Pelvis_M.pim" "Pelvis_M_pointConstraint1.cpim";
connectAttr "Pelvis_M.rp" "Pelvis_M_pointConstraint1.crp";
connectAttr "Pelvis_M.rpt" "Pelvis_M_pointConstraint1.crt";
connectAttr "FKXPelvis_M.t" "Pelvis_M_pointConstraint1.tg[0].tt";
connectAttr "FKXPelvis_M.rp" "Pelvis_M_pointConstraint1.tg[0].trp";
connectAttr "FKXPelvis_M.rpt" "Pelvis_M_pointConstraint1.tg[0].trt";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_pointConstraint1.tg[0].tpm";
connectAttr "Pelvis_M_pointConstraint1.w0" "Pelvis_M_pointConstraint1.tg[0].tw";
connectAttr "IKXPelvis_M.t" "Pelvis_M_pointConstraint1.tg[1].tt";
connectAttr "IKXPelvis_M.rp" "Pelvis_M_pointConstraint1.tg[1].trp";
connectAttr "IKXPelvis_M.rpt" "Pelvis_M_pointConstraint1.tg[1].trt";
connectAttr "IKXPelvis_M.pm" "Pelvis_M_pointConstraint1.tg[1].tpm";
connectAttr "Pelvis_M_pointConstraint1.w1" "Pelvis_M_pointConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "Pelvis_M_pointConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Pelvis_M_pointConstraint1.w1";
connectAttr "Pelvis_M.ro" "Pelvis_M_orientConstraint1.cro";
connectAttr "Pelvis_M.pim" "Pelvis_M_orientConstraint1.cpim";
connectAttr "Pelvis_M.jo" "Pelvis_M_orientConstraint1.cjo";
connectAttr "FKXPelvis_M.r" "Pelvis_M_orientConstraint1.tg[0].tr";
connectAttr "FKXPelvis_M.ro" "Pelvis_M_orientConstraint1.tg[0].tro";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXPelvis_M.jo" "Pelvis_M_orientConstraint1.tg[0].tjo";
connectAttr "Pelvis_M_orientConstraint1.w0" "Pelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKOrientToSpine_M.r" "Pelvis_M_orientConstraint1.tg[1].tr";
connectAttr "IKOrientToSpine_M.ro" "Pelvis_M_orientConstraint1.tg[1].tro";
connectAttr "IKOrientToSpine_M.pm" "Pelvis_M_orientConstraint1.tg[1].tpm";
connectAttr "Pelvis_M_orientConstraint1.w1" "Pelvis_M_orientConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendSpineReverse_M.ox" "Pelvis_M_orientConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Pelvis_M_orientConstraint1.w1";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "irisPuppetSG.msg" "materialInfo3.sg";
connectAttr "BPRig_irisPuppetSG.msg" "BPRig_materialInfo3.sg";
connectAttr "jointVisReverse.ox" "jointLayer.lod";
connectAttr "Main.jointVis" "jointVisReverse.ix";
connectAttr "Main.iog" "ControlSet.dsm" -na;
connectAttr "FKBarrel1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraBarrel1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKBarrel3_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraBarrel3_R.iog" "ControlSet.dsm" -na;
connectAttr "FKBarrel2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraBarrel2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWrist1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWrist1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWrist2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWrist2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKClavicle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraClavicle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHead_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHead_M.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger6_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger6_L.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger5_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger5_L.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWrist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWrist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder_L.iog" "ControlSet.dsm" -na;
connectAttr "FKClavicle1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraClavicle1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKPipe1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraPipe1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKPipe2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraPipe2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKChest_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraChest_M.iog" "ControlSet.dsm" -na;
connectAttr "FKSpineA_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraSpineA_M.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleToe1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKAnkle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraAnkle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHip_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHip_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHipTwist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHipTwist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleToe1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKAnkle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraAnkle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHip_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHip_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHipTwist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHipTwist_L.iog" "ControlSet.dsm" -na;
connectAttr "HipSwingerPelvis_M.iog" "ControlSet.dsm" -na;
connectAttr "IKArm1_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraArm1_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleArm1_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraArm1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIKArm1_R.iog" "ControlSet.dsm" -na;
connectAttr "IKArm_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraArm_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleArm_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraArm_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIKArm_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegToe_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegToe_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKSpine0_M.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraSpine0_M.iog" "ControlSet.dsm" -na;
connectAttr "IKSpine2_M.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraSpine2_M.iog" "ControlSet.dsm" -na;
connectAttr "IKSpine4_M.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraSpine4_M.iog" "ControlSet.dsm" -na;
connectAttr "FKIKSpine_M.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegToe_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegToe_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "ControlSet.dsm" -na;
connectAttr "Center_M.iog" "ControlSet.dsm" -na;
connectAttr "Barrel1_R.iog" "GameSet.dsm" -na;
connectAttr "Barrel3_R.iog" "GameSet.dsm" -na;
connectAttr "Barrel2_R.iog" "GameSet.dsm" -na;
connectAttr "Wrist1_R.iog" "GameSet.dsm" -na;
connectAttr "Wrist2_R.iog" "GameSet.dsm" -na;
connectAttr "Elbow1_R.iog" "GameSet.dsm" -na;
connectAttr "Shoulder1_R.iog" "GameSet.dsm" -na;
connectAttr "Clavicle_R.iog" "GameSet.dsm" -na;
connectAttr "Head_M.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger6_L.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger5_L.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger3_L.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "Wrist_L.iog" "GameSet.dsm" -na;
connectAttr "Elbow_L.iog" "GameSet.dsm" -na;
connectAttr "Shoulder_L.iog" "GameSet.dsm" -na;
connectAttr "Clavicle1_L.iog" "GameSet.dsm" -na;
connectAttr "Pipe1_R.iog" "GameSet.dsm" -na;
connectAttr "Pipe2_R.iog" "GameSet.dsm" -na;
connectAttr "Chest_M.iog" "GameSet.dsm" -na;
connectAttr "SpineA_M.iog" "GameSet.dsm" -na;
connectAttr "MiddleToe1_R.iog" "GameSet.dsm" -na;
connectAttr "Ankle_R.iog" "GameSet.dsm" -na;
connectAttr "Knee_R.iog" "GameSet.dsm" -na;
connectAttr "Hip_R.iog" "GameSet.dsm" -na;
connectAttr "HipTwist_R.iog" "GameSet.dsm" -na;
connectAttr "Pelvis_M.iog" "GameSet.dsm" -na;
connectAttr "MiddleToe1_L.iog" "GameSet.dsm" -na;
connectAttr "Ankle_L.iog" "GameSet.dsm" -na;
connectAttr "Knee_L.iog" "GameSet.dsm" -na;
connectAttr "Hip_L.iog" "GameSet.dsm" -na;
connectAttr "HipTwist_L.iog" "GameSet.dsm" -na;
connectAttr "buildPose.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "CenterBtwFeet_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAddYKnee_L.msg" "AllSet.dnsm" -na;
connectAttr "IKXAnkle_L_IKLength_L.msg" "AllSet.dnsm" -na;
connectAttr "IKXAnkle_L_IKmessureDiv_L.msg" "AllSet.dnsm" -na;
connectAttr "IKXKnee_L_IKLength_L.msg" "AllSet.dnsm" -na;
connectAttr "IKXKnee_L_IKmessureDiv_L.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureBlendStretchLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceClampLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceLeg_LShape_normal.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceLeg_LShape_antiPop.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureBlendAntiPopLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureDivLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "IKSetRangeAntiPopLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "IKSetRangeStretchLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendHip_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendKnee_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAnkle_L.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion12.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion11.msg" "AllSet.dnsm" -na;
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_L.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion10.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion9.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion8.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "Leg_LAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "IKLiftToeLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion7.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAddYKnee_R.msg" "AllSet.dnsm" -na;
connectAttr "IKXAnkle_R_IKLength_R.msg" "AllSet.dnsm" -na;
connectAttr "IKXAnkle_R_IKmessureDiv_R.msg" "AllSet.dnsm" -na;
connectAttr "IKXKnee_R_IKLength_R.msg" "AllSet.dnsm" -na;
connectAttr "IKXKnee_R_IKmessureDiv_R.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureBlendStretchLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceClampLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceLeg_RShape_normal.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceLeg_RShape_antiPop.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureBlendAntiPopLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureDivLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "IKSetRangeAntiPopLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "IKSetRangeStretchLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendPelvis_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendHip_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendKnee_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAnkle_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendSpineA_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendChest_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendShoulder_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendElbow_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendWrist_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendShoulder1_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendElbow1_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendWrist2_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendWrist1_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpinesetRange_M.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpineCondition_M.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpineReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpineUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "IKTwistSpineUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo3_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide3_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo2_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide2_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo1_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide1_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo0_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide0_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "IKCurveInfoAllMultiplySpine_M.msg" "AllSet.dnsm" -na;
connectAttr "IKCurveInfoNormalizeSpine_M.msg" "AllSet.dnsm" -na;
connectAttr "IKCurveInfoSpine_M.msg" "AllSet.dnsm" -na;
connectAttr "IKStiffSpine4_M.msg" "AllSet.dnsm" -na;
connectAttr "IKStiffSpine2_M.msg" "AllSet.dnsm" -na;
connectAttr "IKStiffSpine0_M.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion6.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion5.msg" "AllSet.dnsm" -na;
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_R.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion4.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion3.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion2.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "Leg_RAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "IKLiftToeLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion1.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmsetRange_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmCondition_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "PoleArm_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "IKArm_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArm1setRange_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArm1Condition_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArm1Reverse_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArm1UnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "PoleArm1_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "IKArm1_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "GameSet.msg" "AllSet.dnsm" -na;
connectAttr "ControlSet.msg" "AllSet.dnsm" -na;
connectAttr "jointVisReverse.msg" "AllSet.dnsm" -na;
connectAttr "jointLayer.msg" "AllSet.dnsm" -na;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsOffset_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenter_M.iog" "AllSet.dsm" -na;
connectAttr "Center_MShape.iog" "AllSet.dsm" -na;
connectAttr "Center_M.iog" "AllSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "AllSet.dsm" -na;
connectAttr "CenterOffset_M.iog" "AllSet.dsm" -na;
connectAttr "IKdistanceLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKdistanceLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKmessureConstrainToLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_L.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_L.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Knee_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Ankle_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "MiddleToe1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_L.iog" "AllSet.dsm" -na;
connectAttr "effector10.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKLiftToeLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleToe_L.iog" "AllSet.dsm" -na;
connectAttr "effector9.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_LStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L.iog" "AllSet.dsm" -na;
connectAttr "effector8.iog" "AllSet.dsm" -na;
connectAttr "IKdistanceLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKdistanceLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKmessureConstrainToLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_R.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_R.iog" "AllSet.dsm" -na;
connectAttr "IKOrientToSpine_M.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Knee_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Ankle_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "MiddleToe1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "SpineA_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "SpineA_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Chest_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Chest_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Pipe2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Pipe1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Clavicle1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wrist_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "MiddleFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger3_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger5_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger6_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Head_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "Clavicle_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wrist2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wrist1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Barrel2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Barrel3_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToWrist1_R_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "Barrel1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "FKIKParentConstraintSpine_M.iog" "AllSet.dsm" -na;
connectAttr "FKIKSpine_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKSpine_M.iog" "AllSet.dsm" -na;
connectAttr "UnTwistEndPelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "UnTwistIKPelvis_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "UnTwistIKPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "effector7.iog" "AllSet.dsm" -na;
connectAttr "UnTwistEndPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "UnTwistPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "TwistFollowPelvis_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TwistFollowPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKXChest_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKSpine0AlignTo_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4AlignUnTwistTo_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4AlignUnTwistToOffset_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4AlignTo_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineA_M_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFake1UpLocSpine_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKFake1UpLocSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKEndJointOrientToSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKStiffEndOrientSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKStiffStartOrientSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4_M.iog" "AllSet.dsm" -na;
connectAttr "IKExtraSpine4_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine4_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator4_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator4_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator3_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator3_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine2_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKSpine2_M.iog" "AllSet.dsm" -na;
connectAttr "IKExtraSpine2_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine2_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator2_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator2_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator1_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator1_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine0_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKSpine0_M.iog" "AllSet.dsm" -na;
connectAttr "IKExtraSpine0_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine0_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator0_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator0_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineCurve_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineCurve_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineHandle_M.iog" "AllSet.dsm" -na;
connectAttr "effector6.iog" "AllSet.dsm" -na;
connectAttr "IKfake2Spine_M.iog" "AllSet.dsm" -na;
connectAttr "IKfake1Spine_M.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_R.iog" "AllSet.dsm" -na;
connectAttr "effector5.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKLiftToeLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleToe_R.iog" "AllSet.dsm" -na;
connectAttr "effector4.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_RStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R.iog" "AllSet.dsm" -na;
connectAttr "effector3.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintArm_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_LStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKXArmHandle_L_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_LStatic.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKXArmHandle_L.iog" "AllSet.dsm" -na;
connectAttr "effector2.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintArm1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintArm1_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKArm1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKArm1_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm1_RStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationArm1_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationArm1_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetArm1_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetArm1_R.iog" "AllSet.dsm" -na;
connectAttr "IKXArm1Handle_R_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleArm1_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleArm1_R.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraArm1_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm1_R.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist2_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm1_RStatic.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedArm1_R.iog" "AllSet.dsm" -na;
connectAttr "IKArm1_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKArm1_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraArm1_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm1_R.iog" "AllSet.dsm" -na;
connectAttr "IKXArm1Handle_R.iog" "AllSet.dsm" -na;
connectAttr "effector1.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerStabalizePelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerGroupPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerPelvis_MShape.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerOffsetPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKXHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "IKXHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKHip_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKXKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKXKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetKnee_L.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKXAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHeel_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe2_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe2_End_L.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "Hip_L.iog" "AllSet.dsm" -na;
connectAttr "Knee_L.iog" "AllSet.dsm" -na;
connectAttr "Ankle_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe2_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "IKParentConstraintShoulder1_R_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "IKXPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKXPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKXHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKHip_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKXKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKXKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetKnee_R.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKXAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHeel_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKXSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKSpineA_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "IKXChest_M.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKXChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKChest_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKXPipe2_R.iog" "AllSet.dsm" -na;
connectAttr "FKPipe2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKPipe2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPipe2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPipe2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXPipe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXPipe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKPipe1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKPipe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPipe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPipe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXPipe1_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXClavicle1_L.iog" "AllSet.dsm" -na;
connectAttr "FKClavicle1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKClavicle1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraClavicle1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetClavicle1_L.iog" "AllSet.dsm" -na;
connectAttr "IKXShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "IKXElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow_L.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKWrist_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHand_AP1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger3_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger4_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger5_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger5_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger5_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger5_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger5_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger6_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger6_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger6_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger6_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger6_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger7_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKHead_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHead_End_M.iog" "AllSet.dsm" -na;
connectAttr "FKXClavicle_R.iog" "AllSet.dsm" -na;
connectAttr "FKClavicle_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKClavicle_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraClavicle_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetClavicle_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToChest_M.iog" "AllSet.dsm" -na;
connectAttr "IKXShoulder1_R.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetShoulder1_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintShoulder1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder1_R.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder1_R.iog" "AllSet.dsm" -na;
connectAttr "IKXElbow1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow1_R.iog" "AllSet.dsm" -na;
connectAttr "FKElbow1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow1_R.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist2_R.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToWrist2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWrist2_R.iog" "AllSet.dsm" -na;
connectAttr "FKWrist2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWrist2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWrist2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWrist2_R.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "FKWrist1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXBarrel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKBarrel2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKBarrel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraBarrel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetBarrel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXBarrel2_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXBarrel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKBarrel3_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKBarrel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraBarrel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetBarrel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKXBarrel3_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXBarrel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKBarrel1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKBarrel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraBarrel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetBarrel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXBarrel1_End_R.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "Hip_R.iog" "AllSet.dsm" -na;
connectAttr "Knee_R.iog" "AllSet.dsm" -na;
connectAttr "Ankle_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "SpineA_M.iog" "AllSet.dsm" -na;
connectAttr "Chest_M.iog" "AllSet.dsm" -na;
connectAttr "Pipe2_R.iog" "AllSet.dsm" -na;
connectAttr "Pipe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "Pipe1_R.iog" "AllSet.dsm" -na;
connectAttr "Pipe1_End_R.iog" "AllSet.dsm" -na;
connectAttr "Clavicle1_L.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_L.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L.iog" "AllSet.dsm" -na;
connectAttr "Wrist_L.iog" "AllSet.dsm" -na;
connectAttr "Hand_AP1_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger4_End_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger5_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger6_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger7_End_L.iog" "AllSet.dsm" -na;
connectAttr "Head_M.iog" "AllSet.dsm" -na;
connectAttr "Head_End_M.iog" "AllSet.dsm" -na;
connectAttr "Clavicle_R.iog" "AllSet.dsm" -na;
connectAttr "Shoulder1_R.iog" "AllSet.dsm" -na;
connectAttr "Elbow1_R.iog" "AllSet.dsm" -na;
connectAttr "Wrist2_R.iog" "AllSet.dsm" -na;
connectAttr "Wrist1_R.iog" "AllSet.dsm" -na;
connectAttr "Barrel2_R.iog" "AllSet.dsm" -na;
connectAttr "Barrel2_End_R.iog" "AllSet.dsm" -na;
connectAttr "Barrel3_R.iog" "AllSet.dsm" -na;
connectAttr "Barrel3_End_R.iog" "AllSet.dsm" -na;
connectAttr "Barrel1_R.iog" "AllSet.dsm" -na;
connectAttr "Barrel1_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKMessure.iog" "AllSet.dsm" -na;
connectAttr "IKCrv.iog" "AllSet.dsm" -na;
connectAttr "IKStatic.iog" "AllSet.dsm" -na;
connectAttr "IKHandle.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraint.iog" "AllSet.dsm" -na;
connectAttr "TwistSystem.iog" "AllSet.dsm" -na;
connectAttr "RootSystem.iog" "AllSet.dsm" -na;
connectAttr "FKIKSystem.iog" "AllSet.dsm" -na;
connectAttr "IKSystem.iog" "AllSet.dsm" -na;
connectAttr "FKSystem.iog" "AllSet.dsm" -na;
connectAttr "GameSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MotionSystem.iog" "AllSet.dsm" -na;
connectAttr "BaseSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MainShape.iog" "AllSet.dsm" -na;
connectAttr "Main.iog" "AllSet.dsm" -na;
connectAttr "forwardArrowShape.iog" "AllSet.dsm" -na;
connectAttr "GameSet.msg" "Sets.dnsm" -na;
connectAttr "ControlSet.msg" "Sets.dnsm" -na;
connectAttr "AllSet.msg" "Sets.dnsm" -na;
connectAttr "IKArm1_R.follow" "IKArm1_RSetRangeFollow.vx";
connectAttr "IKArm1_R.follow" "IKArm1_RSetRangeFollow.vy";
connectAttr "PoleArm1_R.follow" "PoleArm1_RSetRangeFollow.vx";
connectAttr "PoleArm1_R.follow" "PoleArm1_RSetRangeFollow.vy";
connectAttr "FKIKArm1_R.FKIKBlend" "FKIKBlendArm1UnitConversion_R.i";
connectAttr "FKIKBlendArm1UnitConversion_R.o" "FKIKBlendArm1Reverse_R.ix";
connectAttr "FKIKArm1_R.autoVis" "FKIKBlendArm1Condition_R.ft";
connectAttr "FKIKArm1_R.IKVis" "FKIKBlendArm1Condition_R.ctr";
connectAttr "FKIKArm1_R.FKVis" "FKIKBlendArm1Condition_R.ctg";
connectAttr "FKIKArm1_R.FKIKBlend" "FKIKBlendArm1Condition_R.cfr";
connectAttr "FKIKBlendArm1setRange_R.ox" "FKIKBlendArm1Condition_R.cfg";
connectAttr "FKIKArm1_R.FKIKBlend" "FKIKBlendArm1setRange_R.vx";
connectAttr "IKArm_L.follow" "IKArm_LSetRangeFollow.vx";
connectAttr "IKArm_L.follow" "IKArm_LSetRangeFollow.vy";
connectAttr "PoleArm_L.follow" "PoleArm_LSetRangeFollow.vx";
connectAttr "PoleArm_L.follow" "PoleArm_LSetRangeFollow.vy";
connectAttr "FKIKArm_L.FKIKBlend" "FKIKBlendArmUnitConversion_L.i";
connectAttr "FKIKBlendArmUnitConversion_L.o" "FKIKBlendArmReverse_L.ix";
connectAttr "FKIKArm_L.autoVis" "FKIKBlendArmCondition_L.ft";
connectAttr "FKIKArm_L.IKVis" "FKIKBlendArmCondition_L.ctr";
connectAttr "FKIKArm_L.FKVis" "FKIKBlendArmCondition_L.ctg";
connectAttr "FKIKArm_L.FKIKBlend" "FKIKBlendArmCondition_L.cfr";
connectAttr "FKIKBlendArmsetRange_L.ox" "FKIKBlendArmCondition_L.cfg";
connectAttr "FKIKArm_L.FKIKBlend" "FKIKBlendArmsetRange_L.vx";
connectAttr "IKLeg_R.swivel" "unitConversion1.i";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vx";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vy";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegUnitConversion_R.i";
connectAttr "FKIKBlendLegUnitConversion_R.o" "FKIKBlendLegReverse_R.ix";
connectAttr "FKIKLeg_R.autoVis" "FKIKBlendLegCondition_R.ft";
connectAttr "FKIKLeg_R.IKVis" "FKIKBlendLegCondition_R.ctr";
connectAttr "FKIKLeg_R.FKVis" "FKIKBlendLegCondition_R.ctg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegCondition_R.cfr";
connectAttr "FKIKBlendLegsetRange_R.ox" "FKIKBlendLegCondition_R.cfg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegsetRange_R.vx";
connectAttr "IKLeg_R.toe" "IKLiftToeLegUnitConversion_R.i";
connectAttr "IKLeg_R.rollAngle" "Leg_RAngleReverse.i1x";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vx";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vy";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vz";
connectAttr "Leg_RAngleReverse.ox" "IKRollAngleLeg_R.nx";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.my";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.mz";
connectAttr "IKRollAngleLeg_R.ox" "unitConversion2.i";
connectAttr "IKRollAngleLeg_R.oy" "unitConversion3.i";
connectAttr "IKRollAngleLeg_R.oz" "unitConversion4.i";
connectAttr "unitConversion5.o" "IKBallToFKBallMiddleToe1blendTwoAttr_R.i[1]";
connectAttr "FKIKBlendLegUnitConversion_R.o" "IKBallToFKBallMiddleToe1blendTwoAttr_R.ab"
		;
connectAttr "IKXMiddleToe1_R.rx" "unitConversion5.i";
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_R.o" "unitConversion6.i";
connectAttr "IKSpine0_M.stiff" "IKStiffSpine0_M.vy";
connectAttr "IKSpine2_M.stiff" "IKStiffSpine2_M.vy";
connectAttr "IKSpine4_M.stiff" "IKStiffSpine4_M.vy";
connectAttr "IKXSpineCurve_MShape.ws" "IKCurveInfoSpine_M.ic";
connectAttr "IKCurveInfoSpine_M.al" "IKCurveInfoNormalizeSpine_M.i1y";
connectAttr "IKCurveInfoNormalizeSpine_M.oy" "IKCurveInfoAllMultiplySpine_M.i1y"
		;
connectAttr "Main.sy" "IKCurveInfoAllMultiplySpine_M.i2y";
connectAttr "IKSpine4_M.stretchy" "stretchySpineUnitConversion_M.i";
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineReverse_M.iy";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide0_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo0_M.ab";
connectAttr "stretchySpineMultiplyDivide0_M.oy" "stretchySpineBlendTwo0_M.i[1]";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide1_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo1_M.ab";
connectAttr "stretchySpineMultiplyDivide1_M.oy" "stretchySpineBlendTwo1_M.i[1]";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide2_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo2_M.ab";
connectAttr "stretchySpineMultiplyDivide2_M.oy" "stretchySpineBlendTwo2_M.i[1]";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide3_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo3_M.ab";
connectAttr "stretchySpineMultiplyDivide3_M.oy" "stretchySpineBlendTwo3_M.i[1]";
connectAttr "UnTwistEndPelvis_M.ry" "IKTwistSpineUnitConversion_M.i";
connectAttr "FKIKSpine_M.FKIKBlend" "FKIKBlendSpineUnitConversion_M.i";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "FKIKBlendSpineReverse_M.ix";
connectAttr "FKIKSpine_M.autoVis" "FKIKBlendSpineCondition_M.ft";
connectAttr "FKIKSpine_M.IKVis" "FKIKBlendSpineCondition_M.ctr";
connectAttr "FKIKSpine_M.FKVis" "FKIKBlendSpineCondition_M.ctg";
connectAttr "FKIKSpine_M.FKIKBlend" "FKIKBlendSpineCondition_M.cfr";
connectAttr "FKIKBlendSpinesetRange_M.ox" "FKIKBlendSpineCondition_M.cfg";
connectAttr "FKIKSpine_M.FKIKBlend" "FKIKBlendSpinesetRange_M.vx";
connectAttr "FKWrist1_R.s" "ScaleBlendWrist1_R.c2";
connectAttr "FKIKBlendArm1UnitConversion_R.o" "ScaleBlendWrist1_R.b";
connectAttr "FKWrist2_R.s" "ScaleBlendWrist2_R.c2";
connectAttr "FKIKBlendArm1UnitConversion_R.o" "ScaleBlendWrist2_R.b";
connectAttr "FKElbow1_R.s" "ScaleBlendElbow1_R.c2";
connectAttr "FKIKBlendArm1UnitConversion_R.o" "ScaleBlendElbow1_R.b";
connectAttr "FKShoulder1_R.s" "ScaleBlendShoulder1_R.c2";
connectAttr "FKIKBlendArm1UnitConversion_R.o" "ScaleBlendShoulder1_R.b";
connectAttr "FKWrist_L.s" "ScaleBlendWrist_L.c2";
connectAttr "FKIKBlendArmUnitConversion_L.o" "ScaleBlendWrist_L.b";
connectAttr "FKElbow_L.s" "ScaleBlendElbow_L.c2";
connectAttr "FKIKBlendArmUnitConversion_L.o" "ScaleBlendElbow_L.b";
connectAttr "FKShoulder_L.s" "ScaleBlendShoulder_L.c2";
connectAttr "FKIKBlendArmUnitConversion_L.o" "ScaleBlendShoulder_L.b";
connectAttr "FKChest_M.s" "ScaleBlendChest_M.c2";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "ScaleBlendChest_M.b";
connectAttr "FKSpineA_M.s" "ScaleBlendSpineA_M.c2";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "ScaleBlendSpineA_M.b";
connectAttr "FKAnkle_R.s" "ScaleBlendAnkle_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendAnkle_R.b";
connectAttr "FKKnee_R.s" "ScaleBlendKnee_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendKnee_R.b";
connectAttr "ScaleBlendAddYKnee_R.o1" "ScaleBlendKnee_R.c1g";
connectAttr "FKHip_R.s" "ScaleBlendHip_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendHip_R.b";
connectAttr "FKPelvis_M.s" "ScaleBlendPelvis_M.c2";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "ScaleBlendPelvis_M.b";
connectAttr "IKLeg_R.stretchy" "IKSetRangeStretchLeg_R.vy";
connectAttr "IKLeg_R.antiPop" "IKSetRangeAntiPopLeg_R.vy";
connectAttr "IKmessureBlendStretchLeg_R.o" "IKmessureDivLeg_R.i1y";
connectAttr "IKSetRangeAntiPopLeg_R.oy" "IKmessureBlendAntiPopLeg_R.ab";
connectAttr "IKdistanceLeg_RShape_normal.o" "IKmessureBlendAntiPopLeg_R.i[0]";
connectAttr "IKdistanceLeg_RShape_antiPop.o" "IKmessureBlendAntiPopLeg_R.i[1]";
connectAttr "IKdistanceLeg_RShape.dist" "IKdistanceLeg_RShape_antiPop.i";
connectAttr "IKdistanceLeg_RShape.dist" "IKdistanceLeg_RShape_normal.i";
connectAttr "IKmessureBlendAntiPopLeg_R.o" "IKdistanceClampLeg_R.ipr";
connectAttr "IKSetRangeStretchLeg_R.oy" "IKmessureBlendStretchLeg_R.ab";
connectAttr "IKdistanceClampLeg_R.opr" "IKmessureBlendStretchLeg_R.i[0]";
connectAttr "IKmessureBlendAntiPopLeg_R.o" "IKmessureBlendStretchLeg_R.i[1]";
connectAttr "IKmessureDivLeg_R.oy" "IKXKnee_R_IKmessureDiv_R.i1y";
connectAttr "IKXKnee_R_IKLength_R.oy" "IKXKnee_R_IKmessureDiv_R.i2y";
connectAttr "IKLeg_R.Length1" "IKXKnee_R_IKLength_R.i1y";
connectAttr "IKmessureDivLeg_R.oy" "IKXAnkle_R_IKmessureDiv_R.i1y";
connectAttr "IKXAnkle_R_IKLength_R.oy" "IKXAnkle_R_IKmessureDiv_R.i2y";
connectAttr "IKLeg_R.Length2" "IKXAnkle_R_IKLength_R.i1y";
connectAttr "IKLeg_R.Length2" "ScaleBlendAddYKnee_R.i1[0]";
connectAttr "IKmessureDivLeg_R.oy" "ScaleBlendAddYKnee_R.i1[1]";
connectAttr "IKLeg_L.swivel" "unitConversion7.i";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vx";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vy";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegUnitConversion_L.i";
connectAttr "FKIKBlendLegUnitConversion_L.o" "FKIKBlendLegReverse_L.ix";
connectAttr "FKIKLeg_L.autoVis" "FKIKBlendLegCondition_L.ft";
connectAttr "FKIKLeg_L.IKVis" "FKIKBlendLegCondition_L.ctr";
connectAttr "FKIKLeg_L.FKVis" "FKIKBlendLegCondition_L.ctg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegCondition_L.cfr";
connectAttr "FKIKBlendLegsetRange_L.ox" "FKIKBlendLegCondition_L.cfg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegsetRange_L.vx";
connectAttr "IKLeg_L.toe" "IKLiftToeLegUnitConversion_L.i";
connectAttr "IKLeg_L.rollAngle" "Leg_LAngleReverse.i1x";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vx";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vy";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vz";
connectAttr "Leg_LAngleReverse.ox" "IKRollAngleLeg_L.nx";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.my";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.mz";
connectAttr "IKRollAngleLeg_L.ox" "unitConversion8.i";
connectAttr "IKRollAngleLeg_L.oy" "unitConversion9.i";
connectAttr "IKRollAngleLeg_L.oz" "unitConversion10.i";
connectAttr "unitConversion11.o" "IKBallToFKBallMiddleToe1blendTwoAttr_L.i[1]";
connectAttr "FKIKBlendLegUnitConversion_L.o" "IKBallToFKBallMiddleToe1blendTwoAttr_L.ab"
		;
connectAttr "IKXMiddleToe1_L.rx" "unitConversion11.i";
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_L.o" "unitConversion12.i";
connectAttr "FKAnkle_L.s" "ScaleBlendAnkle_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendAnkle_L.b";
connectAttr "FKKnee_L.s" "ScaleBlendKnee_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendKnee_L.b";
connectAttr "ScaleBlendAddYKnee_L.o1" "ScaleBlendKnee_L.c1g";
connectAttr "FKHip_L.s" "ScaleBlendHip_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendHip_L.b";
connectAttr "IKLeg_L.stretchy" "IKSetRangeStretchLeg_L.vy";
connectAttr "IKLeg_L.antiPop" "IKSetRangeAntiPopLeg_L.vy";
connectAttr "IKmessureBlendStretchLeg_L.o" "IKmessureDivLeg_L.i1y";
connectAttr "IKSetRangeAntiPopLeg_L.oy" "IKmessureBlendAntiPopLeg_L.ab";
connectAttr "IKdistanceLeg_LShape_normal.o" "IKmessureBlendAntiPopLeg_L.i[0]";
connectAttr "IKdistanceLeg_LShape_antiPop.o" "IKmessureBlendAntiPopLeg_L.i[1]";
connectAttr "IKdistanceLeg_LShape.dist" "IKdistanceLeg_LShape_antiPop.i";
connectAttr "IKdistanceLeg_LShape.dist" "IKdistanceLeg_LShape_normal.i";
connectAttr "IKmessureBlendAntiPopLeg_L.o" "IKdistanceClampLeg_L.ipr";
connectAttr "IKSetRangeStretchLeg_L.oy" "IKmessureBlendStretchLeg_L.ab";
connectAttr "IKdistanceClampLeg_L.opr" "IKmessureBlendStretchLeg_L.i[0]";
connectAttr "IKmessureBlendAntiPopLeg_L.o" "IKmessureBlendStretchLeg_L.i[1]";
connectAttr "IKmessureDivLeg_L.oy" "IKXKnee_L_IKmessureDiv_L.i1y";
connectAttr "IKXKnee_L_IKLength_L.oy" "IKXKnee_L_IKmessureDiv_L.i2y";
connectAttr "IKLeg_L.Length1" "IKXKnee_L_IKLength_L.i1y";
connectAttr "IKmessureDivLeg_L.oy" "IKXAnkle_L_IKmessureDiv_L.i1y";
connectAttr "IKXAnkle_L_IKLength_L.oy" "IKXAnkle_L_IKmessureDiv_L.i2y";
connectAttr "IKLeg_L.Length2" "IKXAnkle_L_IKLength_L.i1y";
connectAttr "IKLeg_L.Length2" "ScaleBlendAddYKnee_L.i1[0]";
connectAttr "IKmessureDivLeg_L.oy" "ScaleBlendAddYKnee_L.i1[1]";
connectAttr "Center_M.CenterBtwFeet" "CenterBtwFeet_M.vx";
connectAttr "Center_M.CenterBtwFeet" "PelvisCenterBtwFeetUnitConversion_M.i";
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwFeetReverse_M.ix"
		;
connectAttr "irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "BPRig_irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ikSCsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikSplineSolver.msg" ":ikSystem.sol" -na;
// End of Berserker__rig.ma
