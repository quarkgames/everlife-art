//Maya ASCII 2013 scene
//Name: Medic__rig.ma
//Last modified: Fri, May 02, 2014 12:22:59 PM
//Codeset: UTF-8
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -13.762563062386128 14.320853044099014 32.911064944643968 ;
	setAttr ".r" -type "double3" -14.738352729781354 692.59999999989384 0 ;
	setAttr ".rp" -type "double3" 2.2204460492503131e-16 8.8817841970012523e-16 -3.5527136788005009e-15 ;
	setAttr ".rpt" -type "double3" -4.4415085609408998e-15 -3.6515745886902187e-16 1.8406139064308405e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 37.471077046554527;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -1.6560905195017757 3.8635805047621026 0.78480103088543629 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.72088652048480428 100.1 -2.297825784045314 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 21.196691130875202;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "ctrl_rig";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Main" -p "ctrl_rig";
	addAttr -ci true -sn "height" -ln "height" -at "double";
	addAttr -ci true -sn "fkVis" -ln "fkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "ikVis" -ln "ikVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fkIkVis" -ln "fkIkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimVis" -ln "aimVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "eyeVis" -ln "eyeVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fingerVis" -ln "fingerVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "bendVis" -ln "bendVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "arrowVis" -ln "arrowVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "jointVis" -ln "jointVis" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -l on ".height" 12.606920638663622;
	setAttr -cb on ".fkVis";
	setAttr -cb on ".ikVis";
	setAttr -cb on ".fkIkVis";
	setAttr -cb on ".aimVis";
	setAttr -cb on ".eyeVis";
	setAttr -cb on ".fingerVis";
	setAttr -cb on ".bendVis";
	setAttr -cb on ".arrowVis";
	setAttr -cb on ".jointVis";
createNode nurbsCurve -n "MainShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.9928279660000001 -0.24952640810000001 -5.938955226
		-1.3561676610000001e-15 -0.24952640810000001 -8.4212658460000007
		-5.9928279660000001 -0.24952640810000001 -5.938955226
		-8.4751385849999998 -0.24952640810000001 0.053872739330000002
		-5.9928279660000001 -0.24952640810000001 6.0467007050000001
		-2.942980343e-15 -0.24952640810000001 8.5290113250000008
		5.9928279660000001 -0.24952640810000001 6.0467007050000001
		8.4751385849999998 -0.24952640810000001 0.053872739330000002
		5.9928279660000001 -0.24952640810000001 -5.938955226
		-1.3561676610000001e-15 -0.24952640810000001 -8.4212658460000007
		-5.9928279660000001 -0.24952640810000001 -5.938955226
		;
createNode transform -n "BaseSkeleton" -p "Main";
	setAttr -l on ".v" no;
createNode transform -n "BaseSkeletonOffset" -p "BaseSkeleton";
createNode joint -n "Pelvis" -p "BaseSkeletonOffset";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0 3.9898529396578266 -0.31454308073907233 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 1;
createNode joint -n "Body" -p "Pelvis";
	setAttr ".t" -type "double3" 0 0.88276794381776913 5.5511151231257827e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".otp" -type "string" "Mid";
createNode joint -n "Head" -p "Body";
	setAttr ".t" -type "double3" 0 5.3531723602299 0.0036960628921906546 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "38";
createNode joint -n "Head_End" -p "Head";
	setAttr ".t" -type "double3" -6.2302109906456059e-17 2.381127394958126 -4.4408920985006262e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".otp" -type "string" "Chest";
createNode joint -n "Shoulder" -p "Body";
	addAttr -ci true -k true -sn "global" -ln "global" -dv 10 -min 0 -max 10 -at "long";
	addAttr -ci true -sn "globalConnect" -ln "globalConnect" -dv 10 -min 0 -max 10 -at "long";
	setAttr ".t" -type "double3" -3.6796203529496982 4.237709675283666 -0.26137462492501046 ;
	setAttr ".r" -type "double3" -90.324097215275856 -58.132997593641655 65.876462716014203 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
	setAttr ".dl" yes;
	setAttr ".sd" 3;
	setAttr -k on ".global";
createNode transform -n "Shoulder_globalLocator" -p "Shoulder";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -3.9756933518293969e-16 0 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "Shoulder_globalLocatorShape" -p "Shoulder_globalLocator";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode joint -n "Elbow5" -p "Shoulder";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 3.8569282443925959 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 2.455802674176053 -7.8957069564397084 79.324385881340135 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "Wrist1" -p "Elbow5";
	setAttr ".t" -type "double3" 8.3266726846886741e-17 1.346593817123479 2.6645352591003757e-15 ;
	setAttr ".r" -type "double3" -5.5840391590728666 1.5659644381906448 -7.60384175031 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "Elbow4" -p "Wrist1";
	setAttr ".t" -type "double3" 9.9920072216264089e-16 1.9711328197031144 -2.6645352591003757e-15 ;
	setAttr ".r" -type "double3" -7.3747870741251536 -12.776491519811229 -32.747662000146974 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 7.3747870741254093 12.776491519811406 32.747662000147031 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "Shoulder1" -p "Body";
	addAttr -ci true -k true -sn "global" -ln "global" -dv 10 -min 0 -max 10 -at "long";
	addAttr -ci true -sn "globalConnect" -ln "globalConnect" -dv 10 -min 0 -max 10 -at "long";
	setAttr ".t" -type "double3" 3.67962 4.2377091165244032 -0.26137491926092776 ;
	setAttr ".r" -type "double3" 89.675959042087442 -58.132943459864947 -114.1235121261734 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
	setAttr -k on ".global";
createNode transform -n "Shoulder1_globalLocator" -p "Shoulder1";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -1.7763568394002505e-15 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 1.1927080055488188e-15 3.1805546814635168e-15 6.3611093629270335e-15 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "Shoulder1_globalLocatorShape" -p "Shoulder1_globalLocator";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode joint -n "Elbow" -p "Shoulder1";
	setAttr ".t" -type "double3" 2.2204460492503131e-15 3.8569249983301481 0 ;
	setAttr ".r" -type "double3" -3.5631951490966083 2.0131587495129413 79.707968020673974 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "Wrist" -p "Elbow";
	setAttr ".t" -type "double3" -1.3877787807814457e-16 0.8326449618597942 1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.8683496313300023e-13 3.8763010180336599e-14 179.99999999999969 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "MiddleFinger1" -p "Wrist";
	setAttr ".t" -type "double3" -0.061912811721444382 -3.4542914752233909 1.0264096342578446 ;
	setAttr ".r" -type "double3" -179.29338354712121 -21.171634238648004 30.00599174283191 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -2.490303168013669e-17 3.8068719241856415 -4.0949047407001542 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "21";
createNode joint -n "MiddleFinger2" -p "MiddleFinger1";
	setAttr ".t" -type "double3" 0 0.4133331343143376 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -24.690176214642342 1.4489558802374185 0.88319718929368018 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0 0 2.5199999009299203 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "20";
createNode joint -n "MiddleFinger3" -p "MiddleFinger2";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0.90189613135496893 0 ;
	setAttr ".r" -type "double3" -26.383751583498853 -2.4869181064952111 2.5129275835259306 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0 0 3.6712939054552742 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "19";
createNode joint -n "MiddleFinger4_End" -p "MiddleFinger3";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 1.1257201477021035 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 1.3517357396219939e-13 -6.361109362927032e-15 -179.99999999999991 ;
	setAttr ".pa" -type "double3" 0 0 3.6712939054552742 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "19";
createNode joint -n "IndexFinger1" -p "Wrist";
	setAttr ".t" -type "double3" 0.76493398944445889 -3.2962525842519952 0.96784838689042907 ;
	setAttr ".r" -type "double3" -175.58486515841929 32.769305879832643 -25.833195800983141 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0.065532877363568762 20.527688987272207 -2.5422327562497964 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "8";
createNode joint -n "IndexFinger2" -p "IndexFinger1";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0.44186264890330518 2.6645352591003757e-15 ;
	setAttr ".r" -type "double3" -26.585680015306881 -4.5354321105553357 -1.1032517067074992 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "7";
createNode joint -n "IndexFinger3" -p "IndexFinger2";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0.88370494679700462 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -31.032122452033462 -0.04225098532445451 0.051105397479917138 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0 0 5.7600000490223469 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "6";
createNode joint -n "IndexFinger4_End" -p "IndexFinger3";
	setAttr ".t" -type "double3" -1.3322676295501878e-15 1.1571989645814331 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 1.6220828875463939e-13 -2.4132458645604436e-12 179.99999999999997 ;
	setAttr ".pa" -type "double3" 0 0 5.7600000490223469 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "6";
createNode joint -n "ThumbFinger1" -p "Wrist";
	setAttr ".t" -type "double3" 0.56809760079276805 -2.0282457218782528 1.8256326612627447 ;
	setAttr ".r" -type "double3" -73.130612545835092 28.025537200539507 -152.54062283743363 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -34.462082586865911 -8.7285733235282201 -1.7903981777634761 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "4";
createNode joint -n "ThumbFinger2" -p "ThumbFinger1";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0.52153996925717383 3.3306690738754696e-16 ;
	setAttr ".r" -type "double3" -24.910132565964854 0.41894141812200442 -3.1009961646291826 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "3";
createNode joint -n "ThumbFinger3" -p "ThumbFinger2";
	setAttr ".t" -type "double3" 0 0.8354786607393323 -1.3322676295501878e-15 ;
	setAttr ".r" -type "double3" -30.62601645969044 -5.8067556195080847 -5.9833542377370179 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "2";
createNode joint -n "ThumbFinger4_End" -p "ThumbFinger3";
	setAttr ".t" -type "double3" 0 1.0809764280246548 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 2.1468744099878747e-13 1.0813885916975959e-13 -179.99999999999963 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "2";
createNode joint -n "PinkyFinger1" -p "Wrist";
	setAttr ".t" -type "double3" -0.68886327801829128 -2.9794436955777557 1.2606811180138753 ;
	setAttr ".r" -type "double3" -144.7798460560102 -35.495310555759794 90.565665653768605 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0.065532877363568762 20.527688987272207 -2.5422327562497964 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "8";
createNode joint -n "PinkyFinger2" -p "PinkyFinger1";
	setAttr ".t" -type "double3" 2.6645352591003757e-15 0.37095051782777233 -1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -24.814776966112749 -4.0509373563737352 -3.2722068873244288 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "7";
createNode joint -n "PinkyFinger3" -p "PinkyFinger2";
	setAttr ".t" -type "double3" 0 0.80315171109900341 0 ;
	setAttr ".r" -type "double3" -24.027853494694636 -0.28744091163072838 0.520759257735669 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0 0 5.7600000490223469 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "6";
createNode joint -n "PinkyFinger4_End" -p "PinkyFinger3";
	setAttr ".t" -type "double3" 0 0.99669641313246837 -6.6613381477509392e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 2.7352770260586251e-13 4.8980542094538183e-12 -179.99999999999986 ;
	setAttr ".pa" -type "double3" 0 0 5.7600000490223469 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "6";
createNode joint -n "Hose1" -p "Body";
	setAttr ".t" -type "double3" -2.1689490945289349 -1.0831980499384857 -4.9349946771425932 ;
	setAttr ".r" -type "double3" -90.000000000000028 0 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
createNode joint -n "Hose1_End" -p "Hose1";
	setAttr ".t" -type "double3" 0 1.2444681704849083 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 90.000000000000028 0 0 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
createNode joint -n "Hose2" -p "Body";
	setAttr ".t" -type "double3" -6.3782461254304437 -2.2020769169102503 -3.3864248464098754 ;
	setAttr ".r" -type "double3" -90.000000000000028 0 0 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
createNode joint -n "Hose2_End" -p "Hose2";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.2444681704849083 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 90.000000000000028 0 0 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
createNode joint -n "Track" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.65512380987240437 4.4408920985006262e-16 0 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 26.58016788997671 -0.0012779026280242865 179.99838883409456 ;
	setAttr ".dl" yes;
	setAttr ".typ" 2;
createNode joint -n "TrackBend" -p "Track";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 1.2425429238991419 0.061160511392438721 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -59.506830963345188 -0.00049904337143792828 -0.00096881069896128822 ;
createNode joint -n "TrackBottom" -p "TrackBend";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 1.1102230246251563e-16 1.3517839773230285 -0.040242726398744377 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 32.926663073431577 -0.00033961572557010333 0.0029978253432159214 ;
	setAttr ".dl" yes;
	setAttr ".typ" 4;
createNode joint -n "Wheel1" -p "TrackBottom";
	setAttr ".t" -type "double3" 0.32672497678868728 0.51822117956943803 3.7376068350208902 ;
	setAttr ".r" -type "double3" 5.374345420446291e-05 -5.3743454204463053e-05 -90.000000000000185 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "Wheel1_End" -p "Wheel1";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.4116922076074294 0 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.3743454353948979e-05 -1.7689481217039169e-10 -89.999999999999844 ;
createNode joint -n "Wheel2" -p "TrackBottom";
	setAttr ".t" -type "double3" 0.13546603245041722 0.078356787930579305 1.3998736841503581 ;
	setAttr ".r" -type "double3" 5.3743454123354633e-05 -5.3743454123354667e-05 -90.000000000000071 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "Wheel2_End" -p "Wheel2";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 1.4116922076074301 1.1102230246251563e-16 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.3743454272840729e-05 -1.7681370379931027e-10 -89.999999999999929 ;
createNode joint -n "Wheel3" -p "TrackBottom";
	setAttr ".t" -type "double3" 0.13546856143442221 0.078356787930578653 -1.2962795322063323 ;
	setAttr ".r" -type "double3" 5.3743454177426838e-05 -5.3743454177426845e-05 -90.000000000000014 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "Wheel3_End" -p "Wheel3";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 1.411692207607429 2.2204460492503131e-16 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.3743454326912914e-05 -1.7686777595938537e-10 -89.999999999999986 ;
createNode joint -n "Wheel4" -p "TrackBottom";
	setAttr ".t" -type "double3" 0.38140015290258034 0.59898164392295494 -3.6256848883138328 ;
	setAttr ".r" -type "double3" 5.3743453970149819e-05 -5.3743453970149894e-05 -90.000000000000071 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "Wheel4_End" -p "Wheel4";
	setAttr ".t" -type "double3" 1.3322676295501878e-15 1.4116922076074347 8.8817841970012523e-16 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.3743454119635888e-05 -1.7666049900349566e-10 -89.999999999999929 ;
createNode joint -n "FrontTrack_End" -p "TrackBottom";
	setAttr ".t" -type "double3" 0.22774006241858524 1.67853593574611 4.6938822643168114 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.3743454262035001e-05 0 -90.000000000000071 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Ball";
createNode joint -n "BackTrack_End" -p "TrackBottom";
	setAttr ".t" -type "double3" 0.22774374126649119 1.5483447246245761 -5.1362642985384479 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.3743454207965554e-05 0 -90.000000000000014 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Heel";
createNode transform -n "MotionSystem" -p "Main";
createNode transform -n "FKSystem" -p "MotionSystem";
createNode transform -n "FKParentConstraintToTrackBottom_R" -p "FKSystem";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000009 1 1 ;
createNode joint -n "FKOffsetWheel1_R" -p "FKParentConstraintToTrackBottom_R";
	setAttr ".t" -type "double3" 0.32672497678868828 0.51822117956942892 3.7376068350208911 ;
	setAttr ".r" -type "double3" 5.3743455807798471e-05 -5.3743455807798471e-05 -90.000002504478161 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraWheel1_R" -p "FKOffsetWheel1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 -2.2204460492503131e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999967 1 ;
createNode transform -n "FKWheel1_R" -p "FKExtraWheel1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 -1.1102230246251563e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel1_RShape" -p "FKWheel1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.520426718 4.3070183569999996e-16 -1.520426718
		-2.1502080860000001 4.7744210500000004e-16 0
		-1.520426718 2.4450326429999997e-16 1.520426718
		-6.2307650939999999e-16 -1.3166227249999999e-16 2.1502080860000001
		1.520426718 -4.3070183569999996e-16 1.520426718
		2.1502080860000001 -4.7744210500000004e-16 0
		1.520426718 -2.4450326429999997e-16 -1.520426718
		1.1548809579999999e-15 1.3166227249999999e-16 -2.1502080860000001
		-1.520426718 4.3070183569999996e-16 -1.520426718
		-2.1502080860000001 4.7744210500000004e-16 0
		-1.520426718 2.4450326429999997e-16 1.520426718
		;
createNode joint -n "FKXWheel1_R" -p "FKWheel1_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel1_End_R" -p "FKXWheel1_R";
	setAttr ".t" -type "double3" 0 1.411692207607429 4.3582915054685145e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743454211479636e-05 3.3123855586690703e-20 -89.999999999999815 ;
createNode joint -n "FKOffsetWheel2_R" -p "FKParentConstraintToTrackBottom_R";
	setAttr ".t" -type "double3" 0.13546603245041755 0.078356787930575988 1.3998736841503581 ;
	setAttr ".r" -type "double3" 5.3743452550910459e-05 -5.3743452550910459e-05 -90.000002504478161 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraWheel2_R" -p "FKOffsetWheel2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0 -2.2204460492503131e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999956 1 ;
createNode transform -n "FKWheel2_R" -p "FKExtraWheel2_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel2_RShape" -p "FKWheel2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.520426718 1.644599071e-15 -1.520426718
		-2.1502080860000001 1.6913393400000001e-15 -1.213897235e-15
		-1.520426718 1.458400499e-15 1.520426718
		-6.2307650939999999e-16 1.082234962e-15 2.1502080860000001
		1.520426718 7.8319539929999996e-16 1.520426718
		2.1502080860000001 7.3645512999999993e-16 -1.213897235e-15
		1.520426718 9.6939397059999995e-16 -1.520426718
		1.1548809579999999e-15 1.3455595069999997e-15 -2.1502080860000001
		-1.520426718 1.644599071e-15 -1.520426718
		-2.1502080860000001 1.6913393400000001e-15 -1.213897235e-15
		-1.520426718 1.458400499e-15 1.520426718
		;
createNode joint -n "FKXWheel2_R" -p "FKWheel2_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel2_End_R" -p "FKXWheel2_R";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 1.4116922076074294 4.3565151486291143e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.374345413037138e-05 1.9196319730530409e-20 -89.999999999999929 ;
createNode joint -n "FKOffsetWheel3_R" -p "FKParentConstraintToTrackBottom_R";
	setAttr ".t" -type "double3" 0.13546856143442201 0.078356787930582206 -1.2962795322063321 ;
	setAttr ".r" -type "double3" 5.3743452550910459e-05 -5.3743452550910459e-05 -90.000002504478161 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraWheel3_R" -p "FKOffsetWheel3_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -1.1102230246251563e-16 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1 ;
createNode transform -n "FKWheel3_R" -p "FKExtraWheel3_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel3_RShape" -p "FKWheel3_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.520426718 4.3070183569999996e-16 -1.520426718
		-2.1502080860000001 4.7744210500000004e-16 0
		-1.520426718 2.4450326429999997e-16 1.520426718
		-1.2300251269999999e-15 -1.3166227249999999e-16 2.1502080860000001
		1.520426718 -4.3070183569999996e-16 1.520426718
		2.1502080860000001 -4.7744210500000004e-16 0
		1.520426718 -2.4450326429999997e-16 -1.520426718
		5.4793234019999999e-16 1.3166227249999999e-16 -2.1502080860000001
		-1.520426718 4.3070183569999996e-16 -1.520426718
		-2.1502080860000001 4.7744210500000004e-16 0
		-1.520426718 2.4450326429999997e-16 1.520426718
		;
createNode joint -n "FKXWheel3_R" -p "FKWheel3_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel3_End_R" -p "FKXWheel3_R";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 1.411692207607429 4.3580694608635895e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743454184443564e-05 1.2531665462661668e-20 -89.999999999999972 ;
createNode joint -n "FKOffsetWheel4_R" -p "FKParentConstraintToTrackBottom_R";
	setAttr ".t" -type "double3" 0.38140015290257945 0.59898164392296449 -3.6256848883138311 ;
	setAttr ".r" -type "double3" 5.3743452550910459e-05 -5.3743452550910459e-05 -90.000002504478161 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraWheel4_R" -p "FKOffsetWheel4_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0 8.8817841970012523e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.999999999999999 1 ;
createNode transform -n "FKWheel4_R" -p "FKExtraWheel4_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel4_RShape" -p "FKWheel4_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.520426718 -7.8319539929999996e-16 -1.520426718
		-2.1502080860000001 -7.3645512999999993e-16 6.0694861749999998e-16
		-1.520426718 -9.6939397059999995e-16 1.520426718
		-1.2300251269999999e-15 -1.3455595069999997e-15 2.1502080860000001
		1.520426718 -1.644599071e-15 1.520426718
		2.1502080860000001 -1.6913393400000001e-15 6.0694861749999998e-16
		1.520426718 -1.458400499e-15 -1.520426718
		5.4793234019999999e-16 -1.082234962e-15 -2.1502080860000001
		-1.520426718 -7.8319539929999996e-16 -1.520426718
		-2.1502080860000001 -7.3645512999999993e-16 6.0694861749999998e-16
		-1.520426718 -9.6939397059999995e-16 1.520426718
		;
createNode joint -n "FKXWheel4_R" -p "FKWheel4_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel4_End_R" -p "FKXWheel4_R";
	setAttr ".t" -type "double3" 1.5543122344752192e-15 1.411692207607435 4.3538506133700139e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743453977166532e-05 -5.0693865924666912e-21 -89.999999999999929 ;
createNode parentConstraint -n "FKParentConstraintToTrackBottom_R_parentConstraint1" 
		-p "FKParentConstraintToTrackBottom_R";
	addAttr -ci true -k true -sn "w0" -ln "TrackBottom_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.791378037353395e-06 -5.3743131034998827e-05 -179.99999999992718 ;
	setAttr ".rst" -type "double3" -0.65521043624401587 1.7932333329704833 -0.47243241678336567 ;
	setAttr ".rsrr" -type "double3" -2.544443745170814e-14 -5.3743277289625293e-05 -179.99999999999997 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToTrackBottom_L" -p "FKSystem";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000009 1 1.0000000000000009 ;
createNode joint -n "FKOffsetWheel1_L" -p "FKParentConstraintToTrackBottom_L";
	setAttr ".t" -type "double3" -0.32672497678869172 -0.51822117956942826 -3.7376068350208898 ;
	setAttr ".r" -type "double3" 5.3743455807798471e-05 -5.3743455807798471e-05 -90.000002504478161 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraWheel1_L" -p "FKOffsetWheel1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 4.4408920985006262e-16 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 7.0165169956657107e-15 -2.4265706491930858e-20 1.9083328088781101e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999989 ;
createNode transform -n "FKWheel1_L" -p "FKExtraWheel1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.1102230246251563e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel1_LShape" -p "FKWheel1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.520426718000415 2.5930368963145156e-12 1.5204267180000033
		2.1502080860004154 3.3930636078594034e-12 5.3290705182007514e-15
		1.5204267180004152 2.592259740197278e-12 -1.5204267179999933
		4.1566750041965861e-13 6.5969452123226802e-13 -2.150208085999993
		-1.5204267179995852 -1.2724266085228919e-12 -1.5204267179999942
		-2.1502080859995858 -2.0723422977653172e-12 4.4408920985006262e-15
		-1.5204267179995852 -1.2715384301031918e-12 1.5204267180000033
		4.1366909897533333e-13 6.6091576655935569e-13 2.1502080860000028
		1.520426718000415 2.5930368963145156e-12 1.5204267180000033
		2.1502080860004154 3.3930636078594034e-12 5.3290705182007514e-15
		1.5204267180004152 2.592259740197278e-12 -1.5204267179999933
		;
createNode joint -n "FKXWheel1_L" -p "FKWheel1_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel1_End_L" -p "FKXWheel1_L";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -1.4116922076074294 -4.3569592378389643e-12 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -179.9999462565458 -3.0752887916177288e-21 -89.999999999999815 ;
createNode joint -n "FKOffsetWheel2_L" -p "FKParentConstraintToTrackBottom_L";
	setAttr ".t" -type "double3" -0.13546603245041866 -0.078356787930575766 -1.3998736841503572 ;
	setAttr ".r" -type "double3" 5.3743452550910459e-05 -5.3743452550910459e-05 -90.000002504478161 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraWheel2_L" -p "FKOffsetWheel2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -1.1102230246251563e-16 0 ;
	setAttr ".r" -type "double3" 7.0166474238381126e-15 -3.336534642801159e-20 -2.0430202193699005e-36 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999956 0.99999999999999967 ;
createNode transform -n "FKWheel2_L" -p "FKExtraWheel2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -1.1102230246251563e-16 -1.1102230246251563e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel2_LShape" -p "FKWheel2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.5204267180001718 2.031486090459111e-12 1.5204267180000015
		2.150208086000172 2.8315128020039992e-12 3.6637359812630166e-15
		1.520426718000172 2.0307089343418734e-12 -1.5204267179999968
		1.7275070263167436e-13 9.8254737679326354e-14 -2.1502080859999975
		-1.520426717999829 -1.8338663920758336e-12 -1.5204267179999973
		-2.1502080859998292 -2.6337820813182589e-12 3.219646771412954e-15
		-1.520426717999829 -1.8332002582610585e-12 1.5204267180000015
		1.7030821197749899e-13 9.9364960703951523e-14 2.1502080860000015
		1.5204267180001718 2.031486090459111e-12 1.5204267180000015
		2.150208086000172 2.8315128020039992e-12 3.6637359812630166e-15
		1.520426718000172 2.0307089343418734e-12 -1.5204267179999968
		;
createNode joint -n "FKXWheel2_L" -p "FKWheel2_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel2_End_L" -p "FKXWheel2_L";
	setAttr ".t" -type "double3" 0 -1.4116922076074294 -4.3560710594192642e-12 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -179.99994625654588 -1.1135807189663507e-20 -89.999999999999929 ;
createNode joint -n "FKOffsetWheel3_L" -p "FKParentConstraintToTrackBottom_L";
	setAttr ".t" -type "double3" -0.13546856143442088 -0.078356787930582206 1.2962795322063314 ;
	setAttr ".r" -type "double3" 5.3743452550910459e-05 -5.3743452550910459e-05 -90.000002504478161 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraWheel3_L" -p "FKOffsetWheel3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 2.2204460492503131e-16 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 7.0166838223978519e-15 -4.2464986362923846e-20 -2.6002210403912851e-36 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999933 0.99999999999999933 ;
createNode transform -n "FKWheel3_L" -p "FKExtraWheel3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -1.1102230246251563e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel3_LShape" -p "FKWheel3_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.5204267180001707 2.0310420012492618e-12 1.520426718
		2.1502080860001711 2.8310687127941492e-12 8.8817841970012523e-16
		1.5204267180001707 2.0303758674344863e-12 -1.5204267179999993
		1.7208456881689926e-13 9.7810648469476291e-14 -2.1502080859999992
		-1.5204267179998296 -1.8343104812856836e-12 -1.5204267179999993
		-2.1502080859998296 -2.6342261705281089e-12 2.2204460492503131e-16
		-1.5204267179998301 -1.8334223028659835e-12 1.520426718
		1.7008616737257398e-13 9.9031893796563963e-14 2.150208086000001
		1.5204267180001707 2.0310420012492618e-12 1.520426718
		2.1502080860001711 2.8310687127941492e-12 8.8817841970012523e-16
		1.5204267180001707 2.0303758674344863e-12 -1.5204267179999993
		;
createNode joint -n "FKXWheel3_L" -p "FKWheel3_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel3_End_L" -p "FKXWheel3_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -1.4116922076074292 -4.3587355946783646e-12 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -179.99994625654583 -1.1734034857690794e-20 -89.999999999999972 ;
createNode joint -n "FKOffsetWheel4_L" -p "FKParentConstraintToTrackBottom_L";
	setAttr ".t" -type "double3" -0.38140015290257617 -0.59898164392296493 3.625684888313828 ;
	setAttr ".r" -type "double3" 5.3743452550910459e-05 -5.3743452550910459e-05 -90.000002504478161 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraWheel4_L" -p "FKOffsetWheel4_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 2.2204460492503131e-16 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 7.0166474238381126e-15 -6.0664266232748351e-20 -3.7145822170361816e-36 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.999999999999999 0.99999999999999922 ;
createNode transform -n "FKWheel4_L" -p "FKExtraWheel4_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel4_LShape" -p "FKWheel4_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.5204267180004831 2.6920687901110796e-12 1.5204267179999986
		2.1502080860004833 3.4920955016559674e-12 -8.8817841970012523e-16
		1.5204267180004831 2.6911806116913795e-12 -1.5204267180000004
		4.8450132794641831e-13 7.5894845963375691e-13 -2.150208086000001
		-1.5204267179995172 -1.1732836924238654e-12 -1.5204267180000008
		-2.1502080859995174 -1.9733104039687532e-12 -8.8817841970012523e-16
		-1.5204267179995172 -1.1723955140041653e-12 1.5204267179999986
		4.82280881897168e-13 7.6005868265838227e-13 2.1502080859999992
		1.5204267180004831 2.6920687901110796e-12 1.5204267179999986
		2.1502080860004833 3.4920955016559674e-12 -8.8817841970012523e-16
		1.5204267180004831 2.6911806116913795e-12 -1.5204267180000004
		;
createNode joint -n "FKXWheel4_L" -p "FKWheel4_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel4_End_L" -p "FKXWheel4_L";
	setAttr ".t" -type "double3" -1.5543122344752192e-15 -1.4116922076074354 -4.3529624349503138e-12 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -179.99994625654602 -1.11358070484185e-20 -89.999999999999929 ;
createNode parentConstraint -n "FKParentConstraintToTrackBottom_L_parentConstraint1" 
		-p "FKParentConstraintToTrackBottom_L";
	addAttr -ci true -k true -sn "w0" -ln "TrackBottom_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.7913780436805577e-06 -179.99994625686898 7.2802923709969246e-11 ;
	setAttr ".rst" -type "double3" 0.65521043624401609 1.7932333329704846 -0.47243241678336351 ;
	setAttr ".rsrr" -type "double3" -1.9083326890483422e-14 -179.99994625672272 -2.5550179838312065e-15 ;
	setAttr -k on ".w0";
createNode transform -n "PelvisCenterBtwLegsBlended_M" -p "FKSystem";
	setAttr ".ro" 3;
createNode transform -n "CenterOffset_M" -p "PelvisCenterBtwLegsBlended_M";
createNode transform -n "CenterExtra_M" -p "CenterOffset_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Center_M" -p "CenterExtra_M";
	addAttr -ci true -k true -sn "CenterBtwFeet" -ln "CenterBtwFeet" -min 0 -max 10 
		-at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 4;
	setAttr -k on ".CenterBtwFeet";
createNode nurbsCurve -n "Center_MShape" -p "Center_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 46 0 no 3
		47 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46
		47
		-3.5135543770000002 0 0
		-3.7372301760000002 1.528565836e-16 0.68840485309999999
		-4.3228225729999998 2.4732750970000002e-16 1.113864081
		-5.0466557659999998 2.4732750970000002e-16 1.113864081
		-5.6322481619999998 1.528565836e-16 0.68840485309999999
		-5.8559239610000002 0 0
		-5.6322481619999998 -1.528565836e-16 -0.68840485309999999
		-5.0466557659999998 -2.4732750970000002e-16 -1.113864081
		-4.3228225729999998 -2.4732750970000002e-16 -1.113864081
		-3.7372301760000002 -1.528565836e-16 -0.68840485309999999
		-3.5135543770000002 0 0
		0 0 0
		3.5135543770000002 0 0
		3.7372301760000002 1.528565836e-16 0.68840485309999999
		4.3228225729999998 2.4732750970000002e-16 1.113864081
		5.0466557659999998 2.4732750970000002e-16 1.113864081
		5.6322481619999998 1.528565836e-16 0.68840485309999999
		5.8559239610000002 0 0
		5.6322481619999998 -1.528565836e-16 -0.68840485309999999
		5.0466557659999998 -2.4732750970000002e-16 -1.113864081
		4.3228225729999998 -2.4732750970000002e-16 -1.113864081
		3.7372301760000002 -1.528565836e-16 -0.68840485309999999
		3.5135543770000002 0 0
		0 0 0
		0 -7.801657935e-16 -3.5135543770000002
		-0.68840485309999999 -8.298317981e-16 -3.7372301760000002
		-1.113864081 -9.5985943030000014e-16 -4.3228225729999998
		-1.113864081 -1.1205826860000002e-15 -5.0466557659999998
		-0.68840485309999999 -1.2506103180000001e-15 -5.6322481619999998
		0 -1.300276322e-15 -5.8559239610000002
		0.68840485309999999 -1.2506103180000001e-15 -5.6322481619999998
		1.113864081 -1.1205826860000002e-15 -5.0466557659999998
		1.113864081 -9.5985943030000014e-16 -4.3228225729999998
		0.68840485309999999 -8.298317981e-16 -3.7372301760000002
		0 -7.801657935e-16 -3.5135543770000002
		0 0 0
		0 7.801657935e-16 3.5135543770000002
		-0.68840485309999999 8.298317981e-16 3.7372301760000002
		-1.113864081 9.5985943030000014e-16 4.3228225729999998
		-1.113864081 1.1205826860000002e-15 5.0466557659999998
		-0.68840485309999999 1.2506103180000001e-15 5.6322481619999998
		0 1.300276322e-15 5.8559239610000002
		0.68840485309999999 1.2506103180000001e-15 5.6322481619999998
		1.113864081 1.1205826860000002e-15 5.0466557659999998
		1.113864081 9.5985943030000014e-16 4.3228225729999998
		0.68840485309999999 8.298317981e-16 3.7372301760000002
		0 7.801657935e-16 3.5135543770000002
		;
createNode joint -n "FKOffsetPelvis_M" -p "Center_M";
	setAttr ".ro" 3;
createNode transform -n "FKExtraPelvis_M" -p "FKOffsetPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode transform -n "FKPelvis_M" -p "FKExtraPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode nurbsCurve -n "FKPelvis_MShape" -p "FKPelvis_M";
	setAttr -l on -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.33227437936806897 9.4125671050081832e-17 -0.33227437936806964
		-0.46990693373142634 1.0434029945192744e-16 0
		-0.33227437936806914 5.3433795536903916e-17 0.33227437936806925
		-1.3616727325210021e-16 -2.8773501114566901e-17 0.46990693373142634
		0.33227437936806903 -9.4125671050081832e-17 0.33227437936806925
		0.46990693373142634 -1.0434029945192744e-16 0
		0.33227437936806914 -5.3433795536903953e-17 -0.33227437936806892
		2.523879307708006e-16 2.8773501114566876e-17 -0.46990693373142634
		-0.33227437936806897 9.4125671050081832e-17 -0.33227437936806964
		-0.46990693373142634 1.0434029945192744e-16 0
		-0.33227437936806914 5.3433795536903916e-17 0.33227437936806925
		;
createNode joint -n "FKXPelvis_M" -p "FKPelvis_M";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetBody_M" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 0 0.88276794381776957 5.5511151231257827e-17 ;
createNode transform -n "FKExtraBody_M" -p "FKOffsetBody_M";
	setAttr -l on -k off ".v";
createNode transform -n "FKBody_M" -p "FKExtraBody_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKBody_MShape" -p "FKBody_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-3.1042667704751854 -3.0049161008200728e-14 -4.2956403500312152
		-4.336113673318013 -3.0033001628200725e-14 -0.44388809002419377
		-3.1042667704751854 -3.0113534888200731e-14 3.4078641701151868
		-0.13032527070720837 -3.0243585478200725e-14 5.0033121953363038
		2.8436162287288695 -3.0346971538200732e-14 3.4078641701151868
		4.0754631315716967 -3.036313090820073e-14 -0.44388809002419377
		2.8436162287288695 -3.0282597658200729e-14 -4.2956403500312152
		-0.13032527070720493 -3.1040725488200722e-14 -4.4260071405004116
		-3.1042667704751854 -3.0049161008200728e-14 -4.2956403500312152
		-4.336113673318013 -3.0033001628200725e-14 -0.44388809002419377
		-3.1042667704751854 -3.0113534888200731e-14 3.4078641701151868
		;
createNode joint -n "FKXBody_M" -p "FKBody_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHead_M" -p "FKXBody_M";
	setAttr ".t" -type "double3" 0 5.3531723602299 0.0036960628921906546 ;
createNode transform -n "FKExtraHead_M" -p "FKOffsetHead_M";
	setAttr -l on -k off ".v";
createNode transform -n "FKHead_M" -p "FKExtraHead_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHead_MShape" -p "FKHead_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.2393013083144422 2.9493054313363025 -2.2393013083144462
		-3.1668502804581022 2.9493054313363025 0
		-2.239301308314444 2.9493054313363025 2.2393013083144453
		-9.1767398293827204e-16 2.9493054313363021 3.1668502804581022
		2.239301308314444 2.9493054313363021 2.2393013083144453
		3.1668502804581022 2.9493054313363021 0
		2.239301308314444 2.9493054313363021 -2.2393013083144422
		1.7009214633180385e-15 2.9493054313363021 -3.1668502804581022
		-2.2393013083144422 2.9493054313363025 -2.2393013083144462
		-3.1668502804581022 2.9493054313363025 0
		-2.239301308314444 2.9493054313363025 2.2393013083144453
		;
createNode joint -n "FKXHead_M" -p "FKHead_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXHead_End_M" -p "FKXHead_M";
	setAttr ".t" -type "double3" -6.2302109906456059e-17 2.381127394958126 -4.4408920985006262e-16 ;
createNode joint -n "FKOffsetShoulder_R" -p "FKXBody_M";
	setAttr ".t" -type "double3" -3.6796203529496982 4.237709675283666 -0.26137462492501046 ;
	setAttr ".r" -type "double3" -90.324094980595277 -58.132996780785803 65.876464205099296 ;
	setAttr ".ro" 5;
createNode transform -n "FKGlobalStaticShoulder_R" -p "FKOffsetShoulder_R";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKGlobalShoulder_R" -p "FKGlobalStaticShoulder_R";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -8.8817841970012523e-16 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraShoulder_R" -p "FKGlobalShoulder_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 8.8817841970012523e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKShoulder_R" -p "FKExtraShoulder_R";
	addAttr -ci true -k true -sn "Global" -ln "Global" -dv 10 -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -8.8817841970012523e-16 1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".Global";
createNode nurbsCurve -n "FKShoulder_RShape" -p "FKShoulder_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.91785554931600277 -0.062229382250781312 -0.94657658355706653
		-1.3131146494302093 -0.062229382250781277 0.0076632966864233236
		-0.91785554931600277 -0.062229382250781423 0.96190317692991301
		0.036384330927486969 -0.062229382250781666 1.3571622770441196
		0.99062421117097699 -0.062229382250781853 0.96190317692991301
		1.3858833112851838 -0.062229382250781881 0.0076632966864233236
		0.99062421117097699 -0.062229382250781728 -0.94657658355706653
		0.036384330927488086 -0.062229382250781493 -1.3418356836712733
		-0.91785554931600277 -0.062229382250781312 -0.94657658355706653
		-1.3131146494302093 -0.062229382250781277 0.0076632966864233236
		-0.91785554931600277 -0.062229382250781423 0.96190317692991301
		;
createNode joint -n "FKXShoulder_R" -p "FKShoulder_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow5_R" -p "FKXShoulder_R";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 3.8569282443925967 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 2.4558026269277242 -7.8957072182712045 79.324382680802387 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraElbow5_R" -p "FKOffsetElbow5_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.7184478546569153e-16 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKElbow5_R" -p "FKExtraElbow5_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.6042284408449632e-16 8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow5_RShape" -p "FKElbow5_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.88965744058531293 -0.2403474036264151 -0.85319713629114069
		-1.2495670493057311 -0.24034740362641505 0.015701522310085814
		-0.88965744058531393 -0.24034740362641516 0.88460018091131243
		-0.020758781984088322 -0.24034740362641535 1.2445097896317301
		0.8481398766171383 -0.24034740362641557 0.88460018091131243
		1.2080494853375561 -0.2403474036264156 0.015701522310085814
		0.84813987661713841 -0.24034740362641543 -0.85319713629113891
		-0.020758781984087305 -0.24034740362641521 -1.2131067450115571
		-0.88965744058531293 -0.2403474036264151 -0.85319713629114069
		-1.2495670493057311 -0.24034740362641505 0.015701522310085814
		-0.88965744058531393 -0.24034740362641516 0.88460018091131243
		;
createNode joint -n "FKXElbow5_R" -p "FKElbow5_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetWrist1_R" -p "FKXElbow5_R";
	setAttr ".t" -type "double3" 5.5511151231257827e-17 1.3465938171234759 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -5.5840390851904962 1.5659644079196993 -7.6038421359791561 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraWrist1_R" -p "FKOffsetWrist1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 3.6082248300317583e-16 -8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "FKWrist1_R" -p "FKExtraWrist1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.9960036108132044e-16 0 -1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWrist1_RShape" -p "FKWrist1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.8208281047286077 1.1050755592927354e-13 -1.8208281047286159
		-2.575039800457299 1.1056353100176882e-13 -2.0765422921512213e-15
		-1.8208281047286083 1.1028456879824825e-13 1.820828104728611
		1.8819417267405404e-15 1.0983408159414468e-13 2.575039800457299
		1.8208281047286148 1.0947595868394654e-13 1.820828104728611
		2.575039800457303 1.0941998361145125e-13 -2.0765422921512213e-15
		1.8208281047286152 1.0969894581497193e-13 -1.8208281047286119
		4.0111827385482846e-15 1.1014943301907536e-13 -2.575039800457303
		-1.8208281047286077 1.1050755592927354e-13 -1.8208281047286159
		-2.575039800457299 1.1056353100176882e-13 -2.0765422921512213e-15
		-1.8208281047286083 1.1028456879824825e-13 1.820828104728611
		;
createNode joint -n "FKXWrist1_R" -p "FKWrist1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXElbow4_R" -p "FKXWrist1_R";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.971132819703115 -1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -64.971632303130207 -61.352170460003542 159.05823763271482 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 64.971632303129766 61.352170460003286 -159.05823763271522 ;
createNode orientConstraint -n "FKGlobalShoulder_R_orientConstraint1" -p "FKGlobalShoulder_R";
	addAttr -ci true -k true -sn "w0" -ln "GlobalShoulder_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "FKGlobalStaticShoulder_RW1" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "FKOffsetShoulder1_L" -p "FKXBody_M";
	setAttr ".t" -type "double3" 3.67962 4.2377091165244032 -0.26137491926092776 ;
	setAttr ".r" -type "double3" 89.675957839685253 -58.132942139272458 -114.12351348310035 ;
	setAttr ".ro" 5;
createNode transform -n "FKGlobalStaticShoulder1_L" -p "FKOffsetShoulder1_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKGlobalShoulder1_L" -p "FKGlobalStaticShoulder1_L";
	setAttr ".t" -type "double3" 0 0 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraShoulder1_L" -p "FKGlobalShoulder1_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
createNode transform -n "FKShoulder1_L" -p "FKExtraShoulder1_L";
	addAttr -ci true -k true -sn "Global" -ln "Global" -dv 10 -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999967 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".Global";
createNode nurbsCurve -n "FKShoulder1_LShape" -p "FKShoulder1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.91492038991010427 2.5917585286434765e-16 -0.91492038991010627
		-1.2938928239025504 2.8730192089877492e-16 0
		-0.9149203899101046 1.4713042016654188e-16 0.91492038991010516
		-3.7493776972435266e-16 -7.9228085261599337e-17 1.2938928239025504
		0.91492038991010449 -2.5917585286434765e-16 0.91492038991010516
		1.2938928239025504 -2.8730192089877502e-16 0
		0.9149203899101046 -1.4713042016654198e-16 -0.91492038991010405
		6.9495235975938702e-16 7.9228085261599263e-17 -1.2938928239025504
		-0.91492038991010427 2.5917585286434765e-16 -0.91492038991010627
		-1.2938928239025504 2.8730192089877492e-16 0
		-0.9149203899101046 1.4713042016654188e-16 0.91492038991010516
		;
createNode joint -n "FKXShoulder1_L" -p "FKShoulder1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow_L" -p "FKXShoulder1_L";
	setAttr ".t" -type "double3" 2.6645352591003757e-15 3.8569249983301503 -3.5527136788005009e-15 ;
	setAttr ".r" -type "double3" -3.5631950708836428 2.0131587708301191 79.707966104590298 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraElbow_L" -p "FKOffsetElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1379786002407857e-15 0 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKElbow_L" -p "FKExtraElbow_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow_LShape" -p "FKElbow_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.0349754619438261 -0.064713236033268465 1.0039584531021024
		1.4657251364941697 -0.064713236033268465 -0.035963252887450697
		1.0349754619438261 -0.064713236033267757 -1.0758849588770063
		-0.0049462440457329337 -0.064713236033267063 -1.506634633427349
		-1.0448679500352829 -0.064713236033267063 -1.0758849588770063
		-1.4756176245856254 -0.064713236033267063 -0.03596325288744788
		-1.0448679500352829 -0.064713236033267757 1.0039584531021024
		-0.0049462440457306257 -0.064713236033267757 1.4347081276524452
		1.0349754619438261 -0.064713236033268465 1.0039584531021024
		1.4657251364941697 -0.064713236033268465 -0.035963252887450697
		1.0349754619438261 -0.064713236033267757 -1.0758849588770063
		;
createNode joint -n "FKXElbow_L" -p "FKElbow_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetWrist_L" -p "FKXElbow_L";
	setAttr ".t" -type "double3" 7.2164496600635165e-16 0.8326449618597942 3.5527136788005009e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.8683496313300023e-13 3.8763010180336611e-14 180.00000500895632 ;
createNode transform -n "FKExtraWrist_L" -p "FKOffsetWrist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -3.3306690738754696e-16 -4.4408920985006262e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKWrist_L" -p "FKExtraWrist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.9960036108132044e-16 4.4408920985006262e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999989 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWrist_LShape" -p "FKWrist_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.6864567181155474 0.056550838915617373 -1.7264034755212072
		-2.3870105510202877 0.056550838915617414 -0.035116910950127485
		-1.6864567181155483 0.056550838915617158 1.6561696536209487
		0.0048298464555265805 0.056550838915616734 2.3567234865256879
		1.6961164110266038 0.056550838915616422 1.6561696536209487
		2.3966702439313439 0.056550838915616367 -0.035116910950127485
		1.696116411026604 0.056550838915616602 -1.7264034755212032
		0.0048298464555285563 0.056550838915617047 -2.4269573084259446
		-1.6864567181155474 0.056550838915617373 -1.7264034755212072
		-2.3870105510202877 0.056550838915617414 -0.035116910950127485
		-1.6864567181155483 0.056550838915617158 1.6561696536209487
		;
createNode joint -n "FKXWrist_L" -p "FKWrist_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger1_L" -p "FKXWrist_L";
	setAttr ".t" -type "double3" -0.061912811721445915 -3.4542914752233922 1.0264096342578446 ;
	setAttr ".r" -type "double3" -179.29338095818349 -21.171634700484205 30.005990910728158 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger1_L" -p "FKOffsetMiddleFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.1102230246251563e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKMiddleFinger1_L" -p "FKExtraMiddleFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 2.2204460492503131e-16 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger1_LShape" -p "FKMiddleFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.58421839988745361 -0.01667713008894478 0.59412422447802038
		0.82587558896243596 -0.016677130088944489 0.010712160759645668
		0.58421839988745361 -0.016677130088944211 -0.57269990295872053
		0.00080633616908009124 -0.01667713008894392 -0.81435709203370288
		-0.58260572754928697 -0.01667713008894392 -0.57269990295872053
		-0.8242629166242692 -0.01667713008894392 0.010712160759646807
		-0.58260572754928697 -0.016677130088944211 0.59412422447802038
		0.00080633616908236925 -0.01667713008894478 0.83578141355300239
		0.58421839988745361 -0.01667713008894478 0.59412422447802038
		0.82587558896243596 -0.016677130088944489 0.010712160759645668
		0.58421839988745361 -0.016677130088944211 -0.57269990295872053
		;
createNode joint -n "FKXMiddleFinger1_L" -p "FKMiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger2_L" -p "FKXMiddleFinger1_L";
	setAttr ".t" -type "double3" 0 0.41333313431433666 3.5527136788005009e-15 ;
	setAttr ".r" -type "double3" -24.690175915565 1.448955903639682 0.88319719406150965 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger2_L" -p "FKOffsetMiddleFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKMiddleFinger2_L" -p "FKExtraMiddleFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger2_LShape" -p "FKMiddleFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.58740138580884105 -0.059165252922985953 0.60509374552222428
		0.82991218429183933 -0.059165252922985953 0.019620886373988709
		0.58740138580884105 -0.059165252922985953 -0.56585197277423516
		0.0019285266605989879 -0.059165252922985953 -0.80836277125723388
		-0.58354433248761872 -0.059165252922985953 -0.56585197277423516
		-0.82605513097061667 -0.059165252922985953 0.019620886373990405
		-0.58354433248761872 -0.059165252922985953 0.60509374552222428
		0.0019285266606006921 -0.059165252922985953 0.847604544005222
		0.58740138580884105 -0.059165252922985953 0.60509374552222428
		0.82991218429183933 -0.059165252922985953 0.019620886373988709
		0.58740138580884105 -0.059165252922985953 -0.56585197277423516
		;
createNode joint -n "FKXMiddleFinger2_L" -p "FKMiddleFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger3_L" -p "FKXMiddleFinger2_L";
	setAttr ".t" -type "double3" 3.5527136788005009e-15 0.90189613135496893 2.6645352591003757e-15 ;
	setAttr ".r" -type "double3" -26.38375205612093 -2.4869181940219516 2.5129275543813603 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger3_L" -p "FKOffsetMiddleFinger3_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKMiddleFinger3_L" -p "FKExtraMiddleFinger3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -8.8817841970012523e-16 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger3_LShape" -p "FKMiddleFinger3_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.48721768738944032 3.8901013433103423e-16 -0.48721768738944143
		-0.68902986133420185 4.0398795969618793e-16 -2.5099259635468587e-16
		-0.48721768738944055 3.2934317619608896e-16 0.48721768738944071
		-1.99663615648573e-16 2.0880168564469214e-16 0.68902986133420185
		0.48721768738944038 1.1297505837833744e-16 0.48721768738944071
		0.68902986133420185 9.7997233013183892e-17 -2.5099259635468587e-16
		0.48721768738944055 1.7264201651328278e-16 -0.48721768738944032
		3.700792826368985e-16 2.9318350706467956e-16 -0.68902986133420185
		-0.48721768738944032 3.8901013433103423e-16 -0.48721768738944143
		-0.68902986133420185 4.0398795969618793e-16 -2.5099259635468587e-16
		-0.48721768738944055 3.2934317619608896e-16 0.48721768738944071
		;
createNode joint -n "FKXMiddleFinger3_L" -p "FKMiddleFinger3_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleFinger4_End_L" -p "FKXMiddleFinger3_L";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.1257201477021042 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 65.062373340805195 58.304136049030049 -89.106356429252926 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -65.062373340805166 -58.304136049030099 -90.893643570747059 ;
createNode joint -n "FKOffsetIndexFinger1_L" -p "FKXWrist_L";
	setAttr ".t" -type "double3" 0.76493398944445845 -3.296252584251997 0.96784838689042996 ;
	setAttr ".r" -type "double3" -175.58486144643337 32.769304448902311 -25.833196120335582 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger1_L" -p "FKOffsetIndexFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -3.7470027081099033e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "FKIndexFinger1_L" -p "FKExtraIndexFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -5.6898930012039273e-16 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger1_LShape" -p "FKIndexFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.54975360702817899 -0.069142440931950752 0.60333905436686153
		0.77874365422551139 -0.069142440931951946 0.050508176321556311
		0.54975360702817899 -0.069142440931950752 -0.50232270172375493
		-0.0030772710171246276 -0.069142440931950752 -0.731312748921087
		-0.55590814906243713 -0.069142440931950752 -0.50232270172375493
		-0.78489819625976986 -0.069142440931949628 0.050508176321556866
		-0.55590814906243713 -0.069142440931949628 0.60333905436686153
		-0.0030772710171246276 -0.069142440931950211 0.83232910156419371
		0.54975360702817899 -0.069142440931950752 0.60333905436686153
		0.77874365422551139 -0.069142440931951946 0.050508176321556311
		0.54975360702817899 -0.069142440931950752 -0.50232270172375493
		;
createNode joint -n "FKXIndexFinger1_L" -p "FKIndexFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetIndexFinger2_L" -p "FKXIndexFinger1_L";
	setAttr ".t" -type "double3" 0 0.44186264890330679 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -26.585679768743184 -4.5354321584301722 -1.1032517484933044 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger2_L" -p "FKOffsetIndexFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKIndexFinger2_L" -p "FKExtraIndexFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -4.4408920985006262e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger2_LShape" -p "FKIndexFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.62601968937846997 -0.07405021211414782 0.65117673571104873
		0.88530747417009836 -0.074050212114148695 0.025200648802118919
		0.62601968937846997 -0.07405021211414782 -0.60077543810683065
		4.3602469532031115e-05 -0.07405021211414782 -0.86006322289845905
		-0.62593248443940974 -0.07405021211414782 -0.60077543810683065
		-0.88522026923103803 -0.07405021211414782 0.025200648802120747
		-0.62593248443940974 -0.07405021211414782 0.65117673571104873
		4.3602469533841576e-05 -0.07405021211414782 0.91046452050267723
		0.62601968937846997 -0.07405021211414782 0.65117673571104873
		0.88530747417009836 -0.074050212114148695 0.025200648802118919
		0.62601968937846997 -0.07405021211414782 -0.60077543810683065
		;
createNode joint -n "FKXIndexFinger2_L" -p "FKIndexFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetIndexFinger3_L" -p "FKXIndexFinger2_L";
	setAttr ".t" -type "double3" 0 0.88370494679700573 -1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -31.032120965590437 -0.042250986581690221 0.05110539636073947 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger3_L" -p "FKOffsetIndexFinger3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
createNode transform -n "FKIndexFinger3_L" -p "FKExtraIndexFinger3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger3_LShape" -p "FKIndexFinger3_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.50490937753852183 1.4302918591158624e-16 -0.50490937753852272
		-0.714049689484335 1.5855088119839045e-16 -2.7349915585970856e-16
		-0.50490937753852216 8.1195620605382896e-17 0.50490937753852216
		-7.0164157969949062e-17 -4.3722933332957602e-17 0.714049689484335
		0.50490937753852205 -1.4302918591158624e-16 0.50490937753852216
		0.714049689484335 -1.585508811983905e-16 -2.7349915585970856e-16
		0.50490937753852216 -8.1195620605382933e-17 -0.50490937753852194
		5.2026705907323044e-16 4.3722933332957546e-17 -0.71404968948433523
		-0.50490937753852183 1.4302918591158624e-16 -0.50490937753852272
		-0.714049689484335 1.5855088119839045e-16 -2.7349915585970856e-16
		-0.50490937753852216 8.1195620605382896e-17 0.50490937753852216
		;
createNode joint -n "FKXIndexFinger3_L" -p "FKIndexFinger3_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXIndexFinger4_End_L" -p "FKXIndexFinger3_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 1.1571989645814322 2.6645352591003757e-15 ;
	setAttr ".r" -type "double3" -11.065256998904839 12.656317452163822 -25.956788437892985 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 11.065256998904848 -12.656317452163833 -154.04321156210705 ;
createNode joint -n "FKOffsetThumbFinger1_L" -p "FKXWrist_L";
	setAttr ".t" -type "double3" 0.56809760079276661 -2.0282457218782541 1.8256326612627456 ;
	setAttr ".r" -type "double3" -73.130610047083934 28.025536579747939 -152.54062280951075 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraThumbFinger1_L" -p "FKOffsetThumbFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -8.8817841970012523e-16 1.1102230246251563e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKThumbFinger1_L" -p "FKExtraThumbFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -8.8817841970012523e-16 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger1_LShape" -p "FKThumbFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.65160195364090179 -0.11649264711911449 0.74488733470194812
		0.92699777963068797 -0.11649264711911449 0.080022995972750546
		0.65160195364090179 -0.11649264711911449 -0.58484134266374077
		-0.013262385106227044 -0.11649264711911449 -0.86023716880549983
		-0.67812672372478766 -0.11649264711911449 -0.58484134266374077
		-0.95352254986654628 -0.11649264711911449 0.08002299597275174
		-0.67812672372478766 -0.11649264711911449 0.74488733470194812
		-0.013262385106227044 -0.11649264711911449 1.0202831606917346
		0.65160195364090179 -0.11649264711911449 0.74488733470194812
		0.92699777963068797 -0.11649264711911449 0.080022995972750546
		0.65160195364090179 -0.11649264711911449 -0.58484134266374077
		;
createNode joint -n "FKXThumbFinger1_L" -p "FKThumbFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetThumbFinger2_L" -p "FKXThumbFinger1_L";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0.52153996925717561 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -24.910131912501505 0.4189414188107945 -3.1009961697488801 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraThumbFinger2_L" -p "FKOffsetThumbFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKThumbFinger2_L" -p "FKExtraThumbFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999967 1 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger2_LShape" -p "FKThumbFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.59173114833218932 -0.033590361660075445 0.60490764254282192
		0.83777554301799007 -0.033590361660075008 0.010903926915698223
		0.59173114833218932 -0.033590361660074571 -0.58309978871144263
		-0.0022725672949445619 -0.033590361660074133 -0.82914418339724283
		-0.59627628292207513 -0.033590361660074133 -0.58309978871144263
		-0.84232067760787566 -0.033590361660074133 0.010903926915699526
		-0.59627628292207513 -0.033590361660074571 0.60490764254282192
		-0.0022725672949428272 -0.033590361660075008 0.85095203722862245
		0.59173114833218932 -0.033590361660075445 0.60490764254282192
		0.83777554301799007 -0.033590361660075008 0.010903926915698223
		0.59173114833218932 -0.033590361660074571 -0.58309978871144263
		;
createNode joint -n "FKXThumbFinger2_L" -p "FKThumbFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetThumbFinger3_L" -p "FKXThumbFinger2_L";
	setAttr ".t" -type "double3" 0 0.83547866073933108 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -30.626014993014405 -5.8067557592121561 -5.9833541421842442 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraThumbFinger3_L" -p "FKOffsetThumbFinger3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 4.4408920985006262e-16 -4.4408920985006262e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKThumbFinger3_L" -p "FKExtraThumbFinger3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger3_LShape" -p "FKThumbFinger3_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.56397636656611938 1.5976150210672627e-16 -0.56397636656612038
		-0.79758302645570645 1.7709900800426813e-16 -2.308046762713287e-16
		-0.56397636656611971 9.0694316895731042e-17 0.56397636656611982
		-2.3111960711502051e-16 -4.8837875020161931e-17 0.79758302645570645
		0.56397636656611949 -1.5976150210672627e-16 0.56397636656611982
		0.79758302645570645 -1.7709900800426816e-16 -2.308046762713287e-16
		0.56397636656611971 -9.0694316895731079e-17 -0.56397636656611927
		4.2838339938205932e-16 4.8837875020161894e-17 -0.79758302645570645
		-0.56397636656611938 1.5976150210672627e-16 -0.56397636656612038
		-0.79758302645570645 1.7709900800426813e-16 -2.308046762713287e-16
		-0.56397636656611971 9.0694316895731042e-17 0.56397636656611982
		;
createNode joint -n "FKXThumbFinger3_L" -p "FKThumbFinger3_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXThumbFinger4_End_L" -p "FKXThumbFinger3_L";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.0809764280246543 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -9.8297910450967088 -74.445514654772907 27.164593916789514 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 9.8297910450967194 74.445514654772893 152.83540608321093 ;
createNode joint -n "FKOffsetPinkyFinger1_L" -p "FKXWrist_L";
	setAttr ".t" -type "double3" -0.6888632780182925 -2.979443695577757 1.2606811180138759 ;
	setAttr ".r" -type "double3" -144.77984768584045 -35.495311493829853 90.565665111160286 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraPinkyFinger1_L" -p "FKOffsetPinkyFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 4.4408920985006262e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKPinkyFinger1_L" -p "FKExtraPinkyFinger1_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKPinkyFinger1_LShape" -p "FKPinkyFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.49189096273771482 -0.095679922986390054 -0.42618532342479526
		-0.69302131444203907 -0.09567992298639004 0.059386299464656206
		-0.49189096273771554 -0.095679922986390109 0.544957922354107
		-0.0063193398482646879 -0.095679922986390234 0.7460882740584317
		0.47925228304118622 -0.095679922986390303 0.544957922354107
		0.68038263474551153 -0.095679922986390317 0.059386299464656206
		0.47925228304118639 -0.095679922986390234 -0.42618532342479432
		-0.0063193398482641189 -0.095679922986390151 -0.62731567512911934
		-0.49189096273771482 -0.095679922986390054 -0.42618532342479526
		-0.69302131444203907 -0.09567992298639004 0.059386299464656206
		-0.49189096273771554 -0.095679922986390109 0.544957922354107
		;
createNode joint -n "FKXPinkyFinger1_L" -p "FKPinkyFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetPinkyFinger2_L" -p "FKXPinkyFinger1_L";
	setAttr ".t" -type "double3" 2.6645352591003757e-15 0.37095051782777194 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -24.814777349046249 -4.0509373854103723 -3.2722068141262972 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraPinkyFinger2_L" -p "FKOffsetPinkyFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKPinkyFinger2_L" -p "FKExtraPinkyFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKPinkyFinger2_LShape" -p "FKPinkyFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.44078535186987217 1.2486432782661066e-16 -0.44078535186987322
		-0.62336462270977067 1.3841475137383223e-16 1.740416170838944e-16
		-0.4407853518698725 7.088369079876278e-17 0.44078535186987267
		-1.8063547230977363e-16 -3.8170074495160875e-17 0.62336462270977067
		0.44078535186987239 -1.2486432782661066e-16 0.44078535186987267
		0.62336462270977067 -1.3841475137383223e-16 1.740416170838944e-16
		0.4407853518698725 -7.0883690798762841e-17 -0.44078535186987206
		3.3481035487627244e-16 3.8170074495160845e-17 -0.62336462270977067
		-0.44078535186987217 1.2486432782661066e-16 -0.44078535186987322
		-0.62336462270977067 1.3841475137383223e-16 1.740416170838944e-16
		-0.4407853518698725 7.088369079876278e-17 0.44078535186987267
		;
createNode joint -n "FKXPinkyFinger2_L" -p "FKPinkyFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetPinkyFinger3_L" -p "FKXPinkyFinger2_L";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0.80315171109900507 -1.3322676295501878e-15 ;
	setAttr ".r" -type "double3" -24.027854179296156 -0.28744090729844968 0.52075923553115966 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraPinkyFinger3_L" -p "FKOffsetPinkyFinger3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 -6.6613381477509392e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKPinkyFinger3_L" -p "FKExtraPinkyFinger3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000009 1.0000000000000009 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKPinkyFinger3_LShape" -p "FKPinkyFinger3_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.49356085087021512 -3.4227207135822549e-17 -0.49356085087021617
		-0.69800044915706316 -1.9054383113319937e-17 -3.4808323416778881e-16
		-0.49356085087021528 -9.4670978756087863e-17 0.49356085087021545
		-2.0226306757325069e-16 -2.1678181787707503e-16 0.69800044915706316
		0.49356085087021517 -3.1385602703196637e-16 0.49356085087021545
		0.69800044915706316 -3.2902885105446904e-16 -3.4808323416778881e-16
		0.49356085087021528 -2.5341225541170113e-16 -0.49356085087021512
		3.7489740285579221e-16 -1.3130141629071397e-16 -0.69800044915706316
		-0.49356085087021512 -3.4227207135822549e-17 -0.49356085087021617
		-0.69800044915706316 -1.9054383113319937e-17 -3.4808323416778881e-16
		-0.49356085087021528 -9.4670978756087863e-17 0.49356085087021545
		;
createNode joint -n "FKXPinkyFinger3_L" -p "FKPinkyFinger3_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXPinkyFinger4_End_L" -p "FKXPinkyFinger3_L";
	setAttr ".t" -type "double3" 0 0.99669641313246915 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 137.46240561568294 30.092976134421299 -151.50360948911424 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -137.46240561568294 -30.092976134421267 -28.496390510885625 ;
createNode orientConstraint -n "FKGlobalShoulder1_L_orientConstraint1" -p "FKGlobalShoulder1_L";
	addAttr -ci true -k true -sn "w0" -ln "GlobalShoulder1_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "FKGlobalStaticShoulder1_LW1" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "FKOffsetHose1_R" -p "FKXBody_M";
	setAttr ".t" -type "double3" -2.1689490945289349 -1.0831980499384857 -4.9349946771425932 ;
	setAttr ".r" -type "double3" -90.000002504478161 0 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraHose1_R" -p "FKOffsetHose1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKHose1_R" -p "FKExtraHose1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHose1_RShape" -p "FKHose1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.79723039188374412 -0.019069895964962717 -0.79723039188374623
		-1.1274540325380096 -0.019069895964962693 0
		-0.79723039188374467 -0.019069895964962821 0.79723039188374478
		-3.2670797195669932e-16 -0.019069895964963009 1.1274540325380096
		0.79723039188374445 -0.019069895964963172 0.79723039188374478
		1.1274540325380096 -0.0190698959649632 0
		0.79723039188374467 -0.019069895964963082 -0.79723039188374412
		6.0555776023960539e-16 -0.019069895964962877 -1.1274540325380096
		-0.79723039188374412 -0.019069895964962717 -0.79723039188374623
		-1.1274540325380096 -0.019069895964962693 0
		-0.79723039188374467 -0.019069895964962821 0.79723039188374478
		;
createNode joint -n "FKXHose1_R" -p "FKHose1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXHose1_End_R" -p "FKXHose1_R";
	setAttr ".t" -type "double3" 0 1.2444681704849063 0 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 90.000000000000028 0 0 ;
createNode joint -n "FKOffsetHose2_R" -p "FKXBody_M";
	setAttr ".t" -type "double3" -6.3782461254304437 -2.2020769169102503 -3.3864248464098754 ;
	setAttr ".r" -type "double3" -90.000002504478161 0 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraHose2_R" -p "FKOffsetHose2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKHose2_R" -p "FKExtraHose2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHose2_RShape" -p "FKHose2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.71426598711427403 -0.030205663853521324 -0.71426598711427403
		-1.0101246461188125 -0.030205663853521303 1.2711880312911708e-17
		-0.71426598711427425 -0.030205663853521407 0.71426598711427325
		-1.6636826183355139e-15 -0.030205663853521591 1.0101246461188111
		0.71426598711427114 -0.03020566385352173 0.71426598711427325
		1.0101246461188094 -0.030205663853521757 1.2711880312911708e-17
		0.71426598711427125 -0.03020566385352165 -0.71426598711427258
		-8.2843384929823657e-16 -0.030205663853521473 -1.0101246461188111
		-0.71426598711427403 -0.030205663853521324 -0.71426598711427403
		-1.0101246461188125 -0.030205663853521303 1.2711880312911708e-17
		-0.71426598711427425 -0.030205663853521407 0.71426598711427325
		;
createNode joint -n "FKXHose2_R" -p "FKHose2_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXHose2_End_R" -p "FKXHose2_R";
	setAttr ".t" -type "double3" 0 1.2444681704849092 0 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 90.000000000000028 0 0 ;
createNode joint -n "FKOffsetHose1_L" -p "FKXBody_M";
	setAttr ".t" -type "double3" 2.1689490945289349 -1.0831980499384852 -4.9349946771425932 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 89.999997495521853 0 0 ;
createNode transform -n "FKExtraHose1_L" -p "FKOffsetHose1_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKHose1_L" -p "FKExtraHose1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHose1_LShape" -p "FKHose1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.79723039188374445 0.019069895964963557 0.79723039188374623
		1.1274540325380094 0.019069895964962669 -4.4408920985006262e-16
		0.79723039188374445 0.019069895964962669 -0.79723039188374445
		4.4408920985006262e-16 0.019069895964962669 -1.1274540325380094
		-0.79723039188374445 0.019069895964962669 -0.79723039188374445
		-1.1274540325380096 0.019069895964963557 -4.4408920985006262e-16
		-0.79723039188374445 0.019069895964963557 0.79723039188374401
		-4.4408920985006262e-16 0.019069895964963557 1.1274540325380094
		0.79723039188374445 0.019069895964963557 0.79723039188374623
		1.1274540325380094 0.019069895964962669 -4.4408920985006262e-16
		0.79723039188374445 0.019069895964962669 -0.79723039188374445
		;
createNode joint -n "FKXHose1_L" -p "FKHose1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXHose1_End_L" -p "FKXHose1_L";
	setAttr ".t" -type "double3" 0 -1.2444681704849074 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -180 0 0 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -90.000000000000014 0 0 ;
createNode joint -n "FKOffsetHose2_L" -p "FKXBody_M";
	setAttr ".t" -type "double3" 6.3782461254304463 -2.2020769169102503 -3.3864248464098754 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 89.999997495521853 0 0 ;
createNode transform -n "FKExtraHose2_L" -p "FKOffsetHose2_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKHose2_L" -p "FKExtraHose2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHose2_LShape" -p "FKHose2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.71426598711427047 0.030205663853521525 0.7142659871142738
		1.0101246461188085 0.030205663853521081 4.4408920985006262e-16
		0.71426598711427047 0.030205663853521081 -0.71426598711427358
		-1.7763568394002505e-15 0.030205663853521525 -1.0101246461188111
		-0.71426598711427491 0.030205663853521525 -0.71426598711427358
		-1.0101246461188127 0.030205663853521525 4.4408920985006262e-16
		-0.71426598711427491 0.030205663853521969 0.71426598711427247
		-2.6645352591003757e-15 0.030205663853521525 1.0101246461188107
		0.71426598711427047 0.030205663853521525 0.7142659871142738
		1.0101246461188085 0.030205663853521081 4.4408920985006262e-16
		0.71426598711427047 0.030205663853521081 -0.71426598711427358
		;
createNode joint -n "FKXHose2_L" -p "FKHose2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXHose2_End_L" -p "FKXHose2_L";
	setAttr ".t" -type "double3" 0 -1.2444681704849092 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -180 0 0 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -90.000000000000014 0 0 ;
createNode joint -n "FKOffsetTrack_R" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" -0.65512380987240437 4.4408920985006262e-16 0 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 26.580167806082983 -0.001277902588734433 179.9983930843122 ;
createNode transform -n "FKExtraTrack_R" -p "FKOffsetTrack_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 2.2204460492503131e-16 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "FKTrack_R" -p "FKExtraTrack_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251563e-16 4.4408920985006262e-16 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrack_RShape" -p "FKTrack_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.2897019359999999 3.653428241e-16 -1.2897019359999999
		-1.8239139689999999 4.0499025659999999e-16 0
		-1.2897019359999999 2.0739988939999999e-16 1.2897019359999999
		-5.2852463739999996e-16 -1.1168252019999999e-16 1.8239139689999999
		1.2897019359999999 -3.653428241e-16 1.2897019359999999
		1.8239139689999999 -4.0499025659999999e-16 0
		1.2897019359999999 -2.0739988939999999e-16 -1.2897019359999999
		9.7962775059999983e-16 1.1168252019999999e-16 -1.8239139689999999
		-1.2897019359999999 3.653428241e-16 -1.2897019359999999
		-1.8239139689999999 4.0499025659999999e-16 0
		-1.2897019359999999 2.0739988939999999e-16 1.2897019359999999
		;
createNode joint -n "FKXTrack_R" -p "FKTrack_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetTrackBend_R" -p "FKXTrack_R";
	setAttr ".t" -type "double3" -5.5511151231257827e-16 1.242542923899141 0.061160511392438721 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" -59.50682786063134 -0.00049904335901457654 -0.0009688106821282371 ;
createNode transform -n "FKExtraTrackBend_R" -p "FKOffsetTrackBend_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251563e-16 0 -2.2204460492503131e-16 ;
	setAttr ".ro" 3;
createNode transform -n "FKTrackBend_R" -p "FKExtraTrackBend_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251563e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrackBend_RShape" -p "FKTrackBend_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.2794834216 7.9171209820000003e-17 -0.2794834216
		-0.3952492453 8.7762962519999995e-17 0
		-0.2794834216 4.4944362050000004e-17 0.2794834216
		-1.1453334289999998e-16 -2.4202036159999997e-17 0.3952492453
		0.2794834216 -7.9171209820000003e-17 0.2794834216
		0.3952492453 -8.7762962519999995e-17 0
		0.2794834216 -4.4944362050000004e-17 -0.2794834216
		2.1228914069999999e-16 2.4202036159999997e-17 -0.3952492453
		-0.2794834216 7.9171209820000003e-17 -0.2794834216
		-0.3952492453 8.7762962519999995e-17 0
		-0.2794834216 4.4944362050000004e-17 0.2794834216
		;
createNode joint -n "FKXTrackBend_R" -p "FKTrackBend_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetTrackBottom_R" -p "FKXTrackBend_R";
	setAttr ".t" -type "double3" 0 1.3517839773230294 -0.040242726398744821 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 32.926661762095648 -0.00033961572143530373 0.0029978254302916369 ;
createNode transform -n "FKExtraTrackBottom_R" -p "FKOffsetTrackBottom_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 0 ;
	setAttr ".ro" 3;
createNode transform -n "FKTrackBottom_R" -p "FKExtraTrackBottom_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrackBottom_RShape" -p "FKTrackBottom_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.168530775 3.3101782800000001e-16 -1.168530775
		-1.65255207 3.6694027159999999e-16 0
		-1.168530775 1.8791408060000002e-16 1.168530775
		-4.7886824639999998e-16 -1.0118963020000001e-16 1.65255207
		1.168530775 -3.3101782800000001e-16 1.168530775
		1.65255207 -3.6694027159999999e-16 0
		1.168530775 -1.8791408060000002e-16 -1.168530775
		8.875889406000001e-16 1.0118963020000001e-16 -1.65255207
		-1.168530775 3.3101782800000001e-16 -1.168530775
		-1.65255207 3.6694027159999999e-16 0
		-1.168530775 1.8791408060000002e-16 1.168530775
		;
createNode joint -n "FKXTrackBottom_R" -p "FKTrackBottom_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToTrackBottom_R" -p "FKXTrackBottom_R";
	setAttr ".r" -type "double3" 180 179.99994625672272 -7.0167092985317876e-15 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode joint -n "FKXFrontTrack_End_R" -p "FKXTrackBottom_R";
	setAttr ".t" -type "double3" 0.22774006241858649 1.6785359357460981 4.6938822643168141 ;
	setAttr ".r" -type "double3" 0 0 89.999999999999929 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 7.0167092985379628e-15 5.3743277296642019e-05 -180 ;
createNode joint -n "FKXBackTrack_End_R" -p "FKXTrackBottom_R";
	setAttr ".t" -type "double3" 0.22774374126649011 1.5483447246245885 -5.1362642985384435 ;
	setAttr ".r" -type "double3" 0 0 89.999999999999986 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 7.0167092985379628e-15 5.3743277296642019e-05 -180 ;
createNode joint -n "FKOffsetTrack_L" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 0.65512380987240437 4.4408920985006262e-16 -1.6653345369377348e-16 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 26.580167806082983 179.99872209736122 0.0016069157507125722 ;
createNode transform -n "FKExtraTrack_L" -p "FKOffsetTrack_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -2.5489049678083722e-31 -2.3765347625211631e-15 -1.22902891502769e-14 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKTrack_L" -p "FKExtraTrack_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.1102230246251563e-16 0 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrack_LShape" -p "FKTrack_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.2897019360016408 7.6161299489285749e-13 1.2897019359983593
		1.8239139689999997 1.0769163338864018e-12 -2.3210322552813523e-12
		1.2897019359983584 7.6072481647315726e-13 -1.2897019360016415
		-2.3210322552813523e-12 -4.4408920985006262e-16 -1.8239139689999999
		-1.2897019360016415 -7.6294526252240747e-13 -1.2897019359983586
		-1.8239139689999999 -1.0791367799356522e-12 2.3212542998862773e-12
		-1.2897019359983588 -7.6338935173225764e-13 1.289701936001642
		2.3199220322567271e-12 -4.4408920985006262e-16 1.8239139690000008
		1.2897019360016408 7.6161299489285749e-13 1.2897019359983593
		1.8239139689999997 1.0769163338864018e-12 -2.3210322552813523e-12
		1.2897019359983584 7.6072481647315726e-13 -1.2897019360016415
		;
createNode joint -n "FKXTrack_L" -p "FKTrack_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetTrackBend_L" -p "FKXTrack_L";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 -1.2425429238991423 -0.061160511392438277 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" -59.50682786063134 -0.00049904335901457654 -0.0009688106821282371 ;
createNode transform -n "FKExtraTrackBend_L" -p "FKOffsetTrackBend_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.1102230246251563e-16 0 0 ;
	setAttr ".r" -type "double3" 1.9083328088781094e-14 -9.5787420439120034e-15 -6.2019263283323016e-15 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKTrackBend_L" -p "FKExtraTrackBend_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.1102230246251563e-16 -4.4408920985006262e-16 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrackBend_LShape" -p "FKTrackBend_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.27948342160069534 3.9124259387790511e-13 0.27948342159996176
		0.3952492453006573 5.5289106626332796e-13 -5.4178883601707639e-14
		0.27948342160061912 3.9124259387790511e-13 -0.27948342160003836
		6.035172361862351e-13 8.8817841970012523e-16 -0.39524924529999994
		-0.279483421599381 -3.8946623703850486e-13 -0.27948342159996198
		-0.39524924529934302 -5.5067062021407764e-13 5.3734794391857577e-14
		-0.27948342159930473 -3.8946623703850486e-13 0.27948342160003814
		7.1076478036502522e-13 1.3322676295501878e-15 0.39524924530000011
		0.27948342160069534 3.9124259387790511e-13 0.27948342159996176
		0.3952492453006573 5.5289106626332796e-13 -5.4178883601707639e-14
		0.27948342160061912 3.9124259387790511e-13 -0.27948342160003836
		;
createNode joint -n "FKXTrackBend_L" -p "FKTrackBend_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetTrackBottom_L" -p "FKXTrackBend_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -1.351783977323028 0.040242726398744377 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 32.926661762095648 -0.00033961572143530373 0.0029978254302916369 ;
createNode transform -n "FKExtraTrackBottom_L" -p "FKOffsetTrackBottom_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251563e-16 0 5.5511151231257827e-17 ;
	setAttr ".r" -type "double3" 1.4033418597069752e-14 2.5444437451708128e-14 -8.0947816467072621e-37 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKTrackBottom_L" -p "FKExtraTrackBottom_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 5.5511151231257827e-17 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrackBottom_LShape" -p "FKTrackBottom_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.1685307750035725 1.2823075934420558e-12 1.1685307749989806
		1.6525520700025518 1.8136603330276555e-12 -1.4428458428028534e-12
		1.1685307750015306 1.2823075934420558e-12 -1.1685307750010203
		1.1077805339709812e-12 -6.6613381477509392e-16 -1.6525520699999996
		-1.1685307749984704 -1.283417816466681e-12 -1.1685307749989788
		-1.6525520699974496 -1.8149926006572059e-12 1.4446777107934847e-12
		-1.1685307749964284 -1.283417816466681e-12 1.1685307750010221
		3.9939163087865389e-12 -6.6613381477509392e-16 1.6525520700000014
		1.1685307750035725 1.2823075934420558e-12 1.1685307749989806
		1.6525520700025518 1.8136603330276555e-12 -1.4428458428028534e-12
		1.1685307750015306 1.2823075934420558e-12 -1.1685307750010203
		;
createNode joint -n "FKXTrackBottom_L" -p "FKTrackBottom_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToTrackBottom_L" -p "FKXTrackBottom_L";
	setAttr ".r" -type "double3" 0 179.99994625672272 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode joint -n "FKXFrontTrack_End_L" -p "FKXTrackBottom_L";
	setAttr ".t" -type "double3" -0.22774006241858849 -1.6785359357460985 -4.6938822643168141 ;
	setAttr ".r" -type "double3" 180 0 89.999999999999929 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 179.99994625672272 0 ;
createNode joint -n "FKXBackTrack_End_L" -p "FKXTrackBottom_L";
	setAttr ".t" -type "double3" -0.22774374126648775 -1.5483447246245898 5.1362642985384426 ;
	setAttr ".r" -type "double3" 7.016709298534876e-15 180 -90.000000000000028 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 179.99994625672272 0 ;
createNode parentConstraint -n "PelvisCenterBtwLegsBlended_M_parentConstraint1" -p
		 "PelvisCenterBtwLegsBlended_M";
	addAttr -ci true -k true -sn "w0" -ln "PelvisCenter_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PelvisCenterBtwLegsOffset_MW1" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0 3.9898529396578266 -0.31454308073907233 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKSystem" -p "MotionSystem";
createNode transform -n "IKParentConstraint" -p "IKSystem";
createNode transform -n "IKParentConstraintTrack_R" -p "IKParentConstraint";
	setAttr ".ro" 3;
createNode transform -n "IKOffsetTrack_R" -p "IKParentConstraintTrack_R";
	setAttr ".t" -type "double3" -0.65512380987240437 4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" 26.580167854041878 0.0012779026285295334 179.9983888340949 ;
	setAttr ".ro" 3;
createNode joint -n "IKXTrack_R" -p "IKOffsetTrack_R";
	setAttr ".t" -type "double3" 0 0 2.2204460492503131e-16 ;
	setAttr ".ro" 3;
createNode joint -n "IKXTrackBend_R" -p "IKXTrack_R";
	setAttr ".t" -type "double3" -9.9920072216264089e-16 1.2425429238991423 0.061160511392438943 ;
	setAttr ".r" -type "double3" -3.8625654506756611e-14 -2.1044086925576395e-20 -7.0688698456816696e-19 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" -59.50682786063134 -0.00049904335901457654 -0.0009688106821282371 ;
createNode joint -n "IKXTrackBottom_R" -p "IKXTrackBend_R";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 1.3517839773230285 -0.040242726398745265 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 32.926661762095641 -0.00033961572143530384 0.0029978254302916369 ;
createNode joint -n "IKXFrontTrack_End_R" -p "IKXTrackBottom_R";
	setAttr ".t" -type "double3" 0.22774006241858649 1.6785359357460985 4.6938822643168141 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743455807798471e-05 0 -90.000002504478161 ;
createNode transform -n "IKFKAlignedLeg_R" -p "IKXTrackBottom_R";
	setAttr ".t" -type "double3" -1.6294592863896895e-07 -2.2476309702312847e-10 -3.2164910179766082e-09 ;
	setAttr ".r" -type "double3" 0 0 4.250215769154677e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode orientConstraint -n "IKXTrackBottom_R_orientConstraint1" -p "IKXTrackBottom_R";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.791378037353395e-06 -5.3743185435206101e-05 179.99999999983746 ;
	setAttr ".o" -type "double3" -179.99999820862197 179.99994625681455 0 ;
	setAttr ".rsrr" -type "double3" -180 180 180.00000000016422 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector2" -p "IKXTrackBottom_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector1" -p "IKXTrackBend_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_R" -p "IKXTrackBend_R";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" -32.92665997090225 0.00038472589023540974 -179.9970313876259 ;
	setAttr ".s" -type "double3" 0.99999999999999967 1 0.99999999999999989 ;
createNode annotationShape -n "PoleAnnotationLeg_RShape" -p "PoleAnnotationLeg_R";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintTrack_R_parentConstraint1" -p "IKParentConstraintTrack_R";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 3.9898529396578266 -0.31454308073907233 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintTrack_L" -p "IKParentConstraint";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "IKOffsetTrack_L" -p "IKParentConstraintTrack_L";
	setAttr ".t" -type "double3" 0.65512380987240437 4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" 26.580167854041889 179.99872209737146 -0.0016111659050619565 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode joint -n "IKXTrack_L" -p "IKOffsetTrack_L";
	setAttr ".t" -type "double3" 3.3306690738754696e-16 0 -6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" 1.3595476214611847e-19 1.206773958318607e-14 -7.1628554958552394e-15 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" -9.5416640443905471e-15 -7.970711368758071e-11 -3.7201172344677582e-11 ;
createNode joint -n "IKXTrackBend_L" -p "IKXTrack_L";
	setAttr ".t" -type "double3" 0 -1.2425429238991423 -0.061160511392438499 ;
	setAttr ".r" -type "double3" 9.0202358649796026e-14 -5.0591479052715504e-19 1.8181562587185387e-18 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" -59.50682786063134 -0.00049904335901457654 -0.0009688106821282371 ;
createNode joint -n "IKXTrackBottom_L" -p "IKXTrackBend_L";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 -1.351783977323028 0.040242726398744377 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 32.926661762095641 -0.00033961572143530384 0.0029978254302916369 ;
createNode joint -n "IKXFrontTrack_End_L" -p "IKXTrackBottom_L";
	setAttr ".t" -type "double3" -0.22774006241858868 -1.6785359357460978 -4.6938822643168132 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743455807798471e-05 0 -90.000002504478161 ;
createNode transform -n "IKFKAlignedLeg_L" -p "IKXTrackBottom_L";
	setAttr ".t" -type "double3" 1.6294337712441376e-07 2.2476509542457279e-10 3.2164915730881205e-09 ;
	setAttr ".r" -type "double3" 0 0 4.2501528055846918e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999933 ;
createNode orientConstraint -n "IKXTrackBottom_L_orientConstraint1" -p "IKXTrackBottom_L";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.7913780437534781e-06 179.99994625681458 1.6256881447867953e-10 ;
	setAttr ".o" -type "double3" 1.7913780501138005e-06 -179.99994625681458 0 ;
	setAttr ".rsrr" -type "double3" 7.583033279089693e-22 -2.6267735854535195e-20 -1.6803046143539897e-12 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector4" -p "IKXTrackBottom_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector3" -p "IKXTrackBend_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_L" -p "IKXTrackBend_L";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 147.07334002909775 0.00038472589020806122 -179.99703138762592 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 0.99999999999999989 ;
createNode annotationShape -n "PoleAnnotationLeg_LShape" -p "PoleAnnotationLeg_L";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintTrack_L_parentConstraint1" -p "IKParentConstraintTrack_L";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 3.9898529396578266 -0.31454308073907233 ;
	setAttr -k on ".w0";
createNode transform -n "IKHandle" -p "IKSystem";
createNode transform -n "IKParentConstraintLeg_R" -p "IKHandle";
	setAttr ".t" -type "double3" -0.65521043624417274 1.7932332913509297 -0.47243235652409776 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
createNode transform -n "IKExtraLeg_R" -p "IKParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251563e-16 0 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_R" -p "IKExtraLeg_R";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
createNode nurbsCurve -n "IKLeg_RShape" -p "IKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.496833481108094 -1.7625894128664179 5.5089773064271332
		-1.4417607033322799 -1.7625894128664179 5.5163158183530312
		-1.4417607033322799 1.6140428450296818 4.0370836956661664
		-1.4417607033322799 1.6140428450296818 -4.68563707468115
		-1.4417607033322799 -1.7625894128664179 -6.230991952479517
		0.496833481108094 -1.7625894128664179 -6.230991952479517
		0.496833481108094 1.6140428450296818 -4.68563707468115
		0.496833481108094 1.6140428450296818 4.0316346360599082
		0.496833481108094 -1.7625894128664179 5.5089773064271332
		0.496833481108094 -1.7625894128664179 -6.230991952479517
		0.496833481108094 1.6140428450296818 -4.68563707468115
		-1.4417607033322799 1.6140428450296818 -4.68563707468115
		-1.4417607033322799 -1.7625894128664179 -6.230991952479517
		-1.4417607033322799 -1.7625894128664179 5.5174756830317264
		-1.4417607033322799 1.6140428450296818 4.0370836956661664
		0.496833481108094 1.6140428450296818 4.0316346360599082
		;
createNode transform -n "IKFootRollLeg_R" -p "IKLeg_R";
	setAttr ".t" -type "double3" 1.1102230246251563e-16 0 0 ;
	setAttr ".r" -type "double3" 0 -2.7777800103461363 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
createNode transform -n "IKRollLegHeel_R" -p "IKFootRollLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.47638712805422517 -1.548344683005036 -5.1191927175450198 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 3.9756933518293969e-16 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_R" -p "IKRollLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_R" -p "IKExtraLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_RShape" -p "IKLegHeel_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1246872227451099e-17 0.31867514730767482 -0.31867514730767438
		-2.7595867378281874e-17 -5.1416596281931871e-17 -0.45067471531375736
		-9.0273322139266484e-17 -0.3186751473076746 -0.3186751473076746
		-1.000698891115442e-16 -0.45067471531375736 -1.3059425750677485e-16
		-5.1246872227451038e-17 -0.31867514730767466 0.31867514730767449
		2.759586737828184e-17 -1.3579708714511481e-16 0.45067471531375736
		9.027332213926646e-17 0.31867514730767438 0.3186751473076746
		1.0006988911154425e-16 0.45067471531375736 2.4205826874171928e-16
		5.1246872227451099e-17 0.31867514730767482 -0.31867514730767438
		-2.7595867378281874e-17 -5.1416596281931871e-17 -0.45067471531375736
		-9.0273322139266484e-17 -0.3186751473076746 -0.3186751473076746
		;
createNode transform -n "IKRollLegBall_R" -p "IKLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.47638712805421279 -0.13019121112151066 9.8185964756401329 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_R" -p "IKRollLegBall_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_R" -p "IKExtraLegBall_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_RShape" -p "IKLegBall_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1246872227451093e-17 0.31867514730767482 -0.31867514730767438
		-2.7595867378281889e-17 -5.1416596281931889e-17 -0.45067471531375736
		-9.0273322139266497e-17 -0.3186751473076746 -0.3186751473076746
		-1.000698891115442e-16 -0.45067471531375736 -1.3059425750677485e-16
		-5.1246872227451032e-17 -0.31867514730767466 0.31867514730767449
		2.7595867378281852e-17 -1.3579708714511476e-16 0.45067471531375736
		9.0273322139266484e-17 0.31867514730767438 0.3186751473076746
		1.0006988911154425e-16 0.45067471531375736 2.4205826874171928e-16
		5.1246872227451093e-17 0.31867514730767482 -0.31867514730767438
		-2.7595867378281889e-17 -5.1416596281931889e-17 -0.45067471531375736
		-9.0273322139266497e-17 -0.3186751473076746 -0.3186751473076746
		;
createNode transform -n "IKFootPivotBallReverseLeg_R" -p "IKLegBall_R";
	setAttr ".r" -type "double3" 0 2.7777800103461363 0 ;
createNode ikHandle -n "IKXLegHandle_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0.22774446526646719 1.6785358941265471 -4.6938819904359503 ;
	setAttr ".ro" 3;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_R_poleVectorConstraint1" -p "IKXLegHandle_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 3.5974623108558923e-05 -1.1925012112016558 2.1222535064386037 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 7.3208106243782806e-12 1.0513676862444754e-07 1.1273980771164815e-07 ;
	setAttr ".r" -type "double3" 1.7913780627990207e-06 5.3743185423684447e-05 180 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 1 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "PoleAimLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode aimConstraint -n "PoleAimLeg_R_aimConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -sn "w0" -ln "IKLeg_RW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -0.00016199315409887299 4.1112524382613609 -90.002259528862055 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_R_pointConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "IKXTrack_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -0.65512380987240437 3.9898529396578271 -0.31454308073907211 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 1 ;
createNode transform -n "PoleExtraLeg_R" -p "PoleParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_R" -p "PoleExtraLeg_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 0 0 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_RShape" -p "PoleLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-6.6565954689999989e-33 0.13501179590000001 -2.9978640870000002e-17
		-1.3313190939999996e-32 0.27002359170000001 -5.9957281749999998e-17
		1.7987184519999999e-16 8.993592262e-17 0.40503538760000002
		1.3313190939999996e-32 -0.27002359170000001 5.9957281749999998e-17
		6.6565954689999989e-33 -0.13501179590000001 2.9978640870000002e-17
		-1.7987184519999999e-16 -0.13501179590000001 -0.40503538760000002
		-1.7987184519999999e-16 0.13501179590000001 -0.40503538760000002
		-6.6565954689999989e-33 0.13501179590000001 -2.9978640870000002e-17
		;
createNode transform -n "PoleAnnotateTargetLeg_R" -p "PoleLeg_R";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_RShape" -p "PoleAnnotateTargetLeg_R";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_R_parentConstraint1" -p "PoleParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_RStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" 0 0 -2.2204460492503131e-16 ;
	setAttr ".tg[1].tot" -type "double3" 1.0372811457664546 7.6775805086470861e-05 2.2022867443512992 ;
	setAttr ".tg[1].tor" -type "double3" -4.1112524414473395 -1.5447036250333816e-13 
		90.002253714466505 ;
	setAttr ".lr" -type "double3" 7.9513867036591223e-16 1.5425690205098054e-13 2.4624450697893316e-14 ;
	setAttr ".rst" -type "double3" -0.65508783524929592 2.7973517284561717 1.807710425699532 ;
	setAttr ".rsrr" -type "double3" 1.8786042112770614e-30 7.7526020360673222e-14 2.7767733254183437e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintLeg_L" -p "IKHandle";
	setAttr ".t" -type "double3" 0.6552104362441723 1.793233291350929 -0.47243235652409626 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000004 ;
createNode transform -n "IKExtraLeg_L" -p "IKParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_L" -p "IKExtraLeg_L";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
createNode nurbsCurve -n "IKLeg_LShape" -p "IKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-0.49683348110809344 -1.7625894128664177 5.5089773064271315
		1.4417607033322803 -1.7625894128664177 5.5163158183530294
		1.4417607033322803 1.6140428450296824 4.0370836956661647
		1.4417607033322803 1.6140428450296824 -4.6856370746811518
		1.4417607033322803 -1.7625894128664177 -6.2309919524795188
		-0.49683348110809344 -1.7625894128664177 -6.2309919524795188
		-0.49683348110809344 1.6140428450296824 -4.6856370746811518
		-0.49683348110809344 1.6140428450296824 4.0316346360599065
		-0.49683348110809344 -1.7625894128664177 5.5089773064271315
		-0.49683348110809344 -1.7625894128664177 -6.2309919524795188
		-0.49683348110809344 1.6140428450296824 -4.6856370746811518
		1.4417607033322803 1.6140428450296824 -4.6856370746811518
		1.4417607033322803 -1.7625894128664177 -6.2309919524795188
		1.4417607033322803 -1.7625894128664177 5.5174756830317246
		1.4417607033322803 1.6140428450296824 4.0370836956661647
		-0.49683348110809344 1.6140428450296824 4.0316346360599065
		;
createNode transform -n "IKFootRollLeg_L" -p "IKLeg_L";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 0 ;
	setAttr ".r" -type "double3" 0 2.7777800103462682 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000004 ;
createNode transform -n "IKRollLegHeel_L" -p "IKFootRollLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.476387128054238 -1.5483446830050378 -5.1191927175450127 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 7.9513867036587919e-16 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_L" -p "IKRollLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_L" -p "IKExtraLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 3.975693351829396e-16 0 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_LShape" -p "IKLegHeel_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		4.4408920985006262e-16 0.31867514730767654 -0.31867514730767788
		6.6613381477509392e-16 1.7208456881689926e-15 -0.45067471531376135
		4.4408920985006262e-16 -0.31867514730767282 -0.31867514730767788
		-2.2204460492503131e-16 -0.45067471531375558 -3.5527136788005009e-15
		-1.1102230246251563e-15 -0.31867514730767288 0.31867514730767077
		-1.5543122344752192e-15 1.6375789613221059e-15 0.45067471531375425
		-1.3322676295501878e-15 0.3186751473076761 0.31867514730767077
		-4.4408920985006262e-16 0.45067471531375913 -3.5527136788005009e-15
		4.4408920985006262e-16 0.31867514730767654 -0.31867514730767788
		6.6613381477509392e-16 1.7208456881689926e-15 -0.45067471531376135
		4.4408920985006262e-16 -0.31867514730767282 -0.31867514730767788
		;
createNode transform -n "IKRollLegBall_L" -p "IKLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.47638712805423467 -0.13019121112150578 9.8185964756401347 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_L" -p "IKRollLegBall_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 3.975693351829396e-16 0 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_L" -p "IKExtraLegBall_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 3.975693351829396e-16 0 ;
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_LShape" -p "IKLegBall_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-4.4408920985006262e-16 0.31867514730767171 -0.31867514730768143
		-1.1102230246251563e-16 -3.1641356201816961e-15 -0.45067471531376491
		-3.3306690738754696e-16 -0.31867514730767771 -0.31867514730768187
		-9.9920072216264089e-16 -0.45067471531376047 -7.1054273576010019e-15
		-1.8873791418627657e-15 -0.31867514730767776 0.31867514730766722
		-2.1094237467877974e-15 -3.2474023470285829e-15 0.45067471531374986
		-1.9984014443252814e-15 0.31867514730767127 0.31867514730766722
		-1.2212453270876722e-15 0.45067471531375425 -7.1054273576010019e-15
		-4.4408920985006262e-16 0.31867514730767171 -0.31867514730768143
		-1.1102230246251563e-16 -3.1641356201816961e-15 -0.45067471531376491
		-3.3306690738754696e-16 -0.31867514730767771 -0.31867514730768187
		;
createNode transform -n "IKFootPivotBallReverseLeg_L" -p "IKLegBall_L";
	setAttr ".r" -type "double3" 0 -2.7777800103462682 0 ;
createNode ikHandle -n "IKXLegHandle_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -0.22774446526646919 1.6785358941265436 -4.6938819904359583 ;
	setAttr ".ro" 3;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_L_poleVectorConstraint1" -p "IKXLegHandle_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -3.5974623108003811e-05 -1.192501211201656 2.122253506438605 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -1.2132295168498786e-11 1.051361129267292e-07 1.1273980238257764e-07 ;
	setAttr ".r" -type "double3" 1.7913780627978326e-06 180.00005374318545 0 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1.0000000000000009 ;
	setAttr ".hs" 1;
	setAttr ".pv" -type "double3" 6.9388939039072284e-18 0 1 ;
	setAttr ".roc" yes;
createNode transform -n "PoleAimLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 1 ;
createNode aimConstraint -n "PoleAimLeg_L_aimConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -sn "w0" -ln "IKLeg_LW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" 0.00016199315382543682 4.1112524382613289 -89.997740471137945 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_L_pointConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "IKXTrack_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0.65512380987240404 3.9898529396578271 -0.31454308073907172 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1.0000000000000004 0.99999999999999989 1.0000000000000002 ;
createNode transform -n "PoleExtraLeg_L" -p "PoleParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_L" -p "PoleExtraLeg_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 0 0 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_LShape" -p "PoleLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-2.2204460492503131e-16 0.13501179589999923 -1.5543122344752192e-15
		-2.2204460492503131e-16 0.27002359169999934 -1.5543122344752192e-15
		-4.4408920985006262e-16 -8.8817841970012523e-16 0.4050353875999983
		-2.2204460492503131e-16 -0.27002359170000068 -1.5543122344752192e-15
		-2.2204460492503131e-16 -0.13501179590000101 -1.5543122344752192e-15
		0 -0.13501179590000101 -0.4050353876000013
		0 0.13501179589999923 -0.4050353876000013
		-2.2204460492503131e-16 0.13501179589999923 -1.5543122344752192e-15
		;
createNode transform -n "PoleAnnotateTargetLeg_L" -p "PoleLeg_L";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_LShape" -p "PoleAnnotateTargetLeg_L";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_L_parentConstraint1" -p "PoleParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_LStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" 1.037281145766455 -7.6775805096684913e-05 2.2022867443512997 ;
	setAttr ".tg[1].tor" -type "double3" -4.1112524414473093 -1.2023593459736154e-13 
		89.997746285533509 ;
	setAttr ".lr" -type "double3" -1.5902773407317473e-15 1.2006593922524775e-13 1.06846758830415e-14 ;
	setAttr ".rst" -type "double3" 0.65508783524929615 2.7973517284561722 1.8077104256995331 ;
	setAttr ".rsrr" -type "double3" -3.9756933518293679e-16 6.0032969612623863e-14 5.3423379415207506e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKStatic" -p "IKSystem";
createNode transform -n "PoleParentConstraintLeg_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -0.65508783524929581 2.7973517284561713 1.807710425699532 ;
createNode transform -n "PoleParentConstraintLeg_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 0.65508783524929604 2.7973517284561717 1.8077104256995331 ;
createNode transform -n "IKCrv" -p "IKSystem";
	setAttr ".it" no;
createNode transform -n "IKMessure" -p "IKSystem";
createNode transform -n "FKIKSystem" -p "MotionSystem";
createNode transform -n "FKIKParentConstraintLeg_R" -p "FKIKSystem";
createNode transform -n "FKIKLeg_R" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Track";
	setAttr -l on ".middleJoint" -type "string" "TrackBend";
	setAttr -l on ".endJoint" -type "string" "TrackBottom";
createNode nurbsCurve -n "FKIKLeg_RShape" -p "FKIKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-0.25215053900000001 -0.072043011150000003 -5.5988666819999994e-17
		-0.25215053900000001 -0.063037634760000003 -5.5988666819999994e-17
		-0.24314516259999999 -0.063037634760000003 -5.3989071580000002e-17
		-0.23413978620000001 -0.054032258360000002 -5.1989476330000002e-17
		-0.23413978620000001 0.054032258360000002 -5.1989476330000002e-17
		-0.24314516259999999 0.063037634760000003 -5.3989071580000002e-17
		-0.25215053900000001 0.063037634760000003 -5.5988666819999994e-17
		-0.25215053900000001 0.072043011150000003 -5.5988666819999994e-17
		-0.090053763940000003 0.072043011150000003 -1.9995952440000001e-17
		-0.090053763940000003 0.063037634760000003 -1.9995952440000001e-17
		-0.099059140330000003 0.063037634760000003 -2.1995547680000002e-17
		-0.1080645167 0.054032258360000002 -2.3995142920000006e-17
		-0.1080645167 0.009005376394 -2.3995142920000006e-17
		-0.054032258360000002 0.054032258360000002 -1.1997571460000001e-17
		-0.063037634760000003 0.063037634760000003 -1.3997166700000001e-17
		-0.072043011150000003 0.063037634760000003 -1.599676195e-17
		-0.072043011150000003 0.072043011150000003 -1.599676195e-17
		-0.018010752790000001 0.072043011150000003 -3.9991904870000001e-18
		-0.018010752790000001 0.063037634760000003 -3.9991904870000001e-18
		-0.027016129180000001 0.063037634760000003 -5.9987857309999997e-18
		-0.090053763940000003 0.009005376394 -1.9995952440000001e-17
		-0.018010752790000001 -0.063037634760000003 -3.9991904870000001e-18
		-0.009005376394 -0.063037634760000003 -1.999595244e-18
		0.036021505580000002 0.072043011150000003 7.9983809740000002e-18
		0.17110215149999999 0.072043011150000003 3.7992309629999995e-17
		0.17110215149999999 0.063037634760000003 3.7992309629999995e-17
		0.16209677510000001 0.063037634760000003 3.5992714380000002e-17
		0.15309139869999999 0.054032258360000002 3.3993119139999998e-17
		0.15309139869999999 0.009005376394 3.3993119139999998e-17
		0.2071236571 0.054032258360000002 4.5990690599999989e-17
		0.19811828070000001 0.063037634760000003 4.3991095360000003e-17
		0.1891129043 0.063037634760000003 4.1991500110000004e-17
		0.1891129043 0.072043011150000003 4.1991500110000004e-17
		0.24314516259999999 0.072043011150000003 5.3989071580000002e-17
		0.24314516259999999 0.063037634760000003 5.3989071580000002e-17
		0.23413978620000001 0.063037634760000003 5.1989476330000002e-17
		0.17110215149999999 0.009005376394 3.7992309629999995e-17
		0.24314516259999999 -0.063037634760000003 5.3989071580000002e-17
		0.25215053900000001 -0.063037634760000003 5.5988666819999994e-17
		0.25215053900000001 -0.072043011150000003 5.5988666819999994e-17
		0.19811828070000001 -0.072043011150000003 4.3991095360000003e-17
		0.19811828070000001 -0.063037634760000003 4.3991095360000003e-17
		0.2071236571 -0.063037634760000003 4.5990690599999989e-17
		0.15309139869999999 -0.009005376394 3.3993119139999998e-17
		0.15309139869999999 -0.054032258360000002 3.3993119139999998e-17
		0.16209677510000001 -0.063037634760000003 3.5992714380000002e-17
		0.17110215149999999 -0.063037634760000003 3.7992309629999995e-17
		0.17110215149999999 -0.072043011150000003 3.7992309629999995e-17
		0.1080645167 -0.072043011150000003 2.3995142920000006e-17
		0.1080645167 -0.063037634760000003 2.3995142920000006e-17
		0.1260752695 -0.063037634760000003 2.7994333409999997e-17
		0.13508064589999999 -0.054032258360000002 2.9993928650000001e-17
		0.13508064589999999 0.054032258360000002 2.9993928650000001e-17
		0.1260752695 0.063037634760000003 2.7994333409999997e-17
		0.099059140330000003 0.063037634760000003 2.1995547680000002e-17
		0.090053763940000003 0.054032258360000002 1.9995952440000001e-17
		0.090053763940000003 -0.054032258360000002 1.9995952440000001e-17
		0.099059140330000003 -0.063037634760000003 2.1995547680000002e-17
		0.1080645167 -0.063037634760000003 2.3995142920000006e-17
		0.1080645167 -0.072043011150000003 2.3995142920000006e-17
		0.054032258360000002 -0.072043011150000003 1.1997571460000001e-17
		0.054032258360000002 -0.063037634760000003 1.1997571460000001e-17
		0.063037634760000003 -0.063037634760000003 1.3997166700000001e-17
		0.072043011150000003 -0.054032258360000002 1.599676195e-17
		0.072043011150000003 0.054032258360000002 1.599676195e-17
		0.063037634760000003 0.063037634760000003 1.3997166700000001e-17
		0.045026881970000002 0.063037634760000003 9.997976217999999e-18
		0 -0.063037634760000003 0
		0 -0.072043011150000003 0
		-0.063037634760000003 -0.072043011150000003 -1.3997166700000001e-17
		-0.063037634760000003 -0.063037634760000003 -1.3997166700000001e-17
		-0.054032258360000002 -0.063037634760000003 -1.1997571460000001e-17
		-0.1080645167 -0.009005376394 -2.3995142920000006e-17
		-0.1080645167 -0.054032258360000002 -2.3995142920000006e-17
		-0.099059140330000003 -0.063037634760000003 -2.1995547680000002e-17
		-0.090053763940000003 -0.063037634760000003 -1.9995952440000001e-17
		-0.090053763940000003 -0.072043011150000003 -1.9995952440000001e-17
		-0.14408602230000001 -0.072043011150000003 -3.19935239e-17
		-0.14408602230000001 -0.063037634760000003 -3.19935239e-17
		-0.13508064589999999 -0.063037634760000003 -2.9993928650000001e-17
		-0.1260752695 -0.054032258360000002 -2.7994333409999997e-17
		-0.1260752695 0.054032258360000002 -2.7994333409999997e-17
		-0.13508064589999999 0.063037634760000003 -2.9993928650000001e-17
		-0.15309139869999999 0.063037634760000003 -3.3993119139999998e-17
		-0.15309139869999999 0.036021505580000002 -3.3993119139999998e-17
		-0.16209677510000001 0.036021505580000002 -3.5992714380000002e-17
		-0.16209677510000001 0.054032258360000002 -3.5992714380000002e-17
		-0.17110215149999999 0.063037634760000003 -3.7992309629999995e-17
		-0.2071236571 0.063037634760000003 -4.5990690599999989e-17
		-0.21612903350000001 0.054032258360000002 -4.799028585e-17
		-0.21612903350000001 0.009005376394 -4.799028585e-17
		-0.1891129043 0.009005376394 -4.1991500110000004e-17
		-0.18010752790000001 0.018010752790000001 -3.999190487e-17
		-0.18010752790000001 0.027016129180000001 -3.999190487e-17
		-0.17110215149999999 0.027016129180000001 -3.7992309629999995e-17
		-0.17110215149999999 -0.018010752790000001 -3.7992309629999995e-17
		-0.18010752790000001 -0.018010752790000001 -3.999190487e-17
		-0.18010752790000001 -0.009005376394 -3.999190487e-17
		-0.1891129043 0 -4.1991500110000004e-17
		-0.21612903350000001 0 -4.799028585e-17
		-0.21612903350000001 -0.054032258360000002 -4.799028585e-17
		-0.2071236571 -0.063037634760000003 -4.5990690599999989e-17
		-0.19811828070000001 -0.063037634760000003 -4.3991095360000003e-17
		-0.19811828070000001 -0.072043011150000003 -4.3991095360000003e-17
		-0.25215053900000001 -0.072043011150000003 -5.5988666819999994e-17
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_R_parentConstraint1" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.9158375285903333 -0.5491086016279465 -0.039424926763531154 ;
	setAttr ".rst" -type "double3" -1.9158375285903333 3.4407443380298801 -0.35396800750260349 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintLeg_L" -p "FKIKSystem";
createNode transform -n "FKIKLeg_L" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Track";
	setAttr -l on ".middleJoint" -type "string" "TrackBend";
	setAttr -l on ".endJoint" -type "string" "TrackBottom";
createNode nurbsCurve -n "FKIKLeg_LShape" -p "FKIKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		0.25215053900000051 -0.072043011170099369 2.4438562284956333e-11
		0.25215053900000051 -0.063037634780099605 2.4438562284956333e-11
		0.24314516260000071 -0.063037634780099605 2.4438562284956333e-11
		0.23413978620000053 -0.054032258380099389 2.4438562284956333e-11
		0.23413978620000053 0.054032258339900441 2.4438562284956333e-11
		0.24314516260000071 0.06303763473990065 2.4438562284956333e-11
		0.25215053900000051 0.06303763473990065 2.4438562284956333e-11
		0.25215053900000051 0.072043011129900414 2.4438562284956333e-11
		0.090053763940000753 0.072043011129900414 2.4438617796107565e-11
		0.090053763940000753 0.06303763473990065 2.4438617796107565e-11
		0.099059140330000517 0.06303763473990065 2.4438617796107565e-11
		0.10806451670000072 0.054032258339900441 2.4438617796107565e-11
		0.10806451670000072 0.0090053763739006421 2.4438617796107565e-11
		0.054032258360000585 0.054032258339900441 2.4438617796107565e-11
		0.063037634760000572 0.06303763473990065 2.4438617796107565e-11
		0.07204301115000078 0.06303763473990065 2.4438617796107565e-11
		0.07204301115000078 0.072043011129900414 2.4438617796107565e-11
		0.018010752790000639 0.072043011129900414 2.4438617796107565e-11
		0.018010752790000639 0.06303763473990065 2.4438617796107565e-11
		0.027016129180000625 0.06303763473990065 2.4438617796107565e-11
		0.090053763940000753 0.0090053763739006421 2.4438617796107565e-11
		0.018010752790000639 -0.063037634780099605 2.4438617796107565e-11
		0.0090053763940005638 -0.063037634780099605 2.4438617796107565e-11
		-0.03602150557999928 0.072043011129900414 2.4438617796107565e-11
		-0.1711021514999993 0.072043011129900414 2.4438673307258796e-11
		-0.1711021514999993 0.06303763473990065 2.4438673307258796e-11
		-0.16209677509999931 0.06303763473990065 2.4438673307258796e-11
		-0.15309139869999933 0.054032258339900441 2.4438673307258796e-11
		-0.15309139869999933 0.0090053763739006421 2.4438673307258796e-11
		-0.20712365709999925 0.054032258339900441 2.4438673307258796e-11
		-0.19811828069999929 0.06303763473990065 2.4438673307258796e-11
		-0.18911290429999927 0.06303763473990065 2.4438673307258796e-11
		-0.18911290429999927 0.072043011129900414 2.4438673307258796e-11
		-0.24314516259999941 0.072043011129900414 2.4438673307258796e-11
		-0.24314516259999941 0.06303763473990065 2.4438673307258796e-11
		-0.23413978619999945 0.06303763473990065 2.4438673307258796e-11
		-0.1711021514999993 0.0090053763739006421 2.4438673307258796e-11
		-0.24314516259999941 -0.063037634780099605 2.4438673307258796e-11
		-0.2521505389999994 -0.063037634780099605 2.4438673307258796e-11
		-0.2521505389999994 -0.072043011170099369 2.4438673307258796e-11
		-0.19811828069999929 -0.072043011170099369 2.4438673307258796e-11
		-0.19811828069999929 -0.063037634780099605 2.4438673307258796e-11
		-0.20712365709999925 -0.063037634780099605 2.4438673307258796e-11
		-0.15309139869999933 -0.0090053764140995973 2.4438673307258796e-11
		-0.15309139869999933 -0.054032258380099389 2.4438673307258796e-11
		-0.16209677509999931 -0.063037634780099605 2.4438673307258796e-11
		-0.1711021514999993 -0.063037634780099605 2.4438673307258796e-11
		-0.1711021514999993 -0.072043011170099369 2.4438673307258796e-11
		-0.10806451669999941 -0.072043011170099369 2.4438617796107565e-11
		-0.10806451669999941 -0.063037634780099605 2.4438617796107565e-11
		-0.12607526949999937 -0.063037634780099605 2.4438673307258796e-11
		-0.13508064589999935 -0.054032258380099389 2.4438673307258796e-11
		-0.13508064589999935 0.054032258339900441 2.4438673307258796e-11
		-0.12607526949999937 0.06303763473990065 2.4438673307258796e-11
		-0.099059140329999407 0.06303763473990065 2.4438617796107565e-11
		-0.090053763939999421 0.054032258339900441 2.4438617796107565e-11
		-0.090053763939999421 -0.054032258380099389 2.4438617796107565e-11
		-0.099059140329999407 -0.063037634780099605 2.4438617796107565e-11
		-0.10806451669999941 -0.063037634780099605 2.4438617796107565e-11
		-0.10806451669999941 -0.072043011170099369 2.4438617796107565e-11
		-0.054032258359999245 -0.072043011170099369 2.4438617796107565e-11
		-0.054032258359999245 -0.063037634780099605 2.4438617796107565e-11
		-0.063037634759999239 -0.063037634780099605 2.4438617796107565e-11
		-0.072043011149999447 -0.054032258380099389 2.4438617796107565e-11
		-0.072043011149999447 0.054032258339900441 2.4438617796107565e-11
		-0.063037634759999239 0.06303763473990065 2.4438617796107565e-11
		-0.045026881969999266 0.06303763473990065 2.4438617796107565e-11
		6.6613381477509392e-16 -0.063037634780099605 2.4438617796107565e-11
		6.6613381477509392e-16 -0.072043011170099369 2.4438617796107565e-11
		0.063037634760000572 -0.072043011170099369 2.4438617796107565e-11
		0.063037634760000572 -0.063037634780099605 2.4438617796107565e-11
		0.054032258360000585 -0.063037634780099605 2.4438617796107565e-11
		0.10806451670000072 -0.0090053764140995973 2.4438617796107565e-11
		0.10806451670000072 -0.054032258380099389 2.4438617796107565e-11
		0.099059140330000517 -0.063037634780099605 2.4438617796107565e-11
		0.090053763940000753 -0.063037634780099605 2.4438617796107565e-11
		0.090053763940000753 -0.072043011170099369 2.4438617796107565e-11
		0.14408602230000067 -0.072043011170099369 2.4438562284956333e-11
		0.14408602230000067 -0.063037634780099605 2.4438562284956333e-11
		0.13508064590000046 -0.063037634780099605 2.4438562284956333e-11
		0.1260752695000007 -0.054032258380099389 2.4438562284956333e-11
		0.1260752695000007 0.054032258339900441 2.4438562284956333e-11
		0.13508064590000046 0.06303763473990065 2.4438562284956333e-11
		0.15309139870000044 0.06303763473990065 2.4438562284956333e-11
		0.15309139870000044 0.036021505559900469 2.4438562284956333e-11
		0.16209677510000065 0.036021505559900469 2.4438562284956333e-11
		0.16209677510000065 0.054032258339900441 2.4438562284956333e-11
		0.17110215150000083 0.06303763473990065 2.4438562284956333e-11
		0.2071236571000008 0.06303763473990065 2.4438562284956333e-11
		0.21612903350000057 0.054032258339900441 2.4438562284956333e-11
		0.21612903350000057 0.0090053763739006421 2.4438562284956333e-11
		0.18911290430000083 0.0090053763739006421 2.4438562284956333e-11
		0.18010752790000062 0.018010752769900495 2.4438562284956333e-11
		0.18010752790000062 0.027016129159900704 2.4438562284956333e-11
		0.17110215150000083 0.027016129159900704 2.4438562284956333e-11
		0.17110215150000083 -0.018010752810099451 2.4438562284956333e-11
		0.18010752790000062 -0.018010752810099451 2.4438562284956333e-11
		0.18010752790000062 -0.0090053764140995973 2.4438562284956333e-11
		0.18911290430000083 -2.0099477637813834e-11 2.4438562284956333e-11
		0.21612903350000057 -2.0099477637813834e-11 2.4438562284956333e-11
		0.21612903350000057 -0.054032258380099389 2.4438562284956333e-11
		0.2071236571000008 -0.063037634780099605 2.4438562284956333e-11
		0.19811828070000059 -0.063037634780099605 2.4438562284956333e-11
		0.19811828070000059 -0.072043011170099369 2.4438562284956333e-11
		0.25215053900000051 -0.072043011170099369 2.4438562284956333e-11
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_L_parentConstraint1" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.9158375285903328 -0.54910860160784702 -0.039424926787969779 ;
	setAttr ".rst" -type "double3" 1.9158375285903328 3.4407443380499796 -0.35396800752704211 ;
	setAttr -k on ".w0";
createNode transform -n "RootSystem" -p "MotionSystem";
createNode transform -n "PelvisCenter_M" -p "RootSystem";
	setAttr ".t" -type "double3" 0 3.9898529396578266 -0.31454308073907233 ;
	setAttr ".ro" 3;
createNode transform -n "PelvisCenterBtwLegs_M" -p "RootSystem";
	setAttr ".t" -type "double3" -2.7755575615628914e-16 3.9898529396578266 -0.47243235652409699 ;
	setAttr ".ro" 3;
createNode orientConstraint -n "PelvisCenterBtwLegs_M_orientConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "PelvisCenterBtwLegs_M_pointConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -2.7755575615628914e-16 1.793233291350929 -0.47243235652409699 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PelvisCenterBtwLegsOffset_M" -p "PelvisCenterBtwLegs_M";
	setAttr ".t" -type "double3" 2.7755575615628914e-16 0 0.15788927578502465 ;
	setAttr ".ro" 3;
createNode transform -n "GlobalSystem" -p "MotionSystem";
createNode transform -n "GlobalOffsetShoulder_R" -p "GlobalSystem";
	setAttr ".t" -type "double3" -3.6796203529496991 9.110330558759264 -0.57591770566408362 ;
	setAttr ".r" -type "double3" -90.423191337643019 66.151133529518987 57.746349108900681 ;
createNode transform -n "GlobalShoulder_R" -p "GlobalOffsetShoulder_R";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 1.0000000000000004 ;
createNode transform -n "GlobalOffsetShoulder1_L" -p "GlobalSystem";
	setAttr ".t" -type "double3" 3.6796199999999977 9.11033 -0.5759179999999996 ;
	setAttr ".r" -type "double3" -89.576880631522471 66.151110946737418 122.25363815551005 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 0.99999999999999989 ;
createNode transform -n "GlobalShoulder1_L" -p "GlobalOffsetShoulder1_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -8.8817841970012523e-16 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "GameSkeleton" -p "Main";
createNode joint -n "Pelvis_M" -p "GameSkeleton";
	setAttr ".ro" 3;
createNode joint -n "Body_M" -p "Pelvis_M";
createNode joint -n "Head_M" -p "Body_M";
createNode joint -n "Head_End_M" -p "Head_M";
	setAttr ".t" -type "double3" -6.2302109906456059e-17 2.381127394958126 -4.4408920985006262e-16 ;
createNode parentConstraint -n "Head_M_parentConstraint1" -p "Head_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXHead_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 5.3531723602299 0.0036960628921906546 ;
	setAttr -k on ".w0";
createNode joint -n "Shoulder_R" -p "Body_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -90.42319256169904 66.151133529518987 57.746349108900667 ;
createNode joint -n "Elbow5_R" -p "Shoulder_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -7.3090613878761008 -3.8722079021756834 79.402292095114746 ;
createNode joint -n "Wrist1_R" -p "Elbow5_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -5.7425607969279184 0.80693606161428011 -7.72068907577769 ;
createNode joint -n "Elbow4_R" -p "Wrist1_R";
	setAttr ".t" -type "double3" 3.3306690738754696e-16 1.9711328197031084 -4.4408920985006262e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "Wrist1_R_parentConstraint1" -p "Wrist1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWrist1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -9.2469848331671135e-07 -2.4709512668342666e-07 -4.206469430457577e-07 ;
	setAttr ".rst" -type "double3" 9.8398414971612169e-08 1.3465937798800014 5.8980673678377116e-08 ;
	setAttr ".rsrr" -type "double3" -4.8949887035513155e-07 -1.0186563193187617e-06 
		-4.2464128750237778e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Elbow5_R_parentConstraint1" -p "Elbow5_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow5_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.0237473313246496e-06 -9.2732706653117413e-08 -3.5669750344044851e-06 ;
	setAttr ".rst" -type "double3" 2.7517154777001451e-08 3.856928244392599 8.2398678991069119e-08 ;
	setAttr ".rsrr" -type "double3" -7.9914261868661212e-07 -1.2841989583081738e-06 
		-3.7351240170658126e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Shoulder_R_parentConstraint1" -p "Shoulder_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.2240560233384389e-06 -7.4454348588713796e-07 -4.0877523638973094e-07 ;
	setAttr ".rst" -type "double3" -3.6796203529496991 4.2377096752836678 -0.26137462492501135 ;
	setAttr ".rsrr" -type "double3" 1.2240560340728112e-06 -7.4454349224824708e-07 -4.0877521969181867e-07 ;
	setAttr -k on ".w0";
createNode joint -n "Shoulder1_L" -p "Body_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -89.576880631522471 66.151110946737418 122.25363815551 ;
createNode joint -n "Elbow_L" -p "Shoulder1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 1.3435007434096222 3.8654477638432505 79.690681516023631 ;
createNode joint -n "Wrist_L" -p "Elbow_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 0 179.99999999999963 ;
createNode joint -n "MiddleFinger1_L" -p "Wrist_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 169.67208797348343 18.596473556984076 -31.569335316080458 ;
createNode joint -n "MiddleFinger2_L" -p "MiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -24.67610432924555 1.6853452105657427 0.19740030734301117 ;
createNode joint -n "MiddleFinger3_L" -p "MiddleFinger2_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -26.470794918004291 -1.1087635889674801 3.3567320730844119 ;
createNode joint -n "MiddleFinger4_End_L" -p "MiddleFinger3_L";
	setAttr ".t" -type "double3" 4.8849813083506888e-15 1.1257201477021019 2.6645352591003757e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 0 -179.99999999999994 ;
createNode parentConstraint -n "MiddleFinger3_L_parentConstraint1" -p "MiddleFinger3_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger3_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.4313774880420032e-07 -7.2321326574551569e-07 -8.145624799580859e-07 ;
	setAttr ".rst" -type "double3" 2.386987998370671e-07 0.90189600980864004 1.2681930083857651e-07 ;
	setAttr ".rsrr" -type "double3" 2.223124844986426e-06 2.6896144175137671e-06 -3.9153058156195445e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "MiddleFinger2_L_parentConstraint1" -p "MiddleFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.9570655367099142e-07 -9.311673958177045e-07 -4.2423851313191929e-07 ;
	setAttr ".rst" -type "double3" 1.6906263056171156e-07 0.41333305937313863 1.2184548392468739e-07 ;
	setAttr ".rsrr" -type "double3" 2.7423282269285024e-06 8.8811203528538409e-07 -4.6726167599868801e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "MiddleFinger1_L_parentConstraint1" -p "MiddleFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.1807548939342575e-06 -1.0454788654489017e-06 -1.7745737380893297e-06 ;
	setAttr ".rst" -type "double3" -0.061912705560136005 -3.4542914760550216 1.0264094803253636 ;
	setAttr ".rsrr" -type "double3" 2.3100891521893997e-06 -1.1580209707382678e-06 -4.6899120499362956e-06 ;
	setAttr -k on ".w0";
createNode joint -n "IndexFinger1_L" -p "Wrist_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 168.80885737457916 -31.283136883424053 27.677037704331749 ;
createNode joint -n "IndexFinger2_L" -p "IndexFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -26.583463999546964 -4.5493905738220821 1.0440798337855843 ;
createNode joint -n "IndexFinger3_L" -p "IndexFinger2_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -31.032146936174303 -0.0098581623075396901 0.06557227781160492 ;
createNode joint -n "IndexFinger4_End_L" -p "IndexFinger3_L";
	setAttr ".t" -type "double3" 3.1086244689504383e-15 1.1571989645814398 -8.8817841970012523e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 0 179.99999999999994 ;
createNode parentConstraint -n "IndexFinger3_L_parentConstraint1" -p "IndexFinger3_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger3_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.7458525203535609e-06 3.4853710167502776e-07 4.8348947744483466e-07 ;
	setAttr ".rst" -type "double3" 2.9447070559740496e-08 0.88370483022358293 2.7052933049276362e-07 ;
	setAttr ".rsrr" -type "double3" 7.2224878294106396e-06 1.0686438504512266e-06 9.4838517823873297e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IndexFinger2_L_parentConstraint1" -p "IndexFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.5874009027887454e-07 5.5155370368837481e-07 2.3522539253791763e-07 ;
	setAttr ".rst" -type "double3" 1.6760999699272361e-08 0.44186262643838325 2.1698243557466412e-07 ;
	setAttr ".rsrr" -type "double3" 5.7343866060889584e-06 1.414527184809022e-06 2.6329804301950021e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IndexFinger1_L_parentConstraint1" -p "IndexFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 3.1759906109240945e-06 6.6461700227027128e-07 1.6479011489750053e-06 ;
	setAttr ".rst" -type "double3" 0.7649340889356433 -3.2962525454891431 0.96784824563351712 ;
	setAttr ".rsrr" -type "double3" 5.4718322691252481e-06 1.5487748969115791e-06 7.4380775394886522e-08 ;
	setAttr -k on ".w0";
createNode joint -n "ThumbFinger1_L" -p "Wrist_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 71.954894358795897 -34.212659586182298 161.29689442147202 ;
createNode joint -n "ThumbFinger2_L" -p "ThumbFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -24.897308891444723 -0.92612325257246864 -2.9890787396928067 ;
createNode joint -n "ThumbFinger3_L" -p "ThumbFinger2_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -30.166111111275189 -8.0298885346612838 -2.2247551139713804 ;
createNode joint -n "ThumbFinger4_End_L" -p "ThumbFinger3_L";
	setAttr ".t" -type "double3" 2.2204460492503131e-15 1.0809764280246541 -3.1086244689504383e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 0 -179.99999999999957 ;
createNode parentConstraint -n "ThumbFinger3_L_parentConstraint1" -p "ThumbFinger3_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger3_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.2140109950052153e-06 -4.2019814784807455e-07 2.6707871786485699e-07 ;
	setAttr ".rst" -type "double3" -1.5227210781176836e-07 0.83547858591077873 -6.4425801582501663e-08 ;
	setAttr ".rsrr" -type "double3" 1.0110405936329978e-06 8.8942967251311082e-07 4.7269210243565988e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ThumbFinger2_L_parentConstraint1" -p "ThumbFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 6.6304445210394443e-07 -2.3982762364947729e-07 5.9403031836894386e-07 ;
	setAttr ".rst" -type "double3" -1.0416851647931935e-07 0.52153988474986956 -1.740316346587889e-08 ;
	setAttr ".rsrr" -type "double3" -8.4263722638785853e-07 3.1945880347790762e-06 3.5923911803265014e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ThumbFinger1_L_parentConstraint1" -p "ThumbFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.8425766580814714e-06 1.8687618000486895e-06 6.5535594612195292e-07 ;
	setAttr ".rst" -type "double3" 0.56809763177244643 -2.0282457103593945 1.8256325445574708 ;
	setAttr ".rsrr" -type "double3" -1.2957294007767123e-06 4.451146065682181e-06 1.9046918492650691e-06 ;
	setAttr -k on ".w0";
createNode joint -n "PinkyFinger1_L" -p "Wrist_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 144.18219713699378 34.890410049681208 -90.561469420018554 ;
createNode joint -n "PinkyFinger2_L" -p "PinkyFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -24.642713841050881 -5.0469366939431426 -1.2775891951054352 ;
createNode joint -n "PinkyFinger3_L" -p "PinkyFinger2_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -24.029420870874819 -0.050481333804833155 0.59267354664134486 ;
createNode joint -n "PinkyFinger4_End_L" -p "PinkyFinger3_L";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 0.99669641313246915 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 0 -179.99999999999986 ;
createNode parentConstraint -n "PinkyFinger3_L_parentConstraint1" -p "PinkyFinger3_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXPinkyFinger3_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.4489259270773772e-06 -1.7626933775157779e-07 1.3794156312492877e-07 ;
	setAttr ".rst" -type "double3" 1.5006956299856713e-07 0.80315159270969394 -7.6198796161719415e-08 ;
	setAttr ".rsrr" -type "double3" -3.2050092476382419e-06 2.7752275698885093e-06 -3.2301035873283134e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "PinkyFinger2_L_parentConstraint1" -p "PinkyFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXPinkyFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -7.6344774110575909e-07 -1.1472210102089276e-07 2.1853243743819003e-07 ;
	setAttr ".rst" -type "double3" 1.2965281293020323e-07 0.3709503904038578 2.4282450716839321e-08 ;
	setAttr ".rsrr" -type "double3" -2.5454076741116909e-06 2.493371009344561e-06 -1.1403693933455104e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "PinkyFinger1_L_parentConstraint1" -p "PinkyFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXPinkyFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -3.9416414563444513e-07 1.5476063317818493e-06 9.9886718177661821e-08 ;
	setAttr ".rst" -type "double3" -0.68886319653134276 -2.9794437304407855 1.2606809670272527 ;
	setAttr ".rsrr" -type "double3" -1.9242794072826256e-06 1.8385296285624029e-06 -2.3704778754127997e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Wrist_L_parentConstraint1" -p "Wrist_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWrist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 0 5.0089567047863747e-06 ;
	setAttr ".rst" -type "double3" 4.2493009499988688e-08 0.83264493814599794 -8.0293781401508113e-08 ;
	setAttr ".rsrr" -type "double3" 1.2098348603506319e-06 5.6124974805196055e-14 2.6579860500001103e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Elbow_L_parentConstraint1" -p "Elbow_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.2098348900120835e-06 6.4710373832274245e-07 -2.3509706322875548e-06 ;
	setAttr ".rst" -type "double3" 2.260179199353729e-08 3.856924998330145 -6.3668267102912068e-08 ;
	setAttr ".rsrr" -type "double3" -1.2098348884218057e-06 6.4710376615259544e-07 -2.3509706198386645e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Shoulder1_L_parentConstraint1" -p "Shoulder1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -9.4581113191705806e-07 -1.1191011573120332e-06 -3.3575638056018083e-07 ;
	setAttr ".rst" -type "double3" 3.6796199999999977 4.2377091165244032 -0.26137491926092732 ;
	setAttr ".rsrr" -type "double3" -9.4581115100038584e-07 -1.1191011382287056e-06 
		-3.3575635829629806e-07 ;
	setAttr -k on ".w0";
createNode joint -n "Hose1_R" -p "Body_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -90.000000000000028 0 0 ;
createNode joint -n "Hose1_End_R" -p "Hose1_R";
	setAttr ".t" -type "double3" 0 1.2444681704849083 0 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 90.000000000000028 0 0 ;
createNode parentConstraint -n "Hose1_R_parentConstraint1" -p "Hose1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHose1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.5044781227376505e-06 0 0 ;
	setAttr ".rst" -type "double3" -2.1689490945289349 -1.0831980499384857 -4.934994677142595 ;
	setAttr ".rsrr" -type "double3" -2.5044781227376505e-06 0 0 ;
	setAttr -k on ".w0";
createNode joint -n "Hose2_R" -p "Body_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -90.000000000000028 0 0 ;
createNode joint -n "Hose2_End_R" -p "Hose2_R";
	setAttr ".t" -type "double3" 5.3290705182007514e-15 1.2444681704849128 2.6645352591003757e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 90.000000000000028 0 0 ;
createNode parentConstraint -n "Hose2_R_parentConstraint1" -p "Hose2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHose2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.5044781227376505e-06 0 0 ;
	setAttr ".rst" -type "double3" -6.3782461254304419 -2.2020769169102494 -3.3864248464098754 ;
	setAttr ".rsrr" -type "double3" -2.5044781227376505e-06 0 0 ;
	setAttr -k on ".w0";
createNode joint -n "Hose1_L" -p "Body_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 90.000000000000014 0 0 ;
createNode joint -n "Hose1_End_L" -p "Hose1_L";
	setAttr ".t" -type "double3" 0 -1.2444681704849083 -4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 90.000000000000014 0 0 ;
createNode parentConstraint -n "Hose1_L_parentConstraint1" -p "Hose1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHose1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.5044781418209783e-06 0 0 ;
	setAttr ".rst" -type "double3" 2.1689490945289349 -1.0831980499384852 -4.9349946771425941 ;
	setAttr ".rsrr" -type "double3" -2.5044781418209783e-06 0 0 ;
	setAttr -k on ".w0";
createNode joint -n "Hose2_L" -p "Body_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 90.000000000000014 0 0 ;
createNode joint -n "Hose2_End_L" -p "Hose2_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -1.2444681704849101 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 90.000000000000014 0 0 ;
createNode parentConstraint -n "Hose2_L_parentConstraint1" -p "Hose2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHose2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.5044781418209783e-06 0 0 ;
	setAttr ".rst" -type "double3" 6.3782461254304454 -2.2020769169102499 -3.3864248464098754 ;
	setAttr ".rsrr" -type "double3" -2.5044781418209783e-06 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Body_M_parentConstraint1" -p "Body_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXBody_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 0.88276794381776957 5.5511151231257827e-17 ;
	setAttr -k on ".w0";
createNode joint -n "Track_R" -p "Pelvis_M";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 26.580167889976693 -0.0012779026280242873 179.99838883409453 ;
createNode joint -n "TrackBend_R" -p "Track_R";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" -59.50683096334518 -0.00049904337142943724 -0.00096881069895129795 ;
createNode joint -n "TrackBottom_R" -p "TrackBend_R";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 32.926663073431406 -0.00033961572561536178 0.002997825343230869 ;
createNode joint -n "Wheel1_R" -p "TrackBottom_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277296665505e-05 5.3743454204439458e-05 -89.99999999994975 ;
createNode joint -n "Wheel1_End_R" -p "Wheel1_R";
	setAttr ".t" -type "double3" 3.1086244689504383e-15 1.4116922076074281 4.3551828809995641e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743454204462917e-05 -1.5208142013672443e-20 -89.999999999999815 ;
createNode parentConstraint -n "Wheel1_R_parentConstraint1" -p "Wheel1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.7849952434265245e-10 1.6002224883586794e-12 -2.5044780145169268e-06 ;
	setAttr ".rst" -type "double3" 0.32672497678868961 0.51822117956942559 3.737606835020892 ;
	setAttr ".rsrr" -type "double3" 1.7849742456194866e-10 1.6389772421782639e-12 -2.5044780208780553e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel2_R" -p "TrackBottom_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.374327729666562e-05 5.3743454123331072e-05 -89.999999999949651 ;
createNode joint -n "Wheel2_End_R" -p "Wheel2_R";
	setAttr ".t" -type "double3" 3.1086244689504383e-15 1.411692207607429 4.3564041263266517e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743454123354654e-05 9.9704604209627972e-22 -89.999999999999929 ;
createNode parentConstraint -n "Wheel2_R_parentConstraint1" -p "Wheel2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.752426362951723e-10 -1.5755572375282373e-12 -2.5044780972113864e-06 ;
	setAttr ".rst" -type "double3" 0.13546603245041799 0.078356787930575558 1.3998736841503581 ;
	setAttr ".rsrr" -type "double3" 1.7524755320756086e-10 -1.5438191434186498e-12 -2.5044780717669622e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel3_R" -p "TrackBottom_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277296665648e-05 5.374345417740325e-05 -89.999999999949594 ;
createNode joint -n "Wheel3_End_R" -p "Wheel3_R";
	setAttr ".t" -type "double3" 2.6645352591003757e-15 1.4116922076074281 4.3587355946783646e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743454177426845e-05 3.9881839282563691e-22 -89.999999999999972 ;
createNode parentConstraint -n "Wheel3_R_parentConstraint1" -p "Wheel3_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel3_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.7524263631609188e-10 -1.629629457669442e-12 -2.5044781481002356e-06 ;
	setAttr ".rst" -type "double3" 0.13546856143442201 0.078356787930582886 -1.2962795322063323 ;
	setAttr ".rsrr" -type "double3" 1.7524755321383481e-10 -1.597891345360565e-12 -2.504478116294702e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel4_R" -p "TrackBottom_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.374327729666562e-05 5.3743453970126245e-05 -89.999999999949651 ;
createNode joint -n "Wheel4_End_R" -p "Wheel4_R";
	setAttr ".t" -type "double3" 4.2188474935755949e-15 1.4116922076074343 4.3574033270488144e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743453970149826e-05 -2.0361670994482629e-21 -89.999999999999929 ;
createNode parentConstraint -n "Wheel4_R_parentConstraint1" -p "Wheel4_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel4_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.7524263631892344e-10 -1.4223523933815454e-12 -2.5044780972114584e-06 ;
	setAttr ".rst" -type "double3" 0.38140015290257923 0.59898164392296649 -3.6256848883138311 ;
	setAttr ".rsrr" -type "double3" 1.7524755320097986e-10 -1.3906143296040907e-12 -2.5044780717670342e-06 ;
	setAttr -k on ".w0";
createNode joint -n "FrontTrack_End_R" -p "TrackBottom_R";
	setAttr ".t" -type "double3" 0.22774006241858857 1.6785359357460985 4.6938822643168141 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277296642019e-05 0 -90.000000000000071 ;
createNode parentConstraint -n "TrackBottom_R_parentConstraint1" -p "TrackBottom_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrackBottom_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrackBottom_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.3113357620998162e-06 5.0831799083604901e-11 7.0803802270394435e-11 ;
	setAttr ".rst" -type "double3" -8.147344465747608e-08 1.3517839802778662 -0.040242654607645312 ;
	setAttr ".rsrr" -type "double3" 1.7494292248863372e-06 -7.2816421053261729e-11 2.1252617781934994e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "TrackBend_R_parentConstraint1" -p "TrackBend_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrackBend_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrackBend_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 3.1027138373271784e-06 -8.1965148294947736e-12 1.9234377512842662e-11 ;
	setAttr ".rst" -type "double3" -4.0199993889267205e-08 1.242542923943867 0.061160510483788455 ;
	setAttr ".rsrr" -type "double3" 3.0608148710285893e-06 -1.1551274153737243e-06 1.78377572732752e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Track_R_parentConstraint1" -p "Track_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrack_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrack_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -2.2608408809167789e-30 -1.1385275360910266e-14 -2.2755117732490919e-14 ;
	setAttr ".rst" -type "double3" -0.65512380987240437 0 4.9960036108132044e-16 ;
	setAttr ".rsrr" -type "double3" -4.1899479061161305e-08 9.5089658421024625e-07 1.9004955217560036e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "Track_L" -p "Pelvis_M";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 26.580167889976703 179.99872209737197 0.0016111659054506397 ;
createNode joint -n "TrackBend_L" -p "Track_L";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" -59.50683096334518 -0.00049904337141038575 -0.00096881069899052168 ;
createNode joint -n "TrackBottom_L" -p "TrackBend_L";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 32.926663073431421 -0.00033961572562681946 0.002997825343191594 ;
createNode joint -n "Wheel1_L" -p "TrackBottom_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277271221022e-05 5.3743454211792158e-05 -89.999999999949779 ;
createNode joint -n "Wheel1_End_L" -p "Wheel1_L";
	setAttr ".t" -type "double3" -2.4424906541753444e-15 -1.4116922076074272 -4.3560710594192642e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743454211815617e-05 -3.0332133116374176e-21 -89.999999999999773 ;
createNode parentConstraint -n "Wheel1_L_parentConstraint1" -p "Wheel1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.7854371634579095e-10 1.6023093016393054e-12 -2.5045507792469097e-06 ;
	setAttr ".rst" -type "double3" -0.32672497678868895 -0.51822117956942848 -3.7376068350208911 ;
	setAttr ".rsrr" -type "double3" 1.7857400677659199e-10 1.6023440131838685e-12 -2.5044779699891269e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel2_L" -p "TrackBottom_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277271221164e-05 5.3743454110014285e-05 -89.999999999949651 ;
createNode joint -n "Wheel2_End_L" -p "Wheel2_L";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -1.4116922076074301 -4.3565151486291143e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743454110037873e-05 -3.0332133116374183e-21 -89.999999999999929 ;
createNode parentConstraint -n "Wheel2_L_parentConstraint1" -p "Wheel2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.7528682832346001e-10 -1.5528009621854732e-12 -2.5045509001080356e-06 ;
	setAttr ".rst" -type "double3" -0.13546603245041877 -0.078356787930575558 -1.3998736841503587 ;
	setAttr ".rsrr" -type "double3" 1.7529167431773356e-10 -1.5527662258211764e-12 -2.5044780972113737e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel3_L" -p "TrackBottom_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277271221198e-05 5.3743454186347566e-05 -89.999999999949623 ;
createNode joint -n "Wheel3_End_L" -p "Wheel3_L";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -1.4116922076074292 -4.3578474162586645e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743454186371195e-05 0 -90 ;
createNode parentConstraint -n "Wheel3_L_parentConstraint1" -p "Wheel3_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel3_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.7528682832194107e-10 -1.6291342624077736e-12 -2.5045509191913279e-06 ;
	setAttr ".rst" -type "double3" -0.13546856143442121 -0.078356787930582886 1.296279532206333 ;
	setAttr ".rsrr" -type "double3" 1.7529167432228106e-10 -1.629099526043477e-12 -2.5044781162946668e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel4_L" -p "TrackBottom_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277271221164e-05 5.3743453982792084e-05 -89.999999999949651 ;
createNode joint -n "Wheel4_End_L" -p "Wheel4_L";
	setAttr ".t" -type "double3" -3.3306690738754696e-15 -1.4116922076074334 -4.354738791789714e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743453982815686e-05 -3.0332133116374183e-21 -89.999999999999929 ;
createNode parentConstraint -n "Wheel4_L_parentConstraint1" -p "Wheel4_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel4_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.7528682831806421e-10 -1.4255787567276523e-12 -2.5045509001080953e-06 ;
	setAttr ".rst" -type "double3" -0.38140015290257706 -0.59898164392296582 3.6256848883138311 ;
	setAttr ".rsrr" -type "double3" 1.7529167431307364e-10 -1.4255440446290625e-12 -2.5044780972114338e-06 ;
	setAttr -k on ".w0";
createNode joint -n "FrontTrack_End_L" -p "TrackBottom_L";
	setAttr ".t" -type "double3" -0.22774006241858891 -1.6785359357460976 -4.6938822643168132 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277271197576e-05 3.0332133116374183e-21 -90.000000000000071 ;
createNode parentConstraint -n "TrackBottom_L_parentConstraint1" -p "TrackBottom_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrackBottom_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrackBottom_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.3113357811827287e-06 5.0862764843155227e-11 7.0830540312304939e-11 ;
	setAttr ".rst" -type "double3" 8.1472168123042366e-08 -1.3517839802778668 0.04024265460764398 ;
	setAttr ".rsrr" -type "double3" 1.7494292119438519e-06 -9.7829175280015742e-11 2.1252303434394996e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "TrackBend_L_parentConstraint1" -p "TrackBend_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrackBend_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrackBend_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 3.10271383732751e-06 -8.2399808248763606e-12 1.9237864354161012e-11 ;
	setAttr ".rst" -type "double3" 4.0199665041207311e-08 -1.2425429239438677 -0.061160510483788899 ;
	setAttr ".rsrr" -type "double3" 3.0608148712630514e-06 -1.1551313566088013e-06 1.7837357084399501e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Track_L_parentConstraint1" -p "Track_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrack_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrack_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0.65512380987240404 4.4408920985006262e-16 6.106226635438361e-16 ;
	setAttr ".rsrr" -type "double3" -4.1899488602080112e-08 9.5086012516900368e-07 1.9004785830220413e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "Pelvis_M_pointConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 3.9898529396578266 -0.31454308073907233 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "Pelvis_M_orientConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode nurbsCurve -n "forwardArrowShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-0.24691114425928079 5.4825287478639397e-17 2.033374296558649
		-0.49382228851856158 1.0965057495727879e-16 2.033374296558649
		-1.6447586243591821e-16 3.2895172487183661e-16 2.7741077293364915
		0.49382228851856158 -1.0965057495727879e-16 2.033374296558649
		0.24691114425928079 -5.4825287478639397e-17 2.033374296558649
		0.24691114425928101 -3.8377701235047599e-16 1.2926408637808069
		-0.24691114425928068 -2.7412643739319724e-16 1.2926408637808069
		-0.24691114425928079 5.4825287478639397e-17 2.033374296558649
		;
createNode transform -n "items" -p "ctrl_rig";
	setAttr -l on ".it" no;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -sn "imrVerbosity" -ln "imrVerbosity" -dv 3 -at "short";
	addAttr -ci true -sn "imrThreads" -ln "imrThreads" -dv 1 -at "short";
	addAttr -ci true -sn "imrThreadsAdjust" -ln "imrThreadsAdjust" -dv 1 -min 0 -max 
		1 -at "bool";
	addAttr -ci true -sn "imrTaskOrder" -ln "imrTaskOrder" -dv 1 -at "short";
	addAttr -ci true -sn "imrTaskSize" -ln "imrTaskSize" -at "short";
	addAttr -ci true -sn "imrTaskAdjust" -ln "imrTaskAdjust" -dv 1 -at "short";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
	setAttr ".imrThreads" 4;
	setAttr ".imrThreadsAdjust" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode shadingEngine -n "irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode shadingEngine -n "BPRig_irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "BPRig_materialInfo3";
createNode ikRPsolver -n "ikRPsolver";
createNode ikSCsolver -n "ikSCsolver";
createNode displayLayer -n "jointLayer";
createNode reverse -n "jointVisReverse";
createNode objectSet -n "ControlSet";
	setAttr ".ihi" 0;
	setAttr -s 97 ".dsm";
createNode objectSet -n "GameSet";
	setAttr ".ihi" 0;
	setAttr -s 39 ".dsm";
createNode objectSet -n "AllSet";
	setAttr ".ihi" 0;
	setAttr -s 449 ".dsm";
	setAttr -s 38 ".dnsm";
createNode objectSet -n "Sets";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dnsm";
createNode unitConversion -n "unitConversion1";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_R";
createNode condition -n "FKIKBlendLegCondition_R";
createNode setRange -n "FKIKBlendLegsetRange_R";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "Leg_RAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_R";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion3";
	setAttr ".cf" 0.017453292519943295;
createNode blendColors -n "ScaleBlendTrackBottom_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendTrackBend_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendTrack_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode unitConversion -n "GlobalShoulder_unitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "GlobalShoulder_reverse_R";
createNode unitConversion -n "GlobalShoulder1_unitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "GlobalShoulder1_reverse_L";
createNode unitConversion -n "unitConversion4";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_L";
createNode condition -n "FKIKBlendLegCondition_L";
createNode setRange -n "FKIKBlendLegsetRange_L";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "Leg_LAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_L";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion5";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion6";
	setAttr ".cf" 0.017453292519943295;
createNode blendColors -n "ScaleBlendTrackBottom_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendTrackBend_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendTrack_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode setRange -n "CenterBtwFeet_M";
	setAttr ".v" -type "float3" 0 1 0 ;
	setAttr ".n" -type "float3" 2 0 0 ;
	setAttr ".om" -type "float3" 0.1 0 0 ;
createNode unitConversion -n "PelvisCenterBtwFeetUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "PelvisCenterBtwFeetReverse_M";
createNode dagPose -n "buildPose";
	addAttr -ci true -sn "udAttr" -ln "udAttr" -dt "string";
	setAttr ".udAttr" -type "string" (
		"setAttr IKExtraLeg_L.translateX 0;setAttr IKExtraLeg_L.translateY -2.220446049e-16;setAttr IKExtraLeg_L.translateZ 0;setAttr IKExtraLeg_L.rotateX -0;setAttr IKExtraLeg_L.rotateY -0;setAttr IKExtraLeg_L.rotateZ 0;setAttr IKLeg_L.translateX 0;setAttr IKLeg_L.translateY 0;setAttr IKLeg_L.translateZ 0;setAttr IKLeg_L.rotateX -0;setAttr IKLeg_L.rotateY -0;setAttr IKLeg_L.rotateZ 0;setAttr IKLeg_L.swivel 0;setAttr IKLeg_L.roll 0;setAttr IKLeg_L.rollAngle 25;setAttr PoleExtraLeg_L.translateX 0;setAttr PoleExtraLeg_L.translateY 0;setAttr PoleExtraLeg_L.translateZ 0;setAttr PoleLeg_L.translateX 0;setAttr PoleLeg_L.translateY 0;setAttr PoleLeg_L.translateZ -2.220446049e-16;setAttr PoleLeg_L.follow 10;setAttr FKIKLeg_L.FKIKBlend 10;setAttr FKIKLeg_L.FKVis 0;setAttr FKIKLeg_L.IKVis 1;setAttr IKExtraLegHeel_L.rotateX 0;setAttr IKExtraLegHeel_L.rotateY -0;setAttr IKExtraLegHeel_L.rotateZ 0;setAttr IKLegHeel_L.rotateX 0;setAttr IKLegHeel_L.rotateY 3.975693352e-16;setAttr IKLegHeel_L.rotateZ 0;setAttr IKExtraLegBall_L.rotateX 0;setAttr IKExtraLegBall_L.rotateY 3.975693352e-16;setAttr IKExtraLegBall_L.rotateZ 0;setAttr IKLegBall_L.rotateX 0;setAttr CenterExtra_M.translateX 0;setAttr CenterExtra_M.translateY 0;setAttr CenterExtra_M.translateZ 0;setAttr CenterExtra_M.rotateX 0;setAttr CenterExtra_M.rotateY -0;setAttr CenterExtra_M.rotateZ 0;setAttr Center_M.translateX 0;setAttr Center_M.translateY 0;setAttr Center_M.translateZ 0;setAttr Center_M.rotateX 0;setAttr Center_M.rotateY -0;setAttr Center_M.rotateZ 0;setAttr Center_M.CenterBtwFeet 0;setAttr Main.visibility 1;setAttr Main.translateX 0;setAttr Main.translateY 0;setAttr Main.translateZ 0;setAttr Main.rotateX 0;setAttr Main.rotateY 0;setAttr Main.rotateZ 0;setAttr Main.scaleX 1;setAttr Main.scaleY 1;setAttr Main.scaleZ 1;setAttr FKExtraHead_M.translateX 0;setAttr FKExtraHead_M.translateY 0;setAttr FKExtraHead_M.translateZ 0;setAttr FKExtraHead_M.rotateX 0;setAttr FKExtraHead_M.rotateY -0;setAttr FKExtraHead_M.rotateZ 0;setAttr FKExtraHead_M.scaleX 1;setAttr FKExtraHead_M.scaleY 1;setAttr FKExtraHead_M.scaleZ 1;setAttr FKHead_M.translateX 0;setAttr FKHead_M.translateY 0;setAttr FKHead_M.translateZ 0;setAttr FKHead_M.rotateX 0;setAttr FKHead_M.rotateY -0;setAttr FKHead_M.rotateZ 0;setAttr FKHead_M.scaleX 1;setAttr FKHead_M.scaleY 1;setAttr FKHead_M.scaleZ 1;setAttr FKExtraWrist1_R.translateX 3.60822483e-16;setAttr FKExtraWrist1_R.translateY -8.881784197e-16;setAttr FKExtraWrist1_R.translateZ 0;setAttr FKExtraWrist1_R.rotateX -0;setAttr FKExtraWrist1_R.rotateY 0;setAttr FKExtraWrist1_R.rotateZ -0;setAttr FKExtraWrist1_R.scaleX 1;setAttr FKExtraWrist1_R.scaleY 1;setAttr FKExtraWrist1_R.scaleZ 1;setAttr FKWrist1_R.translateX 4.996003611e-16;setAttr FKWrist1_R.translateY 0;setAttr FKWrist1_R.translateZ -1.776356839e-15;setAttr FKWrist1_R.rotateX -0;setAttr FKWrist1_R.rotateY 0;setAttr FKWrist1_R.rotateZ -0;setAttr FKWrist1_R.scaleX 1;setAttr FKWrist1_R.scaleY 1;setAttr FKWrist1_R.scaleZ 1;setAttr FKExtraElbow5_R.translateX 4.718447855e-16;setAttr FKExtraElbow5_R.translateY 8.881784197e-16;setAttr FKExtraElbow5_R.translateZ 0;setAttr FKExtraElbow5_R.rotateX -0;setAttr FKExtraElbow5_R.rotateY 0;setAttr FKExtraElbow5_R.rotateZ -0;setAttr FKExtraElbow5_R.scaleX 1;setAttr FKExtraElbow5_R.scaleY 1;setAttr FKExtraElbow5_R.scaleZ 1;setAttr FKElbow5_R.translateX 8.604228441e-16;setAttr FKElbow5_R.translateY 8.881784197e-16;setAttr FKElbow5_R.translateZ 0;setAttr FKElbow5_R.rotateX -0;setAttr FKElbow5_R.rotateY 0;setAttr FKElbow5_R.rotateZ -0;setAttr FKElbow5_R.scaleX 1;setAttr FKElbow5_R.scaleY 1;setAttr FKElbow5_R.scaleZ 1;setAttr FKExtraShoulder_R.translateX -4.440892099e-16;setAttr FKExtraShoulder_R.translateY 8.881784197e-16;setAttr FKExtraShoulder_R.translateZ -8.881784197e-16;setAttr FKExtraShoulder_R.rotateX -0;setAttr FKExtraShoulder_R.rotateY 0;setAttr FKExtraShoulder_R.rotateZ -0;setAttr FKExtraShoulder_R.scaleX 1;setAttr FKExtraShoulder_R.scaleY 1;setAttr FKExtraShoulder_R.scaleZ 1;setAttr FKShoulder_R.translateX 8.881784197e-16;setAttr FKShoulder_R.translateY -8.881784197e-16;setAttr FKShoulder_R.translateZ 1.776356839e-15;setAttr FKShoulder_R.rotateX -0;setAttr FKShoulder_R.rotateY 0;setAttr FKShoulder_R.rotateZ -0;setAttr FKShoulder_R.scaleX 1;setAttr FKShoulder_R.scaleY 1;setAttr FKShoulder_R.scaleZ 1;setAttr FKShoulder_R.Global 10;setAttr FKExtraMiddleFinger3_L.translateX 0;setAttr FKExtraMiddleFinger3_L.translateY 0;setAttr FKExtraMiddleFinger3_L.translateZ 0;setAttr FKExtraMiddleFinger3_L.rotateX -0;setAttr FKExtraMiddleFinger3_L.rotateY 0;setAttr FKExtraMiddleFinger3_L.rotateZ -0;setAttr FKExtraMiddleFinger3_L.scaleX 1;setAttr FKExtraMiddleFinger3_L.scaleY 1;setAttr FKExtraMiddleFinger3_L.scaleZ 1;setAttr FKMiddleFinger3_L.translateX 4.440892099e-16;setAttr FKMiddleFinger3_L.translateY -8.881784197e-16;setAttr FKMiddleFinger3_L.translateZ 8.881784197e-16;setAttr FKMiddleFinger3_L.rotateX -0;setAttr FKMiddleFinger3_L.rotateY 0;setAttr FKMiddleFinger3_L.rotateZ -0;setAttr FKMiddleFinger3_L.scaleX 1;setAttr FKMiddleFinger3_L.scaleY 1;setAttr FKMiddleFinger3_L.scaleZ 1;setAttr FKExtraMiddleFinger2_L.translateX 0;setAttr FKExtraMiddleFinger2_L.translateY -4.440892099e-16;setAttr FKExtraMiddleFinger2_L.translateZ -8.881784197e-16;setAttr FKExtraMiddleFinger2_L.rotateX -0;setAttr FKExtraMiddleFinger2_L.rotateY 0;setAttr FKExtraMiddleFinger2_L.rotateZ -0;setAttr FKExtraMiddleFinger2_L.scaleX 1;setAttr FKExtraMiddleFinger2_L.scaleY 1;setAttr FKExtraMiddleFinger2_L.scaleZ 1;setAttr FKMiddleFinger2_L.translateX 0;setAttr FKMiddleFinger2_L.translateY 0;setAttr FKMiddleFinger2_L.translateZ -8.881784197e-16;setAttr FKMiddleFinger2_L.rotateX -0;setAttr FKMiddleFinger2_L.rotateY 0;setAttr FKMiddleFinger2_L.rotateZ -0;setAttr FKMiddleFinger2_L.scaleX 1;setAttr FKMiddleFinger2_L.scaleY 1;setAttr FKMiddleFinger2_L.scaleZ 1;setAttr FKExtraMiddleFinger1_L.translateX 0;setAttr FKExtraMiddleFinger1_L.translateY 1.110223025e-16;setAttr FKExtraMiddleFinger1_L.translateZ -8.881784197e-16;setAttr FKExtraMiddleFinger1_L.rotateX -0;setAttr FKExtraMiddleFinger1_L.rotateY 0;setAttr FKExtraMiddleFinger1_L.rotateZ -0;setAttr FKExtraMiddleFinger1_L.scaleX 1;setAttr FKExtraMiddleFinger1_L.scaleY 1;setAttr FKExtraMiddleFinger1_L.scaleZ 1;setAttr FKMiddleFinger1_L.translateX -4.440892099e-16;setAttr FKMiddleFinger1_L.translateY 2.220446049e-16;setAttr FKMiddleFinger1_L.translateZ -8.881784197e-16;setAttr FKMiddleFinger1_L.rotateX -0;setAttr FKMiddleFinger1_L.rotateY 0;setAttr FKMiddleFinger1_L.rotateZ -0;setAttr FKMiddleFinger1_L.scaleX 1;setAttr FKMiddleFinger1_L.scaleY 1;setAttr FKMiddleFinger1_L.scaleZ 1;setAttr FKExtraIndexFinger3_L.translateX 0;setAttr FKExtraIndexFinger3_L.translateY 8.881784197e-16;setAttr FKExtraIndexFinger3_L.translateZ -8.881784197e-16;setAttr FKExtraIndexFinger3_L.rotateX -0;setAttr FKExtraIndexFinger3_L.rotateY 0;setAttr FKExtraIndexFinger3_L.rotateZ -0;setAttr FKExtraIndexFinger3_L.scaleX 1;setAttr FKExtraIndexFinger3_L.scaleY 1;setAttr FKExtraIndexFinger3_L.scaleZ 1;setAttr FKIndexFinger3_L.translateX -4.440892099e-16;setAttr FKIndexFinger3_L.translateY 0;setAttr FKIndexFinger3_L.translateZ -8.881784197e-16;setAttr FKIndexFinger3_L.rotateX -0;setAttr FKIndexFinger3_L.rotateY 0;setAttr FKIndexFinger3_L.rotateZ -0;setAttr FKIndexFinger3_L.scaleX 1;setAttr FKIndexFinger3_L.scaleY 1;setAttr FKIndexFinger3_L.scaleZ 1;setAttr FKExtraIndexFinger2_L.translateX 0;setAttr FKExtraIndexFinger2_L.translateY -4.440892099e-16;setAttr FKExtraIndexFinger2_L.translateZ -8.881784197e-16;setAttr FKExtraIndexFinger2_L.rotateX -0;setAttr FKExtraIndexFinger2_L.rotateY 0;setAttr FKExtraIndexFinger2_L.rotateZ -0;setAttr FKExtraIndexFinger2_L.scaleX 1;setAttr FKExtraIndexFinger2_L.scaleY 1;setAttr FKExtraIndexFinger2_L.scaleZ 1;setAttr FKIndexFinger2_L.translateX 4.440892099e-16;setAttr FKIndexFinger2_L.translateY -4.440892099e-16;setAttr FKIndexFinger2_L.translateZ 0;setAttr FKIndexFinger2_L.rotateX -0;setAttr FKIndexFinger2_L.rotateY 0;setAttr FKIndexFinger2_L.rotateZ -0;setAttr FKIndexFinger2_L.scaleX 1;setAttr FKIndexFinger2_L.scaleY 1;setAttr FKIndexFinger2_L.scaleZ 1;setAttr FKExtraIndexFinger1_L.translateX 0;setAttr FKExtraIndexFinger1_L.translateY -3.747002708e-16;setAttr FKExtraIndexFinger1_L.translateZ 0;setAttr FKExtraIndexFinger1_L.rotateX -0;setAttr FKExtraIndexFinger1_L.rotateY 0;setAttr FKExtraIndexFinger1_L.rotateZ -0;setAttr FKExtraIndexFinger1_L.scaleX 1;setAttr FKExtraIndexFinger1_L.scaleY 1;setAttr FKExtraIndexFinger1_L.scaleZ 1;setAttr FKIndexFinger1_L.translateX 0;setAttr FKIndexFinger1_L.translateY -5.689893001e-16;setAttr FKIndexFinger1_L.translateZ 8.881784197e-16;setAttr FKIndexFinger1_L.rotateX -0;setAttr FKIndexFinger1_L.rotateY 0;setAttr FKIndexFinger1_L.rotateZ -0;setAttr FKIndexFinger1_L.scaleX 1;setAttr FKIndexFinger1_L.scaleY 1;setAttr FKIndexFinger1_L.scaleZ 1;setAttr FKExtraThumbFinger3_L.translateX 4.440892099e-16;setAttr FKExtraThumbFinger3_L.translateY 4.440892099e-16;setAttr FKExtraThumbFinger3_L.translateZ -4.440892099e-16;setAttr FKExtraThumbFinger3_L.rotateX -0;setAttr FKExtraThumbFinger3_L.rotateY 0;setAttr FKExtraThumbFinger3_L.rotateZ -0;setAttr FKExtraThumbFinger3_L.scaleX 1;setAttr FKExtraThumbFinger3_L.scaleY 1;setAttr FKExtraThumbFinger3_L.scaleZ 1;setAttr FKThumbFinger3_L.translateX 0;setAttr FKThumbFinger3_L.translateY 2.220446049e-16;setAttr FKThumbFinger3_L.translateZ 0;setAttr FKThumbFinger3_L.rotateX -0;setAttr FKThumbFinger3_L.rotateY 0;setAttr FKThumbFinger3_L.rotateZ -0;setAttr FKThumbFinger3_L.scaleX 1;setAttr FKThumbFinger3_L.scaleY 1;setAttr FKThumbFinger3_L.scaleZ 1;setAttr FKExtraThumbFinger2_L.translateX -4.440892099e-16;setAttr FKExtraThumbFinger2_L.translateY 0;setAttr FKExtraThumbFinger2_L.translateZ 0;setAttr FKExtraThumbFinger2_L.rotateX -0;setAttr FKExtraThumbFinger2_L.rotateY 0;setAttr FKExtraThumbFinger2_L.rotateZ -0;setAttr FKExtraThumbFinger2_L.scaleX 1;setAttr FKExtraThumbFinger2_L.scaleY 1;setAttr FKExtraThumbFinger2_L.scaleZ 1;setAttr FKPinkyFinger1_L.translateX 0;setAttr FKPinkyFinger1_L.translateY 0;setAttr FKPinkyFinger1_L.translateZ 0;setAttr FKPinkyFinger1_L.rotateX -0;setAttr FKPinkyFinger1_L.rotateY 0;setAttr FKPinkyFinger1_L.rotateZ -0;setAttr FKPinkyFinger1_L.scaleX 1;setAttr FKPinkyFinger1_L.scaleY 1;setAttr FKPinkyFinger1_L.scaleZ 1;setAttr FKThumbFinger2_L.translateX 0;setAttr FKThumbFinger2_L.translateY 8.881784197e-16;setAttr FKThumbFinger2_L.translateZ 0;setAttr FKThumbFinger2_L.rotateX -0;setAttr FKThumbFinger2_L.rotateY 0;setAttr FKThumbFinger2_L.rotateZ -0;setAttr FKThumbFinger2_L.scaleX 1;setAttr FKThumbFinger2_L.scaleY 1;setAttr FKThumbFinger2_L.scaleZ 1;setAttr FKExtraThumbFinger1_L.translateX -4.440892099e-16;setAttr FKExtraThumbFinger1_L.translateY -8.881784197e-16;setAttr FKExtraThumbFinger1_L.translateZ 1.110223025e-16;setAttr FKExtraThumbFinger1_L.rotateX -0;setAttr FKExtraThumbFinger1_L.rotateY 0;setAttr FKExtraThumbFinger1_L.rotateZ -0;setAttr FKExtraThumbFinger1_L.scaleX 1;setAttr FKExtraThumbFinger1_L.scaleY 1;setAttr FKExtraThumbFinger1_L.scaleZ 1;setAttr FKThumbFinger1_L.translateX 4.440892099e-16;setAttr FKThumbFinger1_L.translateY -8.881784197e-16;setAttr FKThumbFinger1_L.translateZ 2.220446049e-16;setAttr FKThumbFinger1_L.rotateX -0;setAttr FKThumbFinger1_L.rotateY 0;setAttr FKThumbFinger1_L.rotateZ -0;setAttr FKThumbFinger1_L.scaleX 1;setAttr FKThumbFinger1_L.scaleY 1;setAttr FKThumbFinger1_L.scaleZ 1;setAttr FKExtraPinkyFinger3_L.translateX 0;setAttr FKExtraPinkyFinger3_L.translateY -4.440892099e-16;setAttr FKExtraPinkyFinger3_L.translateZ -6.661338148e-16;setAttr FKExtraPinkyFinger3_L.rotateX -0;setAttr FKExtraPinkyFinger3_L.rotateY 0;setAttr FKExtraPinkyFinger3_L.rotateZ -0;setAttr FKExtraPinkyFinger3_L.scaleX 1;setAttr FKExtraPinkyFinger3_L.scaleY 1;setAttr FKExtraPinkyFinger3_L.scaleZ 1;setAttr FKPinkyFinger3_L.translateX 0;setAttr FKPinkyFinger3_L.translateY 0;setAttr FKPinkyFinger3_L.translateZ -2.220446049e-16;setAttr FKPinkyFinger3_L.rotateX -0;setAttr FKPinkyFinger3_L.rotateY 0;setAttr FKPinkyFinger3_L.rotateZ -0;setAttr FKPinkyFinger3_L.scaleX 1;setAttr FKPinkyFinger3_L.scaleY 1;setAttr FKPinkyFinger3_L.scaleZ 1;setAttr FKExtraPinkyFinger2_L.translateX 0;setAttr FKExtraPinkyFinger2_L.translateY -8.881784197e-16;setAttr FKExtraPinkyFinger2_L.translateZ 8.881784197e-16;setAttr FKExtraPinkyFinger2_L.rotateX -0;setAttr FKExtraPinkyFinger2_L.rotateY 0;setAttr FKExtraPinkyFinger2_L.rotateZ -0;setAttr FKExtraPinkyFinger2_L.scaleX 1;setAttr FKExtraPinkyFinger2_L.scaleY 1;setAttr FKExtraPinkyFinger2_L.scaleZ 1;setAttr FKPinkyFinger2_L.translateX -8.881784197e-16;setAttr FKPinkyFinger2_L.translateY 0;setAttr FKPinkyFinger2_L.translateZ 4.440892099e-16;setAttr FKPinkyFinger2_L.rotateX -0;setAttr FKPinkyFinger2_L.rotateY 0;setAttr FKPinkyFinger2_L.rotateZ -0;setAttr FKPinkyFinger2_L.scaleX 1;setAttr FKPinkyFinger2_L.scaleY 1;setAttr FKPinkyFinger2_L.scaleZ 1;setAttr FKExtraPinkyFinger1_L.translateX 8.881784197e-16;setAttr FKExtraPinkyFinger1_L.translateY 4.440892099e-16;setAttr FKExtraPinkyFinger1_L.translateZ 0;setAttr FKExtraPinkyFinger1_L.rotateX -0;setAttr FKExtraPinkyFinger1_L.rotateY 0;setAttr FKExtraPinkyFinger1_L.rotateZ -0;setAttr FKExtraPinkyFinger1_L.scaleX 1;setAttr FKExtraPinkyFinger1_L.scaleY 1;setAttr FKExtraPinkyFinger1_L.scaleZ 1;setAttr FKHose2_L.translateX -8.881784197e-16;setAttr FKHose2_L.translateY 0;setAttr FKHose2_L.translateZ -4.440892099e-16;setAttr FKHose2_L.rotateX -0;setAttr FKHose2_L.rotateY 0;setAttr FKHose2_L.rotateZ -0;setAttr FKHose2_L.scaleX 1;setAttr FKHose2_L.scaleY 1;setAttr FKHose2_L.scaleZ 1;setAttr FKExtraHose2_L.translateX 0;setAttr FKExtraHose2_L.translateY 0;setAttr FKExtraHose2_L.translateZ 0;setAttr FKExtraHose2_L.rotateX -0;setAttr FKExtraHose2_L.rotateY 0;setAttr FKExtraHose2_L.rotateZ -0;setAttr FKExtraHose2_L.scaleX 1;setAttr FKExtraHose2_L.scaleY 1;setAttr FKExtraHose2_L.scaleZ 1;setAttr FKHose1_L.translateX 0;setAttr FKHose1_L.translateY -8.881784197e-16;setAttr FKHose1_L.translateZ 0;setAttr FKHose1_L.rotateX -0;setAttr FKHose1_L.rotateY 0;setAttr FKHose1_L.rotateZ -0;setAttr FKHose1_L.scaleX 1;setAttr FKHose1_L.scaleY 1;setAttr FKHose1_L.scaleZ 1;setAttr FKExtraHose1_L.translateX 0;setAttr FKExtraHose1_L.translateY 0;setAttr FKExtraHose1_L.translateZ 0;setAttr FKExtraHose1_L.rotateX -0;setAttr FKExtraHose1_L.rotateY 0;setAttr FKExtraHose1_L.rotateZ -0;setAttr FKExtraHose1_L.scaleX 1;setAttr FKExtraHose1_L.scaleY 1;setAttr FKExtraHose1_L.scaleZ 1;setAttr FKExtraWrist_L.translateX -3.330669074e-16;setAttr FKExtraWrist_L.translateY -4.440892099e-16;setAttr FKExtraWrist_L.translateZ 0;setAttr FKExtraWrist_L.rotateX -0;setAttr FKExtraWrist_L.rotateY 0;setAttr FKExtraWrist_L.rotateZ -0;setAttr FKExtraWrist_L.scaleX 1;setAttr FKExtraWrist_L.scaleY 1;setAttr FKExtraWrist_L.scaleZ 1;setAttr FKWrist_L.translateX -4.996003611e-16;setAttr FKWrist_L.translateY 4.440892099e-16;setAttr FKWrist_L.translateZ 0;setAttr FKWrist_L.rotateX -0;setAttr FKWrist_L.rotateY 0;setAttr FKWrist_L.rotateZ -0;setAttr FKWrist_L.scaleX 1;setAttr FKWrist_L.scaleY 1;setAttr FKWrist_L.scaleZ 1;setAttr FKExtraElbow_L.translateX -1.1379786e-15;setAttr FKExtraElbow_L.translateY 0;setAttr FKExtraElbow_L.translateZ 0;setAttr FKExtraElbow_L.rotateX -0;setAttr FKExtraElbow_L.rotateY 0;setAttr FKExtraElbow_L.rotateZ -0;setAttr FKExtraElbow_L.scaleX 1;setAttr FKExtraElbow_L.scaleY 1;setAttr FKExtraElbow_L.scaleZ 1;setAttr FKElbow_L.translateX 0;setAttr FKElbow_L.translateY 0;setAttr FKElbow_L.translateZ 0;setAttr FKElbow_L.rotateX -0;setAttr FKElbow_L.rotateY 0;setAttr FKElbow_L.rotateZ -0;setAttr FKElbow_L.scaleX 1;setAttr FKElbow_L.scaleY 1;setAttr FKElbow_L.scaleZ 1;setAttr FKExtraShoulder1_L.translateX 0;setAttr FKExtraShoulder1_L.translateY 0;setAttr FKExtraShoulder1_L.translateZ 0;setAttr FKExtraShoulder1_L.rotateX -0;setAttr FKExtraShoulder1_L.rotateY 0;setAttr FKExtraShoulder1_L.rotateZ -0;setAttr FKExtraShoulder1_L.scaleX 1;setAttr FKExtraShoulder1_L.scaleY 1;setAttr FKExtraShoulder1_L.scaleZ 1;setAttr FKShoulder1_L.translateX 0;setAttr FKShoulder1_L.translateY 0;setAttr FKShoulder1_L.translateZ 8.881784197e-16;setAttr FKShoulder1_L.rotateX -0;setAttr FKShoulder1_L.rotateY 0;setAttr FKShoulder1_L.rotateZ -0;setAttr FKShoulder1_L.scaleX 1;setAttr FKShoulder1_L.scaleY 1;setAttr FKShoulder1_L.scaleZ 1;setAttr FKShoulder1_L.Global 10;setAttr FKExtraHose1_R.translateX 0;setAttr FKExtraHose1_R.translateY 8.881784197e-16;setAttr FKExtraHose1_R.translateZ 0;setAttr FKExtraHose1_R.rotateX -0;setAttr FKExtraHose1_R.rotateY 0;setAttr FKExtraHose1_R.rotateZ -0;setAttr FKExtraHose1_R.scaleX 1;setAttr FKExtraHose1_R.scaleY 1;setAttr FKExtraHose1_R.scaleZ 1;setAttr FKHose1_R.translateX 0;setAttr FKHose1_R.translateY 8.881784197e-16;setAttr FKHose1_R.translateZ 0;setAttr FKHose1_R.rotateX -0;setAttr FKHose1_R.rotateY 0;setAttr FKHose1_R.rotateZ -0;setAttr FKHose1_R.scaleX 1;setAttr FKHose1_R.scaleY 1;setAttr FKHose1_R.scaleZ 1;setAttr FKExtraHose2_R.translateX 8.881784197e-16;setAttr FKExtraHose2_R.translateY 0;setAttr FKExtraHose2_R.translateZ 4.440892099e-16;setAttr FKExtraHose2_R.rotateX -0;setAttr FKExtraHose2_R.rotateY 0;setAttr FKExtraHose2_R.rotateZ -0;setAttr FKExtraHose2_R.scaleX 1;setAttr FKExtraHose2_R.scaleY 1;setAttr FKExtraHose2_R.scaleZ 1;setAttr FKHose2_R.translateX 8.881784197e-16;setAttr FKHose2_R.translateY 0;setAttr FKHose2_R.translateZ 4.440892099e-16;setAttr FKHose2_R.rotateX -0;setAttr FKHose2_R.rotateY 0;setAttr FKHose2_R.rotateZ -0;setAttr FKHose2_R.scaleX 1;setAttr FKHose2_R.scaleY 1;setAttr FKHose2_R.scaleZ 1;setAttr FKExtraBody_M.translateX 0;setAttr FKExtraBody_M.translateY 0;setAttr FKExtraBody_M.translateZ 0;setAttr FKExtraBody_M.rotateX 0;setAttr FKExtraBody_M.rotateY -0;setAttr FKExtraBody_M.rotateZ 0;setAttr FKExtraBody_M.scaleX 1;setAttr FKExtraBody_M.scaleY 1;setAttr FKExtraBody_M.scaleZ 1;setAttr FKBody_M.translateX 0;setAttr FKBody_M.translateY 0;setAttr FKBody_M.translateZ 0;setAttr FKBody_M.rotateX 0;setAttr FKBody_M.rotateY -0;setAttr FKBody_M.rotateZ 0;setAttr FKBody_M.scaleX 1;setAttr FKBody_M.scaleY 1;setAttr FKBody_M.scaleZ 1;setAttr FKExtraWheel1_R.translateX 2.220446049e-16;setAttr FKExtraWheel1_R.translateY -2.220446049e-16;setAttr FKExtraWheel1_R.translateZ -8.881784197e-16;setAttr FKExtraWheel1_R.rotateX -0;setAttr FKExtraWheel1_R.rotateY 0;setAttr FKExtraWheel1_R.rotateZ 0;setAttr FKExtraWheel1_R.scaleX 1;setAttr FKExtraWheel1_R.scaleY 1;setAttr FKExtraWheel1_R.scaleZ 1;setAttr FKWheel1_R.translateX 2.220446049e-16;setAttr FKWheel1_R.translateY -1.110223025e-16;setAttr FKWheel1_R.translateZ 0;setAttr FKWheel1_R.rotateX -0;setAttr FKWheel1_R.rotateY 0;setAttr FKWheel1_R.rotateZ 0;setAttr FKWheel1_R.scaleX 1;setAttr FKWheel1_R.scaleY 1;setAttr FKWheel1_R.scaleZ 1;setAttr FKExtraWheel2_R.translateX 2.220446049e-16;setAttr FKExtraWheel2_R.translateY 0;setAttr FKExtraWheel2_R.translateZ -2.220446049e-16;setAttr FKExtraWheel2_R.rotateX -0;setAttr FKExtraWheel2_R.rotateY 0;setAttr FKExtraWheel2_R.rotateZ 0;setAttr FKExtraWheel2_R.scaleX 1;setAttr FKExtraWheel2_R.scaleY 1;setAttr FKExtraWheel2_R.scaleZ 1;setAttr FKWheel2_R.translateX 0;setAttr FKWheel2_R.translateY 0;setAttr FKWheel2_R.translateZ 0;setAttr FKWheel2_R.rotateX -0;setAttr FKWheel2_R.rotateY 0;setAttr FKWheel2_R.rotateZ 0;setAttr FKWheel2_R.scaleX 1;setAttr FKWheel2_R.scaleY 1;setAttr FKWheel2_R.scaleZ 1;setAttr FKExtraWheel3_R.translateX 4.440892099e-16;setAttr FKExtraWheel3_R.translateY -1.110223025e-16;setAttr FKExtraWheel3_R.translateZ 0;setAttr FKExtraWheel3_R.rotateX -0;setAttr FKExtraWheel3_R.rotateY 0;setAttr FKExtraWheel3_R.rotateZ 0;setAttr FKExtraWheel3_R.scaleX 1;setAttr FKExtraWheel3_R.scaleY 1;setAttr FKExtraWheel3_R.scaleZ 1;setAttr FKWheel3_R.translateX 0;setAttr FKWheel3_R.translateY 0;setAttr FKWheel3_R.translateZ 0;setAttr FKWheel3_R.rotateX -0;setAttr FKWheel3_R.rotateY 0;setAttr FKWheel3_R.rotateZ 0;setAttr FKWheel3_R.scaleX 1;setAttr FKWheel3_R.scaleY 1;setAttr FKWheel3_R.scaleZ 1;setAttr FKExtraWheel4_R.translateX 4.440892099e-16;setAttr FKExtraWheel4_R.translateY 0;setAttr FKExtraWheel4_R.translateZ 8.881784197e-16;setAttr FKExtraWheel4_R.rotateX -0;setAttr FKExtraWheel4_R.rotateY 0;setAttr FKExtraWheel4_R.rotateZ 0;setAttr FKExtraWheel4_R.scaleX 1;setAttr FKExtraWheel4_R.scaleY 1;setAttr FKExtraWheel4_R.scaleZ 1;setAttr FKWheel4_R.translateX 0;setAttr FKWheel4_R.translateY 0;setAttr FKWheel4_R.translateZ 0;setAttr FKWheel4_R.rotateX -0;setAttr FKWheel4_R.rotateY 0;setAttr FKWheel4_R.rotateZ 0;setAttr FKWheel4_R.scaleX 1;setAttr FKWheel4_R.scaleY 1;setAttr FKWheel4_R.scaleZ 1;setAttr FKExtraTrackBottom_R.translateX 0;setAttr FKExtraTrackBottom_R.translateY 2.220446049e-16;setAttr FKExtraTrackBottom_R.translateZ 0;setAttr FKExtraTrackBottom_R.rotateX -0;setAttr FKExtraTrackBottom_R.rotateY -0;setAttr FKExtraTrackBottom_R.rotateZ 0;setAttr FKExtraTrackBottom_R.scaleX 1;setAttr FKExtraTrackBottom_R.scaleY 1;setAttr FKExtraTrackBottom_R.scaleZ 1;setAttr FKTrackBottom_R.translateX 0;setAttr FKTrackBottom_R.translateY 2.220446049e-16;setAttr FKTrackBottom_R.translateZ 0;setAttr FKTrackBottom_R.rotateX -0;setAttr FKTrackBottom_R.rotateY -0;setAttr FKTrackBottom_R.rotateZ 0;setAttr FKTrackBottom_R.scaleX 1;setAttr FKTrackBottom_R.scaleY 1;setAttr FKTrackBottom_R.scaleZ 1;setAttr FKExtraTrackBend_R.translateX -1.110223025e-16;setAttr FKExtraTrackBend_R.translateY 0;setAttr FKExtraTrackBend_R.translateZ -2.220446049e-16;setAttr FKExtraTrackBend_R.rotateX -0;setAttr FKExtraTrackBend_R.rotateY -0;setAttr FKExtraTrackBend_R.rotateZ 0;setAttr FKExtraTrackBend_R.scaleX 1;setAttr FKExtraTrackBend_R.scaleY 1;setAttr FKExtraTrackBend_R.scaleZ 1;setAttr FKTrackBend_R.translateX -1.110223025e-16;setAttr FKTrackBend_R.translateY 0;setAttr FKTrackBend_R.translateZ 0;setAttr FKTrackBend_R.rotateX -0;setAttr FKTrackBend_R.rotateY -0;setAttr FKTrackBend_R.rotateZ 0;setAttr FKTrackBend_R.scaleX 1;setAttr FKTrackBend_R.scaleY 1;setAttr FKTrackBend_R.scaleZ 1;setAttr FKExtraTrack_R.translateX 0;setAttr FKExtraTrack_R.translateY 4.440892099e-16;setAttr FKExtraTrack_R.translateZ 2.220446049e-16;setAttr FKExtraTrack_R.rotateX -0;setAttr FKExtraTrack_R.rotateY -0;setAttr FKExtraTrack_R.rotateZ 0;setAttr FKExtraTrack_R.scaleX 1;setAttr FKExtraTrack_R.scaleY 1;setAttr FKExtraTrack_R.scaleZ 1;setAttr FKTrack_R.translateX -1.110223025e-16;setAttr FKTrack_R.translateY 4.440892099e-16;setAttr FKTrack_R.translateZ 2.220446049e-16;setAttr FKTrack_R.rotateX -0;setAttr FKTrack_R.rotateY -0;setAttr FKTrack_R.rotateZ 0;setAttr FKTrack_R.scaleX 1;setAttr FKTrack_R.scaleY 1;setAttr FKTrack_R.scaleZ 1;setAttr FKExtraWheel1_L.translateX -2.220446049e-16;setAttr FKExtraWheel1_L.translateY 4.440892099e-16;setAttr FKExtraWheel1_L.translateZ 8.881784197e-16;setAttr FKExtraWheel1_L.rotateX 7.016516996e-15;setAttr FKExtraWheel1_L.rotateY -2.426570649e-20;setAttr FKExtraWheel1_L.rotateZ 1.908332809e-14;setAttr FKExtraWheel1_L.scaleX 1;setAttr FKExtraWheel1_L.scaleY 1;setAttr FKExtraWheel1_L.scaleZ 1;setAttr FKWheel1_L.translateX 0;setAttr FKWheel1_L.translateY 1.110223025e-16;setAttr FKWheel1_L.translateZ 0;setAttr FKWheel1_L.rotateX -0;setAttr FKWheel1_L.rotateY 0;setAttr FKWheel1_L.rotateZ 0;setAttr FKWheel1_L.scaleX 1;setAttr FKWheel1_L.scaleY 1;setAttr FKWheel1_L.scaleZ 1;setAttr FKExtraWheel2_L.translateX -4.440892099e-16;setAttr FKExtraWheel2_L.translateY -1.110223025e-16;setAttr FKExtraWheel2_L.translateZ 0;setAttr FKExtraWheel2_L.rotateX 7.016647424e-15;setAttr FKExtraWheel2_L.rotateY -3.336534643e-20;setAttr FKExtraWheel2_L.rotateZ -2.043020219e-36;setAttr FKExtraWheel2_L.scaleX 1;setAttr FKExtraWheel2_L.scaleY 1;setAttr FKExtraWheel2_L.scaleZ 1;setAttr FKWheel2_L.translateX 0;setAttr FKWheel2_L.translateY -1.110223025e-16;setAttr FKWheel2_L.translateZ -1.110223025e-16;setAttr FKWheel2_L.rotateX -0;setAttr FKWheel2_L.rotateY 0;setAttr FKWheel2_L.rotateZ 0;setAttr FKWheel2_L.scaleX 1;setAttr FKWheel2_L.scaleY 1;setAttr FKWheel2_L.scaleZ 1;setAttr FKExtraWheel3_L.translateX -2.220446049e-16;setAttr FKExtraWheel3_L.translateY 2.220446049e-16;setAttr FKExtraWheel3_L.translateZ 2.220446049e-16;setAttr FKExtraWheel3_L.rotateX 7.016683822e-15;setAttr FKExtraWheel3_L.rotateY -4.246498636e-20;setAttr FKExtraWheel3_L.rotateZ -2.60022104e-36;setAttr FKExtraWheel3_L.scaleX 1;setAttr FKExtraWheel3_L.scaleY 1;setAttr FKExtraWheel3_L.scaleZ 1;setAttr FKWheel3_L.translateX 0;setAttr FKWheel3_L.translateY -1.110223025e-16;setAttr FKWheel3_L.translateZ 0;setAttr FKWheel3_L.rotateX -0;setAttr FKWheel3_L.rotateY 0;setAttr FKWheel3_L.rotateZ 0;setAttr FKWheel3_L.scaleX 1;setAttr FKWheel3_L.scaleY 1;setAttr FKWheel3_L.scaleZ 1;setAttr FKExtraWheel4_L.translateX -4.440892099e-16;setAttr FKExtraWheel4_L.translateY 2.220446049e-16;setAttr FKExtraWheel4_L.translateZ -8.881784197e-16;setAttr FKExtraWheel4_L.rotateX 7.016647424e-15;setAttr FKExtraWheel4_L.rotateY -6.066426623e-20;setAttr FKExtraWheel4_L.rotateZ -3.714582217e-36;setAttr FKExtraWheel4_L.scaleX 1;setAttr FKExtraWheel4_L.scaleY 1;setAttr FKExtraWheel4_L.scaleZ 1;setAttr FKWheel4_L.translateX 0;setAttr FKWheel4_L.translateY 0;setAttr FKWheel4_L.translateZ 0;setAttr FKWheel4_L.rotateX -0;setAttr FKWheel4_L.rotateY 0;setAttr FKWheel4_L.rotateZ 0;setAttr FKWheel4_L.scaleX 1;setAttr FKWheel4_L.scaleY 1;setAttr FKWheel4_L.scaleZ 1;setAttr FKExtraTrackBottom_L.translateX -1.110223025e-16;setAttr FKExtraTrackBottom_L.translateY 0;setAttr FKExtraTrackBottom_L.translateZ 5.551115123e-17;setAttr FKExtraTrackBottom_L.rotateX 1.40334186e-14;setAttr FKExtraTrackBottom_L.rotateY 2.544443745e-14;setAttr FKExtraTrackBottom_L.rotateZ -8.094781647e-37;setAttr FKExtraTrackBottom_L.scaleX 1;setAttr FKExtraTrackBottom_L.scaleY 1;setAttr FKExtraTrackBottom_L.scaleZ 1;setAttr FKTrackBottom_L.translateX 0;setAttr FKTrackBottom_L.translateY 0;setAttr FKTrackBottom_L.translateZ 5.551115123e-17;setAttr FKTrackBottom_L.rotateX -0;setAttr FKTrackBottom_L.rotateY -0;setAttr FKTrackBottom_L.rotateZ 0;setAttr FKTrackBottom_L.scaleX 1;setAttr FKTrackBottom_L.scaleY 1;setAttr FKTrackBottom_L.scaleZ 1;setAttr FKExtraTrackBend_L.translateX 1.110223025e-16;setAttr FKExtraTrackBend_L.translateY 0;setAttr FKExtraTrackBend_L.translateZ 0;setAttr FKExtraTrackBend_L.rotateX 1.908332809e-14;setAttr FKExtraTrackBend_L.rotateY -9.578742044e-15;setAttr FKExtraTrackBend_L.rotateZ -6.201926328e-15;setAttr FKExtraTrackBend_L.scaleX 1;setAttr FKExtraTrackBend_L.scaleY 1;setAttr FKExtraTrackBend_L.scaleZ 1;setAttr FKTrackBend_L.translateX 1.110223025e-16;setAttr FKTrackBend_L.translateY -4.440892099e-16;setAttr FKTrackBend_L.translateZ -2.220446049e-16;setAttr FKTrackBend_L.rotateX -0;setAttr FKTrackBend_L.rotateY -0;setAttr FKTrackBend_L.rotateZ 0;setAttr FKTrackBend_L.scaleX 1;setAttr FKTrackBend_L.scaleY 1;setAttr FKTrackBend_L.scaleZ 1;setAttr FKExtraTrack_L.translateX 2.220446049e-16;setAttr FKExtraTrack_L.translateY 0;setAttr FKExtraTrack_L.translateZ -4.440892099e-16;setAttr FKExtraTrack_L.rotateX -2.548904968e-31;setAttr FKExtraTrack_L.rotateY -2.376534763e-15;setAttr FKExtraTrack_L.rotateZ -1.229028915e-14;setAttr FKExtraTrack_L.scaleX 1;setAttr FKExtraTrack_L.scaleY 1;setAttr FKExtraTrack_L.scaleZ 1;setAttr FKTrack_L.translateX 1.110223025e-16;setAttr FKTrack_L.translateY 0;setAttr FKTrack_L.translateZ -4.440892099e-16;setAttr FKTrack_L.rotateX -0;setAttr FKTrack_L.rotateY -0;setAttr FKTrack_L.rotateZ 0;setAttr FKTrack_L.scaleX 1;setAttr FKTrack_L.scaleY 1;setAttr FKTrack_L.scaleZ 1;setAttr IKExtraLeg_R.translateX -1.110223025e-16;setAttr IKExtraLeg_R.translateY 0;setAttr IKExtraLeg_R.translateZ 0;setAttr IKExtraLeg_R.rotateX -0;setAttr IKExtraLeg_R.rotateY -0;setAttr IKExtraLeg_R.rotateZ 0;setAttr IKLeg_R.translateX 0;setAttr IKLeg_R.translateY 0;setAttr IKLeg_R.translateZ 0;setAttr IKLeg_R.rotateX -0;setAttr IKLeg_R.rotateY -0;setAttr IKLeg_R.rotateZ 0;setAttr IKLeg_R.swivel 0;setAttr IKLeg_R.roll 0;setAttr IKLeg_R.rollAngle 25;setAttr PoleExtraLeg_R.translateX 0;setAttr PoleExtraLeg_R.translateY 0;setAttr PoleExtraLeg_R.translateZ 0;setAttr PoleLeg_R.translateX 0;setAttr PoleLeg_R.translateY 0;setAttr PoleLeg_R.translateZ -2.220446049e-16;setAttr PoleLeg_R.follow 10;setAttr FKIKLeg_R.FKIKBlend 10;setAttr FKIKLeg_R.FKVis 0;setAttr FKIKLeg_R.IKVis 1;setAttr IKExtraLegHeel_R.rotateX 0;setAttr IKExtraLegHeel_R.rotateY -0;setAttr IKExtraLegHeel_R.rotateZ 0;setAttr IKLegHeel_R.rotateX 0;setAttr IKLegHeel_R.rotateY -0;setAttr IKLegHeel_R.rotateZ 0;setAttr IKExtraLegBall_R.rotateX 0;setAttr IKExtraLegBall_R.rotateY -0;setAttr IKExtraLegBall_R.rotateZ 0;setAttr IKLegBall_R.rotateX 0;");
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -s 2 ".sol";
connectAttr "jointLayer.di" "Pelvis.do";
connectAttr "Pelvis.s" "Body.is";
connectAttr "jointLayer.di" "Body.do";
connectAttr "Body.s" "Head.is";
connectAttr "jointLayer.di" "Head.do";
connectAttr "Head.s" "Head_End.is";
connectAttr "jointLayer.di" "Head_End.do";
connectAttr "Body.s" "Shoulder.is";
connectAttr "jointLayer.di" "Shoulder.do";
connectAttr "Shoulder.global" "Shoulder.globalConnect";
connectAttr "Shoulder.s" "Elbow5.is";
connectAttr "jointLayer.di" "Elbow5.do";
connectAttr "jointLayer.di" "Wrist1.do";
connectAttr "Elbow5.s" "Wrist1.is";
connectAttr "Wrist1.s" "Elbow4.is";
connectAttr "jointLayer.di" "Elbow4.do";
connectAttr "Body.s" "Shoulder1.is";
connectAttr "jointLayer.di" "Shoulder1.do";
connectAttr "Shoulder1.global" "Shoulder1.globalConnect";
connectAttr "Shoulder1.s" "Elbow.is";
connectAttr "jointLayer.di" "Elbow.do";
connectAttr "jointLayer.di" "Wrist.do";
connectAttr "Elbow.s" "Wrist.is";
connectAttr "Wrist.s" "MiddleFinger1.is";
connectAttr "jointLayer.di" "MiddleFinger1.do";
connectAttr "MiddleFinger1.s" "MiddleFinger2.is";
connectAttr "jointLayer.di" "MiddleFinger2.do";
connectAttr "MiddleFinger2.s" "MiddleFinger3.is";
connectAttr "jointLayer.di" "MiddleFinger3.do";
connectAttr "jointLayer.di" "MiddleFinger4_End.do";
connectAttr "MiddleFinger3.s" "MiddleFinger4_End.is";
connectAttr "Wrist.s" "IndexFinger1.is";
connectAttr "jointLayer.di" "IndexFinger1.do";
connectAttr "IndexFinger1.s" "IndexFinger2.is";
connectAttr "jointLayer.di" "IndexFinger2.do";
connectAttr "IndexFinger2.s" "IndexFinger3.is";
connectAttr "jointLayer.di" "IndexFinger3.do";
connectAttr "jointLayer.di" "IndexFinger4_End.do";
connectAttr "IndexFinger3.s" "IndexFinger4_End.is";
connectAttr "Wrist.s" "ThumbFinger1.is";
connectAttr "jointLayer.di" "ThumbFinger1.do";
connectAttr "ThumbFinger1.s" "ThumbFinger2.is";
connectAttr "jointLayer.di" "ThumbFinger2.do";
connectAttr "ThumbFinger2.s" "ThumbFinger3.is";
connectAttr "jointLayer.di" "ThumbFinger3.do";
connectAttr "jointLayer.di" "ThumbFinger4_End.do";
connectAttr "ThumbFinger3.s" "ThumbFinger4_End.is";
connectAttr "Wrist.s" "PinkyFinger1.is";
connectAttr "jointLayer.di" "PinkyFinger1.do";
connectAttr "PinkyFinger1.s" "PinkyFinger2.is";
connectAttr "jointLayer.di" "PinkyFinger2.do";
connectAttr "PinkyFinger2.s" "PinkyFinger3.is";
connectAttr "jointLayer.di" "PinkyFinger3.do";
connectAttr "jointLayer.di" "PinkyFinger4_End.do";
connectAttr "PinkyFinger3.s" "PinkyFinger4_End.is";
connectAttr "Body.s" "Hose1.is";
connectAttr "jointLayer.di" "Hose1.do";
connectAttr "jointLayer.di" "Hose1_End.do";
connectAttr "Hose1.s" "Hose1_End.is";
connectAttr "Body.s" "Hose2.is";
connectAttr "jointLayer.di" "Hose2.do";
connectAttr "jointLayer.di" "Hose2_End.do";
connectAttr "Hose2.s" "Hose2_End.is";
connectAttr "Pelvis.s" "Track.is";
connectAttr "jointLayer.di" "Track.do";
connectAttr "Track.s" "TrackBend.is";
connectAttr "jointLayer.di" "TrackBend.do";
connectAttr "TrackBend.s" "TrackBottom.is";
connectAttr "jointLayer.di" "TrackBottom.do";
connectAttr "jointLayer.di" "Wheel1.do";
connectAttr "TrackBottom.s" "Wheel1.is";
connectAttr "Wheel1.s" "Wheel1_End.is";
connectAttr "jointLayer.di" "Wheel1_End.do";
connectAttr "jointLayer.di" "Wheel2.do";
connectAttr "TrackBottom.s" "Wheel2.is";
connectAttr "Wheel2.s" "Wheel2_End.is";
connectAttr "jointLayer.di" "Wheel2_End.do";
connectAttr "jointLayer.di" "Wheel3.do";
connectAttr "TrackBottom.s" "Wheel3.is";
connectAttr "Wheel3.s" "Wheel3_End.is";
connectAttr "jointLayer.di" "Wheel3_End.do";
connectAttr "jointLayer.di" "Wheel4.do";
connectAttr "TrackBottom.s" "Wheel4.is";
connectAttr "Wheel4.s" "Wheel4_End.is";
connectAttr "jointLayer.di" "Wheel4_End.do";
connectAttr "jointLayer.di" "FrontTrack_End.do";
connectAttr "TrackBottom.s" "FrontTrack_End.is";
connectAttr "jointLayer.di" "BackTrack_End.do";
connectAttr "TrackBottom.s" "BackTrack_End.is";
connectAttr "Main.fkVis" "FKSystem.v";
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.ctx" "FKParentConstraintToTrackBottom_R.tx"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.cty" "FKParentConstraintToTrackBottom_R.ty"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.ctz" "FKParentConstraintToTrackBottom_R.tz"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.crx" "FKParentConstraintToTrackBottom_R.rx"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.cry" "FKParentConstraintToTrackBottom_R.ry"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.crz" "FKParentConstraintToTrackBottom_R.rz"
		;
connectAttr "jointLayer.di" "FKOffsetWheel1_R.do";
connectAttr "jointLayer.di" "FKXWheel1_R.do";
connectAttr "FKWheel1_R.s" "FKXWheel1_End_R.is";
connectAttr "jointLayer.di" "FKXWheel1_End_R.do";
connectAttr "jointLayer.di" "FKOffsetWheel2_R.do";
connectAttr "jointLayer.di" "FKXWheel2_R.do";
connectAttr "FKWheel2_R.s" "FKXWheel2_End_R.is";
connectAttr "jointLayer.di" "FKXWheel2_End_R.do";
connectAttr "jointLayer.di" "FKOffsetWheel3_R.do";
connectAttr "jointLayer.di" "FKXWheel3_R.do";
connectAttr "FKWheel3_R.s" "FKXWheel3_End_R.is";
connectAttr "jointLayer.di" "FKXWheel3_End_R.do";
connectAttr "jointLayer.di" "FKOffsetWheel4_R.do";
connectAttr "jointLayer.di" "FKXWheel4_R.do";
connectAttr "FKWheel4_R.s" "FKXWheel4_End_R.is";
connectAttr "jointLayer.di" "FKXWheel4_End_R.do";
connectAttr "FKParentConstraintToTrackBottom_R.ro" "FKParentConstraintToTrackBottom_R_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToTrackBottom_R.pim" "FKParentConstraintToTrackBottom_R_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToTrackBottom_R.rp" "FKParentConstraintToTrackBottom_R_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToTrackBottom_R.rpt" "FKParentConstraintToTrackBottom_R_parentConstraint1.crt"
		;
connectAttr "TrackBottom_R.t" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tt"
		;
connectAttr "TrackBottom_R.rp" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].trp"
		;
connectAttr "TrackBottom_R.rpt" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].trt"
		;
connectAttr "TrackBottom_R.r" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tr"
		;
connectAttr "TrackBottom_R.ro" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tro"
		;
connectAttr "TrackBottom_R.s" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].ts"
		;
connectAttr "TrackBottom_R.pm" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "TrackBottom_R.jo" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.w0" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.ctx" "FKParentConstraintToTrackBottom_L.tx"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.cty" "FKParentConstraintToTrackBottom_L.ty"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.ctz" "FKParentConstraintToTrackBottom_L.tz"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.crx" "FKParentConstraintToTrackBottom_L.rx"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.cry" "FKParentConstraintToTrackBottom_L.ry"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.crz" "FKParentConstraintToTrackBottom_L.rz"
		;
connectAttr "jointLayer.di" "FKOffsetWheel1_L.do";
connectAttr "jointLayer.di" "FKXWheel1_L.do";
connectAttr "FKWheel1_L.s" "FKXWheel1_End_L.is";
connectAttr "jointLayer.di" "FKXWheel1_End_L.do";
connectAttr "jointLayer.di" "FKOffsetWheel2_L.do";
connectAttr "jointLayer.di" "FKXWheel2_L.do";
connectAttr "FKWheel2_L.s" "FKXWheel2_End_L.is";
connectAttr "jointLayer.di" "FKXWheel2_End_L.do";
connectAttr "jointLayer.di" "FKOffsetWheel3_L.do";
connectAttr "jointLayer.di" "FKXWheel3_L.do";
connectAttr "FKWheel3_L.s" "FKXWheel3_End_L.is";
connectAttr "jointLayer.di" "FKXWheel3_End_L.do";
connectAttr "jointLayer.di" "FKOffsetWheel4_L.do";
connectAttr "jointLayer.di" "FKXWheel4_L.do";
connectAttr "FKWheel4_L.s" "FKXWheel4_End_L.is";
connectAttr "jointLayer.di" "FKXWheel4_End_L.do";
connectAttr "FKParentConstraintToTrackBottom_L.ro" "FKParentConstraintToTrackBottom_L_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToTrackBottom_L.pim" "FKParentConstraintToTrackBottom_L_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToTrackBottom_L.rp" "FKParentConstraintToTrackBottom_L_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToTrackBottom_L.rpt" "FKParentConstraintToTrackBottom_L_parentConstraint1.crt"
		;
connectAttr "TrackBottom_L.t" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tt"
		;
connectAttr "TrackBottom_L.rp" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].trp"
		;
connectAttr "TrackBottom_L.rpt" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].trt"
		;
connectAttr "TrackBottom_L.r" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tr"
		;
connectAttr "TrackBottom_L.ro" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tro"
		;
connectAttr "TrackBottom_L.s" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].ts"
		;
connectAttr "TrackBottom_L.pm" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "TrackBottom_L.jo" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.w0" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctx" "PelvisCenterBtwLegsBlended_M.tx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cty" "PelvisCenterBtwLegsBlended_M.ty"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctz" "PelvisCenterBtwLegsBlended_M.tz"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crx" "PelvisCenterBtwLegsBlended_M.rx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cry" "PelvisCenterBtwLegsBlended_M.ry"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crz" "PelvisCenterBtwLegsBlended_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetPelvis_M.do";
connectAttr "jointLayer.di" "FKXPelvis_M.do";
connectAttr "FKXPelvis_M.s" "FKOffsetBody_M.is";
connectAttr "jointLayer.di" "FKOffsetBody_M.do";
connectAttr "FKPelvis_M.s" "FKXBody_M.is";
connectAttr "jointLayer.di" "FKXBody_M.do";
connectAttr "FKXBody_M.s" "FKOffsetHead_M.is";
connectAttr "jointLayer.di" "FKOffsetHead_M.do";
connectAttr "FKBody_M.s" "FKXHead_M.is";
connectAttr "jointLayer.di" "FKXHead_M.do";
connectAttr "FKHead_M.s" "FKXHead_End_M.is";
connectAttr "jointLayer.di" "FKXHead_End_M.do";
connectAttr "FKXBody_M.s" "FKOffsetShoulder_R.is";
connectAttr "jointLayer.di" "FKOffsetShoulder_R.do";
connectAttr "FKGlobalShoulder_R_orientConstraint1.crx" "FKGlobalShoulder_R.rx";
connectAttr "FKGlobalShoulder_R_orientConstraint1.cry" "FKGlobalShoulder_R.ry";
connectAttr "FKGlobalShoulder_R_orientConstraint1.crz" "FKGlobalShoulder_R.rz";
connectAttr "FKBody_M.s" "FKXShoulder_R.is";
connectAttr "jointLayer.di" "FKXShoulder_R.do";
connectAttr "FKXShoulder_R.s" "FKOffsetElbow5_R.is";
connectAttr "jointLayer.di" "FKOffsetElbow5_R.do";
connectAttr "FKShoulder_R.s" "FKXElbow5_R.is";
connectAttr "jointLayer.di" "FKXElbow5_R.do";
connectAttr "FKXElbow5_R.s" "FKOffsetWrist1_R.is";
connectAttr "jointLayer.di" "FKOffsetWrist1_R.do";
connectAttr "FKElbow5_R.s" "FKXWrist1_R.is";
connectAttr "jointLayer.di" "FKXWrist1_R.do";
connectAttr "FKWrist1_R.s" "FKXElbow4_R.is";
connectAttr "jointLayer.di" "FKXElbow4_R.do";
connectAttr "FKGlobalShoulder_R.ro" "FKGlobalShoulder_R_orientConstraint1.cro";
connectAttr "FKGlobalShoulder_R.pim" "FKGlobalShoulder_R_orientConstraint1.cpim"
		;
connectAttr "GlobalShoulder_R.r" "FKGlobalShoulder_R_orientConstraint1.tg[0].tr"
		;
connectAttr "GlobalShoulder_R.ro" "FKGlobalShoulder_R_orientConstraint1.tg[0].tro"
		;
connectAttr "GlobalShoulder_R.pm" "FKGlobalShoulder_R_orientConstraint1.tg[0].tpm"
		;
connectAttr "FKGlobalShoulder_R_orientConstraint1.w0" "FKGlobalShoulder_R_orientConstraint1.tg[0].tw"
		;
connectAttr "FKGlobalStaticShoulder_R.r" "FKGlobalShoulder_R_orientConstraint1.tg[1].tr"
		;
connectAttr "FKGlobalStaticShoulder_R.ro" "FKGlobalShoulder_R_orientConstraint1.tg[1].tro"
		;
connectAttr "FKGlobalStaticShoulder_R.pm" "FKGlobalShoulder_R_orientConstraint1.tg[1].tpm"
		;
connectAttr "FKGlobalShoulder_R_orientConstraint1.w1" "FKGlobalShoulder_R_orientConstraint1.tg[1].tw"
		;
connectAttr "GlobalShoulder_unitConversion_R.o" "FKGlobalShoulder_R_orientConstraint1.w0"
		;
connectAttr "GlobalShoulder_reverse_R.ox" "FKGlobalShoulder_R_orientConstraint1.w1"
		;
connectAttr "FKXBody_M.s" "FKOffsetShoulder1_L.is";
connectAttr "jointLayer.di" "FKOffsetShoulder1_L.do";
connectAttr "FKGlobalShoulder1_L_orientConstraint1.crx" "FKGlobalShoulder1_L.rx"
		;
connectAttr "FKGlobalShoulder1_L_orientConstraint1.cry" "FKGlobalShoulder1_L.ry"
		;
connectAttr "FKGlobalShoulder1_L_orientConstraint1.crz" "FKGlobalShoulder1_L.rz"
		;
connectAttr "FKBody_M.s" "FKXShoulder1_L.is";
connectAttr "jointLayer.di" "FKXShoulder1_L.do";
connectAttr "FKXShoulder1_L.s" "FKOffsetElbow_L.is";
connectAttr "jointLayer.di" "FKOffsetElbow_L.do";
connectAttr "FKShoulder1_L.s" "FKXElbow_L.is";
connectAttr "jointLayer.di" "FKXElbow_L.do";
connectAttr "FKXElbow_L.s" "FKOffsetWrist_L.is";
connectAttr "jointLayer.di" "FKOffsetWrist_L.do";
connectAttr "FKElbow_L.s" "FKXWrist_L.is";
connectAttr "jointLayer.di" "FKXWrist_L.do";
connectAttr "FKXWrist_L.s" "FKOffsetMiddleFinger1_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger1_L.do";
connectAttr "FKWrist_L.s" "FKXMiddleFinger1_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger1_L.do";
connectAttr "FKXMiddleFinger1_L.s" "FKOffsetMiddleFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger2_L.do";
connectAttr "FKMiddleFinger1_L.s" "FKXMiddleFinger2_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger2_L.do";
connectAttr "FKXMiddleFinger2_L.s" "FKOffsetMiddleFinger3_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger3_L.do";
connectAttr "FKMiddleFinger2_L.s" "FKXMiddleFinger3_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger3_L.do";
connectAttr "FKMiddleFinger3_L.s" "FKXMiddleFinger4_End_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger4_End_L.do";
connectAttr "FKXWrist_L.s" "FKOffsetIndexFinger1_L.is";
connectAttr "jointLayer.di" "FKOffsetIndexFinger1_L.do";
connectAttr "FKWrist_L.s" "FKXIndexFinger1_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger1_L.do";
connectAttr "FKXIndexFinger1_L.s" "FKOffsetIndexFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetIndexFinger2_L.do";
connectAttr "FKIndexFinger1_L.s" "FKXIndexFinger2_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger2_L.do";
connectAttr "FKXIndexFinger2_L.s" "FKOffsetIndexFinger3_L.is";
connectAttr "jointLayer.di" "FKOffsetIndexFinger3_L.do";
connectAttr "FKIndexFinger2_L.s" "FKXIndexFinger3_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger3_L.do";
connectAttr "FKIndexFinger3_L.s" "FKXIndexFinger4_End_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger4_End_L.do";
connectAttr "FKXWrist_L.s" "FKOffsetThumbFinger1_L.is";
connectAttr "jointLayer.di" "FKOffsetThumbFinger1_L.do";
connectAttr "FKWrist_L.s" "FKXThumbFinger1_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger1_L.do";
connectAttr "FKXThumbFinger1_L.s" "FKOffsetThumbFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetThumbFinger2_L.do";
connectAttr "FKThumbFinger1_L.s" "FKXThumbFinger2_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger2_L.do";
connectAttr "FKXThumbFinger2_L.s" "FKOffsetThumbFinger3_L.is";
connectAttr "jointLayer.di" "FKOffsetThumbFinger3_L.do";
connectAttr "FKThumbFinger2_L.s" "FKXThumbFinger3_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger3_L.do";
connectAttr "FKThumbFinger3_L.s" "FKXThumbFinger4_End_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger4_End_L.do";
connectAttr "FKXWrist_L.s" "FKOffsetPinkyFinger1_L.is";
connectAttr "jointLayer.di" "FKOffsetPinkyFinger1_L.do";
connectAttr "FKWrist_L.s" "FKXPinkyFinger1_L.is";
connectAttr "jointLayer.di" "FKXPinkyFinger1_L.do";
connectAttr "FKXPinkyFinger1_L.s" "FKOffsetPinkyFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetPinkyFinger2_L.do";
connectAttr "FKPinkyFinger1_L.s" "FKXPinkyFinger2_L.is";
connectAttr "jointLayer.di" "FKXPinkyFinger2_L.do";
connectAttr "FKXPinkyFinger2_L.s" "FKOffsetPinkyFinger3_L.is";
connectAttr "jointLayer.di" "FKOffsetPinkyFinger3_L.do";
connectAttr "FKPinkyFinger2_L.s" "FKXPinkyFinger3_L.is";
connectAttr "jointLayer.di" "FKXPinkyFinger3_L.do";
connectAttr "FKPinkyFinger3_L.s" "FKXPinkyFinger4_End_L.is";
connectAttr "jointLayer.di" "FKXPinkyFinger4_End_L.do";
connectAttr "FKGlobalShoulder1_L.ro" "FKGlobalShoulder1_L_orientConstraint1.cro"
		;
connectAttr "FKGlobalShoulder1_L.pim" "FKGlobalShoulder1_L_orientConstraint1.cpim"
		;
connectAttr "GlobalShoulder1_L.r" "FKGlobalShoulder1_L_orientConstraint1.tg[0].tr"
		;
connectAttr "GlobalShoulder1_L.ro" "FKGlobalShoulder1_L_orientConstraint1.tg[0].tro"
		;
connectAttr "GlobalShoulder1_L.pm" "FKGlobalShoulder1_L_orientConstraint1.tg[0].tpm"
		;
connectAttr "FKGlobalShoulder1_L_orientConstraint1.w0" "FKGlobalShoulder1_L_orientConstraint1.tg[0].tw"
		;
connectAttr "FKGlobalStaticShoulder1_L.r" "FKGlobalShoulder1_L_orientConstraint1.tg[1].tr"
		;
connectAttr "FKGlobalStaticShoulder1_L.ro" "FKGlobalShoulder1_L_orientConstraint1.tg[1].tro"
		;
connectAttr "FKGlobalStaticShoulder1_L.pm" "FKGlobalShoulder1_L_orientConstraint1.tg[1].tpm"
		;
connectAttr "FKGlobalShoulder1_L_orientConstraint1.w1" "FKGlobalShoulder1_L_orientConstraint1.tg[1].tw"
		;
connectAttr "GlobalShoulder1_unitConversion_L.o" "FKGlobalShoulder1_L_orientConstraint1.w0"
		;
connectAttr "GlobalShoulder1_reverse_L.ox" "FKGlobalShoulder1_L_orientConstraint1.w1"
		;
connectAttr "FKXBody_M.s" "FKOffsetHose1_R.is";
connectAttr "jointLayer.di" "FKOffsetHose1_R.do";
connectAttr "FKBody_M.s" "FKXHose1_R.is";
connectAttr "jointLayer.di" "FKXHose1_R.do";
connectAttr "FKHose1_R.s" "FKXHose1_End_R.is";
connectAttr "jointLayer.di" "FKXHose1_End_R.do";
connectAttr "FKXBody_M.s" "FKOffsetHose2_R.is";
connectAttr "jointLayer.di" "FKOffsetHose2_R.do";
connectAttr "FKBody_M.s" "FKXHose2_R.is";
connectAttr "jointLayer.di" "FKXHose2_R.do";
connectAttr "FKHose2_R.s" "FKXHose2_End_R.is";
connectAttr "jointLayer.di" "FKXHose2_End_R.do";
connectAttr "FKXBody_M.s" "FKOffsetHose1_L.is";
connectAttr "jointLayer.di" "FKOffsetHose1_L.do";
connectAttr "FKBody_M.s" "FKXHose1_L.is";
connectAttr "jointLayer.di" "FKXHose1_L.do";
connectAttr "FKHose1_L.s" "FKXHose1_End_L.is";
connectAttr "jointLayer.di" "FKXHose1_End_L.do";
connectAttr "FKXBody_M.s" "FKOffsetHose2_L.is";
connectAttr "jointLayer.di" "FKOffsetHose2_L.do";
connectAttr "FKBody_M.s" "FKXHose2_L.is";
connectAttr "jointLayer.di" "FKXHose2_L.do";
connectAttr "FKHose2_L.s" "FKXHose2_End_L.is";
connectAttr "jointLayer.di" "FKXHose2_End_L.do";
connectAttr "FKXPelvis_M.s" "FKOffsetTrack_R.is";
connectAttr "FKIKBlendLegCondition_R.ocg" "FKOffsetTrack_R.v" -l on;
connectAttr "jointLayer.di" "FKOffsetTrack_R.do";
connectAttr "jointLayer.di" "FKXTrack_R.do";
connectAttr "FKXTrack_R.s" "FKOffsetTrackBend_R.is";
connectAttr "jointLayer.di" "FKOffsetTrackBend_R.do";
connectAttr "FKTrack_R.s" "FKXTrackBend_R.is";
connectAttr "jointLayer.di" "FKXTrackBend_R.do";
connectAttr "FKXTrackBend_R.s" "FKOffsetTrackBottom_R.is";
connectAttr "jointLayer.di" "FKOffsetTrackBottom_R.do";
connectAttr "FKTrackBend_R.s" "FKXTrackBottom_R.is";
connectAttr "jointLayer.di" "FKXTrackBottom_R.do";
connectAttr "FKTrackBottom_R.s" "FKXFrontTrack_End_R.is";
connectAttr "jointLayer.di" "FKXFrontTrack_End_R.do";
connectAttr "FKTrackBottom_R.s" "FKXBackTrack_End_R.is";
connectAttr "jointLayer.di" "FKXBackTrack_End_R.do";
connectAttr "FKXPelvis_M.s" "FKOffsetTrack_L.is";
connectAttr "FKIKBlendLegCondition_L.ocg" "FKOffsetTrack_L.v" -l on;
connectAttr "jointLayer.di" "FKOffsetTrack_L.do";
connectAttr "jointLayer.di" "FKXTrack_L.do";
connectAttr "FKXTrack_L.s" "FKOffsetTrackBend_L.is";
connectAttr "jointLayer.di" "FKOffsetTrackBend_L.do";
connectAttr "FKTrack_L.s" "FKXTrackBend_L.is";
connectAttr "jointLayer.di" "FKXTrackBend_L.do";
connectAttr "FKXTrackBend_L.s" "FKOffsetTrackBottom_L.is";
connectAttr "jointLayer.di" "FKOffsetTrackBottom_L.do";
connectAttr "FKTrackBend_L.s" "FKXTrackBottom_L.is";
connectAttr "jointLayer.di" "FKXTrackBottom_L.do";
connectAttr "FKTrackBottom_L.s" "FKXFrontTrack_End_L.is";
connectAttr "jointLayer.di" "FKXFrontTrack_End_L.do";
connectAttr "FKTrackBottom_L.s" "FKXBackTrack_End_L.is";
connectAttr "jointLayer.di" "FKXBackTrack_End_L.do";
connectAttr "PelvisCenterBtwLegsBlended_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.pim" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crt"
		;
connectAttr "PelvisCenter_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tt"
		;
connectAttr "PelvisCenter_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trp"
		;
connectAttr "PelvisCenter_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trt"
		;
connectAttr "PelvisCenter_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tr"
		;
connectAttr "PelvisCenter_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tro"
		;
connectAttr "PelvisCenter_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].ts"
		;
connectAttr "PelvisCenter_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trp"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tr"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tro"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].ts"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tw"
		;
connectAttr "PelvisCenterBtwFeetReverse_M.ox" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0"
		;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1"
		;
connectAttr "Main.ikVis" "IKSystem.v";
connectAttr "IKParentConstraintTrack_R_parentConstraint1.ctx" "IKParentConstraintTrack_R.tx"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.cty" "IKParentConstraintTrack_R.ty"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.ctz" "IKParentConstraintTrack_R.tz"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.crx" "IKParentConstraintTrack_R.rx"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.cry" "IKParentConstraintTrack_R.ry"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.crz" "IKParentConstraintTrack_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintTrack_R.v";
connectAttr "jointLayer.di" "IKXTrack_R.do";
connectAttr "IKXTrack_R.s" "IKXTrackBend_R.is";
connectAttr "jointLayer.di" "IKXTrackBend_R.do";
connectAttr "IKXTrackBend_R.s" "IKXTrackBottom_R.is";
connectAttr "IKXTrackBottom_R_orientConstraint1.crx" "IKXTrackBottom_R.rx";
connectAttr "IKXTrackBottom_R_orientConstraint1.cry" "IKXTrackBottom_R.ry";
connectAttr "IKXTrackBottom_R_orientConstraint1.crz" "IKXTrackBottom_R.rz";
connectAttr "jointLayer.di" "IKXTrackBottom_R.do";
connectAttr "IKXTrackBottom_R.s" "IKXFrontTrack_End_R.is";
connectAttr "jointLayer.di" "IKXFrontTrack_End_R.do";
connectAttr "IKXTrackBottom_R.ro" "IKXTrackBottom_R_orientConstraint1.cro";
connectAttr "IKXTrackBottom_R.pim" "IKXTrackBottom_R_orientConstraint1.cpim";
connectAttr "IKXTrackBottom_R.jo" "IKXTrackBottom_R_orientConstraint1.cjo";
connectAttr "IKLeg_R.r" "IKXTrackBottom_R_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "IKXTrackBottom_R_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "IKXTrackBottom_R_orientConstraint1.tg[0].tpm";
connectAttr "IKXTrackBottom_R_orientConstraint1.w0" "IKXTrackBottom_R_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXFrontTrack_End_R.tx" "effector2.tx";
connectAttr "IKXFrontTrack_End_R.ty" "effector2.ty";
connectAttr "IKXFrontTrack_End_R.tz" "effector2.tz";
connectAttr "IKXTrackBottom_R.tx" "effector1.tx";
connectAttr "IKXTrackBottom_R.ty" "effector1.ty";
connectAttr "IKXTrackBottom_R.tz" "effector1.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_R.v";
connectAttr "PoleAnnotateTargetLeg_RShape.wm" "PoleAnnotationLeg_RShape.dom" -na
		;
connectAttr "IKParentConstraintTrack_R.ro" "IKParentConstraintTrack_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintTrack_R.pim" "IKParentConstraintTrack_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintTrack_R.rp" "IKParentConstraintTrack_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintTrack_R.rpt" "IKParentConstraintTrack_R_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tt";
connectAttr "Pelvis_M.rp" "IKParentConstraintTrack_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "IKParentConstraintTrack_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tr";
connectAttr "Pelvis_M.ro" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "IKParentConstraintTrack_R_parentConstraint1.tg[0].ts";
connectAttr "Pelvis_M.pm" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.w0" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.ctx" "IKParentConstraintTrack_L.tx"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.cty" "IKParentConstraintTrack_L.ty"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.ctz" "IKParentConstraintTrack_L.tz"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.crx" "IKParentConstraintTrack_L.rx"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.cry" "IKParentConstraintTrack_L.ry"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.crz" "IKParentConstraintTrack_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintTrack_L.v";
connectAttr "jointLayer.di" "IKXTrack_L.do";
connectAttr "IKXTrack_L.s" "IKXTrackBend_L.is";
connectAttr "jointLayer.di" "IKXTrackBend_L.do";
connectAttr "IKXTrackBend_L.s" "IKXTrackBottom_L.is";
connectAttr "IKXTrackBottom_L_orientConstraint1.crx" "IKXTrackBottom_L.rx";
connectAttr "IKXTrackBottom_L_orientConstraint1.cry" "IKXTrackBottom_L.ry";
connectAttr "IKXTrackBottom_L_orientConstraint1.crz" "IKXTrackBottom_L.rz";
connectAttr "jointLayer.di" "IKXTrackBottom_L.do";
connectAttr "IKXTrackBottom_L.s" "IKXFrontTrack_End_L.is";
connectAttr "jointLayer.di" "IKXFrontTrack_End_L.do";
connectAttr "IKXTrackBottom_L.ro" "IKXTrackBottom_L_orientConstraint1.cro";
connectAttr "IKXTrackBottom_L.pim" "IKXTrackBottom_L_orientConstraint1.cpim";
connectAttr "IKXTrackBottom_L.jo" "IKXTrackBottom_L_orientConstraint1.cjo";
connectAttr "IKLeg_L.r" "IKXTrackBottom_L_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_L.ro" "IKXTrackBottom_L_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_L.pm" "IKXTrackBottom_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXTrackBottom_L_orientConstraint1.w0" "IKXTrackBottom_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXFrontTrack_End_L.tx" "effector4.tx";
connectAttr "IKXFrontTrack_End_L.ty" "effector4.ty";
connectAttr "IKXFrontTrack_End_L.tz" "effector4.tz";
connectAttr "IKXTrackBottom_L.tx" "effector3.tx";
connectAttr "IKXTrackBottom_L.ty" "effector3.ty";
connectAttr "IKXTrackBottom_L.tz" "effector3.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_L.v";
connectAttr "PoleAnnotateTargetLeg_LShape.wm" "PoleAnnotationLeg_LShape.dom" -na
		;
connectAttr "IKParentConstraintTrack_L.ro" "IKParentConstraintTrack_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintTrack_L.pim" "IKParentConstraintTrack_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintTrack_L.rp" "IKParentConstraintTrack_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintTrack_L.rpt" "IKParentConstraintTrack_L_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tt";
connectAttr "Pelvis_M.rp" "IKParentConstraintTrack_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "IKParentConstraintTrack_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tr";
connectAttr "Pelvis_M.ro" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "IKParentConstraintTrack_L_parentConstraint1.tg[0].ts";
connectAttr "Pelvis_M.pm" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.w0" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintLeg_R.v";
connectAttr "unitConversion2.o" "IKRollLegHeel_R.rx";
connectAttr "unitConversion3.o" "IKRollLegBall_R.rx";
connectAttr "IKXTrack_R.msg" "IKXLegHandle_R.hsj";
connectAttr "effector1.hp" "IKXLegHandle_R.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_R.hsv";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctx" "IKXLegHandle_R.pvx";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.cty" "IKXLegHandle_R.pvy";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctz" "IKXLegHandle_R.pvz";
connectAttr "IKXLegHandle_R.pim" "IKXLegHandle_R_poleVectorConstraint1.cpim";
connectAttr "IKXTrack_R.pm" "IKXLegHandle_R_poleVectorConstraint1.ps";
connectAttr "IKXTrack_R.t" "IKXLegHandle_R_poleVectorConstraint1.crp";
connectAttr "PoleLeg_R.t" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_R.rp" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_R.rpt" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_R.pm" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.w0" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBottom_R.msg" "IKXLegHandleBall_R.hsj";
connectAttr "effector2.hp" "IKXLegHandleBall_R.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_R.hsv";
connectAttr "PoleAimLeg_R_aimConstraint1.crx" "PoleAimLeg_R.rx";
connectAttr "PoleAimLeg_R_aimConstraint1.cry" "PoleAimLeg_R.ry";
connectAttr "PoleAimLeg_R_aimConstraint1.crz" "PoleAimLeg_R.rz";
connectAttr "PoleAimLeg_R_pointConstraint1.ctx" "PoleAimLeg_R.tx";
connectAttr "PoleAimLeg_R_pointConstraint1.cty" "PoleAimLeg_R.ty";
connectAttr "PoleAimLeg_R_pointConstraint1.ctz" "PoleAimLeg_R.tz";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_aimConstraint1.cpim";
connectAttr "PoleAimLeg_R.t" "PoleAimLeg_R_aimConstraint1.ct";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_aimConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.crt";
connectAttr "PoleAimLeg_R.ro" "PoleAimLeg_R_aimConstraint1.cro";
connectAttr "IKLeg_R.t" "PoleAimLeg_R_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PoleAimLeg_R_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PoleAimLeg_R_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_aimConstraint1.w0" "PoleAimLeg_R_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_R.wm" "PoleAimLeg_R_aimConstraint1.wum";
connectAttr "unitConversion1.o" "PoleAimLeg_R_aimConstraint1.ox";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_pointConstraint1.cpim";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_pointConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_pointConstraint1.crt";
connectAttr "IKXTrack_R.t" "PoleAimLeg_R_pointConstraint1.tg[0].tt";
connectAttr "IKXTrack_R.rp" "PoleAimLeg_R_pointConstraint1.tg[0].trp";
connectAttr "IKXTrack_R.rpt" "PoleAimLeg_R_pointConstraint1.tg[0].trt";
connectAttr "IKXTrack_R.pm" "PoleAimLeg_R_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_pointConstraint1.w0" "PoleAimLeg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctx" "PoleParentConstraintLeg_R.tx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cty" "PoleParentConstraintLeg_R.ty"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctz" "PoleParentConstraintLeg_R.tz"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crx" "PoleParentConstraintLeg_R.rx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cry" "PoleParentConstraintLeg_R.ry"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crz" "PoleParentConstraintLeg_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "PoleParentConstraintLeg_R.v";
connectAttr "PoleParentConstraintLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_R.pim" "PoleParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_RStatic.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_RStatic.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_RStatic.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w0" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_R.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_R.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_R.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_R.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w1" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_RSetRangeFollow.oy" "PoleParentConstraintLeg_R_parentConstraint1.w0"
		;
connectAttr "PoleLeg_RSetRangeFollow.ox" "PoleParentConstraintLeg_R_parentConstraint1.w1"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintLeg_L.v";
connectAttr "unitConversion5.o" "IKRollLegHeel_L.rx";
connectAttr "unitConversion6.o" "IKRollLegBall_L.rx";
connectAttr "IKXTrack_L.msg" "IKXLegHandle_L.hsj";
connectAttr "effector3.hp" "IKXLegHandle_L.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_L.hsv";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctx" "IKXLegHandle_L.pvx";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.cty" "IKXLegHandle_L.pvy";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctz" "IKXLegHandle_L.pvz";
connectAttr "IKXLegHandle_L.pim" "IKXLegHandle_L_poleVectorConstraint1.cpim";
connectAttr "IKXTrack_L.pm" "IKXLegHandle_L_poleVectorConstraint1.ps";
connectAttr "IKXTrack_L.t" "IKXLegHandle_L_poleVectorConstraint1.crp";
connectAttr "PoleLeg_L.t" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_L.rp" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_L.rpt" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_L.pm" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.w0" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBottom_L.msg" "IKXLegHandleBall_L.hsj";
connectAttr "effector4.hp" "IKXLegHandleBall_L.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_L.hsv";
connectAttr "PoleAimLeg_L_aimConstraint1.crx" "PoleAimLeg_L.rx";
connectAttr "PoleAimLeg_L_aimConstraint1.cry" "PoleAimLeg_L.ry";
connectAttr "PoleAimLeg_L_aimConstraint1.crz" "PoleAimLeg_L.rz";
connectAttr "PoleAimLeg_L_pointConstraint1.ctx" "PoleAimLeg_L.tx";
connectAttr "PoleAimLeg_L_pointConstraint1.cty" "PoleAimLeg_L.ty";
connectAttr "PoleAimLeg_L_pointConstraint1.ctz" "PoleAimLeg_L.tz";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_aimConstraint1.cpim";
connectAttr "PoleAimLeg_L.t" "PoleAimLeg_L_aimConstraint1.ct";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_aimConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.crt";
connectAttr "PoleAimLeg_L.ro" "PoleAimLeg_L_aimConstraint1.cro";
connectAttr "IKLeg_L.t" "PoleAimLeg_L_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_L.rp" "PoleAimLeg_L_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_L.pm" "PoleAimLeg_L_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_aimConstraint1.w0" "PoleAimLeg_L_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.wm" "PoleAimLeg_L_aimConstraint1.wum";
connectAttr "unitConversion4.o" "PoleAimLeg_L_aimConstraint1.ox";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_pointConstraint1.cpim";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_pointConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_pointConstraint1.crt";
connectAttr "IKXTrack_L.t" "PoleAimLeg_L_pointConstraint1.tg[0].tt";
connectAttr "IKXTrack_L.rp" "PoleAimLeg_L_pointConstraint1.tg[0].trp";
connectAttr "IKXTrack_L.rpt" "PoleAimLeg_L_pointConstraint1.tg[0].trt";
connectAttr "IKXTrack_L.pm" "PoleAimLeg_L_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_pointConstraint1.w0" "PoleAimLeg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctx" "PoleParentConstraintLeg_L.tx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cty" "PoleParentConstraintLeg_L.ty"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctz" "PoleParentConstraintLeg_L.tz"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crx" "PoleParentConstraintLeg_L.rx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cry" "PoleParentConstraintLeg_L.ry"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crz" "PoleParentConstraintLeg_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "PoleParentConstraintLeg_L.v";
connectAttr "PoleParentConstraintLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_L.pim" "PoleParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_LStatic.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_LStatic.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_LStatic.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w0" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_L.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_L.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_L.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_L.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w1" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_LSetRangeFollow.oy" "PoleParentConstraintLeg_L_parentConstraint1.w0"
		;
connectAttr "PoleLeg_LSetRangeFollow.ox" "PoleParentConstraintLeg_L_parentConstraint1.w1"
		;
connectAttr "Main.fkIkVis" "FKIKSystem.v";
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctx" "FKIKParentConstraintLeg_R.tx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cty" "FKIKParentConstraintLeg_R.ty"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctz" "FKIKParentConstraintLeg_R.tz"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crx" "FKIKParentConstraintLeg_R.rx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cry" "FKIKParentConstraintLeg_R.ry"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crz" "FKIKParentConstraintLeg_R.rz"
		;
connectAttr "FKIKParentConstraintLeg_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_R.pim" "FKIKParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tt";
connectAttr "Pelvis_M.rp" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tr";
connectAttr "Pelvis_M.ro" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].ts";
connectAttr "Pelvis_M.pm" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.w0" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctx" "FKIKParentConstraintLeg_L.tx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cty" "FKIKParentConstraintLeg_L.ty"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctz" "FKIKParentConstraintLeg_L.tz"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crx" "FKIKParentConstraintLeg_L.rx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cry" "FKIKParentConstraintLeg_L.ry"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crz" "FKIKParentConstraintLeg_L.rz"
		;
connectAttr "FKIKParentConstraintLeg_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_L.pim" "FKIKParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tt";
connectAttr "Pelvis_M.rp" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tr";
connectAttr "Pelvis_M.ro" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].ts";
connectAttr "Pelvis_M.pm" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.w0" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.cry" "PelvisCenterBtwLegs_M.ry"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctx" "PelvisCenterBtwLegs_M.tx"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctz" "PelvisCenterBtwLegs_M.tz"
		;
connectAttr "PelvisCenterBtwLegs_M.ro" "PelvisCenterBtwLegs_M_orientConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_orientConstraint1.cpim"
		;
connectAttr "IKLeg_R.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w0" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tr";
connectAttr "IKLeg_L.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tro";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w1" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tw"
		;
connectAttr "CenterBtwFeet_M.ox" "PelvisCenterBtwLegs_M_pointConstraint1.nds";
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_pointConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegs_M.rp" "PelvisCenterBtwLegs_M_pointConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegs_M.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.crt"
		;
connectAttr "IKLeg_R.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w0" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tt";
connectAttr "IKLeg_L.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trp";
connectAttr "IKLeg_L.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trt";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w1" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tw"
		;
connectAttr "Pelvis_M_pointConstraint1.ctx" "Pelvis_M.tx";
connectAttr "Pelvis_M_pointConstraint1.cty" "Pelvis_M.ty";
connectAttr "Pelvis_M_pointConstraint1.ctz" "Pelvis_M.tz";
connectAttr "Pelvis_M_orientConstraint1.crx" "Pelvis_M.rx";
connectAttr "Pelvis_M_orientConstraint1.cry" "Pelvis_M.ry";
connectAttr "Pelvis_M_orientConstraint1.crz" "Pelvis_M.rz";
connectAttr "FKPelvis_M.s" "Pelvis_M.s";
connectAttr "jointLayer.di" "Pelvis_M.do";
connectAttr "FKBody_M.s" "Body_M.s";
connectAttr "Pelvis_M.s" "Body_M.is";
connectAttr "Body_M_parentConstraint1.ctx" "Body_M.tx";
connectAttr "Body_M_parentConstraint1.cty" "Body_M.ty";
connectAttr "Body_M_parentConstraint1.ctz" "Body_M.tz";
connectAttr "Body_M_parentConstraint1.crx" "Body_M.rx";
connectAttr "Body_M_parentConstraint1.cry" "Body_M.ry";
connectAttr "Body_M_parentConstraint1.crz" "Body_M.rz";
connectAttr "jointLayer.di" "Body_M.do";
connectAttr "FKHead_M.s" "Head_M.s";
connectAttr "Body_M.s" "Head_M.is";
connectAttr "Head_M_parentConstraint1.ctx" "Head_M.tx";
connectAttr "Head_M_parentConstraint1.cty" "Head_M.ty";
connectAttr "Head_M_parentConstraint1.ctz" "Head_M.tz";
connectAttr "Head_M_parentConstraint1.crx" "Head_M.rx";
connectAttr "Head_M_parentConstraint1.cry" "Head_M.ry";
connectAttr "Head_M_parentConstraint1.crz" "Head_M.rz";
connectAttr "jointLayer.di" "Head_M.do";
connectAttr "Head_M.s" "Head_End_M.is";
connectAttr "jointLayer.di" "Head_End_M.do";
connectAttr "Head_M.ro" "Head_M_parentConstraint1.cro";
connectAttr "Head_M.pim" "Head_M_parentConstraint1.cpim";
connectAttr "Head_M.rp" "Head_M_parentConstraint1.crp";
connectAttr "Head_M.rpt" "Head_M_parentConstraint1.crt";
connectAttr "Head_M.jo" "Head_M_parentConstraint1.cjo";
connectAttr "FKXHead_M.t" "Head_M_parentConstraint1.tg[0].tt";
connectAttr "FKXHead_M.rp" "Head_M_parentConstraint1.tg[0].trp";
connectAttr "FKXHead_M.rpt" "Head_M_parentConstraint1.tg[0].trt";
connectAttr "FKXHead_M.r" "Head_M_parentConstraint1.tg[0].tr";
connectAttr "FKXHead_M.ro" "Head_M_parentConstraint1.tg[0].tro";
connectAttr "FKXHead_M.s" "Head_M_parentConstraint1.tg[0].ts";
connectAttr "FKXHead_M.pm" "Head_M_parentConstraint1.tg[0].tpm";
connectAttr "FKXHead_M.jo" "Head_M_parentConstraint1.tg[0].tjo";
connectAttr "Head_M_parentConstraint1.w0" "Head_M_parentConstraint1.tg[0].tw";
connectAttr "FKShoulder_R.s" "Shoulder_R.s";
connectAttr "Body_M.s" "Shoulder_R.is";
connectAttr "Shoulder_R_parentConstraint1.ctx" "Shoulder_R.tx";
connectAttr "Shoulder_R_parentConstraint1.cty" "Shoulder_R.ty";
connectAttr "Shoulder_R_parentConstraint1.ctz" "Shoulder_R.tz";
connectAttr "Shoulder_R_parentConstraint1.crx" "Shoulder_R.rx";
connectAttr "Shoulder_R_parentConstraint1.cry" "Shoulder_R.ry";
connectAttr "Shoulder_R_parentConstraint1.crz" "Shoulder_R.rz";
connectAttr "jointLayer.di" "Shoulder_R.do";
connectAttr "FKElbow5_R.s" "Elbow5_R.s";
connectAttr "Shoulder_R.s" "Elbow5_R.is";
connectAttr "Elbow5_R_parentConstraint1.ctx" "Elbow5_R.tx";
connectAttr "Elbow5_R_parentConstraint1.cty" "Elbow5_R.ty";
connectAttr "Elbow5_R_parentConstraint1.ctz" "Elbow5_R.tz";
connectAttr "Elbow5_R_parentConstraint1.crx" "Elbow5_R.rx";
connectAttr "Elbow5_R_parentConstraint1.cry" "Elbow5_R.ry";
connectAttr "Elbow5_R_parentConstraint1.crz" "Elbow5_R.rz";
connectAttr "jointLayer.di" "Elbow5_R.do";
connectAttr "FKWrist1_R.s" "Wrist1_R.s";
connectAttr "Elbow5_R.s" "Wrist1_R.is";
connectAttr "Wrist1_R_parentConstraint1.ctx" "Wrist1_R.tx";
connectAttr "Wrist1_R_parentConstraint1.cty" "Wrist1_R.ty";
connectAttr "Wrist1_R_parentConstraint1.ctz" "Wrist1_R.tz";
connectAttr "Wrist1_R_parentConstraint1.crx" "Wrist1_R.rx";
connectAttr "Wrist1_R_parentConstraint1.cry" "Wrist1_R.ry";
connectAttr "Wrist1_R_parentConstraint1.crz" "Wrist1_R.rz";
connectAttr "jointLayer.di" "Wrist1_R.do";
connectAttr "Wrist1_R.s" "Elbow4_R.is";
connectAttr "jointLayer.di" "Elbow4_R.do";
connectAttr "Wrist1_R.ro" "Wrist1_R_parentConstraint1.cro";
connectAttr "Wrist1_R.pim" "Wrist1_R_parentConstraint1.cpim";
connectAttr "Wrist1_R.rp" "Wrist1_R_parentConstraint1.crp";
connectAttr "Wrist1_R.rpt" "Wrist1_R_parentConstraint1.crt";
connectAttr "Wrist1_R.jo" "Wrist1_R_parentConstraint1.cjo";
connectAttr "FKXWrist1_R.t" "Wrist1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWrist1_R.rp" "Wrist1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWrist1_R.rpt" "Wrist1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWrist1_R.r" "Wrist1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWrist1_R.ro" "Wrist1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWrist1_R.s" "Wrist1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWrist1_R.pm" "Wrist1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWrist1_R.jo" "Wrist1_R_parentConstraint1.tg[0].tjo";
connectAttr "Wrist1_R_parentConstraint1.w0" "Wrist1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Elbow5_R.ro" "Elbow5_R_parentConstraint1.cro";
connectAttr "Elbow5_R.pim" "Elbow5_R_parentConstraint1.cpim";
connectAttr "Elbow5_R.rp" "Elbow5_R_parentConstraint1.crp";
connectAttr "Elbow5_R.rpt" "Elbow5_R_parentConstraint1.crt";
connectAttr "Elbow5_R.jo" "Elbow5_R_parentConstraint1.cjo";
connectAttr "FKXElbow5_R.t" "Elbow5_R_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow5_R.rp" "Elbow5_R_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow5_R.rpt" "Elbow5_R_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow5_R.r" "Elbow5_R_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow5_R.ro" "Elbow5_R_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow5_R.s" "Elbow5_R_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow5_R.pm" "Elbow5_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow5_R.jo" "Elbow5_R_parentConstraint1.tg[0].tjo";
connectAttr "Elbow5_R_parentConstraint1.w0" "Elbow5_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Shoulder_R.ro" "Shoulder_R_parentConstraint1.cro";
connectAttr "Shoulder_R.pim" "Shoulder_R_parentConstraint1.cpim";
connectAttr "Shoulder_R.rp" "Shoulder_R_parentConstraint1.crp";
connectAttr "Shoulder_R.rpt" "Shoulder_R_parentConstraint1.crt";
connectAttr "Shoulder_R.jo" "Shoulder_R_parentConstraint1.cjo";
connectAttr "FKXShoulder_R.t" "Shoulder_R_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder_R.rp" "Shoulder_R_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder_R.rpt" "Shoulder_R_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder_R.r" "Shoulder_R_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder_R.ro" "Shoulder_R_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder_R.s" "Shoulder_R_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder_R.pm" "Shoulder_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder_R.jo" "Shoulder_R_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder_R_parentConstraint1.w0" "Shoulder_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKShoulder1_L.s" "Shoulder1_L.s";
connectAttr "Body_M.s" "Shoulder1_L.is";
connectAttr "Shoulder1_L_parentConstraint1.ctx" "Shoulder1_L.tx";
connectAttr "Shoulder1_L_parentConstraint1.cty" "Shoulder1_L.ty";
connectAttr "Shoulder1_L_parentConstraint1.ctz" "Shoulder1_L.tz";
connectAttr "Shoulder1_L_parentConstraint1.crx" "Shoulder1_L.rx";
connectAttr "Shoulder1_L_parentConstraint1.cry" "Shoulder1_L.ry";
connectAttr "Shoulder1_L_parentConstraint1.crz" "Shoulder1_L.rz";
connectAttr "jointLayer.di" "Shoulder1_L.do";
connectAttr "FKElbow_L.s" "Elbow_L.s";
connectAttr "Shoulder1_L.s" "Elbow_L.is";
connectAttr "Elbow_L_parentConstraint1.ctx" "Elbow_L.tx";
connectAttr "Elbow_L_parentConstraint1.cty" "Elbow_L.ty";
connectAttr "Elbow_L_parentConstraint1.ctz" "Elbow_L.tz";
connectAttr "Elbow_L_parentConstraint1.crx" "Elbow_L.rx";
connectAttr "Elbow_L_parentConstraint1.cry" "Elbow_L.ry";
connectAttr "Elbow_L_parentConstraint1.crz" "Elbow_L.rz";
connectAttr "jointLayer.di" "Elbow_L.do";
connectAttr "FKWrist_L.s" "Wrist_L.s";
connectAttr "Elbow_L.s" "Wrist_L.is";
connectAttr "Wrist_L_parentConstraint1.ctx" "Wrist_L.tx";
connectAttr "Wrist_L_parentConstraint1.cty" "Wrist_L.ty";
connectAttr "Wrist_L_parentConstraint1.ctz" "Wrist_L.tz";
connectAttr "Wrist_L_parentConstraint1.crx" "Wrist_L.rx";
connectAttr "Wrist_L_parentConstraint1.cry" "Wrist_L.ry";
connectAttr "Wrist_L_parentConstraint1.crz" "Wrist_L.rz";
connectAttr "jointLayer.di" "Wrist_L.do";
connectAttr "FKMiddleFinger1_L.s" "MiddleFinger1_L.s";
connectAttr "Wrist_L.s" "MiddleFinger1_L.is";
connectAttr "MiddleFinger1_L_parentConstraint1.ctx" "MiddleFinger1_L.tx";
connectAttr "MiddleFinger1_L_parentConstraint1.cty" "MiddleFinger1_L.ty";
connectAttr "MiddleFinger1_L_parentConstraint1.ctz" "MiddleFinger1_L.tz";
connectAttr "MiddleFinger1_L_parentConstraint1.crx" "MiddleFinger1_L.rx";
connectAttr "MiddleFinger1_L_parentConstraint1.cry" "MiddleFinger1_L.ry";
connectAttr "MiddleFinger1_L_parentConstraint1.crz" "MiddleFinger1_L.rz";
connectAttr "jointLayer.di" "MiddleFinger1_L.do";
connectAttr "FKMiddleFinger2_L.s" "MiddleFinger2_L.s";
connectAttr "MiddleFinger1_L.s" "MiddleFinger2_L.is";
connectAttr "MiddleFinger2_L_parentConstraint1.ctx" "MiddleFinger2_L.tx";
connectAttr "MiddleFinger2_L_parentConstraint1.cty" "MiddleFinger2_L.ty";
connectAttr "MiddleFinger2_L_parentConstraint1.ctz" "MiddleFinger2_L.tz";
connectAttr "MiddleFinger2_L_parentConstraint1.crx" "MiddleFinger2_L.rx";
connectAttr "MiddleFinger2_L_parentConstraint1.cry" "MiddleFinger2_L.ry";
connectAttr "MiddleFinger2_L_parentConstraint1.crz" "MiddleFinger2_L.rz";
connectAttr "jointLayer.di" "MiddleFinger2_L.do";
connectAttr "FKMiddleFinger3_L.s" "MiddleFinger3_L.s";
connectAttr "MiddleFinger2_L.s" "MiddleFinger3_L.is";
connectAttr "MiddleFinger3_L_parentConstraint1.ctx" "MiddleFinger3_L.tx";
connectAttr "MiddleFinger3_L_parentConstraint1.cty" "MiddleFinger3_L.ty";
connectAttr "MiddleFinger3_L_parentConstraint1.ctz" "MiddleFinger3_L.tz";
connectAttr "MiddleFinger3_L_parentConstraint1.crx" "MiddleFinger3_L.rx";
connectAttr "MiddleFinger3_L_parentConstraint1.cry" "MiddleFinger3_L.ry";
connectAttr "MiddleFinger3_L_parentConstraint1.crz" "MiddleFinger3_L.rz";
connectAttr "jointLayer.di" "MiddleFinger3_L.do";
connectAttr "MiddleFinger3_L.s" "MiddleFinger4_End_L.is";
connectAttr "jointLayer.di" "MiddleFinger4_End_L.do";
connectAttr "MiddleFinger3_L.ro" "MiddleFinger3_L_parentConstraint1.cro";
connectAttr "MiddleFinger3_L.pim" "MiddleFinger3_L_parentConstraint1.cpim";
connectAttr "MiddleFinger3_L.rp" "MiddleFinger3_L_parentConstraint1.crp";
connectAttr "MiddleFinger3_L.rpt" "MiddleFinger3_L_parentConstraint1.crt";
connectAttr "MiddleFinger3_L.jo" "MiddleFinger3_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger3_L.t" "MiddleFinger3_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger3_L.rp" "MiddleFinger3_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger3_L.rpt" "MiddleFinger3_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger3_L.r" "MiddleFinger3_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger3_L.ro" "MiddleFinger3_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger3_L.s" "MiddleFinger3_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger3_L.pm" "MiddleFinger3_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger3_L.jo" "MiddleFinger3_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger3_L_parentConstraint1.w0" "MiddleFinger3_L_parentConstraint1.tg[0].tw"
		;
connectAttr "MiddleFinger2_L.ro" "MiddleFinger2_L_parentConstraint1.cro";
connectAttr "MiddleFinger2_L.pim" "MiddleFinger2_L_parentConstraint1.cpim";
connectAttr "MiddleFinger2_L.rp" "MiddleFinger2_L_parentConstraint1.crp";
connectAttr "MiddleFinger2_L.rpt" "MiddleFinger2_L_parentConstraint1.crt";
connectAttr "MiddleFinger2_L.jo" "MiddleFinger2_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger2_L.t" "MiddleFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger2_L.rp" "MiddleFinger2_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger2_L.rpt" "MiddleFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger2_L.r" "MiddleFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger2_L.ro" "MiddleFinger2_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger2_L.s" "MiddleFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger2_L.pm" "MiddleFinger2_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger2_L.jo" "MiddleFinger2_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger2_L_parentConstraint1.w0" "MiddleFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "MiddleFinger1_L.ro" "MiddleFinger1_L_parentConstraint1.cro";
connectAttr "MiddleFinger1_L.pim" "MiddleFinger1_L_parentConstraint1.cpim";
connectAttr "MiddleFinger1_L.rp" "MiddleFinger1_L_parentConstraint1.crp";
connectAttr "MiddleFinger1_L.rpt" "MiddleFinger1_L_parentConstraint1.crt";
connectAttr "MiddleFinger1_L.jo" "MiddleFinger1_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger1_L.t" "MiddleFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger1_L.rp" "MiddleFinger1_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger1_L.rpt" "MiddleFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger1_L.r" "MiddleFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger1_L.ro" "MiddleFinger1_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger1_L.s" "MiddleFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger1_L.pm" "MiddleFinger1_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger1_L.jo" "MiddleFinger1_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger1_L_parentConstraint1.w0" "MiddleFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIndexFinger1_L.s" "IndexFinger1_L.s";
connectAttr "Wrist_L.s" "IndexFinger1_L.is";
connectAttr "IndexFinger1_L_parentConstraint1.ctx" "IndexFinger1_L.tx";
connectAttr "IndexFinger1_L_parentConstraint1.cty" "IndexFinger1_L.ty";
connectAttr "IndexFinger1_L_parentConstraint1.ctz" "IndexFinger1_L.tz";
connectAttr "IndexFinger1_L_parentConstraint1.crx" "IndexFinger1_L.rx";
connectAttr "IndexFinger1_L_parentConstraint1.cry" "IndexFinger1_L.ry";
connectAttr "IndexFinger1_L_parentConstraint1.crz" "IndexFinger1_L.rz";
connectAttr "jointLayer.di" "IndexFinger1_L.do";
connectAttr "FKIndexFinger2_L.s" "IndexFinger2_L.s";
connectAttr "IndexFinger1_L.s" "IndexFinger2_L.is";
connectAttr "IndexFinger2_L_parentConstraint1.ctx" "IndexFinger2_L.tx";
connectAttr "IndexFinger2_L_parentConstraint1.cty" "IndexFinger2_L.ty";
connectAttr "IndexFinger2_L_parentConstraint1.ctz" "IndexFinger2_L.tz";
connectAttr "IndexFinger2_L_parentConstraint1.crx" "IndexFinger2_L.rx";
connectAttr "IndexFinger2_L_parentConstraint1.cry" "IndexFinger2_L.ry";
connectAttr "IndexFinger2_L_parentConstraint1.crz" "IndexFinger2_L.rz";
connectAttr "jointLayer.di" "IndexFinger2_L.do";
connectAttr "FKIndexFinger3_L.s" "IndexFinger3_L.s";
connectAttr "IndexFinger2_L.s" "IndexFinger3_L.is";
connectAttr "IndexFinger3_L_parentConstraint1.ctx" "IndexFinger3_L.tx";
connectAttr "IndexFinger3_L_parentConstraint1.cty" "IndexFinger3_L.ty";
connectAttr "IndexFinger3_L_parentConstraint1.ctz" "IndexFinger3_L.tz";
connectAttr "IndexFinger3_L_parentConstraint1.crx" "IndexFinger3_L.rx";
connectAttr "IndexFinger3_L_parentConstraint1.cry" "IndexFinger3_L.ry";
connectAttr "IndexFinger3_L_parentConstraint1.crz" "IndexFinger3_L.rz";
connectAttr "jointLayer.di" "IndexFinger3_L.do";
connectAttr "IndexFinger3_L.s" "IndexFinger4_End_L.is";
connectAttr "jointLayer.di" "IndexFinger4_End_L.do";
connectAttr "IndexFinger3_L.ro" "IndexFinger3_L_parentConstraint1.cro";
connectAttr "IndexFinger3_L.pim" "IndexFinger3_L_parentConstraint1.cpim";
connectAttr "IndexFinger3_L.rp" "IndexFinger3_L_parentConstraint1.crp";
connectAttr "IndexFinger3_L.rpt" "IndexFinger3_L_parentConstraint1.crt";
connectAttr "IndexFinger3_L.jo" "IndexFinger3_L_parentConstraint1.cjo";
connectAttr "FKXIndexFinger3_L.t" "IndexFinger3_L_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger3_L.rp" "IndexFinger3_L_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger3_L.rpt" "IndexFinger3_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger3_L.r" "IndexFinger3_L_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger3_L.ro" "IndexFinger3_L_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger3_L.s" "IndexFinger3_L_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger3_L.pm" "IndexFinger3_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger3_L.jo" "IndexFinger3_L_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger3_L_parentConstraint1.w0" "IndexFinger3_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IndexFinger2_L.ro" "IndexFinger2_L_parentConstraint1.cro";
connectAttr "IndexFinger2_L.pim" "IndexFinger2_L_parentConstraint1.cpim";
connectAttr "IndexFinger2_L.rp" "IndexFinger2_L_parentConstraint1.crp";
connectAttr "IndexFinger2_L.rpt" "IndexFinger2_L_parentConstraint1.crt";
connectAttr "IndexFinger2_L.jo" "IndexFinger2_L_parentConstraint1.cjo";
connectAttr "FKXIndexFinger2_L.t" "IndexFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger2_L.rp" "IndexFinger2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger2_L.rpt" "IndexFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger2_L.r" "IndexFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger2_L.ro" "IndexFinger2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger2_L.s" "IndexFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger2_L.pm" "IndexFinger2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger2_L.jo" "IndexFinger2_L_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger2_L_parentConstraint1.w0" "IndexFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IndexFinger1_L.ro" "IndexFinger1_L_parentConstraint1.cro";
connectAttr "IndexFinger1_L.pim" "IndexFinger1_L_parentConstraint1.cpim";
connectAttr "IndexFinger1_L.rp" "IndexFinger1_L_parentConstraint1.crp";
connectAttr "IndexFinger1_L.rpt" "IndexFinger1_L_parentConstraint1.crt";
connectAttr "IndexFinger1_L.jo" "IndexFinger1_L_parentConstraint1.cjo";
connectAttr "FKXIndexFinger1_L.t" "IndexFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger1_L.rp" "IndexFinger1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger1_L.rpt" "IndexFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger1_L.r" "IndexFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger1_L.ro" "IndexFinger1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger1_L.s" "IndexFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger1_L.pm" "IndexFinger1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger1_L.jo" "IndexFinger1_L_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger1_L_parentConstraint1.w0" "IndexFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKThumbFinger1_L.s" "ThumbFinger1_L.s";
connectAttr "Wrist_L.s" "ThumbFinger1_L.is";
connectAttr "ThumbFinger1_L_parentConstraint1.ctx" "ThumbFinger1_L.tx";
connectAttr "ThumbFinger1_L_parentConstraint1.cty" "ThumbFinger1_L.ty";
connectAttr "ThumbFinger1_L_parentConstraint1.ctz" "ThumbFinger1_L.tz";
connectAttr "ThumbFinger1_L_parentConstraint1.crx" "ThumbFinger1_L.rx";
connectAttr "ThumbFinger1_L_parentConstraint1.cry" "ThumbFinger1_L.ry";
connectAttr "ThumbFinger1_L_parentConstraint1.crz" "ThumbFinger1_L.rz";
connectAttr "jointLayer.di" "ThumbFinger1_L.do";
connectAttr "FKThumbFinger2_L.s" "ThumbFinger2_L.s";
connectAttr "ThumbFinger1_L.s" "ThumbFinger2_L.is";
connectAttr "ThumbFinger2_L_parentConstraint1.ctx" "ThumbFinger2_L.tx";
connectAttr "ThumbFinger2_L_parentConstraint1.cty" "ThumbFinger2_L.ty";
connectAttr "ThumbFinger2_L_parentConstraint1.ctz" "ThumbFinger2_L.tz";
connectAttr "ThumbFinger2_L_parentConstraint1.crx" "ThumbFinger2_L.rx";
connectAttr "ThumbFinger2_L_parentConstraint1.cry" "ThumbFinger2_L.ry";
connectAttr "ThumbFinger2_L_parentConstraint1.crz" "ThumbFinger2_L.rz";
connectAttr "jointLayer.di" "ThumbFinger2_L.do";
connectAttr "FKThumbFinger3_L.s" "ThumbFinger3_L.s";
connectAttr "ThumbFinger2_L.s" "ThumbFinger3_L.is";
connectAttr "ThumbFinger3_L_parentConstraint1.ctx" "ThumbFinger3_L.tx";
connectAttr "ThumbFinger3_L_parentConstraint1.cty" "ThumbFinger3_L.ty";
connectAttr "ThumbFinger3_L_parentConstraint1.ctz" "ThumbFinger3_L.tz";
connectAttr "ThumbFinger3_L_parentConstraint1.crx" "ThumbFinger3_L.rx";
connectAttr "ThumbFinger3_L_parentConstraint1.cry" "ThumbFinger3_L.ry";
connectAttr "ThumbFinger3_L_parentConstraint1.crz" "ThumbFinger3_L.rz";
connectAttr "jointLayer.di" "ThumbFinger3_L.do";
connectAttr "ThumbFinger3_L.s" "ThumbFinger4_End_L.is";
connectAttr "jointLayer.di" "ThumbFinger4_End_L.do";
connectAttr "ThumbFinger3_L.ro" "ThumbFinger3_L_parentConstraint1.cro";
connectAttr "ThumbFinger3_L.pim" "ThumbFinger3_L_parentConstraint1.cpim";
connectAttr "ThumbFinger3_L.rp" "ThumbFinger3_L_parentConstraint1.crp";
connectAttr "ThumbFinger3_L.rpt" "ThumbFinger3_L_parentConstraint1.crt";
connectAttr "ThumbFinger3_L.jo" "ThumbFinger3_L_parentConstraint1.cjo";
connectAttr "FKXThumbFinger3_L.t" "ThumbFinger3_L_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger3_L.rp" "ThumbFinger3_L_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger3_L.rpt" "ThumbFinger3_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger3_L.r" "ThumbFinger3_L_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger3_L.ro" "ThumbFinger3_L_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger3_L.s" "ThumbFinger3_L_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger3_L.pm" "ThumbFinger3_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger3_L.jo" "ThumbFinger3_L_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger3_L_parentConstraint1.w0" "ThumbFinger3_L_parentConstraint1.tg[0].tw"
		;
connectAttr "ThumbFinger2_L.ro" "ThumbFinger2_L_parentConstraint1.cro";
connectAttr "ThumbFinger2_L.pim" "ThumbFinger2_L_parentConstraint1.cpim";
connectAttr "ThumbFinger2_L.rp" "ThumbFinger2_L_parentConstraint1.crp";
connectAttr "ThumbFinger2_L.rpt" "ThumbFinger2_L_parentConstraint1.crt";
connectAttr "ThumbFinger2_L.jo" "ThumbFinger2_L_parentConstraint1.cjo";
connectAttr "FKXThumbFinger2_L.t" "ThumbFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger2_L.rp" "ThumbFinger2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger2_L.rpt" "ThumbFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger2_L.r" "ThumbFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger2_L.ro" "ThumbFinger2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger2_L.s" "ThumbFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger2_L.pm" "ThumbFinger2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger2_L.jo" "ThumbFinger2_L_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger2_L_parentConstraint1.w0" "ThumbFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "ThumbFinger1_L.ro" "ThumbFinger1_L_parentConstraint1.cro";
connectAttr "ThumbFinger1_L.pim" "ThumbFinger1_L_parentConstraint1.cpim";
connectAttr "ThumbFinger1_L.rp" "ThumbFinger1_L_parentConstraint1.crp";
connectAttr "ThumbFinger1_L.rpt" "ThumbFinger1_L_parentConstraint1.crt";
connectAttr "ThumbFinger1_L.jo" "ThumbFinger1_L_parentConstraint1.cjo";
connectAttr "FKXThumbFinger1_L.t" "ThumbFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger1_L.rp" "ThumbFinger1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger1_L.rpt" "ThumbFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger1_L.r" "ThumbFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger1_L.ro" "ThumbFinger1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger1_L.s" "ThumbFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger1_L.pm" "ThumbFinger1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger1_L.jo" "ThumbFinger1_L_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger1_L_parentConstraint1.w0" "ThumbFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKPinkyFinger1_L.s" "PinkyFinger1_L.s";
connectAttr "Wrist_L.s" "PinkyFinger1_L.is";
connectAttr "PinkyFinger1_L_parentConstraint1.ctx" "PinkyFinger1_L.tx";
connectAttr "PinkyFinger1_L_parentConstraint1.cty" "PinkyFinger1_L.ty";
connectAttr "PinkyFinger1_L_parentConstraint1.ctz" "PinkyFinger1_L.tz";
connectAttr "PinkyFinger1_L_parentConstraint1.crx" "PinkyFinger1_L.rx";
connectAttr "PinkyFinger1_L_parentConstraint1.cry" "PinkyFinger1_L.ry";
connectAttr "PinkyFinger1_L_parentConstraint1.crz" "PinkyFinger1_L.rz";
connectAttr "jointLayer.di" "PinkyFinger1_L.do";
connectAttr "FKPinkyFinger2_L.s" "PinkyFinger2_L.s";
connectAttr "PinkyFinger1_L.s" "PinkyFinger2_L.is";
connectAttr "PinkyFinger2_L_parentConstraint1.ctx" "PinkyFinger2_L.tx";
connectAttr "PinkyFinger2_L_parentConstraint1.cty" "PinkyFinger2_L.ty";
connectAttr "PinkyFinger2_L_parentConstraint1.ctz" "PinkyFinger2_L.tz";
connectAttr "PinkyFinger2_L_parentConstraint1.crx" "PinkyFinger2_L.rx";
connectAttr "PinkyFinger2_L_parentConstraint1.cry" "PinkyFinger2_L.ry";
connectAttr "PinkyFinger2_L_parentConstraint1.crz" "PinkyFinger2_L.rz";
connectAttr "jointLayer.di" "PinkyFinger2_L.do";
connectAttr "FKPinkyFinger3_L.s" "PinkyFinger3_L.s";
connectAttr "PinkyFinger2_L.s" "PinkyFinger3_L.is";
connectAttr "PinkyFinger3_L_parentConstraint1.ctx" "PinkyFinger3_L.tx";
connectAttr "PinkyFinger3_L_parentConstraint1.cty" "PinkyFinger3_L.ty";
connectAttr "PinkyFinger3_L_parentConstraint1.ctz" "PinkyFinger3_L.tz";
connectAttr "PinkyFinger3_L_parentConstraint1.crx" "PinkyFinger3_L.rx";
connectAttr "PinkyFinger3_L_parentConstraint1.cry" "PinkyFinger3_L.ry";
connectAttr "PinkyFinger3_L_parentConstraint1.crz" "PinkyFinger3_L.rz";
connectAttr "jointLayer.di" "PinkyFinger3_L.do";
connectAttr "PinkyFinger3_L.s" "PinkyFinger4_End_L.is";
connectAttr "jointLayer.di" "PinkyFinger4_End_L.do";
connectAttr "PinkyFinger3_L.ro" "PinkyFinger3_L_parentConstraint1.cro";
connectAttr "PinkyFinger3_L.pim" "PinkyFinger3_L_parentConstraint1.cpim";
connectAttr "PinkyFinger3_L.rp" "PinkyFinger3_L_parentConstraint1.crp";
connectAttr "PinkyFinger3_L.rpt" "PinkyFinger3_L_parentConstraint1.crt";
connectAttr "PinkyFinger3_L.jo" "PinkyFinger3_L_parentConstraint1.cjo";
connectAttr "FKXPinkyFinger3_L.t" "PinkyFinger3_L_parentConstraint1.tg[0].tt";
connectAttr "FKXPinkyFinger3_L.rp" "PinkyFinger3_L_parentConstraint1.tg[0].trp";
connectAttr "FKXPinkyFinger3_L.rpt" "PinkyFinger3_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXPinkyFinger3_L.r" "PinkyFinger3_L_parentConstraint1.tg[0].tr";
connectAttr "FKXPinkyFinger3_L.ro" "PinkyFinger3_L_parentConstraint1.tg[0].tro";
connectAttr "FKXPinkyFinger3_L.s" "PinkyFinger3_L_parentConstraint1.tg[0].ts";
connectAttr "FKXPinkyFinger3_L.pm" "PinkyFinger3_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXPinkyFinger3_L.jo" "PinkyFinger3_L_parentConstraint1.tg[0].tjo";
connectAttr "PinkyFinger3_L_parentConstraint1.w0" "PinkyFinger3_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PinkyFinger2_L.ro" "PinkyFinger2_L_parentConstraint1.cro";
connectAttr "PinkyFinger2_L.pim" "PinkyFinger2_L_parentConstraint1.cpim";
connectAttr "PinkyFinger2_L.rp" "PinkyFinger2_L_parentConstraint1.crp";
connectAttr "PinkyFinger2_L.rpt" "PinkyFinger2_L_parentConstraint1.crt";
connectAttr "PinkyFinger2_L.jo" "PinkyFinger2_L_parentConstraint1.cjo";
connectAttr "FKXPinkyFinger2_L.t" "PinkyFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXPinkyFinger2_L.rp" "PinkyFinger2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXPinkyFinger2_L.rpt" "PinkyFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXPinkyFinger2_L.r" "PinkyFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXPinkyFinger2_L.ro" "PinkyFinger2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXPinkyFinger2_L.s" "PinkyFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXPinkyFinger2_L.pm" "PinkyFinger2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXPinkyFinger2_L.jo" "PinkyFinger2_L_parentConstraint1.tg[0].tjo";
connectAttr "PinkyFinger2_L_parentConstraint1.w0" "PinkyFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PinkyFinger1_L.ro" "PinkyFinger1_L_parentConstraint1.cro";
connectAttr "PinkyFinger1_L.pim" "PinkyFinger1_L_parentConstraint1.cpim";
connectAttr "PinkyFinger1_L.rp" "PinkyFinger1_L_parentConstraint1.crp";
connectAttr "PinkyFinger1_L.rpt" "PinkyFinger1_L_parentConstraint1.crt";
connectAttr "PinkyFinger1_L.jo" "PinkyFinger1_L_parentConstraint1.cjo";
connectAttr "FKXPinkyFinger1_L.t" "PinkyFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXPinkyFinger1_L.rp" "PinkyFinger1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXPinkyFinger1_L.rpt" "PinkyFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXPinkyFinger1_L.r" "PinkyFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXPinkyFinger1_L.ro" "PinkyFinger1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXPinkyFinger1_L.s" "PinkyFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXPinkyFinger1_L.pm" "PinkyFinger1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXPinkyFinger1_L.jo" "PinkyFinger1_L_parentConstraint1.tg[0].tjo";
connectAttr "PinkyFinger1_L_parentConstraint1.w0" "PinkyFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Wrist_L.ro" "Wrist_L_parentConstraint1.cro";
connectAttr "Wrist_L.pim" "Wrist_L_parentConstraint1.cpim";
connectAttr "Wrist_L.rp" "Wrist_L_parentConstraint1.crp";
connectAttr "Wrist_L.rpt" "Wrist_L_parentConstraint1.crt";
connectAttr "Wrist_L.jo" "Wrist_L_parentConstraint1.cjo";
connectAttr "FKXWrist_L.t" "Wrist_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWrist_L.rp" "Wrist_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWrist_L.rpt" "Wrist_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWrist_L.r" "Wrist_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWrist_L.ro" "Wrist_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWrist_L.s" "Wrist_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWrist_L.pm" "Wrist_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWrist_L.jo" "Wrist_L_parentConstraint1.tg[0].tjo";
connectAttr "Wrist_L_parentConstraint1.w0" "Wrist_L_parentConstraint1.tg[0].tw";
connectAttr "Elbow_L.ro" "Elbow_L_parentConstraint1.cro";
connectAttr "Elbow_L.pim" "Elbow_L_parentConstraint1.cpim";
connectAttr "Elbow_L.rp" "Elbow_L_parentConstraint1.crp";
connectAttr "Elbow_L.rpt" "Elbow_L_parentConstraint1.crt";
connectAttr "Elbow_L.jo" "Elbow_L_parentConstraint1.cjo";
connectAttr "FKXElbow_L.t" "Elbow_L_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow_L.rp" "Elbow_L_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow_L.rpt" "Elbow_L_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow_L.r" "Elbow_L_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow_L.ro" "Elbow_L_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow_L.s" "Elbow_L_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow_L.pm" "Elbow_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow_L.jo" "Elbow_L_parentConstraint1.tg[0].tjo";
connectAttr "Elbow_L_parentConstraint1.w0" "Elbow_L_parentConstraint1.tg[0].tw";
connectAttr "Shoulder1_L.ro" "Shoulder1_L_parentConstraint1.cro";
connectAttr "Shoulder1_L.pim" "Shoulder1_L_parentConstraint1.cpim";
connectAttr "Shoulder1_L.rp" "Shoulder1_L_parentConstraint1.crp";
connectAttr "Shoulder1_L.rpt" "Shoulder1_L_parentConstraint1.crt";
connectAttr "Shoulder1_L.jo" "Shoulder1_L_parentConstraint1.cjo";
connectAttr "FKXShoulder1_L.t" "Shoulder1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder1_L.rp" "Shoulder1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder1_L.rpt" "Shoulder1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder1_L.r" "Shoulder1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder1_L.ro" "Shoulder1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder1_L.s" "Shoulder1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder1_L.pm" "Shoulder1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder1_L.jo" "Shoulder1_L_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder1_L_parentConstraint1.w0" "Shoulder1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKHose1_R.s" "Hose1_R.s";
connectAttr "Body_M.s" "Hose1_R.is";
connectAttr "Hose1_R_parentConstraint1.ctx" "Hose1_R.tx";
connectAttr "Hose1_R_parentConstraint1.cty" "Hose1_R.ty";
connectAttr "Hose1_R_parentConstraint1.ctz" "Hose1_R.tz";
connectAttr "Hose1_R_parentConstraint1.crx" "Hose1_R.rx";
connectAttr "Hose1_R_parentConstraint1.cry" "Hose1_R.ry";
connectAttr "Hose1_R_parentConstraint1.crz" "Hose1_R.rz";
connectAttr "jointLayer.di" "Hose1_R.do";
connectAttr "Hose1_R.s" "Hose1_End_R.is";
connectAttr "jointLayer.di" "Hose1_End_R.do";
connectAttr "Hose1_R.ro" "Hose1_R_parentConstraint1.cro";
connectAttr "Hose1_R.pim" "Hose1_R_parentConstraint1.cpim";
connectAttr "Hose1_R.rp" "Hose1_R_parentConstraint1.crp";
connectAttr "Hose1_R.rpt" "Hose1_R_parentConstraint1.crt";
connectAttr "Hose1_R.jo" "Hose1_R_parentConstraint1.cjo";
connectAttr "FKXHose1_R.t" "Hose1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHose1_R.rp" "Hose1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHose1_R.rpt" "Hose1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHose1_R.r" "Hose1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHose1_R.ro" "Hose1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHose1_R.s" "Hose1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHose1_R.pm" "Hose1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHose1_R.jo" "Hose1_R_parentConstraint1.tg[0].tjo";
connectAttr "Hose1_R_parentConstraint1.w0" "Hose1_R_parentConstraint1.tg[0].tw";
connectAttr "FKHose2_R.s" "Hose2_R.s";
connectAttr "Body_M.s" "Hose2_R.is";
connectAttr "Hose2_R_parentConstraint1.ctx" "Hose2_R.tx";
connectAttr "Hose2_R_parentConstraint1.cty" "Hose2_R.ty";
connectAttr "Hose2_R_parentConstraint1.ctz" "Hose2_R.tz";
connectAttr "Hose2_R_parentConstraint1.crx" "Hose2_R.rx";
connectAttr "Hose2_R_parentConstraint1.cry" "Hose2_R.ry";
connectAttr "Hose2_R_parentConstraint1.crz" "Hose2_R.rz";
connectAttr "jointLayer.di" "Hose2_R.do";
connectAttr "Hose2_R.s" "Hose2_End_R.is";
connectAttr "jointLayer.di" "Hose2_End_R.do";
connectAttr "Hose2_R.ro" "Hose2_R_parentConstraint1.cro";
connectAttr "Hose2_R.pim" "Hose2_R_parentConstraint1.cpim";
connectAttr "Hose2_R.rp" "Hose2_R_parentConstraint1.crp";
connectAttr "Hose2_R.rpt" "Hose2_R_parentConstraint1.crt";
connectAttr "Hose2_R.jo" "Hose2_R_parentConstraint1.cjo";
connectAttr "FKXHose2_R.t" "Hose2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHose2_R.rp" "Hose2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHose2_R.rpt" "Hose2_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHose2_R.r" "Hose2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHose2_R.ro" "Hose2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHose2_R.s" "Hose2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHose2_R.pm" "Hose2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHose2_R.jo" "Hose2_R_parentConstraint1.tg[0].tjo";
connectAttr "Hose2_R_parentConstraint1.w0" "Hose2_R_parentConstraint1.tg[0].tw";
connectAttr "FKHose1_L.s" "Hose1_L.s";
connectAttr "Body_M.s" "Hose1_L.is";
connectAttr "Hose1_L_parentConstraint1.ctx" "Hose1_L.tx";
connectAttr "Hose1_L_parentConstraint1.cty" "Hose1_L.ty";
connectAttr "Hose1_L_parentConstraint1.ctz" "Hose1_L.tz";
connectAttr "Hose1_L_parentConstraint1.crx" "Hose1_L.rx";
connectAttr "Hose1_L_parentConstraint1.cry" "Hose1_L.ry";
connectAttr "Hose1_L_parentConstraint1.crz" "Hose1_L.rz";
connectAttr "jointLayer.di" "Hose1_L.do";
connectAttr "Hose1_L.s" "Hose1_End_L.is";
connectAttr "jointLayer.di" "Hose1_End_L.do";
connectAttr "Hose1_L.ro" "Hose1_L_parentConstraint1.cro";
connectAttr "Hose1_L.pim" "Hose1_L_parentConstraint1.cpim";
connectAttr "Hose1_L.rp" "Hose1_L_parentConstraint1.crp";
connectAttr "Hose1_L.rpt" "Hose1_L_parentConstraint1.crt";
connectAttr "Hose1_L.jo" "Hose1_L_parentConstraint1.cjo";
connectAttr "FKXHose1_L.t" "Hose1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHose1_L.rp" "Hose1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHose1_L.rpt" "Hose1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHose1_L.r" "Hose1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHose1_L.ro" "Hose1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHose1_L.s" "Hose1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHose1_L.pm" "Hose1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHose1_L.jo" "Hose1_L_parentConstraint1.tg[0].tjo";
connectAttr "Hose1_L_parentConstraint1.w0" "Hose1_L_parentConstraint1.tg[0].tw";
connectAttr "FKHose2_L.s" "Hose2_L.s";
connectAttr "Body_M.s" "Hose2_L.is";
connectAttr "Hose2_L_parentConstraint1.ctx" "Hose2_L.tx";
connectAttr "Hose2_L_parentConstraint1.cty" "Hose2_L.ty";
connectAttr "Hose2_L_parentConstraint1.ctz" "Hose2_L.tz";
connectAttr "Hose2_L_parentConstraint1.crx" "Hose2_L.rx";
connectAttr "Hose2_L_parentConstraint1.cry" "Hose2_L.ry";
connectAttr "Hose2_L_parentConstraint1.crz" "Hose2_L.rz";
connectAttr "jointLayer.di" "Hose2_L.do";
connectAttr "Hose2_L.s" "Hose2_End_L.is";
connectAttr "jointLayer.di" "Hose2_End_L.do";
connectAttr "Hose2_L.ro" "Hose2_L_parentConstraint1.cro";
connectAttr "Hose2_L.pim" "Hose2_L_parentConstraint1.cpim";
connectAttr "Hose2_L.rp" "Hose2_L_parentConstraint1.crp";
connectAttr "Hose2_L.rpt" "Hose2_L_parentConstraint1.crt";
connectAttr "Hose2_L.jo" "Hose2_L_parentConstraint1.cjo";
connectAttr "FKXHose2_L.t" "Hose2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHose2_L.rp" "Hose2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHose2_L.rpt" "Hose2_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHose2_L.r" "Hose2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHose2_L.ro" "Hose2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHose2_L.s" "Hose2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHose2_L.pm" "Hose2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHose2_L.jo" "Hose2_L_parentConstraint1.tg[0].tjo";
connectAttr "Hose2_L_parentConstraint1.w0" "Hose2_L_parentConstraint1.tg[0].tw";
connectAttr "Body_M.ro" "Body_M_parentConstraint1.cro";
connectAttr "Body_M.pim" "Body_M_parentConstraint1.cpim";
connectAttr "Body_M.rp" "Body_M_parentConstraint1.crp";
connectAttr "Body_M.rpt" "Body_M_parentConstraint1.crt";
connectAttr "Body_M.jo" "Body_M_parentConstraint1.cjo";
connectAttr "FKXBody_M.t" "Body_M_parentConstraint1.tg[0].tt";
connectAttr "FKXBody_M.rp" "Body_M_parentConstraint1.tg[0].trp";
connectAttr "FKXBody_M.rpt" "Body_M_parentConstraint1.tg[0].trt";
connectAttr "FKXBody_M.r" "Body_M_parentConstraint1.tg[0].tr";
connectAttr "FKXBody_M.ro" "Body_M_parentConstraint1.tg[0].tro";
connectAttr "FKXBody_M.s" "Body_M_parentConstraint1.tg[0].ts";
connectAttr "FKXBody_M.pm" "Body_M_parentConstraint1.tg[0].tpm";
connectAttr "FKXBody_M.jo" "Body_M_parentConstraint1.tg[0].tjo";
connectAttr "Body_M_parentConstraint1.w0" "Body_M_parentConstraint1.tg[0].tw";
connectAttr "ScaleBlendTrack_R.op" "Track_R.s";
connectAttr "Pelvis_M.s" "Track_R.is";
connectAttr "Track_R_parentConstraint1.ctx" "Track_R.tx";
connectAttr "Track_R_parentConstraint1.cty" "Track_R.ty";
connectAttr "Track_R_parentConstraint1.ctz" "Track_R.tz";
connectAttr "Track_R_parentConstraint1.crx" "Track_R.rx";
connectAttr "Track_R_parentConstraint1.cry" "Track_R.ry";
connectAttr "Track_R_parentConstraint1.crz" "Track_R.rz";
connectAttr "jointLayer.di" "Track_R.do";
connectAttr "ScaleBlendTrackBend_R.op" "TrackBend_R.s";
connectAttr "Track_R.s" "TrackBend_R.is";
connectAttr "TrackBend_R_parentConstraint1.ctx" "TrackBend_R.tx";
connectAttr "TrackBend_R_parentConstraint1.cty" "TrackBend_R.ty";
connectAttr "TrackBend_R_parentConstraint1.ctz" "TrackBend_R.tz";
connectAttr "TrackBend_R_parentConstraint1.crx" "TrackBend_R.rx";
connectAttr "TrackBend_R_parentConstraint1.cry" "TrackBend_R.ry";
connectAttr "TrackBend_R_parentConstraint1.crz" "TrackBend_R.rz";
connectAttr "jointLayer.di" "TrackBend_R.do";
connectAttr "TrackBottom_R_parentConstraint1.ctx" "TrackBottom_R.tx";
connectAttr "TrackBottom_R_parentConstraint1.cty" "TrackBottom_R.ty";
connectAttr "TrackBottom_R_parentConstraint1.ctz" "TrackBottom_R.tz";
connectAttr "TrackBottom_R_parentConstraint1.crx" "TrackBottom_R.rx";
connectAttr "TrackBottom_R_parentConstraint1.cry" "TrackBottom_R.ry";
connectAttr "TrackBottom_R_parentConstraint1.crz" "TrackBottom_R.rz";
connectAttr "ScaleBlendTrackBottom_R.op" "TrackBottom_R.s";
connectAttr "TrackBend_R.s" "TrackBottom_R.is";
connectAttr "jointLayer.di" "TrackBottom_R.do";
connectAttr "FKWheel1_R.s" "Wheel1_R.s";
connectAttr "TrackBottom_R.s" "Wheel1_R.is";
connectAttr "Wheel1_R_parentConstraint1.ctx" "Wheel1_R.tx";
connectAttr "Wheel1_R_parentConstraint1.cty" "Wheel1_R.ty";
connectAttr "Wheel1_R_parentConstraint1.ctz" "Wheel1_R.tz";
connectAttr "Wheel1_R_parentConstraint1.crx" "Wheel1_R.rx";
connectAttr "Wheel1_R_parentConstraint1.cry" "Wheel1_R.ry";
connectAttr "Wheel1_R_parentConstraint1.crz" "Wheel1_R.rz";
connectAttr "jointLayer.di" "Wheel1_R.do";
connectAttr "Wheel1_R.s" "Wheel1_End_R.is";
connectAttr "jointLayer.di" "Wheel1_End_R.do";
connectAttr "Wheel1_R.ro" "Wheel1_R_parentConstraint1.cro";
connectAttr "Wheel1_R.pim" "Wheel1_R_parentConstraint1.cpim";
connectAttr "Wheel1_R.rp" "Wheel1_R_parentConstraint1.crp";
connectAttr "Wheel1_R.rpt" "Wheel1_R_parentConstraint1.crt";
connectAttr "Wheel1_R.jo" "Wheel1_R_parentConstraint1.cjo";
connectAttr "FKXWheel1_R.t" "Wheel1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel1_R.rp" "Wheel1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel1_R.rpt" "Wheel1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel1_R.r" "Wheel1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel1_R.ro" "Wheel1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel1_R.s" "Wheel1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel1_R.pm" "Wheel1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel1_R.jo" "Wheel1_R_parentConstraint1.tg[0].tjo";
connectAttr "Wheel1_R_parentConstraint1.w0" "Wheel1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel2_R.s" "Wheel2_R.s";
connectAttr "TrackBottom_R.s" "Wheel2_R.is";
connectAttr "Wheel2_R_parentConstraint1.ctx" "Wheel2_R.tx";
connectAttr "Wheel2_R_parentConstraint1.cty" "Wheel2_R.ty";
connectAttr "Wheel2_R_parentConstraint1.ctz" "Wheel2_R.tz";
connectAttr "Wheel2_R_parentConstraint1.crx" "Wheel2_R.rx";
connectAttr "Wheel2_R_parentConstraint1.cry" "Wheel2_R.ry";
connectAttr "Wheel2_R_parentConstraint1.crz" "Wheel2_R.rz";
connectAttr "jointLayer.di" "Wheel2_R.do";
connectAttr "Wheel2_R.s" "Wheel2_End_R.is";
connectAttr "jointLayer.di" "Wheel2_End_R.do";
connectAttr "Wheel2_R.ro" "Wheel2_R_parentConstraint1.cro";
connectAttr "Wheel2_R.pim" "Wheel2_R_parentConstraint1.cpim";
connectAttr "Wheel2_R.rp" "Wheel2_R_parentConstraint1.crp";
connectAttr "Wheel2_R.rpt" "Wheel2_R_parentConstraint1.crt";
connectAttr "Wheel2_R.jo" "Wheel2_R_parentConstraint1.cjo";
connectAttr "FKXWheel2_R.t" "Wheel2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel2_R.rp" "Wheel2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel2_R.rpt" "Wheel2_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel2_R.r" "Wheel2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel2_R.ro" "Wheel2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel2_R.s" "Wheel2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel2_R.pm" "Wheel2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel2_R.jo" "Wheel2_R_parentConstraint1.tg[0].tjo";
connectAttr "Wheel2_R_parentConstraint1.w0" "Wheel2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel3_R.s" "Wheel3_R.s";
connectAttr "TrackBottom_R.s" "Wheel3_R.is";
connectAttr "Wheel3_R_parentConstraint1.ctx" "Wheel3_R.tx";
connectAttr "Wheel3_R_parentConstraint1.cty" "Wheel3_R.ty";
connectAttr "Wheel3_R_parentConstraint1.ctz" "Wheel3_R.tz";
connectAttr "Wheel3_R_parentConstraint1.crx" "Wheel3_R.rx";
connectAttr "Wheel3_R_parentConstraint1.cry" "Wheel3_R.ry";
connectAttr "Wheel3_R_parentConstraint1.crz" "Wheel3_R.rz";
connectAttr "jointLayer.di" "Wheel3_R.do";
connectAttr "Wheel3_R.s" "Wheel3_End_R.is";
connectAttr "jointLayer.di" "Wheel3_End_R.do";
connectAttr "Wheel3_R.ro" "Wheel3_R_parentConstraint1.cro";
connectAttr "Wheel3_R.pim" "Wheel3_R_parentConstraint1.cpim";
connectAttr "Wheel3_R.rp" "Wheel3_R_parentConstraint1.crp";
connectAttr "Wheel3_R.rpt" "Wheel3_R_parentConstraint1.crt";
connectAttr "Wheel3_R.jo" "Wheel3_R_parentConstraint1.cjo";
connectAttr "FKXWheel3_R.t" "Wheel3_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel3_R.rp" "Wheel3_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel3_R.rpt" "Wheel3_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel3_R.r" "Wheel3_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel3_R.ro" "Wheel3_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel3_R.s" "Wheel3_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel3_R.pm" "Wheel3_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel3_R.jo" "Wheel3_R_parentConstraint1.tg[0].tjo";
connectAttr "Wheel3_R_parentConstraint1.w0" "Wheel3_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel4_R.s" "Wheel4_R.s";
connectAttr "TrackBottom_R.s" "Wheel4_R.is";
connectAttr "Wheel4_R_parentConstraint1.ctx" "Wheel4_R.tx";
connectAttr "Wheel4_R_parentConstraint1.cty" "Wheel4_R.ty";
connectAttr "Wheel4_R_parentConstraint1.ctz" "Wheel4_R.tz";
connectAttr "Wheel4_R_parentConstraint1.crx" "Wheel4_R.rx";
connectAttr "Wheel4_R_parentConstraint1.cry" "Wheel4_R.ry";
connectAttr "Wheel4_R_parentConstraint1.crz" "Wheel4_R.rz";
connectAttr "jointLayer.di" "Wheel4_R.do";
connectAttr "Wheel4_R.s" "Wheel4_End_R.is";
connectAttr "jointLayer.di" "Wheel4_End_R.do";
connectAttr "Wheel4_R.ro" "Wheel4_R_parentConstraint1.cro";
connectAttr "Wheel4_R.pim" "Wheel4_R_parentConstraint1.cpim";
connectAttr "Wheel4_R.rp" "Wheel4_R_parentConstraint1.crp";
connectAttr "Wheel4_R.rpt" "Wheel4_R_parentConstraint1.crt";
connectAttr "Wheel4_R.jo" "Wheel4_R_parentConstraint1.cjo";
connectAttr "FKXWheel4_R.t" "Wheel4_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel4_R.rp" "Wheel4_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel4_R.rpt" "Wheel4_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel4_R.r" "Wheel4_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel4_R.ro" "Wheel4_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel4_R.s" "Wheel4_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel4_R.pm" "Wheel4_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel4_R.jo" "Wheel4_R_parentConstraint1.tg[0].tjo";
connectAttr "Wheel4_R_parentConstraint1.w0" "Wheel4_R_parentConstraint1.tg[0].tw"
		;
connectAttr "TrackBottom_R.s" "FrontTrack_End_R.is";
connectAttr "jointLayer.di" "FrontTrack_End_R.do";
connectAttr "TrackBottom_R.ro" "TrackBottom_R_parentConstraint1.cro";
connectAttr "TrackBottom_R.pim" "TrackBottom_R_parentConstraint1.cpim";
connectAttr "TrackBottom_R.rp" "TrackBottom_R_parentConstraint1.crp";
connectAttr "TrackBottom_R.rpt" "TrackBottom_R_parentConstraint1.crt";
connectAttr "TrackBottom_R.jo" "TrackBottom_R_parentConstraint1.cjo";
connectAttr "FKXTrackBottom_R.t" "TrackBottom_R_parentConstraint1.tg[0].tt";
connectAttr "FKXTrackBottom_R.rp" "TrackBottom_R_parentConstraint1.tg[0].trp";
connectAttr "FKXTrackBottom_R.rpt" "TrackBottom_R_parentConstraint1.tg[0].trt";
connectAttr "FKXTrackBottom_R.r" "TrackBottom_R_parentConstraint1.tg[0].tr";
connectAttr "FKXTrackBottom_R.ro" "TrackBottom_R_parentConstraint1.tg[0].tro";
connectAttr "FKXTrackBottom_R.s" "TrackBottom_R_parentConstraint1.tg[0].ts";
connectAttr "FKXTrackBottom_R.pm" "TrackBottom_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrackBottom_R.jo" "TrackBottom_R_parentConstraint1.tg[0].tjo";
connectAttr "TrackBottom_R_parentConstraint1.w0" "TrackBottom_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBottom_R.t" "TrackBottom_R_parentConstraint1.tg[1].tt";
connectAttr "IKXTrackBottom_R.rp" "TrackBottom_R_parentConstraint1.tg[1].trp";
connectAttr "IKXTrackBottom_R.rpt" "TrackBottom_R_parentConstraint1.tg[1].trt";
connectAttr "IKXTrackBottom_R.r" "TrackBottom_R_parentConstraint1.tg[1].tr";
connectAttr "IKXTrackBottom_R.ro" "TrackBottom_R_parentConstraint1.tg[1].tro";
connectAttr "IKXTrackBottom_R.s" "TrackBottom_R_parentConstraint1.tg[1].ts";
connectAttr "IKXTrackBottom_R.pm" "TrackBottom_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrackBottom_R.jo" "TrackBottom_R_parentConstraint1.tg[1].tjo";
connectAttr "TrackBottom_R_parentConstraint1.w1" "TrackBottom_R_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendLegReverse_R.ox" "TrackBottom_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "TrackBottom_R_parentConstraint1.w1"
		;
connectAttr "TrackBend_R.ro" "TrackBend_R_parentConstraint1.cro";
connectAttr "TrackBend_R.pim" "TrackBend_R_parentConstraint1.cpim";
connectAttr "TrackBend_R.rp" "TrackBend_R_parentConstraint1.crp";
connectAttr "TrackBend_R.rpt" "TrackBend_R_parentConstraint1.crt";
connectAttr "TrackBend_R.jo" "TrackBend_R_parentConstraint1.cjo";
connectAttr "FKXTrackBend_R.t" "TrackBend_R_parentConstraint1.tg[0].tt";
connectAttr "FKXTrackBend_R.rp" "TrackBend_R_parentConstraint1.tg[0].trp";
connectAttr "FKXTrackBend_R.rpt" "TrackBend_R_parentConstraint1.tg[0].trt";
connectAttr "FKXTrackBend_R.r" "TrackBend_R_parentConstraint1.tg[0].tr";
connectAttr "FKXTrackBend_R.ro" "TrackBend_R_parentConstraint1.tg[0].tro";
connectAttr "FKXTrackBend_R.s" "TrackBend_R_parentConstraint1.tg[0].ts";
connectAttr "FKXTrackBend_R.pm" "TrackBend_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrackBend_R.jo" "TrackBend_R_parentConstraint1.tg[0].tjo";
connectAttr "TrackBend_R_parentConstraint1.w0" "TrackBend_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBend_R.t" "TrackBend_R_parentConstraint1.tg[1].tt";
connectAttr "IKXTrackBend_R.rp" "TrackBend_R_parentConstraint1.tg[1].trp";
connectAttr "IKXTrackBend_R.rpt" "TrackBend_R_parentConstraint1.tg[1].trt";
connectAttr "IKXTrackBend_R.r" "TrackBend_R_parentConstraint1.tg[1].tr";
connectAttr "IKXTrackBend_R.ro" "TrackBend_R_parentConstraint1.tg[1].tro";
connectAttr "IKXTrackBend_R.s" "TrackBend_R_parentConstraint1.tg[1].ts";
connectAttr "IKXTrackBend_R.pm" "TrackBend_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrackBend_R.jo" "TrackBend_R_parentConstraint1.tg[1].tjo";
connectAttr "TrackBend_R_parentConstraint1.w1" "TrackBend_R_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendLegReverse_R.ox" "TrackBend_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "TrackBend_R_parentConstraint1.w1";
connectAttr "Track_R.ro" "Track_R_parentConstraint1.cro";
connectAttr "Track_R.pim" "Track_R_parentConstraint1.cpim";
connectAttr "Track_R.rp" "Track_R_parentConstraint1.crp";
connectAttr "Track_R.rpt" "Track_R_parentConstraint1.crt";
connectAttr "Track_R.jo" "Track_R_parentConstraint1.cjo";
connectAttr "FKXTrack_R.t" "Track_R_parentConstraint1.tg[0].tt";
connectAttr "FKXTrack_R.rp" "Track_R_parentConstraint1.tg[0].trp";
connectAttr "FKXTrack_R.rpt" "Track_R_parentConstraint1.tg[0].trt";
connectAttr "FKXTrack_R.r" "Track_R_parentConstraint1.tg[0].tr";
connectAttr "FKXTrack_R.ro" "Track_R_parentConstraint1.tg[0].tro";
connectAttr "FKXTrack_R.s" "Track_R_parentConstraint1.tg[0].ts";
connectAttr "FKXTrack_R.pm" "Track_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrack_R.jo" "Track_R_parentConstraint1.tg[0].tjo";
connectAttr "Track_R_parentConstraint1.w0" "Track_R_parentConstraint1.tg[0].tw";
connectAttr "IKXTrack_R.t" "Track_R_parentConstraint1.tg[1].tt";
connectAttr "IKXTrack_R.rp" "Track_R_parentConstraint1.tg[1].trp";
connectAttr "IKXTrack_R.rpt" "Track_R_parentConstraint1.tg[1].trt";
connectAttr "IKXTrack_R.r" "Track_R_parentConstraint1.tg[1].tr";
connectAttr "IKXTrack_R.ro" "Track_R_parentConstraint1.tg[1].tro";
connectAttr "IKXTrack_R.s" "Track_R_parentConstraint1.tg[1].ts";
connectAttr "IKXTrack_R.pm" "Track_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrack_R.jo" "Track_R_parentConstraint1.tg[1].tjo";
connectAttr "Track_R_parentConstraint1.w1" "Track_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Track_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Track_R_parentConstraint1.w1";
connectAttr "ScaleBlendTrack_L.op" "Track_L.s";
connectAttr "Pelvis_M.s" "Track_L.is";
connectAttr "Track_L_parentConstraint1.ctx" "Track_L.tx";
connectAttr "Track_L_parentConstraint1.cty" "Track_L.ty";
connectAttr "Track_L_parentConstraint1.ctz" "Track_L.tz";
connectAttr "Track_L_parentConstraint1.crx" "Track_L.rx";
connectAttr "Track_L_parentConstraint1.cry" "Track_L.ry";
connectAttr "Track_L_parentConstraint1.crz" "Track_L.rz";
connectAttr "jointLayer.di" "Track_L.do";
connectAttr "ScaleBlendTrackBend_L.op" "TrackBend_L.s";
connectAttr "Track_L.s" "TrackBend_L.is";
connectAttr "TrackBend_L_parentConstraint1.ctx" "TrackBend_L.tx";
connectAttr "TrackBend_L_parentConstraint1.cty" "TrackBend_L.ty";
connectAttr "TrackBend_L_parentConstraint1.ctz" "TrackBend_L.tz";
connectAttr "TrackBend_L_parentConstraint1.crx" "TrackBend_L.rx";
connectAttr "TrackBend_L_parentConstraint1.cry" "TrackBend_L.ry";
connectAttr "TrackBend_L_parentConstraint1.crz" "TrackBend_L.rz";
connectAttr "jointLayer.di" "TrackBend_L.do";
connectAttr "TrackBottom_L_parentConstraint1.ctx" "TrackBottom_L.tx";
connectAttr "TrackBottom_L_parentConstraint1.cty" "TrackBottom_L.ty";
connectAttr "TrackBottom_L_parentConstraint1.ctz" "TrackBottom_L.tz";
connectAttr "TrackBottom_L_parentConstraint1.crx" "TrackBottom_L.rx";
connectAttr "TrackBottom_L_parentConstraint1.cry" "TrackBottom_L.ry";
connectAttr "TrackBottom_L_parentConstraint1.crz" "TrackBottom_L.rz";
connectAttr "ScaleBlendTrackBottom_L.op" "TrackBottom_L.s";
connectAttr "TrackBend_L.s" "TrackBottom_L.is";
connectAttr "jointLayer.di" "TrackBottom_L.do";
connectAttr "FKWheel1_L.s" "Wheel1_L.s";
connectAttr "TrackBottom_L.s" "Wheel1_L.is";
connectAttr "Wheel1_L_parentConstraint1.ctx" "Wheel1_L.tx";
connectAttr "Wheel1_L_parentConstraint1.cty" "Wheel1_L.ty";
connectAttr "Wheel1_L_parentConstraint1.ctz" "Wheel1_L.tz";
connectAttr "Wheel1_L_parentConstraint1.crx" "Wheel1_L.rx";
connectAttr "Wheel1_L_parentConstraint1.cry" "Wheel1_L.ry";
connectAttr "Wheel1_L_parentConstraint1.crz" "Wheel1_L.rz";
connectAttr "jointLayer.di" "Wheel1_L.do";
connectAttr "Wheel1_L.s" "Wheel1_End_L.is";
connectAttr "jointLayer.di" "Wheel1_End_L.do";
connectAttr "Wheel1_L.ro" "Wheel1_L_parentConstraint1.cro";
connectAttr "Wheel1_L.pim" "Wheel1_L_parentConstraint1.cpim";
connectAttr "Wheel1_L.rp" "Wheel1_L_parentConstraint1.crp";
connectAttr "Wheel1_L.rpt" "Wheel1_L_parentConstraint1.crt";
connectAttr "Wheel1_L.jo" "Wheel1_L_parentConstraint1.cjo";
connectAttr "FKXWheel1_L.t" "Wheel1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel1_L.rp" "Wheel1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel1_L.rpt" "Wheel1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel1_L.r" "Wheel1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel1_L.ro" "Wheel1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel1_L.s" "Wheel1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel1_L.pm" "Wheel1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel1_L.jo" "Wheel1_L_parentConstraint1.tg[0].tjo";
connectAttr "Wheel1_L_parentConstraint1.w0" "Wheel1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel2_L.s" "Wheel2_L.s";
connectAttr "TrackBottom_L.s" "Wheel2_L.is";
connectAttr "Wheel2_L_parentConstraint1.ctx" "Wheel2_L.tx";
connectAttr "Wheel2_L_parentConstraint1.cty" "Wheel2_L.ty";
connectAttr "Wheel2_L_parentConstraint1.ctz" "Wheel2_L.tz";
connectAttr "Wheel2_L_parentConstraint1.crx" "Wheel2_L.rx";
connectAttr "Wheel2_L_parentConstraint1.cry" "Wheel2_L.ry";
connectAttr "Wheel2_L_parentConstraint1.crz" "Wheel2_L.rz";
connectAttr "jointLayer.di" "Wheel2_L.do";
connectAttr "Wheel2_L.s" "Wheel2_End_L.is";
connectAttr "jointLayer.di" "Wheel2_End_L.do";
connectAttr "Wheel2_L.ro" "Wheel2_L_parentConstraint1.cro";
connectAttr "Wheel2_L.pim" "Wheel2_L_parentConstraint1.cpim";
connectAttr "Wheel2_L.rp" "Wheel2_L_parentConstraint1.crp";
connectAttr "Wheel2_L.rpt" "Wheel2_L_parentConstraint1.crt";
connectAttr "Wheel2_L.jo" "Wheel2_L_parentConstraint1.cjo";
connectAttr "FKXWheel2_L.t" "Wheel2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel2_L.rp" "Wheel2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel2_L.rpt" "Wheel2_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel2_L.r" "Wheel2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel2_L.ro" "Wheel2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel2_L.s" "Wheel2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel2_L.pm" "Wheel2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel2_L.jo" "Wheel2_L_parentConstraint1.tg[0].tjo";
connectAttr "Wheel2_L_parentConstraint1.w0" "Wheel2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel3_L.s" "Wheel3_L.s";
connectAttr "TrackBottom_L.s" "Wheel3_L.is";
connectAttr "Wheel3_L_parentConstraint1.ctx" "Wheel3_L.tx";
connectAttr "Wheel3_L_parentConstraint1.cty" "Wheel3_L.ty";
connectAttr "Wheel3_L_parentConstraint1.ctz" "Wheel3_L.tz";
connectAttr "Wheel3_L_parentConstraint1.crx" "Wheel3_L.rx";
connectAttr "Wheel3_L_parentConstraint1.cry" "Wheel3_L.ry";
connectAttr "Wheel3_L_parentConstraint1.crz" "Wheel3_L.rz";
connectAttr "jointLayer.di" "Wheel3_L.do";
connectAttr "Wheel3_L.s" "Wheel3_End_L.is";
connectAttr "jointLayer.di" "Wheel3_End_L.do";
connectAttr "Wheel3_L.ro" "Wheel3_L_parentConstraint1.cro";
connectAttr "Wheel3_L.pim" "Wheel3_L_parentConstraint1.cpim";
connectAttr "Wheel3_L.rp" "Wheel3_L_parentConstraint1.crp";
connectAttr "Wheel3_L.rpt" "Wheel3_L_parentConstraint1.crt";
connectAttr "Wheel3_L.jo" "Wheel3_L_parentConstraint1.cjo";
connectAttr "FKXWheel3_L.t" "Wheel3_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel3_L.rp" "Wheel3_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel3_L.rpt" "Wheel3_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel3_L.r" "Wheel3_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel3_L.ro" "Wheel3_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel3_L.s" "Wheel3_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel3_L.pm" "Wheel3_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel3_L.jo" "Wheel3_L_parentConstraint1.tg[0].tjo";
connectAttr "Wheel3_L_parentConstraint1.w0" "Wheel3_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel4_L.s" "Wheel4_L.s";
connectAttr "TrackBottom_L.s" "Wheel4_L.is";
connectAttr "Wheel4_L_parentConstraint1.ctx" "Wheel4_L.tx";
connectAttr "Wheel4_L_parentConstraint1.cty" "Wheel4_L.ty";
connectAttr "Wheel4_L_parentConstraint1.ctz" "Wheel4_L.tz";
connectAttr "Wheel4_L_parentConstraint1.crx" "Wheel4_L.rx";
connectAttr "Wheel4_L_parentConstraint1.cry" "Wheel4_L.ry";
connectAttr "Wheel4_L_parentConstraint1.crz" "Wheel4_L.rz";
connectAttr "jointLayer.di" "Wheel4_L.do";
connectAttr "Wheel4_L.s" "Wheel4_End_L.is";
connectAttr "jointLayer.di" "Wheel4_End_L.do";
connectAttr "Wheel4_L.ro" "Wheel4_L_parentConstraint1.cro";
connectAttr "Wheel4_L.pim" "Wheel4_L_parentConstraint1.cpim";
connectAttr "Wheel4_L.rp" "Wheel4_L_parentConstraint1.crp";
connectAttr "Wheel4_L.rpt" "Wheel4_L_parentConstraint1.crt";
connectAttr "Wheel4_L.jo" "Wheel4_L_parentConstraint1.cjo";
connectAttr "FKXWheel4_L.t" "Wheel4_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel4_L.rp" "Wheel4_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel4_L.rpt" "Wheel4_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel4_L.r" "Wheel4_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel4_L.ro" "Wheel4_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel4_L.s" "Wheel4_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel4_L.pm" "Wheel4_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel4_L.jo" "Wheel4_L_parentConstraint1.tg[0].tjo";
connectAttr "Wheel4_L_parentConstraint1.w0" "Wheel4_L_parentConstraint1.tg[0].tw"
		;
connectAttr "TrackBottom_L.s" "FrontTrack_End_L.is";
connectAttr "jointLayer.di" "FrontTrack_End_L.do";
connectAttr "TrackBottom_L.ro" "TrackBottom_L_parentConstraint1.cro";
connectAttr "TrackBottom_L.pim" "TrackBottom_L_parentConstraint1.cpim";
connectAttr "TrackBottom_L.rp" "TrackBottom_L_parentConstraint1.crp";
connectAttr "TrackBottom_L.rpt" "TrackBottom_L_parentConstraint1.crt";
connectAttr "TrackBottom_L.jo" "TrackBottom_L_parentConstraint1.cjo";
connectAttr "FKXTrackBottom_L.t" "TrackBottom_L_parentConstraint1.tg[0].tt";
connectAttr "FKXTrackBottom_L.rp" "TrackBottom_L_parentConstraint1.tg[0].trp";
connectAttr "FKXTrackBottom_L.rpt" "TrackBottom_L_parentConstraint1.tg[0].trt";
connectAttr "FKXTrackBottom_L.r" "TrackBottom_L_parentConstraint1.tg[0].tr";
connectAttr "FKXTrackBottom_L.ro" "TrackBottom_L_parentConstraint1.tg[0].tro";
connectAttr "FKXTrackBottom_L.s" "TrackBottom_L_parentConstraint1.tg[0].ts";
connectAttr "FKXTrackBottom_L.pm" "TrackBottom_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrackBottom_L.jo" "TrackBottom_L_parentConstraint1.tg[0].tjo";
connectAttr "TrackBottom_L_parentConstraint1.w0" "TrackBottom_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBottom_L.t" "TrackBottom_L_parentConstraint1.tg[1].tt";
connectAttr "IKXTrackBottom_L.rp" "TrackBottom_L_parentConstraint1.tg[1].trp";
connectAttr "IKXTrackBottom_L.rpt" "TrackBottom_L_parentConstraint1.tg[1].trt";
connectAttr "IKXTrackBottom_L.r" "TrackBottom_L_parentConstraint1.tg[1].tr";
connectAttr "IKXTrackBottom_L.ro" "TrackBottom_L_parentConstraint1.tg[1].tro";
connectAttr "IKXTrackBottom_L.s" "TrackBottom_L_parentConstraint1.tg[1].ts";
connectAttr "IKXTrackBottom_L.pm" "TrackBottom_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrackBottom_L.jo" "TrackBottom_L_parentConstraint1.tg[1].tjo";
connectAttr "TrackBottom_L_parentConstraint1.w1" "TrackBottom_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendLegReverse_L.ox" "TrackBottom_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "TrackBottom_L_parentConstraint1.w1"
		;
connectAttr "TrackBend_L.ro" "TrackBend_L_parentConstraint1.cro";
connectAttr "TrackBend_L.pim" "TrackBend_L_parentConstraint1.cpim";
connectAttr "TrackBend_L.rp" "TrackBend_L_parentConstraint1.crp";
connectAttr "TrackBend_L.rpt" "TrackBend_L_parentConstraint1.crt";
connectAttr "TrackBend_L.jo" "TrackBend_L_parentConstraint1.cjo";
connectAttr "FKXTrackBend_L.t" "TrackBend_L_parentConstraint1.tg[0].tt";
connectAttr "FKXTrackBend_L.rp" "TrackBend_L_parentConstraint1.tg[0].trp";
connectAttr "FKXTrackBend_L.rpt" "TrackBend_L_parentConstraint1.tg[0].trt";
connectAttr "FKXTrackBend_L.r" "TrackBend_L_parentConstraint1.tg[0].tr";
connectAttr "FKXTrackBend_L.ro" "TrackBend_L_parentConstraint1.tg[0].tro";
connectAttr "FKXTrackBend_L.s" "TrackBend_L_parentConstraint1.tg[0].ts";
connectAttr "FKXTrackBend_L.pm" "TrackBend_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrackBend_L.jo" "TrackBend_L_parentConstraint1.tg[0].tjo";
connectAttr "TrackBend_L_parentConstraint1.w0" "TrackBend_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBend_L.t" "TrackBend_L_parentConstraint1.tg[1].tt";
connectAttr "IKXTrackBend_L.rp" "TrackBend_L_parentConstraint1.tg[1].trp";
connectAttr "IKXTrackBend_L.rpt" "TrackBend_L_parentConstraint1.tg[1].trt";
connectAttr "IKXTrackBend_L.r" "TrackBend_L_parentConstraint1.tg[1].tr";
connectAttr "IKXTrackBend_L.ro" "TrackBend_L_parentConstraint1.tg[1].tro";
connectAttr "IKXTrackBend_L.s" "TrackBend_L_parentConstraint1.tg[1].ts";
connectAttr "IKXTrackBend_L.pm" "TrackBend_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrackBend_L.jo" "TrackBend_L_parentConstraint1.tg[1].tjo";
connectAttr "TrackBend_L_parentConstraint1.w1" "TrackBend_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendLegReverse_L.ox" "TrackBend_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "TrackBend_L_parentConstraint1.w1";
connectAttr "Track_L.ro" "Track_L_parentConstraint1.cro";
connectAttr "Track_L.pim" "Track_L_parentConstraint1.cpim";
connectAttr "Track_L.rp" "Track_L_parentConstraint1.crp";
connectAttr "Track_L.rpt" "Track_L_parentConstraint1.crt";
connectAttr "Track_L.jo" "Track_L_parentConstraint1.cjo";
connectAttr "FKXTrack_L.t" "Track_L_parentConstraint1.tg[0].tt";
connectAttr "FKXTrack_L.rp" "Track_L_parentConstraint1.tg[0].trp";
connectAttr "FKXTrack_L.rpt" "Track_L_parentConstraint1.tg[0].trt";
connectAttr "FKXTrack_L.r" "Track_L_parentConstraint1.tg[0].tr";
connectAttr "FKXTrack_L.ro" "Track_L_parentConstraint1.tg[0].tro";
connectAttr "FKXTrack_L.s" "Track_L_parentConstraint1.tg[0].ts";
connectAttr "FKXTrack_L.pm" "Track_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrack_L.jo" "Track_L_parentConstraint1.tg[0].tjo";
connectAttr "Track_L_parentConstraint1.w0" "Track_L_parentConstraint1.tg[0].tw";
connectAttr "IKXTrack_L.t" "Track_L_parentConstraint1.tg[1].tt";
connectAttr "IKXTrack_L.rp" "Track_L_parentConstraint1.tg[1].trp";
connectAttr "IKXTrack_L.rpt" "Track_L_parentConstraint1.tg[1].trt";
connectAttr "IKXTrack_L.r" "Track_L_parentConstraint1.tg[1].tr";
connectAttr "IKXTrack_L.ro" "Track_L_parentConstraint1.tg[1].tro";
connectAttr "IKXTrack_L.s" "Track_L_parentConstraint1.tg[1].ts";
connectAttr "IKXTrack_L.pm" "Track_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrack_L.jo" "Track_L_parentConstraint1.tg[1].tjo";
connectAttr "Track_L_parentConstraint1.w1" "Track_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Track_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Track_L_parentConstraint1.w1";
connectAttr "Pelvis_M.pim" "Pelvis_M_pointConstraint1.cpim";
connectAttr "Pelvis_M.rp" "Pelvis_M_pointConstraint1.crp";
connectAttr "Pelvis_M.rpt" "Pelvis_M_pointConstraint1.crt";
connectAttr "FKXPelvis_M.t" "Pelvis_M_pointConstraint1.tg[0].tt";
connectAttr "FKXPelvis_M.rp" "Pelvis_M_pointConstraint1.tg[0].trp";
connectAttr "FKXPelvis_M.rpt" "Pelvis_M_pointConstraint1.tg[0].trt";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_pointConstraint1.tg[0].tpm";
connectAttr "Pelvis_M_pointConstraint1.w0" "Pelvis_M_pointConstraint1.tg[0].tw";
connectAttr "Pelvis_M.ro" "Pelvis_M_orientConstraint1.cro";
connectAttr "Pelvis_M.pim" "Pelvis_M_orientConstraint1.cpim";
connectAttr "Pelvis_M.jo" "Pelvis_M_orientConstraint1.cjo";
connectAttr "FKXPelvis_M.r" "Pelvis_M_orientConstraint1.tg[0].tr";
connectAttr "FKXPelvis_M.ro" "Pelvis_M_orientConstraint1.tg[0].tro";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXPelvis_M.jo" "Pelvis_M_orientConstraint1.tg[0].tjo";
connectAttr "Pelvis_M_orientConstraint1.w0" "Pelvis_M_orientConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "irisPuppetSG.msg" "materialInfo3.sg";
connectAttr "BPRig_irisPuppetSG.msg" "BPRig_materialInfo3.sg";
connectAttr "jointVisReverse.ox" "jointLayer.lod";
connectAttr "Main.jointVis" "jointVisReverse.ix";
connectAttr "Main.iog" "ControlSet.dsm" -na;
connectAttr "FKHead_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHead_M.iog" "ControlSet.dsm" -na;
connectAttr "FKWrist1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWrist1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow5_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow5_R.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder_R.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKPinkyFinger3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraPinkyFinger3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKPinkyFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraPinkyFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKPinkyFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraPinkyFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWrist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWrist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHose1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHose1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHose2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHose2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKBody_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraBody_M.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel3_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel3_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel4_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel4_R.iog" "ControlSet.dsm" -na;
connectAttr "FKTrackBottom_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrackBottom_R.iog" "ControlSet.dsm" -na;
connectAttr "FKTrackBend_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrackBend_R.iog" "ControlSet.dsm" -na;
connectAttr "FKTrack_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrack_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHose1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHose1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHose2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHose2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel4_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel4_L.iog" "ControlSet.dsm" -na;
connectAttr "FKTrackBottom_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrackBottom_L.iog" "ControlSet.dsm" -na;
connectAttr "FKTrackBend_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrackBend_L.iog" "ControlSet.dsm" -na;
connectAttr "FKTrack_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrack_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "ControlSet.dsm" -na;
connectAttr "Center_M.iog" "ControlSet.dsm" -na;
connectAttr "Head_M.iog" "GameSet.dsm" -na;
connectAttr "Wrist1_R.iog" "GameSet.dsm" -na;
connectAttr "Elbow5_R.iog" "GameSet.dsm" -na;
connectAttr "Shoulder_R.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger3_L.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger3_L.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger3_L.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "PinkyFinger3_L.iog" "GameSet.dsm" -na;
connectAttr "PinkyFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "PinkyFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "Wrist_L.iog" "GameSet.dsm" -na;
connectAttr "Elbow_L.iog" "GameSet.dsm" -na;
connectAttr "Shoulder1_L.iog" "GameSet.dsm" -na;
connectAttr "Hose1_R.iog" "GameSet.dsm" -na;
connectAttr "Hose2_R.iog" "GameSet.dsm" -na;
connectAttr "Body_M.iog" "GameSet.dsm" -na;
connectAttr "Wheel1_R.iog" "GameSet.dsm" -na;
connectAttr "Wheel2_R.iog" "GameSet.dsm" -na;
connectAttr "Wheel3_R.iog" "GameSet.dsm" -na;
connectAttr "Wheel4_R.iog" "GameSet.dsm" -na;
connectAttr "TrackBottom_R.iog" "GameSet.dsm" -na;
connectAttr "TrackBend_R.iog" "GameSet.dsm" -na;
connectAttr "Track_R.iog" "GameSet.dsm" -na;
connectAttr "Pelvis_M.iog" "GameSet.dsm" -na;
connectAttr "Hose1_L.iog" "GameSet.dsm" -na;
connectAttr "Hose2_L.iog" "GameSet.dsm" -na;
connectAttr "Wheel1_L.iog" "GameSet.dsm" -na;
connectAttr "Wheel2_L.iog" "GameSet.dsm" -na;
connectAttr "Wheel3_L.iog" "GameSet.dsm" -na;
connectAttr "Wheel4_L.iog" "GameSet.dsm" -na;
connectAttr "TrackBottom_L.iog" "GameSet.dsm" -na;
connectAttr "TrackBend_L.iog" "GameSet.dsm" -na;
connectAttr "Track_L.iog" "GameSet.dsm" -na;
connectAttr "buildPose.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "CenterBtwFeet_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrack_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrackBend_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrackBottom_L.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion6.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion5.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "Leg_LAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion4.msg" "AllSet.dnsm" -na;
connectAttr "GlobalShoulder1_reverse_L.msg" "AllSet.dnsm" -na;
connectAttr "GlobalShoulder1_unitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "GlobalShoulder_reverse_R.msg" "AllSet.dnsm" -na;
connectAttr "GlobalShoulder_unitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrack_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrackBend_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrackBottom_R.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion3.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion2.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "Leg_RAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion1.msg" "AllSet.dnsm" -na;
connectAttr "GameSet.msg" "AllSet.dnsm" -na;
connectAttr "ControlSet.msg" "AllSet.dnsm" -na;
connectAttr "jointVisReverse.msg" "AllSet.dnsm" -na;
connectAttr "jointLayer.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsOffset_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenter_M.iog" "AllSet.dsm" -na;
connectAttr "Center_MShape.iog" "AllSet.dsm" -na;
connectAttr "Center_M.iog" "AllSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "AllSet.dsm" -na;
connectAttr "CenterOffset_M.iog" "AllSet.dsm" -na;
connectAttr "Track_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TrackBend_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TrackBottom_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.iog" "AllSet.dsm"
		 -na;
connectAttr "Wheel1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hose2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hose1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_L.iog" "AllSet.dsm" -na;
connectAttr "effector4.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_LStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBottom_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L.iog" "AllSet.dsm" -na;
connectAttr "effector3.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalShoulder1_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "GlobalShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "GlobalOffsetShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalShoulder_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "GlobalShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "GlobalOffsetShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Track_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TrackBend_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TrackBottom_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.iog" "AllSet.dsm"
		 -na;
connectAttr "Wheel1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Body_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hose2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hose1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wrist_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PinkyFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PinkyFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PinkyFinger3_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger3_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger3_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger3_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow5_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wrist1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Head_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_R.iog" "AllSet.dsm" -na;
connectAttr "effector2.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_RStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBottom_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R.iog" "AllSet.dsm" -na;
connectAttr "effector1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKXTrack_L.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetTrack_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintTrack_L.iog" "AllSet.dsm" -na;
connectAttr "FKXTrack_L.iog" "AllSet.dsm" -na;
connectAttr "FKTrack_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrack_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrack_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrack_L.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "FKXTrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBend_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKXTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBottom_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKXBackTrack_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKXFrontTrack_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXFrontTrack_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel4_L.iog" "AllSet.dsm" -na;
connectAttr "FKWheel4_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel4_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel4_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel4_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel4_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel3_L.iog" "AllSet.dsm" -na;
connectAttr "FKWheel3_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel3_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel3_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel3_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel3_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel2_L.iog" "AllSet.dsm" -na;
connectAttr "FKWheel2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel2_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel1_L.iog" "AllSet.dsm" -na;
connectAttr "FKWheel1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel1_L.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel1_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHose2_L.iog" "AllSet.dsm" -na;
connectAttr "FKHose2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHose2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHose2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHose2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHose2_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHose1_L.iog" "AllSet.dsm" -na;
connectAttr "FKHose1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHose1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHose1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHose1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHose1_End_L.iog" "AllSet.dsm" -na;
connectAttr "Track_L.iog" "AllSet.dsm" -na;
connectAttr "TrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "TrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FrontTrack_End_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_End_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_End_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_End_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel1_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel1_End_L.iog" "AllSet.dsm" -na;
connectAttr "Hose2_L.iog" "AllSet.dsm" -na;
connectAttr "Hose2_End_L.iog" "AllSet.dsm" -na;
connectAttr "Hose1_L.iog" "AllSet.dsm" -na;
connectAttr "Hose1_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKXPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKXTrack_R.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetTrack_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintTrack_R.iog" "AllSet.dsm" -na;
connectAttr "FKXTrack_R.iog" "AllSet.dsm" -na;
connectAttr "FKTrack_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrack_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrack_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrack_R.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "FKXTrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBend_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKXTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBottom_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKXBackTrack_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXFrontTrack_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXFrontTrack_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel4_R.iog" "AllSet.dsm" -na;
connectAttr "FKWheel4_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel4_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel4_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel4_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel4_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKWheel3_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel3_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKWheel2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel2_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKWheel1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel1_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXBody_M.iog" "AllSet.dsm" -na;
connectAttr "FKBody_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKBody_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraBody_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetBody_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHose2_R.iog" "AllSet.dsm" -na;
connectAttr "FKHose2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHose2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHose2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHose2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHose2_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHose1_R.iog" "AllSet.dsm" -na;
connectAttr "FKHose1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHose1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHose1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHose1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHose1_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalStaticShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKWrist_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKXPinkyFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKPinkyFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKPinkyFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPinkyFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPinkyFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXPinkyFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKPinkyFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKPinkyFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPinkyFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPinkyFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXPinkyFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKPinkyFinger3_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKPinkyFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPinkyFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPinkyFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKXPinkyFinger4_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger3_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger4_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger3_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger4_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger3_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger4_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalStaticShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow5_R.iog" "AllSet.dsm" -na;
connectAttr "FKElbow5_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow5_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow5_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow5_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "FKWrist1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWrist1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow4_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKHead_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHead_End_M.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M.iog" "AllSet.dsm" -na;
connectAttr "Track_R.iog" "AllSet.dsm" -na;
connectAttr "TrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "TrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FrontTrack_End_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_End_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_End_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_End_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel1_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel1_End_R.iog" "AllSet.dsm" -na;
connectAttr "Body_M.iog" "AllSet.dsm" -na;
connectAttr "Hose2_R.iog" "AllSet.dsm" -na;
connectAttr "Hose2_End_R.iog" "AllSet.dsm" -na;
connectAttr "Hose1_R.iog" "AllSet.dsm" -na;
connectAttr "Hose1_End_R.iog" "AllSet.dsm" -na;
connectAttr "Shoulder1_L.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L.iog" "AllSet.dsm" -na;
connectAttr "Wrist_L.iog" "AllSet.dsm" -na;
connectAttr "PinkyFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "PinkyFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "PinkyFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "PinkyFinger4_End_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger4_End_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger4_End_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger3_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger4_End_L.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_R.iog" "AllSet.dsm" -na;
connectAttr "Elbow5_R.iog" "AllSet.dsm" -na;
connectAttr "Wrist1_R.iog" "AllSet.dsm" -na;
connectAttr "Elbow4_R.iog" "AllSet.dsm" -na;
connectAttr "Head_M.iog" "AllSet.dsm" -na;
connectAttr "Head_End_M.iog" "AllSet.dsm" -na;
connectAttr "IKMessure.iog" "AllSet.dsm" -na;
connectAttr "IKCrv.iog" "AllSet.dsm" -na;
connectAttr "IKStatic.iog" "AllSet.dsm" -na;
connectAttr "IKHandle.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraint.iog" "AllSet.dsm" -na;
connectAttr "GlobalSystem.iog" "AllSet.dsm" -na;
connectAttr "RootSystem.iog" "AllSet.dsm" -na;
connectAttr "FKIKSystem.iog" "AllSet.dsm" -na;
connectAttr "IKSystem.iog" "AllSet.dsm" -na;
connectAttr "FKSystem.iog" "AllSet.dsm" -na;
connectAttr "GameSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MotionSystem.iog" "AllSet.dsm" -na;
connectAttr "BaseSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MainShape.iog" "AllSet.dsm" -na;
connectAttr "Main.iog" "AllSet.dsm" -na;
connectAttr "forwardArrowShape.iog" "AllSet.dsm" -na;
connectAttr "GameSet.msg" "Sets.dnsm" -na;
connectAttr "ControlSet.msg" "Sets.dnsm" -na;
connectAttr "AllSet.msg" "Sets.dnsm" -na;
connectAttr "IKLeg_R.swivel" "unitConversion1.i";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vx";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vy";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegUnitConversion_R.i";
connectAttr "FKIKBlendLegUnitConversion_R.o" "FKIKBlendLegReverse_R.ix";
connectAttr "FKIKLeg_R.autoVis" "FKIKBlendLegCondition_R.ft";
connectAttr "FKIKLeg_R.IKVis" "FKIKBlendLegCondition_R.ctr";
connectAttr "FKIKLeg_R.FKVis" "FKIKBlendLegCondition_R.ctg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegCondition_R.cfr";
connectAttr "FKIKBlendLegsetRange_R.ox" "FKIKBlendLegCondition_R.cfg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegsetRange_R.vx";
connectAttr "IKLeg_R.rollAngle" "Leg_RAngleReverse.i1x";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vx";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vy";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vz";
connectAttr "Leg_RAngleReverse.ox" "IKRollAngleLeg_R.nx";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.my";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.mz";
connectAttr "IKRollAngleLeg_R.ox" "unitConversion2.i";
connectAttr "IKRollAngleLeg_R.oy" "unitConversion3.i";
connectAttr "FKTrackBottom_R.s" "ScaleBlendTrackBottom_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendTrackBottom_R.b";
connectAttr "FKTrackBend_R.s" "ScaleBlendTrackBend_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendTrackBend_R.b";
connectAttr "FKTrack_R.s" "ScaleBlendTrack_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendTrack_R.b";
connectAttr "FKShoulder_R.Global" "GlobalShoulder_unitConversion_R.i";
connectAttr "GlobalShoulder_unitConversion_R.o" "GlobalShoulder_reverse_R.ix";
connectAttr "FKShoulder1_L.Global" "GlobalShoulder1_unitConversion_L.i";
connectAttr "GlobalShoulder1_unitConversion_L.o" "GlobalShoulder1_reverse_L.ix";
connectAttr "IKLeg_L.swivel" "unitConversion4.i";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vx";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vy";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegUnitConversion_L.i";
connectAttr "FKIKBlendLegUnitConversion_L.o" "FKIKBlendLegReverse_L.ix";
connectAttr "FKIKLeg_L.autoVis" "FKIKBlendLegCondition_L.ft";
connectAttr "FKIKLeg_L.IKVis" "FKIKBlendLegCondition_L.ctr";
connectAttr "FKIKLeg_L.FKVis" "FKIKBlendLegCondition_L.ctg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegCondition_L.cfr";
connectAttr "FKIKBlendLegsetRange_L.ox" "FKIKBlendLegCondition_L.cfg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegsetRange_L.vx";
connectAttr "IKLeg_L.rollAngle" "Leg_LAngleReverse.i1x";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vx";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vy";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vz";
connectAttr "Leg_LAngleReverse.ox" "IKRollAngleLeg_L.nx";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.my";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.mz";
connectAttr "IKRollAngleLeg_L.ox" "unitConversion5.i";
connectAttr "IKRollAngleLeg_L.oy" "unitConversion6.i";
connectAttr "FKTrackBottom_L.s" "ScaleBlendTrackBottom_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendTrackBottom_L.b";
connectAttr "FKTrackBend_L.s" "ScaleBlendTrackBend_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendTrackBend_L.b";
connectAttr "FKTrack_L.s" "ScaleBlendTrack_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendTrack_L.b";
connectAttr "Center_M.CenterBtwFeet" "CenterBtwFeet_M.vx";
connectAttr "Center_M.CenterBtwFeet" "PelvisCenterBtwFeetUnitConversion_M.i";
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwFeetReverse_M.ix"
		;
connectAttr "irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "BPRig_irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikSCsolver.msg" ":ikSystem.sol" -na;
// End of Medic__rig.ma
