//Maya ASCII 2013 scene
//Name: Juggernaut__rig.ma
//Last modified: Thu, Apr 24, 2014 08:05:56 AM
//Codeset: UTF-8
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.20596150122198109 10.021329225451366 36.919900178496334 ;
	setAttr ".r" -type "double3" -7.5383527269728035 -17.400000000103546 4.1663438790240073e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 35.815020484616575;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -7.4513068437371901 4.5166884175414328 3.8654754616880154 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -4.1361804461634772 100.1 -1.3734464006168321 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 27.29039406909008;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -3.7419607576216833 10.632283737889354 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 3.1310390857996988;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 6.4794975166716737 -1.0195760413273467 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 11.1997179230661;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "ctrl_rig";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Main" -p "ctrl_rig";
	addAttr -ci true -sn "height" -ln "height" -at "double";
	addAttr -ci true -sn "fkVis" -ln "fkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "ikVis" -ln "ikVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fkIkVis" -ln "fkIkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimVis" -ln "aimVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "eyeVis" -ln "eyeVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fingerVis" -ln "fingerVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "bendVis" -ln "bendVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "arrowVis" -ln "arrowVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "jointVis" -ln "jointVis" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -l on ".height" 10.648452901842983;
	setAttr -cb on ".fkVis";
	setAttr -cb on ".ikVis";
	setAttr -cb on ".fkIkVis";
	setAttr -cb on ".aimVis";
	setAttr -cb on ".eyeVis";
	setAttr -cb on ".fingerVis";
	setAttr -cb on ".bendVis";
	setAttr -cb on ".arrowVis";
	setAttr -cb on ".jointVis";
createNode nurbsCurve -n "MainShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		3.644282767 -0.083987242239999996 -3.6525860790000002
		1.4492270940000001e-15 -0.083987242239999996 -5.162097427
		-3.644282767 -0.083987242239999996 -3.6525860790000002
		-5.1537941150000002 -0.083987242239999996 -0.0083033118089999997
		-3.644282767 -0.083987242239999996 3.6359794550000002
		4.8427462940000002e-16 -0.083987242239999996 5.1454908030000004
		3.644282767 -0.083987242239999996 3.6359794550000002
		5.1537941150000002 -0.083987242239999996 -0.0083033118089999997
		3.644282767 -0.083987242239999996 -3.6525860790000002
		1.4492270940000001e-15 -0.083987242239999996 -5.162097427
		-3.644282767 -0.083987242239999996 -3.6525860790000002
		;
createNode transform -n "BaseSkeleton" -p "Main";
	setAttr -l on ".v" no;
createNode transform -n "BaseSkeletonOffset" -p "BaseSkeleton";
createNode joint -n "Pelvis" -p "BaseSkeletonOffset";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 5.9604644775390625e-08 5.2923843071224637 -1.2424887120723724 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 29.999999999999996 0 0 ;
	setAttr ".dl" yes;
	setAttr ".typ" 1;
createNode joint -n "SpineA" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0 1.6020692693748249 -0.068936305360121963 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -29.999999999999996 0 0 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Mid";
createNode joint -n "Chest" -p "SpineA";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.0073184967041016e-05 2.4826371914940086 0.030091693813034004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Chest";
createNode joint -n "Clavicle" -p "Chest";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.9987797908081433 1.4515305646454788 -0.23751408606229596 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000108608329285 89.81681367376099 167.41013204046138 ;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "Shoulder" -p "Clavicle";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.67425712140663374 1.8524702132379485 -0.80961543556772853 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -59.115149396029693 5.8645739860458379 2.340423728907937 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Shoulder";
createNode joint -n "Elbow" -p "Shoulder";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.049680678251219475 4.4542548449164396 -0.25171649050149902 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.6128851661730681 6.3110378293342642 84.200711274501501 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "Wrist" -p "Elbow";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.38546841457728698 1.3131720551496151 -0.12401197395647046 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.3388154323328214 -13.483344189669324 -8.7888035025143108 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Hand";
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "MiddleFinger1" -p "Wrist";
	setAttr ".t" -type "double3" 1.2140707715363643 2.612647685116714 -0.55558593361305419 ;
	setAttr ".r" -type "double3" 51.939018039983843 228.41338174810866 27.707935288204453 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -34.462082586865911 -8.7285733235282201 -1.7903981777634761 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "4";
createNode joint -n "MiddleFinger2" -p "MiddleFinger1";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0.99106178888130891 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" 9.4990663309175272 1.0448838425235487 58.190624037770185 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "3";
createNode joint -n "MiddleFinger3_End" -p "MiddleFinger2";
	setAttr ".t" -type "double3" 0 1.4053515974398842 -1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 7.8877756100295234e-13 -8.5874976399514975e-14 1.4630551534732179e-13 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "3";
createNode joint -n "IndexFinger1" -p "Wrist";
	setAttr ".t" -type "double3" 0.54621867886444853 2.7196885306359215 -0.92085001407918909 ;
	setAttr ".r" -type "double3" 173.83122175214328 -48.702070459132784 164.82754022296552 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0.065532877363568762 20.527688987272207 -2.5422327562497964 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "8";
createNode joint -n "IndexFinger2" -p "IndexFinger1";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0.92790311814927762 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 1.3707126995591168 -7.0081534559372689e-13 61.892678772049649 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "7";
createNode joint -n "IndexFinger3_End" -p "IndexFinger2";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 1.4990935335754476 -1.5987211554602254e-14 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.2876721579330973e-13 -4.7628806354916196e-13 -6.3611093629270361e-14 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "7";
createNode joint -n "ThumbFinger1" -p "Wrist";
	setAttr ".t" -type "double3" 0.0024459017964764129 2.2723833325074554 -1.6528888569724789 ;
	setAttr ".r" -type "double3" 46.30060972453655 201.2291656180592 23.018105103460396 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "ThumbFinger2" -p "ThumbFinger1";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0.79591438041152085 0 ;
	setAttr ".r" -type "double3" 1.596865564912328 -2.9071739957667018 61.204051514185871 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "ThumbFinger3_End" -p "ThumbFinger2";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 1.3935263360622543 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.8484711645708551e-13 -9.2236085762441977e-14 -1.3040274194000422e-12 ;
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "Chest_End" -p "Chest";
	setAttr ".t" -type "double3" -3.3881317890172021e-21 1.3032944042756736 1.0309757934324235 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
createNode joint -n "ShoulderPad1" -p "Chest";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.8142628627476769 0.28608493210809449 -0.28779146322840943 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 77.73341118625774 89.816813673758887 167.41013204061267 ;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "ShoulderPad1_End" -p "ShoulderPad1";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 5.6288307348495437e-14 1.5513811389331269 1.9539925233402755e-14 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "ShoulderPad2" -p "Chest";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.707816068015007 -0.067167164694188486 -0.47936911453115766 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 77.733411186327274 89.816813673757849 167.41013204068079 ;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "ShoulderPad2_End" -p "ShoulderPad2";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 8.5598195198599569e-14 1.5303351113900441 4.4408920985006262e-14 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".otp" -type "string" "PropA1";
createNode joint -n "HipTwist" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.2119851708412173 0.050251571453561183 -0.029227975692607263 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 235.13957903290628 89.999999999976268 ;
createNode joint -n "Hip" -p "HipTwist";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.088558954171692683 1.1113775190156785 -0.041558800164646525 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.72356917514651575 1.0610891793703126 -90.162074787503371 ;
	setAttr ".dl" yes;
	setAttr ".typ" 2;
createNode joint -n "Knee" -p "Hip";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.032864012269020026 2.5867190732225191 0.19800989497251908 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 22.919762006828137 0 0 ;
createNode joint -n "Ankle" -p "Knee";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.036411222619632255 0.88064448175187948 0.022028406801983924 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.4900163786870586 -0.0077691772226815923 0.59745913041447196 ;
	setAttr ".pa" -type "double3" 3.1147589914174403 -1.2104724556304991 -11.405913270501992 ;
	setAttr ".dl" yes;
	setAttr ".typ" 4;
createNode joint -n "Foot_End" -p "Ankle";
	setAttr ".t" -type "double3" 0.06628272549730152 2.1170624371978759 -2.6287438008809714 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 71.642384016319596 179.10051431432228 2.2965204288393681e-05 ;
	setAttr ".pa" -type "double3" -0.00019030234564052423 0.00053514845282692043 25.864574245063647 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Ball";
createNode joint -n "Heel_End" -p "Ankle";
	setAttr ".t" -type "double3" -0.0033414129884676846 1.992840870699784 1.7595106209549125 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 59.095245418304913 179.10046423217725 -1.2722218725854072e-13 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Heel";
createNode transform -n "MotionSystem" -p "Main";
createNode transform -n "FKSystem" -p "MotionSystem";
createNode transform -n "FKParentConstraintToWrist_R" -p "FKSystem";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 1.0000000000000004 ;
createNode joint -n "FKOffsetMiddleFinger1_R" -p "FKParentConstraintToWrist_R";
	setAttr ".t" -type "double3" 1.214070771536365 2.6126476851167144 -0.55558593361305242 ;
	setAttr ".r" -type "double3" 51.939019241392231 228.41338305362552 27.707934490889642 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger1_R" -p "FKOffsetMiddleFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKMiddleFinger1_R" -p "FKExtraMiddleFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger1_RShape" -p "FKMiddleFinger1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.44340467430000002 -0.087030458229999996 -0.50538281500000004
		-0.64998299260000003 -0.087030458229999996 -0.0066586373050000003
		-0.44340467430000002 -0.087030458229999996 0.4920655404
		0.055319503409999998 -0.087030458229999996 0.69864385870000001
		0.55404368110000002 -0.087030458229999996 0.4920655404
		0.76062199939999997 -0.087030458229999996 -0.0066586373050000003
		0.55404368110000002 -0.087030458229999996 -0.50538281500000004
		0.055319503409999998 -0.087030458229999996 -0.71196113329999999
		-0.44340467430000002 -0.087030458229999996 -0.50538281500000004
		-0.64998299260000003 -0.087030458229999996 -0.0066586373050000003
		-0.44340467430000002 -0.087030458229999996 0.4920655404
		;
createNode joint -n "FKXMiddleFinger1_R" -p "FKMiddleFinger1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger2_R" -p "FKXMiddleFinger1_R";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0.99106178888130891 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" 9.49906595140137 1.0448838333324315 58.190623086813204 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger2_R" -p "FKOffsetMiddleFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 7.7715611723760958e-16 -1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKMiddleFinger2_R" -p "FKExtraMiddleFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000009 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger2_RShape" -p "FKMiddleFinger2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.6034296186 -0.01327306875 -0.6034296186
		-0.8533783506 -0.01327306875 0
		-0.6034296186 -0.01327306875 0.6034296186
		-2.4728769620000002e-16 -0.01327306875 0.8533783506
		0.6034296186 -0.01327306875 0.6034296186
		0.8533783506 -0.01327306875 0
		0.6034296186 -0.01327306875 -0.6034296186
		4.5835117680000006e-16 -0.01327306875 -0.8533783506
		-0.6034296186 -0.01327306875 -0.6034296186
		-0.8533783506 -0.01327306875 0
		-0.6034296186 -0.01327306875 0.6034296186
		;
createNode joint -n "FKXMiddleFinger2_R" -p "FKMiddleFinger2_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleFinger3_End_R" -p "FKXMiddleFinger2_R";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 1.4053515974398827 0 ;
	setAttr ".r" -type "double3" -9.9053476941265632 66.284880117077805 156.35490863147388 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 9.9053476941265313 -66.28488011707779 -156.35490863147382 ;
createNode joint -n "FKOffsetIndexFinger1_R" -p "FKParentConstraintToWrist_R";
	setAttr ".t" -type "double3" 0.54621867886444764 2.7196885306359224 -0.92085001407918909 ;
	setAttr ".r" -type "double3" 173.83122403741098 -48.70206965031813 164.82754573009507 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger1_R" -p "FKOffsetIndexFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 -1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "FKIndexFinger1_R" -p "FKExtraIndexFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999933 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger1_RShape" -p "FKIndexFinger1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.47995322820000003 -9.3676013570000002e-17 -0.47995322820000003
		-0.6787563646 -7.8921508920000001e-17 -4.5927139549999997e-16
		-0.47995322820000003 -1.52453332e-16 0.47995322820000003
		-1.9668661329999998e-16 -2.7119753820000001e-16 0.6787563646
		0.47995322820000003 -3.655953819e-16 0.47995322820000003
		0.6787563646 -3.8034988660000001e-16 -4.5927139549999997e-16
		0.47995322820000003 -3.068180634e-16 -0.47995322820000003
		3.645613675e-16 -1.8807385729999998e-16 -0.6787563646
		-0.47995322820000003 -9.3676013570000002e-17 -0.47995322820000003
		-0.6787563646 -7.8921508920000001e-17 -4.5927139549999997e-16
		-0.47995322820000003 -1.52453332e-16 0.47995322820000003
		;
createNode joint -n "FKXIndexFinger1_R" -p "FKIndexFinger1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetIndexFinger2_R" -p "FKXIndexFinger1_R";
	setAttr ".t" -type "double3" -2.6645352591003757e-15 0.92790311814927884 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 1.3707126716026008 -7.0081534559372679e-13 61.892681239608493 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger2_R" -p "FKOffsetIndexFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 4.4408920985006262e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "FKIndexFinger2_R" -p "FKExtraIndexFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger2_RShape" -p "FKIndexFinger2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.63046462700000006 1.7859609339999999e-16 -0.63046462700000006
		-0.8916116261 1.9797755130000001e-16 4.1935237770000002e-16
		-0.63046462700000006 1.013864446e-16 0.63046462700000006
		1.609856349e-16 -5.4595466200000004e-17 0.8916116261
		0.63046462700000006 -1.7859609339999999e-16 0.63046462700000006
		0.8916116261 -1.9797755130000001e-16 4.1935237770000002e-16
		0.63046462700000006 -1.013864446e-16 -0.63046462700000006
		8.9823872139999994e-16 5.4595466200000004e-17 -0.8916116261
		-0.63046462700000006 1.7859609339999999e-16 -0.63046462700000006
		-0.8916116261 1.9797755130000001e-16 4.1935237770000002e-16
		-0.63046462700000006 1.013864446e-16 0.63046462700000006
		;
createNode joint -n "FKXIndexFinger2_R" -p "FKIndexFinger2_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXIndexFinger3_End_R" -p "FKXIndexFinger2_R";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 1.4990935335754476 -1.4210854715202004e-14 ;
	setAttr ".r" -type "double3" 173.25812803596759 84.191074302064266 -20.254146458539569 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -173.25812803596742 -84.19107430206428 20.254146458539378 ;
createNode joint -n "FKOffsetThumbFinger1_R" -p "FKParentConstraintToWrist_R";
	setAttr ".t" -type "double3" 0.002445901796476857 2.2723833325074558 -1.6528888569724769 ;
	setAttr ".r" -type "double3" 46.300609289087497 201.22916185508308 23.018104625610864 ;
createNode transform -n "FKExtraThumbFinger1_R" -p "FKOffsetThumbFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 8.8817841970012523e-16 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -1.908332808878111e-14 -1.5902773407317587e-14 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKThumbFinger1_R" -p "FKExtraThumbFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -6.6613381477509392e-16 -8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -1.908332808878111e-14 3.1805546814635183e-15 4.7708320221952783e-15 ;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999956 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger1_RShape" -p "FKThumbFinger1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.44072577280000003 -3.34423945e-16 -0.44072577280000003
		-0.62328036519999996 -3.2087535300000001e-16 -4.5927139549999997e-16
		-0.44072577280000003 -3.8839728569999997e-16 0.44072577280000003
		-6.5793207729999997e-17 -4.9743631069999999e-16 0.62328036519999996
		0.44072577280000003 -5.8411884589999999e-16 0.44072577280000003
		0.62328036519999996 -5.9766743789999998e-16 -4.5927139549999997e-16
		0.44072577280000003 -5.3014550519999997e-16 -0.44072577280000003
		4.4958294889999999e-16 -4.211064803e-16 -0.62328036519999996
		-0.44072577280000003 -3.34423945e-16 -0.44072577280000003
		-0.62328036519999996 -3.2087535300000001e-16 -4.5927139549999997e-16
		-0.44072577280000003 -3.8839728569999997e-16 0.44072577280000003
		;
createNode joint -n "FKXThumbFinger1_R" -p "FKThumbFinger1_R";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetThumbFinger2_R" -p "FKXThumbFinger1_R";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0.79591438041152085 0 ;
	setAttr ".r" -type "double3" 1.5968655711070121 -2.907173970680724 61.204054737298641 ;
createNode transform -n "FKExtraThumbFinger2_R" -p "FKOffsetThumbFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -3.1805546814635176e-15 3.1805546814635176e-15 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKThumbFinger2_R" -p "FKExtraThumbFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -6.361109362927032e-15 3.1805546814635168e-15 7.9513867036587919e-16 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger2_RShape" -p "FKThumbFinger2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.60001923420000003 1.6997161549999999e-16 -0.60001923420000003
		-0.84855533869999999 1.8841713490000001e-16 0
		-0.60001923420000003 9.6490452070000002e-17 0.60001923420000003
		-3.6213919099999998e-17 -5.1959028970000001e-17 0.84855533869999999
		0.60001923420000003 -1.6997161549999999e-16 0.60001923420000003
		0.84855533869999999 -1.8841713490000001e-16 0
		0.60001923420000003 -9.6490452070000002e-17 -0.60001923420000003
		6.6543691660000002e-16 5.1959028970000001e-17 -0.84855533869999999
		-0.60001923420000003 1.6997161549999999e-16 -0.60001923420000003
		-0.84855533869999999 1.8841713490000001e-16 0
		-0.60001923420000003 9.6490452070000002e-17 0.60001923420000003
		;
createNode joint -n "FKXThumbFinger2_R" -p "FKThumbFinger2_R";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXThumbFinger3_End_R" -p "FKXThumbFinger2_R";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 1.393526336062255 0 ;
	setAttr ".r" -type "double3" 156.87610778030756 -16.638252964976669 8.7551914401189102 ;
	setAttr ".jo" -type "double3" -158.4455455454779 -18.664904387689639 1.7448409252680526 ;
createNode parentConstraint -n "FKParentConstraintToWrist_R_parentConstraint1" -p
		 "FKParentConstraintToWrist_R";
	addAttr -ci true -k true -sn "w0" -ln "Wrist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 101.65920055458389 -140.44660172243076 0.12875360800035185 ;
	setAttr ".rst" -type "double3" -6.9193342330668006 6.4581298526202788 0.37777643545795447 ;
	setAttr ".rsrr" -type "double3" 101.65920410119556 -140.44660394352425 0.12875437218204738 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToChest_M" -p "FKSystem";
createNode joint -n "FKOffsetClavicle_R" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" -1.9987797908081433 1.4515305646454788 -0.23751408606229596 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 90.000111787504906 89.816816830938421 167.41013587792526 ;
createNode transform -n "FKExtraClavicle_R" -p "FKOffsetClavicle_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
createNode transform -n "FKClavicle_R" -p "FKExtraClavicle_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKClavicle_RShape" -p "FKClavicle_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.066140528 -0.33743607520000002 -2.0208767249999999
		-2.9904095869999998 -0.33743607520000002 0.21050617120000001
		-2.066140528 -0.33743607520000002 2.4418890680000001
		0.16524236819999999 -0.33743607520000002 3.3661581260000002
		2.3966252649999999 -0.33743607520000002 2.4418890680000001
		3.3208943230000001 -0.33743607520000002 0.21050617120000001
		2.3966252649999999 -0.33743607520000002 -2.0208767249999999
		0.16524236819999999 -0.33743607520000002 -2.9451457840000002
		-2.066140528 -0.33743607520000002 -2.0208767249999999
		-2.9904095869999998 -0.33743607520000002 0.21050617120000001
		-2.066140528 -0.33743607520000002 2.4418890680000001
		;
createNode joint -n "FKXClavicle_R" -p "FKClavicle_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetShoulder_R" -p "FKXClavicle_R";
	setAttr ".t" -type "double3" 0.67425712140663274 1.8524702132379496 -0.80961543556772497 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -59.11515066267696 5.8645741643099614 2.3404236563732836 ;
createNode transform -n "FKExtraShoulder_R" -p "FKOffsetShoulder_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKShoulder_R" -p "FKExtraShoulder_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulder_RShape" -p "FKShoulder_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.892903027 -0.2135040258 -1.9145888069999999
		-2.695124904 -0.2135040258 0.02214612694
		-1.892903027 -0.2135040258 1.958881061
		0.043831906990000002 -0.2135040258 2.7611029380000001
		1.9805668409999999 -0.2135040258 1.958881061
		2.7827887179999999 -0.2135040258 0.02214612694
		1.9805668409999999 -0.2135040258 -1.9145888069999999
		0.043831906990000002 -0.2135040258 -2.7168106839999999
		-1.892903027 -0.2135040258 -1.9145888069999999
		-2.695124904 -0.2135040258 0.02214612694
		-1.892903027 -0.2135040258 1.958881061
		;
createNode joint -n "FKXShoulder_R" -p "FKShoulder_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow_R" -p "FKXShoulder_R";
	setAttr ".t" -type "double3" 0.049680678251219135 4.4542548449164405 -0.25171649050149725 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.6128853374257202 6.3110380171443063 84.200714276462946 ;
createNode transform -n "FKExtraElbow_R" -p "FKOffsetElbow_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 6.6613381477509392e-16 -2.2204460492503131e-16 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKElbow_R" -p "FKExtraElbow_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow_RShape" -p "FKElbow_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.7477946010000001 -0.10007105099999999 -1.705726952
		-2.4585712329999998 -0.10007105099999999 0.01023963303
		-1.7477946010000001 -0.10007105099999999 1.726206218
		-0.031828016149999999 -0.10007105099999999 2.4369828500000001
		1.6841385689999999 -0.10007105099999999 1.726206218
		2.3949152009999999 -0.10007105099999999 0.01023963303
		1.6841385689999999 -0.10007105099999999 -1.705726952
		-0.031828016149999999 -0.10007105099999999 -2.416503584
		-1.7477946010000001 -0.10007105099999999 -1.705726952
		-2.4585712329999998 -0.10007105099999999 0.01023963303
		-1.7477946010000001 -0.10007105099999999 1.726206218
		;
createNode joint -n "FKXElbow_R" -p "FKElbow_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetWrist_R" -p "FKXElbow_R";
	setAttr ".t" -type "double3" 0.38546841457728631 1.3131720551496162 -0.12401197395647046 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -4.3388153447744235 -13.483344105607005 -8.7888031173482055 ;
createNode transform -n "FKExtraWrist_R" -p "FKOffsetWrist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -1.1102230246251563e-16 1.7763568394002505e-15 ;
	setAttr ".ro" 5;
createNode transform -n "FKWrist_R" -p "FKExtraWrist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-15 -1.1102230246251563e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWrist_RShape" -p "FKWrist_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.547027291 3.0713718349999998e-16 -1.547027291
		-2.1878269760000002 3.5469519550000001e-16 0
		-1.547027291 1.176809816e-16 1.547027291
		6.7702228780000004e-16 -2.6506574619999999e-16 2.1878269760000002
		1.547027291 -5.6933714560000003e-16 1.547027291
		2.1878269760000002 -6.1689515750000001e-16 0
		1.547027291 -3.7988094360000002e-16 -1.547027291
		2.4860859469999999e-15 2.8657841270000001e-18 -2.1878269760000002
		-1.547027291 3.0713718349999998e-16 -1.547027291
		-2.1878269760000002 3.5469519550000001e-16 0
		-1.547027291 1.176809816e-16 1.547027291
		;
createNode joint -n "FKXWrist_R" -p "FKWrist_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToWrist_R" -p "FKXWrist_R";
	setAttr ".r" -type "double3" -80.962584001662066 -7.2667622316160942 141.01152930371074 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode joint -n "FKOffsetShoulderPad1_R" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" -1.8142628627476769 0.28608493210809449 -0.28779146322840943 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 77.733413057052971 89.816816830938421 167.41013587792526 ;
createNode transform -n "FKExtraShoulderPad1_R" -p "FKOffsetShoulderPad1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKShoulderPad1_R" -p "FKExtraShoulderPad1_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulderPad1_RShape" -p "FKShoulderPad1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-3.1349288880000001 -0.00067163300200000003 -2.0483980430000002
		-4.434462581 0.01190761305 0.0010036303869999999
		-3.1363482359999999 0.017511540870000002 2.0498173909999999
		-0.0010036303869999999 0.012857445550000001 2.897875924
		3.1349288880000001 0.00067163300200000003 2.0483980430000002
		4.434462581 -0.01190761305 -0.0010036303869999999
		3.1363482359999999 -0.017511540870000002 -2.0498173909999999
		0.0010036303869999999 -0.012857445550000001 -2.897875924
		-3.1349288880000001 -0.00067163300200000003 -2.0483980430000002
		-4.434462581 0.01190761305 0.0010036303869999999
		-3.1363482359999999 0.017511540870000002 2.0498173909999999
		;
createNode joint -n "FKXShoulderPad1_R" -p "FKShoulderPad1_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXShoulderPad1_End_R" -p "FKXShoulderPad1_R";
	setAttr ".t" -type "double3" 9.0927265716800321e-14 1.5513811389331262 5.3290705182007514e-15 ;
	setAttr ".r" -type "double3" -89.674326153546417 83.136399354554342 7.0424949690195549 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 89.961079566698714 -0.32333986796610392 -90.178784502556084 ;
createNode joint -n "FKOffsetShoulderPad2_R" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" -1.707816068015007 -0.067167164694188486 -0.47936911453115766 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 77.733413057052971 89.816816830938421 167.41013587792526 ;
createNode transform -n "FKExtraShoulderPad2_R" -p "FKOffsetShoulderPad2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.1102230246251563e-16 -2.2204460492503131e-16 -1.7763568394002505e-15 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKShoulderPad2_R" -p "FKExtraShoulderPad2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.1102230246251563e-16 0 -1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulderPad2_RShape" -p "FKShoulderPad2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-3.1056662660000001 -0.00067817958919999999 -2.0315351110000002
		-4.3929812799999999 0.01178279191 -0.0014026417980000001
		-3.107072268 0.017334029609999999 2.0281474419999999
		-0.001207288484 0.012723693750000001 2.8682322280000001
		3.1052400759999998 0.00065245654979999995 2.0267414399999999
		4.3925550910000002 -0.01180851495 -0.0033910295920000001
		3.1066460789999999 -0.017359752650000001 -2.0329411140000002
		0.00078109930969999996 -0.012749416790000001 -2.8730258989999999
		-3.1056662660000001 -0.00067817958919999999 -2.0315351110000002
		-4.3929812799999999 0.01178279191 -0.0014026417980000001
		-3.107072268 0.017334029609999999 2.0281474419999999
		;
createNode joint -n "FKXShoulderPad2_R" -p "FKShoulderPad2_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXShoulderPad2_End_R" -p "FKXShoulderPad2_R";
	setAttr ".t" -type "double3" 1.1124434706744069e-13 1.5303351113900443 -3.1974423109204508e-14 ;
	setAttr ".r" -type "double3" -89.67432615354501 83.136399354583034 7.0424949689915941 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 89.9610795666987 -0.32333986796752934 -90.178784502556823 ;
createNode joint -n "FKOffsetClavicle_L" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" 1.9987998179687876 1.4515305646454788 -0.23751408606229596 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -89.999888212485487 -89.816816830938237 -167.41013587793839 ;
createNode transform -n "FKExtraClavicle_L" -p "FKOffsetClavicle_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251563e-16 0 0 ;
	setAttr ".r" -type "double3" -2.5030965343117876e-12 -6.9474885131309495e-28 3.1805546814635168e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKClavicle_L" -p "FKExtraClavicle_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.1102230246251563e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKClavicle_LShape" -p "FKClavicle_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		2.066140528 0.33743607520000002 2.0208767249999999
		2.9904095869999998 0.33743607520000002 -0.21050617120000001
		2.066140528 0.33743607520000002 -2.4418890680000001
		-0.16524236819999999 0.33743607520000002 -3.3661581260000002
		-2.3966252649999999 0.33743607520000002 -2.4418890680000001
		-3.3208943230000001 0.33743607520000002 -0.21050617120000001
		-2.3966252649999999 0.33743607520000002 2.0208767249999999
		-0.16524236819999999 0.33743607520000002 2.9451457840000002
		2.066140528 0.33743607520000002 2.0208767249999999
		2.9904095869999998 0.33743607520000002 -0.21050617120000001
		2.066140528 0.33743607520000002 -2.4418890680000001
		;
createNode joint -n "FKXClavicle_L" -p "FKClavicle_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetShoulder_L" -p "FKXClavicle_L";
	setAttr ".t" -type "double3" -0.6742571214066323 -1.852470213237952 0.80961543556772497 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -59.11515066267696 5.8645741643099614 2.3404236563732836 ;
createNode transform -n "FKExtraShoulder_L" -p "FKOffsetShoulder_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -8.8817841970012523e-16 0 ;
	setAttr ".r" -type "double3" 9.5416640443905456e-15 -3.180554681463516e-15 -9.5416640443905456e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 0.99999999999999989 ;
createNode transform -n "FKShoulder_L" -p "FKExtraShoulder_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulder_LShape" -p "FKShoulder_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.892903027 0.2135040258 1.9145888069999999
		2.695124904 0.2135040258 -0.02214612694
		1.892903027 0.2135040258 -1.958881061
		-0.043831906990000002 0.2135040258 -2.7611029380000001
		-1.9805668409999999 0.2135040258 -1.958881061
		-2.7827887179999999 0.2135040258 -0.02214612694
		-1.9805668409999999 0.2135040258 1.9145888069999999
		-0.043831906990000002 0.2135040258 2.7168106839999999
		1.892903027 0.2135040258 1.9145888069999999
		2.695124904 0.2135040258 -0.02214612694
		1.892903027 0.2135040258 -1.958881061
		;
createNode joint -n "FKXShoulder_L" -p "FKShoulder_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow_L" -p "FKXShoulder_L";
	setAttr ".t" -type "double3" -0.049680678251221355 -4.4542548449164396 0.25171649050149725 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.6128853374257202 6.3110380171443063 84.200714276462946 ;
createNode transform -n "FKExtraElbow_L" -p "FKOffsetElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 2.2204460492503131e-16 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -3.1805546814635174e-14 -2.5444437451708128e-14 -2.544443745170814e-14 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 1 ;
createNode transform -n "FKElbow_L" -p "FKExtraElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -6.6613381477509392e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999967 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow_LShape" -p "FKElbow_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.7477946010000001 0.10007105099999999 1.705726952
		2.4585712329999998 0.10007105099999999 -0.01023963303
		1.7477946010000001 0.10007105099999999 -1.726206218
		0.031828016149999999 0.10007105099999999 -2.4369828500000001
		-1.6841385689999999 0.10007105099999999 -1.726206218
		-2.3949152009999999 0.10007105099999999 -0.01023963303
		-1.6841385689999999 0.10007105099999999 1.705726952
		0.031828016149999999 0.10007105099999999 2.416503584
		1.7477946010000001 0.10007105099999999 1.705726952
		2.4585712329999998 0.10007105099999999 -0.01023963303
		1.7477946010000001 0.10007105099999999 -1.726206218
		;
createNode joint -n "FKXElbow_L" -p "FKElbow_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetWrist_L" -p "FKXElbow_L";
	setAttr ".t" -type "double3" -0.38546841457728664 -1.3131720551496158 0.12401197395647046 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -4.3388153447744235 -13.483344105607005 -8.7888031173482055 ;
createNode transform -n "FKExtraWrist_L" -p "FKOffsetWrist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -5.2966875576601912e-31 9.5416640443905503e-15 6.3611093629270335e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode transform -n "FKWrist_L" -p "FKExtraWrist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -6.6613381477509392e-16 2.2204460492503131e-16 1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000009 1.0000000000000004 1.0000000000000009 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWrist_LShape" -p "FKWrist_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.547027291 3.8302694350000001e-14 1.547027291
		2.1878269760000002 4.5741188609999989e-14 -1.4033219029999998e-13
		1.547027291 6.7057470690000006e-14 -1.547027291
		4.6629367029999989e-14 8.9594998090000001e-14 -2.1878269760000002
		-1.547027291 1.001421168e-13 -1.547027291
		-2.1878269760000002 9.2925667159999993e-14 -2.6645352589999998e-13
		-1.547027291 7.1609385090000005e-14 1.547027291
		-8.4598994479999992e-14 4.8960835390000003e-14 2.1878269760000002
		1.547027291 3.8302694350000001e-14 1.547027291
		2.1878269760000002 4.5741188609999989e-14 -1.4033219029999998e-13
		1.547027291 6.7057470690000006e-14 -1.547027291
		;
createNode joint -n "FKXWrist_L" -p "FKWrist_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToWrist_L" -p "FKXWrist_L";
	setAttr ".r" -type "double3" 99.037415998337977 7.2667622316161644 -141.01152930371077 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.999999999999999 0.999999999999999 0.99999999999999922 ;
createNode joint -n "FKOffsetShoulderPad1_L" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" 1.8142828899083216 0.28608493210809272 -0.28779146322840943 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -102.26658694292608 -89.81681683093818 -167.41013587794441 ;
createNode transform -n "FKExtraShoulderPad1_L" -p "FKOffsetShoulderPad1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 -1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" 5.3369707554957804e-12 -6.3611093628997776e-15 5.8522206138928686e-13 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKShoulderPad1_L" -p "FKExtraShoulderPad1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulderPad1_LShape" -p "FKShoulderPad1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		3.1349288880000001 0.00067163300200000003 2.0483980430000002
		4.434462581 -0.01190761305 -0.0010036303869999999
		3.1363482359999999 -0.017511540870000002 -2.0498173909999999
		0.0010036303869999999 -0.012857445550000001 -2.897875924
		-3.1349288880000001 -0.00067163300190000005 -2.0483980430000002
		-4.434462581 0.01190761305 0.0010036303869999999
		-3.1363482359999999 0.017511540870000002 2.0498173909999999
		-0.0010036303869999999 0.012857445550000001 2.897875924
		3.1349288880000001 0.00067163300200000003 2.0483980430000002
		4.434462581 -0.01190761305 -0.0010036303869999999
		3.1363482359999999 -0.017511540870000002 -2.0498173909999999
		;
createNode joint -n "FKXShoulderPad1_L" -p "FKShoulderPad1_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXShoulderPad1_End_L" -p "FKXShoulderPad1_L";
	setAttr ".t" -type "double3" -4.163336342344337e-14 -1.5513811389331271 -1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" 89.674326153546403 96.863600645447747 -172.95750503098017 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -90.038920433301314 -0.32333986796610259 -90.178784502554251 ;
createNode joint -n "FKOffsetShoulderPad2_L" -p "FKParentConstraintToChest_M";
	setAttr ".t" -type "double3" 1.7078360951756515 -0.067167164694192039 -0.47936911453115782 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -102.26658694292608 -89.81681683093818 -167.41013587794441 ;
createNode transform -n "FKExtraShoulderPad2_L" -p "FKOffsetShoulderPad2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251563e-16 4.4408920985006262e-16 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" 1.3930829504810207e-12 -1.2722218725838992e-14 1.2404163257707722e-12 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKShoulderPad2_L" -p "FKExtraShoulderPad2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulderPad2_LShape" -p "FKShoulderPad2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		3.1056662660000001 0.00067817958910000001 2.0315351110000002
		4.3929812799999999 -0.01178279191 0.0014026417980000001
		3.107072268 -0.017334029609999999 -2.0281474419999999
		0.001207288484 -0.012723693750000001 -2.8682322280000001
		-3.1052400759999998 -0.00065245654969999997 -2.0267414399999999
		-4.3925550910000002 0.01180851495 0.0033910295920000001
		-3.1066460789999999 0.017359752650000001 2.0329411140000002
		-0.00078109930969999996 0.012749416790000001 2.8730258989999999
		3.1056662660000001 0.00067817958910000001 2.0315351110000002
		4.3929812799999999 -0.01178279191 0.0014026417980000001
		3.107072268 -0.017334029609999999 -2.0281474419999999
		;
createNode joint -n "FKXShoulderPad2_L" -p "FKShoulderPad2_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXShoulderPad2_End_L" -p "FKXShoulderPad2_L";
	setAttr ".t" -type "double3" -8.2711615334574162e-14 -1.5303351113900443 3.1974423109204508e-14 ;
	setAttr ".r" -type "double3" 89.674326153545024 96.863600645418316 -172.95750503100814 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -90.038920433301314 -0.32333986796750308 -90.178784502555729 ;
createNode parentConstraint -n "FKParentConstraintToChest_M_parentConstraint1" -p
		 "FKParentConstraintToChest_M";
	addAttr -ci true -k true -sn "w0" -ln "Chest_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.0013580322265625e-05 9.1969223263959261 -0.47106295453907898 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToPelvis_M" -p "FKSystem";
	setAttr ".ro" 3;
createNode joint -n "FKOffsetHipTwist_R" -p "FKParentConstraintToPelvis_M";
	setAttr ".t" -type "double3" -1.2119851708412173 0.050251571453562072 -0.029227975692607263 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 235.139589423714 90.000002504478161 ;
createNode transform -n "FKExtraHipTwist_R" -p "FKOffsetHipTwist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 2.2204460492503131e-16 4.4408920985006262e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000004 ;
createNode transform -n "FKHipTwist_R" -p "FKExtraHipTwist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999967 0.99999999999999933 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHipTwist_RShape" -p "FKHipTwist_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.74373236639999996 -0.033731662209999999 -0.64979319440000005
		-1.0044809889999999 -0.033731662209999999 -0.02029033257
		-0.74373236639999996 -0.033731662209999999 0.60921252920000002
		-0.1142295046 -0.033731662209999999 0.86996115220000003
		0.5152733572 -0.033731662209999999 0.60921252920000002
		0.77602198020000002 -0.033731662209999999 -0.02029033257
		0.5152733572 -0.033731662209999999 -0.64979319440000005
		-0.1142295046 -0.033731662209999999 -0.91054181739999995
		-0.74373236639999996 -0.033731662209999999 -0.64979319440000005
		-1.0044809889999999 -0.033731662209999999 -0.02029033257
		-0.74373236639999996 -0.033731662209999999 0.60921252920000002
		;
createNode joint -n "FKXHipTwist_R" -p "FKHipTwist_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHip_R" -p "FKXHipTwist_R";
	setAttr ".t" -type "double3" -0.088558954171632287 1.1113775190156687 -0.041558800165023557 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0.72356916313425901 1.0610892041902271 -90.162076063293398 ;
createNode transform -n "FKExtraHip_R" -p "FKOffsetHip_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 8.8817841970012523e-16 -4.4408920985006262e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKHip_R" -p "FKExtraHip_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 -6.6613381477509392e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHip_RShape" -p "FKHip_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.70946947689999995 -0.025387602030000001 -0.71343500530000004
		-1.0032502780000001 -0.025387602030000001 -0.0041854102749999999
		-0.70946947689999995 -0.025387602030000001 0.70506418479999999
		-0.00021988185730000001 -0.025387602030000001 0.99884498619999995
		0.70902971319999997 -0.025387602030000001 0.70506418479999999
		1.002810515 -0.025387602030000001 -0.0041854102749999999
		0.70902971319999997 -0.025387602030000001 -0.71343500530000004
		-0.00021988185730000001 -0.025387602030000001 -1.0072158069999999
		-0.70946947689999995 -0.025387602030000001 -0.71343500530000004
		-1.0032502780000001 -0.025387602030000001 -0.0041854102749999999
		-0.70946947689999995 -0.025387602030000001 0.70506418479999999
		;
createNode joint -n "FKXHip_R" -p "FKHip_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetKnee_R" -p "FKXHip_R";
	setAttr ".t" -type "double3" 0.032864012269020026 2.5867190732225178 0.19800989497251964 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 22.919761854393723 0 0 ;
createNode transform -n "FKExtraKnee_R" -p "FKOffsetKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 5.5511151231257827e-17 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "FKKnee_R" -p "FKExtraKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 5.5511151231257827e-17 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKKnee_RShape" -p "FKKnee_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.8852346056 -0.058392523809999999 -0.87639158989999999
		-1.2528021439999999 -0.058392523809999999 0.010994946029999999
		-0.8852346056 -0.058392523809999999 0.89838148200000001
		0.0021519302919999999 -0.058392523809999999 1.2659490200000001
		0.88953846619999999 -0.058392523809999999 0.89838148200000001
		1.2571060039999999 -0.058392523809999999 0.010994946029999999
		0.88953846619999999 -0.058392523809999999 -0.87639158989999999
		0.0021519302919999999 -0.058392523809999999 -1.243959128
		-0.8852346056 -0.058392523809999999 -0.87639158989999999
		-1.2528021439999999 -0.058392523809999999 0.010994946029999999
		-0.8852346056 -0.058392523809999999 0.89838148200000001
		;
createNode joint -n "FKXKnee_R" -p "FKKnee_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetAnkle_R" -p "FKXKnee_R";
	setAttr ".t" -type "double3" -0.036411222619632255 0.88064448175187948 0.022028406801983761 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 1.4900163328871543 -0.0077691775970408529 0.59745911162008247 ;
createNode transform -n "FKExtraAnkle_R" -p "FKOffsetAnkle_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "FKAnkle_R" -p "FKExtraAnkle_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKAnkle_RShape" -p "FKAnkle_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.0950922940000001 -2.1512737809999999e-08 -0.53632320479999995
		-1.5284986380000001 -3.8003223789999997e-08 0.01061993174
		-1.078897682 -3.3354769590000004e-08 0.56427109639999995
		-0.0096595675669999999 -1.0290376619999999e-08 0.8003089457
		1.0528705190000001 1.7679146509999999e-08 0.58046570890000004
		1.4862768630000001 3.416963249e-08 0.033522572309999998
		1.0366759059999999 2.9521178290000001e-08 -0.52012859239999998
		-0.03256220814 6.456785324e-09 -0.75616644160000002
		-1.0950922940000001 -2.1512737809999999e-08 -0.53632320479999995
		-1.5284986380000001 -3.8003223789999997e-08 0.01061993174
		-1.078897682 -3.3354769590000004e-08 0.56427109639999995
		;
createNode joint -n "FKXAnkle_R" -p "FKAnkle_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToAnkle_R" -p "FKXAnkle_R";
	setAttr ".r" -type "double3" 179.99999999999989 -0.89953576781090627 -1.7655625192200639e-31 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999967 ;
createNode joint -n "FKXFoot_End_R" -p "FKXAnkle_R";
	setAttr ".t" -type "double3" 0.066282725496551897 2.1170624371978843 -2.6287438008809834 ;
	setAttr ".r" -type "double3" -71.642383655790979 6.0206018424774548e-06 179.99994523464616 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 179.99999999999989 -0.89953576781090661 1.7081982243921089e-15 ;
createNode joint -n "FKXHeel_End_R" -p "FKXAnkle_R";
	setAttr ".t" -type "double3" -0.0033414129891737865 1.9928408706997929 1.7595106209549014 ;
	setAttr ".r" -type "double3" 59.095245418304813 0 179.99999999997971 ;
	setAttr ".jo" -type "double3" 179.99999999999989 -0.89953576781090661 1.7081982243921089e-15 ;
createNode joint -n "FKOffsetHipTwist_L" -p "FKParentConstraintToPelvis_M";
	setAttr ".t" -type "double3" 1.2119850516319275 0.050251571453562072 -0.029227975692607263 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.0509930188759925e-06 55.139586592066912 90.000002504478161 ;
createNode transform -n "FKExtraHipTwist_L" -p "FKOffsetHipTwist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 2.2204460492503131e-16 -6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" 7.7573728680895156e-12 -7.951386703658306e-15 7.1562480332923739e-15 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
createNode transform -n "FKHipTwist_L" -p "FKExtraHipTwist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999956 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHipTwist_LShape" -p "FKHipTwist_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.74373242029999997 0.033731636500000002 0.64979313400000005
		1.0044809939999999 0.033731583750000002 0.02029025165
		0.74373232099999997 0.033731572270000003 -0.60921258960000002
		0.1142294387 0.033731608789999999 -0.86996116300000004
		-0.51527340249999998 0.033731671909999997 -0.60921249030000002
		-0.77602197589999999 0.033731724659999997 0.020290392040000001
		-0.51527330329999999 0.033731736140000003 0.64979323330000005
		0.11422957910000001 0.03373169962 0.91054180669999996
		0.74373242029999997 0.033731636500000002 0.64979313400000005
		1.0044809939999999 0.033731583750000002 0.02029025165
		0.74373232099999997 0.033731572270000003 -0.60921258960000002
		;
createNode joint -n "FKXHipTwist_L" -p "FKHipTwist_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHip_L" -p "FKXHipTwist_L";
	setAttr ".t" -type "double3" 0.088558954171633161 -1.1113775190156698 0.041558800165023113 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0.72356916313425901 1.0610892041902271 -90.162076063293398 ;
createNode transform -n "FKExtraHip_L" -p "FKOffsetHip_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -8.8817841970012523e-16 0 ;
	setAttr ".r" -type "double3" -1.5530052155583582e-18 1.739365841425361e-16 1.987846675914698e-16 ;
	setAttr ".ro" 2;
createNode transform -n "FKHip_L" -p "FKExtraHip_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHip_LShape" -p "FKHip_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.70946944590000005 0.025387516379999999 0.71343509130000005
		1.0032502839999999 0.025387438129999999 0.0041855116059999996
		0.70946951999999996 0.025387406840000001 -0.70506409879999998
		0.00021994028619999999 0.025387440840000001 -0.99884493730000001
		-0.70902967009999995 0.025387520220000001 -0.70506417290000001
		-1.0028105089999999 0.025387598479999999 0.0041854067820000004
		-0.70902974429999999 0.02538762977 0.71343501720000002
		0.00021983546250000001 0.025387595759999999 1.007215856
		0.70946944590000005 0.025387516379999999 0.71343509130000005
		1.0032502839999999 0.025387438129999999 0.0041855116059999996
		0.70946951999999996 0.025387406840000001 -0.70506409879999998
		;
createNode joint -n "FKXHip_L" -p "FKHip_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetKnee_L" -p "FKXHip_L";
	setAttr ".t" -type "double3" -0.032864012269020471 -2.5867190732225178 -0.19800989497251889 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 22.919761854393723 0 0 ;
createNode transform -n "FKExtraKnee_L" -p "FKOffsetKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 2.7755575615628914e-17 ;
	setAttr ".r" -type "double3" 5.9790700798996775e-16 -1.9412565194479467e-16 -9.4733318149059809e-17 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKKnee_L" -p "FKExtraKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 2.7755575615628914e-17 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999967 0.99999999999999933 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKKnee_LShape" -p "FKKnee_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.88523434710000004 0.058392552209999997 0.87639192249999998
		1.2528019560000001 0.058392464099999999 -0.01099458433
		0.88523448780000003 0.058392415150000002 -0.89838114940000002
		-0.002152019055 0.058392434040000003 -1.265948758
		-0.88953858409999997 0.058392509699999998 -0.89838129010000001
		-1.257106193 0.058392597810000002 -0.01099478326
		-0.88953872479999996 0.058392646749999999 0.87639178179999999
		-0.0021522179830000001 0.058392627869999998 1.2439593900000001
		0.88523434710000004 0.058392552209999997 0.87639192249999998
		1.2528019560000001 0.058392464099999999 -0.01099458433
		0.88523448780000003 0.058392415150000002 -0.89838114940000002
		;
createNode joint -n "FKXKnee_L" -p "FKKnee_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetAnkle_L" -p "FKXKnee_L";
	setAttr ".t" -type "double3" 0.036411222619632699 -0.88064448175188004 -0.022028406801983508 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 1.4900163328871543 -0.0077691775970408529 0.59745911162008247 ;
createNode transform -n "FKExtraAnkle_L" -p "FKOffsetAnkle_L";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 1.1581116840390229e-13 3.975693351829396e-16 4.868820137709275e-32 ;
	setAttr ".ro" 3;
createNode transform -n "FKAnkle_L" -p "FKExtraAnkle_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKAnkle_LShape" -p "FKAnkle_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.095092014 2.2796448730000001e-08 0.53632362980000003
		1.5284984020000001 -2.559677492e-08 -0.010619472200000001
		1.0788974899999999 -5.0439523670000001e-08 -0.56427067279999998
		0.0096593941630000006 -3.7179252119999997e-08 -0.80030860739999998
		-1.0528707100000001 6.4163523379999999e-09 -0.58046545530000004
		-1.4862770970000001 5.4809576429999998e-08 -0.033522353370000001
		-1.0366761849999999 7.9652324739999999e-08 0.52012884719999997
		0.032561910520000001 6.6392053630000008e-08 0.75616678179999997
		1.095092014 2.2796448730000001e-08 0.53632362980000003
		1.5284984020000001 -2.559677492e-08 -0.010619472200000001
		1.0788974899999999 -5.0439523670000001e-08 -0.56427067279999998
		;
createNode joint -n "FKXAnkle_L" -p "FKAnkle_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToAnkle_L" -p "FKXAnkle_L";
	setAttr ".r" -type "double3" 0 -0.89953576781090605 0 ;
	setAttr ".ro" 3;
createNode joint -n "FKXFoot_End_L" -p "FKXAnkle_L";
	setAttr ".t" -type "double3" -0.066282725496551897 -2.1170624371978901 2.6287438008809803 ;
	setAttr ".r" -type "double3" 108.35761634420903 6.0206018424774624e-06 179.99994523464616 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 -0.89953576781090627 0 ;
createNode joint -n "FKXHeel_End_L" -p "FKXAnkle_L";
	setAttr ".t" -type "double3" 0.0033414129891737865 -1.99284087069979 -1.7595106209549056 ;
	setAttr ".r" -type "double3" 59.095245418304785 180 2.0279216649011382e-11 ;
	setAttr ".jo" -type "double3" 0 -0.89953576781090627 0 ;
createNode parentConstraint -n "FKParentConstraintToPelvis_M_parentConstraint1" -p
		 "FKParentConstraintToPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 29.999999999999996 0 0 ;
	setAttr ".rst" -type "double3" 5.9604644775390625e-08 5.2923843071224637 -1.2424887120723724 ;
	setAttr ".rsrr" -type "double3" 29.999999999999996 0 0 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToWrist_L" -p "FKSystem";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1 0.99999999999999989 ;
createNode joint -n "FKOffsetMiddleFinger1_L" -p "FKParentConstraintToWrist_L";
	setAttr ".t" -type "double3" -1.2140707715363603 -2.6126476851167157 0.55558593361305419 ;
	setAttr ".r" -type "double3" 51.939019241392231 228.41338305362552 27.707934490889642 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger1_L" -p "FKOffsetMiddleFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.6645352591003757e-15 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 3.1805546814635152e-15 1.2722218725854064e-14 1.2722218725854064e-14 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999967 1 ;
createNode transform -n "FKMiddleFinger1_L" -p "FKExtraMiddleFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000002 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger1_LShape" -p "FKMiddleFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.44340486270000001 0.087030455579999999 0.50538264450000003
		0.64998315579999999 0.08703048612 0.0066584562939999998
		0.44340481230000001 0.087030509290000002 -0.49206571100000002
		-0.055319375890000001 0.087030511529999993 -0.69864400410000005
		-0.55404354320000004 0.087030491520000003 -0.49206566060000001
		-0.76062183630000002 0.087030460990000003 0.0066585276080000002
		-0.55404349269999997 0.08703043781 0.50538269489999998
		-0.055319304579999999 0.087030435569999995 0.71196098799999996
		0.44340486270000001 0.087030455579999999 0.50538264450000003
		0.64998315579999999 0.08703048612 0.0066584562939999998
		0.44340481230000001 0.087030509290000002 -0.49206571100000002
		;
createNode joint -n "FKXMiddleFinger1_L" -p "FKMiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger2_L" -p "FKXMiddleFinger1_L";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -0.99106178888130803 -1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" 9.49906595140137 1.0448838333324315 58.190623086813204 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger2_L" -p "FKOffsetMiddleFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -1.9083328088781101e-14 -6.3611093629270335e-15 -3.1805546814635183e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKMiddleFinger2_L" -p "FKExtraMiddleFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger2_LShape" -p "FKMiddleFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.60342969069999997 0.013272863609999999 0.60342942759999996
		0.85337843339999997 0.01327291031 -1.8662939460000002e-07
		0.60342971219999997 0.013272951910000001 -0.60342980970000004
		9.8002240989999976e-08 0.01327296404 -0.85337855240000005
		-0.60342952510000003 0.013272939589999999 -0.60342983120000004
		-0.85337826780000003 0.01327289289 -2.1697028089999998e-07
		-0.60342954650000002 0.01327285129 0.60342940609999995
		6.7661355630000002e-08 0.01327283917 0.85337814879999996
		0.60342969069999997 0.013272863609999999 0.60342942759999996
		0.85337843339999997 0.01327291031 -1.8662939460000002e-07
		0.60342971219999997 0.013272951910000001 -0.60342980970000004
		;
createNode joint -n "FKXMiddleFinger2_L" -p "FKMiddleFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleFinger3_End_L" -p "FKXMiddleFinger2_L";
	setAttr ".t" -type "double3" 0 -1.4053515974398838 1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" 170.09465230587344 66.284880117077805 156.35490863147388 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 9.9053476941265099 246.28488011707779 23.645091368526199 ;
createNode joint -n "FKOffsetIndexFinger1_L" -p "FKParentConstraintToWrist_L";
	setAttr ".t" -type "double3" -0.54621867886444408 -2.7196885306359224 0.92085001407918732 ;
	setAttr ".r" -type "double3" 173.83122403741098 -48.70206965031813 164.82754573009507 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger1_L" -p "FKOffsetIndexFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 -1.3322676295501878e-15 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -1.2722218725854051e-14 2.226388277024463e-14 -8.587497639951495e-14 ;
	setAttr ".ro" 5;
createNode transform -n "FKIndexFinger1_L" -p "FKExtraIndexFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger1_LShape" -p "FKIndexFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.47995336519999998 -8.1083238789999997e-08 0.47995306160000001
		0.6787564787 -5.8121721659999996e-08 -1.7614287980000004e-07
		0.47995331930000001 -2.7515075549999999e-08 -0.47995339479999999
		8.1590195929999994e-08 -7.1922592329999998e-09 -0.67875650830000001
		-0.47995313709999998 -9.0581018239999971e-09 -0.47995334890000002
		-0.67875625049999999 -3.2019619399999999e-08 -1.1119784470000001e-07
		-0.47995309120000001 -6.2626265060000005e-08 0.47995310749999998
		1.4653522750000001e-07 -8.2949081379999997e-08 0.67875622089999998
		0.47995336519999998 -8.1083238789999997e-08 0.47995306160000001
		0.6787564787 -5.8121721659999996e-08 -1.7614287980000004e-07
		0.47995331930000001 -2.7515075549999999e-08 -0.47995339479999999
		;
createNode joint -n "FKXIndexFinger1_L" -p "FKIndexFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetIndexFinger2_L" -p "FKXIndexFinger1_L";
	setAttr ".t" -type "double3" -2.6645352591003757e-15 -0.92790311814927762 2.6645352591003757e-15 ;
	setAttr ".r" -type "double3" 1.3707126716026008 -7.0081534559372679e-13 61.892681239608493 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger2_L" -p "FKOffsetIndexFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" -4.7708320221952752e-15 -7.9513867036587959e-16 9.5416640443905503e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "FKIndexFinger2_L" -p "FKExtraIndexFinger2_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger2_LShape" -p "FKIndexFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.63046461149999999 -1.640619662e-07 0.63046444960000003
		0.8916116275 -1.2646324569999999e-07 -1.703267802e-07
		0.63046464540000002 -7.8227066600000001e-08 -0.63046480429999996
		2.5455804579999994e-08 -4.7609526539999996e-08 -0.89161182039999998
		-0.63046460859999998 -5.2545966240000005e-08 -0.6304648383
		-0.89161162459999999 -9.0144687710000004e-08 -2.183101948e-07
		-0.63046464250000001 -1.3838086720000001e-07 0.6304644157
		-2.252761355e-08 -1.6899840589999996e-07 0.89161143180000002
		0.63046461149999999 -1.640619662e-07 0.63046444960000003
		0.8916116275 -1.2646324569999999e-07 -1.703267802e-07
		0.63046464540000002 -7.8227066600000001e-08 -0.63046480429999996
		;
createNode joint -n "FKXIndexFinger2_L" -p "FKIndexFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXIndexFinger3_End_L" -p "FKXIndexFinger2_L";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 -1.4990935335754481 1.4210854715202004e-14 ;
	setAttr ".r" -type "double3" -6.7418719640323932 84.191074302064266 -20.254146458539665 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 6.7418719640325859 -84.191074302064294 20.254146458539431 ;
createNode joint -n "FKOffsetThumbFinger1_L" -p "FKParentConstraintToWrist_L";
	setAttr ".t" -type "double3" -0.002445901796471972 -2.2723833325074567 1.6528888569724769 ;
	setAttr ".r" -type "double3" 46.300609289087497 201.22916185508308 23.018104625610864 ;
createNode transform -n "FKExtraThumbFinger1_L" -p "FKOffsetThumbFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 8.8817841970012523e-16 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -1.9480897423964041e-14 -6.3611093629270367e-15 -1.3318572728628477e-14 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKThumbFinger1_L" -p "FKExtraThumbFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 -8.8817841970012523e-16 1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 6.3611093629270335e-15 4.7708320221952736e-15 -6.361109362927032e-15 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger1_LShape" -p "FKThumbFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.44072578439999999 -1.635912206e-07 0.44072568680000002
		0.62328037869999997 -1.6172937570000001e-07 -8.5228438529999978e-08
		0.44072578820000002 -1.348587864e-07 -0.44072585879999998
		1.6159017240000001e-08 -9.8719878490000004e-08 -0.62328045310000002
		-0.44072575749999998 -7.4482336740000004e-08 -0.44072586260000002
		-0.62328035179999997 -7.6344180750000006e-08 -9.0579104220000013e-08
		-0.44072576120000001 -1.032147701e-07 0.44072568299999998
		1.080834955e-08 -1.3935367700000001e-07 0.62328027730000002
		0.44072578439999999 -1.635912206e-07 0.44072568680000002
		0.62328037869999997 -1.6172937570000001e-07 -8.5228438529999978e-08
		0.44072578820000002 -1.348587864e-07 -0.44072585879999998
		;
createNode joint -n "FKXThumbFinger1_L" -p "FKThumbFinger1_L";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetThumbFinger2_L" -p "FKXThumbFinger1_L";
	setAttr ".t" -type "double3" -1.1102230246251563e-15 -0.79591438041152163 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 1.5968655711070121 -2.907173970680724 61.204054737298641 ;
createNode transform -n "FKExtraThumbFinger2_L" -p "FKOffsetThumbFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 0 ;
	setAttr ".r" -type "double3" 1.3020395727241272e-14 7.9513867036587899e-16 3.9756933518293969e-16 ;
createNode transform -n "FKThumbFinger2_L" -p "FKExtraThumbFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -1.5902773407317587e-14 3.1805546814635168e-15 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger2_LShape" -p "FKThumbFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.60001908500000001 -7.0316302110000003e-08 0.60001914690000002
		0.84855520900000003 -8.2179409499999987e-08 -7.9154738760000006e-08
		0.60001912400000001 -6.0175210589999994e-08 -0.60001932140000003
		-1.020808349e-07 -1.7193466650000001e-08 -0.84855544540000005
		-0.60001934440000004 2.1587700959999999e-08 -0.60001936050000004
		-0.84855546839999996 3.3450809229999999e-08 -1.343867648e-07
		-0.60001938340000005 1.144661077e-08 0.60001910790000001
		-1.5731286229999999e-07 -3.1535134060000003e-08 0.84855523190000004
		0.60001908500000001 -7.0316302110000003e-08 0.60001914690000002
		0.84855520900000003 -8.2179409499999987e-08 -7.9154738760000006e-08
		0.60001912400000001 -6.0175210589999994e-08 -0.60001932140000003
		;
createNode joint -n "FKXThumbFinger2_L" -p "FKThumbFinger2_L";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXThumbFinger3_End_L" -p "FKXThumbFinger2_L";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -1.3935263360622552 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -23.123892219692465 16.638252964976676 -8.7551914401189119 ;
	setAttr ".jo" -type "double3" 21.554454454522109 -18.664904387689649 1.7448409252680497 ;
createNode parentConstraint -n "FKParentConstraintToWrist_L_parentConstraint1" -p
		 "FKParentConstraintToWrist_L";
	addAttr -ci true -k true -sn "w0" -ln "Wrist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -78.34079944541449 -140.44660172242754 0.1287536080021075 ;
	setAttr ".rst" -type "double3" 6.9193342330667944 6.4581298418186952 0.37777645617570688 ;
	setAttr ".rsrr" -type "double3" -78.34079589880443 -140.44660394352499 0.12875437218184382 ;
	setAttr -k on ".w0";
createNode transform -n "PelvisCenterBtwLegsBlended_M" -p "FKSystem";
	setAttr ".ro" 3;
createNode transform -n "CenterOffset_M" -p "PelvisCenterBtwLegsBlended_M";
createNode transform -n "CenterExtra_M" -p "CenterOffset_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Center_M" -p "CenterExtra_M";
	addAttr -ci true -k true -sn "CenterBtwFeet" -ln "CenterBtwFeet" -min 0 -max 10 
		-at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 4;
	setAttr -k on ".CenterBtwFeet";
createNode nurbsCurve -n "Center_MShape" -p "Center_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 46 0 no 3
		47 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46
		47
		-3.5188755710000001 -0.23222724750000001 2.4989613190000001e-05
		-3.7428901240000001 -0.23222724750000001 0.68947241429999995
		-4.3293693830000004 -0.23222724750000001 1.1155759890000001
		-5.054298803 -0.23222724750000001 1.1155759890000001
		-5.640778063 -0.23222724750000001 0.68947241429999995
		-5.8647926129999997 -0.23222724750000001 2.4989613190000001e-05
		-5.640778063 -0.23222724750000001 -0.68942243510000001
		-5.054298803 -0.23222724750000001 -1.1155260090000001
		-4.3293693830000004 -0.23222724750000001 -1.1155260090000001
		-3.7428901240000001 -0.23222724750000001 -0.68942243510000001
		-3.5188755710000001 -0.23222724750000001 2.4989613190000001e-05
		-6.2660851749999999e-09 -0.23222724750000001 2.4989613190000001e-05
		3.518875558 -0.23222724750000001 2.4989613190000001e-05
		3.7428901109999999 -0.23222724750000001 0.68947241429999995
		4.3293693700000002 -0.23222724750000001 1.1155759890000001
		5.0542987899999998 -0.23222724750000001 1.1155759890000001
		5.6407780519999999 -0.23222724750000001 0.68947241429999995
		5.8647926019999996 -0.23222724750000001 2.4989613190000001e-05
		5.6407780519999999 -0.23222724750000001 -0.68942243510000001
		5.0542987899999998 -0.23222724750000001 -1.1155260090000001
		4.3293693700000002 -0.23222724750000001 -1.1155260090000001
		3.7428901109999999 -0.23222724750000001 -0.68942243510000001
		3.518875558 -0.23222724750000001 2.4989613190000001e-05
		-6.2660851749999999e-09 -0.23222724750000001 2.4989613190000001e-05
		-6.2660851749999999e-09 -0.23222724750000001 -3.5188505750000001
		-0.68944743090000005 -0.23222724750000001 -3.742865128
		-1.1155510049999999 -0.23222724750000001 -4.3293443869999999
		-1.1155510049999999 -0.23222724750000001 -5.0542738070000004
		-0.68944743090000005 -0.23222724750000001 -5.6407530689999996
		-6.2660851749999999e-09 -0.23222724750000001 -5.864767617
		0.68944741850000002 -0.23222724750000001 -5.6407530689999996
		1.115550993 -0.23222724750000001 -5.0542738070000004
		1.115550993 -0.23222724750000001 -4.3293443869999999
		0.68944741850000002 -0.23222724750000001 -3.742865128
		-6.2660851749999999e-09 -0.23222724750000001 -3.5188505750000001
		-6.2660851749999999e-09 -0.23222724750000001 2.4989613190000001e-05
		-6.2660851749999999e-09 -0.23222724750000001 3.518900554
		-0.68944743090000005 -0.23222724750000001 3.742915107
		-1.1155510049999999 -0.23222724750000001 4.3293943659999998
		-1.1155510049999999 -0.23222724750000001 5.0543237870000004
		-0.68944743090000005 -0.23222724750000001 5.6408030470000003
		-6.2660851749999999e-09 -0.23222724750000001 5.864817597
		0.68944741850000002 -0.23222724750000001 5.6408030470000003
		1.115550993 -0.23222724750000001 5.0543237870000004
		1.115550993 -0.23222724750000001 4.3293943659999998
		0.68944741850000002 -0.23222724750000001 3.742915107
		-6.2660851749999999e-09 -0.23222724750000001 3.518900554
		;
createNode joint -n "FKOffsetPelvis_M" -p "Center_M";
	setAttr ".t" -type "double3" 5.9604644775390625e-08 0 0 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 30.000000834826057 0 0 ;
createNode transform -n "FKExtraPelvis_M" -p "FKOffsetPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode transform -n "FKPelvis_M" -p "FKExtraPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode nurbsCurve -n "FKPelvis_MShape" -p "FKPelvis_M";
	setAttr -l on -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.346175437810893 3.8134046529879578e-16 -1.3461754378108961
		-1.9037795614857052 4.2272398059444275e-16 0
		-1.3461754378108937 2.1648152119820597e-16 1.3461754378108943
		-5.516676881144957e-16 -1.1657287731278089e-16 1.9037795614857052
		1.3461754378108934 -3.8134046529879578e-16 1.3461754378108943
		1.9037795614857052 -4.2272398059444285e-16 0
		1.3461754378108937 -2.1648152119820617e-16 -1.3461754378108928
		1.0225237162423788e-15 1.1657287731278081e-16 -1.9037795614857052
		-1.346175437810893 3.8134046529879578e-16 -1.3461754378108961
		-1.9037795614857052 4.2272398059444275e-16 0
		-1.3461754378108937 2.1648152119820597e-16 1.3461754378108943
		;
createNode transform -n "HipSwingerGroupPelvis_M" -p "FKPelvis_M";
	setAttr ".t" -type "double3" 0 1.6020692683703901 -0.068936328703013583 ;
createNode orientConstraint -n "HipSwingerGroupPelvis_M_orientConstraint1" -p "HipSwingerGroupPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "HipSwingerPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode joint -n "FKXPelvis_M" -p "HipSwingerGroupPelvis_M";
	setAttr ".t" -type "double3" 0 -1.6020692683703901 0.068936328703013583 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "HipSwingerStabalizePelvis_M" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 0 1.6020692683703901 -0.068936328703013583 ;
createNode joint -n "FKOffsetSpineA_M" -p "HipSwingerStabalizePelvis_M";
	setAttr ".t" -type "double3" 0 -1.0801580074826234e-08 2.0717754400934041e-08 ;
createNode transform -n "FKExtraSpineA_M" -p "FKOffsetSpineA_M";
	setAttr -l on -k off ".v";
createNode transform -n "FKSpineA_M" -p "FKExtraSpineA_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKSpineA_MShape" -p "FKSpineA_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.6527652430000002 -0.49782815060000002 -2.659207946
		-3.751577476 -0.49782815060000002 -0.0064405471020000001
		-2.6527652430000002 -0.49782815060000002 2.6463268520000001
		2.1559711e-06 -0.49782815060000002 3.7451390849999999
		2.6527695549999999 -0.49782815060000002 2.6463268520000001
		3.7515817880000002 -0.49782815060000002 -0.0064405471020000001
		2.6527695549999999 -0.49782815060000002 -2.659207946
		2.1559711030000001e-06 -0.49782815060000002 -3.7580201789999998
		-2.6527652430000002 -0.49782815060000002 -2.659207946
		-3.751577476 -0.49782815060000002 -0.0064405471020000001
		-2.6527652430000002 -0.49782815060000002 2.6463268520000001
		;
createNode joint -n "FKXSpineA_M" -p "FKSpineA_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetChest_M" -p "FKXSpineA_M";
	setAttr ".t" -type "double3" -1.0073184967041016e-05 2.4826371914940086 0.030091693813034004 ;
createNode transform -n "FKExtraChest_M" -p "FKOffsetChest_M";
	setAttr -l on -k off ".v";
createNode transform -n "FKChest_M" -p "FKExtraChest_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKChest_MShape" -p "FKChest_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-3.2542933230000002 -0.1053735202 -3.2542933230000002
		-4.6022657530000002 -0.1053735202 0
		-3.2542933230000002 -0.1053735202 3.2542933230000002
		-1.3336214749999999e-15 -0.1053735202 4.6022657530000002
		3.2542933230000002 -0.1053735202 3.2542933230000002
		4.6022657530000002 -0.1053735202 0
		3.2542933230000002 -0.1053735202 -3.2542933230000002
		2.471885914e-15 -0.1053735202 -4.6022657530000002
		-3.2542933230000002 -0.1053735202 -3.2542933230000002
		-4.6022657530000002 -0.1053735202 0
		-3.2542933230000002 -0.1053735202 3.2542933230000002
		;
createNode joint -n "FKXChest_M" -p "FKChest_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToChest_M" -p "FKXChest_M";
createNode joint -n "FKXChest_End_M" -p "FKXChest_M";
	setAttr ".t" -type "double3" -3.3881317890172021e-21 1.3032944042756736 1.0309757934324235 ;
	setAttr ".ro" 5;
createNode orientConstraint -n "HipSwingerStabalizePelvis_M_orientConstraint1" -p
		 "HipSwingerStabalizePelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -30.000000834826057 0 0 ;
	setAttr ".rsrr" -type "double3" -30.000000834826057 0 0 ;
	setAttr -k on ".w0";
createNode transform -n "HipSwingerOffsetPelvis_M" -p "Center_M";
	setAttr ".t" -type "double3" 5.9604644775390625e-08 1.4219008385810341 0.74133404300250527 ;
	setAttr ".r" -type "double3" 29.999999999999996 0 0 ;
createNode transform -n "HipSwingerPelvis_M" -p "HipSwingerOffsetPelvis_M";
	setAttr -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "HipSwingerPelvis_MShape" -p "HipSwingerPelvis_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-4.783346216 -2.4837389735268181 -2.817836091520062
		-3.3823365449999998 -0.63211017376008349 0.38623825394438566
		-4.783346216 1.2195186260066473 3.5903125994088305
		-9.8011651149999996e-16 0.67718910668964616 2.651860950865355
		4.783346216 1.2195186260066473 3.5903125994088305
		3.3823365449999998 -0.63211017376008349 0.38623825394438566
		4.783346216 -2.4837389735268181 -2.817836091520062
		1.8166595569999997e-15 -1.9414094542098177 -1.8793844429765865
		-4.783346216 -2.4837389735268181 -2.817836091520062
		-3.3823365449999998 -0.63211017376008349 0.38623825394438566
		-4.783346216 1.2195186260066473 3.5903125994088305
		;
createNode parentConstraint -n "PelvisCenterBtwLegsBlended_M_parentConstraint1" -p
		 "PelvisCenterBtwLegsBlended_M";
	addAttr -ci true -k true -sn "w0" -ln "PelvisCenter_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PelvisCenterBtwLegsOffset_MW1" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0 5.2923843071224637 -1.2424887120723724 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKSystem" -p "MotionSystem";
createNode transform -n "IKParentConstraint" -p "IKSystem";
createNode transform -n "IKParentConstraintShoulder_R" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "IKOffsetShoulder_R" -p "IKParentConstraintShoulder_R";
	setAttr ".t" -type "double3" 0.6742571214066323 1.8524702132379527 -0.80961543556771787 ;
	setAttr ".r" -type "double3" -59.289217946313727 0.99466479855881151 6.234277518582573 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode joint -n "IKXShoulder_R" -p "IKOffsetShoulder_R";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 2.6645352591003757e-15 0 ;
	setAttr ".r" -type "double3" 5.7126386160408169e-07 -2.3344188742736798e-06 2.3575782398290863e-07 ;
	setAttr ".ro" 5;
createNode joint -n "IKXElbow_R" -p "IKXShoulder_R";
	setAttr ".t" -type "double3" 0.04968067825121969 4.4542548449164414 -0.25171649050149725 ;
	setAttr ".r" -type "double3" 2.0935756583057695e-07 -3.6572155812039457e-07 -3.0636022113425124e-06 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.6128853374257202 6.3110380171443063 84.200714276462946 ;
createNode joint -n "IKXWrist_R" -p "IKXElbow_R";
	setAttr ".t" -type "double3" 0.38546841457728698 1.3131720551496158 -0.12401197395647225 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -4.3388153447744235 -13.483344105607005 -8.7888031173482055 ;
createNode transform -n "IKFKAlignedArm_R" -p "IKXWrist_R";
	setAttr ".t" -type "double3" 3.2112782433202369e-08 -1.7674817964774547e-07 -1.5932050168032674e-07 ;
	setAttr ".r" -type "double3" 2.2478034471664164e-06 2.4858097943778014e-06 -1.4501648540796972e-06 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
createNode orientConstraint -n "IKXWrist_R_orientConstraint1" -p "IKXWrist_R";
	addAttr -ci true -k true -sn "w0" -ln "IKArm_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -80.96258386062874 -7.2667634698114334 141.01152949735436 ;
	setAttr ".o" -type "double3" -78.340796409012142 -39.553395807217193 -179.87124256463076 ;
	setAttr ".rsrr" -type "double3" -1.272221872585407e-14 -6.3611093629270304e-15 -2.2263882770244617e-14 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector1" -p "IKXElbow_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationArm_R" -p "IKXElbow_R";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 2.2204460492503131e-16 0 ;
	setAttr ".r" -type "double3" 85.29282370352648 52.880024432355405 175.62546324307235 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999967 ;
createNode annotationShape -n "PoleAnnotationArm_RShape" -p "PoleAnnotationArm_R";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintShoulder_R_parentConstraint1" -p
		 "IKParentConstraintShoulder_R";
	addAttr -ci true -k true -sn "w0" -ln "Clavicle_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -77.410023432688362 90.000001593056183 0.18318161431027841 ;
	setAttr ".rst" -type "double3" -1.9987898043884655 10.648452901842983 -0.70857706131912912 ;
	setAttr ".rsrr" -type "double3" -77.410023432687183 90.000001593056183 0.18318477148766885 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintHip_R" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1 ;
createNode transform -n "IKOffsetHip_R" -p "IKParentConstraintHip_R";
	setAttr ".t" -type "double3" -0.088558954171565674 1.1113775190156745 -0.041558800165024223 ;
	setAttr ".r" -type "double3" 1.0589532311581595 -0.72669176884004516 -90.175490857607215 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999967 1.0000000000000004 1.0000000000000002 ;
createNode joint -n "IKXHip_R" -p "IKOffsetHip_R";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 -2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 1.5269010021029405e-09 -1.458909941463926e-06 -2.4159445750099675e-07 ;
	setAttr ".ro" 2;
createNode joint -n "IKXKnee_R" -p "IKXHip_R";
	setAttr ".t" -type "double3" 0.032864012269018694 2.5867190732225196 0.19800989497251864 ;
	setAttr ".r" -type "double3" 3.7688593373446359e-06 1.4151843465379821e-07 5.7204989554529073e-07 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 22.919761854393723 0 0 ;
createNode joint -n "IKXAnkle_R" -p "IKXKnee_R";
	setAttr ".t" -type "double3" -0.036411222619632255 0.88064448175187904 0.022028406801983896 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 1.4900163328871543 -0.0077691775970408546 0.59745911162008247 ;
createNode joint -n "IKXFoot_End_R" -p "IKXAnkle_R";
	setAttr ".t" -type "double3" 0.066282725496551453 2.1170624371978848 -2.6287438008809838 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 71.642386959019007 179.10051007640067 2.296520474639355e-05 ;
createNode transform -n "IKFKAlignedLeg_R" -p "IKXAnkle_R";
	setAttr ".t" -type "double3" 1.8679027435908324e-07 -1.2050711317712626e-07 -5.5776844093413658e-07 ;
	setAttr ".r" -type "double3" -1.0349119467001593e-05 0 -3.4972233309158576e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode orientConstraint -n "IKXAnkle_R_orientConstraint1" -p "IKXAnkle_R";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -179.99999317442339 -0.89953565303919392 1.6402246863687511e-06 ;
	setAttr ".o" -type "double3" 0 -0.89953576856490003 0 ;
	setAttr ".rsrr" -type "double3" -1.0992055099857669e-14 3.9756933548186512e-16 3.1162816174024908e-09 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector3" -p "IKXAnkle_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector2" -p "IKXKnee_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_R" -p "IKXKnee_R";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0 0 ;
	setAttr ".r" -type "double3" -178.50979672665198 -0.9070007618794641 0.57406674404629043 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode annotationShape -n "PoleAnnotationLeg_RShape" -p "PoleAnnotationLeg_R";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintHip_R_parentConstraint1" -p "IKParentConstraintHip_R";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 154.86041057628606 -2.2700737299075496e-06 89.9999976041086 ;
	setAttr ".rst" -type "double3" -1.2119851112365725 5.3505174324276403 -1.2426750957965838 ;
	setAttr ".rsrr" -type "double3" 154.86042096709369 0 90.000000000016556 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintPelvis_M" -p "IKParentConstraint";
	setAttr ".ro" 3;
createNode joint -n "IKXPelvis_M" -p "IKParentConstraintPelvis_M";
	setAttr ".t" -type "double3" -5.9557004103817993e-23 -8.8817841970012523e-16 2.6645352591003757e-15 ;
	setAttr ".r" -type "double3" -0.35341523849098089 1.4630160284740182e-07 3.1722080184819367e-06 ;
	setAttr ".ro" 3;
createNode joint -n "IKXSpineA_M" -p "IKXPelvis_M";
	setAttr ".t" -type "double3" 0 1.6020692693748249 -0.068936305360121963 ;
	setAttr ".jo" -type "double3" -30.000000834826057 0 0 ;
createNode joint -n "IKXChest_M" -p "IKXSpineA_M";
createNode transform -n "IKFKAlignedSpine_M" -p "IKXChest_M";
	setAttr ".t" -type "double3" 2.0856960390620239e-07 -0.0048326142781647263 0.02406943264222899 ;
	setAttr ".r" -type "double3" 0.35341607331700431 1.6030192893052539e-06 -2.7373770055723746e-06 ;
createNode parentConstraint -n "IKXChest_M_parentConstraint1" -p "IKXChest_M";
	addAttr -ci true -k true -sn "w0" -ln "IKfake2Spine_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKSpine4AlignTo_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 179.32468902838474 -4.927394131537155e-05 -0.00028809526587639229 ;
	setAttr ".rst" -type "double3" -2.2556611863931079e-05 -2.4828194150840872 -0.0008284322392103638 ;
	setAttr ".rsrr" -type "double3" 179.32467762558315 -6.0343706099583549e-05 -0.00028810409261406211 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode aimConstraint -n "IKXSpineA_M_aimConstraint1" -p "IKXSpineA_M";
	addAttr -ci true -sn "w0" -ln "IKfake2Spine_MW0" -dv 1 -at "double";
	addAttr -ci true -sn "w1" -ln "IKSpine4_MW1" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 0 0 1 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -178.77369328455055 -5.2191910576698389e-05 -0.00029347806652696808 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "IKfake1Spine_M" -p "IKXPelvis_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0 1.6020692693748249 -0.068936305360121963 ;
	setAttr ".r" -type "double3" 0.54383203377517986 -9.1216635673997868e-06 -4.4974629099649888e-06 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -30.000000834826057 0 0 ;
createNode joint -n "IKfake2Spine_M" -p "IKfake1Spine_M";
	setAttr ".t" -type "double3" -1.0073184967041016e-05 2.482637191494006 0.030091693813034004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode ikEffector -n "effector4" -p "IKfake1Spine_M";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode transform -n "IKFake1UpLocSpine_M" -p "IKfake1Spine_M";
	setAttr ".t" -type "double3" 0 1 0 ;
	setAttr ".r" -type "double3" 2.4848083448933737e-17 -6.6604956446716849e-07 2.2135475860178089e-09 ;
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999967 0.99999999999999989 ;
createNode locator -n "IKFake1UpLocSpine_MShape" -p "IKFake1UpLocSpine_M";
	setAttr -k off ".v";
createNode parentConstraint -n "IKParentConstraintPelvis_M_parentConstraint1" -p "IKParentConstraintPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine0AlignTo_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.0587911840678754e-22 8.8817841970012523e-16 
		-2.6645352591003757e-15 ;
	setAttr ".tg[0].tor" -type "double3" 0.35341523849097761 1.9566878288978869e-08 
		-3.1721476714987803e-06 ;
	setAttr ".lr" -type "double3" 29.999999999999996 -1.7005437716500094e-08 -9.6787299810428304e-09 ;
	setAttr ".rst" -type "double3" 5.9604644775390651e-08 5.2923843071224628 -1.2424887120723729 ;
	setAttr ".rsrr" -type "double3" 29.999999999999996 -1.7005437716500094e-08 -9.6787299810428304e-09 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintShoulder_L" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999967 0.99999999999999989 ;
createNode transform -n "IKOffsetShoulder_L" -p "IKParentConstraintShoulder_L";
	setAttr ".t" -type "double3" -0.6742571214066303 -1.8524702132379185 0.80961543556780491 ;
	setAttr ".r" -type "double3" -59.289217946315951 0.99466479855877399 6.2342775185826262 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000009 1.0000000000000002 ;
createNode joint -n "IKXShoulder_L" -p "IKOffsetShoulder_L";
	setAttr ".t" -type "double3" -1.1102230246251563e-16 1.7763568394002505e-15 0 ;
	setAttr ".r" -type "double3" 6.0591682318860452e-07 -2.4760250990706211e-06 2.500589330552158e-07 ;
	setAttr ".ro" 5;
createNode joint -n "IKXElbow_L" -p "IKXShoulder_L";
	setAttr ".t" -type "double3" -0.0496806782512208 -4.454254844916437 0.25171649050153633 ;
	setAttr ".r" -type "double3" 2.9745939000901188e-07 -5.2625899823470793e-07 -4.405858162857621e-06 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.6128853374257202 6.3110380171443063 84.200714276462946 ;
createNode joint -n "IKXWrist_L" -p "IKXElbow_L";
	setAttr ".t" -type "double3" -0.38546841457728842 -1.3131720551496151 0.12401197395647225 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -4.3388153447744235 -13.483344105607005 -8.7888031173482055 ;
createNode transform -n "IKFKAlignedArm_L" -p "IKXWrist_L";
	setAttr ".t" -type "double3" -3.2112769554615284e-08 1.7674812102796975e-07 1.5932080010827576e-07 ;
	setAttr ".r" -type "double3" 2.247802916013837e-06 2.4858127499082768e-06 -1.4501642640868972e-06 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999856 1.0000000000000004 ;
createNode orientConstraint -n "IKXWrist_L_orientConstraint1" -p "IKXWrist_L";
	addAttr -ci true -k true -sn "w0" -ln "IKArm_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 99.037416606599606 7.2667647520235858 -141.01152963853301 ;
	setAttr ".o" -type "double3" 101.65920359098791 -39.553395807217591 -179.87124256463062 ;
	setAttr ".rsrr" -type "double3" 360 -9.5416640443905503e-15 -360 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector6" -p "IKXElbow_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationArm_L" -p "IKXElbow_L";
	setAttr ".t" -type "double3" -1.1102230246251563e-15 -2.2204460492503131e-16 0 ;
	setAttr ".r" -type "double3" -94.707176296473591 52.880024432355803 175.62546324307232 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999967 0.99999999999999967 ;
createNode annotationShape -n "PoleAnnotationArm_LShape" -p "PoleAnnotationArm_L";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintShoulder_L_parentConstraint1" -p
		 "IKParentConstraintShoulder_L";
	addAttr -ci true -k true -sn "w0" -ln "Clavicle_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 102.58997656731347 -90.000001593056183 0.18318161431039368 ;
	setAttr ".rst" -type "double3" 1.9987898043884655 10.648452901842983 -0.70857706131912912 ;
	setAttr ".rsrr" -type "double3" 77.410023432687183 89.999998406943817 -179.81681522851233 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintHip_L" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "IKOffsetHip_L" -p "IKParentConstraintHip_L";
	setAttr ".t" -type "double3" 0.088558954171565674 -1.1113775190156747 0.041558800165023779 ;
	setAttr ".r" -type "double3" 1.0589532311676104 -0.72669176884003228 -90.175490857607301 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000002 ;
createNode joint -n "IKXHip_L" -p "IKOffsetHip_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -2.6645352591003757e-15 -2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 5.0641238223211608e-09 -4.8386568008108654e-06 -8.0127785884350961e-07 ;
	setAttr ".ro" 2;
createNode joint -n "IKXKnee_L" -p "IKXHip_L";
	setAttr ".t" -type "double3" -0.032864012268980947 -2.5867190732225183 -0.19800989497252439 ;
	setAttr ".r" -type "double3" -9.9707018953817729e-06 -3.6513598721874097e-07 -1.4815687464369003e-06 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 22.919761854393723 0 0 ;
createNode joint -n "IKXAnkle_L" -p "IKXKnee_L";
	setAttr ".t" -type "double3" 0.036411222619631811 -0.88064448175188004 -0.022028406801983536 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 1.4900163328871543 -0.0077691775970408546 0.59745911162008247 ;
createNode joint -n "IKXFoot_End_L" -p "IKXAnkle_L";
	setAttr ".t" -type "double3" -0.066282725496551897 -2.1170624371978901 2.6287438008809803 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 71.642386959019007 179.10051007640067 2.296520474639355e-05 ;
createNode transform -n "IKFKAlignedLeg_L" -p "IKXAnkle_L";
	setAttr ".t" -type "double3" -2.8360158221829579e-08 1.1838832181254588e-07 3.6158620775328382e-07 ;
	setAttr ".r" -type "double3" -7.5522364308345567e-06 2.8009078130590522e-06 -1.2608877877824898e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 0.99999999999999989 ;
createNode orientConstraint -n "IKXAnkle_L_orientConstraint1" -p "IKXAnkle_L";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.776488222810988e-05 -0.89953474838011216 -6.8185868393447897e-07 ;
	setAttr ".o" -type "double3" 180 0.89953576854918638 0 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector8" -p "IKXAnkle_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector7" -p "IKXKnee_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_L" -p "IKXKnee_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 -5.5511151231257827e-17 ;
	setAttr ".r" -type "double3" 1.4902032733471131 -0.90700076186520584 0.57406674410242686 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode annotationShape -n "PoleAnnotationLeg_LShape" -p "PoleAnnotationLeg_L";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintHip_L_parentConstraint1" -p "IKParentConstraintHip_L";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -25.139586592066966 3.3094356804713364e-06 90.000000025576327 ;
	setAttr ".rst" -type "double3" 1.2119851112365725 5.3505174324276403 -1.2426750957965838 ;
	setAttr ".rsrr" -type "double3" -25.13957903290629 0 90.000000000016541 ;
	setAttr -k on ".w0";
createNode transform -n "IKHandle" -p "IKSystem";
createNode transform -n "IKParentConstraintArm_R" -p "IKHandle";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "IKExtraArm_R" -p "IKParentConstraintArm_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKArm_R" -p "IKExtraArm_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow";
createNode nurbsCurve -n "IKArm_RShape" -p "IKArm_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.765538193 -1.235624517 -1.4563898550000001
		1.765538193 -3.2265927090000002 1.8768182870000001
		-1.765538193 -3.2265927090000002 1.8768182870000001
		-1.765538193 0.1066154341 3.8677864799999999
		1.765538193 0.1066154341 3.8677864799999999
		1.765538193 2.097583626 0.53457833659999998
		-1.765538193 2.097583626 0.53457833659999998
		-1.765538193 -1.235624517 -1.4563898550000001
		1.765538193 -1.235624517 -1.4563898550000001
		1.765538193 2.097583626 0.53457833659999998
		-1.765538193 2.097583626 0.53457833659999998
		-1.765538193 0.1066154341 3.8677864799999999
		1.765538193 0.1038431537 3.8567898540000001
		1.765538193 -3.215596084 1.874046007
		-1.765538193 -3.2265927090000002 1.8768182870000001
		-1.765538193 -1.235624517 -1.4563898550000001
		;
createNode ikHandle -n "IKXArmHandle_R" -p "IKArm_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXArmHandle_R_poleVectorConstraint1" -p "IKXArmHandle_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleArm_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -4.2310210696931865 -4.0426183369308637 -3.2509215796840354 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IKParentConstraintArm_R_parentConstraint1" -p "IKParentConstraintArm_R";
	addAttr -ci true -k true -sn "w0" -ln "IKParentConstraintArm_RStaticW0" -dv 1 -min 
		0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "Clavicle_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" 8.8817841970012523e-16 0 0 ;
	setAttr ".tg[1].tot" -type "double3" -1.0739144895948287 3.8923076346922105 -5.1621097173529336 ;
	setAttr ".tg[1].tor" -type "double3" -90.000108608329285 -89.816813673761004 -167.41013204046263 ;
	setAttr ".rst" -type "double3" -6.9193341818034613 6.4581299000311336 0.37777645315521863 ;
	setAttr ".rsrr" -type "double3" 3.1805546814635156e-15 -3.1805546814635183e-15 -3.8166656177562201e-14 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PoleParentConstraintArm_R" -p "IKHandle";
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "PoleExtraArm_R" -p "PoleParentConstraintArm_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleArm_R" -p "PoleExtraArm_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow";
createNode nurbsCurve -n "PoleArm_RShape" -p "PoleArm_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-6.8549987420000002e-33 0.13903589229999999 -3.087216978e-17
		-1.3709997479999999e-32 0.27807178459999998 -6.174433956e-17
		1.852330187e-16 9.2616509340000006e-17 0.41710767700000001
		1.3709997479999999e-32 -0.27807178459999998 6.174433956e-17
		6.8549987420000002e-33 -0.13903589229999999 3.087216978e-17
		-1.852330187e-16 -0.13903589229999999 -0.41710767700000001
		-1.852330187e-16 0.13903589229999999 -0.41710767700000001
		-6.8549987420000002e-33 0.13903589229999999 -3.087216978e-17
		;
createNode transform -n "PoleAnnotateTargetArm_R" -p "PoleArm_R";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetArm_RShape" -p "PoleAnnotateTargetArm_R";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintArm_R_parentConstraint1" -p "PoleParentConstraintArm_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintArm_RStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKArm_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" -1.2969726593783868 -0.2381880902867346 -5.005606048412834 ;
	setAttr ".rst" -type "double3" -8.2163068411818472 6.219941809744399 -4.6278295952576149 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintLeg_R" -p "IKHandle";
	setAttr ".t" -type "double3" -2.3061573502423123 2.1302381350910089 -0.35273977518071464 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1.0000000000000002 ;
createNode transform -n "IKExtraLeg_R" -p "IKParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -5.5511151231257827e-17 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_R" -p "IKExtraLeg_R";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "toe" -ln "toe" -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
createNode nurbsCurve -n "IKLeg_RShape" -p "IKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-1.1329471149999999 -2.1083742540000001 2.6905080770000001
		1.1329471149999999 -2.1083742540000001 2.6905080770000001
		1.1329471149999999 -0.91099059709999997 2.6905080770000001
		1.5293311700000001 -0.91099059709999997 -1.478531781
		1.5293311700000001 -2.1083742540000001 -1.478531781
		-1.5293311700000001 -2.1083742540000001 -1.478531781
		-1.5293311700000001 -0.91099059709999997 -1.478531781
		-1.1329471149999999 -0.91099059709999997 2.6905080770000001
		-1.1329471149999999 -2.1083742540000001 2.6905080770000001
		-1.5293311700000001 -2.1083742540000001 -1.478531781
		-1.5293311700000001 -0.91099059709999997 -1.478531781
		1.5293311700000001 -0.91099059709999997 -1.478531781
		1.523013768 -2.1083742540000001 -1.476848384
		1.1282671070000001 -2.1083742540000001 2.6888246790000001
		1.1329471149999999 -0.91099059709999997 2.6905080770000001
		-1.1329471149999999 -0.91099059709999997 2.6905080770000001
		;
createNode transform -n "IKFootRollLeg_R" -p "IKLeg_R";
	setAttr ".t" -type "double3" 0 0 5.5511151231257827e-17 ;
	setAttr ".r" -type "double3" 0 0.54484839068924973 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "IKRollLegHeel_R" -p "IKFootRollLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.041010910187023075 -1.992840870703245 -1.7590357880910998 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1 1.0000000000000004 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_R" -p "IKRollLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -5.5511151231257827e-17 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 9.9392333795734924e-17 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999933 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_R" -p "IKExtraLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_RShape" -p "IKLegHeel_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		-1.002780287e-16 -0.45161209270000002 -1.3086588599999999e-16
		-5.1353462759999998e-17 -0.31933797320000001 0.31933797320000001
		2.7653265189999997e-17 -1.3607953719999999e-16 0.45161209270000002
		9.0461085430000003e-17 0.31933797320000001 0.31933797320000001
		1.002780287e-16 0.45161209270000002 2.4256173589999999e-16
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKRollLegBall_R" -p "IKLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.041010910187015526 -0.12422156649809855 4.3886151000819975 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_R" -p "IKRollLegBall_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_R" -p "IKExtraLegBall_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 9.9392333795734887e-17 0 ;
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_RShape" -p "IKLegBall_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-9.9600445170000006e-17 0.6017504897 -0.6017504897
		-2.4847825250000002e-16 -1.550498299e-16 -0.85100370380000001
		-3.6683134469999999e-16 -0.6017504897 -0.6017504897
		-3.8533008560000004e-16 -0.85100370380000001 -3.850084044e-16
		-2.9313816359999997e-16 -0.6017504897 0.6017504897
		-1.4426035630000001e-16 -1.9846354150000001e-16 0.85100370380000001
		-2.5907264070000001e-17 0.6017504897 0.6017504897
		-7.4085231850000001e-18 0.85100370380000001 2.027459622e-16
		-9.9600445170000006e-17 0.6017504897 -0.6017504897
		-2.4847825250000002e-16 -1.550498299e-16 -0.85100370380000001
		-3.6683134469999999e-16 -0.6017504897 -0.6017504897
		;
createNode transform -n "IKFootPivotBallReverseLeg_R" -p "IKLegBall_R";
	setAttr ".r" -type "double3" 0 -0.54484839068924995 0 ;
createNode ikHandle -n "IKXLegHandle_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -0.025005340278342999 2.1170624372013434 -2.6294604182242551 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_R_poleVectorConstraint1" -p "IKXLegHandle_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0.48336369402901025 -1.612191283980307 3.9557294299986703 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 5.0181681032768211e-10 9.1199412605158514e-09 9.6776830993405838e-09 ;
	setAttr ".r" -type "double3" 179.99999980147638 -0.89953576856490047 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000004 ;
	setAttr ".hs" 1;
	setAttr ".pv" -type "double3" 8.6736173798840355e-18 0 1 ;
	setAttr ".roc" yes;
createNode transform -n "PoleAimLeg_R" -p "IKHandle";
createNode aimConstraint -n "PoleAimLeg_R_aimConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -sn "w0" -ln "IKLeg_RW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -0.076961365384115854 -15.013528207757631 -89.702908387560242 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_R_pointConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "IKXHip_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -2.3233626302521162 5.4483429006626301 -1.242674976587798 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "PoleExtraLeg_R" -p "PoleParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_R" -p "PoleExtraLeg_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 2.2204460492503131e-16 -4.4408920985006262e-16 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_RShape" -p "PoleLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		-1.6265306409999996e-32 0.32989960689999998 -7.3252427879999994e-17
		2.1975728359999999e-16 1.0987864179999999e-16 0.49484941030000001
		1.6265306409999996e-32 -0.32989960689999998 7.3252427879999994e-17
		8.1326532040000003e-33 -0.16494980340000001 3.6626213939999997e-17
		-2.1975728359999999e-16 -0.16494980340000001 -0.49484941030000001
		-2.1975728359999999e-16 0.16494980340000001 -0.49484941030000001
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		;
createNode transform -n "PoleAnnotateTargetLeg_R" -p "PoleLeg_R";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_RShape" -p "PoleAnnotateTargetLeg_R";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_R_parentConstraint1" -p "PoleParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_RStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" 2.2204460492503131e-16 0 -4.4408920985006262e-16 ;
	setAttr ".tg[1].tot" -type "double3" 2.5842787389390245 0.47042699589921178 3.4030572080413273 ;
	setAttr ".tg[1].tor" -type "double3" 15.013720928144282 1.9463890308300612e-14 89.713049788392382 ;
	setAttr ".lr" -type "double3" -8.5883389601857063e-07 1.6643453495235495e-08 6.2054909722138835e-08 ;
	setAttr ".rst" -type "double3" -1.8399989362231064 3.8361516166823231 2.7130544534108747 ;
	setAttr ".rsrr" -type "double3" 7.951386703658788e-16 -9.5416640443905503e-15 4.4726550208080702e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintSpine0_M" -p "IKHandle";
createNode transform -n "IKExtraSpine0_M" -p "IKParentConstraintSpine0_M";
createNode transform -n "IKSpine0_M" -p "IKExtraSpine0_M";
	addAttr -ci true -k true -sn "stiff" -ln "stiff" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".stiff";
createNode nurbsCurve -n "IKSpine0_MShape" -p "IKSpine0_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.0527618590000001 -0.4195080934 -1.0997260449999999
		-1.0527618590000001 -0.4195080934 -1.0997260449999999
		-1.0527618590000001 0.62807597979999996 -0.99544210160000002
		-1.0527618590000001 0.4195080934 1.0997260449999999
		-1.0527618590000001 -0.62807597979999996 0.99544210160000002
		1.0527618590000001 -0.62807597979999996 0.99544210160000002
		1.0527618590000001 0.4195080934 1.0997260449999999
		1.0527618590000001 0.62807597979999996 -0.99544210160000002
		1.0527618590000001 -0.4195080934 -1.0997260449999999
		1.0527618590000001 -0.62807597979999996 0.99544210160000002
		1.0527618590000001 0.4195080934 1.0997260449999999
		-1.0527618590000001 0.4195080934 1.0997260449999999
		-1.0484130819999999 -0.62764520089999998 0.9911147132
		-1.0484130819999999 -0.41993887229999999 -1.095398656
		-1.0527618590000001 0.62807597979999996 -0.99544210160000002
		1.0527618590000001 0.62807597979999996 -0.99544210160000002
		;
createNode transform -n "IKXSpineLocator0_M" -p "IKSpine0_M";
	setAttr -l on ".v" no;
createNode locator -n "IKXSpineLocator0_MShape" -p "IKXSpineLocator0_M";
	setAttr -k off ".v";
createNode transform -n "IKStiffStartOrientSpine_M" -p "IKSpine0_M";
	setAttr ".t" -type "double3" 3.3087224502121107e-23 3.5527136788005009e-15 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 147.50539744648808 0.00085945462121784025 179.9993998225593 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "IKXSpineLocator1_M" -p "IKStiffStartOrientSpine_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -1.1495253533777561e-16 0.68355643749237061 0 ;
	setAttr ".r" -type "double3" 147.50539745139619 -0.0010473275921610886 179.99995550145005 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
createNode locator -n "IKXSpineLocator1_MShape" -p "IKXSpineLocator1_M";
	setAttr -k off ".v";
createNode transform -n "IKSpine0AlignTo_M" -p "IKSpine0_M";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 29.646584761508983 -1.5861040092409686e-06 2.7472127300940553e-06 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "IKOrientToSpine_M" -p "IKSpine0_M";
	setAttr ".t" -type "double3" 3.3087224502121107e-23 3.5527136788005009e-15 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 30.000000834826057 0 0 ;
createNode parentConstraint -n "IKParentConstraintSpine0_M_parentConstraint1" -p "IKParentConstraintSpine0_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 5.9604644775390592e-08 -3.5527136788005009e-15 
		8.8817841970012523e-16 ;
	setAttr ".tg[0].tor" -type "double3" -1.5902773407317584e-15 0 0 ;
	setAttr ".rst" -type "double3" 5.9604644775390592e-08 5.2923843071224601 -1.2424887120723715 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintSpine2_M" -p "IKHandle";
createNode transform -n "IKExtraSpine2_M" -p "IKParentConstraintSpine2_M";
createNode transform -n "IKSpine2_M" -p "IKExtraSpine2_M";
	addAttr -ci true -k true -sn "stiff" -ln "stiff" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".stiff";
createNode nurbsCurve -n "IKSpine2_MShape" -p "IKSpine2_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.0527618590000001 -0.36610476759999999 -1.11863833
		-1.0527618590000001 -0.36610476759999999 -1.11863833
		-1.0527618590000001 0.67524780340000001 -0.964066812
		-1.0527618590000001 0.36610476759999999 1.11863833
		-1.0527618590000001 -0.67524780340000001 0.964066812
		1.0527618590000001 -0.67524780340000001 0.964066812
		1.0527618590000001 0.36610476759999999 1.11863833
		1.0527618590000001 0.67524780340000001 -0.964066812
		1.0527618590000001 -0.36610476759999999 -1.11863833
		1.0527618590000001 -0.67524780340000001 0.964066812
		1.0527618590000001 0.36610476759999999 1.11863833
		-1.0527618590000001 0.36610476759999999 1.11863833
		-1.0484130819999999 -0.67460929520000001 0.95976516479999996
		-1.0484130819999999 -0.36674327579999999 -1.1143366830000001
		-1.0527618590000001 0.67524780340000001 -0.964066812
		1.0527618590000001 0.67524780340000001 -0.964066812
		;
createNode transform -n "IKXSpineLocator2_M" -p "IKSpine2_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -1.2705494208814505e-21 8.8817841970012523e-16 8.3266726846886741e-17 ;
	setAttr ".r" -type "double3" 1.2722218725854064e-14 -2.7705613045561103e-15 4.3498705459529879e-15 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode locator -n "IKXSpineLocator2_MShape" -p "IKXSpineLocator2_M";
	setAttr -k off ".v";
createNode parentConstraint -n "IKParentConstraintSpine2_M_parentConstraint1" -p "IKParentConstraintSpine2_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.1758246991862295e-06 1.7919476604438191 1.0743605416746054 ;
	setAttr ".tg[0].tor" -type "double3" -1.5902773407317584e-15 0 0 ;
	setAttr ".rst" -type "double3" 1.1758246991862295e-06 7.0843319675662828 -0.16812817039776706 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintSpine4_M" -p "IKHandle";
createNode transform -n "IKExtraSpine4_M" -p "IKParentConstraintSpine4_M";
createNode transform -n "IKSpine4_M" -p "IKExtraSpine4_M";
	addAttr -ci true -k true -sn "stiff" -ln "stiff" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".stiff";
	setAttr -k on ".stretchy";
createNode nurbsCurve -n "IKSpine4_MShape" -p "IKSpine4_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.0527618590000001 1.099629594 -0.41976084800000002
		-1.0527618590000001 1.099629594 -0.41976084800000002
		-1.0527618590000001 0.99558643489999998 0.62784716659999995
		-1.0527618590000001 -1.099629594 0.41976084800000002
		-1.0527618590000001 -0.99558643489999998 -0.62784716659999995
		1.0527618590000001 -0.99558643489999998 -0.62784716659999995
		1.0527618590000001 -1.099629594 0.41976084800000002
		1.0527618590000001 0.99558643489999998 0.62784716659999995
		1.0527618590000001 1.099629594 -0.41976084800000002
		1.0527618590000001 -0.99558643489999998 -0.62784716659999995
		1.0527618590000001 -1.099629594 0.41976084800000002
		-1.0527618590000001 -1.099629594 0.41976084800000002
		-1.0484130819999999 -0.9912589476 -0.62741738229999999
		-1.0484130819999999 1.095302107 -0.42019063220000002
		-1.0527618590000001 0.99558643489999998 0.62784716659999995
		1.0527618590000001 0.99558643489999998 0.62784716659999995
		;
createNode transform -n "IKXSpineLocator4_M" -p "IKSpine4_M";
	setAttr -l on ".v" no;
createNode locator -n "IKXSpineLocator4_MShape" -p "IKXSpineLocator4_M";
	setAttr -k off ".v";
createNode ikHandle -n "IKXSpineHandle_M" -p "IKSpine4_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -2.2464515708012312e-08 0.0044319452700420925 -0.00053408553064743236 ;
	setAttr ".r" -type "double3" 0.19041596045821643 -1.0735335456569162e-05 -1.6939000568727813e-06 ;
	setAttr ".roc" yes;
createNode transform -n "IKStiffEndOrientSpine_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" 0 -4.3844572417128802e-10 3.6173174233766758e-08 ;
	setAttr ".r" -type "double3" -6.8690386157252501 0.004915729940012244 0.00029091773715961017 ;
createNode transform -n "IKXSpineLocator3_M" -p "IKStiffEndOrientSpine_M";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -6.7500998463885235e-06 -0.65993940830230713 2.800682864567694e-05 ;
	setAttr ".r" -type "double3" 6.8690386652796054 -0.0048456515366866702 -0.00087675262164307714 ;
createNode locator -n "IKXSpineLocator3_MShape" -p "IKXSpineLocator3_M";
	setAttr -k off ".v";
createNode transform -n "IKEndJointOrientToSpine_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" -2.0812707431987132e-07 0.0046840451870018285 -0.024098726561123721 ;
	setAttr ".r" -type "double3" -0.35341607331708075 -1.5861040092409681e-06 2.7472127300940553e-06 ;
createNode transform -n "IKSpine4AlignTo_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" -2.2464515708012312e-08 0.0044319452700420925 -0.00053408553064732134 ;
	setAttr ".r" -type "double3" 0.19041596041193895 0 -1.5178069967235354e-06 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 1 ;
createNode transform -n "IKSpine4AlignUnTwistToOffset_M" -p "IKSpine4_M";
	setAttr ".t" -type "double3" -2.2464515708012312e-08 0.0044319452700403161 -0.00053408553064748787 ;
	setAttr ".r" -type "double3" 29.646584761508983 -1.5861040092409686e-06 2.7472127300940553e-06 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999967 0.99999999999999967 ;
createNode transform -n "IKSpine4AlignUnTwistTo_M" -p "IKSpine4AlignUnTwistToOffset_M";
	setAttr ".t" -type "double3" -1.6940658945086011e-21 8.8817841970012523e-16 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 0 0 1.3270308238413698e-21 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode parentConstraint -n "IKParentConstraintSpine4_M_parentConstraint1" -p "IKParentConstraintSpine4_M";
	addAttr -ci true -k true -sn "w0" -ln "Center_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.0013580322265625e-05 3.9045380305134878 
		0.77142570064236515 ;
	setAttr ".tg[0].tor" -type "double3" -1.5902773407317584e-15 0 0 ;
	setAttr ".rst" -type "double3" -1.0013580322265625e-05 9.1969223376359519 -0.47106301143000734 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintArm_L" -p "IKHandle";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "IKExtraArm_L" -p "IKParentConstraintArm_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKArm_L" -p "IKExtraArm_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow";
createNode nurbsCurve -n "IKArm_LShape" -p "IKArm_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-1.765538193 -1.235624517 -1.4563898550000001
		-1.765538193 -3.2265927090000002 1.8768182870000001
		1.765538193 -3.2265927090000002 1.8768182870000001
		1.765538193 0.1066154341 3.8677864799999999
		-1.765538193 0.1066154341 3.8677864799999999
		-1.765538193 2.097583626 0.53457833659999998
		1.765538193 2.097583626 0.53457833659999998
		1.765538193 -1.235624517 -1.4563898550000001
		-1.765538193 -1.235624517 -1.4563898550000001
		-1.765538193 2.097583626 0.53457833659999998
		1.765538193 2.097583626 0.53457833659999998
		1.765538193 0.1066154341 3.8677864799999999
		-1.765538193 0.1038431537 3.8567898540000001
		-1.765538193 -3.215596084 1.874046007
		1.765538193 -3.2265927090000002 1.8768182870000001
		1.765538193 -1.235624517 -1.4563898550000001
		;
createNode ikHandle -n "IKXArmHandle_L" -p "IKArm_L";
	setAttr -l on ".v" no;
	setAttr ".ro" 5;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXArmHandle_L_poleVectorConstraint1" -p "IKXArmHandle_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleArm_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 4.2310210696931403 -4.0426183369309783 -3.2509215796840234 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IKParentConstraintArm_L_parentConstraint1" -p "IKParentConstraintArm_L";
	addAttr -ci true -k true -sn "w0" -ln "IKParentConstraintArm_LStaticW0" -dv 1 -min 
		0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "Clavicle_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" 8.8817841970012523e-16 0 0 ;
	setAttr ".tg[1].tot" -type "double3" 1.073914489594832 -3.8923076346919654 5.1621097173531165 ;
	setAttr ".tg[1].tor" -type "double3" 89.999891391670758 89.816813673761018 167.41013204046055 ;
	setAttr ".rst" -type "double3" 6.9193341818033973 6.4581298892294798 0.37777647387297431 ;
	setAttr ".rsrr" -type "double3" 0 0 -6.9972202992197363e-14 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PoleParentConstraintArm_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "PoleExtraArm_L" -p "PoleParentConstraintArm_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleArm_L" -p "PoleExtraArm_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow";
createNode nurbsCurve -n "PoleArm_LShape" -p "PoleArm_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		1.6164847240000001e-13 0.13903589229999999 2.0428103649999997e-14
		1.6164847240000001e-13 0.27807178459999998 2.0428103649999997e-14
		1.6164847240000001e-13 2.0783375020000002e-13 0.41710767700000001
		1.6164847240000001e-13 -0.27807178459999998 2.0428103649999997e-14
		1.6164847240000001e-13 -0.13903589229999999 2.0428103649999997e-14
		1.6164847240000001e-13 -0.13903589229999999 -0.41710767700000001
		1.6164847240000001e-13 0.13903589229999999 -0.41710767700000001
		1.6164847240000001e-13 0.13903589229999999 2.0428103649999997e-14
		;
createNode transform -n "PoleAnnotateTargetArm_L" -p "PoleArm_L";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetArm_LShape" -p "PoleAnnotateTargetArm_L";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintArm_L_parentConstraint1" -p "PoleParentConstraintArm_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintArm_LStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKArm_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" 1.2969726593783957 -0.2381880902868021 -5.0056060484128224 ;
	setAttr ".rst" -type "double3" 8.2163068411817939 6.2199417989426777 -4.6278295745398479 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintLeg_L" -p "IKHandle";
	setAttr ".t" -type "double3" 2.3061573502389661 2.1302381350900177 -0.35273977518025446 ;
	setAttr ".ro" 3;
createNode transform -n "IKExtraLeg_L" -p "IKParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_L" -p "IKExtraLeg_L";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "toe" -ln "toe" -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
createNode nurbsCurve -n "IKLeg_LShape" -p "IKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		1.1329471149999999 -2.1083742540000001 2.6905080770000001
		-1.1329471149999999 -2.1083742540000001 2.6905080770000001
		-1.1329471149999999 -0.91099059709999997 2.6905080770000001
		-1.5293311700000001 -0.91099059709999997 -1.478531781
		-1.5293311700000001 -2.1083742540000001 -1.478531781
		1.5293311700000001 -2.1083742540000001 -1.478531781
		1.5293311700000001 -0.91099059709999997 -1.478531781
		1.1329471149999999 -0.91099059709999997 2.6905080770000001
		1.1329471149999999 -2.1083742540000001 2.6905080770000001
		1.5293311700000001 -2.1083742540000001 -1.478531781
		1.5293311700000001 -0.91099059709999997 -1.478531781
		-1.5293311700000001 -0.91099059709999997 -1.478531781
		-1.523013768 -2.1083742540000001 -1.476848384
		-1.1282671070000001 -2.1083742540000001 2.6888246790000001
		-1.1329471149999999 -0.91099059709999997 2.6905080770000001
		1.1329471149999999 -0.91099059709999997 2.6905080770000001
		;
createNode transform -n "IKFootRollLeg_L" -p "IKLeg_L";
	setAttr ".r" -type "double3" 0 -0.54484839061633517 0 ;
createNode transform -n "IKRollLegHeel_L" -p "IKFootRollLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.04101091018144265 -1.9928408707022551 -1.7590357880916458 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_L" -p "IKRollLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_L" -p "IKExtraLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_LShape" -p "IKLegHeel_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-4.4408920989999998e-16 0.31933797320000001 -0.31933797320000001
		-4.4408920989999998e-16 -2.2204460489999999e-16 -0.45161209270000002
		-4.4408920989999998e-16 -0.31933797320000001 -0.31933797320000001
		-4.4408920989999998e-16 -0.45161209270000002 8.881784197e-16
		-4.4408920989999998e-16 -0.31933797320000001 0.31933797320000001
		-4.4408920989999998e-16 -3.0531133179999992e-16 0.45161209270000002
		-4.4408920989999998e-16 0.31933797320000001 0.31933797320000001
		-4.4408920989999998e-16 0.45161209270000002 1.3322676300000001e-15
		-4.4408920989999998e-16 0.31933797320000001 -0.31933797320000001
		-4.4408920989999998e-16 -2.2204460489999999e-16 -0.45161209270000002
		-4.4408920989999998e-16 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKRollLegBall_L" -p "IKLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.041010910181424887 -0.124221566498099 4.3886151000820526 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 9.9392333795734924e-17 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_L" -p "IKRollLegBall_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -3.4694469519536142e-18 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 9.939233379573485e-17 0 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000002 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_L" -p "IKExtraLegBall_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_LShape" -p "IKLegBall_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		8.881784197e-16 0.6017504897 -0.6017504897
		1.3322676300000001e-15 2.931682674e-16 -0.85100370380000001
		1.3322676300000001e-15 -0.6017504897 -0.6017504897
		1.3322676300000001e-15 -0.85100370380000001 3.1086244690000001e-15
		1.3322676300000001e-15 -0.6017504897 0.6017504897
		8.881784197e-16 2.4980018050000001e-16 0.85100370380000001
		8.881784197e-16 0.6017504897 0.6017504897
		8.881784197e-16 0.85100370380000001 3.5527136789999999e-15
		8.881784197e-16 0.6017504897 -0.6017504897
		1.3322676300000001e-15 2.931682674e-16 -0.85100370380000001
		1.3322676300000001e-15 -0.6017504897 -0.6017504897
		;
createNode transform -n "IKFootPivotBallReverseLeg_L" -p "IKLegBall_L";
	setAttr ".t" -type "double3" 0 1.7347234759768071e-18 0 ;
	setAttr ".r" -type "double3" 0 0.54484839061633505 0 ;
	setAttr ".s" -type "double3" 0.99999999999999933 1 0.99999999999999956 ;
createNode ikHandle -n "IKXLegHandle_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0.025005340275000343 2.1170624372003544 -2.6294604182237986 ;
	setAttr ".ro" 3;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_L_poleVectorConstraint1" -p "IKXLegHandle_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -0.48336369403150026 -1.6121912839798944 3.9557294299985695 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -5.0793946826388492e-10 9.1189704728295034e-09 9.6781302971749028e-09 ;
	setAttr ".r" -type "double3" 0 0.89953576854918738 0 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "PoleAimLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode aimConstraint -n "PoleAimLeg_L_aimConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -sn "w0" -ln "IKLeg_LW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" 0.076961365399505446 -15.013528207757441 -90.297091612499045 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_L_pointConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "IKXHip_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 2.3233626302522037 5.4483429006616246 -1.2426749765873273 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "PoleExtraLeg_L" -p "PoleParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_L" -p "PoleExtraLeg_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_LShape" -p "PoleLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		7.5563932890000002e-10 0.1649496603 6.3896842479999994e-08
		7.5563932890000002e-10 0.32989946380000001 6.7781709130000004e-08
		7.5563910679999995e-10 -1.5470839499999999e-07 0.49484947029999998
		7.5563932890000002e-10 -0.32989974999999999 5.224224253e-08
		7.5563932890000002e-10 -0.1649499465 5.6127109180000002e-08
		7.5563955089999996e-10 -0.1649499348 -0.49484935419999998
		7.5563955089999996e-10 0.16494967199999999 -0.4948493464
		7.5563932890000002e-10 0.1649496603 6.3896842479999994e-08
		;
createNode transform -n "PoleAnnotateTargetLeg_L" -p "PoleLeg_L";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_LShape" -p "PoleAnnotateTargetLeg_L";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_L_parentConstraint1" -p "PoleParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_LStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" 4.4408920985006262e-16 4.4408920985006262e-16 
		0 ;
	setAttr ".tg[1].tot" -type "double3" 2.584278738939076 -0.47042699589911918 3.4030572080413397 ;
	setAttr ".tg[1].tor" -type "double3" 15.013720928144169 1.1735933860368687e-14 90.286950211664873 ;
	setAttr ".lr" -type "double3" -1.2434465753893636e-06 6.1330002293058948e-10 2.2867082850420964e-09 ;
	setAttr ".rst" -type "double3" 1.8399989362207041 3.8361516166817302 2.7130544534112424 ;
	setAttr ".rsrr" -type "double3" -7.951386703658788e-16 -7.9513867036587919e-15 -5.3671860249696843e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKStatic" -p "IKSystem";
createNode transform -n "IKParentConstraintArm_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -6.9193341818034613 6.4581299000311327 0.37777645315521891 ;
createNode transform -n "PoleParentConstraintArm_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -8.2163068411818472 6.219941809744399 -4.6278295952576149 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "PoleParentConstraintLeg_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -1.8399989362231064 3.8361516166823231 2.7130544534108747 ;
createNode transform -n "IKParentConstraintArm_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 6.9193341818033973 6.4581298892294798 0.37777647387297431 ;
createNode transform -n "PoleParentConstraintArm_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 8.2163068411817939 6.2199417989426777 -4.6278295745398479 ;
createNode transform -n "PoleParentConstraintLeg_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 1.8399989362207032 3.8361516166817302 2.7130544534112424 ;
createNode transform -n "IKCrv" -p "IKSystem";
	setAttr ".it" no;
createNode transform -n "IKXSpineCurve_M" -p "IKCrv";
	setAttr ".ovdt" 1;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 0 0 2.2204460492503131e-16 ;
createNode nurbsCurve -n "IKXSpineCurve_MShape" -p "IKXSpineCurve_M";
	setAttr -k off ".v";
	setAttr -s 5 ".cp";
	setAttr ".cc" -type "nurbsCurve" 
		3 2 0 no 3
		7 0 0 0 2.0431856428340751 4.0863712856681502 4.0863712856681502 4.0863712856681502
		
		5
		5.9604644775390592e-08 5.2923843071224601 -1.2424887120723715
		5.9048616072308776e-07 5.8689245551495901 -0.87526841628005314
		1.1758246991862282e-06 7.0843319675662837 -0.16812817039776698
		-6.6627735758539231e-06 8.5417232468550832 -0.39210618033975131
		-1.0013580322265625e-05 9.1969223376359519 -0.47106301143000728
		;
createNode transform -n "IKMessure" -p "IKSystem";
createNode transform -n "FKIKSystem" -p "MotionSystem";
createNode transform -n "FKIKParentConstraintArm_R" -p "FKIKSystem";
createNode transform -n "FKIKArm_R" -p "FKIKParentConstraintArm_R";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend";
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis";
	setAttr -k on ".IKVis" no;
	setAttr -l on ".startJoint" -type "string" "Shoulder";
	setAttr -l on ".middleJoint" -type "string" "Elbow";
	setAttr -l on ".endJoint" -type "string" "Wrist";
createNode nurbsCurve -n "FKIKArm_RShape" -p "FKIKArm_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-2.8140172790000002 1.0703132849999999 -7.3217968620000005e-17
		-2.8140172790000002 1.1005257900000001 -7.3217968620000005e-17
		-2.783804774 1.1005257900000001 -6.6509445009999992e-17
		-2.75359227 1.1307382939999999 -5.9800921400000003e-17
		-2.75359227 1.493288347 -5.9800921400000003e-17
		-2.783804774 1.523500852 -6.6509445009999992e-17
		-2.8140172790000002 1.523500852 -7.3217968620000005e-17
		-2.8140172790000002 1.553713356 -7.3217968620000005e-17
		-2.2701921989999998 1.553713356 4.7535456349999999e-17
		-2.2701921989999998 1.523500852 4.7535456349999999e-17
		-2.300404704 1.523500852 4.0826932740000003e-17
		-2.3306172080000001 1.493288347 3.4118409129999996e-17
		-2.3306172080000001 1.3422258250000001 3.4118409129999996e-17
		-2.1493421810000002 1.493288347 7.4369550780000009e-17
		-2.1795546859999999 1.523500852 6.7661027169999995e-17
		-2.20976719 1.523500852 6.0952503560000006e-17
		-2.20976719 1.553713356 6.0952503560000006e-17
		-2.0284921640000002 1.553713356 1.012036452e-16
		-2.0284921640000002 1.523500852 1.012036452e-16
		-2.0587046679999998 1.523500852 9.449512161e-17
		-2.2701921989999998 1.3422258250000001 4.7535456349999999e-17
		-2.0284921640000002 1.1005257900000001 1.012036452e-16
		-1.998279659 1.1005257900000001 1.079121688e-16
		-1.8472171369999999 1.553713356 1.414547869e-16
		-1.3940295709999999 1.553713356 2.4208264100000002e-16
		-1.3940295709999999 1.523500852 2.4208264100000002e-16
		-1.424242075 1.523500852 2.3537411740000001e-16
		-1.4544545799999999 1.493288347 2.2866559380000001e-16
		-1.4544545799999999 1.3422258250000001 2.2866559380000001e-16
		-1.2731795530000001 1.493288347 2.6891673539999999e-16
		-1.303392058 1.523500852 2.6220821179999999e-16
		-1.3336045620000001 1.523500852 2.5549968820000003e-16
		-1.3336045620000001 1.553713356 2.5549968820000003e-16
		-1.152329535 1.553713356 2.9575082990000002e-16
		-1.152329535 1.523500852 2.9575082990000002e-16
		-1.18254204 1.523500852 2.8904230630000001e-16
		-1.3940295709999999 1.3422258250000001 2.4208264100000002e-16
		-1.152329535 1.1005257900000001 2.9575082990000002e-16
		-1.1221170309999999 1.1005257900000001 3.0245935350000008e-16
		-1.1221170309999999 1.0703132849999999 3.0245935350000008e-16
		-1.303392058 1.0703132849999999 2.6220821179999999e-16
		-1.303392058 1.1005257900000001 2.6220821179999999e-16
		-1.2731795530000001 1.1005257900000001 2.6891673539999999e-16
		-1.4544545799999999 1.2818008160000001 2.2866559380000001e-16
		-1.4544545799999999 1.1307382939999999 2.2866559380000001e-16
		-1.424242075 1.1005257900000001 2.3537411740000001e-16
		-1.3940295709999999 1.1005257900000001 2.4208264100000002e-16
		-1.3940295709999999 1.0703132849999999 2.4208264100000002e-16
		-1.6055171020000001 1.0703132849999999 1.951229757e-16
		-1.6055171020000001 1.1005257900000001 1.951229757e-16
		-1.5450920930000001 1.1005257900000001 2.0854002299999999e-16
		-1.514879589 1.1307382939999999 2.1524854659999999e-16
		-1.514879589 1.493288347 2.1524854659999999e-16
		-1.5450920930000001 1.523500852 2.0854002299999999e-16
		-1.6357296059999999 1.523500852 1.8841445210000002e-16
		-1.6659421109999999 1.493288347 1.8170592850000001e-16
		-1.6659421109999999 1.1307382939999999 1.8170592850000001e-16
		-1.6357296059999999 1.1005257900000001 1.8841445210000002e-16
		-1.6055171020000001 1.1005257900000001 1.951229757e-16
		-1.6055171020000001 1.0703132849999999 1.951229757e-16
		-1.7867921280000001 1.0703132849999999 1.5487183410000001e-16
		-1.7867921280000001 1.1005257900000001 1.5487183410000001e-16
		-1.756579624 1.1005257900000001 1.6158035770000002e-16
		-1.7263671190000001 1.1307382939999999 1.682888813e-16
		-1.7263671190000001 1.493288347 1.682888813e-16
		-1.756579624 1.523500852 1.6158035770000002e-16
		-1.817004633 1.523500852 1.4816331050000001e-16
		-1.968067155 1.1005257900000001 1.146206924e-16
		-1.968067155 1.0703132849999999 1.146206924e-16
		-2.1795546859999999 1.0703132849999999 6.7661027169999995e-17
		-2.1795546859999999 1.1005257900000001 6.7661027169999995e-17
		-2.1493421810000002 1.1005257900000001 7.4369550780000009e-17
		-2.3306172080000001 1.2818008160000001 3.4118409129999996e-17
		-2.3306172080000001 1.1307382939999999 3.4118409129999996e-17
		-2.300404704 1.1005257900000001 4.0826932740000003e-17
		-2.2701921989999998 1.1005257900000001 4.7535456349999999e-17
		-2.2701921989999998 1.0703132849999999 4.7535456349999999e-17
		-2.4514672260000001 1.0703132849999999 7.2843146899999999e-18
		-2.4514672260000001 1.1005257900000001 7.2843146899999999e-18
		-2.4212547209999999 1.1005257900000001 1.3992838299999998e-17
		-2.3910422169999999 1.1307382939999999 2.0701361909999999e-17
		-2.3910422169999999 1.493288347 2.0701361909999999e-17
		-2.4212547209999999 1.523500852 1.3992838299999998e-17
		-2.4816797300000002 1.523500852 5.7579108129999999e-19
		-2.4816797300000002 1.4328633390000001 5.7579108129999999e-19
		-2.5118922349999999 1.4328633390000001 -6.132732528e-18
		-2.5118922349999999 1.493288347 -6.132732528e-18
		-2.542104739 1.523500852 -1.2841256139999999e-17
		-2.6629547570000001 1.523500852 -3.9675350569999999e-17
		-2.6931672610000001 1.493288347 -4.638387418e-17
		-2.6931672610000001 1.3422258250000001 -4.638387418e-17
		-2.6025297479999998 1.3422258250000001 -2.6258303360000001e-17
		-2.5723172430000001 1.37243833 -1.9549779749999999e-17
		-2.5723172430000001 1.4026508339999999 -1.9549779749999999e-17
		-2.542104739 1.4026508339999999 -1.2841256139999999e-17
		-2.542104739 1.251588312 -1.2841256139999999e-17
		-2.5723172430000001 1.251588312 -1.9549779749999999e-17
		-2.5723172430000001 1.2818008160000001 -1.9549779749999999e-17
		-2.6025297479999998 1.312013321 -2.6258303360000001e-17
		-2.6931672610000001 1.312013321 -4.638387418e-17
		-2.6931672610000001 1.1307382939999999 -4.638387418e-17
		-2.6629547570000001 1.1005257900000001 -3.9675350569999999e-17
		-2.6327422519999999 1.1005257900000001 -3.2966826959999997e-17
		-2.6327422519999999 1.0703132849999999 -3.2966826959999997e-17
		-2.8140172790000002 1.0703132849999999 -7.3217968620000005e-17
		;
createNode parentConstraint -n "FKIKParentConstraintArm_R_parentConstraint1" -p "FKIKParentConstraintArm_R";
	addAttr -ci true -k true -sn "w0" -ln "Clavicle_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0.067774394774033131 3.2865641466956781 -1.3965794309471882 ;
	setAttr ".tg[0].tor" -type "double3" 90.000000355797667 -12.589976567311638 -90.183186403792774 ;
	setAttr ".lr" -type "double3" -6.8817786956680704e-07 -3.0812628118485059e-06 3.4407250486202357e-14 ;
	setAttr ".rst" -type "double3" -5.5109392255615131 10.001878554233912 -0.76584330132998613 ;
	setAttr ".rsrr" -type "double3" 0 9.5416640443905503e-15 0 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintLeg_R" -p "FKIKSystem";
createNode transform -n "FKIKLeg_R" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Hip";
	setAttr -l on ".middleJoint" -type "string" "Knee";
	setAttr -l on ".endJoint" -type "string" "Ankle";
createNode nurbsCurve -n "FKIKLeg_RShape" -p "FKIKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-1.2029688350000001 -0.57010372590000002 -2.4639494449999998e-16
		-1.2029688350000001 -0.54772876000000004 -2.4639494449999998e-16
		-1.180593869 -0.54772876000000004 -2.4142670399999999e-16
		-1.1582189030000001 -0.52535379400000004 -2.364584635e-16
		-1.1582189030000001 -0.25685420289999999 -2.364584635e-16
		-1.180593869 -0.23447923700000001 -2.4142670399999999e-16
		-1.2029688350000001 -0.23447923700000001 -2.4639494449999998e-16
		-1.2029688350000001 -0.21210427109999999 -2.4639494449999998e-16
		-0.80021944820000002 -0.21210427109999999 -1.56966616e-16
		-0.80021944820000002 -0.23447923700000001 -1.56966616e-16
		-0.82259441420000001 -0.23447923700000001 -1.6193485649999999e-16
		-0.84496937999999999 -0.25685420289999999 -1.6690309699999999e-16
		-0.84496937999999999 -0.36872903260000001 -1.6690309699999999e-16
		-0.71071958449999995 -0.25685420289999999 -1.3709365410000001e-16
		-0.73309455040000004 -0.23447923700000001 -1.4206189460000001e-16
		-0.75546951640000004 -0.23447923700000001 -1.470301351e-16
		-0.75546951640000004 -0.21210427109999999 -1.470301351e-16
		-0.62121972079999999 -0.21210427109999999 -1.172206923e-16
		-0.62121972079999999 -0.23447923700000001 -1.172206923e-16
		-0.64359468669999997 -0.23447923700000001 -1.2218893269999999e-16
		-0.80021944820000002 -0.36872903260000001 -1.56966616e-16
		-0.62121972079999999 -0.54772876000000004 -1.172206923e-16
		-0.5988447549 -0.54772876000000004 -1.1225245180000001e-16
		-0.48696992519999999 -0.21210427109999999 -8.7411249449999998e-17
		-0.15134543640000001 -0.21210427109999999 -1.288764241e-17
		-0.15134543640000001 -0.23447923700000001 -1.288764241e-17
		-0.1737204024 -0.23447923700000001 -1.785588288e-17
		-0.19609536820000001 -0.25685420289999999 -2.282412336e-17
		-0.19609536820000001 -0.36872903260000001 -2.282412336e-17
		-0.061845572629999998 -0.25685420289999999 6.9853194729999997e-18
		-0.084220538619999993 -0.23447923700000001 2.0170790009999999e-18
		-0.1065955044 -0.23447923700000001 -2.95116147e-18
		-0.1065955044 -0.21210427109999999 -2.95116147e-18
		0.027654291120000001 -0.21210427109999999 2.6858281339999999e-17
		0.027654291120000001 -0.23447923700000001 2.6858281339999999e-17
		0.005279325136 -0.23447923700000001 2.1890040869999997e-17
		-0.15134543640000001 -0.36872903260000001 -1.288764241e-17
		0.027654291120000001 -0.54772876000000004 2.6858281339999999e-17
		0.05002925691 -0.54772876000000004 3.1826521810000002e-17
		0.05002925691 -0.57010372590000002 3.1826521810000002e-17
		-0.084220538619999993 -0.57010372590000002 2.0170790009999999e-18
		-0.084220538619999993 -0.54772876000000004 2.0170790009999999e-18
		-0.061845572629999998 -0.54772876000000004 6.9853194729999997e-18
		-0.19609536820000001 -0.41347896439999998 -2.282412336e-17
		-0.19609536820000001 -0.52535379400000004 -2.282412336e-17
		-0.1737204024 -0.54772876000000004 -1.785588288e-17
		-0.15134543640000001 -0.54772876000000004 -1.288764241e-17
		-0.15134543640000001 -0.57010372590000002 -1.288764241e-17
		-0.30797019790000002 -0.57010372590000002 -4.7665325689999997e-17
		-0.30797019790000002 -0.54772876000000004 -4.7665325689999997e-17
		-0.26322026589999997 -0.54772876000000004 -3.7728844749999998e-17
		-0.24084530009999999 -0.52535379400000004 -3.2760604299999996e-17
		-0.24084530009999999 -0.25685420289999999 -3.2760604299999996e-17
		-0.26322026589999997 -0.23447923700000001 -3.7728844749999998e-17
		-0.3303451637 -0.23447923700000001 -5.2633566160000003e-17
		-0.3527201297 -0.25685420289999999 -5.7601806639999997e-17
		-0.3527201297 -0.52535379400000004 -5.7601806639999997e-17
		-0.3303451637 -0.54772876000000004 -5.2633566160000003e-17
		-0.30797019790000002 -0.54772876000000004 -4.7665325689999997e-17
		-0.30797019790000002 -0.57010372590000002 -4.7665325689999997e-17
		-0.44221999340000001 -0.57010372590000002 -7.7474768520000007e-17
		-0.44221999340000001 -0.54772876000000004 -7.7474768520000007e-17
		-0.41984502750000002 -0.54772876000000004 -7.2506528050000001e-17
		-0.39747006159999998 -0.52535379400000004 -6.7538287579999996e-17
		-0.39747006159999998 -0.25685420289999999 -6.7538287579999996e-17
		-0.41984502750000002 -0.23447923700000001 -7.2506528050000001e-17
		-0.4645949593 -0.23447923700000001 -8.2443008990000025e-17
		-0.57646978900000001 -0.54772876000000004 -1.0728421130000001e-16
		-0.57646978900000001 -0.57010372590000002 -1.0728421130000001e-16
		-0.73309455040000004 -0.57010372590000002 -1.4206189460000001e-16
		-0.73309455040000004 -0.54772876000000004 -1.4206189460000001e-16
		-0.71071958449999995 -0.54772876000000004 -1.3709365410000001e-16
		-0.84496937999999999 -0.41347896439999998 -1.6690309699999999e-16
		-0.84496937999999999 -0.52535379400000004 -1.6690309699999999e-16
		-0.82259441420000001 -0.54772876000000004 -1.6193485649999999e-16
		-0.80021944820000002 -0.54772876000000004 -1.56966616e-16
		-0.80021944820000002 -0.57010372590000002 -1.56966616e-16
		-0.93446924379999996 -0.57010372590000002 -1.8677605880000002e-16
		-0.93446924379999996 -0.54772876000000004 -1.8677605880000002e-16
		-0.91209427779999996 -0.54772876000000004 -1.8180781840000001e-16
		-0.88971931199999998 -0.52535379400000004 -1.7683957789999999e-16
		-0.88971931199999998 -0.25685420289999999 -1.7683957789999999e-16
		-0.91209427779999996 -0.23447923700000001 -1.8180781840000001e-16
		-0.95684420979999996 -0.23447923700000001 -1.9174429930000001e-16
		-0.95684420979999996 -0.30160413479999998 -1.9174429930000001e-16
		-0.97921917550000004 -0.30160413479999998 -1.967125398e-16
		-0.97921917550000004 -0.25685420289999999 -1.967125398e-16
		-1.0015941420000001 -0.23447923700000001 -2.0168078020000002e-16
		-1.091094005 -0.23447923700000001 -2.215537421e-16
		-1.1134689710000001 -0.25685420289999999 -2.2652198260000002e-16
		-1.1134689710000001 -0.36872903260000001 -2.2652198260000002e-16
		-1.0463440740000001 -0.36872903260000001 -2.1161726120000002e-16
		-1.023969108 -0.34635406660000001 -2.0664902070000001e-16
		-1.023969108 -0.32397910070000002 -2.0664902070000001e-16
		-1.0015941420000001 -0.32397910070000002 -2.0168078020000002e-16
		-1.0015941420000001 -0.43585393030000003 -2.0168078020000002e-16
		-1.023969108 -0.43585393030000003 -2.0664902070000001e-16
		-1.023969108 -0.41347896439999998 -2.0664902070000001e-16
		-1.0463440740000001 -0.39110399849999999 -2.1161726120000002e-16
		-1.1134689710000001 -0.39110399849999999 -2.2652198260000002e-16
		-1.1134689710000001 -0.52535379400000004 -2.2652198260000002e-16
		-1.091094005 -0.54772876000000004 -2.215537421e-16
		-1.0687190390000001 -0.54772876000000004 -2.1658550170000002e-16
		-1.0687190390000001 -0.57010372590000002 -2.1658550170000002e-16
		-1.2029688350000001 -0.57010372590000002 -2.4639494449999998e-16
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_R_parentConstraint1" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0.78774362489707084 2.1618775422330216 -0.03317857677254521 ;
	setAttr ".tg[0].tor" -type "double3" -154.86042096709372 7.2743588791181185e-11 
		-90.000000000050676 ;
	setAttr ".lr" -type "double3" -1.0390807752269323e-05 -1.2522175532589016e-06 2.1689875744190974e-06 ;
	setAttr ".rst" -type "double3" -3.3738626534702485 4.6514874717918948 -0.87798625492894034 ;
	setAttr ".rsrr" -type "double3" -1.5902773425087834e-14 6.5849874016674115e-11 -3.0923678843152648e-11 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintSpine_M" -p "FKIKSystem";
createNode transform -n "FKIKSpine_M" -p "FKIKParentConstraintSpine_M";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend";
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis";
	setAttr -k on ".IKVis" no;
	setAttr -l on ".startJoint" -type "string" "Pelvis";
	setAttr -l on ".middleJoint" -type "string" "SpineA";
	setAttr -l on ".endJoint" -type "string" "Chest";
createNode nurbsCurve -n "FKIKSpine_MShape" -p "FKIKSpine_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		2.6197255510000002 1.2819547039999999 -1.1468663610000001e-16
		2.6197255510000002 1.300401229 -1.1468663610000001e-16
		2.638172076 1.300401229 -1.1059068480000001e-16
		2.6566185999999998 1.318847753 -1.064947335e-16
		2.6566185999999998 1.5402060500000001 -1.064947335e-16
		2.638172076 1.558652575 -1.1059068480000001e-16
		2.6197255510000002 1.558652575 -1.1468663610000001e-16
		2.6197255510000002 1.577099099 -1.1468663610000001e-16
		2.9517629950000002 1.577099099 -4.0959512880000004e-17
		2.9517629950000002 1.558652575 -4.0959512880000004e-17
		2.9333164709999999 1.558652575 -4.5055464169999988e-17
		2.914869946 1.5402060500000001 -4.9151415460000003e-17
		2.914869946 1.4479734259999999 -4.9151415460000003e-17
		3.025549094 1.5402060500000001 -2.457570772e-17
		3.0071025699999998 1.558652575 -2.8671659009999997e-17
		2.9886560449999999 1.558652575 -3.2767610299999999e-17
		2.9886560449999999 1.577099099 -3.2767610299999999e-17
		3.0993351929999999 1.577099099 -8.191902577000001e-18
		3.0993351929999999 1.558652575 -8.191902577000001e-18
		3.0808886680000001 1.558652575 -1.228785386e-17
		2.9517629950000002 1.4479734259999999 -4.0959512880000004e-17
		3.0993351929999999 1.300401229 -8.191902577000001e-18
		3.1177817179999998 1.300401229 -4.0959512880000013e-18
		3.2100143409999999 1.577099099 1.638380515e-17
		3.486712212 1.577099099 7.7823074480000001e-17
		3.486712212 1.558652575 7.7823074480000001e-17
		3.4682656870000002 1.558652575 7.3727123189999999e-17
		3.4498191619999998 1.5402060500000001 6.9631171899999996e-17
		3.4498191619999998 1.4479734259999999 6.9631171899999996e-17
		3.5604983099999998 1.5402060500000001 9.4206879640000011e-17
		3.542051786 1.558652575 9.0110928349999996e-17
		3.5236052610000002 1.558652575 8.6014977059999994e-17
		3.5236052610000002 1.577099099 8.6014977059999994e-17
		3.6342844090000002 1.577099099 1.1059068480000001e-16
		3.6342844090000002 1.558652575 1.1059068480000001e-16
		3.6158378839999998 1.558652575 1.064947335e-16
		3.486712212 1.4479734259999999 7.7823074480000001e-17
		3.6342844090000002 1.300401229 1.1059068480000001e-16
		3.652730934 1.300401229 1.1468663610000001e-16
		3.652730934 1.2819547039999999 1.1468663610000001e-16
		3.542051786 1.2819547039999999 9.0110928349999996e-17
		3.542051786 1.300401229 9.0110928349999996e-17
		3.5604983099999998 1.300401229 9.4206879640000011e-17
		3.4498191619999998 1.411080377 6.9631171899999996e-17
		3.4498191619999998 1.318847753 6.9631171899999996e-17
		3.4682656870000002 1.300401229 7.3727123189999999e-17
		3.486712212 1.300401229 7.7823074480000001e-17
		3.486712212 1.2819547039999999 7.7823074480000001e-17
		3.3575865390000001 1.2819547039999999 4.9151415460000003e-17
		3.3575865390000001 1.300401229 4.9151415460000003e-17
		3.3944795879999998 1.300401229 5.7343318039999996e-17
		3.4129261130000002 1.318847753 6.1439269320000003e-17
		3.4129261130000002 1.5402060500000001 6.1439269320000003e-17
		3.3944795879999998 1.558652575 5.7343318039999996e-17
		3.3391400139999998 1.558652575 4.5055464169999988e-17
		3.3206934889999999 1.5402060500000001 4.0959512880000004e-17
		3.3206934889999999 1.318847753 4.0959512880000004e-17
		3.3391400139999998 1.300401229 4.5055464169999988e-17
		3.3575865390000001 1.300401229 4.9151415460000003e-17
		3.3575865390000001 1.2819547039999999 4.9151415460000003e-17
		3.2469073910000001 1.2819547039999999 2.457570772e-17
		3.2469073910000001 1.300401229 2.457570772e-17
		3.2653539149999999 1.300401229 2.8671659009999997e-17
		3.2838004399999998 1.318847753 3.2767610299999999e-17
		3.2838004399999998 1.5402060500000001 3.2767610299999999e-17
		3.2653539149999999 1.558652575 2.8671659009999997e-17
		3.2284608659999998 1.558652575 2.0479756430000001e-17
		3.1362282420000001 1.300401229 0
		3.1362282420000001 1.2819547039999999 0
		3.0071025699999998 1.2819547039999999 -2.8671659009999997e-17
		3.0071025699999998 1.300401229 -2.8671659009999997e-17
		3.025549094 1.300401229 -2.457570772e-17
		2.914869946 1.411080377 -4.9151415460000003e-17
		2.914869946 1.318847753 -4.9151415460000003e-17
		2.9333164709999999 1.300401229 -4.5055464169999988e-17
		2.9517629950000002 1.300401229 -4.0959512880000004e-17
		2.9517629950000002 1.2819547039999999 -4.0959512880000004e-17
		2.8410838470000002 1.2819547039999999 -6.5535220609999993e-17
		2.8410838470000002 1.300401229 -6.5535220609999993e-17
		2.859530372 1.300401229 -6.1439269320000003e-17
		2.8779768969999999 1.318847753 -5.7343318039999996e-17
		2.8779768969999999 1.5402060500000001 -5.7343318039999996e-17
		2.859530372 1.558652575 -6.1439269320000003e-17
		2.8226373229999999 1.558652575 -6.9631171899999996e-17
		2.8226373229999999 1.5033129999999999 -6.9631171899999996e-17
		2.804190798 1.5033129999999999 -7.3727123189999999e-17
		2.804190798 1.5402060500000001 -7.3727123189999999e-17
		2.7857442730000002 1.558652575 -7.7823074480000001e-17
		2.7119581739999998 1.558652575 -9.4206879640000011e-17
		2.69351165 1.5402060500000001 -9.8302830930000002e-17
		2.69351165 1.4479734259999999 -9.8302830930000002e-17
		2.748851224 1.4479734259999999 -8.6014977059999994e-17
		2.7672977479999998 1.466419951 -8.1919025770000004e-17
		2.7672977479999998 1.4848664760000001 -8.1919025770000004e-17
		2.7857442730000002 1.4848664760000001 -7.7823074480000001e-17
		2.7857442730000002 1.3926338519999999 -7.7823074480000001e-17
		2.7672977479999998 1.3926338519999999 -8.1919025770000004e-17
		2.7672977479999998 1.411080377 -8.1919025770000004e-17
		2.748851224 1.4295269020000001 -8.6014977059999994e-17
		2.69351165 1.4295269020000001 -9.8302830930000002e-17
		2.69351165 1.318847753 -9.8302830930000002e-17
		2.7119581739999998 1.300401229 -9.4206879640000011e-17
		2.7304046990000002 1.300401229 -9.0110928349999996e-17
		2.7304046990000002 1.2819547039999999 -9.0110928349999996e-17
		2.6197255510000002 1.2819547039999999 -1.1468663610000001e-16
		;
createNode parentConstraint -n "FKIKParentConstraintSpine_M_parentConstraint1" -p
		 "FKIKParentConstraintSpine_M";
	addAttr -ci true -k true -sn "w0" -ln "FKPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.0648427662719278 0.94267827261617387 -0.32171833014540807 ;
	setAttr ".tg[0].tor" -type "double3" -30.000000834826057 0 0 ;
	setAttr ".rst" -type "double3" 1.0648428258765723 6.2696268010683447 -1.0497658082944421 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintArm_L" -p "FKIKSystem";
createNode transform -n "FKIKArm_L" -p "FKIKParentConstraintArm_L";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend";
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis";
	setAttr -k on ".IKVis" no;
	setAttr -l on ".startJoint" -type "string" "Shoulder";
	setAttr -l on ".middleJoint" -type "string" "Elbow";
	setAttr -l on ".endJoint" -type "string" "Wrist";
createNode nurbsCurve -n "FKIKArm_LShape" -p "FKIKArm_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		2.8140172790000002 1.0703132849999999 -4.5408121709999998e-14
		2.8140172790000002 1.1005257900000001 -4.5408121709999998e-14
		2.783804774 1.1005257900000001 -4.5186077100000004e-14
		2.75359227 1.1307382939999999 -4.4964032500000001e-14
		2.75359227 1.493288347 -4.4964032500000001e-14
		2.783804774 1.523500852 -4.5186077100000004e-14
		2.8140172790000002 1.523500852 -4.5408121709999998e-14
		2.8140172790000002 1.553713356 -4.5408121709999998e-14
		2.2701921989999998 1.553713356 -4.096722961e-14
		2.2701921989999998 1.523500852 -4.096722961e-14
		2.300404704 1.523500852 -4.1189274210000002e-14
		2.3306172080000001 1.493288347 -4.1522341120000004e-14
		2.3306172080000001 1.3422258250000001 -4.1522341120000004e-14
		2.1493421810000002 1.493288347 -3.9968028889999999e-14
		2.1795546859999999 1.523500852 -4.0190073489999989e-14
		2.20976719 1.523500852 -4.0412118100000002e-14
		2.20976719 1.553713356 -4.0412118100000002e-14
		2.0284921640000002 1.553713356 -3.896882816e-14
		2.0284921640000002 1.523500852 -3.896882816e-14
		2.0587046679999998 1.523500852 -3.919087277e-14
		2.2701921989999998 1.3422258250000001 -4.096722961e-14
		2.0284921640000002 1.1005257900000001 -3.896882816e-14
		1.998279659 1.1005257900000001 -3.8635761259999996e-14
		1.8472171369999999 1.553713356 -3.7525538230000002e-14
		1.3940295709999999 1.553713356 -3.3750779950000001e-14
		1.3940295709999999 1.523500852 -3.3750779950000001e-14
		1.424242075 1.523500852 -3.4083846860000003e-14
		1.4544545799999999 1.493288347 -3.4194869159999998e-14
		1.4544545799999999 1.3422258250000001 -3.4194869159999998e-14
		1.2731795530000001 1.493288347 -3.2751579230000001e-14
		1.303392058 1.523500852 -3.2973623829999997e-14
		1.3336045620000001 1.523500852 -3.3306690739999998e-14
		1.3336045620000001 1.553713356 -3.3306690739999998e-14
		1.152329535 1.553713356 -3.1752378499999995e-14
		1.152329535 1.523500852 -3.1752378499999995e-14
		1.18254204 1.523500852 -3.1974423110000002e-14
		1.3940295709999999 1.3422258250000001 -3.3750779950000001e-14
		1.152329535 1.1005257900000001 -3.1752378499999995e-14
		1.1221170309999999 1.1005257900000001 -3.1530333899999999e-14
		1.1221170309999999 1.0703132849999999 -3.1530333899999999e-14
		1.303392058 1.0703132849999999 -3.2973623829999997e-14
		1.303392058 1.1005257900000001 -3.2973623829999997e-14
		1.2731795530000001 1.1005257900000001 -3.2751579230000001e-14
		1.4544545799999999 1.2818008160000001 -3.4194869159999998e-14
		1.4544545799999999 1.1307382939999999 -3.4194869159999998e-14
		1.424242075 1.1005257900000001 -3.4083846860000003e-14
		1.3940295709999999 1.1005257900000001 -3.3750779950000001e-14
		1.3940295709999999 1.0703132849999999 -3.3750779950000001e-14
		1.6055171020000001 1.0703132849999999 -3.541611449e-14
		1.6055171020000001 1.1005257900000001 -3.541611449e-14
		1.5450920930000001 1.1005257900000001 -3.4972025279999997e-14
		1.514879589 1.1307382939999999 -3.4749980670000002e-14
		1.514879589 1.493288347 -3.4749980670000002e-14
		1.5450920930000001 1.523500852 -3.4972025279999997e-14
		1.6357296059999999 1.523500852 -3.5749181390000003e-14
		1.6659421109999999 1.493288347 -3.5971226000000004e-14
		1.6659421109999999 1.1307382939999999 -3.5971226000000004e-14
		1.6357296059999999 1.1005257900000001 -3.5749181390000003e-14
		1.6055171020000001 1.1005257900000001 -3.541611449e-14
		1.6055171020000001 1.0703132849999999 -3.541611449e-14
		1.7867921280000001 1.0703132849999999 -3.6970426719999998e-14
		1.7867921280000001 1.1005257900000001 -3.6970426719999998e-14
		1.756579624 1.1005257900000001 -3.6748382120000002e-14
		1.7263671190000001 1.1307382939999999 -3.6526337510000001e-14
		1.7263671190000001 1.493288347 -3.6526337510000001e-14
		1.756579624 1.523500852 -3.6748382120000002e-14
		1.817004633 1.523500852 -3.730349363e-14
		1.968067155 1.1005257900000001 -3.8413716650000002e-14
		1.968067155 1.0703132849999999 -3.8413716650000002e-14
		2.1795546859999999 1.0703132849999999 -4.0190073489999989e-14
		2.1795546859999999 1.1005257900000001 -4.0190073489999989e-14
		2.1493421810000002 1.1005257900000001 -3.9968028889999999e-14
		2.3306172080000001 1.2818008160000001 -4.1522341120000004e-14
		2.3306172080000001 1.1307382939999999 -4.1522341120000004e-14
		2.300404704 1.1005257900000001 -4.1189274210000002e-14
		2.2701921989999998 1.1005257900000001 -4.096722961e-14
		2.2701921989999998 1.0703132849999999 -4.096722961e-14
		2.4514672260000001 1.0703132849999999 -4.2410519539999997e-14
		2.4514672260000001 1.1005257900000001 -4.2410519539999997e-14
		2.4212547209999999 1.1005257900000001 -4.2188474940000001e-14
		2.3910422169999999 1.1307382939999999 -4.1855408030000012e-14
		2.3910422169999999 1.493288347 -4.1855408030000012e-14
		2.4212547209999999 1.523500852 -4.2188474940000001e-14
		2.4816797300000002 1.523500852 -4.2632564149999998e-14
		2.4816797300000002 1.4328633390000001 -4.2632564149999998e-14
		2.5118922349999999 1.4328633390000001 -4.285460875e-14
		2.5118922349999999 1.493288347 -4.285460875e-14
		2.542104739 1.523500852 -4.3187675660000002e-14
		2.6629547570000001 1.523500852 -4.4186876379999997e-14
		2.6931672610000001 1.493288347 -4.4408920989999997e-14
		2.6931672610000001 1.3422258250000001 -4.4408920989999997e-14
		2.6025297479999998 1.3422258250000001 -4.3631764869999999e-14
		2.5723172430000001 1.37243833 -4.3409720260000004e-14
		2.5723172430000001 1.4026508339999999 -4.3409720260000004e-14
		2.542104739 1.4026508339999999 -4.3187675660000002e-14
		2.542104739 1.251588312 -4.3187675660000002e-14
		2.5723172430000001 1.251588312 -4.3409720260000004e-14
		2.5723172430000001 1.2818008160000001 -4.3409720260000004e-14
		2.6025297479999998 1.312013321 -4.3631764869999999e-14
		2.6931672610000001 1.312013321 -4.4408920989999997e-14
		2.6931672610000001 1.1307382939999999 -4.4408920989999997e-14
		2.6629547570000001 1.1005257900000001 -4.4186876379999997e-14
		2.6327422519999999 1.1005257900000001 -4.3964831780000007e-14
		2.6327422519999999 1.0703132849999999 -4.3964831780000007e-14
		2.8140172790000002 1.0703132849999999 -4.5408121709999998e-14
		;
createNode parentConstraint -n "FKIKParentConstraintArm_L_parentConstraint1" -p "FKIKParentConstraintArm_L";
	addAttr -ci true -k true -sn "w0" -ln "Clavicle_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -0.067774394774035573 -3.2865641466956124 1.3965794309473407 ;
	setAttr ".tg[0].tor" -type "double3" -89.999999644202362 -12.58997656731346 -90.18318640379276 ;
	setAttr ".lr" -type "double3" -6.8817784412236961e-07 3.0812626114735609e-06 -3.4407248598675924e-14 ;
	setAttr ".rst" -type "double3" 5.5109392255615024 10.00187854343228 -0.76584328061223517 ;
	setAttr ".rsrr" -type "double3" 3.1805546814635168e-15 -8.8278125961003172e-32 3.1805546814635168e-15 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintLeg_L" -p "FKIKSystem";
createNode transform -n "FKIKLeg_L" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Hip";
	setAttr -l on ".middleJoint" -type "string" "Knee";
	setAttr -l on ".endJoint" -type "string" "Ankle";
createNode nurbsCurve -n "FKIKLeg_LShape" -p "FKIKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		1.2029687499999999 -0.57010383119999997 4.0494779779999989e-07
		1.202968751 -0.54772886529999998 4.031835421000001e-07
		1.1805937849999999 -0.54772886450000002 4.0139788819999999e-07
		1.1582188200000001 -0.52535389769999996 3.978479785e-07
		1.1582188289999999 -0.25685430669999998 3.7667691030000003e-07
		1.1805937959999999 -0.23447934149999999 3.766983088e-07
		1.202968762 -0.2344793423 3.784839627e-07
		1.2029687630000001 -0.21210437639999999 3.76719707e-07
		0.80021937600000004 -0.21210436199999999 3.4457793489999998e-07
		0.80021937519999997 -0.23447932790000001 3.4634219060000004e-07
		0.82259434119999997 -0.23447932869999999 3.4812784470000004e-07
		0.84496930619999999 -0.25685429539999999 3.516777543e-07
		0.84496930219999999 -0.36872912499999999 3.6049903259999998e-07
		0.71071951069999995 -0.25685429059999998 3.4096383019999996e-07
		0.7330944774 -0.2344793255 3.4098522860000001e-07
		0.75546944329999999 -0.23447932630000001 3.4277088259999999e-07
		0.75546944410000005 -0.2121043604 3.4100662680000001e-07
		0.62121964860000001 -0.21210435559999999 3.3029270290000002e-07
		0.62121964780000005 -0.2344793215 3.320569585e-07
		0.64359461370000004 -0.23447932229999999 3.338426126e-07
		0.80021937040000002 -0.36872912340000003 3.569277247e-07
		0.62121963650000001 -0.54772884440000003 3.5675653800000001e-07
		0.59884467060000002 -0.54772884359999996 3.5497088390000002e-07
		0.48696985300000001 -0.2121043507 3.195787789e-07
		0.15134536409999999 -0.21210433870000001 2.927939687e-07
		0.15134536330000001 -0.2344793047 2.9455822440000001e-07
		0.1737203293 -0.23447930550000001 2.9634387839999998e-07
		0.1960952943 -0.25685427220000001 2.99893788e-07
		0.1960952903 -0.36872910180000001 3.087150664e-07
		0.061845498780000001 -0.2568542674 2.891798641e-07
		0.08422046557 -0.23447930219999999 2.8920126239999992e-07
		0.1065954314 -0.23447930310000001 2.9098691630000003e-07
		0.10659543220000001 -0.21210433710000001 2.892226607e-07
		-0.027654363380000001 -0.2121043323 2.7850873660000001e-07
		-0.027654364179999998 -0.23447929819999999 2.8027299229999997e-07
		-0.0052793981880000004 -0.234479299 2.820586463e-07
		0.1513453585 -0.36872910019999999 3.051437584e-07
		-0.0276543754 -0.54772882119999999 3.0497257179999998e-07
		-0.050029341179999999 -0.54772882040000004 3.031869178e-07
		-0.050029341989999997 -0.57010378630000003 3.0495117340000004e-07
		0.084220453540000004 -0.57010379109999998 3.1566509749999997e-07
		0.084220454340000001 -0.54772882519999999 3.1390084180000002e-07
		0.061845488359999999 -0.54772882440000004 3.1211518779999999e-07
		0.19609528870000001 -0.4134790337 3.1224357779999996e-07
		0.19609528470000001 -0.5253538633 3.210648561e-07
		0.17372031809999999 -0.54772882840000003 3.2104345780000003e-07
		0.1513453521 -0.54772882759999997 3.1925780370000003e-07
		0.15134535129999999 -0.57010379349999996 3.2102205960000008e-07
		0.30797011279999997 -0.57010379909999997 3.335216375e-07
		0.30797011359999998 -0.54772883319999999 3.3175738190000001e-07
		0.26322018159999999 -0.54772883159999997 3.2818607390000001e-07
		0.2408452167 -0.52535386490000002 3.2463616420000008e-07
		0.24084522629999999 -0.25685427379999998 3.0346509610000002e-07
		0.26322019289999998 -0.2344793087 3.0348649449999996e-07
		0.3303450906 -0.2344793111 3.088434565e-07
		0.35272005579999999 -0.25685427779999997 3.1239336620000003e-07
		0.35272004620000003 -0.52535386890000002 3.3356443429999998e-07
		0.33034507940000002 -0.54772883400000005 3.3354303589999999e-07
		0.30797011359999998 -0.54772883319999999 3.3175738190000001e-07
		0.30797011279999997 -0.57010379909999997 3.335216375e-07
		0.44221990830000002 -0.57010380400000005 3.4423556159999998e-07
		0.44221990909999997 -0.54772883800000005 3.4247130599999999e-07
		0.41984494319999999 -0.54772883719999998 3.406856519e-07
		0.39746997810000001 -0.52535387050000004 3.3713574220000002e-07
		0.39746998770000003 -0.2568542794 3.1596467419999998e-07
		0.41984495440000003 -0.23447931429999999 3.1598607239999998e-07
		0.46459488630000001 -0.23447931590000001 3.1955738039999999e-07
		0.57646970470000003 -0.5477288428 3.5318523000000001e-07
		0.57646970389999996 -0.5701038088 3.5494948570000002e-07
		0.73309446540000001 -0.57010381440000002 3.6744906370000001e-07
		0.73309446619999996 -0.54772884840000002 3.65684808e-07
		0.71071950019999997 -0.54772884759999996 3.6389915400000002e-07
		0.84496930049999996 -0.41347905689999997 3.64027544e-07
		0.84496929649999997 -0.52535388650000003 3.7284882250000002e-07
		0.82259433000000004 -0.54772885169999996 3.7282742400000001e-07
		0.80021936400000004 -0.54772885090000001 3.710417701e-07
		0.80021936319999998 -0.5701038168 3.7280602569999999e-07
		0.93446915870000002 -0.57010382159999995 3.8351994980000002e-07
		0.93446915949999998 -0.54772885569999996 3.8175569419999999e-07
		0.91209419349999998 -0.54772885490000001 3.7997004009999999e-07
		0.88971922849999996 -0.52535388810000005 3.7642013040000001e-07
		0.88971923809999998 -0.25685429700000001 3.5524906230000001e-07
		0.91209420470000002 -0.2344793319 3.5527046059999998e-07
		0.95684413670000001 -0.2344793335 3.5884176860000003e-07
		0.95684413430000004 -0.30160423130000003 3.6413453559999998e-07
		0.97921910010000002 -0.30160423209999998 3.659201896e-07
		0.97921910170000004 -0.2568543002 3.6239167829999996e-07
		1.0015940679999999 -0.23447933509999999 3.6241307670000001e-07
		1.0910939319999999 -0.23447933830000001 3.6955569270000002e-07
		1.113468897 -0.256854305 3.7310560229999998e-07
		1.1134688930000001 -0.36872913470000002 3.8192688069999998e-07
		1.0463439960000001 -0.36872913229999998 3.765699187e-07
		1.0239690299999999 -0.34635416549999998 3.7302000899999997e-07
		1.023969031 -0.32397919959999999 3.7125575340000004e-07
		1.0015940649999999 -0.32397919879999998 3.6947009940000001e-07
		1.001594061 -0.43585402839999998 3.7829137780000001e-07
		1.0239690269999999 -0.43585402919999999 3.8007703170000002e-07
		1.023969028 -0.4134790633 3.7831277600000002e-07
		1.046343995 -0.39110409820000003 3.7833417440000001e-07
		1.1134688930000001 -0.3911041006 3.8369113639999999e-07
		1.1134688880000001 -0.52535389610000005 3.942766705e-07
		1.0910939209999999 -0.54772886129999998 3.9425527210000001e-07
		1.068718955 -0.54772886050000003 3.924696182e-07
		1.0687189539999999 -0.57010382640000001 3.9423387390000001e-07
		1.2029687499999999 -0.57010383119999997 4.0494779779999989e-07
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_L_parentConstraint1" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -0.7877436248979226 -2.1618775422326904 0.033178576774180124 ;
	setAttr ".tg[0].tor" -type "double3" 25.139579032906283 2.3717810732677695e-11 -90.000000000016527 ;
	setAttr ".lr" -type "double3" -7.5591606837675035e-06 3.3202941266982938e-06 2.3138676194511953e-08 ;
	setAttr ".rst" -type "double3" 3.3738626534694758 4.6514874717898653 -0.87798625492822557 ;
	setAttr ".rsrr" -type "double3" -3.1805546833527917e-15 -2.1461743824058633e-11 
		1.0087482609814564e-11 ;
	setAttr -k on ".w0";
createNode transform -n "RootSystem" -p "MotionSystem";
createNode transform -n "PelvisCenter_M" -p "RootSystem";
	setAttr ".t" -type "double3" 0 5.2923843071224637 -1.2424887120723724 ;
	setAttr ".ro" 3;
createNode transform -n "PelvisCenterBtwLegs_M" -p "RootSystem";
	setAttr ".t" -type "double3" -1.6731060981101109e-12 5.2923843071224637 -0.3527397751804846 ;
	setAttr ".ro" 3;
createNode orientConstraint -n "PelvisCenterBtwLegs_M_orientConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "PelvisCenterBtwLegs_M_pointConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -1.6731060981101109e-12 2.1302381350905133 -0.3527397751804846 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PelvisCenterBtwLegsOffset_M" -p "PelvisCenterBtwLegs_M";
	setAttr ".t" -type "double3" 1.6731060981101109e-12 0 -0.88974893689188783 ;
	setAttr ".ro" 3;
createNode transform -n "TwistSystem" -p "MotionSystem";
createNode transform -n "TwistFollowPelvis_M" -p "TwistSystem";
	setAttr -l on ".v" no;
	setAttr ".ro" 3;
createNode parentConstraint -n "TwistFollowPelvis_M_parentConstraint1" -p "TwistFollowPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine0_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 5.9604644775390592e-08 5.2923843071224601 -1.2424887120723715 ;
	setAttr -k on ".w0";
createNode joint -n "UnTwistPelvis_M" -p "TwistFollowPelvis_M";
	setAttr ".t" -type "double3" 3.3087224502121107e-23 3.5527136788005009e-15 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -18.843805686447045 6.0310983496504238e-05 0.00013281984082593517 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 29.999999999999996 0 0 ;
createNode joint -n "UnTwistEndPelvis_M" -p "UnTwistPelvis_M";
	setAttr ".t" -type "double3" 1.1366660179801571e-08 3.9800141943428833 0.0013815405635022593 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode orientConstraint -n "UnTwistEndPelvis_M_orientConstraint1" -p "UnTwistEndPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine4AlignUnTwistTo_MW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 18.490390447865629 -1.5203664648290452e-05 -0.00014217866226740196 ;
	setAttr ".rsrr" -type "double3" 18.490390447865629 -1.5203664648290445e-05 -0.00014217866226740199 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector5" -p "UnTwistPelvis_M";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode ikHandle -n "UnTwistIKPelvis_M" -p "TwistFollowPelvis_M";
	setAttr ".r" -type "double3" 11.156194825697339 -1.4179076538328612e-05 0.00014518084792311623 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".roc" yes;
createNode pointConstraint -n "UnTwistIKPelvis_M_pointConstraint1" -p "UnTwistIKPelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "IKSpine4_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.0073184967041016e-05 3.9045380305134918 0.77142570064236415 ;
	setAttr -k on ".w0";
createNode transform -n "GameSkeleton" -p "Main";
createNode joint -n "Pelvis_M" -p "GameSkeleton";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 29.999999999999996 0 0 ;
createNode joint -n "SpineA_M" -p "Pelvis_M";
	setAttr ".jo" -type "double3" -29.999999999999996 0 0 ;
createNode joint -n "Chest_M" -p "SpineA_M";
createNode joint -n "Clavicle_R" -p "Chest_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 90.000108608329285 89.816813673761004 167.41013204046254 ;
createNode joint -n "Shoulder_R" -p "Clavicle_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -59.11514939602958 5.8645739860458503 2.3404237289079948 ;
createNode joint -n "Elbow_R" -p "Shoulder_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.6128851661730761 6.3110378293342357 84.200711274501529 ;
createNode joint -n "Wrist_R" -p "Elbow_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -4.3388154323328116 -13.483344189669324 -8.7888035025143267 ;
createNode joint -n "MiddleFinger1_R" -p "Wrist_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 130.29110352885328 -50.744243965514755 -158.22646613214673 ;
createNode joint -n "MiddleFinger2_R" -p "MiddleFinger1_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 5.9210128155416015 -7.513785868345658 57.888297048376998 ;
createNode joint -n "MiddleFinger3_End_R" -p "MiddleFinger2_R";
	setAttr ".t" -type "double3" -3.5527136788005009e-15 1.4053515974398834 -1.5099033134902129e-14 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "MiddleFinger2_R_parentConstraint1" -p "MiddleFinger2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.5997884401722724e-07 4.0171251633615221e-07 -9.5968253915254913e-07 ;
	setAttr ".rst" -type "double3" 2.9334672468905865e-08 0.99106178888130003 -1.7119941020382612e-09 ;
	setAttr ".rsrr" -type "double3" 8.0019670386842496e-07 1.0179903357564779e-06 -2.8839566414031656e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "MiddleFinger1_R_parentConstraint1" -p "MiddleFinger1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -9.8974793428597984e-08 1.5265921897490893e-06 -1.6959120958006739e-06 ;
	setAttr ".rst" -type "double3" 1.2140707715363681 2.6126476851167109 -0.55558593361305064 ;
	setAttr ".rsrr" -type "double3" -9.8974787067489884e-08 1.5265920530846304e-06 -1.6959121530506582e-06 ;
	setAttr -k on ".w0";
createNode joint -n "IndexFinger1_R" -p "Wrist_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 172.03731024008135 -48.506051160996989 -164.02957004147353 ;
createNode joint -n "IndexFinger2_R" -p "IndexFinger1_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0.64587233451773229 -1.2090343877571794 61.885863967235046 ;
createNode joint -n "IndexFinger3_End_R" -p "IndexFinger2_R";
	setAttr ".t" -type "double3" -8.8817841970012523e-15 1.4990935335754494 -2.3092638912203256e-14 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "IndexFinger2_R_parentConstraint1" -p "IndexFinger2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.0496204148700492e-06 -5.2892308921159045e-07 2.495674991752901e-06 ;
	setAttr ".rst" -type "double3" -6.138273622724455e-08 0.92790311814927229 -2.0145857426712155e-08 ;
	setAttr ".rsrr" -type "double3" -1.555705579454798e-06 6.1104645879164761e-07 6.2848258143623372e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IndexFinger1_R_parentConstraint1" -p "IndexFinger1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.2439583795004832e-06 -1.175360209102318e-06 3.7902353578023261e-06 ;
	setAttr ".rst" -type "double3" 0.54621867886444897 2.7196885306359215 -0.92085001407918565 ;
	setAttr ".rsrr" -type "double3" -1.2439583838737445e-06 -1.175360233614953e-06 3.7902354023300917e-06 ;
	setAttr -k on ".w0";
createNode joint -n "ThumbFinger1_R" -p "Wrist_R";
	setAttr ".jo" -type "double3" 46.3006097245365 201.2291656180592 23.018105103460343 ;
createNode joint -n "ThumbFinger2_R" -p "ThumbFinger1_R";
	setAttr ".jo" -type "double3" 1.5968655649123389 -2.9071739957667129 61.204051514185871 ;
createNode joint -n "ThumbFinger3_End_R" -p "ThumbFinger2_R";
	setAttr ".t" -type "double3" -5.773159728050814e-15 1.3935263360622585 -7.1054273576010019e-15 ;
createNode parentConstraint -n "ThumbFinger2_R_parentConstraint1" -p "ThumbFinger2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.2305705250688875e-07 6.4822015227885324e-07 3.2170154713579864e-06 ;
	setAttr ".rst" -type "double3" -4.2066645278282522e-08 0.79591438041152707 -8.4525604293617107e-09 ;
	setAttr ".rsrr" -type "double3" -1.9629559374698736e-06 -3.6140945632335535e-07 
		6.3719815113355535e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ThumbFinger1_R_parentConstraint1" -p "ThumbFinger1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.084779430236893e-07 -2.2777159967342029e-06 3.0282668760909946e-06 ;
	setAttr ".rst" -type "double3" 0.0024459017964779672 2.2723833325074549 -1.6528888569724778 ;
	setAttr ".rsrr" -type "double3" -6.0847794302368919e-07 -2.2777159856022604e-06 
		3.0282669015354298e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Wrist_R_parentConstraint1" -p "Wrist_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWrist_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXWrist_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -9.8905019226909778e-08 2.0725096487537969e-08 4.4363097700770509e-07 ;
	setAttr ".rst" -type "double3" 0.38546836386569711 1.3131719761590193 -0.12401205243806146 ;
	setAttr ".rsrr" -type "double3" 1.7575367571168361e-06 1.0356028230534923e-06 2.608348582425739e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Elbow_R_parentConstraint1" -p "Elbow_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXElbow_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -2.8532280990094335e-07 1.3924679263883631e-08 3.1305124057950488e-06 ;
	setAttr ".rst" -type "double3" 0.049680780809375547 4.4542548325579281 -0.25171657814444259 ;
	setAttr ".rsrr" -type "double3" 9.4809231030736743e-07 9.7389632781916326e-07 2.4572096146294491e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Shoulder_R_parentConstraint1" -p "Shoulder_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXShoulder_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.9130138154101282e-06 2.3114782892225848e-07 1.1506263557368587e-07 ;
	setAttr ".rst" -type "double3" 0.67425717235845584 1.8524701900102851 -0.80961544628136484 ;
	setAttr ".rsrr" -type "double3" -7.9521155049872315e-07 1.4632294244392847e-06 -7.4853991306680947e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Clavicle_R_parentConstraint1" -p "Clavicle_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXClavicle_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.5826784712948249e-07 1.2262974307827783e-08 -3.1571774169724784e-06 ;
	setAttr ".rst" -type "double3" -1.9987797908081439 1.4515305646454788 -0.23751408606229596 ;
	setAttr ".rsrr" -type "double3" -6.5827010850386115e-07 1.2262967946780907e-08 -3.1571774487780264e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Chest_End_M" -p "Chest_M";
	setAttr ".t" -type "double3" -3.3881317890172021e-21 1.3032944042756736 1.0309757934324235 ;
	setAttr ".ro" 5;
createNode joint -n "ShoulderPad1_R" -p "Chest_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 77.733411186393539 89.816813673756982 167.41013204074829 ;
createNode joint -n "ShoulderPad1_End_R" -p "ShoulderPad1_R";
	setAttr ".t" -type "double3" 1.0902390101819035e-13 1.5513811389331269 -3.3750779948604759e-14 ;
	setAttr ".ro" 2;
createNode parentConstraint -n "ShoulderPad1_R_parentConstraint1" -p "ShoulderPad1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulderPad1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.9664967898603291e-06 6.8276469791128336e-07 -3.0824953266711003e-06 ;
	setAttr ".rst" -type "double3" -1.8142628627476771 0.28608493210809449 -0.28779146322840943 ;
	setAttr ".rsrr" -type "double3" -1.9664967644158909e-06 6.8276469791128251e-07 -3.0824953266711003e-06 ;
	setAttr -k on ".w0";
createNode joint -n "ShoulderPad2_R" -p "Chest_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 77.733411186344767 89.816813673757551 167.41013204070074 ;
createNode joint -n "ShoulderPad2_End_R" -p "ShoulderPad2_R";
	setAttr ".t" -type "double3" 9.3702823278363212e-14 1.5303351113900434 4.0856207306205761e-14 ;
	setAttr ".ro" 2;
createNode parentConstraint -n "ShoulderPad2_R_parentConstraint1" -p "ShoulderPad2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulderPad2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.9664955558051148e-06 6.8276473289734164e-07 -3.0824947541712501e-06 ;
	setAttr ".rst" -type "double3" -1.7078160680150067 -0.067167164694192039 -0.47936911453115794 ;
	setAttr ".rsrr" -type "double3" -1.9664955558051148e-06 6.8276473289734164e-07 -3.0824947541712501e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Clavicle_L" -p "Chest_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -89.999891391670729 -89.816813673761018 -167.4101320404607 ;
createNode joint -n "Shoulder_L" -p "Clavicle_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -59.115149396029658 5.8645739860458459 2.3404237289079552 ;
createNode joint -n "Elbow_L" -p "Shoulder_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.6128851661730765 6.3110378293342562 84.200711274501529 ;
createNode joint -n "Wrist_L" -p "Elbow_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -4.3388154323329218 -13.483344189669356 -8.7888035025143942 ;
createNode joint -n "MiddleFinger1_L" -p "Wrist_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 130.29110352885331 -50.744243965514762 -158.22646613214667 ;
createNode joint -n "MiddleFinger2_L" -p "MiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 5.9210128155415891 -7.5137858683456775 57.888297048377034 ;
createNode joint -n "MiddleFinger3_End_L" -p "MiddleFinger2_L";
	setAttr ".t" -type "double3" -3.5527136788005009e-15 -1.4053515974398867 -4.4408920985006262e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "MiddleFinger2_L_parentConstraint1" -p "MiddleFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.5997889172554669e-07 4.0171260618682258e-07 -9.5968255977645821e-07 ;
	setAttr ".rst" -type "double3" -4.0845207216477775e-08 -0.99106177157624931 1.2386370151773464e-08 ;
	setAttr ".rsrr" -type "double3" 8.0019718293948313e-07 1.0179907794438458e-06 -2.8839563884496852e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "MiddleFinger1_L_parentConstraint1" -p "MiddleFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -9.8974895206349218e-08 1.5265921297161211e-06 -1.6959120540558921e-06 ;
	setAttr ".rst" -type "double3" -1.2140707674155466 -2.6126476626527451 0.55558592868473511 ;
	setAttr ".rsrr" -type "double3" -9.8974933373001553e-08 1.5265926124646865e-06 -1.6959118047799181e-06 ;
	setAttr -k on ".w0";
createNode joint -n "IndexFinger1_L" -p "Wrist_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 172.03731024008138 -48.506051160997025 -164.02957004147353 ;
createNode joint -n "IndexFinger2_L" -p "IndexFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0.64587233451774761 -1.2090343877571748 61.885863967235181 ;
createNode joint -n "IndexFinger3_End_L" -p "IndexFinger2_L";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 -1.4990935335754481 6.2172489379008766e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "IndexFinger2_L_parentConstraint1" -p "IndexFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.0496204589008523e-06 -5.28923140100465e-07 2.4956748588995157e-06 ;
	setAttr ".rst" -type "double3" 6.1382729121817192e-08 -0.92790311814927229 2.0145877854815808e-08 ;
	setAttr ".rsrr" -type "double3" -1.5557056344684527e-06 6.110464329496398e-07 6.2848256687649926e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IndexFinger1_L_parentConstraint1" -p "IndexFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.2439584430121831e-06 -1.1753602526113147e-06 3.7902353784759317e-06 ;
	setAttr ".rst" -type "double3" -0.54621867886444475 -2.7196885306359224 0.92085001407918909 ;
	setAttr ".rsrr" -type "double3" -1.2439584112066353e-06 -1.1753602682656063e-06 
		3.7902353991495364e-06 ;
	setAttr -k on ".w0";
createNode joint -n "ThumbFinger1_L" -p "Wrist_L";
	setAttr ".jo" -type "double3" 46.300609724536471 201.2291656180592 23.018105103460449 ;
createNode joint -n "ThumbFinger2_L" -p "ThumbFinger1_L";
	setAttr ".jo" -type "double3" 1.5968655649123269 -2.9071739957667169 61.204051514185871 ;
createNode joint -n "ThumbFinger3_End_L" -p "ThumbFinger2_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-15 -1.3935263360622527 -8.8817841970012523e-16 ;
createNode parentConstraint -n "ThumbFinger2_L_parentConstraint1" -p "ThumbFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.2305705986192073e-07 6.4822018806009324e-07 3.2170154137414916e-06 ;
	setAttr ".rst" -type "double3" 4.2066658600958817e-08 -0.79591438041150653 8.452568422967488e-09 ;
	setAttr ".rsrr" -type "double3" -1.9629558471222388e-06 -3.6140939807945285e-07 
		6.3719815082295429e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ThumbFinger1_L_parentConstraint1" -p "ThumbFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.0847796846812588e-07 -2.2777158917758978e-06 3.0282669715076349e-06 ;
	setAttr ".rst" -type "double3" -0.0024459017964739704 -2.2723833325074563 1.6528888569724769 ;
	setAttr ".rsrr" -type "double3" -6.0847796846812514e-07 -2.2777158806439562e-06 
		3.0282669556048615e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Wrist_L_parentConstraint1" -p "Wrist_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWrist_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXWrist_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -9.8904349123795909e-08 2.0725251142012001e-08 4.4363091796865883e-07 ;
	setAttr ".rst" -type "double3" -0.38546836386574962 -1.3131719761590372 0.12401205243830128 ;
	setAttr ".rsrr" -type "double3" 1.7575365394475737e-06 1.0356048715791917e-06 2.608349125604814e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Elbow_L_parentConstraint1" -p "Elbow_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXElbow_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -2.8532224415978022e-07 1.3924710274307088e-08 3.1305124634674507e-06 ;
	setAttr ".rst" -type "double3" -0.049680780809374103 -4.4542548325579645 0.25171657814473214 ;
	setAttr ".rsrr" -type "double3" 9.4809222820925361e-07 9.7389840949220212e-07 2.4572097841679059e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Shoulder_L_parentConstraint1" -p "Shoulder_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXShoulder_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.9130190287865168e-06 2.3114839744640323e-07 1.1506259422649499e-07 ;
	setAttr ".rst" -type "double3" -0.67425717235845106 -1.8524701900102452 0.80961544628146065 ;
	setAttr ".rsrr" -type "double3" -7.9521424676425796e-07 1.4632296025503642e-06 -7.4853983673346139e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Clavicle_L_parentConstraint1" -p "Clavicle_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXClavicle_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.5826989222614145e-07 1.2263022016203291e-08 -3.1571772325003081e-06 ;
	setAttr ".rst" -type "double3" 1.9987998179687876 1.4515305646454788 -0.23751408606229596 ;
	setAttr ".rsrr" -type "double3" -6.5827375660007932e-07 1.2263015655200362e-08 -3.1571772261391986e-06 ;
	setAttr -k on ".w0";
createNode joint -n "ShoulderPad1_L" -p "Chest_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -102.26658881395841 -89.816813673761985 -167.41013204039675 ;
createNode joint -n "ShoulderPad1_End_L" -p "ShoulderPad1_L";
	setAttr ".t" -type "double3" 2.9198865547641617e-14 -1.551381138933126 -2.8421709430404007e-14 ;
	setAttr ".ro" 2;
createNode parentConstraint -n "ShoulderPad1_L_parentConstraint1" -p "ShoulderPad1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulderPad1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.96649848191545e-06 6.8276474561955819e-07 -3.082490018325348e-06 ;
	setAttr ".rst" -type "double3" 1.8142828899083221 0.28608493210809627 -0.28779146322840943 ;
	setAttr ".rsrr" -type "double3" -1.9664985009987778e-06 6.8276475834177791e-07 -3.0824900437697862e-06 ;
	setAttr -k on ".w0";
createNode joint -n "ShoulderPad2_L" -p "Chest_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -102.26658881374394 -89.816813673758929 -167.41013204060826 ;
createNode joint -n "ShoulderPad2_End_L" -p "ShoulderPad2_L";
	setAttr ".t" -type "double3" -5.6510351953420468e-14 -1.5303351113900436 3.0198066269804258e-14 ;
	setAttr ".ro" 2;
createNode parentConstraint -n "ShoulderPad2_L_parentConstraint1" -p "ShoulderPad2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulderPad2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.9665014589146132e-06 6.8276473925858237e-07 -3.0824931416300622e-06 ;
	setAttr ".rst" -type "double3" 1.7078360951756508 -0.067167164694193815 -0.47936911453115794 ;
	setAttr ".rsrr" -type "double3" -1.9665014779979407e-06 6.8276473925858258e-07 -3.0824931416300622e-06 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "Chest_M_pointConstraint1" -p "Chest_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXChest_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXChest_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -1.0084417224895012e-05 2.4848531589474643 0.029824643320000344 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "Chest_M_orientConstraint1" -p "Chest_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXChest_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXChest_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rsrr" -type "double3" 0.095207980229070774 -5.368371413798601e-06 -8.4249032019183816e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "SpineA_M_pointConstraint1" -p "SpineA_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXSpineA_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXSpineA_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -4.4337047835873864e-08 1.6018414241091286 -0.073876596172575582 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "SpineA_M_orientConstraint1" -p "SpineA_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXSpineA_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXSpineA_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rsrr" -type "double3" -89.567136533874276 0.00011633177828648692 -0.00017286633741597849 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "HipTwist_R" -p "Pelvis_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 235.13957903290628 89.999999999911367 ;
createNode joint -n "Hip_R" -p "HipTwist_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0.72356917514661268 1.0610891793508392 -90.162074787506356 ;
createNode joint -n "Knee_R" -p "Hip_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 22.91976200682813 0 0 ;
createNode joint -n "Ankle_R" -p "Knee_R";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 1.4900163786873937 -0.0077691772221654479 0.59745913039418752 ;
createNode joint -n "Foot_End_R" -p "Ankle_R";
	setAttr ".t" -type "double3" 0.066282725496478179 2.1170624371978559 -2.6287438008810069 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 71.642384016319937 179.10051431432228 2.2965224593054767e-05 ;
createNode parentConstraint -n "Ankle_R_parentConstraint1" -p "Ankle_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXAnkle_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXAnkle_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -4.5802784640924288e-08 -8.6292719866913906e-10 -1.8758008819027772e-08 ;
	setAttr ".rst" -type "double3" -0.036411128641858248 0.88064442980508195 0.022028124115506892 ;
	setAttr ".rsrr" -type "double3" -5.2965755551621058e-06 3.663862875124901e-07 -1.7673816482958364e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Knee_R_parentConstraint1" -p "Knee_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXKnee_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXKnee_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 3.6164249272176787e-06 0 0 ;
	setAttr ".rst" -type "double3" 0.032864079379351097 2.586719100740809 0.19800968840156996 ;
	setAttr ".rsrr" -type "double3" -5.3307710347688226e-06 3.5782215034679347e-07 -1.7391948600329555e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Hip_R_parentConstraint1" -p "Hip_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHip_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXHip_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 5.4069382605480003e-14 -1.458909979096785e-06 -3.690044444204479e-12 ;
	setAttr ".rst" -type "double3" -0.088558944056244635 1.1113775208673391 -0.04155877220233628 ;
	setAttr ".rsrr" -type "double3" -5.1783370232887817e-06 1.0069239602702799e-06 -1.4625218256422613e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "HipTwist_R_parentConstraint1" -p "HipTwist_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.0551148057093192e-06 1.0390807686990654e-05 -1.4315578959445881e-06 ;
	setAttr ".rst" -type "double3" -1.2119851708412177 0.05025157145356296 -0.029227975692608155 ;
	setAttr ".rsrr" -type "double3" 2.0551148152509844e-06 1.039080769017121e-05 -1.4315579023056969e-06 ;
	setAttr -k on ".w0";
createNode joint -n "HipTwist_L" -p "Pelvis_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 55.139579032906283 90.000000000028919 ;
createNode joint -n "Hip_L" -p "HipTwist_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0.72356917514656316 1.0610891793508277 -90.162074787506356 ;
createNode joint -n "Knee_L" -p "Hip_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 22.919762006828162 0 0 ;
createNode joint -n "Ankle_L" -p "Knee_L";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 1.490016378687254 -0.0077691772221520402 0.59745913039418774 ;
createNode joint -n "Foot_End_L" -p "Ankle_L";
	setAttr ".t" -type "double3" -0.066282725495936834 -2.1170624371978835 2.6287438008810007 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 71.642384016319838 179.10051431432228 2.2965224593054767e-05 ;
createNode parentConstraint -n "Ankle_L_parentConstraint1" -p "Ankle_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXAnkle_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXAnkle_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -4.5802644961420924e-08 -8.6294060886915244e-10 -1.8758008654150351e-08 ;
	setAttr ".rst" -type "double3" 0.036411207847807603 -0.88064442748841554 -0.022028222189853647 ;
	setAttr ".rsrr" -type "double3" -3.8981338794510719e-06 1.4004023495553833e-06 -6.492158622886681e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Knee_L_parentConstraint1" -p "Knee_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXKnee_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXKnee_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.0123136336768711e-05 5.0762933771010396e-23 -1.4815687464369001e-06 ;
	setAttr ".rst" -type "double3" -0.032864017376552646 -2.5867190701332383 -0.19800975807974089 ;
	setAttr ".rsrr" -type "double3" -3.943036074056013e-06 1.3812895496999534e-06 -5.8404099536372017e-07 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Hip_L_parentConstraint1" -p "Hip_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHip_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXHip_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 6.3784870285940584e-14 -4.8386473809312679e-06 -3.7874920292239829e-12 ;
	setAttr ".rst" -type "double3" 0.088558970796627889 -1.1113775189959298 0.041558765266143771 ;
	setAttr ".rsrr" -type "double3" -3.7906017545610136e-06 1.4997048111684433e-06 -1.1167658298157462e-08 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "HipTwist_L_parentConstraint1" -p "HipTwist_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.9959747473716681e-06 7.5591607155028948e-06 1.4314907885585261e-06 ;
	setAttr ".rst" -type "double3" 1.2119850516319273 0.050251571453562072 -0.029227975692608155 ;
	setAttr ".rsrr" -type "double3" 2.9959747410105586e-06 7.5591607282251137e-06 1.4314907949196351e-06 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "Pelvis_M_pointConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXPelvis_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 5.9604644775390605e-08 5.2923843071224628 -1.2424887120723718 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode orientConstraint -n "Pelvis_M_orientConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKOrientToSpine_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 8.348260578755083e-07 0 0 ;
	setAttr ".rsrr" -type "double3" 8.348260578755083e-07 0 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode nurbsCurve -n "forwardArrowShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-0.20855383847834053 4.6308254670521884e-17 1.7174924035230621
		-0.41710767695668105 9.2616509341043767e-17 1.7174924035230621
		-1.3892476401156569e-16 2.7784952802313159e-16 2.343153918958083
		0.41710767695668105 -9.2616509341043767e-17 1.7174924035230621
		0.20855383847834053 -4.6308254670521884e-17 1.7174924035230621
		0.20855383847834069 -3.2415778269365345e-16 1.0918308880880407
		-0.20855383847834039 -2.3154127335260972e-16 1.0918308880880407
		-0.20855383847834053 4.6308254670521884e-17 1.7174924035230621
		;
createNode transform -n "items" -p "ctrl_rig";
	setAttr -l on ".it" no;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 9 ".lnk";
	setAttr -s 9 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr ".cm" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".cs" 3;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode shadingEngine -n "irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode shadingEngine -n "BPRig_irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "BPRig_materialInfo3";
createNode ikSCsolver -n "ikSCsolver";
createNode ikRPsolver -n "ikRPsolver";
createNode ikSplineSolver -n "ikSplineSolver";
createNode displayLayer -n "jointLayer";
createNode reverse -n "jointVisReverse";
createNode objectSet -n "ControlSet";
	setAttr ".ihi" 0;
	setAttr -s 107 ".dsm";
createNode objectSet -n "GameSet";
	setAttr ".ihi" 0;
	setAttr -s 35 ".dsm";
createNode objectSet -n "AllSet";
	setAttr ".ihi" 0;
	setAttr -s 536 ".dsm";
	setAttr -s 76 ".dnsm";
createNode objectSet -n "Sets";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dnsm";
createNode setRange -n "IKArm_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode setRange -n "PoleArm_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendArmUnitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendArmReverse_R";
createNode condition -n "FKIKBlendArmCondition_R";
createNode setRange -n "FKIKBlendArmsetRange_R";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode unitConversion -n "unitConversion1";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_R";
createNode condition -n "FKIKBlendLegCondition_R";
createNode setRange -n "FKIKBlendLegsetRange_R";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "Leg_RAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_R";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion3";
	setAttr ".cf" 0.017453292519943295;
createNode setRange -n "IKStiffSpine0_M";
	setAttr ".m" -type "float3" 0 1.3671129 0 ;
	setAttr ".on" -type "float3" 0 -10 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode setRange -n "IKStiffSpine2_M";
	setAttr ".on" -type "float3" 0 -10 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode setRange -n "IKStiffSpine4_M";
	setAttr ".m" -type "float3" 0 -1.3198788 0 ;
	setAttr ".on" -type "float3" 0 -10 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode curveInfo -n "IKCurveInfoSpine_M";
createNode multiplyDivide -n "IKCurveInfoNormalizeSpine_M";
	setAttr ".op" 2;
	setAttr ".i2" -type "float3" 1 4.1094079 1 ;
createNode multiplyDivide -n "IKCurveInfoAllMultiplySpine_M";
	setAttr ".op" 2;
createNode unitConversion -n "stretchySpineUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "stretchySpineReverse_M";
createNode multiplyDivide -n "stretchySpineMultiplyDivide0_M";
	setAttr ".i1" -type "float3" 0 2.4826372 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo0_M";
	setAttr -s 2 ".i[0:1]"  2.482637191494006 2.4826371669769287;
createNode multiplyDivide -n "stretchySpineMultiplyDivide1_M";
	setAttr ".i1" -type "float3" 0 1.6020693 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo1_M";
	setAttr -s 2 ".i[0:1]"  1.6020692693748249 1.6020692586898804;
createNode multiplyDivide -n "stretchySpineMultiplyDivide2_M";
	setAttr ".i1" -type "float3" 0 2.4826372 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo2_M";
	setAttr -s 2 ".i[0:1]"  2.482637191494006 2.4826371669769287;
createNode multiplyDivide -n "stretchySpineMultiplyDivide3_M";
	setAttr ".i1" -type "float3" 0 1.6020693 0 ;
createNode blendTwoAttr -n "stretchySpineBlendTwo3_M";
	setAttr -s 2 ".i[0:1]"  1.6020692693748249 1.6020692586898804;
createNode unitConversion -n "IKTwistSpineUnitConversion_M";
	setAttr ".cf" 0.75;
createNode unitConversion -n "FKIKBlendSpineUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendSpineReverse_M";
createNode condition -n "FKIKBlendSpineCondition_M";
createNode setRange -n "FKIKBlendSpinesetRange_M";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode blendColors -n "ScaleBlendWrist_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendElbow_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendShoulder_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendChest_M";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendSpineA_M";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendAnkle_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendKnee_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendHip_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendPelvis_M";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode setRange -n "IKArm_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode setRange -n "PoleArm_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendArmUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendArmReverse_L";
createNode condition -n "FKIKBlendArmCondition_L";
createNode setRange -n "FKIKBlendArmsetRange_L";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode unitConversion -n "unitConversion4";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_L";
createNode condition -n "FKIKBlendLegCondition_L";
createNode setRange -n "FKIKBlendLegsetRange_L";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "Leg_LAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_L";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion5";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion6";
	setAttr ".cf" 0.017453292519943295;
createNode blendColors -n "ScaleBlendWrist_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendElbow_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendShoulder_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendAnkle_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendKnee_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendHip_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode setRange -n "CenterBtwFeet_M";
	setAttr ".v" -type "float3" 0 1 0 ;
	setAttr ".n" -type "float3" 2 0 0 ;
	setAttr ".om" -type "float3" 0.1 0 0 ;
createNode unitConversion -n "PelvisCenterBtwFeetUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "PelvisCenterBtwFeetReverse_M";
createNode dagPose -n "buildPose";
	addAttr -ci true -sn "udAttr" -ln "udAttr" -dt "string";
	setAttr ".udAttr" -type "string" (
		"setAttr FKIKSpine_M.FKIKBlend 0;setAttr FKIKSpine_M.FKVis 1;setAttr FKIKSpine_M.IKVis 0;setAttr IKExtraArm_L.translateX 0;setAttr IKExtraArm_L.translateY 0;setAttr IKExtraArm_L.translateZ 0;setAttr IKExtraArm_L.rotateX -0;setAttr IKExtraArm_L.rotateY 0;setAttr IKExtraArm_L.rotateZ -0;setAttr IKArm_L.translateX 0;setAttr IKArm_L.translateY 0;setAttr IKArm_L.translateZ 0;setAttr IKArm_L.rotateX -0;setAttr IKArm_L.rotateY 0;setAttr IKArm_L.rotateZ -0;setAttr IKArm_L.follow 0;setAttr PoleExtraArm_L.translateX 0;setAttr PoleExtraArm_L.translateY 0;setAttr PoleExtraArm_L.translateZ 0;setAttr PoleArm_L.translateX 0;setAttr PoleArm_L.translateY 0;setAttr PoleArm_L.translateZ 0;setAttr PoleArm_L.follow 0;setAttr FKIKArm_L.FKIKBlend 0;setAttr FKIKArm_L.FKVis 1;setAttr FKIKArm_L.IKVis 0;setAttr IKExtraLeg_L.translateX 0;setAttr IKExtraLeg_L.translateY 0;setAttr IKExtraLeg_L.translateZ 0;setAttr IKExtraLeg_L.rotateX -0;setAttr IKExtraLeg_L.rotateY -0;setAttr IKExtraLeg_L.rotateZ 0;setAttr IKLeg_L.translateX 0;setAttr IKLeg_L.translateY 0;setAttr IKLeg_L.translateZ 0;setAttr IKLeg_L.rotateX -0;setAttr IKLeg_L.rotateY -0;setAttr IKLeg_L.rotateZ 0;setAttr IKLeg_L.swivel 0;setAttr IKLeg_L.roll 0;setAttr IKLeg_L.rollAngle 25;setAttr PoleExtraLeg_L.translateX 0;setAttr PoleExtraLeg_L.translateY 0;setAttr PoleExtraLeg_L.translateZ 0;setAttr PoleLeg_L.translateX 0;setAttr PoleLeg_L.translateY 0;setAttr PoleLeg_L.translateZ 0;setAttr PoleLeg_L.follow 10;setAttr FKExtraChest_M.translateX 0;setAttr FKExtraChest_M.translateY 0;setAttr FKExtraChest_M.translateZ 0;setAttr FKExtraChest_M.rotateX 0;setAttr FKExtraChest_M.rotateY -0;setAttr FKExtraChest_M.rotateZ 0;setAttr FKExtraChest_M.scaleX 1;setAttr FKExtraChest_M.scaleY 1;setAttr FKExtraChest_M.scaleZ 1;setAttr FKChest_M.translateX 0;setAttr FKChest_M.translateY 0;setAttr FKChest_M.translateZ 0;setAttr FKChest_M.rotateX 0;setAttr FKChest_M.rotateY -0;setAttr FKChest_M.rotateZ 0;setAttr FKChest_M.scaleX 1;setAttr FKChest_M.scaleY 1;setAttr FKChest_M.scaleZ 1;setAttr FKExtraSpineA_M.translateX 0;setAttr FKExtraSpineA_M.translateY 0;setAttr FKExtraSpineA_M.translateZ 0;setAttr FKExtraSpineA_M.rotateX 0;setAttr FKExtraSpineA_M.rotateY -0;setAttr FKExtraSpineA_M.rotateZ 0;setAttr FKExtraSpineA_M.scaleX 1;setAttr FKExtraSpineA_M.scaleY 1;setAttr FKExtraSpineA_M.scaleZ 1;setAttr FKSpineA_M.translateX 0;setAttr FKSpineA_M.translateY 0;setAttr FKSpineA_M.translateZ 0;setAttr FKSpineA_M.rotateX 0;setAttr FKSpineA_M.rotateY -0;setAttr FKSpineA_M.rotateZ 0;setAttr FKSpineA_M.scaleX 1;setAttr FKSpineA_M.scaleY 1;setAttr FKSpineA_M.scaleZ 1;setAttr FKExtraAnkle_R.translateX 0;setAttr FKExtraAnkle_R.translateY 0;setAttr FKExtraAnkle_R.translateZ 0;setAttr FKExtraAnkle_R.rotateX -0;setAttr FKExtraAnkle_R.rotateY -0;setAttr FKExtraAnkle_R.rotateZ 0;setAttr FKExtraAnkle_R.scaleX 1;setAttr FKExtraAnkle_R.scaleY 1;setAttr FKExtraAnkle_R.scaleZ 1;setAttr FKAnkle_R.translateX 0;setAttr FKAnkle_R.translateY 0;setAttr FKAnkle_R.translateZ 0;setAttr FKAnkle_R.rotateX -0;setAttr FKAnkle_R.rotateY -0;setAttr FKAnkle_R.rotateZ 0;setAttr FKAnkle_R.scaleX 1;setAttr FKAnkle_R.scaleY 1;setAttr FKAnkle_R.scaleZ 1;setAttr FKExtraKnee_R.translateX 0;setAttr FKExtraKnee_R.translateY -4.440892099e-16;setAttr FKExtraKnee_R.translateZ 5.551115123e-17;setAttr FKExtraKnee_R.rotateX -0;setAttr FKExtraKnee_R.rotateY 0;setAttr FKExtraKnee_R.rotateZ 0;setAttr FKExtraKnee_R.scaleX 1;setAttr FKExtraKnee_R.scaleY 1;setAttr FKExtraKnee_R.scaleZ 1;setAttr FKKnee_R.translateX 0;setAttr FKKnee_R.translateY 0;setAttr FKKnee_R.translateZ 5.551115123e-17;setAttr FKKnee_R.rotateX -0;setAttr FKKnee_R.rotateY 0;setAttr FKKnee_R.rotateZ 0;setAttr FKKnee_R.scaleX 1;setAttr FKKnee_R.scaleY 1;setAttr FKKnee_R.scaleZ 1;setAttr FKExtraHip_R.translateX -4.440892099e-16;setAttr FKExtraHip_R.translateY 8.881784197e-16;setAttr FKExtraHip_R.translateZ -4.440892099e-16;setAttr FKExtraHip_R.rotateX -0;setAttr FKExtraHip_R.rotateY 0;setAttr FKExtraHip_R.rotateZ 0;setAttr FKExtraHip_R.scaleX 1;setAttr FKExtraHip_R.scaleY 1;setAttr FKExtraHip_R.scaleZ 1;setAttr FKHip_R.translateX -4.440892099e-16;setAttr FKHip_R.translateY 0;setAttr FKHip_R.translateZ -6.661338148e-16;setAttr FKHip_R.rotateX -0;setAttr FKHip_R.rotateY 0;setAttr FKHip_R.rotateZ 0;setAttr FKHip_R.scaleX 1;setAttr FKHip_R.scaleY 1;setAttr FKHip_R.scaleZ 1;setAttr FKExtraHipTwist_R.translateX -8.881784197e-16;setAttr FKExtraHipTwist_R.translateY 2.220446049e-16;setAttr FKExtraHipTwist_R.translateZ 4.440892099e-16;setAttr FKExtraHipTwist_R.rotateX -0;setAttr FKExtraHipTwist_R.rotateY 0;setAttr FKExtraHipTwist_R.rotateZ 0;setAttr FKExtraHipTwist_R.scaleX 1;setAttr FKExtraHipTwist_R.scaleY 1;setAttr FKExtraHipTwist_R.scaleZ 1;setAttr FKHipTwist_R.translateX 0;setAttr FKHipTwist_R.translateY 2.220446049e-16;setAttr FKHipTwist_R.translateZ -2.220446049e-16;setAttr FKHipTwist_R.rotateX -0;setAttr FKHipTwist_R.rotateY 0;setAttr FKHipTwist_R.rotateZ 0;setAttr FKHipTwist_R.scaleX 1;setAttr FKHipTwist_R.scaleY 1;setAttr FKHipTwist_R.scaleZ 1;setAttr FKExtraMiddleFinger2_L.translateX 0;setAttr FKExtraMiddleFinger2_L.translateY 2.220446049e-16;setAttr FKExtraMiddleFinger2_L.translateZ 8.881784197e-16;setAttr FKExtraMiddleFinger2_L.rotateX -1.908332809e-14;setAttr FKExtraMiddleFinger2_L.rotateY -6.361109363e-15;setAttr FKExtraMiddleFinger2_L.rotateZ -3.180554681e-15;setAttr FKExtraMiddleFinger2_L.scaleX 1;setAttr FKExtraMiddleFinger2_L.scaleY 1;setAttr FKExtraMiddleFinger2_L.scaleZ 1;setAttr FKMiddleFinger2_L.translateX 0;setAttr FKMiddleFinger2_L.translateY -8.881784197e-16;setAttr FKMiddleFinger2_L.translateZ 0;setAttr FKMiddleFinger2_L.rotateX -0;setAttr FKMiddleFinger2_L.rotateY 0;setAttr FKMiddleFinger2_L.rotateZ -0;setAttr FKMiddleFinger2_L.scaleX 1;setAttr FKMiddleFinger2_L.scaleY 1;setAttr FKMiddleFinger2_L.scaleZ 1;setAttr FKExtraMiddleFinger1_L.translateX 0;setAttr FKExtraMiddleFinger1_L.translateY -2.664535259e-15;setAttr FKExtraMiddleFinger1_L.translateZ 8.881784197e-16;setAttr FKExtraMiddleFinger1_L.rotateX 3.180554681e-15;setAttr FKExtraMiddleFinger1_L.rotateY 1.272221873e-14;setAttr FKExtraMiddleFinger1_L.rotateZ 1.272221873e-14;setAttr FKExtraMiddleFinger1_L.scaleX 1;setAttr FKExtraMiddleFinger1_L.scaleY 1;setAttr FKExtraMiddleFinger1_L.scaleZ 1;setAttr FKMiddleFinger1_L.translateX 8.881784197e-16;setAttr FKMiddleFinger1_L.translateY 0;setAttr FKMiddleFinger1_L.translateZ 0;setAttr FKMiddleFinger1_L.rotateX -0;setAttr FKMiddleFinger1_L.rotateY 0;setAttr FKMiddleFinger1_L.rotateZ -0;setAttr FKMiddleFinger1_L.scaleX 1;setAttr FKMiddleFinger1_L.scaleY 1;setAttr FKMiddleFinger1_L.scaleZ 1;setAttr FKExtraIndexFinger2_L.translateX 0;setAttr FKExtraIndexFinger2_L.translateY 4.440892099e-16;setAttr FKExtraIndexFinger2_L.translateZ 0;setAttr FKExtraIndexFinger2_L.rotateX -4.770832022e-15;setAttr FKExtraIndexFinger2_L.rotateY -7.951386704e-16;setAttr FKExtraIndexFinger2_L.rotateZ 9.541664044e-15;setAttr FKExtraIndexFinger2_L.scaleX 1;setAttr FKExtraIndexFinger2_L.scaleY 1;setAttr FKExtraIndexFinger2_L.scaleZ 1;setAttr FKIndexFinger2_L.translateX 0;setAttr FKIndexFinger2_L.translateY 0;setAttr FKIndexFinger2_L.translateZ 0;setAttr FKIndexFinger2_L.rotateX -0;setAttr FKIndexFinger2_L.rotateY 0;setAttr FKIndexFinger2_L.rotateZ -0;setAttr FKIndexFinger2_L.scaleX 1;setAttr FKIndexFinger2_L.scaleY 1;setAttr FKIndexFinger2_L.scaleZ 1;setAttr FKExtraIndexFinger1_L.translateX 1.776356839e-15;setAttr FKExtraIndexFinger1_L.translateY -1.33226763e-15;setAttr FKExtraIndexFinger1_L.translateZ -8.881784197e-16;setAttr FKExtraIndexFinger1_L.rotateX -1.272221873e-14;setAttr FKExtraIndexFinger1_L.rotateY 2.226388277e-14;setAttr FKExtraIndexFinger1_L.rotateZ -8.58749764e-14;setAttr FKExtraIndexFinger1_L.scaleX 1;setAttr FKExtraIndexFinger1_L.scaleY 1;setAttr FKExtraIndexFinger1_L.scaleZ 1;setAttr FKIndexFinger1_L.translateX 0;setAttr FKIndexFinger1_L.translateY -4.440892099e-16;setAttr FKIndexFinger1_L.translateZ -8.881784197e-16;setAttr FKIndexFinger1_L.rotateX -0;setAttr FKIndexFinger1_L.rotateY 0;setAttr FKIndexFinger1_L.rotateZ -0;setAttr FKIndexFinger1_L.scaleX 1;setAttr FKIndexFinger1_L.scaleY 1;setAttr FKIndexFinger1_L.scaleZ 1;setAttr FKExtraThumbFinger2_L.translateX -4.440892099e-16;setAttr FKExtraThumbFinger2_L.translateY 0;setAttr FKExtraThumbFinger2_L.translateZ 0;setAttr FKExtraThumbFinger2_L.rotateX 1.302039573e-14;setAttr FKExtraThumbFinger2_L.rotateY 7.951386704e-16;setAttr FKExtraThumbFinger2_L.rotateZ 3.975693352e-16;setAttr FKExtraThumbFinger2_L.scaleX 1;setAttr FKExtraThumbFinger2_L.scaleY 1;setAttr FKExtraThumbFinger2_L.scaleZ 1;setAttr FKThumbFinger2_L.translateX -4.440892099e-16;setAttr FKThumbFinger2_L.translateY 0;setAttr FKThumbFinger2_L.translateZ 0;setAttr FKThumbFinger2_L.rotateX -1.590277341e-14;setAttr FKThumbFinger2_L.rotateY 3.180554681e-15;setAttr FKThumbFinger2_L.rotateZ 0;setAttr FKThumbFinger2_L.scaleX 1;setAttr FKThumbFinger2_L.scaleY 1;setAttr FKThumbFinger2_L.scaleZ 1;setAttr FKExtraThumbFinger1_L.translateX 4.440892099e-16;setAttr FKExtraThumbFinger1_L.translateY 8.881784197e-16;setAttr FKExtraThumbFinger1_L.translateZ -8.881784197e-16;setAttr FKExtraThumbFinger1_L.rotateX -1.948089742e-14;setAttr FKExtraThumbFinger1_L.rotateY -6.361109363e-15;setAttr FKExtraThumbFinger1_L.rotateZ -1.331857273e-14;setAttr FKExtraThumbFinger1_L.scaleX 1;setAttr FKExtraThumbFinger1_L.scaleY 1;setAttr FKExtraThumbFinger1_L.scaleZ 1;setAttr FKThumbFinger1_L.translateX 2.220446049e-16;setAttr FKThumbFinger1_L.translateY -8.881784197e-16;setAttr FKThumbFinger1_L.translateZ 1.776356839e-15;setAttr FKThumbFinger1_L.rotateX 6.361109363e-15;setAttr FKThumbFinger1_L.rotateY 4.770832022e-15;setAttr FKThumbFinger1_L.rotateZ -6.361109363e-15;setAttr FKThumbFinger1_L.scaleX 1;setAttr FKThumbFinger1_L.scaleY 1;setAttr FKThumbFinger1_L.scaleZ 1;setAttr FKExtraWrist_L.translateX 4.440892099e-16;setAttr FKExtraWrist_L.translateY 0;setAttr FKExtraWrist_L.translateZ 1.776356839e-15;setAttr FKExtraWrist_L.rotateX -5.296687558e-31;setAttr FKExtraWrist_L.rotateY 9.541664044e-15;setAttr FKExtraWrist_L.rotateZ 6.361109363e-15;setAttr FKExtraWrist_L.scaleX 1;setAttr FKExtraWrist_L.scaleY 1;setAttr FKExtraWrist_L.scaleZ 1;setAttr FKWrist_L.translateX -6.661338148e-16;setAttr FKWrist_L.translateY 2.220446049e-16;setAttr FKWrist_L.translateZ 1.776356839e-15;setAttr FKWrist_L.rotateX -0;setAttr FKWrist_L.rotateY 0;setAttr FKWrist_L.rotateZ -0;setAttr FKWrist_L.scaleX 1;setAttr FKWrist_L.scaleY 1;setAttr FKWrist_L.scaleZ 1;setAttr FKExtraElbow_L.translateX -2.220446049e-16;setAttr FKExtraElbow_L.translateY 2.220446049e-16;setAttr FKExtraElbow_L.translateZ 1.776356839e-15;setAttr FKExtraElbow_L.rotateX -3.180554681e-14;setAttr FKExtraElbow_L.rotateY -2.544443745e-14;setAttr FKExtraElbow_L.rotateZ -2.544443745e-14;setAttr FKExtraElbow_L.scaleX 1;setAttr FKExtraElbow_L.scaleY 1;setAttr FKExtraElbow_L.scaleZ 1;setAttr FKElbow_L.translateX -6.661338148e-16;setAttr FKElbow_L.translateY 0;setAttr FKElbow_L.translateZ 0;setAttr FKElbow_L.rotateX -0;setAttr FKElbow_L.rotateY 0;setAttr FKElbow_L.rotateZ -0;setAttr FKElbow_L.scaleX 1;setAttr FKElbow_L.scaleY 1;setAttr FKElbow_L.scaleZ 1;setAttr FKExtraShoulder_L.translateX 4.440892099e-16;setAttr FKExtraShoulder_L.translateY -8.881784197e-16;setAttr FKExtraShoulder_L.translateZ 0;setAttr FKExtraShoulder_L.rotateX 9.541664044e-15;setAttr FKExtraShoulder_L.rotateY -3.180554681e-15;setAttr FKExtraShoulder_L.rotateZ -9.541664044e-15;setAttr FKExtraShoulder_L.scaleX 1;setAttr FKExtraShoulder_L.scaleY 1;setAttr FKExtraShoulder_L.scaleZ 1;setAttr FKShoulder_L.translateX 0;setAttr FKShoulder_L.translateY -8.881784197e-16;setAttr FKShoulder_L.translateZ 0;setAttr FKShoulder_L.rotateX -0;setAttr FKShoulder_L.rotateY 0;setAttr FKShoulder_L.rotateZ -0;setAttr FKShoulder_L.scaleX 1;setAttr FKShoulder_L.scaleY 1;setAttr FKShoulder_L.scaleZ 1;setAttr FKExtraClavicle_L.translateX -1.110223025e-16;setAttr FKExtraClavicle_L.translateY 0;setAttr FKExtraClavicle_L.translateZ 0;setAttr FKExtraClavicle_L.rotateX -2.503096534e-12;setAttr FKExtraClavicle_L.rotateY -6.947488513e-28;setAttr FKExtraClavicle_L.rotateZ 3.180554681e-14;setAttr FKExtraClavicle_L.scaleX 1;setAttr FKExtraClavicle_L.scaleY 1;setAttr FKExtraClavicle_L.scaleZ 1;setAttr FKClavicle_L.translateX 1.110223025e-16;setAttr FKClavicle_L.translateY 0;setAttr FKClavicle_L.translateZ 0;setAttr FKClavicle_L.rotateX -0;setAttr FKClavicle_L.rotateY 0;setAttr FKClavicle_L.rotateZ 0;setAttr FKClavicle_L.scaleX 1;setAttr FKClavicle_L.scaleY 1;setAttr FKClavicle_L.scaleZ 1;setAttr FKExtraShoulderPad1_L.translateX 0;setAttr FKExtraShoulderPad1_L.translateY -2.220446049e-16;setAttr FKExtraShoulderPad1_L.translateZ -1.776356839e-15;setAttr FKExtraShoulderPad1_L.rotateX 5.336970755e-12;setAttr FKExtraShoulderPad1_L.rotateY -6.361109363e-15;setAttr FKExtraShoulderPad1_L.rotateZ 5.852220614e-13;setAttr FKExtraShoulderPad1_L.scaleX 1;setAttr FKExtraShoulderPad1_L.scaleY 1;setAttr FKExtraShoulderPad1_L.scaleZ 1;setAttr FKShoulderPad1_L.translateX 0;setAttr FKShoulderPad1_L.translateY 0;setAttr FKShoulderPad1_L.translateZ -1.776356839e-15;setAttr FKShoulderPad1_L.rotateX -0;setAttr FKShoulderPad1_L.rotateY 0;setAttr FKShoulderPad1_L.rotateZ 0;setAttr FKShoulderPad1_L.scaleX 1;setAttr FKShoulderPad1_L.scaleY 1;setAttr FKShoulderPad1_L.scaleZ 1;setAttr FKExtraShoulderPad2_L.translateX -1.110223025e-16;setAttr FKExtraShoulderPad2_L.translateY 4.440892099e-16;setAttr FKExtraShoulderPad2_L.translateZ 1.776356839e-15;setAttr FKExtraShoulderPad2_L.rotateX 1.39308295e-12;setAttr FKExtraShoulderPad2_L.rotateY -1.272221873e-14;setAttr FKExtraShoulderPad2_L.rotateZ 1.240416326e-12;setAttr FKExtraShoulderPad2_L.scaleX 1;setAttr FKExtraShoulderPad2_L.scaleY 1;setAttr FKExtraShoulderPad2_L.scaleZ 1;setAttr FKShoulderPad2_L.translateX 0;setAttr FKShoulderPad2_L.translateY 2.220446049e-16;setAttr FKShoulderPad2_L.translateZ 0;setAttr FKShoulderPad2_L.rotateX -0;setAttr FKShoulderPad2_L.rotateY 0;setAttr FKShoulderPad2_L.rotateZ 0;setAttr FKShoulderPad2_L.scaleX 1;setAttr FKShoulderPad2_L.scaleY 1;setAttr FKShoulderPad2_L.scaleZ 1;setAttr FKExtraAnkle_L.translateX 0;setAttr FKExtraAnkle_L.translateY 0;setAttr FKExtraAnkle_L.translateZ 0;setAttr FKExtraAnkle_L.rotateX 1.158111684e-13;setAttr FKExtraAnkle_L.rotateY 3.975693352e-16;setAttr FKExtraAnkle_L.rotateZ 4.868820138e-32;setAttr FKExtraAnkle_L.scaleX 1;setAttr FKExtraAnkle_L.scaleY 1;setAttr FKExtraAnkle_L.scaleZ 1;setAttr FKAnkle_L.translateX 0;setAttr FKAnkle_L.translateY 0;setAttr FKAnkle_L.translateZ 0;setAttr FKAnkle_L.rotateX -0;setAttr FKAnkle_L.rotateY -0;setAttr FKAnkle_L.rotateZ 0;setAttr FKAnkle_L.scaleX 1;setAttr FKAnkle_L.scaleY 1;setAttr FKAnkle_L.scaleZ 1;setAttr FKExtraKnee_L.translateX -4.440892099e-16;setAttr FKExtraKnee_L.translateY 0;setAttr FKExtraKnee_L.translateZ 2.775557562e-17;setAttr FKExtraKnee_L.rotateX 5.97907008e-16;setAttr FKExtraKnee_L.rotateY -1.941256519e-16;setAttr FKExtraKnee_L.rotateZ -9.473331815e-17;setAttr FKExtraKnee_L.scaleX 1;setAttr FKExtraKnee_L.scaleY 1;setAttr FKExtraKnee_L.scaleZ 1;setAttr FKKnee_L.translateX 0;setAttr FKKnee_L.translateY 0;setAttr FKKnee_L.translateZ 2.775557562e-17;setAttr FKKnee_L.rotateX -0;setAttr FKKnee_L.rotateY 0;setAttr FKKnee_L.rotateZ 0;setAttr FKKnee_L.scaleX 1;setAttr FKKnee_L.scaleY 1;setAttr FKKnee_L.scaleZ 1;setAttr FKExtraHip_L.translateX 4.440892099e-16;setAttr FKExtraHip_L.translateY -8.881784197e-16;setAttr FKExtraHip_L.translateZ 0;setAttr FKExtraHip_L.rotateX -1.553005216e-18;setAttr FKExtraHip_L.rotateY 1.739365841e-16;setAttr FKExtraHip_L.rotateZ 1.987846676e-16;setAttr FKExtraHip_L.scaleX 1;setAttr FKExtraHip_L.scaleY 1;setAttr FKExtraHip_L.scaleZ 1;setAttr FKHip_L.translateX 4.440892099e-16;setAttr FKHip_L.translateY -8.881784197e-16;setAttr FKHip_L.translateZ 0;setAttr FKHip_L.rotateX -0;setAttr FKHip_L.rotateY 0;setAttr FKHip_L.rotateZ 0;setAttr FKHip_L.scaleX 1;setAttr FKHip_L.scaleY 1;setAttr FKHip_L.scaleZ 1;setAttr FKExtraHipTwist_L.translateX -8.881784197e-16;setAttr FKExtraHipTwist_L.translateY 2.220446049e-16;setAttr FKExtraHipTwist_L.translateZ -6.661338148e-16;setAttr FKExtraHipTwist_L.rotateX 7.757372868e-12;setAttr FKExtraHipTwist_L.rotateY -7.951386704e-15;setAttr FKExtraHipTwist_L.rotateZ 7.156248033e-15;setAttr FKExtraHipTwist_L.scaleX 1;setAttr FKExtraHipTwist_L.scaleY 1;setAttr FKExtraHipTwist_L.scaleZ 1;setAttr FKHipTwist_L.translateX 8.881784197e-16;setAttr FKHipTwist_L.translateY 0;setAttr FKHipTwist_L.translateZ 0;setAttr FKHipTwist_L.rotateX -0;setAttr FKHipTwist_L.rotateY 0;setAttr FKHipTwist_L.rotateZ 0;setAttr FKHipTwist_L.scaleX 1;setAttr FKHipTwist_L.scaleY 1;setAttr FKHipTwist_L.scaleZ 1;setAttr HipSwingerPelvis_M.rotateX 0;setAttr HipSwingerPelvis_M.rotateY -0;setAttr HipSwingerPelvis_M.rotateZ 0;setAttr IKExtraArm_R.translateX 0;setAttr IKExtraArm_R.translateY 8.881784197e-16;setAttr IKExtraArm_R.translateZ 0;setAttr IKExtraArm_R.rotateX -0;setAttr IKExtraArm_R.rotateY 0;setAttr IKExtraArm_R.rotateZ -0;setAttr IKArm_R.translateX 0;setAttr IKArm_R.translateY 0;setAttr IKArm_R.translateZ 0;setAttr IKArm_R.rotateX -0;setAttr IKArm_R.rotateY 0;setAttr IKArm_R.rotateZ -0;setAttr IKArm_R.follow 0;setAttr PoleExtraArm_R.translateX 0;setAttr PoleExtraArm_R.translateY 0;setAttr PoleExtraArm_R.translateZ 0;setAttr PoleArm_R.translateX 0;setAttr PoleArm_R.translateY 0;setAttr PoleArm_R.translateZ 0;setAttr PoleArm_R.follow 0;setAttr Main.visibility 1;setAttr Main.translateX 0;setAttr Main.translateY 0;setAttr Main.translateZ 0;setAttr Main.rotateX 0;setAttr Main.rotateY 0;setAttr Main.rotateZ 0;setAttr Main.scaleX 1;setAttr Main.scaleY 1;setAttr Main.scaleZ 1;setAttr FKExtraMiddleFinger2_R.translateX 0;setAttr FKExtraMiddleFinger2_R.translateY 7.771561172e-16;setAttr FKExtraMiddleFinger2_R.translateZ -1.776356839e-15;setAttr FKExtraMiddleFinger2_R.rotateX -0;setAttr FKExtraMiddleFinger2_R.rotateY 0;setAttr FKExtraMiddleFinger2_R.rotateZ -0;setAttr FKExtraMiddleFinger2_R.scaleX 1;setAttr FKExtraMiddleFinger2_R.scaleY 1;setAttr FKExtraMiddleFinger2_R.scaleZ 1;setAttr FKMiddleFinger2_R.translateX 0;setAttr FKMiddleFinger2_R.translateY 4.440892099e-16;setAttr FKMiddleFinger2_R.translateZ -8.881784197e-16;setAttr FKMiddleFinger2_R.rotateX -0;setAttr FKMiddleFinger2_R.rotateY 0;setAttr FKMiddleFinger2_R.rotateZ -0;setAttr FKMiddleFinger2_R.scaleX 1;setAttr FKMiddleFinger2_R.scaleY 1;setAttr FKMiddleFinger2_R.scaleZ 1;setAttr FKExtraMiddleFinger1_R.translateX -1.776356839e-15;setAttr FKExtraMiddleFinger1_R.translateY -8.881784197e-16;setAttr FKExtraMiddleFinger1_R.translateZ 0;setAttr FKExtraMiddleFinger1_R.rotateX -0;setAttr FKExtraMiddleFinger1_R.rotateY 0;setAttr FKExtraMiddleFinger1_R.rotateZ -0;setAttr FKExtraMiddleFinger1_R.scaleX 1;setAttr FKExtraMiddleFinger1_R.scaleY 1;setAttr FKExtraMiddleFinger1_R.scaleZ 1;setAttr FKMiddleFinger1_R.translateX 0;setAttr FKMiddleFinger1_R.translateY 8.881784197e-16;setAttr FKMiddleFinger1_R.translateZ -8.881784197e-16;setAttr FKMiddleFinger1_R.rotateX -0;setAttr FKMiddleFinger1_R.rotateY 0;setAttr FKMiddleFinger1_R.rotateZ -0;setAttr FKMiddleFinger1_R.scaleX 1;setAttr FKMiddleFinger1_R.scaleY 1;setAttr FKMiddleFinger1_R.scaleZ 1;setAttr FKExtraIndexFinger2_R.translateX -8.881784197e-16;setAttr FKExtraIndexFinger2_R.translateY 4.440892099e-16;setAttr FKExtraIndexFinger2_R.translateZ 0;setAttr FKExtraIndexFinger2_R.rotateX -0;setAttr FKExtraIndexFinger2_R.rotateY 0;setAttr FKExtraIndexFinger2_R.rotateZ -0;setAttr FKExtraIndexFinger2_R.scaleX 1;setAttr FKExtraIndexFinger2_R.scaleY 1;setAttr FKExtraIndexFinger2_R.scaleZ 1;setAttr FKIndexFinger2_R.translateX 0;setAttr FKIndexFinger2_R.translateY -4.440892099e-16;setAttr FKIndexFinger2_R.translateZ -8.881784197e-16;setAttr FKIndexFinger2_R.rotateX -0;setAttr FKIndexFinger2_R.rotateY 0;setAttr FKIndexFinger2_R.rotateZ -0;setAttr FKIndexFinger2_R.scaleX 1;setAttr FKIndexFinger2_R.scaleY 1;setAttr FKIndexFinger2_R.scaleZ 1;setAttr FKExtraIndexFinger1_R.translateX 0;setAttr FKExtraIndexFinger1_R.translateY -8.881784197e-16;setAttr FKExtraIndexFinger1_R.translateZ -1.776356839e-15;setAttr FKExtraIndexFinger1_R.rotateX -0;setAttr FKExtraIndexFinger1_R.rotateY 0;setAttr FKExtraIndexFinger1_R.rotateZ -0;setAttr FKExtraIndexFinger1_R.scaleX 1;setAttr FKExtraIndexFinger1_R.scaleY 1;setAttr FKExtraIndexFinger1_R.scaleZ 1;setAttr FKIndexFinger1_R.translateX 8.881784197e-16;setAttr FKIndexFinger1_R.translateY 0;setAttr FKIndexFinger1_R.translateZ -8.881784197e-16;setAttr FKIndexFinger1_R.rotateX -0;setAttr FKIndexFinger1_R.rotateY 0;setAttr FKIndexFinger1_R.rotateZ -0;setAttr FKIndexFinger1_R.scaleX 1;setAttr FKIndexFinger1_R.scaleY 1;setAttr FKIndexFinger1_R.scaleZ 1;setAttr FKExtraThumbFinger2_R.translateX 0;setAttr FKExtraThumbFinger2_R.translateY 4.440892099e-16;setAttr FKExtraThumbFinger2_R.translateZ -8.881784197e-16;setAttr FKExtraThumbFinger2_R.rotateX -3.180554681e-15;setAttr FKExtraThumbFinger2_R.rotateY 3.180554681e-15;setAttr FKExtraThumbFinger2_R.rotateZ 0;setAttr FKExtraThumbFinger2_R.scaleX 1;setAttr FKExtraThumbFinger2_R.scaleY 1;setAttr FKExtraThumbFinger2_R.scaleZ 1;setAttr FKThumbFinger2_R.translateX 0;setAttr FKThumbFinger2_R.translateY 4.440892099e-16;setAttr FKThumbFinger2_R.translateZ 0;setAttr FKThumbFinger2_R.rotateX -6.361109363e-15;setAttr FKThumbFinger2_R.rotateY 3.180554681e-15;setAttr FKThumbFinger2_R.rotateZ 7.951386704e-16;setAttr FKThumbFinger2_R.scaleX 1;setAttr FKThumbFinger2_R.scaleY 1;setAttr FKThumbFinger2_R.scaleZ 1;setAttr FKExtraThumbFinger1_R.translateX -4.440892099e-16;setAttr FKExtraThumbFinger1_R.translateY 8.881784197e-16;setAttr FKExtraThumbFinger1_R.translateZ 8.881784197e-16;setAttr FKExtraThumbFinger1_R.rotateX -1.908332809e-14;setAttr FKExtraThumbFinger1_R.rotateY -1.590277341e-14;setAttr FKExtraThumbFinger1_R.rotateZ 0;setAttr FKExtraThumbFinger1_R.scaleX 1;setAttr FKExtraThumbFinger1_R.scaleY 1;setAttr FKExtraThumbFinger1_R.scaleZ 1;setAttr FKThumbFinger1_R.translateX -6.661338148e-16;setAttr FKThumbFinger1_R.translateY -8.881784197e-16;setAttr FKThumbFinger1_R.translateZ 0;setAttr FKThumbFinger1_R.rotateX -1.908332809e-14;setAttr FKThumbFinger1_R.rotateY 3.180554681e-15;setAttr FKThumbFinger1_R.rotateZ 4.770832022e-15;setAttr FKThumbFinger1_R.scaleX 1;setAttr FKThumbFinger1_R.scaleY 1;setAttr FKThumbFinger1_R.scaleZ 1;setAttr FKExtraWrist_R.translateX 4.440892099e-16;setAttr FKExtraWrist_R.translateY -1.110223025e-16;setAttr FKExtraWrist_R.translateZ 1.776356839e-15;setAttr FKExtraWrist_R.rotateX -0;setAttr FKExtraWrist_R.rotateY 0;setAttr FKExtraWrist_R.rotateZ -0;setAttr FKExtraWrist_R.scaleX 1;setAttr FKExtraWrist_R.scaleY 1;setAttr FKExtraWrist_R.scaleZ 1;setAttr FKWrist_R.translateX 2.220446049e-15;setAttr FKWrist_R.translateY -1.110223025e-16;setAttr FKWrist_R.translateZ 0;setAttr FKWrist_R.rotateX -0;setAttr FKWrist_R.rotateY 0;setAttr FKWrist_R.rotateZ -0;setAttr FKWrist_R.scaleX 1;setAttr FKWrist_R.scaleY 1;setAttr FKWrist_R.scaleZ 1;setAttr FKExtraElbow_R.translateX 6.661338148e-16;setAttr FKExtraElbow_R.translateY -2.220446049e-16;setAttr FKExtraElbow_R.translateZ 0;setAttr FKExtraElbow_R.rotateX -0;setAttr FKExtraElbow_R.rotateY 0;setAttr FKExtraElbow_R.rotateZ -0;setAttr FKExtraElbow_R.scaleX 1;setAttr FKExtraElbow_R.scaleY 1;setAttr FKExtraElbow_R.scaleZ 1;setAttr FKElbow_R.translateX -2.220446049e-16;setAttr FKElbow_R.translateY 0;setAttr FKElbow_R.translateZ 0;setAttr FKElbow_R.rotateX -0;setAttr FKElbow_R.rotateY 0;setAttr FKElbow_R.rotateZ -0;setAttr FKElbow_R.scaleX 1;setAttr FKElbow_R.scaleY 1;setAttr FKElbow_R.scaleZ 1;setAttr FKExtraShoulder_R.translateX 2.220446049e-16;setAttr FKExtraShoulder_R.translateY 8.881784197e-16;setAttr FKExtraShoulder_R.translateZ 0;setAttr FKExtraShoulder_R.rotateX -0;setAttr FKExtraShoulder_R.rotateY 0;setAttr FKExtraShoulder_R.rotateZ -0;setAttr FKExtraShoulder_R.scaleX 1;setAttr FKExtraShoulder_R.scaleY 1;setAttr FKExtraShoulder_R.scaleZ 1;setAttr FKShoulder_R.translateX 0;setAttr FKShoulder_R.translateY 8.881784197e-16;setAttr FKShoulder_R.translateZ 0;setAttr FKShoulder_R.rotateX -0;setAttr FKShoulder_R.rotateY 0;setAttr FKShoulder_R.rotateZ -0;setAttr FKShoulder_R.scaleX 1;setAttr FKShoulder_R.scaleY 1;setAttr FKShoulder_R.scaleZ 1;setAttr FKExtraClavicle_R.translateX 0;setAttr FKExtraClavicle_R.translateY 0;setAttr FKExtraClavicle_R.translateZ 0;setAttr FKExtraClavicle_R.rotateX -0;setAttr FKExtraClavicle_R.rotateY 0;setAttr FKExtraClavicle_R.rotateZ 0;setAttr FKExtraClavicle_R.scaleX 1;setAttr FKExtraClavicle_R.scaleY 1;setAttr FKExtraClavicle_R.scaleZ 1;setAttr FKClavicle_R.translateX 0;setAttr FKClavicle_R.translateY 8.881784197e-16;setAttr FKClavicle_R.translateZ 0;setAttr FKClavicle_R.rotateX -0;setAttr FKClavicle_R.rotateY 0;setAttr FKClavicle_R.rotateZ 0;setAttr FKClavicle_R.scaleX 1;setAttr FKClavicle_R.scaleY 1;setAttr FKClavicle_R.scaleZ 1;setAttr FKExtraShoulderPad1_R.translateX 0;setAttr FKExtraShoulderPad1_R.translateY 2.220446049e-16;setAttr FKExtraShoulderPad1_R.translateZ 0;setAttr FKExtraShoulderPad1_R.rotateX -0;setAttr FKExtraShoulderPad1_R.rotateY 0;setAttr FKExtraShoulderPad1_R.rotateZ 0;setAttr FKExtraShoulderPad1_R.scaleX 1;setAttr FKExtraShoulderPad1_R.scaleY 1;setAttr FKExtraShoulderPad1_R.scaleZ 1;setAttr FKShoulderPad1_R.translateX 0;setAttr FKShoulderPad1_R.translateY 0;setAttr FKShoulderPad1_R.translateZ 0;setAttr FKShoulderPad1_R.rotateX -0;setAttr FKShoulderPad1_R.rotateY 0;setAttr FKShoulderPad1_R.rotateZ 0;setAttr FKShoulderPad1_R.scaleX 1;setAttr FKShoulderPad1_R.scaleY 1;setAttr FKShoulderPad1_R.scaleZ 1;setAttr FKExtraShoulderPad2_R.translateX 1.110223025e-16;setAttr FKExtraShoulderPad2_R.translateY -2.220446049e-16;setAttr FKExtraShoulderPad2_R.translateZ -1.776356839e-15;setAttr FKExtraShoulderPad2_R.rotateX -0;setAttr FKExtraShoulderPad2_R.rotateY 0;setAttr FKExtraShoulderPad2_R.rotateZ 0;setAttr FKExtraShoulderPad2_R.scaleX 1;setAttr FKExtraShoulderPad2_R.scaleY 1;setAttr FKExtraShoulderPad2_R.scaleZ 1;setAttr FKShoulderPad2_R.translateX 1.110223025e-16;setAttr FKShoulderPad2_R.translateY 0;setAttr FKShoulderPad2_R.translateZ -1.776356839e-15;setAttr FKShoulderPad2_R.rotateX -0;setAttr FKShoulderPad2_R.rotateY 0;setAttr FKShoulderPad2_R.rotateZ 0;setAttr FKShoulderPad2_R.scaleX 1;setAttr FKShoulderPad2_R.scaleY 1;setAttr FKShoulderPad2_R.scaleZ 1;setAttr FKIKArm_R.FKIKBlend 0;setAttr FKIKArm_R.FKVis 1;setAttr FKIKArm_R.IKVis 0;setAttr IKExtraLeg_R.translateX 0;setAttr IKExtraLeg_R.translateY 0;setAttr IKExtraLeg_R.translateZ -5.551115123e-17;setAttr IKExtraLeg_R.rotateX -0;setAttr IKExtraLeg_R.rotateY -0;setAttr IKExtraLeg_R.rotateZ 0;setAttr IKLeg_R.translateX 0;setAttr IKLeg_R.translateY 0;setAttr IKLeg_R.translateZ 0;setAttr IKLeg_R.rotateX -0;setAttr IKLeg_R.rotateY -0;setAttr IKLeg_R.rotateZ 0;setAttr IKLeg_R.swivel 0;setAttr IKLeg_R.roll 0;setAttr IKLeg_R.rollAngle 25;setAttr PoleExtraLeg_R.translateX 0;setAttr PoleExtraLeg_R.translateY 0;setAttr PoleExtraLeg_R.translateZ 0;setAttr PoleLeg_R.translateX 2.220446049e-16;setAttr PoleLeg_R.translateY -4.440892099e-16;setAttr PoleLeg_R.translateZ 0;setAttr PoleLeg_R.follow 10;setAttr FKIKLeg_R.FKIKBlend 10;setAttr FKIKLeg_R.FKVis 0;setAttr FKIKLeg_R.IKVis 1;setAttr IKExtraLegHeel_R.rotateX 0;setAttr IKExtraLegHeel_R.rotateY 9.93923338e-17;setAttr IKExtraLegHeel_R.rotateZ 0;setAttr IKLegHeel_R.rotateX 0;setAttr IKLegHeel_R.rotateY -0;setAttr IKLegHeel_R.rotateZ 0;setAttr IKExtraLegBall_R.rotateX 0;setAttr IKExtraLegBall_R.rotateY -0;setAttr IKExtraLegBall_R.rotateZ 0;setAttr IKLegBall_R.rotateX 0;setAttr IKExtraSpine0_M.visibility 1;setAttr IKExtraSpine0_M.translateX 0;setAttr IKExtraSpine0_M.translateY 0;setAttr IKExtraSpine0_M.translateZ 0;setAttr IKExtraSpine0_M.rotateX -0;setAttr IKExtraSpine0_M.rotateY -0;setAttr IKExtraSpine0_M.rotateZ 0;setAttr IKExtraSpine0_M.scaleX 1;setAttr IKExtraSpine0_M.scaleY 1;setAttr IKExtraSpine0_M.scaleZ 1;setAttr IKSpine0_M.translateX 0;setAttr IKSpine0_M.translateY 0;setAttr IKSpine0_M.translateZ 0;setAttr IKSpine0_M.rotateX -0;setAttr IKSpine0_M.rotateY -0;setAttr IKSpine0_M.rotateZ 0;setAttr IKSpine0_M.stiff 0;setAttr IKExtraSpine2_M.visibility 1;setAttr IKExtraSpine2_M.translateX 0;setAttr IKExtraSpine2_M.translateY 0;setAttr IKExtraSpine2_M.translateZ 0;setAttr IKExtraSpine2_M.rotateX 0;setAttr IKExtraSpine2_M.rotateY -0;setAttr IKExtraSpine2_M.rotateZ 0;setAttr IKExtraSpine2_M.scaleX 1;setAttr IKExtraSpine2_M.scaleY 1;setAttr IKExtraSpine2_M.scaleZ 1;setAttr IKSpine2_M.translateX 0;setAttr IKSpine2_M.translateY 0;setAttr IKSpine2_M.translateZ 0;setAttr IKSpine2_M.rotateX 0;setAttr IKSpine2_M.rotateY -0;setAttr IKSpine2_M.rotateZ 0;setAttr IKSpine2_M.stiff 0;setAttr IKExtraSpine4_M.visibility 1;setAttr IKExtraSpine4_M.translateX 0;setAttr IKExtraSpine4_M.translateY 0;setAttr IKExtraSpine4_M.translateZ 0;setAttr IKExtraSpine4_M.rotateX 0;setAttr IKExtraSpine4_M.rotateY -0;setAttr IKExtraSpine4_M.rotateZ 0;setAttr IKExtraSpine4_M.scaleX 1;setAttr IKExtraSpine4_M.scaleY 1;setAttr IKExtraSpine4_M.scaleZ 1;setAttr IKSpine4_M.translateX 0;setAttr IKSpine4_M.translateY 0;setAttr IKSpine4_M.translateZ 0;setAttr IKSpine4_M.rotateX 0;setAttr IKSpine4_M.rotateY -0;setAttr IKSpine4_M.rotateZ 0;setAttr IKSpine4_M.stiff 0;setAttr IKSpine4_M.stretchy 0;setAttr FKIKLeg_L.FKIKBlend 10;setAttr FKIKLeg_L.FKVis 0;setAttr FKIKLeg_L.IKVis 1;setAttr IKExtraLegHeel_L.rotateX 0;setAttr IKExtraLegHeel_L.rotateY -0;setAttr IKExtraLegHeel_L.rotateZ 0;setAttr IKLegHeel_L.rotateX 0;setAttr IKLegHeel_L.rotateY -0;setAttr IKLegHeel_L.rotateZ 0;setAttr IKExtraLegBall_L.rotateX 0;setAttr IKExtraLegBall_L.rotateY 9.93923338e-17;setAttr IKExtraLegBall_L.rotateZ 0;setAttr IKLegBall_L.rotateX 0;setAttr CenterExtra_M.translateX 0;setAttr CenterExtra_M.translateY 0;setAttr CenterExtra_M.translateZ 0;setAttr CenterExtra_M.rotateX 0;setAttr CenterExtra_M.rotateY -0;setAttr CenterExtra_M.rotateZ 0;setAttr Center_M.translateX 0;setAttr Center_M.translateY 0;setAttr Center_M.translateZ 0;setAttr Center_M.rotateX 0;setAttr Center_M.rotateY -0;setAttr Center_M.rotateZ 0;setAttr Center_M.CenterBtwFeet 0;");
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -s 3 ".sol";
connectAttr "jointLayer.di" "Pelvis.do";
connectAttr "Pelvis.s" "SpineA.is";
connectAttr "jointLayer.di" "SpineA.do";
connectAttr "SpineA.s" "Chest.is";
connectAttr "jointLayer.di" "Chest.do";
connectAttr "Chest.s" "Clavicle.is";
connectAttr "jointLayer.di" "Clavicle.do";
connectAttr "Clavicle.s" "Shoulder.is";
connectAttr "jointLayer.di" "Shoulder.do";
connectAttr "Shoulder.s" "Elbow.is";
connectAttr "jointLayer.di" "Elbow.do";
connectAttr "Elbow.s" "Wrist.is";
connectAttr "jointLayer.di" "Wrist.do";
connectAttr "Wrist.s" "MiddleFinger1.is";
connectAttr "jointLayer.di" "MiddleFinger1.do";
connectAttr "MiddleFinger1.s" "MiddleFinger2.is";
connectAttr "jointLayer.di" "MiddleFinger2.do";
connectAttr "jointLayer.di" "MiddleFinger3_End.do";
connectAttr "MiddleFinger2.s" "MiddleFinger3_End.is";
connectAttr "Wrist.s" "IndexFinger1.is";
connectAttr "jointLayer.di" "IndexFinger1.do";
connectAttr "IndexFinger1.s" "IndexFinger2.is";
connectAttr "jointLayer.di" "IndexFinger2.do";
connectAttr "jointLayer.di" "IndexFinger3_End.do";
connectAttr "IndexFinger2.s" "IndexFinger3_End.is";
connectAttr "Wrist.s" "ThumbFinger1.is";
connectAttr "jointLayer.di" "ThumbFinger1.do";
connectAttr "ThumbFinger1.s" "ThumbFinger2.is";
connectAttr "jointLayer.di" "ThumbFinger2.do";
connectAttr "jointLayer.di" "ThumbFinger3_End.do";
connectAttr "ThumbFinger2.s" "ThumbFinger3_End.is";
connectAttr "Chest.s" "Chest_End.is";
connectAttr "jointLayer.di" "Chest_End.do";
connectAttr "Chest.s" "ShoulderPad1.is";
connectAttr "jointLayer.di" "ShoulderPad1.do";
connectAttr "jointLayer.di" "ShoulderPad1_End.do";
connectAttr "Chest.s" "ShoulderPad2.is";
connectAttr "jointLayer.di" "ShoulderPad2.do";
connectAttr "jointLayer.di" "ShoulderPad2_End.do";
connectAttr "Pelvis.s" "HipTwist.is";
connectAttr "jointLayer.di" "HipTwist.do";
connectAttr "jointLayer.di" "Hip.do";
connectAttr "Hip.s" "Knee.is";
connectAttr "jointLayer.di" "Knee.do";
connectAttr "Knee.s" "Ankle.is";
connectAttr "jointLayer.di" "Ankle.do";
connectAttr "Ankle.s" "Foot_End.is";
connectAttr "jointLayer.di" "Foot_End.do";
connectAttr "Ankle.s" "Heel_End.is";
connectAttr "jointLayer.di" "Heel_End.do";
connectAttr "Main.fkVis" "FKSystem.v";
connectAttr "Main.fingerVis" "FKParentConstraintToWrist_R.v";
connectAttr "FKParentConstraintToWrist_R_parentConstraint1.ctx" "FKParentConstraintToWrist_R.tx"
		;
connectAttr "FKParentConstraintToWrist_R_parentConstraint1.cty" "FKParentConstraintToWrist_R.ty"
		;
connectAttr "FKParentConstraintToWrist_R_parentConstraint1.ctz" "FKParentConstraintToWrist_R.tz"
		;
connectAttr "FKParentConstraintToWrist_R_parentConstraint1.crx" "FKParentConstraintToWrist_R.rx"
		;
connectAttr "FKParentConstraintToWrist_R_parentConstraint1.cry" "FKParentConstraintToWrist_R.ry"
		;
connectAttr "FKParentConstraintToWrist_R_parentConstraint1.crz" "FKParentConstraintToWrist_R.rz"
		;
connectAttr "jointLayer.di" "FKOffsetMiddleFinger1_R.do";
connectAttr "jointLayer.di" "FKXMiddleFinger1_R.do";
connectAttr "FKXMiddleFinger1_R.s" "FKOffsetMiddleFinger2_R.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger2_R.do";
connectAttr "FKMiddleFinger1_R.s" "FKXMiddleFinger2_R.is";
connectAttr "jointLayer.di" "FKXMiddleFinger2_R.do";
connectAttr "FKMiddleFinger2_R.s" "FKXMiddleFinger3_End_R.is";
connectAttr "jointLayer.di" "FKXMiddleFinger3_End_R.do";
connectAttr "jointLayer.di" "FKOffsetIndexFinger1_R.do";
connectAttr "jointLayer.di" "FKXIndexFinger1_R.do";
connectAttr "FKXIndexFinger1_R.s" "FKOffsetIndexFinger2_R.is";
connectAttr "jointLayer.di" "FKOffsetIndexFinger2_R.do";
connectAttr "FKIndexFinger1_R.s" "FKXIndexFinger2_R.is";
connectAttr "jointLayer.di" "FKXIndexFinger2_R.do";
connectAttr "FKIndexFinger2_R.s" "FKXIndexFinger3_End_R.is";
connectAttr "jointLayer.di" "FKXIndexFinger3_End_R.do";
connectAttr "jointLayer.di" "FKOffsetThumbFinger1_R.do";
connectAttr "jointLayer.di" "FKXThumbFinger1_R.do";
connectAttr "FKXThumbFinger1_R.s" "FKOffsetThumbFinger2_R.is";
connectAttr "jointLayer.di" "FKOffsetThumbFinger2_R.do";
connectAttr "FKThumbFinger1_R.s" "FKXThumbFinger2_R.is";
connectAttr "jointLayer.di" "FKXThumbFinger2_R.do";
connectAttr "FKThumbFinger2_R.s" "FKXThumbFinger3_End_R.is";
connectAttr "jointLayer.di" "FKXThumbFinger3_End_R.do";
connectAttr "FKParentConstraintToWrist_R.ro" "FKParentConstraintToWrist_R_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToWrist_R.pim" "FKParentConstraintToWrist_R_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToWrist_R.rp" "FKParentConstraintToWrist_R_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToWrist_R.rpt" "FKParentConstraintToWrist_R_parentConstraint1.crt"
		;
connectAttr "Wrist_R.t" "FKParentConstraintToWrist_R_parentConstraint1.tg[0].tt"
		;
connectAttr "Wrist_R.rp" "FKParentConstraintToWrist_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Wrist_R.rpt" "FKParentConstraintToWrist_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Wrist_R.r" "FKParentConstraintToWrist_R_parentConstraint1.tg[0].tr"
		;
connectAttr "Wrist_R.ro" "FKParentConstraintToWrist_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Wrist_R.s" "FKParentConstraintToWrist_R_parentConstraint1.tg[0].ts"
		;
connectAttr "Wrist_R.pm" "FKParentConstraintToWrist_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Wrist_R.jo" "FKParentConstraintToWrist_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToWrist_R_parentConstraint1.w0" "FKParentConstraintToWrist_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.ctx" "FKParentConstraintToChest_M.tx"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.cty" "FKParentConstraintToChest_M.ty"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.ctz" "FKParentConstraintToChest_M.tz"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.crx" "FKParentConstraintToChest_M.rx"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.cry" "FKParentConstraintToChest_M.ry"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.crz" "FKParentConstraintToChest_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetClavicle_R.do";
connectAttr "jointLayer.di" "FKXClavicle_R.do";
connectAttr "FKXClavicle_R.s" "FKOffsetShoulder_R.is";
connectAttr "FKIKBlendArmCondition_R.ocg" "FKOffsetShoulder_R.v" -l on;
connectAttr "jointLayer.di" "FKOffsetShoulder_R.do";
connectAttr "jointLayer.di" "FKXShoulder_R.do";
connectAttr "FKXShoulder_R.s" "FKOffsetElbow_R.is";
connectAttr "jointLayer.di" "FKOffsetElbow_R.do";
connectAttr "FKShoulder_R.s" "FKXElbow_R.is";
connectAttr "jointLayer.di" "FKXElbow_R.do";
connectAttr "FKXElbow_R.s" "FKOffsetWrist_R.is";
connectAttr "jointLayer.di" "FKOffsetWrist_R.do";
connectAttr "FKElbow_R.s" "FKXWrist_R.is";
connectAttr "jointLayer.di" "FKXWrist_R.do";
connectAttr "jointLayer.di" "FKOffsetShoulderPad1_R.do";
connectAttr "jointLayer.di" "FKXShoulderPad1_R.do";
connectAttr "FKShoulderPad1_R.s" "FKXShoulderPad1_End_R.is";
connectAttr "jointLayer.di" "FKXShoulderPad1_End_R.do";
connectAttr "jointLayer.di" "FKOffsetShoulderPad2_R.do";
connectAttr "jointLayer.di" "FKXShoulderPad2_R.do";
connectAttr "FKShoulderPad2_R.s" "FKXShoulderPad2_End_R.is";
connectAttr "jointLayer.di" "FKXShoulderPad2_End_R.do";
connectAttr "jointLayer.di" "FKOffsetClavicle_L.do";
connectAttr "jointLayer.di" "FKXClavicle_L.do";
connectAttr "FKXClavicle_L.s" "FKOffsetShoulder_L.is";
connectAttr "FKIKBlendArmCondition_L.ocg" "FKOffsetShoulder_L.v" -l on;
connectAttr "jointLayer.di" "FKOffsetShoulder_L.do";
connectAttr "jointLayer.di" "FKXShoulder_L.do";
connectAttr "FKXShoulder_L.s" "FKOffsetElbow_L.is";
connectAttr "jointLayer.di" "FKOffsetElbow_L.do";
connectAttr "FKShoulder_L.s" "FKXElbow_L.is";
connectAttr "jointLayer.di" "FKXElbow_L.do";
connectAttr "FKXElbow_L.s" "FKOffsetWrist_L.is";
connectAttr "jointLayer.di" "FKOffsetWrist_L.do";
connectAttr "FKElbow_L.s" "FKXWrist_L.is";
connectAttr "jointLayer.di" "FKXWrist_L.do";
connectAttr "jointLayer.di" "FKOffsetShoulderPad1_L.do";
connectAttr "jointLayer.di" "FKXShoulderPad1_L.do";
connectAttr "FKShoulderPad1_L.s" "FKXShoulderPad1_End_L.is";
connectAttr "jointLayer.di" "FKXShoulderPad1_End_L.do";
connectAttr "jointLayer.di" "FKOffsetShoulderPad2_L.do";
connectAttr "jointLayer.di" "FKXShoulderPad2_L.do";
connectAttr "FKShoulderPad2_L.s" "FKXShoulderPad2_End_L.is";
connectAttr "jointLayer.di" "FKXShoulderPad2_End_L.do";
connectAttr "FKParentConstraintToChest_M.ro" "FKParentConstraintToChest_M_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToChest_M.pim" "FKParentConstraintToChest_M_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToChest_M.rp" "FKParentConstraintToChest_M_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToChest_M.rpt" "FKParentConstraintToChest_M_parentConstraint1.crt"
		;
connectAttr "Chest_M.t" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Chest_M.rp" "FKParentConstraintToChest_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Chest_M.rpt" "FKParentConstraintToChest_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Chest_M.r" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Chest_M.ro" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Chest_M.s" "FKParentConstraintToChest_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Chest_M.pm" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "Chest_M.jo" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.w0" "FKParentConstraintToChest_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.ctx" "FKParentConstraintToPelvis_M.tx"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.cty" "FKParentConstraintToPelvis_M.ty"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.ctz" "FKParentConstraintToPelvis_M.tz"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.crx" "FKParentConstraintToPelvis_M.rx"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.cry" "FKParentConstraintToPelvis_M.ry"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.crz" "FKParentConstraintToPelvis_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetHipTwist_R.do";
connectAttr "jointLayer.di" "FKXHipTwist_R.do";
connectAttr "FKXHipTwist_R.s" "FKOffsetHip_R.is";
connectAttr "FKIKBlendLegCondition_R.ocg" "FKOffsetHip_R.v" -l on;
connectAttr "jointLayer.di" "FKOffsetHip_R.do";
connectAttr "jointLayer.di" "FKXHip_R.do";
connectAttr "FKXHip_R.s" "FKOffsetKnee_R.is";
connectAttr "jointLayer.di" "FKOffsetKnee_R.do";
connectAttr "FKHip_R.s" "FKXKnee_R.is";
connectAttr "jointLayer.di" "FKXKnee_R.do";
connectAttr "FKXKnee_R.s" "FKOffsetAnkle_R.is";
connectAttr "jointLayer.di" "FKOffsetAnkle_R.do";
connectAttr "FKKnee_R.s" "FKXAnkle_R.is";
connectAttr "jointLayer.di" "FKXAnkle_R.do";
connectAttr "FKAnkle_R.s" "FKXFoot_End_R.is";
connectAttr "jointLayer.di" "FKXFoot_End_R.do";
connectAttr "FKAnkle_R.s" "FKXHeel_End_R.is";
connectAttr "jointLayer.di" "FKXHeel_End_R.do";
connectAttr "jointLayer.di" "FKOffsetHipTwist_L.do";
connectAttr "jointLayer.di" "FKXHipTwist_L.do";
connectAttr "FKXHipTwist_L.s" "FKOffsetHip_L.is";
connectAttr "FKIKBlendLegCondition_L.ocg" "FKOffsetHip_L.v" -l on;
connectAttr "jointLayer.di" "FKOffsetHip_L.do";
connectAttr "jointLayer.di" "FKXHip_L.do";
connectAttr "FKXHip_L.s" "FKOffsetKnee_L.is";
connectAttr "jointLayer.di" "FKOffsetKnee_L.do";
connectAttr "FKHip_L.s" "FKXKnee_L.is";
connectAttr "jointLayer.di" "FKXKnee_L.do";
connectAttr "FKXKnee_L.s" "FKOffsetAnkle_L.is";
connectAttr "jointLayer.di" "FKOffsetAnkle_L.do";
connectAttr "FKKnee_L.s" "FKXAnkle_L.is";
connectAttr "jointLayer.di" "FKXAnkle_L.do";
connectAttr "FKAnkle_L.s" "FKXFoot_End_L.is";
connectAttr "jointLayer.di" "FKXFoot_End_L.do";
connectAttr "FKAnkle_L.s" "FKXHeel_End_L.is";
connectAttr "jointLayer.di" "FKXHeel_End_L.do";
connectAttr "FKParentConstraintToPelvis_M.ro" "FKParentConstraintToPelvis_M_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToPelvis_M.pim" "FKParentConstraintToPelvis_M_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToPelvis_M.rp" "FKParentConstraintToPelvis_M_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToPelvis_M.rpt" "FKParentConstraintToPelvis_M_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Pelvis_M.rp" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Pelvis_M.ro" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Pelvis_M.pm" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.w0" "FKParentConstraintToPelvis_M_parentConstraint1.tg[0].tw"
		;
connectAttr "Main.fingerVis" "FKParentConstraintToWrist_L.v";
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.ctx" "FKParentConstraintToWrist_L.tx"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.cty" "FKParentConstraintToWrist_L.ty"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.ctz" "FKParentConstraintToWrist_L.tz"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.crx" "FKParentConstraintToWrist_L.rx"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.cry" "FKParentConstraintToWrist_L.ry"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.crz" "FKParentConstraintToWrist_L.rz"
		;
connectAttr "jointLayer.di" "FKOffsetMiddleFinger1_L.do";
connectAttr "jointLayer.di" "FKXMiddleFinger1_L.do";
connectAttr "FKXMiddleFinger1_L.s" "FKOffsetMiddleFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger2_L.do";
connectAttr "FKMiddleFinger1_L.s" "FKXMiddleFinger2_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger2_L.do";
connectAttr "FKMiddleFinger2_L.s" "FKXMiddleFinger3_End_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger3_End_L.do";
connectAttr "jointLayer.di" "FKOffsetIndexFinger1_L.do";
connectAttr "jointLayer.di" "FKXIndexFinger1_L.do";
connectAttr "FKXIndexFinger1_L.s" "FKOffsetIndexFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetIndexFinger2_L.do";
connectAttr "FKIndexFinger1_L.s" "FKXIndexFinger2_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger2_L.do";
connectAttr "FKIndexFinger2_L.s" "FKXIndexFinger3_End_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger3_End_L.do";
connectAttr "jointLayer.di" "FKOffsetThumbFinger1_L.do";
connectAttr "jointLayer.di" "FKXThumbFinger1_L.do";
connectAttr "FKXThumbFinger1_L.s" "FKOffsetThumbFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetThumbFinger2_L.do";
connectAttr "FKThumbFinger1_L.s" "FKXThumbFinger2_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger2_L.do";
connectAttr "FKThumbFinger2_L.s" "FKXThumbFinger3_End_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger3_End_L.do";
connectAttr "FKParentConstraintToWrist_L.ro" "FKParentConstraintToWrist_L_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToWrist_L.pim" "FKParentConstraintToWrist_L_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToWrist_L.rp" "FKParentConstraintToWrist_L_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToWrist_L.rpt" "FKParentConstraintToWrist_L_parentConstraint1.crt"
		;
connectAttr "Wrist_L.t" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Wrist_L.rp" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Wrist_L.rpt" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Wrist_L.r" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Wrist_L.ro" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Wrist_L.s" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Wrist_L.pm" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Wrist_L.jo" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.w0" "FKParentConstraintToWrist_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctx" "PelvisCenterBtwLegsBlended_M.tx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cty" "PelvisCenterBtwLegsBlended_M.ty"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctz" "PelvisCenterBtwLegsBlended_M.tz"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crx" "PelvisCenterBtwLegsBlended_M.rx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cry" "PelvisCenterBtwLegsBlended_M.ry"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crz" "PelvisCenterBtwLegsBlended_M.rz"
		;
connectAttr "FKIKBlendSpineCondition_M.ocg" "FKOffsetPelvis_M.v" -l on;
connectAttr "jointLayer.di" "FKOffsetPelvis_M.do";
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.crx" "HipSwingerGroupPelvis_M.rx"
		;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.cry" "HipSwingerGroupPelvis_M.ry"
		;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.crz" "HipSwingerGroupPelvis_M.rz"
		;
connectAttr "HipSwingerGroupPelvis_M.ro" "HipSwingerGroupPelvis_M_orientConstraint1.cro"
		;
connectAttr "HipSwingerGroupPelvis_M.pim" "HipSwingerGroupPelvis_M_orientConstraint1.cpim"
		;
connectAttr "HipSwingerPelvis_M.r" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tr"
		;
connectAttr "HipSwingerPelvis_M.ro" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tro"
		;
connectAttr "HipSwingerPelvis_M.pm" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tpm"
		;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.w0" "HipSwingerGroupPelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "jointLayer.di" "FKXPelvis_M.do";
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.crx" "HipSwingerStabalizePelvis_M.rx"
		;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.cry" "HipSwingerStabalizePelvis_M.ry"
		;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.crz" "HipSwingerStabalizePelvis_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetSpineA_M.do";
connectAttr "FKPelvis_M.s" "FKXSpineA_M.is";
connectAttr "jointLayer.di" "FKXSpineA_M.do";
connectAttr "FKXSpineA_M.s" "FKOffsetChest_M.is";
connectAttr "jointLayer.di" "FKOffsetChest_M.do";
connectAttr "FKSpineA_M.s" "FKXChest_M.is";
connectAttr "jointLayer.di" "FKXChest_M.do";
connectAttr "FKChest_M.s" "FKXChest_End_M.is";
connectAttr "jointLayer.di" "FKXChest_End_M.do";
connectAttr "HipSwingerStabalizePelvis_M.ro" "HipSwingerStabalizePelvis_M_orientConstraint1.cro"
		;
connectAttr "HipSwingerStabalizePelvis_M.pim" "HipSwingerStabalizePelvis_M_orientConstraint1.cpim"
		;
connectAttr "Center_M.r" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tro"
		;
connectAttr "Center_M.pm" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tpm"
		;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.w0" "HipSwingerStabalizePelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocg" "HipSwingerPelvis_M.v" -l on;
connectAttr "PelvisCenterBtwLegsBlended_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.pim" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crt"
		;
connectAttr "PelvisCenter_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tt"
		;
connectAttr "PelvisCenter_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trp"
		;
connectAttr "PelvisCenter_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trt"
		;
connectAttr "PelvisCenter_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tr"
		;
connectAttr "PelvisCenter_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tro"
		;
connectAttr "PelvisCenter_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].ts"
		;
connectAttr "PelvisCenter_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trp"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tr"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tro"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].ts"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tw"
		;
connectAttr "PelvisCenterBtwFeetReverse_M.ox" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0"
		;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1"
		;
connectAttr "Main.ikVis" "IKSystem.v";
connectAttr "IKParentConstraintShoulder_R_parentConstraint1.ctx" "IKParentConstraintShoulder_R.tx"
		;
connectAttr "IKParentConstraintShoulder_R_parentConstraint1.cty" "IKParentConstraintShoulder_R.ty"
		;
connectAttr "IKParentConstraintShoulder_R_parentConstraint1.ctz" "IKParentConstraintShoulder_R.tz"
		;
connectAttr "IKParentConstraintShoulder_R_parentConstraint1.crx" "IKParentConstraintShoulder_R.rx"
		;
connectAttr "IKParentConstraintShoulder_R_parentConstraint1.cry" "IKParentConstraintShoulder_R.ry"
		;
connectAttr "IKParentConstraintShoulder_R_parentConstraint1.crz" "IKParentConstraintShoulder_R.rz"
		;
connectAttr "FKIKBlendArmCondition_R.ocr" "IKParentConstraintShoulder_R.v";
connectAttr "jointLayer.di" "IKXShoulder_R.do";
connectAttr "IKXShoulder_R.s" "IKXElbow_R.is";
connectAttr "jointLayer.di" "IKXElbow_R.do";
connectAttr "IKXElbow_R.s" "IKXWrist_R.is";
connectAttr "IKXWrist_R_orientConstraint1.crx" "IKXWrist_R.rx";
connectAttr "IKXWrist_R_orientConstraint1.cry" "IKXWrist_R.ry";
connectAttr "IKXWrist_R_orientConstraint1.crz" "IKXWrist_R.rz";
connectAttr "jointLayer.di" "IKXWrist_R.do";
connectAttr "IKXWrist_R.ro" "IKXWrist_R_orientConstraint1.cro";
connectAttr "IKXWrist_R.pim" "IKXWrist_R_orientConstraint1.cpim";
connectAttr "IKXWrist_R.jo" "IKXWrist_R_orientConstraint1.cjo";
connectAttr "IKArm_R.r" "IKXWrist_R_orientConstraint1.tg[0].tr";
connectAttr "IKArm_R.ro" "IKXWrist_R_orientConstraint1.tg[0].tro";
connectAttr "IKArm_R.pm" "IKXWrist_R_orientConstraint1.tg[0].tpm";
connectAttr "IKXWrist_R_orientConstraint1.w0" "IKXWrist_R_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXWrist_R.tx" "effector1.tx";
connectAttr "IKXWrist_R.ty" "effector1.ty";
connectAttr "IKXWrist_R.tz" "effector1.tz";
connectAttr "Main.arrowVis" "PoleAnnotationArm_R.v";
connectAttr "PoleAnnotateTargetArm_RShape.wm" "PoleAnnotationArm_RShape.dom" -na
		;
connectAttr "IKParentConstraintShoulder_R.ro" "IKParentConstraintShoulder_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintShoulder_R.pim" "IKParentConstraintShoulder_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintShoulder_R.rp" "IKParentConstraintShoulder_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintShoulder_R.rpt" "IKParentConstraintShoulder_R_parentConstraint1.crt"
		;
connectAttr "Clavicle_R.t" "IKParentConstraintShoulder_R_parentConstraint1.tg[0].tt"
		;
connectAttr "Clavicle_R.rp" "IKParentConstraintShoulder_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Clavicle_R.rpt" "IKParentConstraintShoulder_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Clavicle_R.r" "IKParentConstraintShoulder_R_parentConstraint1.tg[0].tr"
		;
connectAttr "Clavicle_R.ro" "IKParentConstraintShoulder_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Clavicle_R.s" "IKParentConstraintShoulder_R_parentConstraint1.tg[0].ts"
		;
connectAttr "Clavicle_R.pm" "IKParentConstraintShoulder_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Clavicle_R.jo" "IKParentConstraintShoulder_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintShoulder_R_parentConstraint1.w0" "IKParentConstraintShoulder_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.ctx" "IKParentConstraintHip_R.tx"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.cty" "IKParentConstraintHip_R.ty"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.ctz" "IKParentConstraintHip_R.tz"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.crx" "IKParentConstraintHip_R.rx"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.cry" "IKParentConstraintHip_R.ry"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.crz" "IKParentConstraintHip_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintHip_R.v";
connectAttr "jointLayer.di" "IKXHip_R.do";
connectAttr "IKXHip_R.s" "IKXKnee_R.is";
connectAttr "jointLayer.di" "IKXKnee_R.do";
connectAttr "IKXKnee_R.s" "IKXAnkle_R.is";
connectAttr "IKXAnkle_R_orientConstraint1.crx" "IKXAnkle_R.rx";
connectAttr "IKXAnkle_R_orientConstraint1.cry" "IKXAnkle_R.ry";
connectAttr "IKXAnkle_R_orientConstraint1.crz" "IKXAnkle_R.rz";
connectAttr "jointLayer.di" "IKXAnkle_R.do";
connectAttr "IKXAnkle_R.s" "IKXFoot_End_R.is";
connectAttr "jointLayer.di" "IKXFoot_End_R.do";
connectAttr "IKXAnkle_R.ro" "IKXAnkle_R_orientConstraint1.cro";
connectAttr "IKXAnkle_R.pim" "IKXAnkle_R_orientConstraint1.cpim";
connectAttr "IKXAnkle_R.jo" "IKXAnkle_R_orientConstraint1.cjo";
connectAttr "IKLeg_R.r" "IKXAnkle_R_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "IKXAnkle_R_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "IKXAnkle_R_orientConstraint1.tg[0].tpm";
connectAttr "IKXAnkle_R_orientConstraint1.w0" "IKXAnkle_R_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXFoot_End_R.tx" "effector3.tx";
connectAttr "IKXFoot_End_R.ty" "effector3.ty";
connectAttr "IKXFoot_End_R.tz" "effector3.tz";
connectAttr "IKXAnkle_R.tx" "effector2.tx";
connectAttr "IKXAnkle_R.ty" "effector2.ty";
connectAttr "IKXAnkle_R.tz" "effector2.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_R.v";
connectAttr "PoleAnnotateTargetLeg_RShape.wm" "PoleAnnotationLeg_RShape.dom" -na
		;
connectAttr "IKParentConstraintHip_R.ro" "IKParentConstraintHip_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintHip_R.pim" "IKParentConstraintHip_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintHip_R.rp" "IKParentConstraintHip_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintHip_R.rpt" "IKParentConstraintHip_R_parentConstraint1.crt"
		;
connectAttr "HipTwist_R.t" "IKParentConstraintHip_R_parentConstraint1.tg[0].tt";
connectAttr "HipTwist_R.rp" "IKParentConstraintHip_R_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_R.rpt" "IKParentConstraintHip_R_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_R.r" "IKParentConstraintHip_R_parentConstraint1.tg[0].tr";
connectAttr "HipTwist_R.ro" "IKParentConstraintHip_R_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_R.s" "IKParentConstraintHip_R_parentConstraint1.tg[0].ts";
connectAttr "HipTwist_R.pm" "IKParentConstraintHip_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_R.jo" "IKParentConstraintHip_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.w0" "IKParentConstraintHip_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintPelvis_M.v";
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.ctx" "IKParentConstraintPelvis_M.tx"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.cty" "IKParentConstraintPelvis_M.ty"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.ctz" "IKParentConstraintPelvis_M.tz"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.crx" "IKParentConstraintPelvis_M.rx"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.cry" "IKParentConstraintPelvis_M.ry"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.crz" "IKParentConstraintPelvis_M.rz"
		;
connectAttr "jointLayer.di" "IKXPelvis_M.do";
connectAttr "IKXPelvis_M.s" "IKXSpineA_M.is";
connectAttr "stretchySpineBlendTwo1_M.o" "IKXSpineA_M.ty";
connectAttr "IKXSpineA_M_aimConstraint1.crx" "IKXSpineA_M.rx";
connectAttr "IKXSpineA_M_aimConstraint1.cry" "IKXSpineA_M.ry";
connectAttr "IKXSpineA_M_aimConstraint1.crz" "IKXSpineA_M.rz";
connectAttr "jointLayer.di" "IKXSpineA_M.do";
connectAttr "IKXSpineA_M.s" "IKXChest_M.is";
connectAttr "IKXChest_M_parentConstraint1.crx" "IKXChest_M.rx";
connectAttr "IKXChest_M_parentConstraint1.cry" "IKXChest_M.ry";
connectAttr "IKXChest_M_parentConstraint1.crz" "IKXChest_M.rz";
connectAttr "IKXChest_M_parentConstraint1.cty" "IKXChest_M.ty";
connectAttr "IKXChest_M_parentConstraint1.ctx" "IKXChest_M.tx";
connectAttr "IKXChest_M_parentConstraint1.ctz" "IKXChest_M.tz";
connectAttr "jointLayer.di" "IKXChest_M.do";
connectAttr "IKXChest_M.ro" "IKXChest_M_parentConstraint1.cro";
connectAttr "IKXChest_M.pim" "IKXChest_M_parentConstraint1.cpim";
connectAttr "IKXChest_M.rp" "IKXChest_M_parentConstraint1.crp";
connectAttr "IKXChest_M.rpt" "IKXChest_M_parentConstraint1.crt";
connectAttr "IKXChest_M.jo" "IKXChest_M_parentConstraint1.cjo";
connectAttr "IKfake2Spine_M.t" "IKXChest_M_parentConstraint1.tg[0].tt";
connectAttr "IKfake2Spine_M.rp" "IKXChest_M_parentConstraint1.tg[0].trp";
connectAttr "IKfake2Spine_M.rpt" "IKXChest_M_parentConstraint1.tg[0].trt";
connectAttr "IKfake2Spine_M.r" "IKXChest_M_parentConstraint1.tg[0].tr";
connectAttr "IKfake2Spine_M.ro" "IKXChest_M_parentConstraint1.tg[0].tro";
connectAttr "IKfake2Spine_M.s" "IKXChest_M_parentConstraint1.tg[0].ts";
connectAttr "IKfake2Spine_M.pm" "IKXChest_M_parentConstraint1.tg[0].tpm";
connectAttr "IKfake2Spine_M.jo" "IKXChest_M_parentConstraint1.tg[0].tjo";
connectAttr "IKXChest_M_parentConstraint1.w0" "IKXChest_M_parentConstraint1.tg[0].tw"
		;
connectAttr "IKSpine4AlignTo_M.t" "IKXChest_M_parentConstraint1.tg[1].tt";
connectAttr "IKSpine4AlignTo_M.rp" "IKXChest_M_parentConstraint1.tg[1].trp";
connectAttr "IKSpine4AlignTo_M.rpt" "IKXChest_M_parentConstraint1.tg[1].trt";
connectAttr "IKSpine4AlignTo_M.r" "IKXChest_M_parentConstraint1.tg[1].tr";
connectAttr "IKSpine4AlignTo_M.ro" "IKXChest_M_parentConstraint1.tg[1].tro";
connectAttr "IKSpine4AlignTo_M.s" "IKXChest_M_parentConstraint1.tg[1].ts";
connectAttr "IKSpine4AlignTo_M.pm" "IKXChest_M_parentConstraint1.tg[1].tpm";
connectAttr "IKXChest_M_parentConstraint1.w1" "IKXChest_M_parentConstraint1.tg[1].tw"
		;
connectAttr "stretchySpineReverse_M.oy" "IKXChest_M_parentConstraint1.w0";
connectAttr "stretchySpineUnitConversion_M.o" "IKXChest_M_parentConstraint1.w1";
connectAttr "stretchySpineReverse_M.oy" "IKXSpineA_M_aimConstraint1.w0";
connectAttr "stretchySpineUnitConversion_M.o" "IKXSpineA_M_aimConstraint1.w1";
connectAttr "IKXSpineA_M.pim" "IKXSpineA_M_aimConstraint1.cpim";
connectAttr "IKXSpineA_M.t" "IKXSpineA_M_aimConstraint1.ct";
connectAttr "IKXSpineA_M.rp" "IKXSpineA_M_aimConstraint1.crp";
connectAttr "IKXSpineA_M.rpt" "IKXSpineA_M_aimConstraint1.crt";
connectAttr "IKXSpineA_M.ro" "IKXSpineA_M_aimConstraint1.cro";
connectAttr "IKXSpineA_M.jo" "IKXSpineA_M_aimConstraint1.cjo";
connectAttr "IKfake2Spine_M.t" "IKXSpineA_M_aimConstraint1.tg[0].tt";
connectAttr "IKfake2Spine_M.rp" "IKXSpineA_M_aimConstraint1.tg[0].trp";
connectAttr "IKfake2Spine_M.rpt" "IKXSpineA_M_aimConstraint1.tg[0].trt";
connectAttr "IKfake2Spine_M.pm" "IKXSpineA_M_aimConstraint1.tg[0].tpm";
connectAttr "IKXSpineA_M_aimConstraint1.w0" "IKXSpineA_M_aimConstraint1.tg[0].tw"
		;
connectAttr "IKSpine4_M.t" "IKXSpineA_M_aimConstraint1.tg[1].tt";
connectAttr "IKSpine4_M.rp" "IKXSpineA_M_aimConstraint1.tg[1].trp";
connectAttr "IKSpine4_M.rpt" "IKXSpineA_M_aimConstraint1.tg[1].trt";
connectAttr "IKSpine4_M.pm" "IKXSpineA_M_aimConstraint1.tg[1].tpm";
connectAttr "IKXSpineA_M_aimConstraint1.w1" "IKXSpineA_M_aimConstraint1.tg[1].tw"
		;
connectAttr "IKFake1UpLocSpine_M.wm" "IKXSpineA_M_aimConstraint1.wum";
connectAttr "stretchySpineBlendTwo3_M.o" "IKfake1Spine_M.ty";
connectAttr "jointLayer.di" "IKfake1Spine_M.do";
connectAttr "stretchySpineBlendTwo2_M.o" "IKfake2Spine_M.ty";
connectAttr "IKfake1Spine_M.s" "IKfake2Spine_M.is";
connectAttr "jointLayer.di" "IKfake2Spine_M.do";
connectAttr "IKfake2Spine_M.tx" "effector4.tx";
connectAttr "IKfake2Spine_M.ty" "effector4.ty";
connectAttr "IKfake2Spine_M.tz" "effector4.tz";
connectAttr "IKParentConstraintPelvis_M.ro" "IKParentConstraintPelvis_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintPelvis_M.pim" "IKParentConstraintPelvis_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintPelvis_M.rp" "IKParentConstraintPelvis_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintPelvis_M.rpt" "IKParentConstraintPelvis_M_parentConstraint1.crt"
		;
connectAttr "IKSpine0AlignTo_M.t" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tt"
		;
connectAttr "IKSpine0AlignTo_M.rp" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].trp"
		;
connectAttr "IKSpine0AlignTo_M.rpt" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].trt"
		;
connectAttr "IKSpine0AlignTo_M.r" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tr"
		;
connectAttr "IKSpine0AlignTo_M.ro" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tro"
		;
connectAttr "IKSpine0AlignTo_M.s" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].ts"
		;
connectAttr "IKSpine0AlignTo_M.pm" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.w0" "IKParentConstraintPelvis_M_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.ctx" "IKParentConstraintShoulder_L.tx"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.cty" "IKParentConstraintShoulder_L.ty"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.ctz" "IKParentConstraintShoulder_L.tz"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.crx" "IKParentConstraintShoulder_L.rx"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.cry" "IKParentConstraintShoulder_L.ry"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.crz" "IKParentConstraintShoulder_L.rz"
		;
connectAttr "FKIKBlendArmCondition_L.ocr" "IKParentConstraintShoulder_L.v";
connectAttr "jointLayer.di" "IKXShoulder_L.do";
connectAttr "IKXShoulder_L.s" "IKXElbow_L.is";
connectAttr "jointLayer.di" "IKXElbow_L.do";
connectAttr "IKXElbow_L.s" "IKXWrist_L.is";
connectAttr "IKXWrist_L_orientConstraint1.crx" "IKXWrist_L.rx";
connectAttr "IKXWrist_L_orientConstraint1.cry" "IKXWrist_L.ry";
connectAttr "IKXWrist_L_orientConstraint1.crz" "IKXWrist_L.rz";
connectAttr "jointLayer.di" "IKXWrist_L.do";
connectAttr "IKXWrist_L.ro" "IKXWrist_L_orientConstraint1.cro";
connectAttr "IKXWrist_L.pim" "IKXWrist_L_orientConstraint1.cpim";
connectAttr "IKXWrist_L.jo" "IKXWrist_L_orientConstraint1.cjo";
connectAttr "IKArm_L.r" "IKXWrist_L_orientConstraint1.tg[0].tr";
connectAttr "IKArm_L.ro" "IKXWrist_L_orientConstraint1.tg[0].tro";
connectAttr "IKArm_L.pm" "IKXWrist_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXWrist_L_orientConstraint1.w0" "IKXWrist_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXWrist_L.tx" "effector6.tx";
connectAttr "IKXWrist_L.ty" "effector6.ty";
connectAttr "IKXWrist_L.tz" "effector6.tz";
connectAttr "Main.arrowVis" "PoleAnnotationArm_L.v";
connectAttr "PoleAnnotateTargetArm_LShape.wm" "PoleAnnotationArm_LShape.dom" -na
		;
connectAttr "IKParentConstraintShoulder_L.ro" "IKParentConstraintShoulder_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintShoulder_L.pim" "IKParentConstraintShoulder_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintShoulder_L.rp" "IKParentConstraintShoulder_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintShoulder_L.rpt" "IKParentConstraintShoulder_L_parentConstraint1.crt"
		;
connectAttr "Clavicle_L.t" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Clavicle_L.rp" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Clavicle_L.rpt" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Clavicle_L.r" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Clavicle_L.ro" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Clavicle_L.s" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Clavicle_L.pm" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Clavicle_L.jo" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.w0" "IKParentConstraintShoulder_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.ctx" "IKParentConstraintHip_L.tx"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.cty" "IKParentConstraintHip_L.ty"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.ctz" "IKParentConstraintHip_L.tz"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.crx" "IKParentConstraintHip_L.rx"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.cry" "IKParentConstraintHip_L.ry"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.crz" "IKParentConstraintHip_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintHip_L.v";
connectAttr "jointLayer.di" "IKXHip_L.do";
connectAttr "IKXHip_L.s" "IKXKnee_L.is";
connectAttr "jointLayer.di" "IKXKnee_L.do";
connectAttr "IKXKnee_L.s" "IKXAnkle_L.is";
connectAttr "IKXAnkle_L_orientConstraint1.crx" "IKXAnkle_L.rx";
connectAttr "IKXAnkle_L_orientConstraint1.cry" "IKXAnkle_L.ry";
connectAttr "IKXAnkle_L_orientConstraint1.crz" "IKXAnkle_L.rz";
connectAttr "jointLayer.di" "IKXAnkle_L.do";
connectAttr "IKXAnkle_L.s" "IKXFoot_End_L.is";
connectAttr "jointLayer.di" "IKXFoot_End_L.do";
connectAttr "IKXAnkle_L.ro" "IKXAnkle_L_orientConstraint1.cro";
connectAttr "IKXAnkle_L.pim" "IKXAnkle_L_orientConstraint1.cpim";
connectAttr "IKXAnkle_L.jo" "IKXAnkle_L_orientConstraint1.cjo";
connectAttr "IKLeg_L.r" "IKXAnkle_L_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_L.ro" "IKXAnkle_L_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_L.pm" "IKXAnkle_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXAnkle_L_orientConstraint1.w0" "IKXAnkle_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXFoot_End_L.tx" "effector8.tx";
connectAttr "IKXFoot_End_L.ty" "effector8.ty";
connectAttr "IKXFoot_End_L.tz" "effector8.tz";
connectAttr "IKXAnkle_L.tx" "effector7.tx";
connectAttr "IKXAnkle_L.ty" "effector7.ty";
connectAttr "IKXAnkle_L.tz" "effector7.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_L.v";
connectAttr "PoleAnnotateTargetLeg_LShape.wm" "PoleAnnotationLeg_LShape.dom" -na
		;
connectAttr "IKParentConstraintHip_L.ro" "IKParentConstraintHip_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintHip_L.pim" "IKParentConstraintHip_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintHip_L.rp" "IKParentConstraintHip_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintHip_L.rpt" "IKParentConstraintHip_L_parentConstraint1.crt"
		;
connectAttr "HipTwist_L.t" "IKParentConstraintHip_L_parentConstraint1.tg[0].tt";
connectAttr "HipTwist_L.rp" "IKParentConstraintHip_L_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_L.rpt" "IKParentConstraintHip_L_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_L.r" "IKParentConstraintHip_L_parentConstraint1.tg[0].tr";
connectAttr "HipTwist_L.ro" "IKParentConstraintHip_L_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_L.s" "IKParentConstraintHip_L_parentConstraint1.tg[0].ts";
connectAttr "HipTwist_L.pm" "IKParentConstraintHip_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_L.jo" "IKParentConstraintHip_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.w0" "IKParentConstraintHip_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintArm_R_parentConstraint1.ctx" "IKParentConstraintArm_R.tx"
		;
connectAttr "IKParentConstraintArm_R_parentConstraint1.cty" "IKParentConstraintArm_R.ty"
		;
connectAttr "IKParentConstraintArm_R_parentConstraint1.ctz" "IKParentConstraintArm_R.tz"
		;
connectAttr "IKParentConstraintArm_R_parentConstraint1.crx" "IKParentConstraintArm_R.rx"
		;
connectAttr "IKParentConstraintArm_R_parentConstraint1.cry" "IKParentConstraintArm_R.ry"
		;
connectAttr "IKParentConstraintArm_R_parentConstraint1.crz" "IKParentConstraintArm_R.rz"
		;
connectAttr "FKIKBlendArmCondition_R.ocr" "IKParentConstraintArm_R.v";
connectAttr "IKXShoulder_R.msg" "IKXArmHandle_R.hsj";
connectAttr "effector1.hp" "IKXArmHandle_R.hee";
connectAttr "ikRPsolver.msg" "IKXArmHandle_R.hsv";
connectAttr "IKXArmHandle_R_poleVectorConstraint1.ctx" "IKXArmHandle_R.pvx";
connectAttr "IKXArmHandle_R_poleVectorConstraint1.cty" "IKXArmHandle_R.pvy";
connectAttr "IKXArmHandle_R_poleVectorConstraint1.ctz" "IKXArmHandle_R.pvz";
connectAttr "IKXArmHandle_R.pim" "IKXArmHandle_R_poleVectorConstraint1.cpim";
connectAttr "IKXShoulder_R.pm" "IKXArmHandle_R_poleVectorConstraint1.ps";
connectAttr "IKXShoulder_R.t" "IKXArmHandle_R_poleVectorConstraint1.crp";
connectAttr "PoleArm_R.t" "IKXArmHandle_R_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleArm_R.rp" "IKXArmHandle_R_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleArm_R.rpt" "IKXArmHandle_R_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleArm_R.pm" "IKXArmHandle_R_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXArmHandle_R_poleVectorConstraint1.w0" "IKXArmHandle_R_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintArm_R.ro" "IKParentConstraintArm_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintArm_R.pim" "IKParentConstraintArm_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintArm_R.rp" "IKParentConstraintArm_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintArm_R.rpt" "IKParentConstraintArm_R_parentConstraint1.crt"
		;
connectAttr "IKParentConstraintArm_RStatic.t" "IKParentConstraintArm_R_parentConstraint1.tg[0].tt"
		;
connectAttr "IKParentConstraintArm_RStatic.rp" "IKParentConstraintArm_R_parentConstraint1.tg[0].trp"
		;
connectAttr "IKParentConstraintArm_RStatic.rpt" "IKParentConstraintArm_R_parentConstraint1.tg[0].trt"
		;
connectAttr "IKParentConstraintArm_RStatic.r" "IKParentConstraintArm_R_parentConstraint1.tg[0].tr"
		;
connectAttr "IKParentConstraintArm_RStatic.ro" "IKParentConstraintArm_R_parentConstraint1.tg[0].tro"
		;
connectAttr "IKParentConstraintArm_RStatic.s" "IKParentConstraintArm_R_parentConstraint1.tg[0].ts"
		;
connectAttr "IKParentConstraintArm_RStatic.pm" "IKParentConstraintArm_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintArm_R_parentConstraint1.w0" "IKParentConstraintArm_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Clavicle_R.t" "IKParentConstraintArm_R_parentConstraint1.tg[1].tt";
connectAttr "Clavicle_R.rp" "IKParentConstraintArm_R_parentConstraint1.tg[1].trp"
		;
connectAttr "Clavicle_R.rpt" "IKParentConstraintArm_R_parentConstraint1.tg[1].trt"
		;
connectAttr "Clavicle_R.r" "IKParentConstraintArm_R_parentConstraint1.tg[1].tr";
connectAttr "Clavicle_R.ro" "IKParentConstraintArm_R_parentConstraint1.tg[1].tro"
		;
connectAttr "Clavicle_R.s" "IKParentConstraintArm_R_parentConstraint1.tg[1].ts";
connectAttr "Clavicle_R.pm" "IKParentConstraintArm_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "Clavicle_R.jo" "IKParentConstraintArm_R_parentConstraint1.tg[1].tjo"
		;
connectAttr "IKParentConstraintArm_R_parentConstraint1.w1" "IKParentConstraintArm_R_parentConstraint1.tg[1].tw"
		;
connectAttr "IKArm_RSetRangeFollow.oy" "IKParentConstraintArm_R_parentConstraint1.w0"
		;
connectAttr "IKArm_RSetRangeFollow.ox" "IKParentConstraintArm_R_parentConstraint1.w1"
		;
connectAttr "PoleParentConstraintArm_R_parentConstraint1.ctx" "PoleParentConstraintArm_R.tx"
		;
connectAttr "PoleParentConstraintArm_R_parentConstraint1.cty" "PoleParentConstraintArm_R.ty"
		;
connectAttr "PoleParentConstraintArm_R_parentConstraint1.ctz" "PoleParentConstraintArm_R.tz"
		;
connectAttr "PoleParentConstraintArm_R_parentConstraint1.crx" "PoleParentConstraintArm_R.rx"
		;
connectAttr "PoleParentConstraintArm_R_parentConstraint1.cry" "PoleParentConstraintArm_R.ry"
		;
connectAttr "PoleParentConstraintArm_R_parentConstraint1.crz" "PoleParentConstraintArm_R.rz"
		;
connectAttr "FKIKBlendArmCondition_R.ocr" "PoleParentConstraintArm_R.v";
connectAttr "PoleParentConstraintArm_R.ro" "PoleParentConstraintArm_R_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintArm_R.pim" "PoleParentConstraintArm_R_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintArm_R.rp" "PoleParentConstraintArm_R_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintArm_R.rpt" "PoleParentConstraintArm_R_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintArm_RStatic.t" "PoleParentConstraintArm_R_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintArm_RStatic.rp" "PoleParentConstraintArm_R_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintArm_RStatic.rpt" "PoleParentConstraintArm_R_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintArm_RStatic.r" "PoleParentConstraintArm_R_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintArm_RStatic.ro" "PoleParentConstraintArm_R_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintArm_RStatic.s" "PoleParentConstraintArm_R_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintArm_RStatic.pm" "PoleParentConstraintArm_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintArm_R_parentConstraint1.w0" "PoleParentConstraintArm_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKArm_R.t" "PoleParentConstraintArm_R_parentConstraint1.tg[1].tt";
connectAttr "IKArm_R.rp" "PoleParentConstraintArm_R_parentConstraint1.tg[1].trp"
		;
connectAttr "IKArm_R.rpt" "PoleParentConstraintArm_R_parentConstraint1.tg[1].trt"
		;
connectAttr "IKArm_R.r" "PoleParentConstraintArm_R_parentConstraint1.tg[1].tr";
connectAttr "IKArm_R.ro" "PoleParentConstraintArm_R_parentConstraint1.tg[1].tro"
		;
connectAttr "IKArm_R.s" "PoleParentConstraintArm_R_parentConstraint1.tg[1].ts";
connectAttr "IKArm_R.pm" "PoleParentConstraintArm_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintArm_R_parentConstraint1.w1" "PoleParentConstraintArm_R_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleArm_RSetRangeFollow.oy" "PoleParentConstraintArm_R_parentConstraint1.w0"
		;
connectAttr "PoleArm_RSetRangeFollow.ox" "PoleParentConstraintArm_R_parentConstraint1.w1"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintLeg_R.v";
connectAttr "unitConversion2.o" "IKRollLegHeel_R.rx";
connectAttr "unitConversion3.o" "IKRollLegBall_R.rx";
connectAttr "IKXHip_R.msg" "IKXLegHandle_R.hsj";
connectAttr "effector2.hp" "IKXLegHandle_R.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_R.hsv";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctx" "IKXLegHandle_R.pvx";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.cty" "IKXLegHandle_R.pvy";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctz" "IKXLegHandle_R.pvz";
connectAttr "IKXLegHandle_R.pim" "IKXLegHandle_R_poleVectorConstraint1.cpim";
connectAttr "IKXHip_R.pm" "IKXLegHandle_R_poleVectorConstraint1.ps";
connectAttr "IKXHip_R.t" "IKXLegHandle_R_poleVectorConstraint1.crp";
connectAttr "PoleLeg_R.t" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_R.rp" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_R.rpt" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_R.pm" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.w0" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXAnkle_R.msg" "IKXLegHandleBall_R.hsj";
connectAttr "effector3.hp" "IKXLegHandleBall_R.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_R.hsv";
connectAttr "PoleAimLeg_R_aimConstraint1.crx" "PoleAimLeg_R.rx";
connectAttr "PoleAimLeg_R_aimConstraint1.cry" "PoleAimLeg_R.ry";
connectAttr "PoleAimLeg_R_aimConstraint1.crz" "PoleAimLeg_R.rz";
connectAttr "PoleAimLeg_R_pointConstraint1.ctx" "PoleAimLeg_R.tx";
connectAttr "PoleAimLeg_R_pointConstraint1.cty" "PoleAimLeg_R.ty";
connectAttr "PoleAimLeg_R_pointConstraint1.ctz" "PoleAimLeg_R.tz";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_aimConstraint1.cpim";
connectAttr "PoleAimLeg_R.t" "PoleAimLeg_R_aimConstraint1.ct";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_aimConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.crt";
connectAttr "PoleAimLeg_R.ro" "PoleAimLeg_R_aimConstraint1.cro";
connectAttr "IKLeg_R.t" "PoleAimLeg_R_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PoleAimLeg_R_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PoleAimLeg_R_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_aimConstraint1.w0" "PoleAimLeg_R_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_R.wm" "PoleAimLeg_R_aimConstraint1.wum";
connectAttr "unitConversion1.o" "PoleAimLeg_R_aimConstraint1.ox";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_pointConstraint1.cpim";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_pointConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_pointConstraint1.crt";
connectAttr "IKXHip_R.t" "PoleAimLeg_R_pointConstraint1.tg[0].tt";
connectAttr "IKXHip_R.rp" "PoleAimLeg_R_pointConstraint1.tg[0].trp";
connectAttr "IKXHip_R.rpt" "PoleAimLeg_R_pointConstraint1.tg[0].trt";
connectAttr "IKXHip_R.pm" "PoleAimLeg_R_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_pointConstraint1.w0" "PoleAimLeg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctx" "PoleParentConstraintLeg_R.tx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cty" "PoleParentConstraintLeg_R.ty"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctz" "PoleParentConstraintLeg_R.tz"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crx" "PoleParentConstraintLeg_R.rx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cry" "PoleParentConstraintLeg_R.ry"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crz" "PoleParentConstraintLeg_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "PoleParentConstraintLeg_R.v";
connectAttr "PoleParentConstraintLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_R.pim" "PoleParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_RStatic.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_RStatic.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_RStatic.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w0" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_R.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_R.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_R.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_R.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w1" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_RSetRangeFollow.oy" "PoleParentConstraintLeg_R_parentConstraint1.w0"
		;
connectAttr "PoleLeg_RSetRangeFollow.ox" "PoleParentConstraintLeg_R_parentConstraint1.w1"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintSpine0_M.v";
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.ctx" "IKParentConstraintSpine0_M.tx"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.cty" "IKParentConstraintSpine0_M.ty"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.ctz" "IKParentConstraintSpine0_M.tz"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.crx" "IKParentConstraintSpine0_M.rx"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.cry" "IKParentConstraintSpine0_M.ry"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.crz" "IKParentConstraintSpine0_M.rz"
		;
connectAttr "IKStiffSpine0_M.oy" "IKXSpineLocator1_M.ty";
connectAttr "IKParentConstraintSpine0_M.ro" "IKParentConstraintSpine0_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintSpine0_M.pim" "IKParentConstraintSpine0_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintSpine0_M.rp" "IKParentConstraintSpine0_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintSpine0_M.rpt" "IKParentConstraintSpine0_M_parentConstraint1.crt"
		;
connectAttr "Center_M.t" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Center_M.rp" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Center_M.rpt" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Center_M.r" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Center_M.s" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Center_M.pm" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.w0" "IKParentConstraintSpine0_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintSpine2_M.v";
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.ctx" "IKParentConstraintSpine2_M.tx"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.cty" "IKParentConstraintSpine2_M.ty"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.ctz" "IKParentConstraintSpine2_M.tz"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.crx" "IKParentConstraintSpine2_M.rx"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.cry" "IKParentConstraintSpine2_M.ry"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.crz" "IKParentConstraintSpine2_M.rz"
		;
connectAttr "IKParentConstraintSpine2_M.ro" "IKParentConstraintSpine2_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintSpine2_M.pim" "IKParentConstraintSpine2_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintSpine2_M.rp" "IKParentConstraintSpine2_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintSpine2_M.rpt" "IKParentConstraintSpine2_M_parentConstraint1.crt"
		;
connectAttr "Center_M.t" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Center_M.rp" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Center_M.rpt" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Center_M.r" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Center_M.s" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Center_M.pm" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.w0" "IKParentConstraintSpine2_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKParentConstraintSpine4_M.v";
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.ctx" "IKParentConstraintSpine4_M.tx"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.cty" "IKParentConstraintSpine4_M.ty"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.ctz" "IKParentConstraintSpine4_M.tz"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.crx" "IKParentConstraintSpine4_M.rx"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.cry" "IKParentConstraintSpine4_M.ry"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.crz" "IKParentConstraintSpine4_M.rz"
		;
connectAttr "IKXPelvis_M.msg" "IKXSpineHandle_M.hsj";
connectAttr "effector4.hp" "IKXSpineHandle_M.hee";
connectAttr "ikSplineSolver.msg" "IKXSpineHandle_M.hsv";
connectAttr "IKXSpineCurve_MShape.ws" "IKXSpineHandle_M.ic";
connectAttr "IKTwistSpineUnitConversion_M.o" "IKXSpineHandle_M.twi";
connectAttr "IKStiffSpine4_M.oy" "IKXSpineLocator3_M.ty";
connectAttr "IKParentConstraintSpine4_M.ro" "IKParentConstraintSpine4_M_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintSpine4_M.pim" "IKParentConstraintSpine4_M_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintSpine4_M.rp" "IKParentConstraintSpine4_M_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintSpine4_M.rpt" "IKParentConstraintSpine4_M_parentConstraint1.crt"
		;
connectAttr "Center_M.t" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tt"
		;
connectAttr "Center_M.rp" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].trp"
		;
connectAttr "Center_M.rpt" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].trt"
		;
connectAttr "Center_M.r" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tr"
		;
connectAttr "Center_M.ro" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tro"
		;
connectAttr "Center_M.s" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].ts"
		;
connectAttr "Center_M.pm" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.w0" "IKParentConstraintSpine4_M_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.ctx" "IKParentConstraintArm_L.tx"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.cty" "IKParentConstraintArm_L.ty"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.ctz" "IKParentConstraintArm_L.tz"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.crx" "IKParentConstraintArm_L.rx"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.cry" "IKParentConstraintArm_L.ry"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.crz" "IKParentConstraintArm_L.rz"
		;
connectAttr "FKIKBlendArmCondition_L.ocr" "IKParentConstraintArm_L.v";
connectAttr "IKXShoulder_L.msg" "IKXArmHandle_L.hsj";
connectAttr "effector6.hp" "IKXArmHandle_L.hee";
connectAttr "ikRPsolver.msg" "IKXArmHandle_L.hsv";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.ctx" "IKXArmHandle_L.pvx";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.cty" "IKXArmHandle_L.pvy";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.ctz" "IKXArmHandle_L.pvz";
connectAttr "IKXArmHandle_L.pim" "IKXArmHandle_L_poleVectorConstraint1.cpim";
connectAttr "IKXShoulder_L.pm" "IKXArmHandle_L_poleVectorConstraint1.ps";
connectAttr "IKXShoulder_L.t" "IKXArmHandle_L_poleVectorConstraint1.crp";
connectAttr "PoleArm_L.t" "IKXArmHandle_L_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleArm_L.rp" "IKXArmHandle_L_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleArm_L.rpt" "IKXArmHandle_L_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleArm_L.pm" "IKXArmHandle_L_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXArmHandle_L_poleVectorConstraint1.w0" "IKXArmHandle_L_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintArm_L.ro" "IKParentConstraintArm_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintArm_L.pim" "IKParentConstraintArm_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintArm_L.rp" "IKParentConstraintArm_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintArm_L.rpt" "IKParentConstraintArm_L_parentConstraint1.crt"
		;
connectAttr "IKParentConstraintArm_LStatic.t" "IKParentConstraintArm_L_parentConstraint1.tg[0].tt"
		;
connectAttr "IKParentConstraintArm_LStatic.rp" "IKParentConstraintArm_L_parentConstraint1.tg[0].trp"
		;
connectAttr "IKParentConstraintArm_LStatic.rpt" "IKParentConstraintArm_L_parentConstraint1.tg[0].trt"
		;
connectAttr "IKParentConstraintArm_LStatic.r" "IKParentConstraintArm_L_parentConstraint1.tg[0].tr"
		;
connectAttr "IKParentConstraintArm_LStatic.ro" "IKParentConstraintArm_L_parentConstraint1.tg[0].tro"
		;
connectAttr "IKParentConstraintArm_LStatic.s" "IKParentConstraintArm_L_parentConstraint1.tg[0].ts"
		;
connectAttr "IKParentConstraintArm_LStatic.pm" "IKParentConstraintArm_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.w0" "IKParentConstraintArm_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Clavicle_L.t" "IKParentConstraintArm_L_parentConstraint1.tg[1].tt";
connectAttr "Clavicle_L.rp" "IKParentConstraintArm_L_parentConstraint1.tg[1].trp"
		;
connectAttr "Clavicle_L.rpt" "IKParentConstraintArm_L_parentConstraint1.tg[1].trt"
		;
connectAttr "Clavicle_L.r" "IKParentConstraintArm_L_parentConstraint1.tg[1].tr";
connectAttr "Clavicle_L.ro" "IKParentConstraintArm_L_parentConstraint1.tg[1].tro"
		;
connectAttr "Clavicle_L.s" "IKParentConstraintArm_L_parentConstraint1.tg[1].ts";
connectAttr "Clavicle_L.pm" "IKParentConstraintArm_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "Clavicle_L.jo" "IKParentConstraintArm_L_parentConstraint1.tg[1].tjo"
		;
connectAttr "IKParentConstraintArm_L_parentConstraint1.w1" "IKParentConstraintArm_L_parentConstraint1.tg[1].tw"
		;
connectAttr "IKArm_LSetRangeFollow.oy" "IKParentConstraintArm_L_parentConstraint1.w0"
		;
connectAttr "IKArm_LSetRangeFollow.ox" "IKParentConstraintArm_L_parentConstraint1.w1"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.ctx" "PoleParentConstraintArm_L.tx"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.cty" "PoleParentConstraintArm_L.ty"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.ctz" "PoleParentConstraintArm_L.tz"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.crx" "PoleParentConstraintArm_L.rx"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.cry" "PoleParentConstraintArm_L.ry"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.crz" "PoleParentConstraintArm_L.rz"
		;
connectAttr "FKIKBlendArmCondition_L.ocr" "PoleParentConstraintArm_L.v";
connectAttr "PoleParentConstraintArm_L.ro" "PoleParentConstraintArm_L_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintArm_L.pim" "PoleParentConstraintArm_L_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintArm_L.rp" "PoleParentConstraintArm_L_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintArm_L.rpt" "PoleParentConstraintArm_L_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintArm_LStatic.t" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintArm_LStatic.rp" "PoleParentConstraintArm_L_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintArm_LStatic.rpt" "PoleParentConstraintArm_L_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintArm_LStatic.r" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintArm_LStatic.ro" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintArm_LStatic.s" "PoleParentConstraintArm_L_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintArm_LStatic.pm" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.w0" "PoleParentConstraintArm_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKArm_L.t" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tt";
connectAttr "IKArm_L.rp" "PoleParentConstraintArm_L_parentConstraint1.tg[1].trp"
		;
connectAttr "IKArm_L.rpt" "PoleParentConstraintArm_L_parentConstraint1.tg[1].trt"
		;
connectAttr "IKArm_L.r" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tr";
connectAttr "IKArm_L.ro" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tro"
		;
connectAttr "IKArm_L.s" "PoleParentConstraintArm_L_parentConstraint1.tg[1].ts";
connectAttr "IKArm_L.pm" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.w1" "PoleParentConstraintArm_L_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleArm_LSetRangeFollow.oy" "PoleParentConstraintArm_L_parentConstraint1.w0"
		;
connectAttr "PoleArm_LSetRangeFollow.ox" "PoleParentConstraintArm_L_parentConstraint1.w1"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintLeg_L.v";
connectAttr "unitConversion5.o" "IKRollLegHeel_L.rx";
connectAttr "unitConversion6.o" "IKRollLegBall_L.rx";
connectAttr "IKXHip_L.msg" "IKXLegHandle_L.hsj";
connectAttr "effector7.hp" "IKXLegHandle_L.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_L.hsv";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctx" "IKXLegHandle_L.pvx";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.cty" "IKXLegHandle_L.pvy";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctz" "IKXLegHandle_L.pvz";
connectAttr "IKXLegHandle_L.pim" "IKXLegHandle_L_poleVectorConstraint1.cpim";
connectAttr "IKXHip_L.pm" "IKXLegHandle_L_poleVectorConstraint1.ps";
connectAttr "IKXHip_L.t" "IKXLegHandle_L_poleVectorConstraint1.crp";
connectAttr "PoleLeg_L.t" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_L.rp" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_L.rpt" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_L.pm" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.w0" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXAnkle_L.msg" "IKXLegHandleBall_L.hsj";
connectAttr "effector8.hp" "IKXLegHandleBall_L.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_L.hsv";
connectAttr "PoleAimLeg_L_aimConstraint1.crx" "PoleAimLeg_L.rx";
connectAttr "PoleAimLeg_L_aimConstraint1.cry" "PoleAimLeg_L.ry";
connectAttr "PoleAimLeg_L_aimConstraint1.crz" "PoleAimLeg_L.rz";
connectAttr "PoleAimLeg_L_pointConstraint1.ctx" "PoleAimLeg_L.tx";
connectAttr "PoleAimLeg_L_pointConstraint1.cty" "PoleAimLeg_L.ty";
connectAttr "PoleAimLeg_L_pointConstraint1.ctz" "PoleAimLeg_L.tz";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_aimConstraint1.cpim";
connectAttr "PoleAimLeg_L.t" "PoleAimLeg_L_aimConstraint1.ct";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_aimConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.crt";
connectAttr "PoleAimLeg_L.ro" "PoleAimLeg_L_aimConstraint1.cro";
connectAttr "IKLeg_L.t" "PoleAimLeg_L_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_L.rp" "PoleAimLeg_L_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_L.pm" "PoleAimLeg_L_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_aimConstraint1.w0" "PoleAimLeg_L_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.wm" "PoleAimLeg_L_aimConstraint1.wum";
connectAttr "unitConversion4.o" "PoleAimLeg_L_aimConstraint1.ox";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_pointConstraint1.cpim";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_pointConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_pointConstraint1.crt";
connectAttr "IKXHip_L.t" "PoleAimLeg_L_pointConstraint1.tg[0].tt";
connectAttr "IKXHip_L.rp" "PoleAimLeg_L_pointConstraint1.tg[0].trp";
connectAttr "IKXHip_L.rpt" "PoleAimLeg_L_pointConstraint1.tg[0].trt";
connectAttr "IKXHip_L.pm" "PoleAimLeg_L_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_pointConstraint1.w0" "PoleAimLeg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctx" "PoleParentConstraintLeg_L.tx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cty" "PoleParentConstraintLeg_L.ty"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctz" "PoleParentConstraintLeg_L.tz"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crx" "PoleParentConstraintLeg_L.rx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cry" "PoleParentConstraintLeg_L.ry"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crz" "PoleParentConstraintLeg_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "PoleParentConstraintLeg_L.v";
connectAttr "PoleParentConstraintLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_L.pim" "PoleParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_LStatic.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_LStatic.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_LStatic.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w0" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_L.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_L.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_L.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_L.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w1" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_LSetRangeFollow.oy" "PoleParentConstraintLeg_L_parentConstraint1.w0"
		;
connectAttr "PoleLeg_LSetRangeFollow.ox" "PoleParentConstraintLeg_L_parentConstraint1.w1"
		;
connectAttr "FKIKBlendSpineCondition_M.ocr" "IKXSpineCurve_M.v";
connectAttr "IKXSpineLocator0_MShape.wp" "IKXSpineCurve_MShape.cp[0]";
connectAttr "IKXSpineLocator1_MShape.wp" "IKXSpineCurve_MShape.cp[1]";
connectAttr "IKXSpineLocator2_MShape.wp" "IKXSpineCurve_MShape.cp[2]";
connectAttr "IKXSpineLocator3_MShape.wp" "IKXSpineCurve_MShape.cp[3]";
connectAttr "IKXSpineLocator4_MShape.wp" "IKXSpineCurve_MShape.cp[4]";
connectAttr "Main.fkIkVis" "FKIKSystem.v";
connectAttr "FKIKParentConstraintArm_R_parentConstraint1.ctx" "FKIKParentConstraintArm_R.tx"
		;
connectAttr "FKIKParentConstraintArm_R_parentConstraint1.cty" "FKIKParentConstraintArm_R.ty"
		;
connectAttr "FKIKParentConstraintArm_R_parentConstraint1.ctz" "FKIKParentConstraintArm_R.tz"
		;
connectAttr "FKIKParentConstraintArm_R_parentConstraint1.crx" "FKIKParentConstraintArm_R.rx"
		;
connectAttr "FKIKParentConstraintArm_R_parentConstraint1.cry" "FKIKParentConstraintArm_R.ry"
		;
connectAttr "FKIKParentConstraintArm_R_parentConstraint1.crz" "FKIKParentConstraintArm_R.rz"
		;
connectAttr "FKIKParentConstraintArm_R.ro" "FKIKParentConstraintArm_R_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintArm_R.pim" "FKIKParentConstraintArm_R_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintArm_R.rp" "FKIKParentConstraintArm_R_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintArm_R.rpt" "FKIKParentConstraintArm_R_parentConstraint1.crt"
		;
connectAttr "Clavicle_R.t" "FKIKParentConstraintArm_R_parentConstraint1.tg[0].tt"
		;
connectAttr "Clavicle_R.rp" "FKIKParentConstraintArm_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Clavicle_R.rpt" "FKIKParentConstraintArm_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Clavicle_R.r" "FKIKParentConstraintArm_R_parentConstraint1.tg[0].tr"
		;
connectAttr "Clavicle_R.ro" "FKIKParentConstraintArm_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Clavicle_R.s" "FKIKParentConstraintArm_R_parentConstraint1.tg[0].ts"
		;
connectAttr "Clavicle_R.pm" "FKIKParentConstraintArm_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Clavicle_R.jo" "FKIKParentConstraintArm_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintArm_R_parentConstraint1.w0" "FKIKParentConstraintArm_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctx" "FKIKParentConstraintLeg_R.tx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cty" "FKIKParentConstraintLeg_R.ty"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctz" "FKIKParentConstraintLeg_R.tz"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crx" "FKIKParentConstraintLeg_R.rx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cry" "FKIKParentConstraintLeg_R.ry"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crz" "FKIKParentConstraintLeg_R.rz"
		;
connectAttr "FKIKParentConstraintLeg_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_R.pim" "FKIKParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "HipTwist_R.t" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "HipTwist_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_R.r" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "HipTwist_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_R.s" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "HipTwist_R.pm" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_R.jo" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.w0" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.ctx" "FKIKParentConstraintSpine_M.tx"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.cty" "FKIKParentConstraintSpine_M.ty"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.ctz" "FKIKParentConstraintSpine_M.tz"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.crx" "FKIKParentConstraintSpine_M.rx"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.cry" "FKIKParentConstraintSpine_M.ry"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.crz" "FKIKParentConstraintSpine_M.rz"
		;
connectAttr "FKIKParentConstraintSpine_M.ro" "FKIKParentConstraintSpine_M_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintSpine_M.pim" "FKIKParentConstraintSpine_M_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintSpine_M.rp" "FKIKParentConstraintSpine_M_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintSpine_M.rpt" "FKIKParentConstraintSpine_M_parentConstraint1.crt"
		;
connectAttr "FKPelvis_M.t" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tt"
		;
connectAttr "FKPelvis_M.rp" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].trp"
		;
connectAttr "FKPelvis_M.rpt" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].trt"
		;
connectAttr "FKPelvis_M.r" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tr"
		;
connectAttr "FKPelvis_M.ro" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tro"
		;
connectAttr "FKPelvis_M.s" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].ts"
		;
connectAttr "FKPelvis_M.pm" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.w0" "FKIKParentConstraintSpine_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.ctx" "FKIKParentConstraintArm_L.tx"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.cty" "FKIKParentConstraintArm_L.ty"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.ctz" "FKIKParentConstraintArm_L.tz"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.crx" "FKIKParentConstraintArm_L.rx"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.cry" "FKIKParentConstraintArm_L.ry"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.crz" "FKIKParentConstraintArm_L.rz"
		;
connectAttr "FKIKParentConstraintArm_L.ro" "FKIKParentConstraintArm_L_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintArm_L.pim" "FKIKParentConstraintArm_L_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintArm_L.rp" "FKIKParentConstraintArm_L_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintArm_L.rpt" "FKIKParentConstraintArm_L_parentConstraint1.crt"
		;
connectAttr "Clavicle_L.t" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Clavicle_L.rp" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Clavicle_L.rpt" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Clavicle_L.r" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Clavicle_L.ro" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Clavicle_L.s" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Clavicle_L.pm" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Clavicle_L.jo" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.w0" "FKIKParentConstraintArm_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctx" "FKIKParentConstraintLeg_L.tx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cty" "FKIKParentConstraintLeg_L.ty"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctz" "FKIKParentConstraintLeg_L.tz"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crx" "FKIKParentConstraintLeg_L.rx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cry" "FKIKParentConstraintLeg_L.ry"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crz" "FKIKParentConstraintLeg_L.rz"
		;
connectAttr "FKIKParentConstraintLeg_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_L.pim" "FKIKParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "HipTwist_L.t" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "HipTwist_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_L.r" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "HipTwist_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_L.s" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "HipTwist_L.pm" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_L.jo" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.w0" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.cry" "PelvisCenterBtwLegs_M.ry"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctx" "PelvisCenterBtwLegs_M.tx"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctz" "PelvisCenterBtwLegs_M.tz"
		;
connectAttr "PelvisCenterBtwLegs_M.ro" "PelvisCenterBtwLegs_M_orientConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_orientConstraint1.cpim"
		;
connectAttr "IKLeg_R.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w0" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tr";
connectAttr "IKLeg_L.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tro";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w1" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tw"
		;
connectAttr "CenterBtwFeet_M.ox" "PelvisCenterBtwLegs_M_pointConstraint1.nds";
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_pointConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegs_M.rp" "PelvisCenterBtwLegs_M_pointConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegs_M.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.crt"
		;
connectAttr "IKLeg_R.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w0" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tt";
connectAttr "IKLeg_L.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trp";
connectAttr "IKLeg_L.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trt";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w1" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tw"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.ctx" "TwistFollowPelvis_M.tx"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.cty" "TwistFollowPelvis_M.ty"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.ctz" "TwistFollowPelvis_M.tz"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.crx" "TwistFollowPelvis_M.rx"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.cry" "TwistFollowPelvis_M.ry"
		;
connectAttr "TwistFollowPelvis_M_parentConstraint1.crz" "TwistFollowPelvis_M.rz"
		;
connectAttr "TwistFollowPelvis_M.ro" "TwistFollowPelvis_M_parentConstraint1.cro"
		;
connectAttr "TwistFollowPelvis_M.pim" "TwistFollowPelvis_M_parentConstraint1.cpim"
		;
connectAttr "TwistFollowPelvis_M.rp" "TwistFollowPelvis_M_parentConstraint1.crp"
		;
connectAttr "TwistFollowPelvis_M.rpt" "TwistFollowPelvis_M_parentConstraint1.crt"
		;
connectAttr "IKSpine0_M.t" "TwistFollowPelvis_M_parentConstraint1.tg[0].tt";
connectAttr "IKSpine0_M.rp" "TwistFollowPelvis_M_parentConstraint1.tg[0].trp";
connectAttr "IKSpine0_M.rpt" "TwistFollowPelvis_M_parentConstraint1.tg[0].trt";
connectAttr "IKSpine0_M.r" "TwistFollowPelvis_M_parentConstraint1.tg[0].tr";
connectAttr "IKSpine0_M.ro" "TwistFollowPelvis_M_parentConstraint1.tg[0].tro";
connectAttr "IKSpine0_M.s" "TwistFollowPelvis_M_parentConstraint1.tg[0].ts";
connectAttr "IKSpine0_M.pm" "TwistFollowPelvis_M_parentConstraint1.tg[0].tpm";
connectAttr "TwistFollowPelvis_M_parentConstraint1.w0" "TwistFollowPelvis_M_parentConstraint1.tg[0].tw"
		;
connectAttr "jointLayer.di" "UnTwistPelvis_M.do";
connectAttr "UnTwistPelvis_M.s" "UnTwistEndPelvis_M.is";
connectAttr "UnTwistEndPelvis_M_orientConstraint1.crx" "UnTwistEndPelvis_M.rx";
connectAttr "UnTwistEndPelvis_M_orientConstraint1.cry" "UnTwistEndPelvis_M.ry";
connectAttr "UnTwistEndPelvis_M_orientConstraint1.crz" "UnTwistEndPelvis_M.rz";
connectAttr "jointLayer.di" "UnTwistEndPelvis_M.do";
connectAttr "UnTwistEndPelvis_M.ro" "UnTwistEndPelvis_M_orientConstraint1.cro";
connectAttr "UnTwistEndPelvis_M.pim" "UnTwistEndPelvis_M_orientConstraint1.cpim"
		;
connectAttr "UnTwistEndPelvis_M.jo" "UnTwistEndPelvis_M_orientConstraint1.cjo";
connectAttr "IKSpine4AlignUnTwistTo_M.r" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tr"
		;
connectAttr "IKSpine4AlignUnTwistTo_M.ro" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tro"
		;
connectAttr "IKSpine4AlignUnTwistTo_M.pm" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tpm"
		;
connectAttr "UnTwistEndPelvis_M_orientConstraint1.w0" "UnTwistEndPelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "UnTwistEndPelvis_M.tx" "effector5.tx";
connectAttr "UnTwistEndPelvis_M.ty" "effector5.ty";
connectAttr "UnTwistEndPelvis_M.tz" "effector5.tz";
connectAttr "UnTwistPelvis_M.msg" "UnTwistIKPelvis_M.hsj";
connectAttr "effector5.hp" "UnTwistIKPelvis_M.hee";
connectAttr "ikSCsolver.msg" "UnTwistIKPelvis_M.hsv";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.ctx" "UnTwistIKPelvis_M.tx";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.cty" "UnTwistIKPelvis_M.ty";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.ctz" "UnTwistIKPelvis_M.tz";
connectAttr "UnTwistIKPelvis_M.pim" "UnTwistIKPelvis_M_pointConstraint1.cpim";
connectAttr "UnTwistIKPelvis_M.rp" "UnTwistIKPelvis_M_pointConstraint1.crp";
connectAttr "UnTwistIKPelvis_M.rpt" "UnTwistIKPelvis_M_pointConstraint1.crt";
connectAttr "IKSpine4_M.t" "UnTwistIKPelvis_M_pointConstraint1.tg[0].tt";
connectAttr "IKSpine4_M.rp" "UnTwistIKPelvis_M_pointConstraint1.tg[0].trp";
connectAttr "IKSpine4_M.rpt" "UnTwistIKPelvis_M_pointConstraint1.tg[0].trt";
connectAttr "IKSpine4_M.pm" "UnTwistIKPelvis_M_pointConstraint1.tg[0].tpm";
connectAttr "UnTwistIKPelvis_M_pointConstraint1.w0" "UnTwistIKPelvis_M_pointConstraint1.tg[0].tw"
		;
connectAttr "Pelvis_M_pointConstraint1.ctx" "Pelvis_M.tx";
connectAttr "Pelvis_M_pointConstraint1.cty" "Pelvis_M.ty";
connectAttr "Pelvis_M_pointConstraint1.ctz" "Pelvis_M.tz";
connectAttr "Pelvis_M_orientConstraint1.crx" "Pelvis_M.rx";
connectAttr "Pelvis_M_orientConstraint1.cry" "Pelvis_M.ry";
connectAttr "Pelvis_M_orientConstraint1.crz" "Pelvis_M.rz";
connectAttr "ScaleBlendPelvis_M.op" "Pelvis_M.s";
connectAttr "jointLayer.di" "Pelvis_M.do";
connectAttr "ScaleBlendSpineA_M.op" "SpineA_M.s";
connectAttr "Pelvis_M.s" "SpineA_M.is";
connectAttr "SpineA_M_pointConstraint1.ctx" "SpineA_M.tx";
connectAttr "SpineA_M_pointConstraint1.cty" "SpineA_M.ty";
connectAttr "SpineA_M_pointConstraint1.ctz" "SpineA_M.tz";
connectAttr "SpineA_M_orientConstraint1.crx" "SpineA_M.rx";
connectAttr "SpineA_M_orientConstraint1.cry" "SpineA_M.ry";
connectAttr "SpineA_M_orientConstraint1.crz" "SpineA_M.rz";
connectAttr "jointLayer.di" "SpineA_M.do";
connectAttr "Chest_M_pointConstraint1.ctx" "Chest_M.tx";
connectAttr "Chest_M_pointConstraint1.cty" "Chest_M.ty";
connectAttr "Chest_M_pointConstraint1.ctz" "Chest_M.tz";
connectAttr "Chest_M_orientConstraint1.crx" "Chest_M.rx";
connectAttr "Chest_M_orientConstraint1.cry" "Chest_M.ry";
connectAttr "Chest_M_orientConstraint1.crz" "Chest_M.rz";
connectAttr "ScaleBlendChest_M.op" "Chest_M.s";
connectAttr "SpineA_M.s" "Chest_M.is";
connectAttr "jointLayer.di" "Chest_M.do";
connectAttr "Clavicle_R_parentConstraint1.ctx" "Clavicle_R.tx";
connectAttr "Clavicle_R_parentConstraint1.cty" "Clavicle_R.ty";
connectAttr "Clavicle_R_parentConstraint1.ctz" "Clavicle_R.tz";
connectAttr "Clavicle_R_parentConstraint1.crx" "Clavicle_R.rx";
connectAttr "Clavicle_R_parentConstraint1.cry" "Clavicle_R.ry";
connectAttr "Clavicle_R_parentConstraint1.crz" "Clavicle_R.rz";
connectAttr "FKClavicle_R.s" "Clavicle_R.s";
connectAttr "Chest_M.s" "Clavicle_R.is";
connectAttr "jointLayer.di" "Clavicle_R.do";
connectAttr "ScaleBlendShoulder_R.op" "Shoulder_R.s";
connectAttr "Clavicle_R.s" "Shoulder_R.is";
connectAttr "Shoulder_R_parentConstraint1.ctx" "Shoulder_R.tx";
connectAttr "Shoulder_R_parentConstraint1.cty" "Shoulder_R.ty";
connectAttr "Shoulder_R_parentConstraint1.ctz" "Shoulder_R.tz";
connectAttr "Shoulder_R_parentConstraint1.crx" "Shoulder_R.rx";
connectAttr "Shoulder_R_parentConstraint1.cry" "Shoulder_R.ry";
connectAttr "Shoulder_R_parentConstraint1.crz" "Shoulder_R.rz";
connectAttr "jointLayer.di" "Shoulder_R.do";
connectAttr "ScaleBlendElbow_R.op" "Elbow_R.s";
connectAttr "Shoulder_R.s" "Elbow_R.is";
connectAttr "Elbow_R_parentConstraint1.ctx" "Elbow_R.tx";
connectAttr "Elbow_R_parentConstraint1.cty" "Elbow_R.ty";
connectAttr "Elbow_R_parentConstraint1.ctz" "Elbow_R.tz";
connectAttr "Elbow_R_parentConstraint1.crx" "Elbow_R.rx";
connectAttr "Elbow_R_parentConstraint1.cry" "Elbow_R.ry";
connectAttr "Elbow_R_parentConstraint1.crz" "Elbow_R.rz";
connectAttr "jointLayer.di" "Elbow_R.do";
connectAttr "Wrist_R_parentConstraint1.ctx" "Wrist_R.tx";
connectAttr "Wrist_R_parentConstraint1.cty" "Wrist_R.ty";
connectAttr "Wrist_R_parentConstraint1.ctz" "Wrist_R.tz";
connectAttr "Wrist_R_parentConstraint1.crx" "Wrist_R.rx";
connectAttr "Wrist_R_parentConstraint1.cry" "Wrist_R.ry";
connectAttr "Wrist_R_parentConstraint1.crz" "Wrist_R.rz";
connectAttr "ScaleBlendWrist_R.op" "Wrist_R.s";
connectAttr "Elbow_R.s" "Wrist_R.is";
connectAttr "jointLayer.di" "Wrist_R.do";
connectAttr "FKMiddleFinger1_R.s" "MiddleFinger1_R.s";
connectAttr "Wrist_R.s" "MiddleFinger1_R.is";
connectAttr "MiddleFinger1_R_parentConstraint1.ctx" "MiddleFinger1_R.tx";
connectAttr "MiddleFinger1_R_parentConstraint1.cty" "MiddleFinger1_R.ty";
connectAttr "MiddleFinger1_R_parentConstraint1.ctz" "MiddleFinger1_R.tz";
connectAttr "MiddleFinger1_R_parentConstraint1.crx" "MiddleFinger1_R.rx";
connectAttr "MiddleFinger1_R_parentConstraint1.cry" "MiddleFinger1_R.ry";
connectAttr "MiddleFinger1_R_parentConstraint1.crz" "MiddleFinger1_R.rz";
connectAttr "jointLayer.di" "MiddleFinger1_R.do";
connectAttr "FKMiddleFinger2_R.s" "MiddleFinger2_R.s";
connectAttr "MiddleFinger1_R.s" "MiddleFinger2_R.is";
connectAttr "MiddleFinger2_R_parentConstraint1.ctx" "MiddleFinger2_R.tx";
connectAttr "MiddleFinger2_R_parentConstraint1.cty" "MiddleFinger2_R.ty";
connectAttr "MiddleFinger2_R_parentConstraint1.ctz" "MiddleFinger2_R.tz";
connectAttr "MiddleFinger2_R_parentConstraint1.crx" "MiddleFinger2_R.rx";
connectAttr "MiddleFinger2_R_parentConstraint1.cry" "MiddleFinger2_R.ry";
connectAttr "MiddleFinger2_R_parentConstraint1.crz" "MiddleFinger2_R.rz";
connectAttr "jointLayer.di" "MiddleFinger2_R.do";
connectAttr "MiddleFinger2_R.s" "MiddleFinger3_End_R.is";
connectAttr "jointLayer.di" "MiddleFinger3_End_R.do";
connectAttr "MiddleFinger2_R.ro" "MiddleFinger2_R_parentConstraint1.cro";
connectAttr "MiddleFinger2_R.pim" "MiddleFinger2_R_parentConstraint1.cpim";
connectAttr "MiddleFinger2_R.rp" "MiddleFinger2_R_parentConstraint1.crp";
connectAttr "MiddleFinger2_R.rpt" "MiddleFinger2_R_parentConstraint1.crt";
connectAttr "MiddleFinger2_R.jo" "MiddleFinger2_R_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger2_R.t" "MiddleFinger2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger2_R.rp" "MiddleFinger2_R_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger2_R.rpt" "MiddleFinger2_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger2_R.r" "MiddleFinger2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger2_R.ro" "MiddleFinger2_R_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger2_R.s" "MiddleFinger2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger2_R.pm" "MiddleFinger2_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger2_R.jo" "MiddleFinger2_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger2_R_parentConstraint1.w0" "MiddleFinger2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "MiddleFinger1_R.ro" "MiddleFinger1_R_parentConstraint1.cro";
connectAttr "MiddleFinger1_R.pim" "MiddleFinger1_R_parentConstraint1.cpim";
connectAttr "MiddleFinger1_R.rp" "MiddleFinger1_R_parentConstraint1.crp";
connectAttr "MiddleFinger1_R.rpt" "MiddleFinger1_R_parentConstraint1.crt";
connectAttr "MiddleFinger1_R.jo" "MiddleFinger1_R_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger1_R.t" "MiddleFinger1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger1_R.rp" "MiddleFinger1_R_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger1_R.rpt" "MiddleFinger1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger1_R.r" "MiddleFinger1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger1_R.ro" "MiddleFinger1_R_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger1_R.s" "MiddleFinger1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger1_R.pm" "MiddleFinger1_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger1_R.jo" "MiddleFinger1_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger1_R_parentConstraint1.w0" "MiddleFinger1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIndexFinger1_R.s" "IndexFinger1_R.s";
connectAttr "Wrist_R.s" "IndexFinger1_R.is";
connectAttr "IndexFinger1_R_parentConstraint1.ctx" "IndexFinger1_R.tx";
connectAttr "IndexFinger1_R_parentConstraint1.cty" "IndexFinger1_R.ty";
connectAttr "IndexFinger1_R_parentConstraint1.ctz" "IndexFinger1_R.tz";
connectAttr "IndexFinger1_R_parentConstraint1.crx" "IndexFinger1_R.rx";
connectAttr "IndexFinger1_R_parentConstraint1.cry" "IndexFinger1_R.ry";
connectAttr "IndexFinger1_R_parentConstraint1.crz" "IndexFinger1_R.rz";
connectAttr "jointLayer.di" "IndexFinger1_R.do";
connectAttr "FKIndexFinger2_R.s" "IndexFinger2_R.s";
connectAttr "IndexFinger1_R.s" "IndexFinger2_R.is";
connectAttr "IndexFinger2_R_parentConstraint1.ctx" "IndexFinger2_R.tx";
connectAttr "IndexFinger2_R_parentConstraint1.cty" "IndexFinger2_R.ty";
connectAttr "IndexFinger2_R_parentConstraint1.ctz" "IndexFinger2_R.tz";
connectAttr "IndexFinger2_R_parentConstraint1.crx" "IndexFinger2_R.rx";
connectAttr "IndexFinger2_R_parentConstraint1.cry" "IndexFinger2_R.ry";
connectAttr "IndexFinger2_R_parentConstraint1.crz" "IndexFinger2_R.rz";
connectAttr "jointLayer.di" "IndexFinger2_R.do";
connectAttr "IndexFinger2_R.s" "IndexFinger3_End_R.is";
connectAttr "jointLayer.di" "IndexFinger3_End_R.do";
connectAttr "IndexFinger2_R.ro" "IndexFinger2_R_parentConstraint1.cro";
connectAttr "IndexFinger2_R.pim" "IndexFinger2_R_parentConstraint1.cpim";
connectAttr "IndexFinger2_R.rp" "IndexFinger2_R_parentConstraint1.crp";
connectAttr "IndexFinger2_R.rpt" "IndexFinger2_R_parentConstraint1.crt";
connectAttr "IndexFinger2_R.jo" "IndexFinger2_R_parentConstraint1.cjo";
connectAttr "FKXIndexFinger2_R.t" "IndexFinger2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger2_R.rp" "IndexFinger2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger2_R.rpt" "IndexFinger2_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger2_R.r" "IndexFinger2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger2_R.ro" "IndexFinger2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger2_R.s" "IndexFinger2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger2_R.pm" "IndexFinger2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger2_R.jo" "IndexFinger2_R_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger2_R_parentConstraint1.w0" "IndexFinger2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IndexFinger1_R.ro" "IndexFinger1_R_parentConstraint1.cro";
connectAttr "IndexFinger1_R.pim" "IndexFinger1_R_parentConstraint1.cpim";
connectAttr "IndexFinger1_R.rp" "IndexFinger1_R_parentConstraint1.crp";
connectAttr "IndexFinger1_R.rpt" "IndexFinger1_R_parentConstraint1.crt";
connectAttr "IndexFinger1_R.jo" "IndexFinger1_R_parentConstraint1.cjo";
connectAttr "FKXIndexFinger1_R.t" "IndexFinger1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger1_R.rp" "IndexFinger1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger1_R.rpt" "IndexFinger1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger1_R.r" "IndexFinger1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger1_R.ro" "IndexFinger1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger1_R.s" "IndexFinger1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger1_R.pm" "IndexFinger1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger1_R.jo" "IndexFinger1_R_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger1_R_parentConstraint1.w0" "IndexFinger1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKThumbFinger1_R.s" "ThumbFinger1_R.s";
connectAttr "Wrist_R.s" "ThumbFinger1_R.is";
connectAttr "ThumbFinger1_R_parentConstraint1.ctx" "ThumbFinger1_R.tx";
connectAttr "ThumbFinger1_R_parentConstraint1.cty" "ThumbFinger1_R.ty";
connectAttr "ThumbFinger1_R_parentConstraint1.ctz" "ThumbFinger1_R.tz";
connectAttr "ThumbFinger1_R_parentConstraint1.crx" "ThumbFinger1_R.rx";
connectAttr "ThumbFinger1_R_parentConstraint1.cry" "ThumbFinger1_R.ry";
connectAttr "ThumbFinger1_R_parentConstraint1.crz" "ThumbFinger1_R.rz";
connectAttr "jointLayer.di" "ThumbFinger1_R.do";
connectAttr "FKThumbFinger2_R.s" "ThumbFinger2_R.s";
connectAttr "ThumbFinger1_R.s" "ThumbFinger2_R.is";
connectAttr "ThumbFinger2_R_parentConstraint1.ctx" "ThumbFinger2_R.tx";
connectAttr "ThumbFinger2_R_parentConstraint1.cty" "ThumbFinger2_R.ty";
connectAttr "ThumbFinger2_R_parentConstraint1.ctz" "ThumbFinger2_R.tz";
connectAttr "ThumbFinger2_R_parentConstraint1.crx" "ThumbFinger2_R.rx";
connectAttr "ThumbFinger2_R_parentConstraint1.cry" "ThumbFinger2_R.ry";
connectAttr "ThumbFinger2_R_parentConstraint1.crz" "ThumbFinger2_R.rz";
connectAttr "jointLayer.di" "ThumbFinger2_R.do";
connectAttr "ThumbFinger2_R.s" "ThumbFinger3_End_R.is";
connectAttr "jointLayer.di" "ThumbFinger3_End_R.do";
connectAttr "ThumbFinger2_R.ro" "ThumbFinger2_R_parentConstraint1.cro";
connectAttr "ThumbFinger2_R.pim" "ThumbFinger2_R_parentConstraint1.cpim";
connectAttr "ThumbFinger2_R.rp" "ThumbFinger2_R_parentConstraint1.crp";
connectAttr "ThumbFinger2_R.rpt" "ThumbFinger2_R_parentConstraint1.crt";
connectAttr "ThumbFinger2_R.jo" "ThumbFinger2_R_parentConstraint1.cjo";
connectAttr "FKXThumbFinger2_R.t" "ThumbFinger2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger2_R.rp" "ThumbFinger2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger2_R.rpt" "ThumbFinger2_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger2_R.r" "ThumbFinger2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger2_R.ro" "ThumbFinger2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger2_R.s" "ThumbFinger2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger2_R.pm" "ThumbFinger2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger2_R.jo" "ThumbFinger2_R_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger2_R_parentConstraint1.w0" "ThumbFinger2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "ThumbFinger1_R.ro" "ThumbFinger1_R_parentConstraint1.cro";
connectAttr "ThumbFinger1_R.pim" "ThumbFinger1_R_parentConstraint1.cpim";
connectAttr "ThumbFinger1_R.rp" "ThumbFinger1_R_parentConstraint1.crp";
connectAttr "ThumbFinger1_R.rpt" "ThumbFinger1_R_parentConstraint1.crt";
connectAttr "ThumbFinger1_R.jo" "ThumbFinger1_R_parentConstraint1.cjo";
connectAttr "FKXThumbFinger1_R.t" "ThumbFinger1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger1_R.rp" "ThumbFinger1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger1_R.rpt" "ThumbFinger1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger1_R.r" "ThumbFinger1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger1_R.ro" "ThumbFinger1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger1_R.s" "ThumbFinger1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger1_R.pm" "ThumbFinger1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger1_R.jo" "ThumbFinger1_R_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger1_R_parentConstraint1.w0" "ThumbFinger1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Wrist_R.ro" "Wrist_R_parentConstraint1.cro";
connectAttr "Wrist_R.pim" "Wrist_R_parentConstraint1.cpim";
connectAttr "Wrist_R.rp" "Wrist_R_parentConstraint1.crp";
connectAttr "Wrist_R.rpt" "Wrist_R_parentConstraint1.crt";
connectAttr "Wrist_R.jo" "Wrist_R_parentConstraint1.cjo";
connectAttr "FKXWrist_R.t" "Wrist_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWrist_R.rp" "Wrist_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWrist_R.rpt" "Wrist_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWrist_R.r" "Wrist_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWrist_R.ro" "Wrist_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWrist_R.s" "Wrist_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWrist_R.pm" "Wrist_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWrist_R.jo" "Wrist_R_parentConstraint1.tg[0].tjo";
connectAttr "Wrist_R_parentConstraint1.w0" "Wrist_R_parentConstraint1.tg[0].tw";
connectAttr "IKXWrist_R.t" "Wrist_R_parentConstraint1.tg[1].tt";
connectAttr "IKXWrist_R.rp" "Wrist_R_parentConstraint1.tg[1].trp";
connectAttr "IKXWrist_R.rpt" "Wrist_R_parentConstraint1.tg[1].trt";
connectAttr "IKXWrist_R.r" "Wrist_R_parentConstraint1.tg[1].tr";
connectAttr "IKXWrist_R.ro" "Wrist_R_parentConstraint1.tg[1].tro";
connectAttr "IKXWrist_R.s" "Wrist_R_parentConstraint1.tg[1].ts";
connectAttr "IKXWrist_R.pm" "Wrist_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXWrist_R.jo" "Wrist_R_parentConstraint1.tg[1].tjo";
connectAttr "Wrist_R_parentConstraint1.w1" "Wrist_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendArmReverse_R.ox" "Wrist_R_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_R.o" "Wrist_R_parentConstraint1.w1";
connectAttr "Elbow_R.ro" "Elbow_R_parentConstraint1.cro";
connectAttr "Elbow_R.pim" "Elbow_R_parentConstraint1.cpim";
connectAttr "Elbow_R.rp" "Elbow_R_parentConstraint1.crp";
connectAttr "Elbow_R.rpt" "Elbow_R_parentConstraint1.crt";
connectAttr "Elbow_R.jo" "Elbow_R_parentConstraint1.cjo";
connectAttr "FKXElbow_R.t" "Elbow_R_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow_R.rp" "Elbow_R_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow_R.rpt" "Elbow_R_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow_R.r" "Elbow_R_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow_R.ro" "Elbow_R_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow_R.s" "Elbow_R_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow_R.pm" "Elbow_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow_R.jo" "Elbow_R_parentConstraint1.tg[0].tjo";
connectAttr "Elbow_R_parentConstraint1.w0" "Elbow_R_parentConstraint1.tg[0].tw";
connectAttr "IKXElbow_R.t" "Elbow_R_parentConstraint1.tg[1].tt";
connectAttr "IKXElbow_R.rp" "Elbow_R_parentConstraint1.tg[1].trp";
connectAttr "IKXElbow_R.rpt" "Elbow_R_parentConstraint1.tg[1].trt";
connectAttr "IKXElbow_R.r" "Elbow_R_parentConstraint1.tg[1].tr";
connectAttr "IKXElbow_R.ro" "Elbow_R_parentConstraint1.tg[1].tro";
connectAttr "IKXElbow_R.s" "Elbow_R_parentConstraint1.tg[1].ts";
connectAttr "IKXElbow_R.pm" "Elbow_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXElbow_R.jo" "Elbow_R_parentConstraint1.tg[1].tjo";
connectAttr "Elbow_R_parentConstraint1.w1" "Elbow_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendArmReverse_R.ox" "Elbow_R_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_R.o" "Elbow_R_parentConstraint1.w1";
connectAttr "Shoulder_R.ro" "Shoulder_R_parentConstraint1.cro";
connectAttr "Shoulder_R.pim" "Shoulder_R_parentConstraint1.cpim";
connectAttr "Shoulder_R.rp" "Shoulder_R_parentConstraint1.crp";
connectAttr "Shoulder_R.rpt" "Shoulder_R_parentConstraint1.crt";
connectAttr "Shoulder_R.jo" "Shoulder_R_parentConstraint1.cjo";
connectAttr "FKXShoulder_R.t" "Shoulder_R_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder_R.rp" "Shoulder_R_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder_R.rpt" "Shoulder_R_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder_R.r" "Shoulder_R_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder_R.ro" "Shoulder_R_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder_R.s" "Shoulder_R_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder_R.pm" "Shoulder_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder_R.jo" "Shoulder_R_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder_R_parentConstraint1.w0" "Shoulder_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXShoulder_R.t" "Shoulder_R_parentConstraint1.tg[1].tt";
connectAttr "IKXShoulder_R.rp" "Shoulder_R_parentConstraint1.tg[1].trp";
connectAttr "IKXShoulder_R.rpt" "Shoulder_R_parentConstraint1.tg[1].trt";
connectAttr "IKXShoulder_R.r" "Shoulder_R_parentConstraint1.tg[1].tr";
connectAttr "IKXShoulder_R.ro" "Shoulder_R_parentConstraint1.tg[1].tro";
connectAttr "IKXShoulder_R.s" "Shoulder_R_parentConstraint1.tg[1].ts";
connectAttr "IKXShoulder_R.pm" "Shoulder_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXShoulder_R.jo" "Shoulder_R_parentConstraint1.tg[1].tjo";
connectAttr "Shoulder_R_parentConstraint1.w1" "Shoulder_R_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArmReverse_R.ox" "Shoulder_R_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_R.o" "Shoulder_R_parentConstraint1.w1";
connectAttr "Clavicle_R.ro" "Clavicle_R_parentConstraint1.cro";
connectAttr "Clavicle_R.pim" "Clavicle_R_parentConstraint1.cpim";
connectAttr "Clavicle_R.rp" "Clavicle_R_parentConstraint1.crp";
connectAttr "Clavicle_R.rpt" "Clavicle_R_parentConstraint1.crt";
connectAttr "Clavicle_R.jo" "Clavicle_R_parentConstraint1.cjo";
connectAttr "FKXClavicle_R.t" "Clavicle_R_parentConstraint1.tg[0].tt";
connectAttr "FKXClavicle_R.rp" "Clavicle_R_parentConstraint1.tg[0].trp";
connectAttr "FKXClavicle_R.rpt" "Clavicle_R_parentConstraint1.tg[0].trt";
connectAttr "FKXClavicle_R.r" "Clavicle_R_parentConstraint1.tg[0].tr";
connectAttr "FKXClavicle_R.ro" "Clavicle_R_parentConstraint1.tg[0].tro";
connectAttr "FKXClavicle_R.s" "Clavicle_R_parentConstraint1.tg[0].ts";
connectAttr "FKXClavicle_R.pm" "Clavicle_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXClavicle_R.jo" "Clavicle_R_parentConstraint1.tg[0].tjo";
connectAttr "Clavicle_R_parentConstraint1.w0" "Clavicle_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Chest_M.s" "Chest_End_M.is";
connectAttr "jointLayer.di" "Chest_End_M.do";
connectAttr "FKShoulderPad1_R.s" "ShoulderPad1_R.s";
connectAttr "Chest_M.s" "ShoulderPad1_R.is";
connectAttr "ShoulderPad1_R_parentConstraint1.ctx" "ShoulderPad1_R.tx";
connectAttr "ShoulderPad1_R_parentConstraint1.cty" "ShoulderPad1_R.ty";
connectAttr "ShoulderPad1_R_parentConstraint1.ctz" "ShoulderPad1_R.tz";
connectAttr "ShoulderPad1_R_parentConstraint1.crx" "ShoulderPad1_R.rx";
connectAttr "ShoulderPad1_R_parentConstraint1.cry" "ShoulderPad1_R.ry";
connectAttr "ShoulderPad1_R_parentConstraint1.crz" "ShoulderPad1_R.rz";
connectAttr "jointLayer.di" "ShoulderPad1_R.do";
connectAttr "jointLayer.di" "ShoulderPad1_End_R.do";
connectAttr "ShoulderPad1_R.ro" "ShoulderPad1_R_parentConstraint1.cro";
connectAttr "ShoulderPad1_R.pim" "ShoulderPad1_R_parentConstraint1.cpim";
connectAttr "ShoulderPad1_R.rp" "ShoulderPad1_R_parentConstraint1.crp";
connectAttr "ShoulderPad1_R.rpt" "ShoulderPad1_R_parentConstraint1.crt";
connectAttr "ShoulderPad1_R.jo" "ShoulderPad1_R_parentConstraint1.cjo";
connectAttr "FKXShoulderPad1_R.t" "ShoulderPad1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulderPad1_R.rp" "ShoulderPad1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulderPad1_R.rpt" "ShoulderPad1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXShoulderPad1_R.r" "ShoulderPad1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulderPad1_R.ro" "ShoulderPad1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulderPad1_R.s" "ShoulderPad1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulderPad1_R.pm" "ShoulderPad1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulderPad1_R.jo" "ShoulderPad1_R_parentConstraint1.tg[0].tjo";
connectAttr "ShoulderPad1_R_parentConstraint1.w0" "ShoulderPad1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKShoulderPad2_R.s" "ShoulderPad2_R.s";
connectAttr "Chest_M.s" "ShoulderPad2_R.is";
connectAttr "ShoulderPad2_R_parentConstraint1.ctx" "ShoulderPad2_R.tx";
connectAttr "ShoulderPad2_R_parentConstraint1.cty" "ShoulderPad2_R.ty";
connectAttr "ShoulderPad2_R_parentConstraint1.ctz" "ShoulderPad2_R.tz";
connectAttr "ShoulderPad2_R_parentConstraint1.crx" "ShoulderPad2_R.rx";
connectAttr "ShoulderPad2_R_parentConstraint1.cry" "ShoulderPad2_R.ry";
connectAttr "ShoulderPad2_R_parentConstraint1.crz" "ShoulderPad2_R.rz";
connectAttr "jointLayer.di" "ShoulderPad2_R.do";
connectAttr "jointLayer.di" "ShoulderPad2_End_R.do";
connectAttr "ShoulderPad2_R.ro" "ShoulderPad2_R_parentConstraint1.cro";
connectAttr "ShoulderPad2_R.pim" "ShoulderPad2_R_parentConstraint1.cpim";
connectAttr "ShoulderPad2_R.rp" "ShoulderPad2_R_parentConstraint1.crp";
connectAttr "ShoulderPad2_R.rpt" "ShoulderPad2_R_parentConstraint1.crt";
connectAttr "ShoulderPad2_R.jo" "ShoulderPad2_R_parentConstraint1.cjo";
connectAttr "FKXShoulderPad2_R.t" "ShoulderPad2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulderPad2_R.rp" "ShoulderPad2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulderPad2_R.rpt" "ShoulderPad2_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXShoulderPad2_R.r" "ShoulderPad2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulderPad2_R.ro" "ShoulderPad2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulderPad2_R.s" "ShoulderPad2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulderPad2_R.pm" "ShoulderPad2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulderPad2_R.jo" "ShoulderPad2_R_parentConstraint1.tg[0].tjo";
connectAttr "ShoulderPad2_R_parentConstraint1.w0" "ShoulderPad2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Clavicle_L_parentConstraint1.ctx" "Clavicle_L.tx";
connectAttr "Clavicle_L_parentConstraint1.cty" "Clavicle_L.ty";
connectAttr "Clavicle_L_parentConstraint1.ctz" "Clavicle_L.tz";
connectAttr "Clavicle_L_parentConstraint1.crx" "Clavicle_L.rx";
connectAttr "Clavicle_L_parentConstraint1.cry" "Clavicle_L.ry";
connectAttr "Clavicle_L_parentConstraint1.crz" "Clavicle_L.rz";
connectAttr "FKClavicle_L.s" "Clavicle_L.s";
connectAttr "Chest_M.s" "Clavicle_L.is";
connectAttr "jointLayer.di" "Clavicle_L.do";
connectAttr "ScaleBlendShoulder_L.op" "Shoulder_L.s";
connectAttr "Clavicle_L.s" "Shoulder_L.is";
connectAttr "Shoulder_L_parentConstraint1.ctx" "Shoulder_L.tx";
connectAttr "Shoulder_L_parentConstraint1.cty" "Shoulder_L.ty";
connectAttr "Shoulder_L_parentConstraint1.ctz" "Shoulder_L.tz";
connectAttr "Shoulder_L_parentConstraint1.crx" "Shoulder_L.rx";
connectAttr "Shoulder_L_parentConstraint1.cry" "Shoulder_L.ry";
connectAttr "Shoulder_L_parentConstraint1.crz" "Shoulder_L.rz";
connectAttr "jointLayer.di" "Shoulder_L.do";
connectAttr "ScaleBlendElbow_L.op" "Elbow_L.s";
connectAttr "Shoulder_L.s" "Elbow_L.is";
connectAttr "Elbow_L_parentConstraint1.ctx" "Elbow_L.tx";
connectAttr "Elbow_L_parentConstraint1.cty" "Elbow_L.ty";
connectAttr "Elbow_L_parentConstraint1.ctz" "Elbow_L.tz";
connectAttr "Elbow_L_parentConstraint1.crx" "Elbow_L.rx";
connectAttr "Elbow_L_parentConstraint1.cry" "Elbow_L.ry";
connectAttr "Elbow_L_parentConstraint1.crz" "Elbow_L.rz";
connectAttr "jointLayer.di" "Elbow_L.do";
connectAttr "Wrist_L_parentConstraint1.ctx" "Wrist_L.tx";
connectAttr "Wrist_L_parentConstraint1.cty" "Wrist_L.ty";
connectAttr "Wrist_L_parentConstraint1.ctz" "Wrist_L.tz";
connectAttr "Wrist_L_parentConstraint1.crx" "Wrist_L.rx";
connectAttr "Wrist_L_parentConstraint1.cry" "Wrist_L.ry";
connectAttr "Wrist_L_parentConstraint1.crz" "Wrist_L.rz";
connectAttr "ScaleBlendWrist_L.op" "Wrist_L.s";
connectAttr "Elbow_L.s" "Wrist_L.is";
connectAttr "jointLayer.di" "Wrist_L.do";
connectAttr "FKMiddleFinger1_L.s" "MiddleFinger1_L.s";
connectAttr "Wrist_L.s" "MiddleFinger1_L.is";
connectAttr "MiddleFinger1_L_parentConstraint1.ctx" "MiddleFinger1_L.tx";
connectAttr "MiddleFinger1_L_parentConstraint1.cty" "MiddleFinger1_L.ty";
connectAttr "MiddleFinger1_L_parentConstraint1.ctz" "MiddleFinger1_L.tz";
connectAttr "MiddleFinger1_L_parentConstraint1.crx" "MiddleFinger1_L.rx";
connectAttr "MiddleFinger1_L_parentConstraint1.cry" "MiddleFinger1_L.ry";
connectAttr "MiddleFinger1_L_parentConstraint1.crz" "MiddleFinger1_L.rz";
connectAttr "jointLayer.di" "MiddleFinger1_L.do";
connectAttr "FKMiddleFinger2_L.s" "MiddleFinger2_L.s";
connectAttr "MiddleFinger1_L.s" "MiddleFinger2_L.is";
connectAttr "MiddleFinger2_L_parentConstraint1.ctx" "MiddleFinger2_L.tx";
connectAttr "MiddleFinger2_L_parentConstraint1.cty" "MiddleFinger2_L.ty";
connectAttr "MiddleFinger2_L_parentConstraint1.ctz" "MiddleFinger2_L.tz";
connectAttr "MiddleFinger2_L_parentConstraint1.crx" "MiddleFinger2_L.rx";
connectAttr "MiddleFinger2_L_parentConstraint1.cry" "MiddleFinger2_L.ry";
connectAttr "MiddleFinger2_L_parentConstraint1.crz" "MiddleFinger2_L.rz";
connectAttr "jointLayer.di" "MiddleFinger2_L.do";
connectAttr "MiddleFinger2_L.s" "MiddleFinger3_End_L.is";
connectAttr "jointLayer.di" "MiddleFinger3_End_L.do";
connectAttr "MiddleFinger2_L.ro" "MiddleFinger2_L_parentConstraint1.cro";
connectAttr "MiddleFinger2_L.pim" "MiddleFinger2_L_parentConstraint1.cpim";
connectAttr "MiddleFinger2_L.rp" "MiddleFinger2_L_parentConstraint1.crp";
connectAttr "MiddleFinger2_L.rpt" "MiddleFinger2_L_parentConstraint1.crt";
connectAttr "MiddleFinger2_L.jo" "MiddleFinger2_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger2_L.t" "MiddleFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger2_L.rp" "MiddleFinger2_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger2_L.rpt" "MiddleFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger2_L.r" "MiddleFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger2_L.ro" "MiddleFinger2_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger2_L.s" "MiddleFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger2_L.pm" "MiddleFinger2_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger2_L.jo" "MiddleFinger2_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger2_L_parentConstraint1.w0" "MiddleFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "MiddleFinger1_L.ro" "MiddleFinger1_L_parentConstraint1.cro";
connectAttr "MiddleFinger1_L.pim" "MiddleFinger1_L_parentConstraint1.cpim";
connectAttr "MiddleFinger1_L.rp" "MiddleFinger1_L_parentConstraint1.crp";
connectAttr "MiddleFinger1_L.rpt" "MiddleFinger1_L_parentConstraint1.crt";
connectAttr "MiddleFinger1_L.jo" "MiddleFinger1_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger1_L.t" "MiddleFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger1_L.rp" "MiddleFinger1_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger1_L.rpt" "MiddleFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger1_L.r" "MiddleFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger1_L.ro" "MiddleFinger1_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger1_L.s" "MiddleFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger1_L.pm" "MiddleFinger1_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger1_L.jo" "MiddleFinger1_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger1_L_parentConstraint1.w0" "MiddleFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIndexFinger1_L.s" "IndexFinger1_L.s";
connectAttr "Wrist_L.s" "IndexFinger1_L.is";
connectAttr "IndexFinger1_L_parentConstraint1.ctx" "IndexFinger1_L.tx";
connectAttr "IndexFinger1_L_parentConstraint1.cty" "IndexFinger1_L.ty";
connectAttr "IndexFinger1_L_parentConstraint1.ctz" "IndexFinger1_L.tz";
connectAttr "IndexFinger1_L_parentConstraint1.crx" "IndexFinger1_L.rx";
connectAttr "IndexFinger1_L_parentConstraint1.cry" "IndexFinger1_L.ry";
connectAttr "IndexFinger1_L_parentConstraint1.crz" "IndexFinger1_L.rz";
connectAttr "jointLayer.di" "IndexFinger1_L.do";
connectAttr "FKIndexFinger2_L.s" "IndexFinger2_L.s";
connectAttr "IndexFinger1_L.s" "IndexFinger2_L.is";
connectAttr "IndexFinger2_L_parentConstraint1.ctx" "IndexFinger2_L.tx";
connectAttr "IndexFinger2_L_parentConstraint1.cty" "IndexFinger2_L.ty";
connectAttr "IndexFinger2_L_parentConstraint1.ctz" "IndexFinger2_L.tz";
connectAttr "IndexFinger2_L_parentConstraint1.crx" "IndexFinger2_L.rx";
connectAttr "IndexFinger2_L_parentConstraint1.cry" "IndexFinger2_L.ry";
connectAttr "IndexFinger2_L_parentConstraint1.crz" "IndexFinger2_L.rz";
connectAttr "jointLayer.di" "IndexFinger2_L.do";
connectAttr "IndexFinger2_L.s" "IndexFinger3_End_L.is";
connectAttr "jointLayer.di" "IndexFinger3_End_L.do";
connectAttr "IndexFinger2_L.ro" "IndexFinger2_L_parentConstraint1.cro";
connectAttr "IndexFinger2_L.pim" "IndexFinger2_L_parentConstraint1.cpim";
connectAttr "IndexFinger2_L.rp" "IndexFinger2_L_parentConstraint1.crp";
connectAttr "IndexFinger2_L.rpt" "IndexFinger2_L_parentConstraint1.crt";
connectAttr "IndexFinger2_L.jo" "IndexFinger2_L_parentConstraint1.cjo";
connectAttr "FKXIndexFinger2_L.t" "IndexFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger2_L.rp" "IndexFinger2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger2_L.rpt" "IndexFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger2_L.r" "IndexFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger2_L.ro" "IndexFinger2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger2_L.s" "IndexFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger2_L.pm" "IndexFinger2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger2_L.jo" "IndexFinger2_L_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger2_L_parentConstraint1.w0" "IndexFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IndexFinger1_L.ro" "IndexFinger1_L_parentConstraint1.cro";
connectAttr "IndexFinger1_L.pim" "IndexFinger1_L_parentConstraint1.cpim";
connectAttr "IndexFinger1_L.rp" "IndexFinger1_L_parentConstraint1.crp";
connectAttr "IndexFinger1_L.rpt" "IndexFinger1_L_parentConstraint1.crt";
connectAttr "IndexFinger1_L.jo" "IndexFinger1_L_parentConstraint1.cjo";
connectAttr "FKXIndexFinger1_L.t" "IndexFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger1_L.rp" "IndexFinger1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger1_L.rpt" "IndexFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger1_L.r" "IndexFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger1_L.ro" "IndexFinger1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger1_L.s" "IndexFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger1_L.pm" "IndexFinger1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger1_L.jo" "IndexFinger1_L_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger1_L_parentConstraint1.w0" "IndexFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKThumbFinger1_L.s" "ThumbFinger1_L.s";
connectAttr "Wrist_L.s" "ThumbFinger1_L.is";
connectAttr "ThumbFinger1_L_parentConstraint1.ctx" "ThumbFinger1_L.tx";
connectAttr "ThumbFinger1_L_parentConstraint1.cty" "ThumbFinger1_L.ty";
connectAttr "ThumbFinger1_L_parentConstraint1.ctz" "ThumbFinger1_L.tz";
connectAttr "ThumbFinger1_L_parentConstraint1.crx" "ThumbFinger1_L.rx";
connectAttr "ThumbFinger1_L_parentConstraint1.cry" "ThumbFinger1_L.ry";
connectAttr "ThumbFinger1_L_parentConstraint1.crz" "ThumbFinger1_L.rz";
connectAttr "jointLayer.di" "ThumbFinger1_L.do";
connectAttr "FKThumbFinger2_L.s" "ThumbFinger2_L.s";
connectAttr "ThumbFinger1_L.s" "ThumbFinger2_L.is";
connectAttr "ThumbFinger2_L_parentConstraint1.ctx" "ThumbFinger2_L.tx";
connectAttr "ThumbFinger2_L_parentConstraint1.cty" "ThumbFinger2_L.ty";
connectAttr "ThumbFinger2_L_parentConstraint1.ctz" "ThumbFinger2_L.tz";
connectAttr "ThumbFinger2_L_parentConstraint1.crx" "ThumbFinger2_L.rx";
connectAttr "ThumbFinger2_L_parentConstraint1.cry" "ThumbFinger2_L.ry";
connectAttr "ThumbFinger2_L_parentConstraint1.crz" "ThumbFinger2_L.rz";
connectAttr "jointLayer.di" "ThumbFinger2_L.do";
connectAttr "ThumbFinger2_L.s" "ThumbFinger3_End_L.is";
connectAttr "jointLayer.di" "ThumbFinger3_End_L.do";
connectAttr "ThumbFinger2_L.ro" "ThumbFinger2_L_parentConstraint1.cro";
connectAttr "ThumbFinger2_L.pim" "ThumbFinger2_L_parentConstraint1.cpim";
connectAttr "ThumbFinger2_L.rp" "ThumbFinger2_L_parentConstraint1.crp";
connectAttr "ThumbFinger2_L.rpt" "ThumbFinger2_L_parentConstraint1.crt";
connectAttr "ThumbFinger2_L.jo" "ThumbFinger2_L_parentConstraint1.cjo";
connectAttr "FKXThumbFinger2_L.t" "ThumbFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger2_L.rp" "ThumbFinger2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger2_L.rpt" "ThumbFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger2_L.r" "ThumbFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger2_L.ro" "ThumbFinger2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger2_L.s" "ThumbFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger2_L.pm" "ThumbFinger2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger2_L.jo" "ThumbFinger2_L_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger2_L_parentConstraint1.w0" "ThumbFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "ThumbFinger1_L.ro" "ThumbFinger1_L_parentConstraint1.cro";
connectAttr "ThumbFinger1_L.pim" "ThumbFinger1_L_parentConstraint1.cpim";
connectAttr "ThumbFinger1_L.rp" "ThumbFinger1_L_parentConstraint1.crp";
connectAttr "ThumbFinger1_L.rpt" "ThumbFinger1_L_parentConstraint1.crt";
connectAttr "ThumbFinger1_L.jo" "ThumbFinger1_L_parentConstraint1.cjo";
connectAttr "FKXThumbFinger1_L.t" "ThumbFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger1_L.rp" "ThumbFinger1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger1_L.rpt" "ThumbFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger1_L.r" "ThumbFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger1_L.ro" "ThumbFinger1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger1_L.s" "ThumbFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger1_L.pm" "ThumbFinger1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger1_L.jo" "ThumbFinger1_L_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger1_L_parentConstraint1.w0" "ThumbFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Wrist_L.ro" "Wrist_L_parentConstraint1.cro";
connectAttr "Wrist_L.pim" "Wrist_L_parentConstraint1.cpim";
connectAttr "Wrist_L.rp" "Wrist_L_parentConstraint1.crp";
connectAttr "Wrist_L.rpt" "Wrist_L_parentConstraint1.crt";
connectAttr "Wrist_L.jo" "Wrist_L_parentConstraint1.cjo";
connectAttr "FKXWrist_L.t" "Wrist_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWrist_L.rp" "Wrist_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWrist_L.rpt" "Wrist_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWrist_L.r" "Wrist_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWrist_L.ro" "Wrist_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWrist_L.s" "Wrist_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWrist_L.pm" "Wrist_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWrist_L.jo" "Wrist_L_parentConstraint1.tg[0].tjo";
connectAttr "Wrist_L_parentConstraint1.w0" "Wrist_L_parentConstraint1.tg[0].tw";
connectAttr "IKXWrist_L.t" "Wrist_L_parentConstraint1.tg[1].tt";
connectAttr "IKXWrist_L.rp" "Wrist_L_parentConstraint1.tg[1].trp";
connectAttr "IKXWrist_L.rpt" "Wrist_L_parentConstraint1.tg[1].trt";
connectAttr "IKXWrist_L.r" "Wrist_L_parentConstraint1.tg[1].tr";
connectAttr "IKXWrist_L.ro" "Wrist_L_parentConstraint1.tg[1].tro";
connectAttr "IKXWrist_L.s" "Wrist_L_parentConstraint1.tg[1].ts";
connectAttr "IKXWrist_L.pm" "Wrist_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXWrist_L.jo" "Wrist_L_parentConstraint1.tg[1].tjo";
connectAttr "Wrist_L_parentConstraint1.w1" "Wrist_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendArmReverse_L.ox" "Wrist_L_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_L.o" "Wrist_L_parentConstraint1.w1";
connectAttr "Elbow_L.ro" "Elbow_L_parentConstraint1.cro";
connectAttr "Elbow_L.pim" "Elbow_L_parentConstraint1.cpim";
connectAttr "Elbow_L.rp" "Elbow_L_parentConstraint1.crp";
connectAttr "Elbow_L.rpt" "Elbow_L_parentConstraint1.crt";
connectAttr "Elbow_L.jo" "Elbow_L_parentConstraint1.cjo";
connectAttr "FKXElbow_L.t" "Elbow_L_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow_L.rp" "Elbow_L_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow_L.rpt" "Elbow_L_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow_L.r" "Elbow_L_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow_L.ro" "Elbow_L_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow_L.s" "Elbow_L_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow_L.pm" "Elbow_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow_L.jo" "Elbow_L_parentConstraint1.tg[0].tjo";
connectAttr "Elbow_L_parentConstraint1.w0" "Elbow_L_parentConstraint1.tg[0].tw";
connectAttr "IKXElbow_L.t" "Elbow_L_parentConstraint1.tg[1].tt";
connectAttr "IKXElbow_L.rp" "Elbow_L_parentConstraint1.tg[1].trp";
connectAttr "IKXElbow_L.rpt" "Elbow_L_parentConstraint1.tg[1].trt";
connectAttr "IKXElbow_L.r" "Elbow_L_parentConstraint1.tg[1].tr";
connectAttr "IKXElbow_L.ro" "Elbow_L_parentConstraint1.tg[1].tro";
connectAttr "IKXElbow_L.s" "Elbow_L_parentConstraint1.tg[1].ts";
connectAttr "IKXElbow_L.pm" "Elbow_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXElbow_L.jo" "Elbow_L_parentConstraint1.tg[1].tjo";
connectAttr "Elbow_L_parentConstraint1.w1" "Elbow_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendArmReverse_L.ox" "Elbow_L_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_L.o" "Elbow_L_parentConstraint1.w1";
connectAttr "Shoulder_L.ro" "Shoulder_L_parentConstraint1.cro";
connectAttr "Shoulder_L.pim" "Shoulder_L_parentConstraint1.cpim";
connectAttr "Shoulder_L.rp" "Shoulder_L_parentConstraint1.crp";
connectAttr "Shoulder_L.rpt" "Shoulder_L_parentConstraint1.crt";
connectAttr "Shoulder_L.jo" "Shoulder_L_parentConstraint1.cjo";
connectAttr "FKXShoulder_L.t" "Shoulder_L_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder_L.rp" "Shoulder_L_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder_L.rpt" "Shoulder_L_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder_L.r" "Shoulder_L_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder_L.ro" "Shoulder_L_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder_L.s" "Shoulder_L_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder_L.pm" "Shoulder_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder_L.jo" "Shoulder_L_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder_L_parentConstraint1.w0" "Shoulder_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXShoulder_L.t" "Shoulder_L_parentConstraint1.tg[1].tt";
connectAttr "IKXShoulder_L.rp" "Shoulder_L_parentConstraint1.tg[1].trp";
connectAttr "IKXShoulder_L.rpt" "Shoulder_L_parentConstraint1.tg[1].trt";
connectAttr "IKXShoulder_L.r" "Shoulder_L_parentConstraint1.tg[1].tr";
connectAttr "IKXShoulder_L.ro" "Shoulder_L_parentConstraint1.tg[1].tro";
connectAttr "IKXShoulder_L.s" "Shoulder_L_parentConstraint1.tg[1].ts";
connectAttr "IKXShoulder_L.pm" "Shoulder_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXShoulder_L.jo" "Shoulder_L_parentConstraint1.tg[1].tjo";
connectAttr "Shoulder_L_parentConstraint1.w1" "Shoulder_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendArmReverse_L.ox" "Shoulder_L_parentConstraint1.w0";
connectAttr "FKIKBlendArmUnitConversion_L.o" "Shoulder_L_parentConstraint1.w1";
connectAttr "Clavicle_L.ro" "Clavicle_L_parentConstraint1.cro";
connectAttr "Clavicle_L.pim" "Clavicle_L_parentConstraint1.cpim";
connectAttr "Clavicle_L.rp" "Clavicle_L_parentConstraint1.crp";
connectAttr "Clavicle_L.rpt" "Clavicle_L_parentConstraint1.crt";
connectAttr "Clavicle_L.jo" "Clavicle_L_parentConstraint1.cjo";
connectAttr "FKXClavicle_L.t" "Clavicle_L_parentConstraint1.tg[0].tt";
connectAttr "FKXClavicle_L.rp" "Clavicle_L_parentConstraint1.tg[0].trp";
connectAttr "FKXClavicle_L.rpt" "Clavicle_L_parentConstraint1.tg[0].trt";
connectAttr "FKXClavicle_L.r" "Clavicle_L_parentConstraint1.tg[0].tr";
connectAttr "FKXClavicle_L.ro" "Clavicle_L_parentConstraint1.tg[0].tro";
connectAttr "FKXClavicle_L.s" "Clavicle_L_parentConstraint1.tg[0].ts";
connectAttr "FKXClavicle_L.pm" "Clavicle_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXClavicle_L.jo" "Clavicle_L_parentConstraint1.tg[0].tjo";
connectAttr "Clavicle_L_parentConstraint1.w0" "Clavicle_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKShoulderPad1_L.s" "ShoulderPad1_L.s";
connectAttr "Chest_M.s" "ShoulderPad1_L.is";
connectAttr "ShoulderPad1_L_parentConstraint1.ctx" "ShoulderPad1_L.tx";
connectAttr "ShoulderPad1_L_parentConstraint1.cty" "ShoulderPad1_L.ty";
connectAttr "ShoulderPad1_L_parentConstraint1.ctz" "ShoulderPad1_L.tz";
connectAttr "ShoulderPad1_L_parentConstraint1.crx" "ShoulderPad1_L.rx";
connectAttr "ShoulderPad1_L_parentConstraint1.cry" "ShoulderPad1_L.ry";
connectAttr "ShoulderPad1_L_parentConstraint1.crz" "ShoulderPad1_L.rz";
connectAttr "jointLayer.di" "ShoulderPad1_L.do";
connectAttr "jointLayer.di" "ShoulderPad1_End_L.do";
connectAttr "ShoulderPad1_L.ro" "ShoulderPad1_L_parentConstraint1.cro";
connectAttr "ShoulderPad1_L.pim" "ShoulderPad1_L_parentConstraint1.cpim";
connectAttr "ShoulderPad1_L.rp" "ShoulderPad1_L_parentConstraint1.crp";
connectAttr "ShoulderPad1_L.rpt" "ShoulderPad1_L_parentConstraint1.crt";
connectAttr "ShoulderPad1_L.jo" "ShoulderPad1_L_parentConstraint1.cjo";
connectAttr "FKXShoulderPad1_L.t" "ShoulderPad1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulderPad1_L.rp" "ShoulderPad1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulderPad1_L.rpt" "ShoulderPad1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXShoulderPad1_L.r" "ShoulderPad1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulderPad1_L.ro" "ShoulderPad1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulderPad1_L.s" "ShoulderPad1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulderPad1_L.pm" "ShoulderPad1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulderPad1_L.jo" "ShoulderPad1_L_parentConstraint1.tg[0].tjo";
connectAttr "ShoulderPad1_L_parentConstraint1.w0" "ShoulderPad1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKShoulderPad2_L.s" "ShoulderPad2_L.s";
connectAttr "Chest_M.s" "ShoulderPad2_L.is";
connectAttr "ShoulderPad2_L_parentConstraint1.ctx" "ShoulderPad2_L.tx";
connectAttr "ShoulderPad2_L_parentConstraint1.cty" "ShoulderPad2_L.ty";
connectAttr "ShoulderPad2_L_parentConstraint1.ctz" "ShoulderPad2_L.tz";
connectAttr "ShoulderPad2_L_parentConstraint1.crx" "ShoulderPad2_L.rx";
connectAttr "ShoulderPad2_L_parentConstraint1.cry" "ShoulderPad2_L.ry";
connectAttr "ShoulderPad2_L_parentConstraint1.crz" "ShoulderPad2_L.rz";
connectAttr "jointLayer.di" "ShoulderPad2_L.do";
connectAttr "jointLayer.di" "ShoulderPad2_End_L.do";
connectAttr "ShoulderPad2_L.ro" "ShoulderPad2_L_parentConstraint1.cro";
connectAttr "ShoulderPad2_L.pim" "ShoulderPad2_L_parentConstraint1.cpim";
connectAttr "ShoulderPad2_L.rp" "ShoulderPad2_L_parentConstraint1.crp";
connectAttr "ShoulderPad2_L.rpt" "ShoulderPad2_L_parentConstraint1.crt";
connectAttr "ShoulderPad2_L.jo" "ShoulderPad2_L_parentConstraint1.cjo";
connectAttr "FKXShoulderPad2_L.t" "ShoulderPad2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulderPad2_L.rp" "ShoulderPad2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulderPad2_L.rpt" "ShoulderPad2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXShoulderPad2_L.r" "ShoulderPad2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulderPad2_L.ro" "ShoulderPad2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulderPad2_L.s" "ShoulderPad2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulderPad2_L.pm" "ShoulderPad2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulderPad2_L.jo" "ShoulderPad2_L_parentConstraint1.tg[0].tjo";
connectAttr "ShoulderPad2_L_parentConstraint1.w0" "ShoulderPad2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Chest_M.pim" "Chest_M_pointConstraint1.cpim";
connectAttr "Chest_M.rp" "Chest_M_pointConstraint1.crp";
connectAttr "Chest_M.rpt" "Chest_M_pointConstraint1.crt";
connectAttr "FKXChest_M.t" "Chest_M_pointConstraint1.tg[0].tt";
connectAttr "FKXChest_M.rp" "Chest_M_pointConstraint1.tg[0].trp";
connectAttr "FKXChest_M.rpt" "Chest_M_pointConstraint1.tg[0].trt";
connectAttr "FKXChest_M.pm" "Chest_M_pointConstraint1.tg[0].tpm";
connectAttr "Chest_M_pointConstraint1.w0" "Chest_M_pointConstraint1.tg[0].tw";
connectAttr "IKXChest_M.t" "Chest_M_pointConstraint1.tg[1].tt";
connectAttr "IKXChest_M.rp" "Chest_M_pointConstraint1.tg[1].trp";
connectAttr "IKXChest_M.rpt" "Chest_M_pointConstraint1.tg[1].trt";
connectAttr "IKXChest_M.pm" "Chest_M_pointConstraint1.tg[1].tpm";
connectAttr "Chest_M_pointConstraint1.w1" "Chest_M_pointConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "Chest_M_pointConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Chest_M_pointConstraint1.w1";
connectAttr "Chest_M.ro" "Chest_M_orientConstraint1.cro";
connectAttr "Chest_M.pim" "Chest_M_orientConstraint1.cpim";
connectAttr "Chest_M.jo" "Chest_M_orientConstraint1.cjo";
connectAttr "FKXChest_M.r" "Chest_M_orientConstraint1.tg[0].tr";
connectAttr "FKXChest_M.ro" "Chest_M_orientConstraint1.tg[0].tro";
connectAttr "FKXChest_M.pm" "Chest_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXChest_M.jo" "Chest_M_orientConstraint1.tg[0].tjo";
connectAttr "Chest_M_orientConstraint1.w0" "Chest_M_orientConstraint1.tg[0].tw";
connectAttr "IKXChest_M.r" "Chest_M_orientConstraint1.tg[1].tr";
connectAttr "IKXChest_M.ro" "Chest_M_orientConstraint1.tg[1].tro";
connectAttr "IKXChest_M.pm" "Chest_M_orientConstraint1.tg[1].tpm";
connectAttr "IKXChest_M.jo" "Chest_M_orientConstraint1.tg[1].tjo";
connectAttr "Chest_M_orientConstraint1.w1" "Chest_M_orientConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "Chest_M_orientConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Chest_M_orientConstraint1.w1";
connectAttr "SpineA_M.pim" "SpineA_M_pointConstraint1.cpim";
connectAttr "SpineA_M.rp" "SpineA_M_pointConstraint1.crp";
connectAttr "SpineA_M.rpt" "SpineA_M_pointConstraint1.crt";
connectAttr "FKXSpineA_M.t" "SpineA_M_pointConstraint1.tg[0].tt";
connectAttr "FKXSpineA_M.rp" "SpineA_M_pointConstraint1.tg[0].trp";
connectAttr "FKXSpineA_M.rpt" "SpineA_M_pointConstraint1.tg[0].trt";
connectAttr "FKXSpineA_M.pm" "SpineA_M_pointConstraint1.tg[0].tpm";
connectAttr "SpineA_M_pointConstraint1.w0" "SpineA_M_pointConstraint1.tg[0].tw";
connectAttr "IKXSpineA_M.t" "SpineA_M_pointConstraint1.tg[1].tt";
connectAttr "IKXSpineA_M.rp" "SpineA_M_pointConstraint1.tg[1].trp";
connectAttr "IKXSpineA_M.rpt" "SpineA_M_pointConstraint1.tg[1].trt";
connectAttr "IKXSpineA_M.pm" "SpineA_M_pointConstraint1.tg[1].tpm";
connectAttr "SpineA_M_pointConstraint1.w1" "SpineA_M_pointConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "SpineA_M_pointConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "SpineA_M_pointConstraint1.w1";
connectAttr "SpineA_M.ro" "SpineA_M_orientConstraint1.cro";
connectAttr "SpineA_M.pim" "SpineA_M_orientConstraint1.cpim";
connectAttr "SpineA_M.jo" "SpineA_M_orientConstraint1.cjo";
connectAttr "FKXSpineA_M.r" "SpineA_M_orientConstraint1.tg[0].tr";
connectAttr "FKXSpineA_M.ro" "SpineA_M_orientConstraint1.tg[0].tro";
connectAttr "FKXSpineA_M.pm" "SpineA_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXSpineA_M.jo" "SpineA_M_orientConstraint1.tg[0].tjo";
connectAttr "SpineA_M_orientConstraint1.w0" "SpineA_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXSpineA_M.r" "SpineA_M_orientConstraint1.tg[1].tr";
connectAttr "IKXSpineA_M.ro" "SpineA_M_orientConstraint1.tg[1].tro";
connectAttr "IKXSpineA_M.pm" "SpineA_M_orientConstraint1.tg[1].tpm";
connectAttr "IKXSpineA_M.jo" "SpineA_M_orientConstraint1.tg[1].tjo";
connectAttr "SpineA_M_orientConstraint1.w1" "SpineA_M_orientConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendSpineReverse_M.ox" "SpineA_M_orientConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "SpineA_M_orientConstraint1.w1";
connectAttr "HipTwist_R_parentConstraint1.ctx" "HipTwist_R.tx";
connectAttr "HipTwist_R_parentConstraint1.cty" "HipTwist_R.ty";
connectAttr "HipTwist_R_parentConstraint1.ctz" "HipTwist_R.tz";
connectAttr "HipTwist_R_parentConstraint1.crx" "HipTwist_R.rx";
connectAttr "HipTwist_R_parentConstraint1.cry" "HipTwist_R.ry";
connectAttr "HipTwist_R_parentConstraint1.crz" "HipTwist_R.rz";
connectAttr "FKHipTwist_R.s" "HipTwist_R.s";
connectAttr "Pelvis_M.s" "HipTwist_R.is";
connectAttr "jointLayer.di" "HipTwist_R.do";
connectAttr "ScaleBlendHip_R.op" "Hip_R.s";
connectAttr "Hip_R_parentConstraint1.ctx" "Hip_R.tx";
connectAttr "Hip_R_parentConstraint1.cty" "Hip_R.ty";
connectAttr "Hip_R_parentConstraint1.ctz" "Hip_R.tz";
connectAttr "Hip_R_parentConstraint1.crx" "Hip_R.rx";
connectAttr "Hip_R_parentConstraint1.cry" "Hip_R.ry";
connectAttr "Hip_R_parentConstraint1.crz" "Hip_R.rz";
connectAttr "jointLayer.di" "Hip_R.do";
connectAttr "ScaleBlendKnee_R.op" "Knee_R.s";
connectAttr "Hip_R.s" "Knee_R.is";
connectAttr "Knee_R_parentConstraint1.ctx" "Knee_R.tx";
connectAttr "Knee_R_parentConstraint1.cty" "Knee_R.ty";
connectAttr "Knee_R_parentConstraint1.ctz" "Knee_R.tz";
connectAttr "Knee_R_parentConstraint1.crx" "Knee_R.rx";
connectAttr "Knee_R_parentConstraint1.cry" "Knee_R.ry";
connectAttr "Knee_R_parentConstraint1.crz" "Knee_R.rz";
connectAttr "jointLayer.di" "Knee_R.do";
connectAttr "ScaleBlendAnkle_R.op" "Ankle_R.s";
connectAttr "Knee_R.s" "Ankle_R.is";
connectAttr "Ankle_R_parentConstraint1.ctx" "Ankle_R.tx";
connectAttr "Ankle_R_parentConstraint1.cty" "Ankle_R.ty";
connectAttr "Ankle_R_parentConstraint1.ctz" "Ankle_R.tz";
connectAttr "Ankle_R_parentConstraint1.crx" "Ankle_R.rx";
connectAttr "Ankle_R_parentConstraint1.cry" "Ankle_R.ry";
connectAttr "Ankle_R_parentConstraint1.crz" "Ankle_R.rz";
connectAttr "jointLayer.di" "Ankle_R.do";
connectAttr "Ankle_R.s" "Foot_End_R.is";
connectAttr "jointLayer.di" "Foot_End_R.do";
connectAttr "Ankle_R.ro" "Ankle_R_parentConstraint1.cro";
connectAttr "Ankle_R.pim" "Ankle_R_parentConstraint1.cpim";
connectAttr "Ankle_R.rp" "Ankle_R_parentConstraint1.crp";
connectAttr "Ankle_R.rpt" "Ankle_R_parentConstraint1.crt";
connectAttr "Ankle_R.jo" "Ankle_R_parentConstraint1.cjo";
connectAttr "FKXAnkle_R.t" "Ankle_R_parentConstraint1.tg[0].tt";
connectAttr "FKXAnkle_R.rp" "Ankle_R_parentConstraint1.tg[0].trp";
connectAttr "FKXAnkle_R.rpt" "Ankle_R_parentConstraint1.tg[0].trt";
connectAttr "FKXAnkle_R.r" "Ankle_R_parentConstraint1.tg[0].tr";
connectAttr "FKXAnkle_R.ro" "Ankle_R_parentConstraint1.tg[0].tro";
connectAttr "FKXAnkle_R.s" "Ankle_R_parentConstraint1.tg[0].ts";
connectAttr "FKXAnkle_R.pm" "Ankle_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXAnkle_R.jo" "Ankle_R_parentConstraint1.tg[0].tjo";
connectAttr "Ankle_R_parentConstraint1.w0" "Ankle_R_parentConstraint1.tg[0].tw";
connectAttr "IKXAnkle_R.t" "Ankle_R_parentConstraint1.tg[1].tt";
connectAttr "IKXAnkle_R.rp" "Ankle_R_parentConstraint1.tg[1].trp";
connectAttr "IKXAnkle_R.rpt" "Ankle_R_parentConstraint1.tg[1].trt";
connectAttr "IKXAnkle_R.r" "Ankle_R_parentConstraint1.tg[1].tr";
connectAttr "IKXAnkle_R.ro" "Ankle_R_parentConstraint1.tg[1].tro";
connectAttr "IKXAnkle_R.s" "Ankle_R_parentConstraint1.tg[1].ts";
connectAttr "IKXAnkle_R.pm" "Ankle_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXAnkle_R.jo" "Ankle_R_parentConstraint1.tg[1].tjo";
connectAttr "Ankle_R_parentConstraint1.w1" "Ankle_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Ankle_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Ankle_R_parentConstraint1.w1";
connectAttr "Knee_R.ro" "Knee_R_parentConstraint1.cro";
connectAttr "Knee_R.pim" "Knee_R_parentConstraint1.cpim";
connectAttr "Knee_R.rp" "Knee_R_parentConstraint1.crp";
connectAttr "Knee_R.rpt" "Knee_R_parentConstraint1.crt";
connectAttr "Knee_R.jo" "Knee_R_parentConstraint1.cjo";
connectAttr "FKXKnee_R.t" "Knee_R_parentConstraint1.tg[0].tt";
connectAttr "FKXKnee_R.rp" "Knee_R_parentConstraint1.tg[0].trp";
connectAttr "FKXKnee_R.rpt" "Knee_R_parentConstraint1.tg[0].trt";
connectAttr "FKXKnee_R.r" "Knee_R_parentConstraint1.tg[0].tr";
connectAttr "FKXKnee_R.ro" "Knee_R_parentConstraint1.tg[0].tro";
connectAttr "FKXKnee_R.s" "Knee_R_parentConstraint1.tg[0].ts";
connectAttr "FKXKnee_R.pm" "Knee_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXKnee_R.jo" "Knee_R_parentConstraint1.tg[0].tjo";
connectAttr "Knee_R_parentConstraint1.w0" "Knee_R_parentConstraint1.tg[0].tw";
connectAttr "IKXKnee_R.t" "Knee_R_parentConstraint1.tg[1].tt";
connectAttr "IKXKnee_R.rp" "Knee_R_parentConstraint1.tg[1].trp";
connectAttr "IKXKnee_R.rpt" "Knee_R_parentConstraint1.tg[1].trt";
connectAttr "IKXKnee_R.r" "Knee_R_parentConstraint1.tg[1].tr";
connectAttr "IKXKnee_R.ro" "Knee_R_parentConstraint1.tg[1].tro";
connectAttr "IKXKnee_R.s" "Knee_R_parentConstraint1.tg[1].ts";
connectAttr "IKXKnee_R.pm" "Knee_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXKnee_R.jo" "Knee_R_parentConstraint1.tg[1].tjo";
connectAttr "Knee_R_parentConstraint1.w1" "Knee_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Knee_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Knee_R_parentConstraint1.w1";
connectAttr "Hip_R.ro" "Hip_R_parentConstraint1.cro";
connectAttr "Hip_R.pim" "Hip_R_parentConstraint1.cpim";
connectAttr "Hip_R.rp" "Hip_R_parentConstraint1.crp";
connectAttr "Hip_R.rpt" "Hip_R_parentConstraint1.crt";
connectAttr "Hip_R.jo" "Hip_R_parentConstraint1.cjo";
connectAttr "FKXHip_R.t" "Hip_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHip_R.rp" "Hip_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHip_R.rpt" "Hip_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHip_R.r" "Hip_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHip_R.ro" "Hip_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHip_R.s" "Hip_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHip_R.pm" "Hip_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHip_R.jo" "Hip_R_parentConstraint1.tg[0].tjo";
connectAttr "Hip_R_parentConstraint1.w0" "Hip_R_parentConstraint1.tg[0].tw";
connectAttr "IKXHip_R.t" "Hip_R_parentConstraint1.tg[1].tt";
connectAttr "IKXHip_R.rp" "Hip_R_parentConstraint1.tg[1].trp";
connectAttr "IKXHip_R.rpt" "Hip_R_parentConstraint1.tg[1].trt";
connectAttr "IKXHip_R.r" "Hip_R_parentConstraint1.tg[1].tr";
connectAttr "IKXHip_R.ro" "Hip_R_parentConstraint1.tg[1].tro";
connectAttr "IKXHip_R.s" "Hip_R_parentConstraint1.tg[1].ts";
connectAttr "IKXHip_R.pm" "Hip_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXHip_R.jo" "Hip_R_parentConstraint1.tg[1].tjo";
connectAttr "Hip_R_parentConstraint1.w1" "Hip_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Hip_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Hip_R_parentConstraint1.w1";
connectAttr "HipTwist_R.ro" "HipTwist_R_parentConstraint1.cro";
connectAttr "HipTwist_R.pim" "HipTwist_R_parentConstraint1.cpim";
connectAttr "HipTwist_R.rp" "HipTwist_R_parentConstraint1.crp";
connectAttr "HipTwist_R.rpt" "HipTwist_R_parentConstraint1.crt";
connectAttr "HipTwist_R.jo" "HipTwist_R_parentConstraint1.cjo";
connectAttr "FKXHipTwist_R.t" "HipTwist_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHipTwist_R.rp" "HipTwist_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHipTwist_R.rpt" "HipTwist_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHipTwist_R.r" "HipTwist_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHipTwist_R.ro" "HipTwist_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHipTwist_R.s" "HipTwist_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHipTwist_R.pm" "HipTwist_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHipTwist_R.jo" "HipTwist_R_parentConstraint1.tg[0].tjo";
connectAttr "HipTwist_R_parentConstraint1.w0" "HipTwist_R_parentConstraint1.tg[0].tw"
		;
connectAttr "HipTwist_L_parentConstraint1.ctx" "HipTwist_L.tx";
connectAttr "HipTwist_L_parentConstraint1.cty" "HipTwist_L.ty";
connectAttr "HipTwist_L_parentConstraint1.ctz" "HipTwist_L.tz";
connectAttr "HipTwist_L_parentConstraint1.crx" "HipTwist_L.rx";
connectAttr "HipTwist_L_parentConstraint1.cry" "HipTwist_L.ry";
connectAttr "HipTwist_L_parentConstraint1.crz" "HipTwist_L.rz";
connectAttr "FKHipTwist_L.s" "HipTwist_L.s";
connectAttr "Pelvis_M.s" "HipTwist_L.is";
connectAttr "jointLayer.di" "HipTwist_L.do";
connectAttr "ScaleBlendHip_L.op" "Hip_L.s";
connectAttr "Hip_L_parentConstraint1.ctx" "Hip_L.tx";
connectAttr "Hip_L_parentConstraint1.cty" "Hip_L.ty";
connectAttr "Hip_L_parentConstraint1.ctz" "Hip_L.tz";
connectAttr "Hip_L_parentConstraint1.crx" "Hip_L.rx";
connectAttr "Hip_L_parentConstraint1.cry" "Hip_L.ry";
connectAttr "Hip_L_parentConstraint1.crz" "Hip_L.rz";
connectAttr "jointLayer.di" "Hip_L.do";
connectAttr "ScaleBlendKnee_L.op" "Knee_L.s";
connectAttr "Hip_L.s" "Knee_L.is";
connectAttr "Knee_L_parentConstraint1.ctx" "Knee_L.tx";
connectAttr "Knee_L_parentConstraint1.cty" "Knee_L.ty";
connectAttr "Knee_L_parentConstraint1.ctz" "Knee_L.tz";
connectAttr "Knee_L_parentConstraint1.crx" "Knee_L.rx";
connectAttr "Knee_L_parentConstraint1.cry" "Knee_L.ry";
connectAttr "Knee_L_parentConstraint1.crz" "Knee_L.rz";
connectAttr "jointLayer.di" "Knee_L.do";
connectAttr "ScaleBlendAnkle_L.op" "Ankle_L.s";
connectAttr "Knee_L.s" "Ankle_L.is";
connectAttr "Ankle_L_parentConstraint1.ctx" "Ankle_L.tx";
connectAttr "Ankle_L_parentConstraint1.cty" "Ankle_L.ty";
connectAttr "Ankle_L_parentConstraint1.ctz" "Ankle_L.tz";
connectAttr "Ankle_L_parentConstraint1.crx" "Ankle_L.rx";
connectAttr "Ankle_L_parentConstraint1.cry" "Ankle_L.ry";
connectAttr "Ankle_L_parentConstraint1.crz" "Ankle_L.rz";
connectAttr "jointLayer.di" "Ankle_L.do";
connectAttr "Ankle_L.s" "Foot_End_L.is";
connectAttr "jointLayer.di" "Foot_End_L.do";
connectAttr "Ankle_L.ro" "Ankle_L_parentConstraint1.cro";
connectAttr "Ankle_L.pim" "Ankle_L_parentConstraint1.cpim";
connectAttr "Ankle_L.rp" "Ankle_L_parentConstraint1.crp";
connectAttr "Ankle_L.rpt" "Ankle_L_parentConstraint1.crt";
connectAttr "Ankle_L.jo" "Ankle_L_parentConstraint1.cjo";
connectAttr "FKXAnkle_L.t" "Ankle_L_parentConstraint1.tg[0].tt";
connectAttr "FKXAnkle_L.rp" "Ankle_L_parentConstraint1.tg[0].trp";
connectAttr "FKXAnkle_L.rpt" "Ankle_L_parentConstraint1.tg[0].trt";
connectAttr "FKXAnkle_L.r" "Ankle_L_parentConstraint1.tg[0].tr";
connectAttr "FKXAnkle_L.ro" "Ankle_L_parentConstraint1.tg[0].tro";
connectAttr "FKXAnkle_L.s" "Ankle_L_parentConstraint1.tg[0].ts";
connectAttr "FKXAnkle_L.pm" "Ankle_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXAnkle_L.jo" "Ankle_L_parentConstraint1.tg[0].tjo";
connectAttr "Ankle_L_parentConstraint1.w0" "Ankle_L_parentConstraint1.tg[0].tw";
connectAttr "IKXAnkle_L.t" "Ankle_L_parentConstraint1.tg[1].tt";
connectAttr "IKXAnkle_L.rp" "Ankle_L_parentConstraint1.tg[1].trp";
connectAttr "IKXAnkle_L.rpt" "Ankle_L_parentConstraint1.tg[1].trt";
connectAttr "IKXAnkle_L.r" "Ankle_L_parentConstraint1.tg[1].tr";
connectAttr "IKXAnkle_L.ro" "Ankle_L_parentConstraint1.tg[1].tro";
connectAttr "IKXAnkle_L.s" "Ankle_L_parentConstraint1.tg[1].ts";
connectAttr "IKXAnkle_L.pm" "Ankle_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXAnkle_L.jo" "Ankle_L_parentConstraint1.tg[1].tjo";
connectAttr "Ankle_L_parentConstraint1.w1" "Ankle_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Ankle_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Ankle_L_parentConstraint1.w1";
connectAttr "Knee_L.ro" "Knee_L_parentConstraint1.cro";
connectAttr "Knee_L.pim" "Knee_L_parentConstraint1.cpim";
connectAttr "Knee_L.rp" "Knee_L_parentConstraint1.crp";
connectAttr "Knee_L.rpt" "Knee_L_parentConstraint1.crt";
connectAttr "Knee_L.jo" "Knee_L_parentConstraint1.cjo";
connectAttr "FKXKnee_L.t" "Knee_L_parentConstraint1.tg[0].tt";
connectAttr "FKXKnee_L.rp" "Knee_L_parentConstraint1.tg[0].trp";
connectAttr "FKXKnee_L.rpt" "Knee_L_parentConstraint1.tg[0].trt";
connectAttr "FKXKnee_L.r" "Knee_L_parentConstraint1.tg[0].tr";
connectAttr "FKXKnee_L.ro" "Knee_L_parentConstraint1.tg[0].tro";
connectAttr "FKXKnee_L.s" "Knee_L_parentConstraint1.tg[0].ts";
connectAttr "FKXKnee_L.pm" "Knee_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXKnee_L.jo" "Knee_L_parentConstraint1.tg[0].tjo";
connectAttr "Knee_L_parentConstraint1.w0" "Knee_L_parentConstraint1.tg[0].tw";
connectAttr "IKXKnee_L.t" "Knee_L_parentConstraint1.tg[1].tt";
connectAttr "IKXKnee_L.rp" "Knee_L_parentConstraint1.tg[1].trp";
connectAttr "IKXKnee_L.rpt" "Knee_L_parentConstraint1.tg[1].trt";
connectAttr "IKXKnee_L.r" "Knee_L_parentConstraint1.tg[1].tr";
connectAttr "IKXKnee_L.ro" "Knee_L_parentConstraint1.tg[1].tro";
connectAttr "IKXKnee_L.s" "Knee_L_parentConstraint1.tg[1].ts";
connectAttr "IKXKnee_L.pm" "Knee_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXKnee_L.jo" "Knee_L_parentConstraint1.tg[1].tjo";
connectAttr "Knee_L_parentConstraint1.w1" "Knee_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Knee_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Knee_L_parentConstraint1.w1";
connectAttr "Hip_L.ro" "Hip_L_parentConstraint1.cro";
connectAttr "Hip_L.pim" "Hip_L_parentConstraint1.cpim";
connectAttr "Hip_L.rp" "Hip_L_parentConstraint1.crp";
connectAttr "Hip_L.rpt" "Hip_L_parentConstraint1.crt";
connectAttr "Hip_L.jo" "Hip_L_parentConstraint1.cjo";
connectAttr "FKXHip_L.t" "Hip_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHip_L.rp" "Hip_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHip_L.rpt" "Hip_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHip_L.r" "Hip_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHip_L.ro" "Hip_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHip_L.s" "Hip_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHip_L.pm" "Hip_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHip_L.jo" "Hip_L_parentConstraint1.tg[0].tjo";
connectAttr "Hip_L_parentConstraint1.w0" "Hip_L_parentConstraint1.tg[0].tw";
connectAttr "IKXHip_L.t" "Hip_L_parentConstraint1.tg[1].tt";
connectAttr "IKXHip_L.rp" "Hip_L_parentConstraint1.tg[1].trp";
connectAttr "IKXHip_L.rpt" "Hip_L_parentConstraint1.tg[1].trt";
connectAttr "IKXHip_L.r" "Hip_L_parentConstraint1.tg[1].tr";
connectAttr "IKXHip_L.ro" "Hip_L_parentConstraint1.tg[1].tro";
connectAttr "IKXHip_L.s" "Hip_L_parentConstraint1.tg[1].ts";
connectAttr "IKXHip_L.pm" "Hip_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXHip_L.jo" "Hip_L_parentConstraint1.tg[1].tjo";
connectAttr "Hip_L_parentConstraint1.w1" "Hip_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Hip_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Hip_L_parentConstraint1.w1";
connectAttr "HipTwist_L.ro" "HipTwist_L_parentConstraint1.cro";
connectAttr "HipTwist_L.pim" "HipTwist_L_parentConstraint1.cpim";
connectAttr "HipTwist_L.rp" "HipTwist_L_parentConstraint1.crp";
connectAttr "HipTwist_L.rpt" "HipTwist_L_parentConstraint1.crt";
connectAttr "HipTwist_L.jo" "HipTwist_L_parentConstraint1.cjo";
connectAttr "FKXHipTwist_L.t" "HipTwist_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHipTwist_L.rp" "HipTwist_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHipTwist_L.rpt" "HipTwist_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHipTwist_L.r" "HipTwist_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHipTwist_L.ro" "HipTwist_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHipTwist_L.s" "HipTwist_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHipTwist_L.pm" "HipTwist_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHipTwist_L.jo" "HipTwist_L_parentConstraint1.tg[0].tjo";
connectAttr "HipTwist_L_parentConstraint1.w0" "HipTwist_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Pelvis_M.pim" "Pelvis_M_pointConstraint1.cpim";
connectAttr "Pelvis_M.rp" "Pelvis_M_pointConstraint1.crp";
connectAttr "Pelvis_M.rpt" "Pelvis_M_pointConstraint1.crt";
connectAttr "FKXPelvis_M.t" "Pelvis_M_pointConstraint1.tg[0].tt";
connectAttr "FKXPelvis_M.rp" "Pelvis_M_pointConstraint1.tg[0].trp";
connectAttr "FKXPelvis_M.rpt" "Pelvis_M_pointConstraint1.tg[0].trt";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_pointConstraint1.tg[0].tpm";
connectAttr "Pelvis_M_pointConstraint1.w0" "Pelvis_M_pointConstraint1.tg[0].tw";
connectAttr "IKXPelvis_M.t" "Pelvis_M_pointConstraint1.tg[1].tt";
connectAttr "IKXPelvis_M.rp" "Pelvis_M_pointConstraint1.tg[1].trp";
connectAttr "IKXPelvis_M.rpt" "Pelvis_M_pointConstraint1.tg[1].trt";
connectAttr "IKXPelvis_M.pm" "Pelvis_M_pointConstraint1.tg[1].tpm";
connectAttr "Pelvis_M_pointConstraint1.w1" "Pelvis_M_pointConstraint1.tg[1].tw";
connectAttr "FKIKBlendSpineReverse_M.ox" "Pelvis_M_pointConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Pelvis_M_pointConstraint1.w1";
connectAttr "Pelvis_M.ro" "Pelvis_M_orientConstraint1.cro";
connectAttr "Pelvis_M.pim" "Pelvis_M_orientConstraint1.cpim";
connectAttr "Pelvis_M.jo" "Pelvis_M_orientConstraint1.cjo";
connectAttr "FKXPelvis_M.r" "Pelvis_M_orientConstraint1.tg[0].tr";
connectAttr "FKXPelvis_M.ro" "Pelvis_M_orientConstraint1.tg[0].tro";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXPelvis_M.jo" "Pelvis_M_orientConstraint1.tg[0].tjo";
connectAttr "Pelvis_M_orientConstraint1.w0" "Pelvis_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKOrientToSpine_M.r" "Pelvis_M_orientConstraint1.tg[1].tr";
connectAttr "IKOrientToSpine_M.ro" "Pelvis_M_orientConstraint1.tg[1].tro";
connectAttr "IKOrientToSpine_M.pm" "Pelvis_M_orientConstraint1.tg[1].tpm";
connectAttr "Pelvis_M_orientConstraint1.w1" "Pelvis_M_orientConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendSpineReverse_M.ox" "Pelvis_M_orientConstraint1.w0";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "Pelvis_M_orientConstraint1.w1";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "irisPuppetSG.msg" "materialInfo3.sg";
connectAttr "BPRig_irisPuppetSG.msg" "BPRig_materialInfo3.sg";
connectAttr "jointVisReverse.ox" "jointLayer.lod";
connectAttr "Main.jointVis" "jointVisReverse.ix";
connectAttr "Main.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWrist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWrist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow_R.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder_R.iog" "ControlSet.dsm" -na;
connectAttr "FKClavicle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraClavicle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulderPad1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulderPad1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulderPad2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulderPad2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKChest_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraChest_M.iog" "ControlSet.dsm" -na;
connectAttr "FKSpineA_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraSpineA_M.iog" "ControlSet.dsm" -na;
connectAttr "FKAnkle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraAnkle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHip_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHip_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHipTwist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHipTwist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWrist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWrist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder_L.iog" "ControlSet.dsm" -na;
connectAttr "FKClavicle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraClavicle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulderPad1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulderPad1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulderPad2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulderPad2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKAnkle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraAnkle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHip_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHip_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHipTwist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHipTwist_L.iog" "ControlSet.dsm" -na;
connectAttr "HipSwingerPelvis_M.iog" "ControlSet.dsm" -na;
connectAttr "IKArm_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraArm_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleArm_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraArm_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIKArm_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKSpine0_M.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraSpine0_M.iog" "ControlSet.dsm" -na;
connectAttr "IKSpine2_M.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraSpine2_M.iog" "ControlSet.dsm" -na;
connectAttr "IKSpine4_M.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraSpine4_M.iog" "ControlSet.dsm" -na;
connectAttr "FKIKSpine_M.iog" "ControlSet.dsm" -na;
connectAttr "IKArm_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraArm_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleArm_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraArm_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIKArm_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "ControlSet.dsm" -na;
connectAttr "Center_M.iog" "ControlSet.dsm" -na;
connectAttr "MiddleFinger2_R.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger1_R.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger2_R.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger1_R.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger2_R.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger1_R.iog" "GameSet.dsm" -na;
connectAttr "Wrist_R.iog" "GameSet.dsm" -na;
connectAttr "Elbow_R.iog" "GameSet.dsm" -na;
connectAttr "Shoulder_R.iog" "GameSet.dsm" -na;
connectAttr "Clavicle_R.iog" "GameSet.dsm" -na;
connectAttr "ShoulderPad1_R.iog" "GameSet.dsm" -na;
connectAttr "ShoulderPad2_R.iog" "GameSet.dsm" -na;
connectAttr "Chest_M.iog" "GameSet.dsm" -na;
connectAttr "SpineA_M.iog" "GameSet.dsm" -na;
connectAttr "Ankle_R.iog" "GameSet.dsm" -na;
connectAttr "Knee_R.iog" "GameSet.dsm" -na;
connectAttr "Hip_R.iog" "GameSet.dsm" -na;
connectAttr "HipTwist_R.iog" "GameSet.dsm" -na;
connectAttr "Pelvis_M.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "Wrist_L.iog" "GameSet.dsm" -na;
connectAttr "Elbow_L.iog" "GameSet.dsm" -na;
connectAttr "Shoulder_L.iog" "GameSet.dsm" -na;
connectAttr "Clavicle_L.iog" "GameSet.dsm" -na;
connectAttr "ShoulderPad1_L.iog" "GameSet.dsm" -na;
connectAttr "ShoulderPad2_L.iog" "GameSet.dsm" -na;
connectAttr "Ankle_L.iog" "GameSet.dsm" -na;
connectAttr "Knee_L.iog" "GameSet.dsm" -na;
connectAttr "Hip_L.iog" "GameSet.dsm" -na;
connectAttr "HipTwist_L.iog" "GameSet.dsm" -na;
connectAttr "buildPose.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "CenterBtwFeet_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendHip_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendKnee_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAnkle_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendShoulder_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendElbow_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendWrist_L.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion6.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion5.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "Leg_LAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion4.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmsetRange_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmCondition_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "PoleArm_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "IKArm_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendPelvis_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendHip_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendKnee_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAnkle_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendSpineA_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendChest_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendShoulder_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendElbow_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendWrist_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpinesetRange_M.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpineCondition_M.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpineReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendSpineUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "IKTwistSpineUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo3_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide3_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo2_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide2_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo1_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide1_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineBlendTwo0_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineMultiplyDivide0_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "stretchySpineUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "IKCurveInfoAllMultiplySpine_M.msg" "AllSet.dnsm" -na;
connectAttr "IKCurveInfoNormalizeSpine_M.msg" "AllSet.dnsm" -na;
connectAttr "IKCurveInfoSpine_M.msg" "AllSet.dnsm" -na;
connectAttr "IKStiffSpine4_M.msg" "AllSet.dnsm" -na;
connectAttr "IKStiffSpine2_M.msg" "AllSet.dnsm" -na;
connectAttr "IKStiffSpine0_M.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion3.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion2.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "Leg_RAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion1.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmsetRange_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmCondition_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmReverse_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendArmUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "PoleArm_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "IKArm_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "GameSet.msg" "AllSet.dnsm" -na;
connectAttr "ControlSet.msg" "AllSet.dnsm" -na;
connectAttr "jointVisReverse.msg" "AllSet.dnsm" -na;
connectAttr "jointLayer.msg" "AllSet.dnsm" -na;
connectAttr "IKParentConstraintSpine2_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine4_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine0_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintPelvis_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerStabalizePelvis_M_orientConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsOffset_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenter_M.iog" "AllSet.dsm" -na;
connectAttr "Center_MShape.iog" "AllSet.dsm" -na;
connectAttr "Center_M.iog" "AllSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "AllSet.dsm" -na;
connectAttr "CenterOffset_M.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Knee_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Ankle_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Clavicle_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wrist_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToWrist_L_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "MiddleFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_L.iog" "AllSet.dsm" -na;
connectAttr "effector8.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_LStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L.iog" "AllSet.dsm" -na;
connectAttr "effector7.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintArm_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintArm_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_LStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKXArmHandle_L_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraArm_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_LStatic.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKArm_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_L.iog" "AllSet.dsm" -na;
connectAttr "IKXArmHandle_L.iog" "AllSet.dsm" -na;
connectAttr "effector6.iog" "AllSet.dsm" -na;
connectAttr "IKOrientToSpine_M.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Knee_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Ankle_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToPelvis_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "SpineA_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "SpineA_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Chest_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Chest_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToChest_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "Clavicle_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wrist_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToWrist_R_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "MiddleFinger1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintSpine_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "FKIKParentConstraintSpine_M.iog" "AllSet.dsm" -na;
connectAttr "FKIKSpine_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKSpine_M.iog" "AllSet.dsm" -na;
connectAttr "UnTwistEndPelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "UnTwistIKPelvis_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "UnTwistIKPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "effector5.iog" "AllSet.dsm" -na;
connectAttr "UnTwistEndPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "UnTwistPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "TwistFollowPelvis_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TwistFollowPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKXChest_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKSpine0AlignTo_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4AlignUnTwistTo_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4AlignUnTwistToOffset_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4AlignTo_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineA_M_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFake1UpLocSpine_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKFake1UpLocSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKEndJointOrientToSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKStiffEndOrientSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKStiffStartOrientSpine_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKSpine4_M.iog" "AllSet.dsm" -na;
connectAttr "IKExtraSpine4_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine4_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator4_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator4_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator3_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator3_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine2_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKSpine2_M.iog" "AllSet.dsm" -na;
connectAttr "IKExtraSpine2_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine2_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator2_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator2_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator1_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator1_M.iog" "AllSet.dsm" -na;
connectAttr "IKSpine0_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKSpine0_M.iog" "AllSet.dsm" -na;
connectAttr "IKExtraSpine0_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintSpine0_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator0_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineLocator0_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineCurve_MShape.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineCurve_M.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineHandle_M.iog" "AllSet.dsm" -na;
connectAttr "effector4.iog" "AllSet.dsm" -na;
connectAttr "IKfake2Spine_M.iog" "AllSet.dsm" -na;
connectAttr "IKfake1Spine_M.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_R.iog" "AllSet.dsm" -na;
connectAttr "effector3.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_RStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R.iog" "AllSet.dsm" -na;
connectAttr "effector2.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintArm_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintArm_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKArm_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKArm_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_RStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationArm_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationArm_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetArm_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetArm_R.iog" "AllSet.dsm" -na;
connectAttr "IKXArmHandle_R_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleArm_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleArm_R.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraArm_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintArm_R.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_RStatic.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedArm_R.iog" "AllSet.dsm" -na;
connectAttr "IKArm_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKArm_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraArm_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintArm_R.iog" "AllSet.dsm" -na;
connectAttr "IKXArmHandle_R.iog" "AllSet.dsm" -na;
connectAttr "effector1.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerStabalizePelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerGroupPelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerGroupPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerPelvis_MShape.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipSwingerOffsetPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintShoulder_L_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "FKXHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "IKXHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKHip_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKXKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKXKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetKnee_L.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKXAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHeel_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKXFoot_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXFoot_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulderPad2_L.iog" "AllSet.dsm" -na;
connectAttr "FKShoulderPad2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulderPad2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulderPad2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulderPad2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulderPad2_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulderPad1_L.iog" "AllSet.dsm" -na;
connectAttr "FKShoulderPad1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulderPad1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulderPad1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulderPad1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulderPad1_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXClavicle_L.iog" "AllSet.dsm" -na;
connectAttr "FKClavicle_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKClavicle_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraClavicle_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetClavicle_L.iog" "AllSet.dsm" -na;
connectAttr "IKXShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "IKXElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow_L.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKWrist_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToWrist_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "Hip_L.iog" "AllSet.dsm" -na;
connectAttr "Knee_L.iog" "AllSet.dsm" -na;
connectAttr "Ankle_L.iog" "AllSet.dsm" -na;
connectAttr "Foot_End_L.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad2_L.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad2_End_L.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad1_L.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad1_End_L.iog" "AllSet.dsm" -na;
connectAttr "Clavicle_L.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_L.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L.iog" "AllSet.dsm" -na;
connectAttr "Wrist_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintShoulder_R_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "IKXPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKXPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKXHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKHip_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKXKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKXKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetKnee_R.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKXAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHeel_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXFoot_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXFoot_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKXSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKSpineA_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetSpineA_M.iog" "AllSet.dsm" -na;
connectAttr "IKXChest_M.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKXChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKChest_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetChest_M.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulderPad2_R.iog" "AllSet.dsm" -na;
connectAttr "FKShoulderPad2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulderPad2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulderPad2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulderPad2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulderPad2_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulderPad1_R.iog" "AllSet.dsm" -na;
connectAttr "FKShoulderPad1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulderPad1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulderPad1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulderPad1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulderPad1_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXChest_End_M.iog" "AllSet.dsm" -na;
connectAttr "FKXClavicle_R.iog" "AllSet.dsm" -na;
connectAttr "FKClavicle_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKClavicle_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraClavicle_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetClavicle_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToChest_M.iog" "AllSet.dsm" -na;
connectAttr "IKXShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "IKXElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow_R.iog" "AllSet.dsm" -na;
connectAttr "IKXWrist_R.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToWrist_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWrist_R.iog" "AllSet.dsm" -na;
connectAttr "FKWrist_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWrist_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWrist_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWrist_R.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToWrist_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "Hip_R.iog" "AllSet.dsm" -na;
connectAttr "Knee_R.iog" "AllSet.dsm" -na;
connectAttr "Ankle_R.iog" "AllSet.dsm" -na;
connectAttr "Foot_End_R.iog" "AllSet.dsm" -na;
connectAttr "SpineA_M.iog" "AllSet.dsm" -na;
connectAttr "Chest_M.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad2_R.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad2_End_R.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad1_R.iog" "AllSet.dsm" -na;
connectAttr "ShoulderPad1_End_R.iog" "AllSet.dsm" -na;
connectAttr "Chest_End_M.iog" "AllSet.dsm" -na;
connectAttr "Clavicle_R.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_R.iog" "AllSet.dsm" -na;
connectAttr "Elbow_R.iog" "AllSet.dsm" -na;
connectAttr "Wrist_R.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKMessure.iog" "AllSet.dsm" -na;
connectAttr "IKCrv.iog" "AllSet.dsm" -na;
connectAttr "IKStatic.iog" "AllSet.dsm" -na;
connectAttr "IKHandle.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraint.iog" "AllSet.dsm" -na;
connectAttr "TwistSystem.iog" "AllSet.dsm" -na;
connectAttr "RootSystem.iog" "AllSet.dsm" -na;
connectAttr "FKIKSystem.iog" "AllSet.dsm" -na;
connectAttr "IKSystem.iog" "AllSet.dsm" -na;
connectAttr "FKSystem.iog" "AllSet.dsm" -na;
connectAttr "GameSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MotionSystem.iog" "AllSet.dsm" -na;
connectAttr "BaseSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MainShape.iog" "AllSet.dsm" -na;
connectAttr "Main.iog" "AllSet.dsm" -na;
connectAttr "forwardArrowShape.iog" "AllSet.dsm" -na;
connectAttr "GameSet.msg" "Sets.dnsm" -na;
connectAttr "ControlSet.msg" "Sets.dnsm" -na;
connectAttr "AllSet.msg" "Sets.dnsm" -na;
connectAttr "IKArm_R.follow" "IKArm_RSetRangeFollow.vx";
connectAttr "IKArm_R.follow" "IKArm_RSetRangeFollow.vy";
connectAttr "PoleArm_R.follow" "PoleArm_RSetRangeFollow.vx";
connectAttr "PoleArm_R.follow" "PoleArm_RSetRangeFollow.vy";
connectAttr "FKIKArm_R.FKIKBlend" "FKIKBlendArmUnitConversion_R.i";
connectAttr "FKIKBlendArmUnitConversion_R.o" "FKIKBlendArmReverse_R.ix";
connectAttr "FKIKArm_R.autoVis" "FKIKBlendArmCondition_R.ft";
connectAttr "FKIKArm_R.IKVis" "FKIKBlendArmCondition_R.ctr";
connectAttr "FKIKArm_R.FKVis" "FKIKBlendArmCondition_R.ctg";
connectAttr "FKIKArm_R.FKIKBlend" "FKIKBlendArmCondition_R.cfr";
connectAttr "FKIKBlendArmsetRange_R.ox" "FKIKBlendArmCondition_R.cfg";
connectAttr "FKIKArm_R.FKIKBlend" "FKIKBlendArmsetRange_R.vx";
connectAttr "IKLeg_R.swivel" "unitConversion1.i";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vx";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vy";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegUnitConversion_R.i";
connectAttr "FKIKBlendLegUnitConversion_R.o" "FKIKBlendLegReverse_R.ix";
connectAttr "FKIKLeg_R.autoVis" "FKIKBlendLegCondition_R.ft";
connectAttr "FKIKLeg_R.IKVis" "FKIKBlendLegCondition_R.ctr";
connectAttr "FKIKLeg_R.FKVis" "FKIKBlendLegCondition_R.ctg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegCondition_R.cfr";
connectAttr "FKIKBlendLegsetRange_R.ox" "FKIKBlendLegCondition_R.cfg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegsetRange_R.vx";
connectAttr "IKLeg_R.rollAngle" "Leg_RAngleReverse.i1x";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vx";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vy";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vz";
connectAttr "Leg_RAngleReverse.ox" "IKRollAngleLeg_R.nx";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.my";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.mz";
connectAttr "IKRollAngleLeg_R.ox" "unitConversion2.i";
connectAttr "IKRollAngleLeg_R.oy" "unitConversion3.i";
connectAttr "IKSpine0_M.stiff" "IKStiffSpine0_M.vy";
connectAttr "IKSpine2_M.stiff" "IKStiffSpine2_M.vy";
connectAttr "IKSpine4_M.stiff" "IKStiffSpine4_M.vy";
connectAttr "IKXSpineCurve_MShape.ws" "IKCurveInfoSpine_M.ic";
connectAttr "IKCurveInfoSpine_M.al" "IKCurveInfoNormalizeSpine_M.i1y";
connectAttr "IKCurveInfoNormalizeSpine_M.oy" "IKCurveInfoAllMultiplySpine_M.i1y"
		;
connectAttr "Main.sy" "IKCurveInfoAllMultiplySpine_M.i2y";
connectAttr "IKSpine4_M.stretchy" "stretchySpineUnitConversion_M.i";
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineReverse_M.iy";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide0_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo0_M.ab";
connectAttr "stretchySpineMultiplyDivide0_M.oy" "stretchySpineBlendTwo0_M.i[1]";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide1_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo1_M.ab";
connectAttr "stretchySpineMultiplyDivide1_M.oy" "stretchySpineBlendTwo1_M.i[1]";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide2_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo2_M.ab";
connectAttr "stretchySpineMultiplyDivide2_M.oy" "stretchySpineBlendTwo2_M.i[1]";
connectAttr "IKCurveInfoAllMultiplySpine_M.oy" "stretchySpineMultiplyDivide3_M.i2y"
		;
connectAttr "stretchySpineUnitConversion_M.o" "stretchySpineBlendTwo3_M.ab";
connectAttr "stretchySpineMultiplyDivide3_M.oy" "stretchySpineBlendTwo3_M.i[1]";
connectAttr "UnTwistEndPelvis_M.ry" "IKTwistSpineUnitConversion_M.i";
connectAttr "FKIKSpine_M.FKIKBlend" "FKIKBlendSpineUnitConversion_M.i";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "FKIKBlendSpineReverse_M.ix";
connectAttr "FKIKSpine_M.autoVis" "FKIKBlendSpineCondition_M.ft";
connectAttr "FKIKSpine_M.IKVis" "FKIKBlendSpineCondition_M.ctr";
connectAttr "FKIKSpine_M.FKVis" "FKIKBlendSpineCondition_M.ctg";
connectAttr "FKIKSpine_M.FKIKBlend" "FKIKBlendSpineCondition_M.cfr";
connectAttr "FKIKBlendSpinesetRange_M.ox" "FKIKBlendSpineCondition_M.cfg";
connectAttr "FKIKSpine_M.FKIKBlend" "FKIKBlendSpinesetRange_M.vx";
connectAttr "FKWrist_R.s" "ScaleBlendWrist_R.c2";
connectAttr "FKIKBlendArmUnitConversion_R.o" "ScaleBlendWrist_R.b";
connectAttr "FKElbow_R.s" "ScaleBlendElbow_R.c2";
connectAttr "FKIKBlendArmUnitConversion_R.o" "ScaleBlendElbow_R.b";
connectAttr "FKShoulder_R.s" "ScaleBlendShoulder_R.c2";
connectAttr "FKIKBlendArmUnitConversion_R.o" "ScaleBlendShoulder_R.b";
connectAttr "FKChest_M.s" "ScaleBlendChest_M.c2";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "ScaleBlendChest_M.b";
connectAttr "FKSpineA_M.s" "ScaleBlendSpineA_M.c2";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "ScaleBlendSpineA_M.b";
connectAttr "FKAnkle_R.s" "ScaleBlendAnkle_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendAnkle_R.b";
connectAttr "FKKnee_R.s" "ScaleBlendKnee_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendKnee_R.b";
connectAttr "FKHip_R.s" "ScaleBlendHip_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendHip_R.b";
connectAttr "FKPelvis_M.s" "ScaleBlendPelvis_M.c2";
connectAttr "FKIKBlendSpineUnitConversion_M.o" "ScaleBlendPelvis_M.b";
connectAttr "IKArm_L.follow" "IKArm_LSetRangeFollow.vx";
connectAttr "IKArm_L.follow" "IKArm_LSetRangeFollow.vy";
connectAttr "PoleArm_L.follow" "PoleArm_LSetRangeFollow.vx";
connectAttr "PoleArm_L.follow" "PoleArm_LSetRangeFollow.vy";
connectAttr "FKIKArm_L.FKIKBlend" "FKIKBlendArmUnitConversion_L.i";
connectAttr "FKIKBlendArmUnitConversion_L.o" "FKIKBlendArmReverse_L.ix";
connectAttr "FKIKArm_L.autoVis" "FKIKBlendArmCondition_L.ft";
connectAttr "FKIKArm_L.IKVis" "FKIKBlendArmCondition_L.ctr";
connectAttr "FKIKArm_L.FKVis" "FKIKBlendArmCondition_L.ctg";
connectAttr "FKIKArm_L.FKIKBlend" "FKIKBlendArmCondition_L.cfr";
connectAttr "FKIKBlendArmsetRange_L.ox" "FKIKBlendArmCondition_L.cfg";
connectAttr "FKIKArm_L.FKIKBlend" "FKIKBlendArmsetRange_L.vx";
connectAttr "IKLeg_L.swivel" "unitConversion4.i";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vx";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vy";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegUnitConversion_L.i";
connectAttr "FKIKBlendLegUnitConversion_L.o" "FKIKBlendLegReverse_L.ix";
connectAttr "FKIKLeg_L.autoVis" "FKIKBlendLegCondition_L.ft";
connectAttr "FKIKLeg_L.IKVis" "FKIKBlendLegCondition_L.ctr";
connectAttr "FKIKLeg_L.FKVis" "FKIKBlendLegCondition_L.ctg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegCondition_L.cfr";
connectAttr "FKIKBlendLegsetRange_L.ox" "FKIKBlendLegCondition_L.cfg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegsetRange_L.vx";
connectAttr "IKLeg_L.rollAngle" "Leg_LAngleReverse.i1x";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vx";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vy";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vz";
connectAttr "Leg_LAngleReverse.ox" "IKRollAngleLeg_L.nx";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.my";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.mz";
connectAttr "IKRollAngleLeg_L.ox" "unitConversion5.i";
connectAttr "IKRollAngleLeg_L.oy" "unitConversion6.i";
connectAttr "FKWrist_L.s" "ScaleBlendWrist_L.c2";
connectAttr "FKIKBlendArmUnitConversion_L.o" "ScaleBlendWrist_L.b";
connectAttr "FKElbow_L.s" "ScaleBlendElbow_L.c2";
connectAttr "FKIKBlendArmUnitConversion_L.o" "ScaleBlendElbow_L.b";
connectAttr "FKShoulder_L.s" "ScaleBlendShoulder_L.c2";
connectAttr "FKIKBlendArmUnitConversion_L.o" "ScaleBlendShoulder_L.b";
connectAttr "FKAnkle_L.s" "ScaleBlendAnkle_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendAnkle_L.b";
connectAttr "FKKnee_L.s" "ScaleBlendKnee_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendKnee_L.b";
connectAttr "FKHip_L.s" "ScaleBlendHip_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendHip_L.b";
connectAttr "Center_M.CenterBtwFeet" "CenterBtwFeet_M.vx";
connectAttr "Center_M.CenterBtwFeet" "PelvisCenterBtwFeetUnitConversion_M.i";
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwFeetReverse_M.ix"
		;
connectAttr "irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "BPRig_irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ikSCsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikSplineSolver.msg" ":ikSystem.sol" -na;
// End of Juggernaut__rig.ma
