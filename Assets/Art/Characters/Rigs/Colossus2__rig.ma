//Maya ASCII 2013 scene
//Name: Colossus2__rig.ma
//Last modified: Fri, Jun 06, 2014 11:35:40 AM
//Codeset: UTF-8
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -30.566657669740465 13.758259092197752 36.869060860914225 ;
	setAttr ".r" -type "double3" -11.138352730855308 2115.7999999997896 1.1091182943639407e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 46.11497237275637;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 4.1784972405491922 7.7612609843192404 1.6715583172870974 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.5698517566574912 9.2330006461955882 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 17.434325782968777;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 7.4644855127609473 0.34231023451443998 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 23.777666600935916;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "ctrl_rig";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Main" -p "ctrl_rig";
	addAttr -ci true -sn "height" -ln "height" -at "double";
	addAttr -ci true -sn "fkVis" -ln "fkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "ikVis" -ln "ikVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fkIkVis" -ln "fkIkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimVis" -ln "aimVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "eyeVis" -ln "eyeVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fingerVis" -ln "fingerVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "bendVis" -ln "bendVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "arrowVis" -ln "arrowVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "jointVis" -ln "jointVis" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -l on ".height" 13.973514703036168;
	setAttr -cb on ".fkVis";
	setAttr -cb on ".ikVis";
	setAttr -cb on ".fkIkVis";
	setAttr -cb on ".aimVis";
	setAttr -cb on ".eyeVis";
	setAttr -cb on ".fingerVis";
	setAttr -cb on ".bendVis";
	setAttr -cb on ".arrowVis";
	setAttr -cb on ".jointVis";
createNode nurbsCurve -n "MainShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		-3.1944594689999992e-16 1.7145024419999999e-16 -2.7999949750000002
		-1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		-2.7999949750000002 4.9681991409999996e-32 -8.113684933e-16
		-1.9798954339999999 -1.2123363030000002e-16 1.9798954339999999
		-8.4369313090000001e-16 -1.7145024419999999e-16 2.7999949750000002
		1.9798954339999999 -1.2123363030000002e-16 1.9798954339999999
		2.7999949750000002 -9.2086260590000001e-32 1.503882763e-15
		1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		-3.1944594689999992e-16 1.7145024419999999e-16 -2.7999949750000002
		-1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		;
createNode transform -n "BaseSkeleton" -p "Main";
	setAttr -l on ".v" no;
createNode transform -n "BaseSkeletonOffset" -p "BaseSkeleton";
createNode joint -n "Pelvis" -p "BaseSkeletonOffset";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0 9.3382860296854506 0.55075592595768108 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 1;
createNode joint -n "HipTwist" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.9801654815673833 0.80333365262656287 -1.3252944876594559 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 205.1395790329064 89.999999999999758 ;
createNode joint -n "Hip" -p "HipTwist";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.0075656531445478237 2.1545137619359291 -0.3210443750556351 ;
	setAttr ".r" -type "double3" -1.1752566656535359 -35.295006097216394 -83.747378136107159 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 2;
createNode joint -n "Knee" -p "Hip";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.078236693501629873 5.7960342782580536 1.8108363937159615 ;
	setAttr ".r" -type "double3" -63.22298348183373 11.179388613527411 -13.010752049480663 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "Ankle" -p "Knee";
	setAttr ".t" -type "double3" -0.042059329120927913 6.9248591590581219 0.36347897434355386 ;
	setAttr ".r" -type "double3" 53.614568254588967 0.067470303103074031 -0.13352626925836877 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".pa" -type "double3" 3.1147589914174403 -1.2104724556304991 -11.405913270501992 ;
	setAttr ".dl" yes;
	setAttr ".typ" 4;
createNode joint -n "MiddleToe1" -p "Ankle";
	setAttr ".t" -type "double3" -0.048125071465983993 0.11671634841598412 -1.7423030526541037 ;
	setAttr ".r" -type "double3" 101.25684442881602 1.2928842515974439 179.0631713382237 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 7.3114940522536968e-15 -6.1623246953355635e-15 4.7056058031418247e-14 ;
	setAttr ".pa" -type "double3" -0.00019030234564052423 0.00053514845282692043 25.864574245063647 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Ball";
createNode joint -n "MiddleToe2_End" -p "MiddleToe1";
	setAttr ".t" -type "double3" 1.865174681370263e-14 2.0908640057041477 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -8.2480775474559511 0.057116226054612015 0.74000483750692625 ;
	setAttr ".dl" yes;
	setAttr ".typ" 5;
createNode joint -n "Heel_End" -p "Ankle";
	setAttr ".t" -type "double3" -0.16321778003202692 0.54525385587302388 1.0197533410667614 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 59.467892085048035 179.95177563522736 1.2503912846228187 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Heel";
createNode joint -n "ElbowShield" -p "Hip";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.18317404968246451 6.5338547349225919 2.1275027892559897 ;
	setAttr ".r" -type "double3" 11.735762420490044 -1.6901391111902413 -5.9460919655529132 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "ElbowShield_End" -p "ElbowShield";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 2.0399706718155866 0.13654328049844011 ;
	setAttr ".r" -type "double3" -63.22298348183373 11.179388613527411 -13.010752049480663 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 63.661942569916398 -5.0110212704251458 3.0045374368972722 ;
createNode joint -n "Head" -p "Pelvis";
	addAttr -ci true -k true -sn "global" -ln "global" -dv 10 -min 0 -max 10 -at "long";
	addAttr -ci true -sn "globalConnect" -ln "globalConnect" -dv 10 -min 0 -max 10 -at "long";
	setAttr ".t" -type "double3" 0 2.3102937814159556 -0.49011399193109967 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
	setAttr -k on ".global";
createNode joint -n "Head_End" -p "Head";
	setAttr ".t" -type "double3" 4.0601869640127816e-16 2.3178568510379023 2.248201624865942e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "23";
createNode joint -n "Jaw" -p "Head";
	setAttr ".t" -type "double3" 0 -0.79810682069005701 1.0250995629197761 ;
	setAttr ".r" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "23";
createNode joint -n "Jaw_End" -p "Jaw";
	setAttr ".t" -type "double3" 0 4.6908618918588001 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "23";
createNode transform -n "Head_globalLocator" -p "Head";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1.2852876108190543 1.2852876108190543 1.2852876108190543 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "Head_globalLocatorShape" -p "Head_globalLocator";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode joint -n "Shoulder" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -2.3090522421826116 -0.86558199877464403 0.48592887005093544 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -152.0053170522194 4.9505588210855915 -11.904809842065388 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
	setAttr ".otp" -type "string" "Shoulder1";
createNode joint -n "Elbow" -p "Shoulder";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.17735108621914186 3.5019857675373847 -0.057058645954093379 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -129.97952417590875 1.4902849999290784 0.83989225685842428 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "Fingers" -p "Elbow";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.12737200878343111 3.5950005764102659 0.34354560055083638 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 36.446714387517382 -2.231045088328806 -1.1437535407680814 ;
	setAttr ".otp" -type "string" "Hand1";
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "Fingers_End" -p "Fingers";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -5.3290705182007514e-15 1.7859488381795052 1.1546319456101628e-14 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".otp" -type "string" "Hand1";
	setAttr ".radi" 0.97203911833656331;
createNode transform -n "MotionSystem" -p "Main";
createNode transform -n "FKSystem" -p "MotionSystem";
createNode transform -n "FKParentConstraintToAnkle_R" -p "FKSystem";
	setAttr ".ro" 3;
createNode joint -n "FKOffsetMiddleToe1_R" -p "FKParentConstraintToAnkle_R";
	setAttr ".t" -type "double3" -0.048125071465984881 0.11671634841598422 -1.7423030526541037 ;
	setAttr ".r" -type "double3" 101.25683902660606 1.2928842668358922 179.06317626239738 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 7.3114939570091397e-15 -6.1623246953355635e-15 4.7056058031418247e-14 ;
createNode transform -n "FKExtraMiddleToe1_R" -p "FKOffsetMiddleToe1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -8.8817841970012523e-16 2.2204460492503131e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999989 ;
createNode transform -n "FKMiddleToe1_R" -p "FKExtraMiddleToe1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleToe1_RShape" -p "FKMiddleToe1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.83138740909999997 -0.0099768334289999996 -0.83138740909999997
		-1.1757593500000001 -0.0099768334289999996 -9.4956468399999994e-17
		-0.83138740909999997 -0.0099768334289999996 0.83138740909999997
		-3.6109525879999998e-16 -0.0099768334289999996 1.1757593500000001
		0.83138740909999997 -0.0099768334289999996 0.83138740909999997
		1.1757593500000001 -0.0099768334289999996 -9.4956468399999994e-17
		0.83138740909999997 -0.0099768334289999996 -0.83138740909999997
		6.111130259e-16 -0.0099768334289999996 -1.1757593500000001
		-0.83138740909999997 -0.0099768334289999996 -0.83138740909999997
		-1.1757593500000001 -0.0099768334289999996 -9.4956468399999994e-17
		-0.83138740909999997 -0.0099768334289999996 0.83138740909999997
		;
createNode joint -n "FKXMiddleToe1_R" -p "FKMiddleToe1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleToe2_End_R" -p "FKXMiddleToe1_R";
	setAttr ".t" -type "double3" 1.9539925233402755e-14 2.0908640057041485 -1.1102230246251563e-15 ;
	setAttr ".r" -type "double3" -70.53683372388943 1.7841727691760274 -178.70576745105654 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 78.78748328319449 -1.6371641940609554 179.20316282202387 ;
createNode parentConstraint -n "FKParentConstraintToAnkle_R_parentConstraint1" -p
		 "FKParentConstraintToAnkle_R";
	addAttr -ci true -k true -sn "w0" -ln "Ankle_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 180.00000566143601 1.7669474938124101 -5.721939225274367e-07 ;
	setAttr ".rst" -type "double3" -4.774054413169452 0.53817581497616107 2.0891292000427062 ;
	setAttr ".rsrr" -type "double3" 179.99999999999997 1.7669612584690433 -9.2072154870506024e-14 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToHip_R" -p "FKSystem";
	setAttr ".ro" 2;
createNode joint -n "FKOffsetElbowShield_R" -p "FKParentConstraintToHip_R";
	setAttr ".t" -type "double3" 0.18317404968246276 6.5338547349225884 2.1275027892559937 ;
	setAttr ".r" -type "double3" 11.735762513057432 -1.6901390612995293 -5.946092045167239 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraElbowShield_R" -p "FKOffsetElbowShield_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.3322676295501878e-15 8.8817841970012523e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999956 0.99999999999999956 ;
createNode transform -n "FKElbowShield_R" -p "FKExtraElbowShield_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1.0000000000000009 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbowShield_RShape" -p "FKElbowShield_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.9119495900000001 0.0077942343670000004 -1.309209343
		-2.7266358519999998 0.01132448126 0.02272174352
		-1.9440837630000001 0.0082208406590000005 1.3413424199999999
		-0.022701722139999998 0.00030138312810000002 1.8742225770000001
		1.9119907279999999 -0.0077947805150000004 1.3092082469999999
		2.7266769910000002 -0.011325027410000001 -0.02272283932
		1.9441249009999999 -0.0082213868070000005 -1.341343516
		0.022742860699999999 -0.00030192927580000001 -1.8742236729999999
		-1.9119495900000001 0.0077942343670000004 -1.309209343
		-2.7266358519999998 0.01132448126 0.02272174352
		-1.9440837630000001 0.0082208406590000005 1.3413424199999999
		;
createNode joint -n "FKXElbowShield_R" -p "FKElbowShield_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXElbowShield_End_R" -p "FKXElbowShield_R";
	setAttr ".t" -type "double3" -4.4408920985006262e-15 2.0399706718155861 0.13654328049843834 ;
	setAttr ".r" -type "double3" 202.00051147617185 1.6455413485447246 0.14401326115675117 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 157.99127861345528 1.5256844242989995 -0.76060326268134437 ;
createNode parentConstraint -n "FKParentConstraintToHip_R_parentConstraint1" -p "FKParentConstraintToHip_R";
	addAttr -ci true -k true -sn "w0" -ln "Hip_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -169.8467347066981 1.1939583362991846 6.0421172473827092 ;
	setAttr ".rst" -type "double3" -4.134679243503312 10.28485631942749 -0.48711901530623503 ;
	setAttr ".rsrr" -type "double3" -169.84673193873002 1.1939570157979933 6.0421192743625225 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToAnkle_L" -p "FKSystem";
	setAttr ".ro" 3;
createNode joint -n "FKOffsetMiddleToe1_L" -p "FKParentConstraintToAnkle_L";
	setAttr ".t" -type "double3" 0.048125071465984881 -0.11671634841598388 1.7423030526541039 ;
	setAttr ".r" -type "double3" 101.25684923124025 1.2928842668358922 179.06317626239738 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 7.3114939570091397e-15 -6.1623246953355635e-15 4.7056058031418247e-14 ;
createNode transform -n "FKExtraMiddleToe1_L" -p "FKOffsetMiddleToe1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 8.8817841970012523e-16 0 ;
	setAttr ".r" -type "double3" -3.8166656177562208e-14 -8.0010828705566563e-15 -1.6499127410092e-14 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKMiddleToe1_L" -p "FKExtraMiddleToe1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 4.4408920985006262e-16 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleToe1_LShape" -p "FKMiddleToe1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.83138744759111916 0.0099753630521477454 0.83138721535271887
		1.1757593205246393 0.0099751428954095722 -2.2189988690968221e-07
		0.83138731165821955 0.0099752094740708248 -0.83138760284726876
		-1.2559433137937503e-07 0.0099755237872556357 -1.1757594757808487
		-0.83138750654163196 0.0099759017145619744 -0.83138746691443333
		-1.1757593794751513 0.0099761218713005917 -2.9661827660731888e-08
		-0.83138737060873147 0.0099760552926393409 0.83138735128555408
		6.6643818463774096e-08 0.0099757409794549723 1.1757592242191339
		0.83138744759111916 0.0099753630521477454 0.83138721535271887
		1.1757593205246393 0.0099751428954095722 -2.2189988690968221e-07
		0.83138731165821955 0.0099752094740708248 -0.83138760284726876
		;
createNode joint -n "FKXMiddleToe1_L" -p "FKMiddleToe1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleToe2_End_L" -p "FKXMiddleToe1_L";
	setAttr ".t" -type "double3" -1.9539925233402755e-14 -2.0908640057041485 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 109.46316627611058 1.7841727691760272 -178.70576745105654 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -101.21251671680552 -1.6371641940609554 179.20316282202387 ;
createNode parentConstraint -n "FKParentConstraintToAnkle_L_parentConstraint1" -p
		 "FKParentConstraintToAnkle_L";
	addAttr -ci true -k true -sn "w0" -ln "Ankle_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -9.2183612246763682e-06 -1.7669719286523733 -4.1079248563805997e-07 ;
	setAttr ".rst" -type "double3" 4.7740544131694493 0.53817581497616107 2.0891292000427084 ;
	setAttr ".rsrr" -type "double3" 3.4798376276001204e-15 -1.7669612584690262 6.7058201510284748e-15 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToHip_L" -p "FKSystem";
	setAttr ".ro" 2;
createNode joint -n "FKOffsetElbowShield_L" -p "FKParentConstraintToHip_L";
	setAttr ".t" -type "double3" -0.18317404968246365 -6.5338547349225919 -2.1275027892559946 ;
	setAttr ".r" -type "double3" 11.735762513057432 -1.6901390612995293 -5.946092045167239 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraElbowShield_L" -p "FKOffsetElbowShield_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -9.5571940965461361e-15 -7.4544250346801169e-16 -6.4605016967227689e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKElbowShield_L" -p "FKExtraElbowShield_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 4.4408920985006262e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbowShield_LShape" -p "FKElbowShield_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.9119492884140581 -0.0077942636128542908 1.309210270873856
		2.7266356239427099 -0.011324655634814109 -0.022720770287086189
		1.9440836079826085 -0.0082210994605729226 -1.3413414903122447
		0.022701596887973352 -0.00030161619818080965 -1.8742217542534592
		-1.9119908841973441 0.0077946682615044338 -1.3092075319460139
		-2.7266772207259962 0.011325060285464428 0.02272350901492803
		-1.944125203765896 0.0082215041092235097 1.3413442292400886
		-0.022743193231259617 0.00030202084653074834 1.8742244931813019
		1.9119492884140581 -0.0077942636128542908 1.309210270873856
		2.7266356239427099 -0.011324655634814109 -0.022720770287086189
		1.9440836079826085 -0.0082210994605729226 -1.3413414903122447
		;
createNode joint -n "FKXElbowShield_L" -p "FKElbowShield_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXElbowShield_End_L" -p "FKXElbowShield_L";
	setAttr ".t" -type "double3" 3.5527136788005009e-15 -2.0399706718155866 -0.13654328049843922 ;
	setAttr ".r" -type "double3" 22.000511476171837 -1.6455413485447239 0.14401326115675112 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -22.00872138654471 1.5256844242989978 -0.76060326268134526 ;
createNode parentConstraint -n "FKParentConstraintToHip_L_parentConstraint1" -p "FKParentConstraintToHip_L";
	addAttr -ci true -k true -sn "w0" -ln "Hip_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 10.153270196466845 -1.1939556121231027 6.0421205096742758 ;
	setAttr ".rst" -type "double3" 4.134679243503312 10.284856319427488 -0.48711901530623153 ;
	setAttr ".rsrr" -type "double3" 10.153268061269941 -1.1939570157980166 6.0421192743624346 ;
	setAttr -k on ".w0";
createNode transform -n "PelvisCenterBtwLegsBlended_M" -p "FKSystem";
	setAttr ".ro" 3;
createNode transform -n "CenterOffset_M" -p "PelvisCenterBtwLegsBlended_M";
createNode transform -n "CenterExtra_M" -p "CenterOffset_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Center_M" -p "CenterExtra_M";
	addAttr -ci true -k true -sn "CenterBtwFeet" -ln "CenterBtwFeet" -min 0 -max 10 
		-at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 4;
	setAttr -k on ".CenterBtwFeet";
createNode nurbsCurve -n "Center_MShape" -p "Center_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 46 0 no 3
		47 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46
		47
		-4.7936814910000001 -0.1083674835 3.5399155420000001e-05
		-5.0988512510000001 -0.1083674835 0.93925342339999995
		-5.897798163 -0.1083674835 1.5197243090000001
		-6.8853524760000004 -0.1083674835 1.5197243090000001
		-7.6842993899999996 -0.1083674835 0.93925342339999995
		-7.9894691470000003 -0.1083674835 3.5399155420000001e-05
		-7.6842993899999996 -0.1083674835 -0.93918262509999995
		-6.8853524760000004 -0.1083674835 -1.519653511
		-5.897798163 -0.1083674835 -1.519653511
		-5.0988512510000001 -0.1083674835 -0.93918262509999995
		-4.7936814910000001 -0.1083674835 3.5399155420000001e-05
		-4.1267516329999997e-09 -0.1083674835 3.5399155420000001e-05
		4.7936814820000002 -0.1083674835 3.5399155420000001e-05
		5.0988512420000003 -0.1083674835 0.93925342339999995
		5.8977981540000002 -0.1083674835 1.5197243090000001
		6.8853524679999998 -0.1083674835 1.5197243090000001
		7.6842993829999999 -0.1083674835 0.93925342339999995
		7.9894691399999997 -0.1083674835 3.5399155420000001e-05
		7.6842993829999999 -0.1083674835 -0.93918262509999995
		6.8853524679999998 -0.1083674835 -1.519653511
		5.8977981540000002 -0.1083674835 -1.519653511
		5.0988512420000003 -0.1083674835 -0.93918262509999995
		4.7936814820000002 -0.1083674835 3.5399155420000001e-05
		-4.1267516329999997e-09 -0.1083674835 3.5399155420000001e-05
		-4.1267516329999997e-09 -0.1083674835 -4.7936460869999999
		-0.93921802830000001 -0.1083674835 -5.098815847
		-1.5196889140000001 -0.1083674835 -5.8977627589999999
		-1.5196889140000001 -0.1083674835 -6.8853170730000004
		-0.93921802830000001 -0.1083674835 -7.6842639879999997
		-4.1267516329999997e-09 -0.1083674835 -7.9894337430000002
		0.9392180202 -0.1083674835 -7.6842639879999997
		1.5196889060000001 -0.1083674835 -6.8853170730000004
		1.5196889060000001 -0.1083674835 -5.8977627589999999
		0.9392180202 -0.1083674835 -5.098815847
		-4.1267516329999997e-09 -0.1083674835 -4.7936460869999999
		-4.1267516329999997e-09 -0.1083674835 3.5399155420000001e-05
		-4.1267516329999997e-09 -0.1083674835 4.7937168860000003
		-0.93921802830000001 -0.1083674835 5.0988866460000004
		-1.5196889140000001 -0.1083674835 5.8978335580000003
		-1.5196889140000001 -0.1083674835 6.8853878709999998
		-0.93921802830000001 -0.1083674835 7.6843347849999999
		-4.1267516329999997e-09 -0.1083674835 7.9895045419999997
		0.9392180202 -0.1083674835 7.6843347849999999
		1.5196889060000001 -0.1083674835 6.8853878709999998
		1.5196889060000001 -0.1083674835 5.8978335580000003
		0.9392180202 -0.1083674835 5.0988866460000004
		-4.1267516329999997e-09 -0.1083674835 4.7937168860000003
		;
createNode joint -n "FKOffsetPelvis_M" -p "Center_M";
	setAttr ".ro" 3;
createNode transform -n "FKExtraPelvis_M" -p "FKOffsetPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode transform -n "FKPelvis_M" -p "FKExtraPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode nurbsCurve -n "FKPelvis_MShape" -p "FKPelvis_M";
	setAttr -l on -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.66921139417839381 1.895721592233463e-16 -0.66921139417839526
		-0.94640782974169246 2.1014475265295037e-16 0
		-0.66921139417839415 1.0761740003999552e-16 0.66921139417839448
		-2.7424531180469714e-16 -5.7950765969057784e-17 0.94640782974169246
		0.66921139417839404 -1.895721592233463e-16 0.66921139417839448
		0.94640782974169246 -2.1014475265295042e-16 0
		0.66921139417839415 -1.0761740003999558e-16 -0.6692113941783937
		5.0831749154463561e-16 5.7950765969057722e-17 -0.94640782974169246
		-0.66921139417839381 1.895721592233463e-16 -0.66921139417839526
		-0.94640782974169246 2.1014475265295037e-16 0
		-0.66921139417839415 1.0761740003999552e-16 0.66921139417839448
		;
createNode joint -n "FKXPelvis_M" -p "FKPelvis_M";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHipTwist_R" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" -1.9801654815673833 0.80333365262656287 -1.3252944876594559 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 205.13958175869882 90.000002504478161 ;
createNode transform -n "FKExtraHipTwist_R" -p "FKOffsetHipTwist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 4.4408920985006262e-16 ;
	setAttr ".ro" 2;
createNode transform -n "FKHipTwist_R" -p "FKExtraHipTwist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHipTwist_RShape" -p "FKHipTwist_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.0946772921096795 0.41246799316949362 -1.6240152643266417
		-2.7305782098500391 0.41246799316949362 -0.088814645567652631
		-2.0946772921096795 0.41246799316949362 1.4463859728616659
		-0.55947667418985114 0.41246799316949362 2.0822868907518748
		0.97572394417952757 0.41246799316949362 1.4463859728616659
		1.6116248614703359 0.41246799316949362 -0.088814645567652631
		0.97572394417952757 0.41246799316949362 -1.6240152643266417
		-0.55947667418985114 0.41246799316949362 -2.2599161820669997
		-2.0946772921096795 0.41246799316949362 -1.6240152643266417
		-2.7305782098500391 0.41246799316949362 -0.088814645567652631
		-2.0946772921096795 0.41246799316949362 1.4463859728616659
		;
createNode joint -n "FKXHipTwist_R" -p "FKHipTwist_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHip_R" -p "FKXHipTwist_R";
	setAttr ".t" -type "double3" -0.0075656531445478237 2.1545137619359291 -0.32104437505564043 ;
	setAttr ".r" -type "double3" -1.1752566699406366 -35.295005951135472 -83.747380960870871 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraHip_R" -p "FKOffsetHip_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 1.7763568394002505e-15 0 ;
	setAttr ".ro" 2;
createNode transform -n "FKHip_R" -p "FKExtraHip_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHip_RShape" -p "FKHip_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.2842300230000001 -0.16230252070000001 -1.2130084750000001
		-1.813909606 -0.16230252070000001 0.065751159010000004
		-1.2842300230000001 -0.16230252070000001 1.344510793
		-0.0054703886439999997 -0.16230252070000001 1.8741903769999999
		1.273289245 -0.16230252070000001 1.344510793
		1.8029688290000001 -0.16230252070000001 0.065751159010000004
		1.273289245 -0.16230252070000001 -1.2130084750000001
		-0.0054703886439999997 -0.16230252070000001 -1.7426880579999999
		-1.2842300230000001 -0.16230252070000001 -1.2130084750000001
		-1.813909606 -0.16230252070000001 0.065751159010000004
		-1.2842300230000001 -0.16230252070000001 1.344510793
		;
createNode joint -n "FKXHip_R" -p "FKHip_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetKnee_R" -p "FKXHip_R";
	setAttr ".t" -type "double3" -0.078236693501628096 5.7960342782580501 1.8108363937159624 ;
	setAttr ".r" -type "double3" -63.222983523873538 11.179388963646533 -13.010752217876419 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraKnee_R" -p "FKOffsetKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 -4.4408920985006262e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKKnee_R" -p "FKExtraKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999956 0.99999999999999933 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKKnee_RShape" -p "FKKnee_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.521240943 -0.062070414990000003 -1.5349183500000001
		-2.1515813819999998 -0.062070414990000003 -0.01314191462
		-1.521240943 -0.062070414990000003 1.5086345210000001
		0.0005354922933 -0.062070414990000003 2.1389749600000001
		1.5223119279999999 -0.062070414990000003 1.5086345210000001
		2.152652367 -0.062070414990000003 -0.01314191462
		1.5223119279999999 -0.062070414990000003 -1.5349183500000001
		0.0005354922933 -0.062070414990000003 -2.1652587890000001
		-1.521240943 -0.062070414990000003 -1.5349183500000001
		-2.1515813819999998 -0.062070414990000003 -0.01314191462
		-1.521240943 -0.062070414990000003 1.5086345210000001
		;
createNode joint -n "FKXKnee_R" -p "FKKnee_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetAnkle_R" -p "FKXKnee_R";
	setAttr ".t" -type "double3" -0.042059329120927025 6.9248591590581272 0.36347897434355247 ;
	setAttr ".r" -type "double3" 53.614567097633909 0.067470302828465237 -0.1335262629108486 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraAnkle_R" -p "FKOffsetAnkle_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.1102230246251563e-16 -2.2204460492503131e-16 ;
	setAttr ".ro" 3;
createNode transform -n "FKAnkle_R" -p "FKExtraAnkle_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.1102230246251563e-16 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKAnkle_RShape" -p "FKAnkle_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.3738108659999999 -0.025228781580000002 -1.440718438
		-1.9482038669999999 -0.025228781580000002 -0.054011064179999999
		-1.3738108659999999 -0.025228781580000002 1.33269631
		0.012896507729999999 -0.025228781580000002 1.907089311
		1.3996038820000001 -0.025228781580000002 1.33269631
		1.9739968830000001 -0.025228781580000002 -0.054011064179999999
		1.3996038820000001 -0.025228781580000002 -1.440718438
		0.012896507729999999 -0.025228781580000002 -2.015111439
		-1.3738108659999999 -0.025228781580000002 -1.440718438
		-1.9482038669999999 -0.025228781580000002 -0.054011064179999999
		-1.3738108659999999 -0.025228781580000002 1.33269631
		;
createNode joint -n "FKXAnkle_R" -p "FKAnkle_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToAnkle_R" -p "FKXAnkle_R";
	setAttr ".r" -type "double3" -180 1.7669612584689351 -2.2069531490250804e-32 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode joint -n "FKXHeel_End_R" -p "FKXAnkle_R";
	setAttr ".t" -type "double3" -0.16321778003202603 0.545253855873024 1.0197533410667612 ;
	setAttr ".r" -type "double3" 59.506463443299218 -1.8147647267136775 178.74898158894112 ;
	setAttr ".jo" -type "double3" -180 1.7669612584689347 2.1645897125257776e-16 ;
createNode joint -n "FKOffsetHead_M" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 0 2.3102937814159556 -0.49011399193109967 ;
	setAttr ".ro" 5;
createNode transform -n "FKGlobalStaticHead_M" -p "FKOffsetHead_M";
	setAttr ".ro" 5;
createNode transform -n "FKGlobalHead_M" -p "FKGlobalStaticHead_M";
	setAttr ".ro" 5;
createNode transform -n "FKExtraHead_M" -p "FKGlobalHead_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
createNode transform -n "FKHead_M" -p "FKExtraHead_M";
	addAttr -ci true -k true -sn "Global" -ln "Global" -dv 10 -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".Global";
createNode nurbsCurve -n "FKHead_MShape" -p "FKHead_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.998771254 3.2346477230000001 -4.2327875989999999
		-2.826689418 3.2346477230000001 -0.53116021170000005
		-1.998771254 3.2346477230000001 3.1704671750000002
		-8.1910387480000003e-16 3.2346477230000001 4.7037314439999998
		1.998771254 3.2346477230000001 3.1704671750000002
		2.826689418 3.2346477230000001 -0.53116021170000005
		1.998771254 3.2346477230000001 -4.2327875989999999
		1.5182204e-15 3.2346477230000001 -5.7660518679999999
		-1.998771254 3.2346477230000001 -4.2327875989999999
		-2.826689418 3.2346477230000001 -0.53116021170000005
		-1.998771254 3.2346477230000001 3.1704671750000002
		;
createNode joint -n "FKXHead_M" -p "FKHead_M";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXHead_End_M" -p "FKXHead_M";
	setAttr ".t" -type "double3" 4.0601869640127816e-16 2.3178568510379023 2.248201624865942e-15 ;
createNode joint -n "FKOffsetJaw_M" -p "FKXHead_M";
	setAttr ".t" -type "double3" 0 -0.79810682069005701 1.0250995629197761 ;
	setAttr ".r" -type "double3" 90.000002504478161 0 0 ;
createNode transform -n "FKExtraJaw_M" -p "FKOffsetJaw_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -1.7763568394002505e-15 ;
createNode transform -n "FKJaw_M" -p "FKExtraJaw_M";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKJaw_MShape" -p "FKJaw_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.7360137249999998 -0.015106699059999999 -0.88702694959999995
		-3.8693077169999999 -0.015106699059999999 6.7428673029999991e-14
		-2.7360137249999998 -0.015106699059999999 0.88702694959999995
		-1.121228574e-15 -0.015106699059999999 1.254445542
		2.7360137249999998 -0.015106699059999999 0.88702694959999995
		3.8693077169999999 -0.015106699059999999 6.7428673029999991e-14
		2.7360137249999998 -0.015106699059999999 -0.88702694959999995
		2.078212723e-15 -0.015106699059999999 -1.254445542
		-2.7360137249999998 -0.015106699059999999 -0.88702694959999995
		-3.8693077169999999 -0.015106699059999999 6.7428673029999991e-14
		-2.7360137249999998 -0.015106699059999999 0.88702694959999995
		;
createNode joint -n "FKXJaw_M" -p "FKJaw_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXJaw_End_M" -p "FKXJaw_M";
	setAttr ".t" -type "double3" 0 4.6908618918588001 1.7763568394002505e-15 ;
	setAttr ".jo" -type "double3" -89.999999999999986 0 0 ;
createNode orientConstraint -n "FKGlobalHead_M_orientConstraint1" -p "FKGlobalHead_M";
	addAttr -ci true -k true -sn "w0" -ln "GlobalHead_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "FKGlobalStaticHead_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "FKOffsetShoulder_R" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" -2.3090522421826116 -0.86558199877464403 0.48592887005093544 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -152.00531356350018 4.9505586764656773 -11.904809695015247 ;
createNode transform -n "FKExtraShoulder_R" -p "FKOffsetShoulder_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKShoulder_R" -p "FKExtraShoulder_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.7763568394002505e-15 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulder_RShape" -p "FKShoulder_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.035304236 -0.020948132229999999 -1.009085974
		-1.46479385 -0.020948132229999999 0.02779367848
		-1.035304236 -0.020948132229999999 1.0646733310000001
		0.001575416987 -0.020948132229999999 1.4941629460000001
		1.0384550699999999 -0.020948132229999999 1.0646733310000001
		1.4679446840000001 -0.020948132229999999 0.02779367848
		1.0384550699999999 -0.020948132229999999 -1.009085974
		0.001575416987 -0.020948132229999999 -1.438575589
		-1.035304236 -0.020948132229999999 -1.009085974
		-1.46479385 -0.020948132229999999 0.02779367848
		-1.035304236 -0.020948132229999999 1.0646733310000001
		;
createNode joint -n "FKXShoulder_R" -p "FKShoulder_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow_R" -p "FKXShoulder_R";
	setAttr ".t" -type "double3" -0.17735108621914186 3.5019857675373842 -0.057058645954092491 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -129.97952443371815 1.4902849514206058 0.839892247258895 ;
createNode transform -n "FKExtraElbow_R" -p "FKOffsetElbow_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 5.5511151231257827e-17 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKElbow_R" -p "FKExtraElbow_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 1.3877787807814457e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow_RShape" -p "FKElbow_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.89801102610000005 8.4782677449999995e-14 -0.89801102610000005
		-1.2699793720000001 8.4810283700000003e-14 1.8280962089999998e-14
		-0.89801102610000005 8.4672702819999997e-14 0.89801102610000005
		-3.680082497e-16 8.4450527829999997e-14 1.2699793720000001
		0.89801102610000005 8.4273905809999993e-14 0.89801102610000005
		1.2699793720000001 8.4246299570000003e-14 1.8280962089999998e-14
		0.89801102610000005 8.4383880449999996e-14 -0.89801102610000005
		6.8210839820000001e-16 8.4606055439999996e-14 -1.2699793720000001
		-0.89801102610000005 8.4782677449999995e-14 -0.89801102610000005
		-1.2699793720000001 8.4810283700000003e-14 1.8280962089999998e-14
		-0.89801102610000005 8.4672702819999997e-14 0.89801102610000005
		;
createNode joint -n "FKXElbow_R" -p "FKElbow_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetFingers_R" -p "FKXElbow_R";
	setAttr ".t" -type "double3" 0.1273720087834338 3.5950005764102655 0.34354560055083549 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 36.446715864077611 -2.231045007004699 -1.1437534896110106 ;
createNode transform -n "FKExtraFingers_R" -p "FKOffsetFingers_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -1.1102230246251563e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode transform -n "FKFingers_R" -p "FKExtraFingers_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -4.4408920985006262e-16 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKFingers_RShape" -p "FKFingers_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.81385025219999996 -0.06295614745 -0.81385025219999996
		-1.1509580639999999 -0.06295614745 -5.9497747390000005e-16
		-0.81385025219999996 -0.06295614745 0.81385025219999996
		1.1869648260000001e-15 -0.06295614745 1.1509580639999999
		0.81385025219999996 -0.06295614745 0.81385025219999996
		1.1509580639999999 -0.06295614745 -5.9497747390000005e-16
		0.81385025219999996 -0.06295614745 -0.81385025219999996
		2.1386655000000001e-15 -0.06295614745 -1.1509580639999999
		-0.81385025219999996 -0.06295614745 -0.81385025219999996
		-1.1509580639999999 -0.06295614745 -5.9497747390000005e-16
		-0.81385025219999996 -0.06295614745 0.81385025219999996
		;
createNode joint -n "FKXFingers_R" -p "FKFingers_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXFingers_End_R" -p "FKXFingers_R";
	setAttr ".t" -type "double3" -4.4408920985006262e-15 1.7859488381795066 1.4210854715202004e-14 ;
	setAttr ".r" -type "double3" 115.11746200516393 -16.272889478817831 2.3654194306648035 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -115.1174620051639 16.272889478817817 -2.3654194306647995 ;
createNode joint -n "FKOffsetHipTwist_L" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 1.9801654815673839 0.80333365262656109 -1.3252944876594557 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.0509930133114849e-06 25.139577240952882 90.000002504478161 ;
createNode transform -n "FKExtraHipTwist_L" -p "FKOffsetHipTwist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 0 ;
	setAttr ".r" -type "double3" 1.0495830448829606e-13 1.5294185322743802e-29 1.6697912077683464e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKHipTwist_L" -p "FKExtraHipTwist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 0.99999999999999933 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHipTwist_LShape" -p "FKHipTwist_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		2.0946773875191003 -0.41246807609331282 1.6240151202052182
		2.7305781842095942 -0.41246820474105728 0.088814451305744058
		2.0946771454193769 -0.41246823273622879 -1.4463861169830738
		0.55947647735906614 -0.41246814367963602 -2.0822869138234252
		-0.97572409086980916 -0.41246798973942256 -1.4463858748833642
		-1.611624887110751 -0.4124678610916781 0.088814793686438964
		-0.97572384877008556 -0.41246783309650659 1.6240153623049292
		0.55947681973977659 -0.4124679221530998 2.259916158995428
		2.0946773875191003 -0.41246807609331282 1.6240151202052182
		2.7305781842095942 -0.41246820474105728 0.088814451305744058
		2.0946771454193769 -0.41246823273622879 -1.4463861169830738
		;
createNode joint -n "FKXHipTwist_L" -p "FKHipTwist_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHip_L" -p "FKXHipTwist_L";
	setAttr ".t" -type "double3" 0.0075656531445496 -2.1545137619359283 0.3210443750556391 ;
	setAttr ".r" -type "double3" -1.1752566699406366 -35.295005951135472 -83.747380960870871 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraHip_L" -p "FKOffsetHip_L";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" -4.8205281890931427e-15 -9.9392333795734763e-17 -3.1308585145656493e-15 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
createNode transform -n "FKHip_L" -p "FKExtraHip_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHip_LShape" -p "FKHip_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.2842299154190604 0.16230253147033302 1.2130087344316287
		1.8139095983956848 0.16230240043775446 -0.065750858166686132
		1.2842301153723137 0.16230230488616115 -1.3445105335683532
		0.0054705224279931741 0.16230230078838659 -1.8741902175449792
		-1.2732891526276768 0.16230239054484841 -1.3445107335216131
		-1.8029688366043009 0.16230252157742697 -0.065751140943298036
		-1.2732893525809297 0.16230261712902028 1.213008534478369
		0.0054702396513901519 0.16230262122679484 1.7426882174549947
		1.2842299154190604 0.16230253147033302 1.2130087344316287
		1.8139095983956848 0.16230240043775446 -0.065750858166686132
		1.2842301153723137 0.16230230488616115 -1.3445105335683532
		;
createNode joint -n "FKXHip_L" -p "FKHip_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetKnee_L" -p "FKXHip_L";
	setAttr ".t" -type "double3" 0.078236693501631649 -5.7960342782580492 -1.8108363937159655 ;
	setAttr ".r" -type "double3" -63.222983523873538 11.179388963646533 -13.010752217876419 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraKnee_L" -p "FKOffsetKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" -2.5447543462139257e-14 -3.478731682850721e-16 -1.987846675914697e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "FKKnee_L" -p "FKExtraKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000009 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKKnee_LShape" -p "FKKnee_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.521240892421468 0.062069677585376397 1.5349184651699184
		2.1515813449677772 0.062069496903038861 0.013142035400995011
		1.521240919514093 0.062069432632187997 -1.5086344058300702
		-0.0005355101681319141 0.062069522421816181 -2.1389748583763919
		-1.5223119514858912 0.062069713674377525 -1.5086344329227184
		-2.1526524040322008 0.062069894356715061 0.013141997086205226
		-1.5223119785785173 0.062069958627565917 1.5349184380772711
		-0.00053554848289039114 0.062069868837938635 2.1652588906235923
		1.521240892421468 0.062069677585376397 1.5349184651699184
		2.1515813449677772 0.062069496903038861 0.013142035400995011
		1.521240919514093 0.062069432632187997 -1.5086344058300702
		;
createNode joint -n "FKXKnee_L" -p "FKKnee_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetAnkle_L" -p "FKXKnee_L";
	setAttr ".t" -type "double3" 0.042059329120926137 -6.9248591590581192 -0.36347897434354914 ;
	setAttr ".r" -type "double3" 53.614567097633909 0.067470302828465237 -0.1335262629108486 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraAnkle_L" -p "FKOffsetAnkle_L";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 0 7.9513867036587899e-16 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKAnkle_L" -p "FKExtraAnkle_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKAnkle_LShape" -p "FKAnkle_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.373810074343301 0.025228918250391089 1.4407195471849985
		1.9482031860107121 0.025228779531192513 0.054012219204955025
		1.3738102956781288 0.025228695398498369 -1.3326952008149808
		-0.012897032211920845 0.025228715136099918 -1.9070883124823961
		-1.3996044523218585 0.02522882718197772 -1.3326954221498193
		-1.9739975639892693 0.025228965901176403 0.05401190619022489
		-1.3996046736566856 0.025229050033870437 1.4407193258501607
		-0.012897345226636324 0.025229030296268773 2.0151124375175753
		1.373810074343301 0.025228918250391089 1.4407195471849985
		1.9482031860107121 0.025228779531192513 0.054012219204955025
		1.3738102956781288 0.025228695398498369 -1.3326952008149808
		;
createNode joint -n "FKXAnkle_L" -p "FKAnkle_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToAnkle_L" -p "FKXAnkle_L";
	setAttr ".r" -type "double3" 0 1.7669612584689365 0 ;
	setAttr ".ro" 3;
createNode joint -n "FKXHeel_End_L" -p "FKXAnkle_L";
	setAttr ".t" -type "double3" 0.16321778003202692 -0.54525385587302422 -1.0197533410667616 ;
	setAttr ".r" -type "double3" 59.506463443299197 178.18523527328631 1.2510184110588598 ;
	setAttr ".jo" -type "double3" 0 1.766961258468936 0 ;
createNode joint -n "FKOffsetShoulder_L" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 2.3090522421826107 -0.86558199877464403 0.48592887005093693 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 27.99468558272617 -4.9505586764656755 11.904809695015247 ;
createNode transform -n "FKExtraShoulder_L" -p "FKOffsetShoulder_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -2.6645352591003757e-15 0 ;
	setAttr ".r" -type "double3" 6.5350459470695698e-15 1.1927080055488188e-15 1.1927080055488188e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
createNode transform -n "FKShoulder_L" -p "FKExtraShoulder_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulder_LShape" -p "FKShoulder_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.0353042360000029 0.020948117193445803 1.0090859743121501
		1.4647938500000022 0.020948132644159401 -0.027793678167851169
		1.0353042360000029 0.020948148094868557 -1.0646733306878504
		-0.0015754169869977019 0.020948154494763394 -1.4941629456878509
		-1.0384550699999977 0.020948148094869445 -1.0646733306878504
		-1.4679446839999979 0.020948132644159401 -0.027793678167850722
		-1.0384550699999977 0.02094811719344758 1.0090859743121494
		-0.0015754169869985901 0.020948110793552743 1.4385755893121499
		1.0353042360000029 0.020948117193445803 1.0090859743121501
		1.4647938500000022 0.020948132644159401 -0.027793678167851169
		1.0353042360000029 0.020948148094868557 -1.0646733306878504
		;
createNode joint -n "FKXShoulder_L" -p "FKShoulder_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow_L" -p "FKXShoulder_L";
	setAttr ".t" -type "double3" 0.17735108621914364 -3.5019857675373829 0.057058645954093379 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -129.97952443371815 1.4902849514206058 0.839892247258895 ;
createNode transform -n "FKExtraElbow_L" -p "FKOffsetElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 5.5511151231257827e-17 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -1.9480897423964041e-14 -3.9756933518293952e-15 -3.1805546814635176e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
createNode transform -n "FKElbow_L" -p "FKExtraElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000009 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow_LShape" -p "FKElbow_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.89801102730408555 2.6770527988828619e-08 0.89801105910675716
		1.2699793733447049 3.9991103067960054e-08 3.3064984528152763e-08
		0.89801102758532292 5.3521415216417623e-08 -0.89801099309324162
		1.5435679401321067e-09 5.9435591143186528e-08 -1.2699793391338612
		-0.89801102461467852 5.4269186616773268e-08 -0.89801099337447887
		-1.2699793706552978 4.1048611482130681e-08 3.2667256455454208e-08
		-0.89801102489591555 2.7518299167139659e-08 0.89801105882552079
		1.1458380910767121e-09 2.1604123351393056e-08 1.269979404866139
		0.89801102730408555 2.6770527988828619e-08 0.89801105910675716
		1.2699793733447049 3.9991103067960054e-08 3.3064984528152763e-08
		0.89801102758532292 5.3521415216417623e-08 -0.89801099309324162
		;
createNode joint -n "FKXElbow_L" -p "FKElbow_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetFingers_L" -p "FKXElbow_L";
	setAttr ".t" -type "double3" -0.12737200878343469 -3.5950005764102624 -0.34354560055083727 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 36.446715864077611 -2.231045007004699 -1.1437534896110106 ;
createNode transform -n "FKExtraFingers_L" -p "FKOffsetFingers_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 5.5511151231257827e-16 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 2.5842006786891062e-14 5.565970692561152e-15 -7.9513867036588008e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999967 0.99999999999999989 ;
createNode transform -n "FKFingers_L" -p "FKExtraFingers_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -6.106226635438361e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000004 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKFingers_LShape" -p "FKFingers_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.81385025005103007 0.062956159110399867 0.81385020978195211
		1.1509580622403766 0.0629561709118166 -4.2256774968052462e-08
		0.81385025082972273 0.062956183309715308 -0.81385029461804859
		-1.2090071166426242e-09 0.06295618904157585 -1.1509581068073942
		-0.81385025357027851 0.062956184749751176 -0.81385029539674125
		-1.150958065759625 0.062956172948334221 -4.3358012291605519e-08
		-0.81385025434897118 0.06295616055043507 0.81385020900325955
		-2.3102462165525139e-09 0.06295615481857475 1.1509580211926052
		0.81385025005103007 0.062956159110399867 0.81385020978195211
		1.1509580622403766 0.0629561709118166 -4.2256774968052462e-08
		0.81385025082972273 0.062956183309715308 -0.81385029461804859
		;
createNode joint -n "FKXFingers_L" -p "FKFingers_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXFingers_End_L" -p "FKXFingers_L";
	setAttr ".t" -type "double3" 4.4408920985006262e-15 -1.7859488381795039 -1.1546319456101628e-14 ;
	setAttr ".r" -type "double3" -64.882537994836113 -16.272889478817838 2.3654194306648022 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 64.882537994836127 16.272889478817838 -2.3654194306648 ;
createNode parentConstraint -n "PelvisCenterBtwLegsBlended_M_parentConstraint1" -p
		 "PelvisCenterBtwLegsBlended_M";
	addAttr -ci true -k true -sn "w0" -ln "PelvisCenter_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PelvisCenterBtwLegsOffset_MW1" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0 9.3382860296854506 0.55075592595768108 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKSystem" -p "MotionSystem";
createNode transform -n "IKParentConstraint" -p "IKSystem";
createNode transform -n "IKParentConstraintHip_R" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999989 ;
createNode transform -n "IKOffsetHip_R" -p "IKParentConstraintHip_R";
	setAttr ".t" -type "double3" -0.0075656531445460473 2.1545137619359296 -0.3210443750556391 ;
	setAttr ".r" -type "double3" -1.1752566656536314 -35.295006097216358 -83.747378136107159 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1 ;
createNode joint -n "IKXHip_R" -p "IKOffsetHip_R";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 0 ;
	setAttr ".r" -type "double3" -3.7960493353007844e-16 -1.2731483717422901e-14 5.890845437457887e-15 ;
	setAttr ".ro" 2;
createNode joint -n "IKXKnee_R" -p "IKXHip_R";
	setAttr ".t" -type "double3" -0.078236693501632537 5.7960342782580527 1.8108363937159633 ;
	setAttr ".r" -type "double3" -63.222984979310397 11.179388969587604 -13.010752380657332 ;
	setAttr ".ro" 2;
	setAttr ".pa" -type "double3" -63.222983523873538 11.179388963646533 -13.010752217876419 ;
createNode joint -n "IKXAnkle_R" -p "IKXKnee_R";
	setAttr ".t" -type "double3" -0.042059329120926137 6.9248591590581245 0.3634789743435527 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" 53.614567097633909 0.067470302828465237 -0.1335262629108486 ;
createNode joint -n "IKXMiddleToe1_R" -p "IKXAnkle_R";
	setAttr ".t" -type "double3" -0.048125071465984881 0.11671634841598422 -1.7423030526541037 ;
	setAttr ".r" -type "double3" 101.25683902660606 1.2928861596352164 179.06318977298912 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 7.3114939570091397e-15 -6.1623246953355635e-15 4.7056058031418247e-14 ;
	setAttr ".pa" -type "double3" 101.25684410576535 1.2928842668358922 179.06317626239738 ;
createNode joint -n "IKXMiddleToe2_End_R" -p "IKXMiddleToe1_R";
	setAttr ".t" -type "double3" 1.865174681370263e-14 2.0908640057041472 -2.2204460492503131e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -8.2480775313258068 0.05711622296450853 0.740004839433131 ;
createNode ikEffector -n "effector2" -p "IKXMiddleToe1_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "IKFKAlignedLeg_R" -p "IKXAnkle_R";
	setAttr ".t" -type "double3" 8.558117432500012e-07 1.2955358230648528e-08 -4.9298207760450907e-07 ;
	setAttr ".r" -type "double3" -2.975562156621711e-06 0 -5.1958786948909267e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode orientConstraint -n "IKXAnkle_R_orientConstraint1" -p "IKXAnkle_R";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -126.3959983945421 1.115813483790568 1.2889014725046297 ;
	setAttr ".o" -type "double3" 0 1.7669610520758228 0 ;
	setAttr ".rsrr" -type "double3" 53.614567097584199 0.067470281495837461 -0.1335262471914922 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector3" -p "IKXAnkle_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector1" -p "IKXKnee_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_R" -p "IKXKnee_R";
	setAttr ".r" -type "double3" -126.37090066489559 1.1155319192321096 1.2891436652395634 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000004 ;
createNode annotationShape -n "PoleAnnotationLeg_RShape" -p "PoleAnnotationLeg_R";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintHip_R_parentConstraint1" -p "IKParentConstraintHip_R";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 154.8604182413012 1.526666247102488e-13 89.999997732757279 ;
	setAttr ".rst" -type "double3" -1.9801654815673833 10.141619682312012 -0.77453856170177493 ;
	setAttr ".rsrr" -type "double3" 154.8604209670936 0 90.000000000000242 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintHip_L" -p "IKParentConstraint";
	setAttr ".ro" 2;
createNode transform -n "IKOffsetHip_L" -p "IKParentConstraintHip_L";
	setAttr ".t" -type "double3" 0.0075656531445460473 -2.1545137619359283 0.32104437505564043 ;
	setAttr ".r" -type "double3" -1.1752566656535302 -35.295006097216337 -83.74737813610723 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999967 0.99999999999999989 ;
createNode joint -n "IKXHip_L" -p "IKOffsetHip_L";
	setAttr ".t" -type "double3" 0 1.7763568394002505e-15 -1.3322676295501878e-15 ;
	setAttr ".r" -type "double3" -9.522157467865993e-08 -3.1936163862708512e-06 1.4776832206107444e-06 ;
	setAttr ".ro" 2;
createNode joint -n "IKXKnee_L" -p "IKXHip_L";
	setAttr ".t" -type "double3" 0.078236693501640531 -5.7960342782580527 -1.8108363937159653 ;
	setAttr ".r" -type "double3" -63.222982464453061 11.17938896147105 -13.010751988988568 ;
	setAttr ".ro" 2;
	setAttr ".pa" -type "double3" -63.222983523873538 11.179388963646533 -13.010752217876419 ;
createNode joint -n "IKXAnkle_L" -p "IKXKnee_L";
	setAttr ".t" -type "double3" 0.042059329120927025 -6.9248591590581245 -0.36347897434354759 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" 53.614567097633909 0.067470302828465237 -0.1335262629108486 ;
createNode joint -n "IKXMiddleToe1_L" -p "IKXAnkle_L";
	setAttr ".t" -type "double3" 0.048125071465984881 -0.11671634841598388 1.7423030526541039 ;
	setAttr ".r" -type "double3" 101.25684923124025 1.2928827710337973 179.06316582383599 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 7.3114939570091397e-15 -6.1623246953355635e-15 4.7056058031418247e-14 ;
	setAttr ".pa" -type "double3" 101.25684410576535 1.2928842668358922 179.06317626239738 ;
createNode joint -n "IKXMiddleToe2_End_L" -p "IKXMiddleToe1_L";
	setAttr ".t" -type "double3" -1.865174681370263e-14 -2.0908640057041481 2.2204460492503131e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -8.2480775313258068 0.05711622296450853 0.740004839433131 ;
createNode ikEffector -n "effector5" -p "IKXMiddleToe1_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "IKFKAlignedLeg_L" -p "IKXAnkle_L";
	setAttr ".t" -type "double3" -1.7793416429157105e-07 -9.9138811826549045e-08 -5.0859200650954506e-07 ;
	setAttr ".r" -type "double3" 1.2339322987714122e-06 5.0662754805550887e-06 -2.4733728567549788e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode orientConstraint -n "IKXAnkle_L_orientConstraint1" -p "IKXAnkle_L";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 53.60399429517981 1.115814367791091 1.2888973449260832 ;
	setAttr ".o" -type "double3" 180 -1.7669610520757555 0 ;
	setAttr ".rsrr" -type "double3" 53.614567097584221 0.067470281495747797 -0.13352624719142753 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector6" -p "IKXAnkle_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector4" -p "IKXKnee_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_L" -p "IKXKnee_L";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 53.62909933510447 1.1155319192319784 1.2891436652395749 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
createNode annotationShape -n "PoleAnnotationLeg_LShape" -p "PoleAnnotationLeg_L";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintHip_L_parentConstraint1" -p "IKParentConstraintHip_L";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -25.139577240952999 4.4042152622383557e-06 90.000000396222816 ;
	setAttr ".rst" -type "double3" 1.9801654815673833 10.141619682312012 -0.77453856170177493 ;
	setAttr ".rsrr" -type "double3" -25.139579032906394 -1.7566776614494369e-15 90.000000000000242 ;
	setAttr -k on ".w0";
createNode transform -n "IKHandle" -p "IKSystem";
createNode transform -n "IKParentConstraintLeg_R" -p "IKHandle";
	setAttr ".t" -type "double3" -4.774054429486509 0.53817582214506032 2.0891292062547202 ;
	setAttr ".ro" 3;
createNode transform -n "IKExtraLeg_R" -p "IKParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_R" -p "IKExtraLeg_R";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "toe" -ln "toe" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".toe";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
createNode nurbsCurve -n "IKLeg_RShape" -p "IKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-2.3619209589999999 -0.2009769293 2.6905080770000001
		2.3619209589999999 -0.2009769293 2.6905080770000001
		2.3619209589999999 0.99640672789999996 2.6905080770000001
		1.5293311700000001 0.99640672789999996 -1.478531781
		1.5293311700000001 -0.2009769293 -1.478531781
		-1.5293311700000001 -0.2009769293 -1.478531781
		-1.5293311700000001 0.99640672789999996 -1.478531781
		-2.3619209589999999 0.99640672789999996 2.6905080770000001
		-2.3619209589999999 -0.2009769293 2.6905080770000001
		-1.5293311700000001 -0.2009769293 -1.478531781
		-1.5293311700000001 0.99640672789999996 -1.478531781
		1.5293311700000001 0.99640672789999996 -1.478531781
		1.523013768 -0.2009769293 -1.476848384
		2.3521642730000001 -0.2009769293 2.6888246790000001
		2.3619209589999999 0.99640672789999996 2.6905080770000001
		-2.3619209589999999 0.99640672789999996 2.6905080770000001
		;
createNode transform -n "IKFootRollLeg_R" -p "IKLeg_R";
	setAttr ".r" -type "double3" 0 0.52387069249546392 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "IKRollLegHeel_R" -p "IKFootRollLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.18530220696568733 -0.54525386304192147 -1.015972449764204 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_R" -p "IKRollLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -1.7347234759768071e-18 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 1.9878466759146992e-16 0 ;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999956 0.99999999999999933 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_R" -p "IKExtraLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.939233379573485e-17 0 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_RShape" -p "IKLegHeel_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		-1.002780287e-16 -0.45161209270000002 -1.3086588599999999e-16
		-5.1353462759999998e-17 -0.31933797320000001 0.31933797320000001
		2.7653265189999997e-17 -1.3607953719999999e-16 0.45161209270000002
		9.0461085430000003e-17 0.31933797320000001 0.31933797320000001
		1.002780287e-16 0.45161209270000002 2.4256173589999999e-16
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKRollLegToe_R" -p "IKLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.18530220696570421 0.021196263244541935 4.8096847034785162 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.939233379573485e-17 0 ;
	setAttr ".s" -type "double3" 1.0000000000000009 1.0000000000000002 1.0000000000000009 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegToe_R" -p "IKRollLegToe_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0 1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999989 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegToe_R" -p "IKExtraLegToe_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegToe_RShape" -p "IKLegToe_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		-1.002780287e-16 -0.45161209270000002 -1.3086588599999999e-16
		-5.1353462759999998e-17 -0.31933797320000001 0.31933797320000001
		2.7653265189999997e-17 -1.3607953719999999e-16 0.45161209270000002
		9.0461085430000003e-17 0.31933797320000001 0.31933797320000001
		1.002780287e-16 0.45161209270000002 2.4256173589999999e-16
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKLiftToeLegToe_R" -p "IKLegToe_R";
	setAttr ".t" -type "double3" -0.010315669426050889 0.4073412889039475 -2.0507752079451049 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
createNode ikHandle -n "IKXLegHandleToe_R" -p "IKLiftToeLegToe_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0.010315819310472207 -0.40734119303136129 2.0507752257819312 ;
	setAttr ".r" -type "double3" 78.776762569158478 -0.57624218788987369 178.54917012388876 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr ".hs" 1;
	setAttr ".pv" -type "double3" -0.0091431410440529076 0 0.99995820061232987 ;
	setAttr ".roc" yes;
createNode transform -n "IKRollLegBall_R" -p "IKLegToe_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.010315646828939151 0.40734124421249929 -2.0507752164835402 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_R" -p "IKRollLegBall_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.9392333795734924e-17 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_R" -p "IKExtraLegBall_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_RShape" -p "IKLegBall_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-9.9600445170000006e-17 0.6017504897 -0.6017504897
		-2.4847825250000002e-16 -1.550498299e-16 -0.85100370380000001
		-3.6683134469999999e-16 -0.6017504897 -0.6017504897
		-3.8533008560000004e-16 -0.85100370380000001 -3.850084044e-16
		-2.9313816359999997e-16 -0.6017504897 0.6017504897
		-1.4426035630000001e-16 -1.9846354150000001e-16 0.85100370380000001
		-2.5907264070000001e-17 0.6017504897 0.6017504897
		-7.4085231850000001e-18 0.85100370380000001 2.027459622e-16
		-9.9600445170000006e-17 0.6017504897 -0.6017504897
		-2.4847825250000002e-16 -1.550498299e-16 -0.85100370380000001
		-3.6683134469999999e-16 -0.6017504897 -0.6017504897
		;
createNode transform -n "IKFootPivotBallReverseLeg_R" -p "IKLegBall_R";
	setAttr ".r" -type "double3" 0 -0.52387069249546392 0 ;
createNode ikHandle -n "IKXLegHandle_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -0.0056207035210977807 0.11671635558488114 -1.7429585009437858 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_R_poleVectorConstraint1" -p "IKXLegHandle_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.4264477999634444 -6.8328585490039888 -8.5752820529637983 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -2.2518098496959738e-08 4.4691445211242353e-08 8.7446840879579213e-09 ;
	setAttr ".r" -type "double3" 179.99999876606779 1.7669610520758223 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".hs" 1;
	setAttr ".roc" yes;
createNode transform -n "PoleAimLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 1 1.0000000000000004 1.0000000000000004 ;
createNode aimConstraint -n "PoleAimLeg_R_aimConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -sn "w0" -ln "IKLeg_RW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" 0.95846422788692864 -14.775483580870374 -93.75318418072041 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_R_pointConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "IKXHip_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -4.1346792435033128 10.284856319427492 -0.48711901530623414 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_R" -p "IKHandle";
createNode transform -n "PoleExtraLeg_R" -p "PoleParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_R" -p "PoleExtraLeg_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_RShape" -p "PoleLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		-1.6265306409999996e-32 0.32989960689999998 -7.3252427879999994e-17
		2.1975728359999999e-16 1.0987864179999999e-16 0.49484941030000001
		1.6265306409999996e-32 -0.32989960689999998 7.3252427879999994e-17
		8.1326532040000003e-33 -0.16494980340000001 3.6626213939999997e-17
		-2.1975728359999999e-16 -0.16494980340000001 -0.49484941030000001
		-2.1975728359999999e-16 0.16494980340000001 -0.49484941030000001
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		;
createNode transform -n "PoleAnnotateTargetLeg_R" -p "PoleLeg_R";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_RShape" -p "PoleAnnotateTargetLeg_R";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_R_parentConstraint1" -p "PoleParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_RStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" 0 4.4408920985006262e-16 0 ;
	setAttr ".tg[1].tot" -type "double3" 4.4960577532931811 -1.1441682890379692 -10.036658565977651 ;
	setAttr ".tg[1].tor" -type "double3" 14.805847264701455 2.2293399999236265e-14 93.628907075394281 ;
	setAttr ".lr" -type "double3" 6.9333463328605388e-08 -5.8889019375539808e-09 -2.2279546011690559e-08 ;
	setAttr ".rst" -type "double3" -5.5611270434667572 3.4519977704235032 -9.0624010682700344 ;
	setAttr ".rsrr" -type "double3" 7.951386703658784e-16 -1.1927080055488189e-14 7.9513867036587919e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintLeg_L" -p "IKHandle";
	setAttr ".t" -type "double3" 4.7740544294865037 0.53817582214507187 2.0891292062547282 ;
	setAttr ".ro" 3;
createNode transform -n "IKExtraLeg_L" -p "IKParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_L" -p "IKExtraLeg_L";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "toe" -ln "toe" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".toe";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
createNode nurbsCurve -n "IKLeg_LShape" -p "IKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		2.3619209590000052 -0.20097692930001151 2.6905080769999925
		-2.3619209589999945 -0.20097692930001151 2.6905080769999925
		-2.3619209589999945 0.99640672789998852 2.6905080769999925
		-1.5293311699999943 0.99640672789998852 -1.478531781000008
		-1.5293311699999943 -0.20097692930001151 -1.478531781000008
		1.5293311700000052 -0.20097692930001151 -1.478531781000008
		1.5293311700000052 0.99640672789998852 -1.478531781000008
		2.3619209590000052 0.99640672789998852 2.6905080769999925
		2.3619209590000052 -0.20097692930001151 2.6905080769999925
		1.5293311700000052 -0.20097692930001151 -1.478531781000008
		1.5293311700000052 0.99640672789998852 -1.478531781000008
		-1.5293311699999943 0.99640672789998852 -1.478531781000008
		-1.5230137679999949 -0.20097692930001151 -1.476848384000008
		-2.3521642729999948 -0.20097692930001151 2.6888246789999917
		-2.3619209589999945 0.99640672789998852 2.6905080769999925
		2.3619209590000052 0.99640672789998852 2.6905080769999925
		;
createNode transform -n "IKFootRollLeg_L" -p "IKLeg_L";
	setAttr ".r" -type "double3" 0 -0.52387069249546392 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "IKRollLegHeel_L" -p "IKFootRollLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.18530220696568733 -0.54525386304193435 -1.0159724497642122 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.9392333795734924e-17 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_L" -p "IKRollLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 1.7347234759768071e-18 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.939233379573485e-17 0 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_L" -p "IKExtraLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.9392333795734948e-17 0 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_LShape" -p "IKLegHeel_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		4.4408920985006262e-15 0.31933797320000135 -0.31933797319999935
		5.3290705182007514e-15 1.27675647831893e-15 -0.4516120926999993
		4.4408920985006262e-15 -0.31933797319999868 -0.31933797319999935
		5.3290705182007514e-15 -0.45161209269999875 2.2204460492503131e-16
		5.3290705182007514e-15 -0.31933797319999868 0.31933797320000035
		5.3290705182007514e-15 1.1917550279960665e-15 0.45161209270000002
		5.3290705182007514e-15 0.31933797320000135 0.31933797320000035
		5.3290705182007514e-15 0.45161209270000136 6.6613381477509392e-16
		4.4408920985006262e-15 0.31933797320000135 -0.31933797319999935
		5.3290705182007514e-15 1.27675647831893e-15 -0.4516120926999993
		4.4408920985006262e-15 -0.31933797319999868 -0.31933797319999935
		;
createNode transform -n "IKRollLegToe_L" -p "IKLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.18530220696570687 0.021196263244538938 4.8096847034785153 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.9392333795734924e-17 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999933 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegToe_L" -p "IKRollLegToe_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.9392333795734887e-17 0 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegToe_L" -p "IKExtraLegToe_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.9392333795734887e-17 0 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegToe_LShape" -p "IKLegToe_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.0658141036401504e-14 0.31933797320000434 -0.31933797319999879
		1.0658141036401504e-14 4.2778280917588063e-15 -0.45161209269999875
		1.0658141036401504e-14 -0.31933797319999568 -0.31933797319999879
		1.0658141036401504e-14 -0.45161209269999569 8.8817841970012523e-16
		1.0658141036401504e-14 -0.31933797319999568 0.31933797320000057
		1.0658141036401504e-14 4.1945613649119203e-15 0.45161209270000047
		1.0658141036401504e-14 0.31933797320000434 0.31933797320000057
		1.0658141036401504e-14 0.4516120927000043 8.8817841970012523e-16
		1.0658141036401504e-14 0.31933797320000434 -0.31933797319999879
		1.0658141036401504e-14 4.2778280917588063e-15 -0.45161209269999875
		1.0658141036401504e-14 -0.31933797319999568 -0.31933797319999879
		;
createNode transform -n "IKLiftToeLegToe_L" -p "IKLegToe_L";
	setAttr ".t" -type "double3" 0.010315669426058882 0.40734128890396615 -2.0507752079450952 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode ikHandle -n "IKXLegHandleToe_L" -p "IKLiftToeLegToe_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -0.010315819310470433 -0.40734119303135891 2.0507752257819334 ;
	setAttr ".r" -type "double3" 78.776762569158464 179.42375781211018 1.4508298761112486 ;
	setAttr ".s" -type "double3" 1.0000000000000009 1 1 ;
	setAttr ".hs" 1;
	setAttr ".pv" -type "double3" 0.0091431410440529076 0 0.99995820061232987 ;
	setAttr ".roc" yes;
createNode transform -n "IKRollLegBall_L" -p "IKLegToe_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.010315646828943592 0.40734124421250167 -2.0507752164835411 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.9392333795734887e-17 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_L" -p "IKRollLegBall_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.1102230246251563e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -1.9878466759146992e-16 0 ;
	setAttr ".s" -type "double3" 0.99999999999999933 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_L" -p "IKExtraLegBall_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 9.939233379573485e-17 0 ;
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_LShape" -p "IKLegBall_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.3290705182007514e-15 0.60175048970000167 -0.60175048969999789
		5.3290705182007514e-15 1.6653345369377348e-15 -0.85100370379999823
		5.3290705182007514e-15 -0.60175048969999811 -0.60175048969999789
		5.3290705182007514e-15 -0.85100370379999823 1.3322676295501878e-15
		5.3290705182007514e-15 -0.60175048969999811 0.60175048970000189
		5.3290705182007514e-15 1.609823385706477e-15 0.85100370380000223
		5.3290705182007514e-15 0.60175048970000167 0.60175048970000189
		5.3290705182007514e-15 0.85100370380000179 1.7763568394002505e-15
		5.3290705182007514e-15 0.60175048970000167 -0.60175048969999789
		5.3290705182007514e-15 1.6653345369377348e-15 -0.85100370379999823
		5.3290705182007514e-15 -0.60175048969999811 -0.60175048969999789
		;
createNode transform -n "IKFootPivotBallReverseLeg_L" -p "IKLegBall_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 0 ;
	setAttr ".r" -type "double3" 0 0.52387069249546381 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode ikHandle -n "IKXLegHandle_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0.0056207035211004452 0.1167163555848892 -1.7429585009437818 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_L_poleVectorConstraint1" -p "IKXLegHandle_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.4264477999634355 -6.8328585490039924 -8.5752820529637823 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 2.2518102937851836e-08 4.4691464362589528e-08 8.7446974106342168e-09 ;
	setAttr ".r" -type "double3" -1.2339322878667032e-06 -1.7669610520757566 -2.9635315136059198e-24 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr ".hs" 1;
	setAttr ".pv" -type "double3" -3.8518598887744717e-34 0 1 ;
	setAttr ".roc" yes;
createNode transform -n "PoleAimLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode aimConstraint -n "PoleAimLeg_L_aimConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -sn "w0" -ln "IKLeg_LW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -0.95846422788687935 -14.775483580870425 -86.24681581927959 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_L_pointConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "IKXHip_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 4.134679243503312 10.284856319427492 -0.48711901530623242 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "PoleExtraLeg_L" -p "PoleParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_L" -p "PoleExtraLeg_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_LShape" -p "PoleLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-9.0489882254018994e-09 0.16494988647402933 7.8597690134074569e-08
		-9.0489882254018994e-09 0.32989968997402919 7.8597690134074569e-08
		-9.0489882254018994e-09 8.3074029433305441e-08 0.49484948889769065
		-9.0489882254018994e-09 -0.32989952382597032 7.8597690134074569e-08
		-9.0489882254018994e-09 -0.16494972032597044 7.8597690134074569e-08
		-9.0489882254018994e-09 -0.16494972032597044 -0.49484933170231038
		-9.0489882254018994e-09 0.16494988647402933 -0.49484933170231038
		-9.0489882254018994e-09 0.16494988647402933 7.8597690134074569e-08
		;
createNode transform -n "PoleAnnotateTargetLeg_L" -p "PoleLeg_L";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_LShape" -p "PoleAnnotateTargetLeg_L";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_L_parentConstraint1" -p "PoleParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_LStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" 0 4.4408920985006262e-16 0 ;
	setAttr ".tg[1].tot" -type "double3" 4.4960577532931785 1.1441682890379514 -10.036658565977646 ;
	setAttr ".tg[1].tor" -type "double3" 14.805847264701503 2.4791798275012735e-14 86.371092924605733 ;
	setAttr ".lr" -type "double3" -9.1361036844084004e-07 -3.6207807710821199e-08 -1.3698437457430987e-07 ;
	setAttr ".rst" -type "double3" 5.5611270434667475 3.4519977704235005 -9.0624010682700149 ;
	setAttr ".rsrr" -type "double3" 7.951386703658784e-16 -1.1927080055488189e-14 7.9513867036587919e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKStatic" -p "IKSystem";
createNode transform -n "PoleParentConstraintLeg_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -5.5611270434667572 3.4519977704235032 -9.0624010682700327 ;
createNode transform -n "PoleParentConstraintLeg_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 5.5611270434667475 3.4519977704234996 -9.0624010682700149 ;
createNode transform -n "IKCrv" -p "IKSystem";
	setAttr ".it" no;
createNode transform -n "IKMessure" -p "IKSystem";
createNode transform -n "FKIKSystem" -p "MotionSystem";
createNode transform -n "FKIKParentConstraintLeg_R" -p "FKIKSystem";
createNode transform -n "FKIKLeg_R" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Hip";
	setAttr -l on ".middleJoint" -type "string" "Knee";
	setAttr -l on ".endJoint" -type "string" "Ankle";
createNode nurbsCurve -n "FKIKLeg_RShape" -p "FKIKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		-0.3080633183 -0.07701582958 -6.8403797810000007e-17
		-0.29706105700000002 -0.07701582958 -6.5960805029999992e-17
		-0.28605879560000003 -0.066013568209999998 -6.3517812250000002e-17
		-0.28605879560000003 0.066013568209999998 -6.3517812250000002e-17
		-0.29706105700000002 0.07701582958 -6.5960805029999992e-17
		-0.3080633183 0.07701582958 -6.8403797810000007e-17
		-0.3080633183 0.088018090950000002 -6.8403797810000007e-17
		-0.11002261369999999 0.088018090950000002 -2.4429927789999999e-17
		-0.11002261369999999 0.07701582958 -2.4429927789999999e-17
		-0.1210248751 0.07701582958 -2.6872920570000002e-17
		-0.13202713639999999 0.066013568209999998 -2.9315913349999996e-17
		-0.13202713639999999 0.01100226137 -2.9315913349999996e-17
		-0.066013568209999998 0.066013568209999998 -1.465795667e-17
		-0.07701582958 0.07701582958 -1.710094945e-17
		-0.088018090950000002 0.07701582958 -1.954394223e-17
		-0.088018090950000002 0.088018090950000002 -1.954394223e-17
		-0.022004522740000001 0.088018090950000002 -4.8859855579999998e-18
		-0.022004522740000001 0.07701582958 -4.8859855579999998e-18
		-0.03300678411 0.07701582958 -7.3289783369999998e-18
		-0.11002261369999999 0.01100226137 -2.4429927789999999e-17
		-0.022004522740000001 -0.07701582958 -4.8859855579999998e-18
		-0.01100226137 -0.07701582958 -2.4429927789999999e-18
		0.044009045480000002 0.088018090950000002 9.7719711159999997e-18
		0.209042966 0.088018090950000002 4.6416862800000002e-17
		0.209042966 0.07701582958 4.6416862800000002e-17
		0.19804070460000001 0.07701582958 4.3973870020000011e-17
		0.1870384433 0.066013568209999998 4.1530877239999996e-17
		0.1870384433 0.01100226137 4.1530877239999996e-17
		0.25305201150000001 0.066013568209999998 5.6188833920000001e-17
		0.24204975009999999 0.07701582958 5.3745841140000004e-17
		0.23104748880000001 0.07701582958 5.1302848360000001e-17
		0.23104748880000001 0.088018090950000002 5.1302848360000001e-17
		0.29706105700000002 0.088018090950000002 6.5960805029999992e-17
		0.29706105700000002 0.07701582958 6.5960805029999992e-17
		0.28605879560000003 0.07701582958 6.3517812250000002e-17
		0.209042966 0.01100226137 4.6416862800000002e-17
		0.29706105700000002 -0.07701582958 6.5960805029999992e-17
		0.3080633183 -0.07701582958 6.8403797810000007e-17
		0.3080633183 -0.088018090950000002 6.8403797810000007e-17
		0.24204975009999999 -0.088018090950000002 5.3745841140000004e-17
		0.24204975009999999 -0.07701582958 5.3745841140000004e-17
		0.25305201150000001 -0.07701582958 5.6188833920000001e-17
		0.1870384433 -0.01100226137 4.1530877239999996e-17
		0.1870384433 -0.066013568209999998 4.1530877239999996e-17
		0.19804070460000001 -0.07701582958 4.3973870020000011e-17
		0.209042966 -0.07701582958 4.6416862800000002e-17
		0.209042966 -0.088018090950000002 4.6416862800000002e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.1540316592 -0.07701582958 3.4201898910000001e-17
		0.16503392049999999 -0.066013568209999998 3.6644891679999997e-17
		0.16503392049999999 0.066013568209999998 3.6644891679999997e-17
		0.1540316592 0.07701582958 3.4201898910000001e-17
		0.1210248751 0.07701582958 2.6872920570000002e-17
		0.11002261369999999 0.066013568209999998 2.4429927789999999e-17
		0.11002261369999999 -0.066013568209999998 2.4429927789999999e-17
		0.1210248751 -0.07701582958 2.6872920570000002e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.066013568209999998 -0.088018090950000002 1.465795667e-17
		0.066013568209999998 -0.07701582958 1.465795667e-17
		0.07701582958 -0.07701582958 1.710094945e-17
		0.088018090950000002 -0.066013568209999998 1.954394223e-17
		0.088018090950000002 0.066013568209999998 1.954394223e-17
		0.07701582958 0.07701582958 1.710094945e-17
		0.055011306849999997 0.07701582958 1.2214963889999999e-17
		0 -0.07701582958 0
		0 -0.088018090950000002 0
		-0.07701582958 -0.088018090950000002 -1.710094945e-17
		-0.07701582958 -0.07701582958 -1.710094945e-17
		-0.066013568209999998 -0.07701582958 -1.465795667e-17
		-0.13202713639999999 -0.01100226137 -2.9315913349999996e-17
		-0.13202713639999999 -0.066013568209999998 -2.9315913349999996e-17
		-0.1210248751 -0.07701582958 -2.6872920570000002e-17
		-0.11002261369999999 -0.07701582958 -2.4429927789999999e-17
		-0.11002261369999999 -0.088018090950000002 -2.4429927789999999e-17
		-0.1760361819 -0.088018090950000002 -3.9087884459999999e-17
		-0.1760361819 -0.07701582958 -3.9087884459999999e-17
		-0.16503392049999999 -0.07701582958 -3.6644891679999997e-17
		-0.1540316592 -0.066013568209999998 -3.4201898910000001e-17
		-0.1540316592 0.066013568209999998 -3.4201898910000001e-17
		-0.16503392049999999 0.07701582958 -3.6644891679999997e-17
		-0.1870384433 0.07701582958 -4.1530877239999996e-17
		-0.1870384433 0.044009045480000002 -4.1530877239999996e-17
		-0.19804070460000001 0.044009045480000002 -4.3973870020000011e-17
		-0.19804070460000001 0.066013568209999998 -4.3973870020000011e-17
		-0.209042966 0.07701582958 -4.6416862800000002e-17
		-0.25305201150000001 0.07701582958 -5.6188833920000001e-17
		-0.2640542729 0.066013568209999998 -5.8631826699999991e-17
		-0.2640542729 0.01100226137 -5.8631826699999991e-17
		-0.23104748880000001 0.01100226137 -5.1302848360000001e-17
		-0.22004522739999999 0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 0.03300678411 -4.8859855579999998e-17
		-0.209042966 0.03300678411 -4.6416862800000002e-17
		-0.209042966 -0.022004522740000001 -4.6416862800000002e-17
		-0.22004522739999999 -0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 -0.01100226137 -4.8859855579999998e-17
		-0.23104748880000001 0 -5.1302848360000001e-17
		-0.2640542729 0 -5.8631826699999991e-17
		-0.2640542729 -0.066013568209999998 -5.8631826699999991e-17
		-0.25305201150000001 -0.07701582958 -5.6188833920000001e-17
		-0.24204975009999999 -0.07701582958 -5.3745841140000004e-17
		-0.24204975009999999 -0.088018090950000002 -5.3745841140000004e-17
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_R_parentConstraint1" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 2.5299643591669927 0.92878212344761524 0.30252742018122181 ;
	setAttr ".tg[0].tor" -type "double3" -154.8604209670936 1.3115279850922298e-13 -90.000000000000284 ;
	setAttr ".lr" -type "double3" -2.7257923641564898e-06 9.6318144766585015e-07 2.0524795888796987e-06 ;
	setAttr ".rst" -type "double3" -2.9089476050150109 7.7227836257187805 0.026382551730276926 ;
	setAttr ".rsrr" -type "double3" 1.2722218725854009e-14 1.1449996853268662e-13 -5.7249984266343283e-14 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintLeg_L" -p "FKIKSystem";
createNode transform -n "FKIKLeg_L" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Hip";
	setAttr -l on ".middleJoint" -type "string" "Knee";
	setAttr -l on ".endJoint" -type "string" "Ankle";
createNode nurbsCurve -n "FKIKLeg_LShape" -p "FKIKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		0.3080631359731596 -0.088018077919145732 3.1198528552614513e-07
		0.30806313636728833 -0.07701581654914591 3.1111776233050392e-07
		0.29706087506728895 -0.077015816155016736 3.1023971710716913e-07
		0.28605861406141786 -0.066013554390887741 3.084941486777848e-07
		0.28605861879096395 0.066013582029111895 2.9808387032662087e-07
		0.29706088058509295 0.077015843004982543 2.9809439236472279e-07
		0.30806314188509232 0.077015842610854257 2.9897243758805758e-07
		0.30806314227922105 0.088018103980854079 2.9810491439241643e-07
		0.11002243767922204 0.088018111075172101 2.8230010025095953e-07
		0.11002243728509332 0.077015849705171391 2.8316762344660074e-07
		0.12102469868509312 0.077015849311043993 2.8404566867687442e-07
		0.13202695959096469 0.066013587546914998 2.8579123709931986e-07
		0.13202695762032057 0.011002280706915002 2.9012885308099534e-07
		0.066013391400964405 0.066013589911687376 2.8052296571767776e-07
		0.077015653165093415 0.07701585088755801 2.8053348775231024e-07
		0.088017914535093222 0.077015850493429738 2.8141153298605337e-07
		0.088017914929222396 0.08801811186342956 2.8054400979041216e-07
		0.022004346719222138 0.088018114228201938 2.7527573840877007e-07
		0.022004346325093405 0.077015852858202116 2.7614326160441127e-07
		0.033006607695093226 0.077015852464072942 2.7702130683121551e-07
		0.11002243492032047 0.011002281495171571 2.8837276262044803e-07
		0.022004340807289413 -0.077015806301797163 2.8828858634338816e-07
		0.011002079437290035 -0.077015805907668877 2.8741054111311448e-07
		-0.04400922150077724 0.088018116592975204 2.7000746702365852e-07
		-0.20904314202077681 0.088018122504907481 2.5683678856955328e-07
		-0.20904314241490551 0.077015861134906771 2.5770431176519448e-07
		-0.19804088101490569 0.077015860740778486 2.5858235699893761e-07
		-0.18703862010903416 0.06601359897664949 2.6032792541791361e-07
		-0.18703862207967825 0.011002292136649494 2.6466554139611964e-07
		-0.25305218830903442 0.066013601341421868 2.5505965403627151e-07
		-0.24204992651490545 0.077015862317292516 2.5507017607437343e-07
		-0.23104766521490561 0.077015861923165119 2.5594822130117767e-07
		-0.23104766482077688 0.08801812329316494 2.5508069810206707e-07
		-0.29706123302077625 0.08801812565793643 2.4981242672042492e-07
		-0.29706123341490498 0.077015864287936608 2.5067994991953557e-07
		-0.28605897201490516 0.077015863893808323 2.515579951532787e-07
		-0.20904314477967831 0.011002292924906952 2.6290945093904172e-07
		-0.29706123893270897 -0.077015794872062671 2.6282527466198191e-07
		-0.30806350023270879 -0.077015794477934385 2.6194722943517768e-07
		-0.30806350062683752 -0.088018055847934207 2.6281475263081888e-07
		-0.24204993242683817 -0.088018058212706599 2.6808302401246098e-07
		-0.24204993203270939 -0.077015796842706763 2.6721550081335033e-07
		-0.25305219343270924 -0.077015796448578477 2.6633745558307664e-07
		-0.1870386228679366 -0.011002230603350148 2.664005877908715e-07
		-0.18703862483858069 -0.066013537443350145 2.7073820376907753e-07
		-0.19804088653270924 -0.077015798419221682 2.7072768173791451e-07
		-0.20904314793270953 -0.077015798025092508 2.6984963650417138e-07
		-0.20904314832683824 -0.08801805939509233 2.7071715970328203e-07
		-0.13202731872683815 -0.088018062153993881 2.7686347631519781e-07
		-0.13202731833270942 -0.077015800783994059 2.759959531195566e-07
		-0.15403184113270951 -0.0770157999957366 2.7423986265553979e-07
		-0.1650341020385806 -0.066013538231607605 2.7249429423309435e-07
		-0.16503409730903451 0.066013598188392031 2.6208401588539987e-07
		-0.15403183561490552 0.077015859164263567 2.6209453791309345e-07
		-0.12102505151490606 0.077015857981876934 2.647286736039145e-07
		-0.11002279050903496 0.066013596217747939 2.6647424203329884e-07
		-0.11002279523858104 -0.066013540202251697 2.7688452038099332e-07
		-0.1210250570327096 -0.077015801178123233 2.7687399834636084e-07
		-0.13202731833270942 -0.077015800783994059 2.759959531195566e-07
		-0.13202731872683815 -0.088018062153993881 2.7686347631519781e-07
		-0.066013750536838778 -0.088018064518767147 2.8213174769683991e-07
		-0.066013750142710048 -0.077015803148767326 2.812642245011987e-07
		-0.07701601151270987 -0.077015802754638138 2.8038617927092502e-07
		-0.088018272488580962 -0.066013540990509156 2.7864061084154068e-07
		-0.088018267759034874 0.066013595429490479 2.682303324938462e-07
		-0.077016005994905878 0.077015856405361113 2.6824085452847868e-07
		-0.055011483264906243 0.077015855617103668 2.6999694498902604e-07
		-1.8193270978628109e-07 -0.07701580551353969 2.865324958828408e-07
		-1.8232683851593381e-07 -0.088018066883539525 2.8740001907848201e-07
		0.077015647253160666 -0.088018069642441965 2.9354633569039779e-07
		0.077015647647289409 -0.077015808272441241 2.9267881249475658e-07
		0.066013386277289587 -0.07701580787831297 2.918007672644829e-07
		0.13202695683206225 -0.011002242033084642 2.9186389947227775e-07
		0.13202695486141858 -0.066013548873084638 2.9620151544701434e-07
		0.1210246931672896 -0.077015809848956174 2.9619099341932076e-07
		0.11002243176728976 -0.077015809454827888 2.9531294818904708e-07
		0.1100224313731606 -0.08801807082482771 2.9618047138468828e-07
		0.17603599957316041 -0.088018073189600102 3.0144874276633038e-07
		0.17603599996728914 -0.077015811819600266 3.0058121957068917e-07
		0.16503373856728931 -0.077015811425471092 2.997031743334766e-07
		0.15403147766141825 -0.066013549661342097 2.979576059145006e-07
		0.15403148239096431 0.066013586758657539 2.8754732756333667e-07
		0.16503374408509286 0.077015847734528187 2.875578495944997e-07
		0.18703826688509295 0.077015846946270713 2.8931394005851652e-07
		0.18703826570270632 0.044009062846271263 2.9191650964544019e-07
		0.19804052700270655 0.044009062452142089 2.9279455486877493e-07
		0.19804052779096404 0.066013585182141732 2.9105950848096196e-07
		0.20904278958509304 0.077015846158013268 2.9107003051906388e-07
		0.25305183508509277 0.077015844581498349 2.9458221144015861e-07
		0.26405409609096386 0.066013582817369354 2.9632777986954295e-07
		0.26405409412031977 0.011002275977369358 3.0066539584774893e-07
		0.23104731002031983 0.011002277159755993 2.9803126015692794e-07
		0.22004504901444877 0.022004538923884983 2.9628569173101305e-07
		0.2200450494085775 0.033006800293884808 2.9541816853190239e-07
		0.20904278800857767 0.033006800688013094 2.9454012330162871e-07
		0.20904278603793311 -0.022004506161986903 2.9887773927983474e-07
		0.22004504743793296 -0.022004506556115189 2.9975578451357787e-07
		0.22004504783206169 -0.011002245186115369 2.9888826131793667e-07
		0.23104730962619069 1.5789756169226621e-08 2.9889878335256914e-07
		0.2640540937261906 1.4607369536179249e-08 3.0153291904339019e-07
		0.26405409136141778 -0.066013553602630282 3.0673805822070688e-07
		0.25305182956728922 -0.077015814578501818 3.0672753618260495e-07
		0.24204956816728895 -0.077015814184372644 3.0584949094886182e-07
		0.24204956777316025 -0.088018075554372466 3.0671701414450303e-07
		0.3080631359731596 -0.088018077919145732 3.1198528552614513e-07
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_L_parentConstraint1" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -2.5299643591669891 -0.92878212344761635 -0.30252742018121559 ;
	setAttr ".tg[0].tor" -type "double3" 25.139579032906383 1.0412922365280961e-13 -90.000000000000213 ;
	setAttr ".lr" -type "double3" 1.7919534155983632e-06 4.5725404653730651e-06 3.5869063703863209e-07 ;
	setAttr ".rst" -type "double3" 2.9089476050150096 7.7227836257187876 0.026382551730280812 ;
	setAttr ".rsrr" -type "double3" 1.5902773407317205e-15 -9.2236085762441977e-14 4.6913181551586881e-14 ;
	setAttr -k on ".w0";
createNode transform -n "RootSystem" -p "MotionSystem";
createNode transform -n "PelvisCenter_M" -p "RootSystem";
	setAttr ".t" -type "double3" 0 9.3382860296854506 0.55075592595768108 ;
	setAttr ".ro" 3;
createNode transform -n "PelvisCenterBtwLegs_M" -p "RootSystem";
	setAttr ".t" -type "double3" -2.6645352591003757e-15 9.3382860296854506 2.0891292062547242 ;
	setAttr ".ro" 3;
createNode orientConstraint -n "PelvisCenterBtwLegs_M_orientConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "PelvisCenterBtwLegs_M_pointConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -2.6645352591003757e-15 0.5381758221450661 2.0891292062547242 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PelvisCenterBtwLegsOffset_M" -p "PelvisCenterBtwLegs_M";
	setAttr ".t" -type "double3" 2.6645352591003757e-15 0 -1.5383732802970429 ;
	setAttr ".ro" 3;
createNode transform -n "GlobalSystem" -p "MotionSystem";
createNode transform -n "GlobalOffsetHead_M" -p "GlobalSystem";
	setAttr ".t" -type "double3" 0 11.648579811101406 0.060641934026581357 ;
createNode transform -n "GlobalHead_M" -p "GlobalOffsetHead_M";
	setAttr ".ro" 5;
createNode transform -n "GameSkeleton" -p "Main";
createNode joint -n "Pelvis_M" -p "GameSkeleton";
	setAttr ".ro" 3;
createNode joint -n "HipTwist_R" -p "Pelvis_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 205.1395790329064 89.999999999999702 ;
createNode joint -n "Hip_R" -p "HipTwist_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 35.053931728105312 -4.5637958222710635 -85.563283529155981 ;
createNode joint -n "Knee_R" -p "Hip_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -63.769337864119137 -0.47407648659921214 -5.8215609805559323 ;
createNode joint -n "Ankle_R" -p "Knee_R";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 53.614411016936451 0.067470119883889568 -0.13352636183798375 ;
createNode joint -n "MiddleToe1_R" -p "Ankle_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 78.745287469947726 180.66645448100903 1.4508556180935599 ;
createNode joint -n "MiddleToe2_End_R" -p "MiddleToe1_R";
	setAttr ".t" -type "double3" 2.9309887850104133e-14 2.090864005704145 6.4392935428259079e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -8.2480775474559689 0.057116226054674535 0.74000483750698332 ;
createNode parentConstraint -n "MiddleToe1_R_parentConstraint1" -p "MiddleToe1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleToe1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 5.3937064995677616e-06 6.3421956191184496e-07 4.9139507666314404e-06 ;
	setAttr ".rst" -type "double3" -0.048125071465985769 0.116716348415981 -1.7423030526541006 ;
	setAttr ".rsrr" -type "double3" 3.2317447591670764e-07 -9.9557542226970611e-09 4.9168845506853415e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Ankle_R_parentConstraint1" -p "Ankle_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXAnkle_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXAnkle_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 9.9443610935884967e-06 1.4731322174771102e-05 2.5378898314518569e-06 ;
	setAttr ".rst" -type "double3" -0.042058917421928577 6.9248593607185853 0.36347882369533829 ;
	setAttr ".rsrr" -type "double3" -2.6832243680692541e-06 3.525375739584904e-07 -2.5959461577564678e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Knee_R_parentConstraint1" -p "Knee_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXKnee_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXKnee_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.4951473504334027e-06 -1.808388333030294e-07 -1.3298000728063369e-08 ;
	setAttr ".rst" -type "double3" -0.078236429721319389 5.7960343688259126 1.8108362405890603 ;
	setAttr ".rsrr" -type "double3" -1.560613563377247e-06 2.3855851728683486e-06 -1.19646954607882e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ElbowShield_R" -p "Hip_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 11.845804730726796 -0.47407648659921292 -5.8215609805559607 ;
createNode joint -n "ElbowShield_End_R" -p "ElbowShield_R";
	setAttr ".t" -type "double3" 7.1054273576010019e-15 2.0399706718155821 0.13654328049844189 ;
	setAttr ".ro" 2;
createNode parentConstraint -n "ElbowShield_R_parentConstraint1" -p "ElbowShield_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbowShield_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 8.7009094160745378e-08 5.8174318093029844e-08 -8.976199999228347e-08 ;
	setAttr ".rst" -type "double3" 0.18317404968246451 6.5338547349225902 2.1275027892559959 ;
	setAttr ".rsrr" -type "double3" 8.7009079605980517e-08 5.8174311036174109e-08 -8.9762038581357065e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Hip_R_parentConstraint1" -p "Hip_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHip_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXHip_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -3.4986101496098631e-14 1.1609024587341838e-13 -4.9298597562684546e-14 ;
	setAttr ".rst" -type "double3" -0.0075656181532579581 2.1545137650664667 -0.32104435487134753 ;
	setAttr ".rsrr" -type "double3" -1.47650661438339e-06 -6.2499004431436732e-08 -2.6152910912908145e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "HipTwist_R_parentConstraint1" -p "HipTwist_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.0639648514587052e-06 2.725792393753111e-06 -2.2672430427380352e-06 ;
	setAttr ".rst" -type "double3" -1.9801654815673828 0.80333365262656287 -1.3252944876594568 ;
	setAttr ".rsrr" -type "double3" 1.0639648530489828e-06 2.7257923603572866e-06 -2.2672430872658017e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Head_M" -p "Pelvis_M";
	setAttr ".ro" 5;
createNode joint -n "Head_End_M" -p "Head_M";
	setAttr ".t" -type "double3" 4.0601869640127816e-16 2.3178568510379023 2.248201624865942e-15 ;
createNode joint -n "Jaw_M" -p "Head_M";
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
createNode joint -n "Jaw_End_M" -p "Jaw_M";
	setAttr ".t" -type "double3" 0 4.6908618918588001 -8.8817841970012523e-15 ;
	setAttr ".jo" -type "double3" -89.999999999999986 0 0 ;
createNode parentConstraint -n "Jaw_M_parentConstraint1" -p "Jaw_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXJaw_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.5044781545431972e-06 0 0 ;
	setAttr ".rst" -type "double3" 0 -0.79810682069005345 1.0250995629197761 ;
	setAttr ".rsrr" -type "double3" 2.5044781545431972e-06 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Head_M_parentConstraint1" -p "Head_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXHead_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 2.3102937814159556 -0.49011399193109967 ;
	setAttr -k on ".w0";
createNode joint -n "Shoulder_R" -p "Pelvis_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -152.00531705221945 4.9505588210855853 -11.904809842065401 ;
createNode joint -n "Elbow_R" -p "Shoulder_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -129.97952417590869 1.4902849999290686 0.83989225685842606 ;
createNode joint -n "Fingers_R" -p "Elbow_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 36.446714387517368 -2.2310450883288184 -1.1437535407680832 ;
createNode joint -n "Fingers_End_R" -p "Fingers_R";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 1.7859488381795037 1.2434497875801752e-14 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "Fingers_R_parentConstraint1" -p "Fingers_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXFingers_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.2296816747098999e-06 3.0326077443597128e-07 2.6830877143647141e-08 ;
	setAttr ".rst" -type "double3" 0.12737201325339689 3.5950003915528899 0.3435456687201679 ;
	setAttr ".rsrr" -type "double3" 4.6977841375294198e-06 1.91663084865281e-07 -1.7954642820691848e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Elbow_R_parentConstraint1" -p "Elbow_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.5156647710962205e-07 1.5173396510619981e-07 1.4082184994072506e-07 ;
	setAttr ".rst" -type "double3" -0.17735107422212426 3.5019857716095619 -0.057058433312651591 ;
	setAttr ".rsrr" -type "double3" 3.2229138102270438e-06 1.1520613002947429e-07 4.3699261425226144e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Shoulder_R_parentConstraint1" -p "Shoulder_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 3.4760294306938255e-06 5.8931764157778699e-08 -1.9724269194177163e-07 ;
	setAttr ".rst" -type "double3" -2.3090522421826121 -0.86558199877464581 0.48592887005093366 ;
	setAttr ".rsrr" -type "double3" 3.47602940497606e-06 5.8931765350486718e-08 -1.9724269353204899e-07 ;
	setAttr -k on ".w0";
createNode joint -n "HipTwist_L" -p "Pelvis_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.7566776614494369e-15 25.139579032906383 90.000000000000242 ;
createNode joint -n "Hip_L" -p "HipTwist_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 35.053931728105269 -4.5637958222710449 -85.563283529155996 ;
createNode joint -n "Knee_L" -p "Hip_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -63.769337864119038 -0.47407648659920537 -5.8215609805559403 ;
createNode joint -n "Ankle_L" -p "Knee_L";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 53.614411016936423 0.067470119883889318 -0.13352636183798422 ;
createNode joint -n "MiddleToe1_L" -p "Ankle_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 78.745287469947769 180.66645448100903 1.4508556180935599 ;
createNode joint -n "MiddleToe2_End_L" -p "MiddleToe1_L";
	setAttr ".t" -type "double3" -2.6645352591003757e-14 -2.0908640057041485 -4.6629367034256575e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -8.2480775474560453 0.05711622605467425 0.74000483750696677 ;
createNode parentConstraint -n "MiddleToe1_L_parentConstraint1" -p "MiddleToe1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleToe1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -4.7955324746395657e-06 -4.9660151163165387e-07 4.9523622697854973e-06 ;
	setAttr ".rst" -type "double3" 0.048125071465985769 -0.11671634841598028 1.7423030526540986 ;
	setAttr ".rsrr" -type "double3" 3.2317444411116437e-07 -9.9558341341347677e-09 4.916884560624576e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Ankle_L_parentConstraint1" -p "Ankle_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXAnkle_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXAnkle_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.1365612054544598e-05 -1.1856095096817951e-05 -1.6053443727528968e-06 ;
	setAttr ".rst" -type "double3" 0.04205925684885603 -6.924858983916196 -0.363479155860019 ;
	setAttr ".rsrr" -type "double3" -3.8128352439818511e-07 2.6388079255262e-06 -1.2346932785066131e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Knee_L_parentConstraint1" -p "Knee_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXKnee_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXKnee_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 9.5597127120104656e-07 3.8177801588100148e-07 3.7112456776662146e-07 ;
	setAttr ".rst" -type "double3" 0.078236465086846785 -5.7960342604579633 -1.8108365871013976 ;
	setAttr ".rsrr" -type "double3" 7.4504434449623265e-07 2.6405985679831411e-06 1.448881436212326e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ElbowShield_L" -p "Hip_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 11.845804730726845 -0.47407648659921015 -5.8215609805559634 ;
createNode joint -n "ElbowShield_End_L" -p "ElbowShield_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -2.0399706718155848 -0.13654328049844455 ;
	setAttr ".ro" 2;
createNode parentConstraint -n "ElbowShield_L_parentConstraint1" -p "ElbowShield_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbowShield_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 8.700904845890789e-08 5.8174303581749126e-08 -8.9762012751774362e-08 ;
	setAttr ".rst" -type "double3" -0.18317404968246631 -6.5338547349225919 -2.1275027892559963 ;
	setAttr ".rsrr" -type "double3" 8.7009042371127448e-08 5.8174291555276722e-08 -8.9762033189323028e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Hip_L_parentConstraint1" -p "Hip_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHip_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXHip_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -9.7064958177335713e-15 -3.1936162955891535e-06 1.4776831613593445e-06 ;
	setAttr ".rst" -type "double3" 0.0075656907521111094 -2.1545137729564567 0.32104430021111563 ;
	setAttr ".rsrr" -type "double3" 1.0615617322735478e-06 2.1772645962204994e-06 -1.6557906856579183e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "HipTwist_L_parentConstraint1" -p "HipTwist_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 3.9870285645229463e-06 -1.7919533208102199e-06 2.2672426641864098e-06 ;
	setAttr ".rst" -type "double3" 1.9801654815673837 0.80333365262656109 -1.3252944876594546 ;
	setAttr ".rsrr" -type "double3" 3.9870285692937797e-06 -1.7919533239907746e-06 2.2672426653791186e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Shoulder_L" -p "Pelvis_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 27.994682947780603 -4.9505588210855906 11.904809842065388 ;
createNode joint -n "Elbow_L" -p "Shoulder_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -129.97952417590878 1.4902849999290819 0.83989225685842261 ;
createNode joint -n "Fingers_L" -p "Elbow_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 36.446714387517325 -2.2310450883288286 -1.143753540768079 ;
createNode joint -n "Fingers_End_L" -p "Fingers_L";
	setAttr ".t" -type "double3" 7.1054273576010019e-15 -1.7859488381795086 -1.6875389974302379e-14 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "Fingers_L_parentConstraint1" -p "Fingers_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXFingers_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.229681795352004e-06 3.032607695905949e-07 2.6830865216566817e-08 ;
	setAttr ".rst" -type "double3" -0.12737201315512614 -3.5950004372428115 -0.34354564802055609 ;
	setAttr ".rsrr" -type "double3" 3.8459575909347549e-06 2.1907342662396253e-07 -1.2885656219237018e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Elbow_L_parentConstraint1" -p "Elbow_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.5156640083299437e-07 1.5173396073293723e-07 1.408218462880567e-07 ;
	setAttr ".rst" -type "double3" 0.17735107422212604 -3.5019857707593225 0.057058485496309874 ;
	setAttr ".rsrr" -type "double3" 2.3695207130147087e-06 1.2417800025592996e-07 6.7554265005946865e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Shoulder_L_parentConstraint1" -p "Shoulder_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.6222557198620634e-06 5.893177834957019e-08 -1.9724268852092608e-07 ;
	setAttr ".rst" -type "double3" 2.3090522421826103 -0.86558199877464748 0.48592887005093566 ;
	setAttr ".rsrr" -type "double3" 2.6222557263722612e-06 5.8931779144708916e-08 -1.9724268553915607e-07 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "Pelvis_M_pointConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 9.3382860296854506 0.55075592595768108 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "Pelvis_M_orientConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode nurbsCurve -n "forwardArrowShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-0.27367638803636346 6.0768365458843865e-17 2.2537926940380917
		-0.54735277607272692 1.2153673091768773e-16 2.2537926940380917
		-1.8230509637653166e-16 3.6461019275306341e-16 3.0748218581471818
		0.54735277607272692 -1.2153673091768773e-16 2.2537926940380917
		0.27367638803636346 -6.0768365458843865e-17 2.2537926940380917
		0.27367638803636368 -4.2537855821190726e-16 1.4327635299290016
		-0.27367638803636329 -3.0384182729421956e-16 1.4327635299290016
		-0.27367638803636346 6.0768365458843865e-17 2.2537926940380917
		;
createNode transform -n "items" -p "ctrl_rig";
	setAttr -l on ".it" no;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr ".cm" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".cs" 3;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode shadingEngine -n "irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode shadingEngine -n "BPRig_irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "BPRig_materialInfo3";
createNode ikSCsolver -n "ikSCsolver";
createNode ikRPsolver -n "ikRPsolver";
createNode displayLayer -n "jointLayer";
createNode reverse -n "jointVisReverse";
createNode objectSet -n "ControlSet";
	setAttr ".ihi" 0;
	setAttr -s 65 ".dsm";
createNode objectSet -n "GameSet";
	setAttr ".ihi" 0;
	setAttr -s 21 ".dsm";
createNode objectSet -n "AllSet";
	setAttr ".ihi" 0;
	setAttr -s 314 ".dsm";
	setAttr -s 46 ".dnsm";
createNode objectSet -n "Sets";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dnsm";
createNode unitConversion -n "unitConversion1";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_R";
createNode condition -n "FKIKBlendLegCondition_R";
createNode setRange -n "FKIKBlendLegsetRange_R";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode unitConversion -n "IKLiftToeLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode multiplyDivide -n "Leg_RAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_R";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion4";
	setAttr ".cf" 0.017453292519943295;
createNode blendTwoAttr -n "IKBallToFKBallMiddleToe1blendTwoAttr_R";
	setAttr -s 2 ".i[0:1]"  101.25684410576535 101.25683902660606;
createNode unitConversion -n "unitConversion5";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion6";
	setAttr ".cf" 0.017453292519943295;
createNode blendColors -n "ScaleBlendAnkle_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendKnee_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendHip_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode unitConversion -n "GlobalHead_unitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "GlobalHead_reverse_M";
createNode unitConversion -n "unitConversion7";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_L";
createNode condition -n "FKIKBlendLegCondition_L";
createNode setRange -n "FKIKBlendLegsetRange_L";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode unitConversion -n "IKLiftToeLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode multiplyDivide -n "Leg_LAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_L";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion8";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion9";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion10";
	setAttr ".cf" 0.017453292519943295;
createNode blendTwoAttr -n "IKBallToFKBallMiddleToe1blendTwoAttr_L";
	setAttr -s 2 ".i[0:1]"  101.25684410576535 101.25684923124025;
createNode unitConversion -n "unitConversion11";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion12";
	setAttr ".cf" 0.017453292519943295;
createNode blendColors -n "ScaleBlendAnkle_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendKnee_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendHip_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode setRange -n "CenterBtwFeet_M";
	setAttr ".v" -type "float3" 0 1 0 ;
	setAttr ".n" -type "float3" 2 0 0 ;
	setAttr ".om" -type "float3" 0.1 0 0 ;
createNode unitConversion -n "PelvisCenterBtwFeetUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "PelvisCenterBtwFeetReverse_M";
createNode dagPose -n "buildPose";
	addAttr -ci true -sn "udAttr" -ln "udAttr" -dt "string";
	setAttr ".udAttr" -type "string" (
		"setAttr FKExtraHead_M.translateX 0;setAttr FKExtraHead_M.translateY 0;setAttr FKExtraHead_M.translateZ 0;setAttr FKExtraHead_M.rotateX -0;setAttr FKExtraHead_M.rotateY 0;setAttr FKExtraHead_M.rotateZ -0;setAttr FKExtraHead_M.scaleX 1;setAttr FKExtraHead_M.scaleY 1;setAttr FKExtraHead_M.scaleZ 1;setAttr FKHead_M.translateX 0;setAttr FKHead_M.translateY 0;setAttr FKHead_M.translateZ 0;setAttr FKHead_M.rotateX -0;setAttr FKHead_M.rotateY 0;setAttr FKHead_M.rotateZ -0;setAttr FKHead_M.scaleX 1;setAttr FKHead_M.scaleY 1;setAttr FKHead_M.scaleZ 1;setAttr FKHead_M.Global 10;setAttr FKExtraFingers_R.translateX -8.881784197e-16;setAttr FKExtraFingers_R.translateY -1.110223025e-16;setAttr FKExtraFingers_R.translateZ -8.881784197e-16;setAttr FKExtraFingers_R.rotateX -0;setAttr FKExtraFingers_R.rotateY 0;setAttr FKExtraFingers_R.rotateZ -0;setAttr FKExtraFingers_R.scaleX 1;setAttr FKExtraFingers_R.scaleY 1;setAttr FKExtraFingers_R.scaleZ 1;setAttr FKFingers_R.translateX 8.881784197e-16;setAttr FKFingers_R.translateY -4.440892099e-16;setAttr FKFingers_R.translateZ -8.881784197e-16;setAttr FKFingers_R.rotateX -0;setAttr FKFingers_R.rotateY 0;setAttr FKFingers_R.rotateZ -0;setAttr FKFingers_R.scaleX 1;setAttr FKFingers_R.scaleY 1;setAttr FKFingers_R.scaleZ 1;setAttr FKExtraElbow_R.translateX -8.881784197e-16;setAttr FKExtraElbow_R.translateY 5.551115123e-17;setAttr FKExtraElbow_R.translateZ 0;setAttr FKExtraElbow_R.rotateX -0;setAttr FKExtraElbow_R.rotateY 0;setAttr FKExtraElbow_R.rotateZ -0;setAttr FKExtraElbow_R.scaleX 1;setAttr FKExtraElbow_R.scaleY 1;setAttr FKExtraElbow_R.scaleZ 1;setAttr FKElbow_R.translateX -8.881784197e-16;setAttr FKElbow_R.translateY 1.387778781e-16;setAttr FKElbow_R.translateZ 0;setAttr FKElbow_R.rotateX -0;setAttr FKElbow_R.rotateY 0;setAttr FKElbow_R.rotateZ -0;setAttr FKElbow_R.scaleX 1;setAttr FKElbow_R.scaleY 1;setAttr FKElbow_R.scaleZ 1;setAttr FKExtraShoulder_R.translateX 0;setAttr FKExtraShoulder_R.translateY 8.881784197e-16;setAttr FKExtraShoulder_R.translateZ 0;setAttr FKExtraShoulder_R.rotateX -0;setAttr FKExtraShoulder_R.rotateY 0;setAttr FKExtraShoulder_R.rotateZ -0;setAttr FKExtraShoulder_R.scaleX 1;setAttr FKExtraShoulder_R.scaleY 1;setAttr FKExtraShoulder_R.scaleZ 1;setAttr FKShoulder_R.translateX 0;setAttr FKShoulder_R.translateY 1.776356839e-15;setAttr FKShoulder_R.translateZ 4.440892099e-16;setAttr FKShoulder_R.rotateX -0;setAttr FKShoulder_R.rotateY 0;setAttr FKShoulder_R.rotateZ -0;setAttr FKShoulder_R.scaleX 1;setAttr FKShoulder_R.scaleY 1;setAttr FKShoulder_R.scaleZ 1;setAttr FKExtraMiddleToe1_L.translateX 1.776356839e-15;setAttr FKExtraMiddleToe1_L.translateY 8.881784197e-16;setAttr FKExtraMiddleToe1_L.translateZ 0;setAttr FKExtraMiddleToe1_L.rotateX -3.816665618e-14;setAttr FKExtraMiddleToe1_L.rotateY -8.001082871e-15;setAttr FKExtraMiddleToe1_L.rotateZ -1.649912741e-14;setAttr FKExtraMiddleToe1_L.scaleX 1;setAttr FKExtraMiddleToe1_L.scaleY 1;setAttr FKExtraMiddleToe1_L.scaleZ 1;setAttr FKMiddleToe1_L.translateX 8.881784197e-16;setAttr FKMiddleToe1_L.translateY 4.440892099e-16;setAttr FKMiddleToe1_L.translateZ 4.440892099e-16;setAttr FKMiddleToe1_L.rotateX -0;setAttr FKMiddleToe1_L.rotateY 0;setAttr FKMiddleToe1_L.rotateZ -0;setAttr FKMiddleToe1_L.scaleX 1;setAttr FKMiddleToe1_L.scaleY 1;setAttr FKMiddleToe1_L.scaleZ 1;setAttr FKExtraAnkle_L.translateX 0;setAttr FKExtraAnkle_L.translateY 0;setAttr FKExtraAnkle_L.translateZ 0;setAttr FKExtraAnkle_L.rotateX -0;setAttr FKExtraAnkle_L.rotateY 7.951386704e-16;setAttr FKExtraAnkle_L.rotateZ 0;setAttr FKExtraAnkle_L.scaleX 1;setAttr FKExtraAnkle_L.scaleY 1;setAttr FKExtraAnkle_L.scaleZ 1;setAttr FKAnkle_L.translateX -8.881784197e-16;setAttr FKAnkle_L.translateY 0;setAttr FKAnkle_L.translateZ 0;setAttr FKAnkle_L.rotateX -0;setAttr FKAnkle_L.rotateY -0;setAttr FKAnkle_L.rotateZ 0;setAttr FKAnkle_L.scaleX 1;setAttr FKAnkle_L.scaleY 1;setAttr FKAnkle_L.scaleZ 1;setAttr FKExtraKnee_L.translateX 0;setAttr FKExtraKnee_L.translateY 0;setAttr FKExtraKnee_L.translateZ 6.661338148e-16;setAttr FKExtraKnee_L.rotateX -2.544754346e-14;setAttr FKExtraKnee_L.rotateY -3.478731683e-16;setAttr FKExtraKnee_L.rotateZ -1.987846676e-16;setAttr FKExtraKnee_L.scaleX 1;setAttr FKExtraKnee_L.scaleY 1;setAttr FKExtraKnee_L.scaleZ 1;setAttr FKKnee_L.translateX 8.881784197e-16;setAttr FKKnee_L.translateY 0;setAttr FKKnee_L.translateZ 8.881784197e-16;setAttr FKKnee_L.rotateX -0;setAttr FKKnee_L.rotateY 0;setAttr FKKnee_L.rotateZ 0;setAttr FKKnee_L.scaleX 1;setAttr FKKnee_L.scaleY 1;setAttr FKKnee_L.scaleZ 1;setAttr FKExtraElbowShield_L.translateX 0;setAttr FKExtraElbowShield_L.translateY 0;setAttr FKExtraElbowShield_L.translateZ 8.881784197e-16;setAttr FKExtraElbowShield_L.rotateX -9.557194097e-15;setAttr FKExtraElbowShield_L.rotateY -7.454425035e-16;setAttr FKExtraElbowShield_L.rotateZ -6.460501697e-16;setAttr FKExtraElbowShield_L.scaleX 1;setAttr FKExtraElbowShield_L.scaleY 1;setAttr FKExtraElbowShield_L.scaleZ 1;setAttr FKElbowShield_L.translateX -8.881784197e-16;setAttr FKElbowShield_L.translateY 4.440892099e-16;setAttr FKElbowShield_L.translateZ 0;setAttr FKElbowShield_L.rotateX -0;setAttr FKElbowShield_L.rotateY 0;setAttr FKElbowShield_L.rotateZ 0;setAttr FKElbowShield_L.scaleX 1;setAttr FKElbowShield_L.scaleY 1;setAttr FKElbowShield_L.scaleZ 1;setAttr FKExtraHip_L.translateX 0;setAttr FKExtraHip_L.translateY 0;setAttr FKExtraHip_L.translateZ 0;setAttr FKExtraHip_L.rotateX -4.820528189e-15;setAttr FKExtraHip_L.rotateY -9.93923338e-17;setAttr FKExtraHip_L.rotateZ -3.130858515e-15;setAttr FKExtraHip_L.scaleX 1;setAttr FKExtraHip_L.scaleY 1;setAttr FKExtraHip_L.scaleZ 1;setAttr FKHip_L.translateX -8.881784197e-16;setAttr FKHip_L.translateY 0;setAttr FKHip_L.translateZ 4.440892099e-16;setAttr FKHip_L.rotateX -0;setAttr FKHip_L.rotateY 0;setAttr FKHip_L.rotateZ 0;setAttr FKHip_L.scaleX 1;setAttr FKHip_L.scaleY 1;setAttr FKHip_L.scaleZ 1;setAttr FKExtraHipTwist_L.translateX 0;setAttr FKExtraHipTwist_L.translateY 2.220446049e-16;setAttr FKExtraHipTwist_L.translateZ 0;setAttr FKExtraHipTwist_L.rotateX 1.049583045e-13;setAttr FKExtraHipTwist_L.rotateY 1.529418532e-29;setAttr FKExtraHipTwist_L.rotateZ 1.669791208e-14;setAttr FKExtraHipTwist_L.scaleX 1;setAttr FKExtraHipTwist_L.scaleY 1;setAttr FKExtraHipTwist_L.scaleZ 1;setAttr FKHipTwist_L.translateX 0;setAttr FKHipTwist_L.translateY 0;setAttr FKHipTwist_L.translateZ 8.881784197e-16;setAttr FKHipTwist_L.rotateX -0;setAttr FKHipTwist_L.rotateY 0;setAttr FKHipTwist_L.rotateZ 0;setAttr FKHipTwist_L.scaleX 1;setAttr FKHipTwist_L.scaleY 1;setAttr FKHipTwist_L.scaleZ 1;setAttr FKExtraFingers_L.translateX 8.881784197e-16;setAttr FKExtraFingers_L.translateY 5.551115123e-16;setAttr FKExtraFingers_L.translateZ -8.881784197e-16;setAttr FKExtraFingers_L.rotateX 2.584200679e-14;setAttr FKExtraFingers_L.rotateY 5.565970693e-15;setAttr FKExtraFingers_L.rotateZ -7.951386704e-16;setAttr FKExtraFingers_L.scaleX 1;setAttr FKExtraFingers_L.scaleY 1;setAttr FKExtraFingers_L.scaleZ 1;setAttr FKFingers_L.translateX 0;setAttr FKFingers_L.translateY -6.106226635e-16;setAttr FKFingers_L.translateZ 0;setAttr FKFingers_L.rotateX -0;setAttr FKFingers_L.rotateY 0;setAttr FKFingers_L.rotateZ -0;setAttr FKFingers_L.scaleX 1;setAttr FKFingers_L.scaleY 1;setAttr FKFingers_L.scaleZ 1;setAttr FKExtraElbow_L.translateX 8.881784197e-16;setAttr FKExtraElbow_L.translateY 5.551115123e-17;setAttr FKExtraElbow_L.translateZ 8.881784197e-16;setAttr FKExtraElbow_L.rotateX -1.948089742e-14;setAttr FKExtraElbow_L.rotateY -3.975693352e-15;setAttr FKExtraElbow_L.rotateZ -3.180554681e-15;setAttr FKExtraElbow_L.scaleX 1;setAttr FKExtraElbow_L.scaleY 1;setAttr FKExtraElbow_L.scaleZ 1;setAttr FKElbow_L.translateX 8.881784197e-16;setAttr FKElbow_L.translateY 0;setAttr FKElbow_L.translateZ 0;setAttr FKElbow_L.rotateX -0;setAttr FKElbow_L.rotateY 0;setAttr FKElbow_L.rotateZ -0;setAttr FKElbow_L.scaleX 1;setAttr FKElbow_L.scaleY 1;setAttr FKElbow_L.scaleZ 1;setAttr FKExtraShoulder_L.translateX -8.881784197e-16;setAttr FKExtraShoulder_L.translateY -2.664535259e-15;setAttr FKExtraShoulder_L.translateZ 0;setAttr FKExtraShoulder_L.rotateX 6.535045947e-15;setAttr FKExtraShoulder_L.rotateY 1.192708006e-15;setAttr FKExtraShoulder_L.rotateZ 1.192708006e-15;setAttr FKExtraShoulder_L.scaleX 1;setAttr FKExtraShoulder_L.scaleY 1;setAttr FKExtraShoulder_L.scaleZ 1;setAttr FKShoulder_L.translateX 0;setAttr FKShoulder_L.translateY -8.881784197e-16;setAttr FKShoulder_L.translateZ 4.440892099e-16;setAttr FKShoulder_L.rotateX -0;setAttr FKShoulder_L.rotateY 0;setAttr FKShoulder_L.rotateZ -0;setAttr FKShoulder_L.scaleX 1;setAttr FKShoulder_L.scaleY 1;setAttr FKShoulder_L.scaleZ 1;setAttr IKExtraLeg_R.translateX 0;setAttr IKExtraLeg_R.translateY 0;setAttr IKExtraLeg_R.translateZ 0;setAttr IKExtraLeg_R.rotateX -0;setAttr IKExtraLeg_R.rotateY -0;setAttr IKExtraLeg_R.rotateZ 0;setAttr IKLeg_R.translateX 0;setAttr IKLeg_R.translateY 0;setAttr IKLeg_R.translateZ 0;setAttr IKLeg_R.rotateX -0;setAttr IKLeg_R.rotateY -0;setAttr IKLeg_R.rotateZ 0;setAttr IKLeg_R.swivel 0;setAttr IKLeg_R.toe 0;setAttr IKLeg_R.roll 0;setAttr IKLeg_R.rollAngle 25;setAttr PoleExtraLeg_R.translateX 0;setAttr PoleExtraLeg_R.translateY 0;setAttr PoleExtraLeg_R.translateZ 0;setAttr PoleLeg_R.translateX 0;setAttr PoleLeg_R.translateY 0;setAttr PoleLeg_R.translateZ 0;setAttr PoleLeg_R.follow 10;setAttr FKIKLeg_R.FKIKBlend 10;setAttr FKIKLeg_R.FKVis 0;setAttr FKIKLeg_R.IKVis 1;setAttr IKExtraLegHeel_R.rotateX 0;setAttr IKExtraLegHeel_R.rotateY 1.987846676e-16;setAttr IKExtraLegHeel_R.rotateZ 0;setAttr IKLegHeel_R.rotateX 0;setAttr IKLegHeel_R.rotateY -9.93923338e-17;setAttr IKLegHeel_R.rotateZ 0;setAttr IKExtraLegToe_R.rotateX 0;setAttr IKExtraLegToe_R.rotateY -0;setAttr IKExtraLegToe_R.rotateZ 0;setAttr IKLegToe_R.rotateX 0;setAttr IKLegToe_R.rotateY -0;setAttr IKLegToe_R.rotateZ 0;setAttr IKExtraLegBall_R.rotateX 0;setAttr IKExtraLegBall_R.rotateY -9.93923338e-17;setAttr IKExtraLegBall_R.rotateZ 0;setAttr IKLegBall_R.rotateX 0;setAttr IKExtraLeg_L.translateX 0;setAttr IKExtraLeg_L.translateY 0;setAttr IKExtraLeg_L.translateZ 0;setAttr IKExtraLeg_L.rotateX -0;setAttr IKExtraLeg_L.rotateY -0;setAttr IKExtraLeg_L.rotateZ 0;setAttr IKLeg_L.translateX 0;setAttr IKLeg_L.translateY 0;setAttr IKLeg_L.translateZ 0;setAttr IKLeg_L.rotateX -0;setAttr IKLeg_L.rotateY -0;setAttr IKLeg_L.rotateZ 0;setAttr IKLeg_L.swivel 0;setAttr IKLeg_L.toe 0;setAttr IKLeg_L.roll 0;setAttr IKLeg_L.rollAngle 25;setAttr PoleExtraLeg_L.translateX 0;setAttr PoleExtraLeg_L.translateY 0;setAttr PoleExtraLeg_L.translateZ 0;setAttr PoleLeg_L.translateX 0;setAttr PoleLeg_L.translateY -4.440892099e-16;setAttr PoleLeg_L.translateZ 0;setAttr PoleLeg_L.follow 10;setAttr FKIKLeg_L.FKIKBlend 10;setAttr FKIKLeg_L.FKVis 0;setAttr FKIKLeg_L.IKVis 1;setAttr IKExtraLegHeel_L.rotateX 0;setAttr IKExtraLegHeel_L.rotateY -9.93923338e-17;setAttr IKExtraLegHeel_L.rotateZ 0;setAttr IKLegHeel_L.rotateX 0;setAttr IKLegHeel_L.rotateY -9.93923338e-17;setAttr IKLegHeel_L.rotateZ 0;setAttr IKExtraLegToe_L.rotateX 0;setAttr IKExtraLegToe_L.rotateY -9.93923338e-17;setAttr IKExtraLegToe_L.rotateZ 0;setAttr IKLegToe_L.rotateX 0;setAttr IKLegToe_L.rotateY -9.93923338e-17;setAttr IKLegToe_L.rotateZ 0;setAttr IKExtraLegBall_L.rotateX 0;setAttr IKExtraLegBall_L.rotateY -1.987846676e-16;setAttr IKExtraLegBall_L.rotateZ 0;setAttr IKLegBall_L.rotateX 0;setAttr CenterExtra_M.translateX 0;setAttr CenterExtra_M.translateY 0;setAttr CenterExtra_M.translateZ 0;setAttr CenterExtra_M.rotateX 0;setAttr CenterExtra_M.rotateY -0;setAttr CenterExtra_M.rotateZ 0;setAttr Center_M.translateX 0;setAttr Center_M.translateY 0;setAttr Center_M.translateZ 0;setAttr Center_M.rotateX 0;setAttr Center_M.rotateY -0;setAttr Center_M.rotateZ 0;setAttr Center_M.CenterBtwFeet 0;setAttr Main.visibility 1;setAttr Main.translateX 0;setAttr Main.translateY 0;setAttr Main.translateZ 0;setAttr Main.rotateX 0;setAttr Main.rotateY 0;setAttr Main.rotateZ 0;setAttr Main.scaleX 1;setAttr Main.scaleY 1;setAttr Main.scaleZ 1;setAttr FKExtraMiddleToe1_R.translateX -8.881784197e-16;setAttr FKExtraMiddleToe1_R.translateY -8.881784197e-16;setAttr FKExtraMiddleToe1_R.translateZ 2.220446049e-16;setAttr FKExtraMiddleToe1_R.rotateX -0;setAttr FKExtraMiddleToe1_R.rotateY 0;setAttr FKExtraMiddleToe1_R.rotateZ -0;setAttr FKExtraMiddleToe1_R.scaleX 1;setAttr FKExtraMiddleToe1_R.scaleY 1;setAttr FKExtraMiddleToe1_R.scaleZ 1;setAttr FKMiddleToe1_R.translateX -8.881784197e-16;setAttr FKMiddleToe1_R.translateY 0;setAttr FKMiddleToe1_R.translateZ 2.220446049e-16;setAttr FKMiddleToe1_R.rotateX -0;setAttr FKMiddleToe1_R.rotateY 0;setAttr FKMiddleToe1_R.rotateZ -0;setAttr FKMiddleToe1_R.scaleX 1;setAttr FKMiddleToe1_R.scaleY 1;setAttr FKMiddleToe1_R.scaleZ 1;setAttr FKExtraAnkle_R.translateX 0;setAttr FKExtraAnkle_R.translateY 1.110223025e-16;setAttr FKExtraAnkle_R.translateZ -2.220446049e-16;setAttr FKExtraAnkle_R.rotateX -0;setAttr FKExtraAnkle_R.rotateY -0;setAttr FKExtraAnkle_R.rotateZ 0;setAttr FKExtraAnkle_R.scaleX 1;setAttr FKExtraAnkle_R.scaleY 1;setAttr FKExtraAnkle_R.scaleZ 1;setAttr FKAnkle_R.translateX 0;setAttr FKAnkle_R.translateY 1.110223025e-16;setAttr FKAnkle_R.translateZ -2.220446049e-16;setAttr FKAnkle_R.rotateX -0;setAttr FKAnkle_R.rotateY -0;setAttr FKAnkle_R.rotateZ 0;setAttr FKAnkle_R.scaleX 1;setAttr FKAnkle_R.scaleY 1;setAttr FKAnkle_R.scaleZ 1;setAttr FKExtraKnee_R.translateX 8.881784197e-16;setAttr FKExtraKnee_R.translateY 0;setAttr FKExtraKnee_R.translateZ -4.440892099e-16;setAttr FKExtraKnee_R.rotateX -0;setAttr FKExtraKnee_R.rotateY 0;setAttr FKExtraKnee_R.rotateZ 0;setAttr FKExtraKnee_R.scaleX 1;setAttr FKExtraKnee_R.scaleY 1;setAttr FKExtraKnee_R.scaleZ 1;setAttr FKKnee_R.translateX 0;setAttr FKKnee_R.translateY -8.881784197e-16;setAttr FKKnee_R.translateZ -2.220446049e-16;setAttr FKKnee_R.rotateX -0;setAttr FKKnee_R.rotateY 0;setAttr FKKnee_R.rotateZ 0;setAttr FKKnee_R.scaleX 1;setAttr FKKnee_R.scaleY 1;setAttr FKKnee_R.scaleZ 1;setAttr FKExtraElbowShield_R.translateX 8.881784197e-16;setAttr FKExtraElbowShield_R.translateY 1.33226763e-15;setAttr FKExtraElbowShield_R.translateZ 8.881784197e-16;setAttr FKExtraElbowShield_R.rotateX -0;setAttr FKExtraElbowShield_R.rotateY 0;setAttr FKExtraElbowShield_R.rotateZ 0;setAttr FKExtraElbowShield_R.scaleX 1;setAttr FKExtraElbowShield_R.scaleY 1;setAttr FKExtraElbowShield_R.scaleZ 1;setAttr FKElbowShield_R.translateX 0;setAttr FKElbowShield_R.translateY 0;setAttr FKElbowShield_R.translateZ 0;setAttr FKElbowShield_R.rotateX -0;setAttr FKElbowShield_R.rotateY 0;setAttr FKElbowShield_R.rotateZ 0;setAttr FKElbowShield_R.scaleX 1;setAttr FKElbowShield_R.scaleY 1;setAttr FKElbowShield_R.scaleZ 1;setAttr FKExtraHip_R.translateX -8.881784197e-16;setAttr FKExtraHip_R.translateY 1.776356839e-15;setAttr FKExtraHip_R.translateZ 0;setAttr FKExtraHip_R.rotateX -0;setAttr FKExtraHip_R.rotateY 0;setAttr FKExtraHip_R.rotateZ 0;setAttr FKExtraHip_R.scaleX 1;setAttr FKExtraHip_R.scaleY 1;setAttr FKExtraHip_R.scaleZ 1;setAttr FKHip_R.translateX -1.776356839e-15;setAttr FKHip_R.translateY 0;setAttr FKHip_R.translateZ 4.440892099e-16;setAttr FKHip_R.rotateX -0;setAttr FKHip_R.rotateY 0;setAttr FKHip_R.rotateZ 0;setAttr FKHip_R.scaleX 1;setAttr FKHip_R.scaleY 1;setAttr FKHip_R.scaleZ 1;setAttr FKExtraHipTwist_R.translateX 0;setAttr FKExtraHipTwist_R.translateY -2.220446049e-16;setAttr FKExtraHipTwist_R.translateZ 4.440892099e-16;setAttr FKExtraHipTwist_R.rotateX -0;setAttr FKExtraHipTwist_R.rotateY 0;setAttr FKExtraHipTwist_R.rotateZ 0;setAttr FKExtraHipTwist_R.scaleX 1;setAttr FKExtraHipTwist_R.scaleY 1;setAttr FKExtraHipTwist_R.scaleZ 1;setAttr FKHipTwist_R.translateX 0;setAttr FKHipTwist_R.translateY -2.220446049e-16;setAttr FKHipTwist_R.translateZ 4.440892099e-16;setAttr FKHipTwist_R.rotateX -0;setAttr FKHipTwist_R.rotateY 0;setAttr FKHipTwist_R.rotateZ 0;setAttr FKHipTwist_R.scaleX 1;setAttr FKHipTwist_R.scaleY 1;setAttr FKHipTwist_R.scaleZ 1;setAttr FKExtraJaw_M.translateX 0;setAttr FKExtraJaw_M.translateY 0;setAttr FKExtraJaw_M.translateZ -1.776356839e-15;setAttr FKExtraJaw_M.rotateX 0;setAttr FKExtraJaw_M.rotateY -0;setAttr FKExtraJaw_M.rotateZ 0;setAttr FKExtraJaw_M.scaleX 1;setAttr FKExtraJaw_M.scaleY 1;setAttr FKExtraJaw_M.scaleZ 1;setAttr FKJaw_M.translateX 0;setAttr FKJaw_M.translateY 0;setAttr FKJaw_M.translateZ -1.776356839e-15;setAttr FKJaw_M.rotateX 0;setAttr FKJaw_M.rotateY -0;setAttr FKJaw_M.rotateZ 0;setAttr FKJaw_M.scaleX 1;setAttr FKJaw_M.scaleY 1;setAttr FKJaw_M.scaleZ 1;");
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 2 ".sol";
connectAttr "jointLayer.di" "Pelvis.do";
connectAttr "Pelvis.s" "HipTwist.is";
connectAttr "jointLayer.di" "HipTwist.do";
connectAttr "jointLayer.di" "Hip.do";
connectAttr "Hip.s" "Knee.is";
connectAttr "jointLayer.di" "Knee.do";
connectAttr "Knee.s" "Ankle.is";
connectAttr "jointLayer.di" "Ankle.do";
connectAttr "Ankle.s" "MiddleToe1.is";
connectAttr "jointLayer.di" "MiddleToe1.do";
connectAttr "MiddleToe1.s" "MiddleToe2_End.is";
connectAttr "jointLayer.di" "MiddleToe2_End.do";
connectAttr "Ankle.s" "Heel_End.is";
connectAttr "jointLayer.di" "Heel_End.do";
connectAttr "Hip.s" "ElbowShield.is";
connectAttr "jointLayer.di" "ElbowShield.do";
connectAttr "jointLayer.di" "ElbowShield_End.do";
connectAttr "Pelvis.s" "Head.is";
connectAttr "jointLayer.di" "Head.do";
connectAttr "Head.global" "Head.globalConnect";
connectAttr "Head.s" "Head_End.is";
connectAttr "jointLayer.di" "Head_End.do";
connectAttr "Head.s" "Jaw.is";
connectAttr "jointLayer.di" "Jaw.do";
connectAttr "Jaw.s" "Jaw_End.is";
connectAttr "jointLayer.di" "Jaw_End.do";
connectAttr "Pelvis.s" "Shoulder.is";
connectAttr "jointLayer.di" "Shoulder.do";
connectAttr "Shoulder.s" "Elbow.is";
connectAttr "jointLayer.di" "Elbow.do";
connectAttr "Elbow.s" "Fingers.is";
connectAttr "jointLayer.di" "Fingers.do";
connectAttr "jointLayer.di" "Fingers_End.do";
connectAttr "Main.fkVis" "FKSystem.v";
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.ctx" "FKParentConstraintToAnkle_R.tx"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.cty" "FKParentConstraintToAnkle_R.ty"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.ctz" "FKParentConstraintToAnkle_R.tz"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.crx" "FKParentConstraintToAnkle_R.rx"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.cry" "FKParentConstraintToAnkle_R.ry"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.crz" "FKParentConstraintToAnkle_R.rz"
		;
connectAttr "unitConversion6.o" "FKOffsetMiddleToe1_R.rx";
connectAttr "jointLayer.di" "FKOffsetMiddleToe1_R.do";
connectAttr "jointLayer.di" "FKXMiddleToe1_R.do";
connectAttr "FKMiddleToe1_R.s" "FKXMiddleToe2_End_R.is";
connectAttr "jointLayer.di" "FKXMiddleToe2_End_R.do";
connectAttr "FKParentConstraintToAnkle_R.ro" "FKParentConstraintToAnkle_R_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToAnkle_R.pim" "FKParentConstraintToAnkle_R_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToAnkle_R.rp" "FKParentConstraintToAnkle_R_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToAnkle_R.rpt" "FKParentConstraintToAnkle_R_parentConstraint1.crt"
		;
connectAttr "Ankle_R.t" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tt"
		;
connectAttr "Ankle_R.rp" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Ankle_R.rpt" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Ankle_R.r" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tr"
		;
connectAttr "Ankle_R.ro" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Ankle_R.s" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].ts"
		;
connectAttr "Ankle_R.pm" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Ankle_R.jo" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.w0" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.ctx" "FKParentConstraintToHip_R.tx"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.cty" "FKParentConstraintToHip_R.ty"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.ctz" "FKParentConstraintToHip_R.tz"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.crx" "FKParentConstraintToHip_R.rx"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.cry" "FKParentConstraintToHip_R.ry"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.crz" "FKParentConstraintToHip_R.rz"
		;
connectAttr "jointLayer.di" "FKOffsetElbowShield_R.do";
connectAttr "jointLayer.di" "FKXElbowShield_R.do";
connectAttr "FKElbowShield_R.s" "FKXElbowShield_End_R.is";
connectAttr "jointLayer.di" "FKXElbowShield_End_R.do";
connectAttr "FKParentConstraintToHip_R.ro" "FKParentConstraintToHip_R_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToHip_R.pim" "FKParentConstraintToHip_R_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToHip_R.rp" "FKParentConstraintToHip_R_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToHip_R.rpt" "FKParentConstraintToHip_R_parentConstraint1.crt"
		;
connectAttr "Hip_R.t" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tt";
connectAttr "Hip_R.rp" "FKParentConstraintToHip_R_parentConstraint1.tg[0].trp";
connectAttr "Hip_R.rpt" "FKParentConstraintToHip_R_parentConstraint1.tg[0].trt";
connectAttr "Hip_R.r" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tr";
connectAttr "Hip_R.ro" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tro";
connectAttr "Hip_R.s" "FKParentConstraintToHip_R_parentConstraint1.tg[0].ts";
connectAttr "Hip_R.pm" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tpm";
connectAttr "Hip_R.jo" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tjo";
connectAttr "FKParentConstraintToHip_R_parentConstraint1.w0" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.ctx" "FKParentConstraintToAnkle_L.tx"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.cty" "FKParentConstraintToAnkle_L.ty"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.ctz" "FKParentConstraintToAnkle_L.tz"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.crx" "FKParentConstraintToAnkle_L.rx"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.cry" "FKParentConstraintToAnkle_L.ry"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.crz" "FKParentConstraintToAnkle_L.rz"
		;
connectAttr "unitConversion12.o" "FKOffsetMiddleToe1_L.rx";
connectAttr "jointLayer.di" "FKOffsetMiddleToe1_L.do";
connectAttr "jointLayer.di" "FKXMiddleToe1_L.do";
connectAttr "FKMiddleToe1_L.s" "FKXMiddleToe2_End_L.is";
connectAttr "jointLayer.di" "FKXMiddleToe2_End_L.do";
connectAttr "FKParentConstraintToAnkle_L.ro" "FKParentConstraintToAnkle_L_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToAnkle_L.pim" "FKParentConstraintToAnkle_L_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToAnkle_L.rp" "FKParentConstraintToAnkle_L_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToAnkle_L.rpt" "FKParentConstraintToAnkle_L_parentConstraint1.crt"
		;
connectAttr "Ankle_L.t" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Ankle_L.rp" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Ankle_L.rpt" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Ankle_L.r" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Ankle_L.ro" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Ankle_L.s" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Ankle_L.pm" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Ankle_L.jo" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.w0" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.ctx" "FKParentConstraintToHip_L.tx"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.cty" "FKParentConstraintToHip_L.ty"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.ctz" "FKParentConstraintToHip_L.tz"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.crx" "FKParentConstraintToHip_L.rx"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.cry" "FKParentConstraintToHip_L.ry"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.crz" "FKParentConstraintToHip_L.rz"
		;
connectAttr "jointLayer.di" "FKOffsetElbowShield_L.do";
connectAttr "jointLayer.di" "FKXElbowShield_L.do";
connectAttr "FKElbowShield_L.s" "FKXElbowShield_End_L.is";
connectAttr "jointLayer.di" "FKXElbowShield_End_L.do";
connectAttr "FKParentConstraintToHip_L.ro" "FKParentConstraintToHip_L_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToHip_L.pim" "FKParentConstraintToHip_L_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToHip_L.rp" "FKParentConstraintToHip_L_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToHip_L.rpt" "FKParentConstraintToHip_L_parentConstraint1.crt"
		;
connectAttr "Hip_L.t" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tt";
connectAttr "Hip_L.rp" "FKParentConstraintToHip_L_parentConstraint1.tg[0].trp";
connectAttr "Hip_L.rpt" "FKParentConstraintToHip_L_parentConstraint1.tg[0].trt";
connectAttr "Hip_L.r" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tr";
connectAttr "Hip_L.ro" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tro";
connectAttr "Hip_L.s" "FKParentConstraintToHip_L_parentConstraint1.tg[0].ts";
connectAttr "Hip_L.pm" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tpm";
connectAttr "Hip_L.jo" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tjo";
connectAttr "FKParentConstraintToHip_L_parentConstraint1.w0" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctx" "PelvisCenterBtwLegsBlended_M.tx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cty" "PelvisCenterBtwLegsBlended_M.ty"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctz" "PelvisCenterBtwLegsBlended_M.tz"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crx" "PelvisCenterBtwLegsBlended_M.rx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cry" "PelvisCenterBtwLegsBlended_M.ry"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crz" "PelvisCenterBtwLegsBlended_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetPelvis_M.do";
connectAttr "jointLayer.di" "FKXPelvis_M.do";
connectAttr "FKXPelvis_M.s" "FKOffsetHipTwist_R.is";
connectAttr "jointLayer.di" "FKOffsetHipTwist_R.do";
connectAttr "FKPelvis_M.s" "FKXHipTwist_R.is";
connectAttr "jointLayer.di" "FKXHipTwist_R.do";
connectAttr "FKXHipTwist_R.s" "FKOffsetHip_R.is";
connectAttr "FKIKBlendLegCondition_R.ocg" "FKOffsetHip_R.v" -l on;
connectAttr "jointLayer.di" "FKOffsetHip_R.do";
connectAttr "jointLayer.di" "FKXHip_R.do";
connectAttr "FKXHip_R.s" "FKOffsetKnee_R.is";
connectAttr "jointLayer.di" "FKOffsetKnee_R.do";
connectAttr "FKHip_R.s" "FKXKnee_R.is";
connectAttr "jointLayer.di" "FKXKnee_R.do";
connectAttr "FKXKnee_R.s" "FKOffsetAnkle_R.is";
connectAttr "jointLayer.di" "FKOffsetAnkle_R.do";
connectAttr "FKKnee_R.s" "FKXAnkle_R.is";
connectAttr "jointLayer.di" "FKXAnkle_R.do";
connectAttr "FKAnkle_R.s" "FKXHeel_End_R.is";
connectAttr "jointLayer.di" "FKXHeel_End_R.do";
connectAttr "FKXPelvis_M.s" "FKOffsetHead_M.is";
connectAttr "jointLayer.di" "FKOffsetHead_M.do";
connectAttr "FKGlobalHead_M_orientConstraint1.crx" "FKGlobalHead_M.rx";
connectAttr "FKGlobalHead_M_orientConstraint1.cry" "FKGlobalHead_M.ry";
connectAttr "FKGlobalHead_M_orientConstraint1.crz" "FKGlobalHead_M.rz";
connectAttr "FKPelvis_M.s" "FKXHead_M.is";
connectAttr "jointLayer.di" "FKXHead_M.do";
connectAttr "FKHead_M.s" "FKXHead_End_M.is";
connectAttr "jointLayer.di" "FKXHead_End_M.do";
connectAttr "FKXHead_M.s" "FKOffsetJaw_M.is";
connectAttr "jointLayer.di" "FKOffsetJaw_M.do";
connectAttr "FKHead_M.s" "FKXJaw_M.is";
connectAttr "jointLayer.di" "FKXJaw_M.do";
connectAttr "FKJaw_M.s" "FKXJaw_End_M.is";
connectAttr "jointLayer.di" "FKXJaw_End_M.do";
connectAttr "FKGlobalHead_M.ro" "FKGlobalHead_M_orientConstraint1.cro";
connectAttr "FKGlobalHead_M.pim" "FKGlobalHead_M_orientConstraint1.cpim";
connectAttr "GlobalHead_M.r" "FKGlobalHead_M_orientConstraint1.tg[0].tr";
connectAttr "GlobalHead_M.ro" "FKGlobalHead_M_orientConstraint1.tg[0].tro";
connectAttr "GlobalHead_M.pm" "FKGlobalHead_M_orientConstraint1.tg[0].tpm";
connectAttr "FKGlobalHead_M_orientConstraint1.w0" "FKGlobalHead_M_orientConstraint1.tg[0].tw"
		;
connectAttr "FKGlobalStaticHead_M.r" "FKGlobalHead_M_orientConstraint1.tg[1].tr"
		;
connectAttr "FKGlobalStaticHead_M.ro" "FKGlobalHead_M_orientConstraint1.tg[1].tro"
		;
connectAttr "FKGlobalStaticHead_M.pm" "FKGlobalHead_M_orientConstraint1.tg[1].tpm"
		;
connectAttr "FKGlobalHead_M_orientConstraint1.w1" "FKGlobalHead_M_orientConstraint1.tg[1].tw"
		;
connectAttr "GlobalHead_unitConversion_M.o" "FKGlobalHead_M_orientConstraint1.w0"
		;
connectAttr "GlobalHead_reverse_M.ox" "FKGlobalHead_M_orientConstraint1.w1";
connectAttr "FKXPelvis_M.s" "FKOffsetShoulder_R.is";
connectAttr "jointLayer.di" "FKOffsetShoulder_R.do";
connectAttr "FKPelvis_M.s" "FKXShoulder_R.is";
connectAttr "jointLayer.di" "FKXShoulder_R.do";
connectAttr "FKXShoulder_R.s" "FKOffsetElbow_R.is";
connectAttr "jointLayer.di" "FKOffsetElbow_R.do";
connectAttr "FKShoulder_R.s" "FKXElbow_R.is";
connectAttr "jointLayer.di" "FKXElbow_R.do";
connectAttr "FKXElbow_R.s" "FKOffsetFingers_R.is";
connectAttr "jointLayer.di" "FKOffsetFingers_R.do";
connectAttr "FKElbow_R.s" "FKXFingers_R.is";
connectAttr "jointLayer.di" "FKXFingers_R.do";
connectAttr "FKFingers_R.s" "FKXFingers_End_R.is";
connectAttr "jointLayer.di" "FKXFingers_End_R.do";
connectAttr "FKXPelvis_M.s" "FKOffsetHipTwist_L.is";
connectAttr "jointLayer.di" "FKOffsetHipTwist_L.do";
connectAttr "FKPelvis_M.s" "FKXHipTwist_L.is";
connectAttr "jointLayer.di" "FKXHipTwist_L.do";
connectAttr "FKXHipTwist_L.s" "FKOffsetHip_L.is";
connectAttr "FKIKBlendLegCondition_L.ocg" "FKOffsetHip_L.v" -l on;
connectAttr "jointLayer.di" "FKOffsetHip_L.do";
connectAttr "jointLayer.di" "FKXHip_L.do";
connectAttr "FKXHip_L.s" "FKOffsetKnee_L.is";
connectAttr "jointLayer.di" "FKOffsetKnee_L.do";
connectAttr "FKHip_L.s" "FKXKnee_L.is";
connectAttr "jointLayer.di" "FKXKnee_L.do";
connectAttr "FKXKnee_L.s" "FKOffsetAnkle_L.is";
connectAttr "jointLayer.di" "FKOffsetAnkle_L.do";
connectAttr "FKKnee_L.s" "FKXAnkle_L.is";
connectAttr "jointLayer.di" "FKXAnkle_L.do";
connectAttr "FKAnkle_L.s" "FKXHeel_End_L.is";
connectAttr "jointLayer.di" "FKXHeel_End_L.do";
connectAttr "FKXPelvis_M.s" "FKOffsetShoulder_L.is";
connectAttr "jointLayer.di" "FKOffsetShoulder_L.do";
connectAttr "FKPelvis_M.s" "FKXShoulder_L.is";
connectAttr "jointLayer.di" "FKXShoulder_L.do";
connectAttr "FKXShoulder_L.s" "FKOffsetElbow_L.is";
connectAttr "jointLayer.di" "FKOffsetElbow_L.do";
connectAttr "FKShoulder_L.s" "FKXElbow_L.is";
connectAttr "jointLayer.di" "FKXElbow_L.do";
connectAttr "FKXElbow_L.s" "FKOffsetFingers_L.is";
connectAttr "jointLayer.di" "FKOffsetFingers_L.do";
connectAttr "FKElbow_L.s" "FKXFingers_L.is";
connectAttr "jointLayer.di" "FKXFingers_L.do";
connectAttr "FKFingers_L.s" "FKXFingers_End_L.is";
connectAttr "jointLayer.di" "FKXFingers_End_L.do";
connectAttr "PelvisCenterBtwLegsBlended_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.pim" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crt"
		;
connectAttr "PelvisCenter_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tt"
		;
connectAttr "PelvisCenter_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trp"
		;
connectAttr "PelvisCenter_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trt"
		;
connectAttr "PelvisCenter_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tr"
		;
connectAttr "PelvisCenter_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tro"
		;
connectAttr "PelvisCenter_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].ts"
		;
connectAttr "PelvisCenter_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trp"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tr"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tro"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].ts"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tw"
		;
connectAttr "PelvisCenterBtwFeetReverse_M.ox" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0"
		;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1"
		;
connectAttr "Main.ikVis" "IKSystem.v";
connectAttr "IKParentConstraintHip_R_parentConstraint1.ctx" "IKParentConstraintHip_R.tx"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.cty" "IKParentConstraintHip_R.ty"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.ctz" "IKParentConstraintHip_R.tz"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.crx" "IKParentConstraintHip_R.rx"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.cry" "IKParentConstraintHip_R.ry"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.crz" "IKParentConstraintHip_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintHip_R.v";
connectAttr "jointLayer.di" "IKXHip_R.do";
connectAttr "IKXHip_R.s" "IKXKnee_R.is";
connectAttr "jointLayer.di" "IKXKnee_R.do";
connectAttr "IKXKnee_R.s" "IKXAnkle_R.is";
connectAttr "IKXAnkle_R_orientConstraint1.crx" "IKXAnkle_R.rx";
connectAttr "IKXAnkle_R_orientConstraint1.cry" "IKXAnkle_R.ry";
connectAttr "IKXAnkle_R_orientConstraint1.crz" "IKXAnkle_R.rz";
connectAttr "jointLayer.di" "IKXAnkle_R.do";
connectAttr "IKXAnkle_R.s" "IKXMiddleToe1_R.is";
connectAttr "jointLayer.di" "IKXMiddleToe1_R.do";
connectAttr "IKXMiddleToe1_R.s" "IKXMiddleToe2_End_R.is";
connectAttr "jointLayer.di" "IKXMiddleToe2_End_R.do";
connectAttr "IKXMiddleToe2_End_R.tx" "effector2.tx";
connectAttr "IKXMiddleToe2_End_R.ty" "effector2.ty";
connectAttr "IKXMiddleToe2_End_R.tz" "effector2.tz";
connectAttr "IKXAnkle_R.ro" "IKXAnkle_R_orientConstraint1.cro";
connectAttr "IKXAnkle_R.pim" "IKXAnkle_R_orientConstraint1.cpim";
connectAttr "IKXAnkle_R.jo" "IKXAnkle_R_orientConstraint1.cjo";
connectAttr "IKLeg_R.r" "IKXAnkle_R_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "IKXAnkle_R_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "IKXAnkle_R_orientConstraint1.tg[0].tpm";
connectAttr "IKXAnkle_R_orientConstraint1.w0" "IKXAnkle_R_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXMiddleToe1_R.tx" "effector3.tx";
connectAttr "IKXMiddleToe1_R.ty" "effector3.ty";
connectAttr "IKXMiddleToe1_R.tz" "effector3.tz";
connectAttr "IKXAnkle_R.tx" "effector1.tx";
connectAttr "IKXAnkle_R.ty" "effector1.ty";
connectAttr "IKXAnkle_R.tz" "effector1.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_R.v";
connectAttr "PoleAnnotateTargetLeg_RShape.wm" "PoleAnnotationLeg_RShape.dom" -na
		;
connectAttr "IKParentConstraintHip_R.ro" "IKParentConstraintHip_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintHip_R.pim" "IKParentConstraintHip_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintHip_R.rp" "IKParentConstraintHip_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintHip_R.rpt" "IKParentConstraintHip_R_parentConstraint1.crt"
		;
connectAttr "HipTwist_R.t" "IKParentConstraintHip_R_parentConstraint1.tg[0].tt";
connectAttr "HipTwist_R.rp" "IKParentConstraintHip_R_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_R.rpt" "IKParentConstraintHip_R_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_R.r" "IKParentConstraintHip_R_parentConstraint1.tg[0].tr";
connectAttr "HipTwist_R.ro" "IKParentConstraintHip_R_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_R.s" "IKParentConstraintHip_R_parentConstraint1.tg[0].ts";
connectAttr "HipTwist_R.pm" "IKParentConstraintHip_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_R.jo" "IKParentConstraintHip_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.w0" "IKParentConstraintHip_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.ctx" "IKParentConstraintHip_L.tx"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.cty" "IKParentConstraintHip_L.ty"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.ctz" "IKParentConstraintHip_L.tz"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.crx" "IKParentConstraintHip_L.rx"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.cry" "IKParentConstraintHip_L.ry"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.crz" "IKParentConstraintHip_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintHip_L.v";
connectAttr "jointLayer.di" "IKXHip_L.do";
connectAttr "IKXHip_L.s" "IKXKnee_L.is";
connectAttr "jointLayer.di" "IKXKnee_L.do";
connectAttr "IKXKnee_L.s" "IKXAnkle_L.is";
connectAttr "IKXAnkle_L_orientConstraint1.crx" "IKXAnkle_L.rx";
connectAttr "IKXAnkle_L_orientConstraint1.cry" "IKXAnkle_L.ry";
connectAttr "IKXAnkle_L_orientConstraint1.crz" "IKXAnkle_L.rz";
connectAttr "jointLayer.di" "IKXAnkle_L.do";
connectAttr "IKXAnkle_L.s" "IKXMiddleToe1_L.is";
connectAttr "jointLayer.di" "IKXMiddleToe1_L.do";
connectAttr "IKXMiddleToe1_L.s" "IKXMiddleToe2_End_L.is";
connectAttr "jointLayer.di" "IKXMiddleToe2_End_L.do";
connectAttr "IKXMiddleToe2_End_L.tx" "effector5.tx";
connectAttr "IKXMiddleToe2_End_L.ty" "effector5.ty";
connectAttr "IKXMiddleToe2_End_L.tz" "effector5.tz";
connectAttr "IKXAnkle_L.ro" "IKXAnkle_L_orientConstraint1.cro";
connectAttr "IKXAnkle_L.pim" "IKXAnkle_L_orientConstraint1.cpim";
connectAttr "IKXAnkle_L.jo" "IKXAnkle_L_orientConstraint1.cjo";
connectAttr "IKLeg_L.r" "IKXAnkle_L_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_L.ro" "IKXAnkle_L_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_L.pm" "IKXAnkle_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXAnkle_L_orientConstraint1.w0" "IKXAnkle_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXMiddleToe1_L.tx" "effector6.tx";
connectAttr "IKXMiddleToe1_L.ty" "effector6.ty";
connectAttr "IKXMiddleToe1_L.tz" "effector6.tz";
connectAttr "IKXAnkle_L.tx" "effector4.tx";
connectAttr "IKXAnkle_L.ty" "effector4.ty";
connectAttr "IKXAnkle_L.tz" "effector4.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_L.v";
connectAttr "PoleAnnotateTargetLeg_LShape.wm" "PoleAnnotationLeg_LShape.dom" -na
		;
connectAttr "IKParentConstraintHip_L.ro" "IKParentConstraintHip_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintHip_L.pim" "IKParentConstraintHip_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintHip_L.rp" "IKParentConstraintHip_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintHip_L.rpt" "IKParentConstraintHip_L_parentConstraint1.crt"
		;
connectAttr "HipTwist_L.t" "IKParentConstraintHip_L_parentConstraint1.tg[0].tt";
connectAttr "HipTwist_L.rp" "IKParentConstraintHip_L_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_L.rpt" "IKParentConstraintHip_L_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_L.r" "IKParentConstraintHip_L_parentConstraint1.tg[0].tr";
connectAttr "HipTwist_L.ro" "IKParentConstraintHip_L_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_L.s" "IKParentConstraintHip_L_parentConstraint1.tg[0].ts";
connectAttr "HipTwist_L.pm" "IKParentConstraintHip_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_L.jo" "IKParentConstraintHip_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.w0" "IKParentConstraintHip_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintLeg_R.v";
connectAttr "unitConversion2.o" "IKRollLegHeel_R.rx";
connectAttr "unitConversion4.o" "IKRollLegToe_R.rx";
connectAttr "IKLiftToeLegUnitConversion_R.o" "IKLiftToeLegToe_R.rx";
connectAttr "IKXMiddleToe1_R.msg" "IKXLegHandleToe_R.hsj";
connectAttr "effector2.hp" "IKXLegHandleToe_R.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleToe_R.hsv";
connectAttr "unitConversion3.o" "IKRollLegBall_R.rx";
connectAttr "IKXHip_R.msg" "IKXLegHandle_R.hsj";
connectAttr "effector1.hp" "IKXLegHandle_R.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_R.hsv";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctx" "IKXLegHandle_R.pvx";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.cty" "IKXLegHandle_R.pvy";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctz" "IKXLegHandle_R.pvz";
connectAttr "IKXLegHandle_R.pim" "IKXLegHandle_R_poleVectorConstraint1.cpim";
connectAttr "IKXHip_R.pm" "IKXLegHandle_R_poleVectorConstraint1.ps";
connectAttr "IKXHip_R.t" "IKXLegHandle_R_poleVectorConstraint1.crp";
connectAttr "PoleLeg_R.t" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_R.rp" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_R.rpt" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_R.pm" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.w0" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXAnkle_R.msg" "IKXLegHandleBall_R.hsj";
connectAttr "effector3.hp" "IKXLegHandleBall_R.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_R.hsv";
connectAttr "PoleAimLeg_R_aimConstraint1.crx" "PoleAimLeg_R.rx";
connectAttr "PoleAimLeg_R_aimConstraint1.cry" "PoleAimLeg_R.ry";
connectAttr "PoleAimLeg_R_aimConstraint1.crz" "PoleAimLeg_R.rz";
connectAttr "PoleAimLeg_R_pointConstraint1.ctx" "PoleAimLeg_R.tx";
connectAttr "PoleAimLeg_R_pointConstraint1.cty" "PoleAimLeg_R.ty";
connectAttr "PoleAimLeg_R_pointConstraint1.ctz" "PoleAimLeg_R.tz";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_aimConstraint1.cpim";
connectAttr "PoleAimLeg_R.t" "PoleAimLeg_R_aimConstraint1.ct";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_aimConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.crt";
connectAttr "PoleAimLeg_R.ro" "PoleAimLeg_R_aimConstraint1.cro";
connectAttr "IKLeg_R.t" "PoleAimLeg_R_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PoleAimLeg_R_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PoleAimLeg_R_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_aimConstraint1.w0" "PoleAimLeg_R_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_R.wm" "PoleAimLeg_R_aimConstraint1.wum";
connectAttr "unitConversion1.o" "PoleAimLeg_R_aimConstraint1.ox";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_pointConstraint1.cpim";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_pointConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_pointConstraint1.crt";
connectAttr "IKXHip_R.t" "PoleAimLeg_R_pointConstraint1.tg[0].tt";
connectAttr "IKXHip_R.rp" "PoleAimLeg_R_pointConstraint1.tg[0].trp";
connectAttr "IKXHip_R.rpt" "PoleAimLeg_R_pointConstraint1.tg[0].trt";
connectAttr "IKXHip_R.pm" "PoleAimLeg_R_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_pointConstraint1.w0" "PoleAimLeg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctx" "PoleParentConstraintLeg_R.tx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cty" "PoleParentConstraintLeg_R.ty"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctz" "PoleParentConstraintLeg_R.tz"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crx" "PoleParentConstraintLeg_R.rx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cry" "PoleParentConstraintLeg_R.ry"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crz" "PoleParentConstraintLeg_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "PoleParentConstraintLeg_R.v";
connectAttr "PoleParentConstraintLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_R.pim" "PoleParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_RStatic.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_RStatic.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_RStatic.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w0" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_R.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_R.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_R.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_R.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w1" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_RSetRangeFollow.oy" "PoleParentConstraintLeg_R_parentConstraint1.w0"
		;
connectAttr "PoleLeg_RSetRangeFollow.ox" "PoleParentConstraintLeg_R_parentConstraint1.w1"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintLeg_L.v";
connectAttr "unitConversion8.o" "IKRollLegHeel_L.rx";
connectAttr "unitConversion10.o" "IKRollLegToe_L.rx";
connectAttr "IKLiftToeLegUnitConversion_L.o" "IKLiftToeLegToe_L.rx";
connectAttr "IKXMiddleToe1_L.msg" "IKXLegHandleToe_L.hsj";
connectAttr "effector5.hp" "IKXLegHandleToe_L.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleToe_L.hsv";
connectAttr "unitConversion9.o" "IKRollLegBall_L.rx";
connectAttr "IKXHip_L.msg" "IKXLegHandle_L.hsj";
connectAttr "effector4.hp" "IKXLegHandle_L.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_L.hsv";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctx" "IKXLegHandle_L.pvx";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.cty" "IKXLegHandle_L.pvy";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctz" "IKXLegHandle_L.pvz";
connectAttr "IKXLegHandle_L.pim" "IKXLegHandle_L_poleVectorConstraint1.cpim";
connectAttr "IKXHip_L.pm" "IKXLegHandle_L_poleVectorConstraint1.ps";
connectAttr "IKXHip_L.t" "IKXLegHandle_L_poleVectorConstraint1.crp";
connectAttr "PoleLeg_L.t" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_L.rp" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_L.rpt" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_L.pm" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.w0" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXAnkle_L.msg" "IKXLegHandleBall_L.hsj";
connectAttr "effector6.hp" "IKXLegHandleBall_L.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_L.hsv";
connectAttr "PoleAimLeg_L_aimConstraint1.crx" "PoleAimLeg_L.rx";
connectAttr "PoleAimLeg_L_aimConstraint1.cry" "PoleAimLeg_L.ry";
connectAttr "PoleAimLeg_L_aimConstraint1.crz" "PoleAimLeg_L.rz";
connectAttr "PoleAimLeg_L_pointConstraint1.ctx" "PoleAimLeg_L.tx";
connectAttr "PoleAimLeg_L_pointConstraint1.cty" "PoleAimLeg_L.ty";
connectAttr "PoleAimLeg_L_pointConstraint1.ctz" "PoleAimLeg_L.tz";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_aimConstraint1.cpim";
connectAttr "PoleAimLeg_L.t" "PoleAimLeg_L_aimConstraint1.ct";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_aimConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.crt";
connectAttr "PoleAimLeg_L.ro" "PoleAimLeg_L_aimConstraint1.cro";
connectAttr "IKLeg_L.t" "PoleAimLeg_L_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_L.rp" "PoleAimLeg_L_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_L.pm" "PoleAimLeg_L_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_aimConstraint1.w0" "PoleAimLeg_L_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.wm" "PoleAimLeg_L_aimConstraint1.wum";
connectAttr "unitConversion7.o" "PoleAimLeg_L_aimConstraint1.ox";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_pointConstraint1.cpim";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_pointConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_pointConstraint1.crt";
connectAttr "IKXHip_L.t" "PoleAimLeg_L_pointConstraint1.tg[0].tt";
connectAttr "IKXHip_L.rp" "PoleAimLeg_L_pointConstraint1.tg[0].trp";
connectAttr "IKXHip_L.rpt" "PoleAimLeg_L_pointConstraint1.tg[0].trt";
connectAttr "IKXHip_L.pm" "PoleAimLeg_L_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_pointConstraint1.w0" "PoleAimLeg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctx" "PoleParentConstraintLeg_L.tx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cty" "PoleParentConstraintLeg_L.ty"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctz" "PoleParentConstraintLeg_L.tz"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crx" "PoleParentConstraintLeg_L.rx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cry" "PoleParentConstraintLeg_L.ry"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crz" "PoleParentConstraintLeg_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "PoleParentConstraintLeg_L.v";
connectAttr "PoleParentConstraintLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_L.pim" "PoleParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_LStatic.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_LStatic.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_LStatic.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w0" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_L.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_L.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_L.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_L.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w1" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_LSetRangeFollow.oy" "PoleParentConstraintLeg_L_parentConstraint1.w0"
		;
connectAttr "PoleLeg_LSetRangeFollow.ox" "PoleParentConstraintLeg_L_parentConstraint1.w1"
		;
connectAttr "Main.fkIkVis" "FKIKSystem.v";
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctx" "FKIKParentConstraintLeg_R.tx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cty" "FKIKParentConstraintLeg_R.ty"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctz" "FKIKParentConstraintLeg_R.tz"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crx" "FKIKParentConstraintLeg_R.rx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cry" "FKIKParentConstraintLeg_R.ry"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crz" "FKIKParentConstraintLeg_R.rz"
		;
connectAttr "FKIKParentConstraintLeg_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_R.pim" "FKIKParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "HipTwist_R.t" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "HipTwist_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_R.r" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "HipTwist_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_R.s" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "HipTwist_R.pm" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_R.jo" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.w0" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctx" "FKIKParentConstraintLeg_L.tx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cty" "FKIKParentConstraintLeg_L.ty"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctz" "FKIKParentConstraintLeg_L.tz"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crx" "FKIKParentConstraintLeg_L.rx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cry" "FKIKParentConstraintLeg_L.ry"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crz" "FKIKParentConstraintLeg_L.rz"
		;
connectAttr "FKIKParentConstraintLeg_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_L.pim" "FKIKParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "HipTwist_L.t" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "HipTwist_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_L.r" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "HipTwist_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_L.s" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "HipTwist_L.pm" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_L.jo" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.w0" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.cry" "PelvisCenterBtwLegs_M.ry"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctx" "PelvisCenterBtwLegs_M.tx"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctz" "PelvisCenterBtwLegs_M.tz"
		;
connectAttr "PelvisCenterBtwLegs_M.ro" "PelvisCenterBtwLegs_M_orientConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_orientConstraint1.cpim"
		;
connectAttr "IKLeg_R.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w0" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tr";
connectAttr "IKLeg_L.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tro";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w1" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tw"
		;
connectAttr "CenterBtwFeet_M.ox" "PelvisCenterBtwLegs_M_pointConstraint1.nds";
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_pointConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegs_M.rp" "PelvisCenterBtwLegs_M_pointConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegs_M.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.crt"
		;
connectAttr "IKLeg_R.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w0" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tt";
connectAttr "IKLeg_L.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trp";
connectAttr "IKLeg_L.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trt";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w1" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tw"
		;
connectAttr "FKPelvis_M.s" "Pelvis_M.s";
connectAttr "Pelvis_M_pointConstraint1.ctx" "Pelvis_M.tx";
connectAttr "Pelvis_M_pointConstraint1.cty" "Pelvis_M.ty";
connectAttr "Pelvis_M_pointConstraint1.ctz" "Pelvis_M.tz";
connectAttr "Pelvis_M_orientConstraint1.crx" "Pelvis_M.rx";
connectAttr "Pelvis_M_orientConstraint1.cry" "Pelvis_M.ry";
connectAttr "Pelvis_M_orientConstraint1.crz" "Pelvis_M.rz";
connectAttr "jointLayer.di" "Pelvis_M.do";
connectAttr "HipTwist_R_parentConstraint1.ctx" "HipTwist_R.tx";
connectAttr "HipTwist_R_parentConstraint1.cty" "HipTwist_R.ty";
connectAttr "HipTwist_R_parentConstraint1.ctz" "HipTwist_R.tz";
connectAttr "HipTwist_R_parentConstraint1.crx" "HipTwist_R.rx";
connectAttr "HipTwist_R_parentConstraint1.cry" "HipTwist_R.ry";
connectAttr "HipTwist_R_parentConstraint1.crz" "HipTwist_R.rz";
connectAttr "FKHipTwist_R.s" "HipTwist_R.s";
connectAttr "Pelvis_M.s" "HipTwist_R.is";
connectAttr "jointLayer.di" "HipTwist_R.do";
connectAttr "Hip_R_parentConstraint1.ctx" "Hip_R.tx";
connectAttr "Hip_R_parentConstraint1.cty" "Hip_R.ty";
connectAttr "Hip_R_parentConstraint1.ctz" "Hip_R.tz";
connectAttr "Hip_R_parentConstraint1.crx" "Hip_R.rx";
connectAttr "Hip_R_parentConstraint1.cry" "Hip_R.ry";
connectAttr "Hip_R_parentConstraint1.crz" "Hip_R.rz";
connectAttr "ScaleBlendHip_R.op" "Hip_R.s";
connectAttr "jointLayer.di" "Hip_R.do";
connectAttr "ScaleBlendKnee_R.op" "Knee_R.s";
connectAttr "Hip_R.s" "Knee_R.is";
connectAttr "Knee_R_parentConstraint1.ctx" "Knee_R.tx";
connectAttr "Knee_R_parentConstraint1.cty" "Knee_R.ty";
connectAttr "Knee_R_parentConstraint1.ctz" "Knee_R.tz";
connectAttr "Knee_R_parentConstraint1.crx" "Knee_R.rx";
connectAttr "Knee_R_parentConstraint1.cry" "Knee_R.ry";
connectAttr "Knee_R_parentConstraint1.crz" "Knee_R.rz";
connectAttr "jointLayer.di" "Knee_R.do";
connectAttr "Ankle_R_parentConstraint1.ctx" "Ankle_R.tx";
connectAttr "Ankle_R_parentConstraint1.cty" "Ankle_R.ty";
connectAttr "Ankle_R_parentConstraint1.ctz" "Ankle_R.tz";
connectAttr "Ankle_R_parentConstraint1.crx" "Ankle_R.rx";
connectAttr "Ankle_R_parentConstraint1.cry" "Ankle_R.ry";
connectAttr "Ankle_R_parentConstraint1.crz" "Ankle_R.rz";
connectAttr "ScaleBlendAnkle_R.op" "Ankle_R.s";
connectAttr "Knee_R.s" "Ankle_R.is";
connectAttr "jointLayer.di" "Ankle_R.do";
connectAttr "FKMiddleToe1_R.s" "MiddleToe1_R.s";
connectAttr "Ankle_R.s" "MiddleToe1_R.is";
connectAttr "MiddleToe1_R_parentConstraint1.ctx" "MiddleToe1_R.tx";
connectAttr "MiddleToe1_R_parentConstraint1.cty" "MiddleToe1_R.ty";
connectAttr "MiddleToe1_R_parentConstraint1.ctz" "MiddleToe1_R.tz";
connectAttr "MiddleToe1_R_parentConstraint1.crx" "MiddleToe1_R.rx";
connectAttr "MiddleToe1_R_parentConstraint1.cry" "MiddleToe1_R.ry";
connectAttr "MiddleToe1_R_parentConstraint1.crz" "MiddleToe1_R.rz";
connectAttr "jointLayer.di" "MiddleToe1_R.do";
connectAttr "MiddleToe1_R.s" "MiddleToe2_End_R.is";
connectAttr "jointLayer.di" "MiddleToe2_End_R.do";
connectAttr "MiddleToe1_R.ro" "MiddleToe1_R_parentConstraint1.cro";
connectAttr "MiddleToe1_R.pim" "MiddleToe1_R_parentConstraint1.cpim";
connectAttr "MiddleToe1_R.rp" "MiddleToe1_R_parentConstraint1.crp";
connectAttr "MiddleToe1_R.rpt" "MiddleToe1_R_parentConstraint1.crt";
connectAttr "MiddleToe1_R.jo" "MiddleToe1_R_parentConstraint1.cjo";
connectAttr "FKXMiddleToe1_R.t" "MiddleToe1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleToe1_R.rp" "MiddleToe1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXMiddleToe1_R.rpt" "MiddleToe1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXMiddleToe1_R.r" "MiddleToe1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleToe1_R.ro" "MiddleToe1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXMiddleToe1_R.s" "MiddleToe1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleToe1_R.pm" "MiddleToe1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXMiddleToe1_R.jo" "MiddleToe1_R_parentConstraint1.tg[0].tjo";
connectAttr "MiddleToe1_R_parentConstraint1.w0" "MiddleToe1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Ankle_R.ro" "Ankle_R_parentConstraint1.cro";
connectAttr "Ankle_R.pim" "Ankle_R_parentConstraint1.cpim";
connectAttr "Ankle_R.rp" "Ankle_R_parentConstraint1.crp";
connectAttr "Ankle_R.rpt" "Ankle_R_parentConstraint1.crt";
connectAttr "Ankle_R.jo" "Ankle_R_parentConstraint1.cjo";
connectAttr "FKXAnkle_R.t" "Ankle_R_parentConstraint1.tg[0].tt";
connectAttr "FKXAnkle_R.rp" "Ankle_R_parentConstraint1.tg[0].trp";
connectAttr "FKXAnkle_R.rpt" "Ankle_R_parentConstraint1.tg[0].trt";
connectAttr "FKXAnkle_R.r" "Ankle_R_parentConstraint1.tg[0].tr";
connectAttr "FKXAnkle_R.ro" "Ankle_R_parentConstraint1.tg[0].tro";
connectAttr "FKXAnkle_R.s" "Ankle_R_parentConstraint1.tg[0].ts";
connectAttr "FKXAnkle_R.pm" "Ankle_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXAnkle_R.jo" "Ankle_R_parentConstraint1.tg[0].tjo";
connectAttr "Ankle_R_parentConstraint1.w0" "Ankle_R_parentConstraint1.tg[0].tw";
connectAttr "IKXAnkle_R.t" "Ankle_R_parentConstraint1.tg[1].tt";
connectAttr "IKXAnkle_R.rp" "Ankle_R_parentConstraint1.tg[1].trp";
connectAttr "IKXAnkle_R.rpt" "Ankle_R_parentConstraint1.tg[1].trt";
connectAttr "IKXAnkle_R.r" "Ankle_R_parentConstraint1.tg[1].tr";
connectAttr "IKXAnkle_R.ro" "Ankle_R_parentConstraint1.tg[1].tro";
connectAttr "IKXAnkle_R.s" "Ankle_R_parentConstraint1.tg[1].ts";
connectAttr "IKXAnkle_R.pm" "Ankle_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXAnkle_R.jo" "Ankle_R_parentConstraint1.tg[1].tjo";
connectAttr "Ankle_R_parentConstraint1.w1" "Ankle_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Ankle_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Ankle_R_parentConstraint1.w1";
connectAttr "Knee_R.ro" "Knee_R_parentConstraint1.cro";
connectAttr "Knee_R.pim" "Knee_R_parentConstraint1.cpim";
connectAttr "Knee_R.rp" "Knee_R_parentConstraint1.crp";
connectAttr "Knee_R.rpt" "Knee_R_parentConstraint1.crt";
connectAttr "Knee_R.jo" "Knee_R_parentConstraint1.cjo";
connectAttr "FKXKnee_R.t" "Knee_R_parentConstraint1.tg[0].tt";
connectAttr "FKXKnee_R.rp" "Knee_R_parentConstraint1.tg[0].trp";
connectAttr "FKXKnee_R.rpt" "Knee_R_parentConstraint1.tg[0].trt";
connectAttr "FKXKnee_R.r" "Knee_R_parentConstraint1.tg[0].tr";
connectAttr "FKXKnee_R.ro" "Knee_R_parentConstraint1.tg[0].tro";
connectAttr "FKXKnee_R.s" "Knee_R_parentConstraint1.tg[0].ts";
connectAttr "FKXKnee_R.pm" "Knee_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXKnee_R.jo" "Knee_R_parentConstraint1.tg[0].tjo";
connectAttr "Knee_R_parentConstraint1.w0" "Knee_R_parentConstraint1.tg[0].tw";
connectAttr "IKXKnee_R.t" "Knee_R_parentConstraint1.tg[1].tt";
connectAttr "IKXKnee_R.rp" "Knee_R_parentConstraint1.tg[1].trp";
connectAttr "IKXKnee_R.rpt" "Knee_R_parentConstraint1.tg[1].trt";
connectAttr "IKXKnee_R.r" "Knee_R_parentConstraint1.tg[1].tr";
connectAttr "IKXKnee_R.ro" "Knee_R_parentConstraint1.tg[1].tro";
connectAttr "IKXKnee_R.s" "Knee_R_parentConstraint1.tg[1].ts";
connectAttr "IKXKnee_R.pm" "Knee_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXKnee_R.jo" "Knee_R_parentConstraint1.tg[1].tjo";
connectAttr "Knee_R_parentConstraint1.w1" "Knee_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Knee_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Knee_R_parentConstraint1.w1";
connectAttr "FKElbowShield_R.s" "ElbowShield_R.s";
connectAttr "Hip_R.s" "ElbowShield_R.is";
connectAttr "ElbowShield_R_parentConstraint1.ctx" "ElbowShield_R.tx";
connectAttr "ElbowShield_R_parentConstraint1.cty" "ElbowShield_R.ty";
connectAttr "ElbowShield_R_parentConstraint1.ctz" "ElbowShield_R.tz";
connectAttr "ElbowShield_R_parentConstraint1.crx" "ElbowShield_R.rx";
connectAttr "ElbowShield_R_parentConstraint1.cry" "ElbowShield_R.ry";
connectAttr "ElbowShield_R_parentConstraint1.crz" "ElbowShield_R.rz";
connectAttr "jointLayer.di" "ElbowShield_R.do";
connectAttr "jointLayer.di" "ElbowShield_End_R.do";
connectAttr "ElbowShield_R.ro" "ElbowShield_R_parentConstraint1.cro";
connectAttr "ElbowShield_R.pim" "ElbowShield_R_parentConstraint1.cpim";
connectAttr "ElbowShield_R.rp" "ElbowShield_R_parentConstraint1.crp";
connectAttr "ElbowShield_R.rpt" "ElbowShield_R_parentConstraint1.crt";
connectAttr "ElbowShield_R.jo" "ElbowShield_R_parentConstraint1.cjo";
connectAttr "FKXElbowShield_R.t" "ElbowShield_R_parentConstraint1.tg[0].tt";
connectAttr "FKXElbowShield_R.rp" "ElbowShield_R_parentConstraint1.tg[0].trp";
connectAttr "FKXElbowShield_R.rpt" "ElbowShield_R_parentConstraint1.tg[0].trt";
connectAttr "FKXElbowShield_R.r" "ElbowShield_R_parentConstraint1.tg[0].tr";
connectAttr "FKXElbowShield_R.ro" "ElbowShield_R_parentConstraint1.tg[0].tro";
connectAttr "FKXElbowShield_R.s" "ElbowShield_R_parentConstraint1.tg[0].ts";
connectAttr "FKXElbowShield_R.pm" "ElbowShield_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbowShield_R.jo" "ElbowShield_R_parentConstraint1.tg[0].tjo";
connectAttr "ElbowShield_R_parentConstraint1.w0" "ElbowShield_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Hip_R.ro" "Hip_R_parentConstraint1.cro";
connectAttr "Hip_R.pim" "Hip_R_parentConstraint1.cpim";
connectAttr "Hip_R.rp" "Hip_R_parentConstraint1.crp";
connectAttr "Hip_R.rpt" "Hip_R_parentConstraint1.crt";
connectAttr "Hip_R.jo" "Hip_R_parentConstraint1.cjo";
connectAttr "FKXHip_R.t" "Hip_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHip_R.rp" "Hip_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHip_R.rpt" "Hip_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHip_R.r" "Hip_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHip_R.ro" "Hip_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHip_R.s" "Hip_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHip_R.pm" "Hip_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHip_R.jo" "Hip_R_parentConstraint1.tg[0].tjo";
connectAttr "Hip_R_parentConstraint1.w0" "Hip_R_parentConstraint1.tg[0].tw";
connectAttr "IKXHip_R.t" "Hip_R_parentConstraint1.tg[1].tt";
connectAttr "IKXHip_R.rp" "Hip_R_parentConstraint1.tg[1].trp";
connectAttr "IKXHip_R.rpt" "Hip_R_parentConstraint1.tg[1].trt";
connectAttr "IKXHip_R.r" "Hip_R_parentConstraint1.tg[1].tr";
connectAttr "IKXHip_R.ro" "Hip_R_parentConstraint1.tg[1].tro";
connectAttr "IKXHip_R.s" "Hip_R_parentConstraint1.tg[1].ts";
connectAttr "IKXHip_R.pm" "Hip_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXHip_R.jo" "Hip_R_parentConstraint1.tg[1].tjo";
connectAttr "Hip_R_parentConstraint1.w1" "Hip_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Hip_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Hip_R_parentConstraint1.w1";
connectAttr "HipTwist_R.ro" "HipTwist_R_parentConstraint1.cro";
connectAttr "HipTwist_R.pim" "HipTwist_R_parentConstraint1.cpim";
connectAttr "HipTwist_R.rp" "HipTwist_R_parentConstraint1.crp";
connectAttr "HipTwist_R.rpt" "HipTwist_R_parentConstraint1.crt";
connectAttr "HipTwist_R.jo" "HipTwist_R_parentConstraint1.cjo";
connectAttr "FKXHipTwist_R.t" "HipTwist_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHipTwist_R.rp" "HipTwist_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHipTwist_R.rpt" "HipTwist_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHipTwist_R.r" "HipTwist_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHipTwist_R.ro" "HipTwist_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHipTwist_R.s" "HipTwist_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHipTwist_R.pm" "HipTwist_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHipTwist_R.jo" "HipTwist_R_parentConstraint1.tg[0].tjo";
connectAttr "HipTwist_R_parentConstraint1.w0" "HipTwist_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKHead_M.s" "Head_M.s";
connectAttr "Pelvis_M.s" "Head_M.is";
connectAttr "Head_M_parentConstraint1.ctx" "Head_M.tx";
connectAttr "Head_M_parentConstraint1.cty" "Head_M.ty";
connectAttr "Head_M_parentConstraint1.ctz" "Head_M.tz";
connectAttr "Head_M_parentConstraint1.crx" "Head_M.rx";
connectAttr "Head_M_parentConstraint1.cry" "Head_M.ry";
connectAttr "Head_M_parentConstraint1.crz" "Head_M.rz";
connectAttr "jointLayer.di" "Head_M.do";
connectAttr "Head_M.s" "Head_End_M.is";
connectAttr "jointLayer.di" "Head_End_M.do";
connectAttr "FKJaw_M.s" "Jaw_M.s";
connectAttr "Head_M.s" "Jaw_M.is";
connectAttr "Jaw_M_parentConstraint1.ctx" "Jaw_M.tx";
connectAttr "Jaw_M_parentConstraint1.cty" "Jaw_M.ty";
connectAttr "Jaw_M_parentConstraint1.ctz" "Jaw_M.tz";
connectAttr "Jaw_M_parentConstraint1.crx" "Jaw_M.rx";
connectAttr "Jaw_M_parentConstraint1.cry" "Jaw_M.ry";
connectAttr "Jaw_M_parentConstraint1.crz" "Jaw_M.rz";
connectAttr "jointLayer.di" "Jaw_M.do";
connectAttr "Jaw_M.s" "Jaw_End_M.is";
connectAttr "jointLayer.di" "Jaw_End_M.do";
connectAttr "Jaw_M.ro" "Jaw_M_parentConstraint1.cro";
connectAttr "Jaw_M.pim" "Jaw_M_parentConstraint1.cpim";
connectAttr "Jaw_M.rp" "Jaw_M_parentConstraint1.crp";
connectAttr "Jaw_M.rpt" "Jaw_M_parentConstraint1.crt";
connectAttr "Jaw_M.jo" "Jaw_M_parentConstraint1.cjo";
connectAttr "FKXJaw_M.t" "Jaw_M_parentConstraint1.tg[0].tt";
connectAttr "FKXJaw_M.rp" "Jaw_M_parentConstraint1.tg[0].trp";
connectAttr "FKXJaw_M.rpt" "Jaw_M_parentConstraint1.tg[0].trt";
connectAttr "FKXJaw_M.r" "Jaw_M_parentConstraint1.tg[0].tr";
connectAttr "FKXJaw_M.ro" "Jaw_M_parentConstraint1.tg[0].tro";
connectAttr "FKXJaw_M.s" "Jaw_M_parentConstraint1.tg[0].ts";
connectAttr "FKXJaw_M.pm" "Jaw_M_parentConstraint1.tg[0].tpm";
connectAttr "FKXJaw_M.jo" "Jaw_M_parentConstraint1.tg[0].tjo";
connectAttr "Jaw_M_parentConstraint1.w0" "Jaw_M_parentConstraint1.tg[0].tw";
connectAttr "Head_M.ro" "Head_M_parentConstraint1.cro";
connectAttr "Head_M.pim" "Head_M_parentConstraint1.cpim";
connectAttr "Head_M.rp" "Head_M_parentConstraint1.crp";
connectAttr "Head_M.rpt" "Head_M_parentConstraint1.crt";
connectAttr "Head_M.jo" "Head_M_parentConstraint1.cjo";
connectAttr "FKXHead_M.t" "Head_M_parentConstraint1.tg[0].tt";
connectAttr "FKXHead_M.rp" "Head_M_parentConstraint1.tg[0].trp";
connectAttr "FKXHead_M.rpt" "Head_M_parentConstraint1.tg[0].trt";
connectAttr "FKXHead_M.r" "Head_M_parentConstraint1.tg[0].tr";
connectAttr "FKXHead_M.ro" "Head_M_parentConstraint1.tg[0].tro";
connectAttr "FKXHead_M.s" "Head_M_parentConstraint1.tg[0].ts";
connectAttr "FKXHead_M.pm" "Head_M_parentConstraint1.tg[0].tpm";
connectAttr "FKXHead_M.jo" "Head_M_parentConstraint1.tg[0].tjo";
connectAttr "Head_M_parentConstraint1.w0" "Head_M_parentConstraint1.tg[0].tw";
connectAttr "FKShoulder_R.s" "Shoulder_R.s";
connectAttr "Pelvis_M.s" "Shoulder_R.is";
connectAttr "Shoulder_R_parentConstraint1.ctx" "Shoulder_R.tx";
connectAttr "Shoulder_R_parentConstraint1.cty" "Shoulder_R.ty";
connectAttr "Shoulder_R_parentConstraint1.ctz" "Shoulder_R.tz";
connectAttr "Shoulder_R_parentConstraint1.crx" "Shoulder_R.rx";
connectAttr "Shoulder_R_parentConstraint1.cry" "Shoulder_R.ry";
connectAttr "Shoulder_R_parentConstraint1.crz" "Shoulder_R.rz";
connectAttr "jointLayer.di" "Shoulder_R.do";
connectAttr "FKElbow_R.s" "Elbow_R.s";
connectAttr "Shoulder_R.s" "Elbow_R.is";
connectAttr "Elbow_R_parentConstraint1.ctx" "Elbow_R.tx";
connectAttr "Elbow_R_parentConstraint1.cty" "Elbow_R.ty";
connectAttr "Elbow_R_parentConstraint1.ctz" "Elbow_R.tz";
connectAttr "Elbow_R_parentConstraint1.crx" "Elbow_R.rx";
connectAttr "Elbow_R_parentConstraint1.cry" "Elbow_R.ry";
connectAttr "Elbow_R_parentConstraint1.crz" "Elbow_R.rz";
connectAttr "jointLayer.di" "Elbow_R.do";
connectAttr "FKFingers_R.s" "Fingers_R.s";
connectAttr "Elbow_R.s" "Fingers_R.is";
connectAttr "Fingers_R_parentConstraint1.ctx" "Fingers_R.tx";
connectAttr "Fingers_R_parentConstraint1.cty" "Fingers_R.ty";
connectAttr "Fingers_R_parentConstraint1.ctz" "Fingers_R.tz";
connectAttr "Fingers_R_parentConstraint1.crx" "Fingers_R.rx";
connectAttr "Fingers_R_parentConstraint1.cry" "Fingers_R.ry";
connectAttr "Fingers_R_parentConstraint1.crz" "Fingers_R.rz";
connectAttr "jointLayer.di" "Fingers_R.do";
connectAttr "jointLayer.di" "Fingers_End_R.do";
connectAttr "Fingers_R.ro" "Fingers_R_parentConstraint1.cro";
connectAttr "Fingers_R.pim" "Fingers_R_parentConstraint1.cpim";
connectAttr "Fingers_R.rp" "Fingers_R_parentConstraint1.crp";
connectAttr "Fingers_R.rpt" "Fingers_R_parentConstraint1.crt";
connectAttr "Fingers_R.jo" "Fingers_R_parentConstraint1.cjo";
connectAttr "FKXFingers_R.t" "Fingers_R_parentConstraint1.tg[0].tt";
connectAttr "FKXFingers_R.rp" "Fingers_R_parentConstraint1.tg[0].trp";
connectAttr "FKXFingers_R.rpt" "Fingers_R_parentConstraint1.tg[0].trt";
connectAttr "FKXFingers_R.r" "Fingers_R_parentConstraint1.tg[0].tr";
connectAttr "FKXFingers_R.ro" "Fingers_R_parentConstraint1.tg[0].tro";
connectAttr "FKXFingers_R.s" "Fingers_R_parentConstraint1.tg[0].ts";
connectAttr "FKXFingers_R.pm" "Fingers_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXFingers_R.jo" "Fingers_R_parentConstraint1.tg[0].tjo";
connectAttr "Fingers_R_parentConstraint1.w0" "Fingers_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Elbow_R.ro" "Elbow_R_parentConstraint1.cro";
connectAttr "Elbow_R.pim" "Elbow_R_parentConstraint1.cpim";
connectAttr "Elbow_R.rp" "Elbow_R_parentConstraint1.crp";
connectAttr "Elbow_R.rpt" "Elbow_R_parentConstraint1.crt";
connectAttr "Elbow_R.jo" "Elbow_R_parentConstraint1.cjo";
connectAttr "FKXElbow_R.t" "Elbow_R_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow_R.rp" "Elbow_R_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow_R.rpt" "Elbow_R_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow_R.r" "Elbow_R_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow_R.ro" "Elbow_R_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow_R.s" "Elbow_R_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow_R.pm" "Elbow_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow_R.jo" "Elbow_R_parentConstraint1.tg[0].tjo";
connectAttr "Elbow_R_parentConstraint1.w0" "Elbow_R_parentConstraint1.tg[0].tw";
connectAttr "Shoulder_R.ro" "Shoulder_R_parentConstraint1.cro";
connectAttr "Shoulder_R.pim" "Shoulder_R_parentConstraint1.cpim";
connectAttr "Shoulder_R.rp" "Shoulder_R_parentConstraint1.crp";
connectAttr "Shoulder_R.rpt" "Shoulder_R_parentConstraint1.crt";
connectAttr "Shoulder_R.jo" "Shoulder_R_parentConstraint1.cjo";
connectAttr "FKXShoulder_R.t" "Shoulder_R_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder_R.rp" "Shoulder_R_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder_R.rpt" "Shoulder_R_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder_R.r" "Shoulder_R_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder_R.ro" "Shoulder_R_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder_R.s" "Shoulder_R_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder_R.pm" "Shoulder_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder_R.jo" "Shoulder_R_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder_R_parentConstraint1.w0" "Shoulder_R_parentConstraint1.tg[0].tw"
		;
connectAttr "HipTwist_L_parentConstraint1.ctx" "HipTwist_L.tx";
connectAttr "HipTwist_L_parentConstraint1.cty" "HipTwist_L.ty";
connectAttr "HipTwist_L_parentConstraint1.ctz" "HipTwist_L.tz";
connectAttr "HipTwist_L_parentConstraint1.crx" "HipTwist_L.rx";
connectAttr "HipTwist_L_parentConstraint1.cry" "HipTwist_L.ry";
connectAttr "HipTwist_L_parentConstraint1.crz" "HipTwist_L.rz";
connectAttr "FKHipTwist_L.s" "HipTwist_L.s";
connectAttr "Pelvis_M.s" "HipTwist_L.is";
connectAttr "jointLayer.di" "HipTwist_L.do";
connectAttr "Hip_L_parentConstraint1.ctx" "Hip_L.tx";
connectAttr "Hip_L_parentConstraint1.cty" "Hip_L.ty";
connectAttr "Hip_L_parentConstraint1.ctz" "Hip_L.tz";
connectAttr "Hip_L_parentConstraint1.crx" "Hip_L.rx";
connectAttr "Hip_L_parentConstraint1.cry" "Hip_L.ry";
connectAttr "Hip_L_parentConstraint1.crz" "Hip_L.rz";
connectAttr "ScaleBlendHip_L.op" "Hip_L.s";
connectAttr "jointLayer.di" "Hip_L.do";
connectAttr "ScaleBlendKnee_L.op" "Knee_L.s";
connectAttr "Hip_L.s" "Knee_L.is";
connectAttr "Knee_L_parentConstraint1.ctx" "Knee_L.tx";
connectAttr "Knee_L_parentConstraint1.cty" "Knee_L.ty";
connectAttr "Knee_L_parentConstraint1.ctz" "Knee_L.tz";
connectAttr "Knee_L_parentConstraint1.crx" "Knee_L.rx";
connectAttr "Knee_L_parentConstraint1.cry" "Knee_L.ry";
connectAttr "Knee_L_parentConstraint1.crz" "Knee_L.rz";
connectAttr "jointLayer.di" "Knee_L.do";
connectAttr "Ankle_L_parentConstraint1.ctx" "Ankle_L.tx";
connectAttr "Ankle_L_parentConstraint1.cty" "Ankle_L.ty";
connectAttr "Ankle_L_parentConstraint1.ctz" "Ankle_L.tz";
connectAttr "Ankle_L_parentConstraint1.crx" "Ankle_L.rx";
connectAttr "Ankle_L_parentConstraint1.cry" "Ankle_L.ry";
connectAttr "Ankle_L_parentConstraint1.crz" "Ankle_L.rz";
connectAttr "ScaleBlendAnkle_L.op" "Ankle_L.s";
connectAttr "Knee_L.s" "Ankle_L.is";
connectAttr "jointLayer.di" "Ankle_L.do";
connectAttr "FKMiddleToe1_L.s" "MiddleToe1_L.s";
connectAttr "Ankle_L.s" "MiddleToe1_L.is";
connectAttr "MiddleToe1_L_parentConstraint1.ctx" "MiddleToe1_L.tx";
connectAttr "MiddleToe1_L_parentConstraint1.cty" "MiddleToe1_L.ty";
connectAttr "MiddleToe1_L_parentConstraint1.ctz" "MiddleToe1_L.tz";
connectAttr "MiddleToe1_L_parentConstraint1.crx" "MiddleToe1_L.rx";
connectAttr "MiddleToe1_L_parentConstraint1.cry" "MiddleToe1_L.ry";
connectAttr "MiddleToe1_L_parentConstraint1.crz" "MiddleToe1_L.rz";
connectAttr "jointLayer.di" "MiddleToe1_L.do";
connectAttr "MiddleToe1_L.s" "MiddleToe2_End_L.is";
connectAttr "jointLayer.di" "MiddleToe2_End_L.do";
connectAttr "MiddleToe1_L.ro" "MiddleToe1_L_parentConstraint1.cro";
connectAttr "MiddleToe1_L.pim" "MiddleToe1_L_parentConstraint1.cpim";
connectAttr "MiddleToe1_L.rp" "MiddleToe1_L_parentConstraint1.crp";
connectAttr "MiddleToe1_L.rpt" "MiddleToe1_L_parentConstraint1.crt";
connectAttr "MiddleToe1_L.jo" "MiddleToe1_L_parentConstraint1.cjo";
connectAttr "FKXMiddleToe1_L.t" "MiddleToe1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleToe1_L.rp" "MiddleToe1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXMiddleToe1_L.rpt" "MiddleToe1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXMiddleToe1_L.r" "MiddleToe1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleToe1_L.ro" "MiddleToe1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXMiddleToe1_L.s" "MiddleToe1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleToe1_L.pm" "MiddleToe1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXMiddleToe1_L.jo" "MiddleToe1_L_parentConstraint1.tg[0].tjo";
connectAttr "MiddleToe1_L_parentConstraint1.w0" "MiddleToe1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Ankle_L.ro" "Ankle_L_parentConstraint1.cro";
connectAttr "Ankle_L.pim" "Ankle_L_parentConstraint1.cpim";
connectAttr "Ankle_L.rp" "Ankle_L_parentConstraint1.crp";
connectAttr "Ankle_L.rpt" "Ankle_L_parentConstraint1.crt";
connectAttr "Ankle_L.jo" "Ankle_L_parentConstraint1.cjo";
connectAttr "FKXAnkle_L.t" "Ankle_L_parentConstraint1.tg[0].tt";
connectAttr "FKXAnkle_L.rp" "Ankle_L_parentConstraint1.tg[0].trp";
connectAttr "FKXAnkle_L.rpt" "Ankle_L_parentConstraint1.tg[0].trt";
connectAttr "FKXAnkle_L.r" "Ankle_L_parentConstraint1.tg[0].tr";
connectAttr "FKXAnkle_L.ro" "Ankle_L_parentConstraint1.tg[0].tro";
connectAttr "FKXAnkle_L.s" "Ankle_L_parentConstraint1.tg[0].ts";
connectAttr "FKXAnkle_L.pm" "Ankle_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXAnkle_L.jo" "Ankle_L_parentConstraint1.tg[0].tjo";
connectAttr "Ankle_L_parentConstraint1.w0" "Ankle_L_parentConstraint1.tg[0].tw";
connectAttr "IKXAnkle_L.t" "Ankle_L_parentConstraint1.tg[1].tt";
connectAttr "IKXAnkle_L.rp" "Ankle_L_parentConstraint1.tg[1].trp";
connectAttr "IKXAnkle_L.rpt" "Ankle_L_parentConstraint1.tg[1].trt";
connectAttr "IKXAnkle_L.r" "Ankle_L_parentConstraint1.tg[1].tr";
connectAttr "IKXAnkle_L.ro" "Ankle_L_parentConstraint1.tg[1].tro";
connectAttr "IKXAnkle_L.s" "Ankle_L_parentConstraint1.tg[1].ts";
connectAttr "IKXAnkle_L.pm" "Ankle_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXAnkle_L.jo" "Ankle_L_parentConstraint1.tg[1].tjo";
connectAttr "Ankle_L_parentConstraint1.w1" "Ankle_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Ankle_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Ankle_L_parentConstraint1.w1";
connectAttr "Knee_L.ro" "Knee_L_parentConstraint1.cro";
connectAttr "Knee_L.pim" "Knee_L_parentConstraint1.cpim";
connectAttr "Knee_L.rp" "Knee_L_parentConstraint1.crp";
connectAttr "Knee_L.rpt" "Knee_L_parentConstraint1.crt";
connectAttr "Knee_L.jo" "Knee_L_parentConstraint1.cjo";
connectAttr "FKXKnee_L.t" "Knee_L_parentConstraint1.tg[0].tt";
connectAttr "FKXKnee_L.rp" "Knee_L_parentConstraint1.tg[0].trp";
connectAttr "FKXKnee_L.rpt" "Knee_L_parentConstraint1.tg[0].trt";
connectAttr "FKXKnee_L.r" "Knee_L_parentConstraint1.tg[0].tr";
connectAttr "FKXKnee_L.ro" "Knee_L_parentConstraint1.tg[0].tro";
connectAttr "FKXKnee_L.s" "Knee_L_parentConstraint1.tg[0].ts";
connectAttr "FKXKnee_L.pm" "Knee_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXKnee_L.jo" "Knee_L_parentConstraint1.tg[0].tjo";
connectAttr "Knee_L_parentConstraint1.w0" "Knee_L_parentConstraint1.tg[0].tw";
connectAttr "IKXKnee_L.t" "Knee_L_parentConstraint1.tg[1].tt";
connectAttr "IKXKnee_L.rp" "Knee_L_parentConstraint1.tg[1].trp";
connectAttr "IKXKnee_L.rpt" "Knee_L_parentConstraint1.tg[1].trt";
connectAttr "IKXKnee_L.r" "Knee_L_parentConstraint1.tg[1].tr";
connectAttr "IKXKnee_L.ro" "Knee_L_parentConstraint1.tg[1].tro";
connectAttr "IKXKnee_L.s" "Knee_L_parentConstraint1.tg[1].ts";
connectAttr "IKXKnee_L.pm" "Knee_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXKnee_L.jo" "Knee_L_parentConstraint1.tg[1].tjo";
connectAttr "Knee_L_parentConstraint1.w1" "Knee_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Knee_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Knee_L_parentConstraint1.w1";
connectAttr "FKElbowShield_L.s" "ElbowShield_L.s";
connectAttr "Hip_L.s" "ElbowShield_L.is";
connectAttr "ElbowShield_L_parentConstraint1.ctx" "ElbowShield_L.tx";
connectAttr "ElbowShield_L_parentConstraint1.cty" "ElbowShield_L.ty";
connectAttr "ElbowShield_L_parentConstraint1.ctz" "ElbowShield_L.tz";
connectAttr "ElbowShield_L_parentConstraint1.crx" "ElbowShield_L.rx";
connectAttr "ElbowShield_L_parentConstraint1.cry" "ElbowShield_L.ry";
connectAttr "ElbowShield_L_parentConstraint1.crz" "ElbowShield_L.rz";
connectAttr "jointLayer.di" "ElbowShield_L.do";
connectAttr "jointLayer.di" "ElbowShield_End_L.do";
connectAttr "ElbowShield_L.ro" "ElbowShield_L_parentConstraint1.cro";
connectAttr "ElbowShield_L.pim" "ElbowShield_L_parentConstraint1.cpim";
connectAttr "ElbowShield_L.rp" "ElbowShield_L_parentConstraint1.crp";
connectAttr "ElbowShield_L.rpt" "ElbowShield_L_parentConstraint1.crt";
connectAttr "ElbowShield_L.jo" "ElbowShield_L_parentConstraint1.cjo";
connectAttr "FKXElbowShield_L.t" "ElbowShield_L_parentConstraint1.tg[0].tt";
connectAttr "FKXElbowShield_L.rp" "ElbowShield_L_parentConstraint1.tg[0].trp";
connectAttr "FKXElbowShield_L.rpt" "ElbowShield_L_parentConstraint1.tg[0].trt";
connectAttr "FKXElbowShield_L.r" "ElbowShield_L_parentConstraint1.tg[0].tr";
connectAttr "FKXElbowShield_L.ro" "ElbowShield_L_parentConstraint1.tg[0].tro";
connectAttr "FKXElbowShield_L.s" "ElbowShield_L_parentConstraint1.tg[0].ts";
connectAttr "FKXElbowShield_L.pm" "ElbowShield_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbowShield_L.jo" "ElbowShield_L_parentConstraint1.tg[0].tjo";
connectAttr "ElbowShield_L_parentConstraint1.w0" "ElbowShield_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Hip_L.ro" "Hip_L_parentConstraint1.cro";
connectAttr "Hip_L.pim" "Hip_L_parentConstraint1.cpim";
connectAttr "Hip_L.rp" "Hip_L_parentConstraint1.crp";
connectAttr "Hip_L.rpt" "Hip_L_parentConstraint1.crt";
connectAttr "Hip_L.jo" "Hip_L_parentConstraint1.cjo";
connectAttr "FKXHip_L.t" "Hip_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHip_L.rp" "Hip_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHip_L.rpt" "Hip_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHip_L.r" "Hip_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHip_L.ro" "Hip_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHip_L.s" "Hip_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHip_L.pm" "Hip_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHip_L.jo" "Hip_L_parentConstraint1.tg[0].tjo";
connectAttr "Hip_L_parentConstraint1.w0" "Hip_L_parentConstraint1.tg[0].tw";
connectAttr "IKXHip_L.t" "Hip_L_parentConstraint1.tg[1].tt";
connectAttr "IKXHip_L.rp" "Hip_L_parentConstraint1.tg[1].trp";
connectAttr "IKXHip_L.rpt" "Hip_L_parentConstraint1.tg[1].trt";
connectAttr "IKXHip_L.r" "Hip_L_parentConstraint1.tg[1].tr";
connectAttr "IKXHip_L.ro" "Hip_L_parentConstraint1.tg[1].tro";
connectAttr "IKXHip_L.s" "Hip_L_parentConstraint1.tg[1].ts";
connectAttr "IKXHip_L.pm" "Hip_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXHip_L.jo" "Hip_L_parentConstraint1.tg[1].tjo";
connectAttr "Hip_L_parentConstraint1.w1" "Hip_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Hip_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Hip_L_parentConstraint1.w1";
connectAttr "HipTwist_L.ro" "HipTwist_L_parentConstraint1.cro";
connectAttr "HipTwist_L.pim" "HipTwist_L_parentConstraint1.cpim";
connectAttr "HipTwist_L.rp" "HipTwist_L_parentConstraint1.crp";
connectAttr "HipTwist_L.rpt" "HipTwist_L_parentConstraint1.crt";
connectAttr "HipTwist_L.jo" "HipTwist_L_parentConstraint1.cjo";
connectAttr "FKXHipTwist_L.t" "HipTwist_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHipTwist_L.rp" "HipTwist_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHipTwist_L.rpt" "HipTwist_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHipTwist_L.r" "HipTwist_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHipTwist_L.ro" "HipTwist_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHipTwist_L.s" "HipTwist_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHipTwist_L.pm" "HipTwist_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHipTwist_L.jo" "HipTwist_L_parentConstraint1.tg[0].tjo";
connectAttr "HipTwist_L_parentConstraint1.w0" "HipTwist_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKShoulder_L.s" "Shoulder_L.s";
connectAttr "Pelvis_M.s" "Shoulder_L.is";
connectAttr "Shoulder_L_parentConstraint1.ctx" "Shoulder_L.tx";
connectAttr "Shoulder_L_parentConstraint1.cty" "Shoulder_L.ty";
connectAttr "Shoulder_L_parentConstraint1.ctz" "Shoulder_L.tz";
connectAttr "Shoulder_L_parentConstraint1.crx" "Shoulder_L.rx";
connectAttr "Shoulder_L_parentConstraint1.cry" "Shoulder_L.ry";
connectAttr "Shoulder_L_parentConstraint1.crz" "Shoulder_L.rz";
connectAttr "jointLayer.di" "Shoulder_L.do";
connectAttr "FKElbow_L.s" "Elbow_L.s";
connectAttr "Shoulder_L.s" "Elbow_L.is";
connectAttr "Elbow_L_parentConstraint1.ctx" "Elbow_L.tx";
connectAttr "Elbow_L_parentConstraint1.cty" "Elbow_L.ty";
connectAttr "Elbow_L_parentConstraint1.ctz" "Elbow_L.tz";
connectAttr "Elbow_L_parentConstraint1.crx" "Elbow_L.rx";
connectAttr "Elbow_L_parentConstraint1.cry" "Elbow_L.ry";
connectAttr "Elbow_L_parentConstraint1.crz" "Elbow_L.rz";
connectAttr "jointLayer.di" "Elbow_L.do";
connectAttr "FKFingers_L.s" "Fingers_L.s";
connectAttr "Elbow_L.s" "Fingers_L.is";
connectAttr "Fingers_L_parentConstraint1.ctx" "Fingers_L.tx";
connectAttr "Fingers_L_parentConstraint1.cty" "Fingers_L.ty";
connectAttr "Fingers_L_parentConstraint1.ctz" "Fingers_L.tz";
connectAttr "Fingers_L_parentConstraint1.crx" "Fingers_L.rx";
connectAttr "Fingers_L_parentConstraint1.cry" "Fingers_L.ry";
connectAttr "Fingers_L_parentConstraint1.crz" "Fingers_L.rz";
connectAttr "jointLayer.di" "Fingers_L.do";
connectAttr "jointLayer.di" "Fingers_End_L.do";
connectAttr "Fingers_L.ro" "Fingers_L_parentConstraint1.cro";
connectAttr "Fingers_L.pim" "Fingers_L_parentConstraint1.cpim";
connectAttr "Fingers_L.rp" "Fingers_L_parentConstraint1.crp";
connectAttr "Fingers_L.rpt" "Fingers_L_parentConstraint1.crt";
connectAttr "Fingers_L.jo" "Fingers_L_parentConstraint1.cjo";
connectAttr "FKXFingers_L.t" "Fingers_L_parentConstraint1.tg[0].tt";
connectAttr "FKXFingers_L.rp" "Fingers_L_parentConstraint1.tg[0].trp";
connectAttr "FKXFingers_L.rpt" "Fingers_L_parentConstraint1.tg[0].trt";
connectAttr "FKXFingers_L.r" "Fingers_L_parentConstraint1.tg[0].tr";
connectAttr "FKXFingers_L.ro" "Fingers_L_parentConstraint1.tg[0].tro";
connectAttr "FKXFingers_L.s" "Fingers_L_parentConstraint1.tg[0].ts";
connectAttr "FKXFingers_L.pm" "Fingers_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXFingers_L.jo" "Fingers_L_parentConstraint1.tg[0].tjo";
connectAttr "Fingers_L_parentConstraint1.w0" "Fingers_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Elbow_L.ro" "Elbow_L_parentConstraint1.cro";
connectAttr "Elbow_L.pim" "Elbow_L_parentConstraint1.cpim";
connectAttr "Elbow_L.rp" "Elbow_L_parentConstraint1.crp";
connectAttr "Elbow_L.rpt" "Elbow_L_parentConstraint1.crt";
connectAttr "Elbow_L.jo" "Elbow_L_parentConstraint1.cjo";
connectAttr "FKXElbow_L.t" "Elbow_L_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow_L.rp" "Elbow_L_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow_L.rpt" "Elbow_L_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow_L.r" "Elbow_L_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow_L.ro" "Elbow_L_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow_L.s" "Elbow_L_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow_L.pm" "Elbow_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow_L.jo" "Elbow_L_parentConstraint1.tg[0].tjo";
connectAttr "Elbow_L_parentConstraint1.w0" "Elbow_L_parentConstraint1.tg[0].tw";
connectAttr "Shoulder_L.ro" "Shoulder_L_parentConstraint1.cro";
connectAttr "Shoulder_L.pim" "Shoulder_L_parentConstraint1.cpim";
connectAttr "Shoulder_L.rp" "Shoulder_L_parentConstraint1.crp";
connectAttr "Shoulder_L.rpt" "Shoulder_L_parentConstraint1.crt";
connectAttr "Shoulder_L.jo" "Shoulder_L_parentConstraint1.cjo";
connectAttr "FKXShoulder_L.t" "Shoulder_L_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder_L.rp" "Shoulder_L_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder_L.rpt" "Shoulder_L_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder_L.r" "Shoulder_L_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder_L.ro" "Shoulder_L_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder_L.s" "Shoulder_L_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder_L.pm" "Shoulder_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder_L.jo" "Shoulder_L_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder_L_parentConstraint1.w0" "Shoulder_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Pelvis_M.pim" "Pelvis_M_pointConstraint1.cpim";
connectAttr "Pelvis_M.rp" "Pelvis_M_pointConstraint1.crp";
connectAttr "Pelvis_M.rpt" "Pelvis_M_pointConstraint1.crt";
connectAttr "FKXPelvis_M.t" "Pelvis_M_pointConstraint1.tg[0].tt";
connectAttr "FKXPelvis_M.rp" "Pelvis_M_pointConstraint1.tg[0].trp";
connectAttr "FKXPelvis_M.rpt" "Pelvis_M_pointConstraint1.tg[0].trt";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_pointConstraint1.tg[0].tpm";
connectAttr "Pelvis_M_pointConstraint1.w0" "Pelvis_M_pointConstraint1.tg[0].tw";
connectAttr "Pelvis_M.ro" "Pelvis_M_orientConstraint1.cro";
connectAttr "Pelvis_M.pim" "Pelvis_M_orientConstraint1.cpim";
connectAttr "Pelvis_M.jo" "Pelvis_M_orientConstraint1.cjo";
connectAttr "FKXPelvis_M.r" "Pelvis_M_orientConstraint1.tg[0].tr";
connectAttr "FKXPelvis_M.ro" "Pelvis_M_orientConstraint1.tg[0].tro";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXPelvis_M.jo" "Pelvis_M_orientConstraint1.tg[0].tjo";
connectAttr "Pelvis_M_orientConstraint1.w0" "Pelvis_M_orientConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "irisPuppetSG.msg" "materialInfo3.sg";
connectAttr "BPRig_irisPuppetSG.msg" "BPRig_materialInfo3.sg";
connectAttr "jointVisReverse.ox" "jointLayer.lod";
connectAttr "Main.jointVis" "jointVisReverse.ix";
connectAttr "Main.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleToe1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKAnkle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraAnkle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKElbowShield_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbowShield_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHip_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHip_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHipTwist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHipTwist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKJaw_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraJaw_M.iog" "ControlSet.dsm" -na;
connectAttr "FKHead_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHead_M.iog" "ControlSet.dsm" -na;
connectAttr "FKFingers_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraFingers_R.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow_R.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder_R.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleToe1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKAnkle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraAnkle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKElbowShield_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbowShield_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHip_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHip_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHipTwist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHipTwist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKFingers_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraFingers_L.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegToe_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegToe_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegToe_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegToe_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "ControlSet.dsm" -na;
connectAttr "Center_M.iog" "ControlSet.dsm" -na;
connectAttr "MiddleToe1_R.iog" "GameSet.dsm" -na;
connectAttr "Ankle_R.iog" "GameSet.dsm" -na;
connectAttr "Knee_R.iog" "GameSet.dsm" -na;
connectAttr "ElbowShield_R.iog" "GameSet.dsm" -na;
connectAttr "Hip_R.iog" "GameSet.dsm" -na;
connectAttr "HipTwist_R.iog" "GameSet.dsm" -na;
connectAttr "Jaw_M.iog" "GameSet.dsm" -na;
connectAttr "Head_M.iog" "GameSet.dsm" -na;
connectAttr "Fingers_R.iog" "GameSet.dsm" -na;
connectAttr "Elbow_R.iog" "GameSet.dsm" -na;
connectAttr "Shoulder_R.iog" "GameSet.dsm" -na;
connectAttr "Pelvis_M.iog" "GameSet.dsm" -na;
connectAttr "MiddleToe1_L.iog" "GameSet.dsm" -na;
connectAttr "Ankle_L.iog" "GameSet.dsm" -na;
connectAttr "Knee_L.iog" "GameSet.dsm" -na;
connectAttr "ElbowShield_L.iog" "GameSet.dsm" -na;
connectAttr "Hip_L.iog" "GameSet.dsm" -na;
connectAttr "HipTwist_L.iog" "GameSet.dsm" -na;
connectAttr "Fingers_L.iog" "GameSet.dsm" -na;
connectAttr "Elbow_L.iog" "GameSet.dsm" -na;
connectAttr "Shoulder_L.iog" "GameSet.dsm" -na;
connectAttr "buildPose.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "CenterBtwFeet_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendHip_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendKnee_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAnkle_L.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion12.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion11.msg" "AllSet.dnsm" -na;
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_L.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion10.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion9.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion8.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "Leg_LAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "IKLiftToeLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion7.msg" "AllSet.dnsm" -na;
connectAttr "GlobalHead_reverse_M.msg" "AllSet.dnsm" -na;
connectAttr "GlobalHead_unitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendHip_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendKnee_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAnkle_R.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion6.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion5.msg" "AllSet.dnsm" -na;
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_R.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion4.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion3.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion2.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "Leg_RAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "IKLiftToeLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion1.msg" "AllSet.dnsm" -na;
connectAttr "GameSet.msg" "AllSet.dnsm" -na;
connectAttr "ControlSet.msg" "AllSet.dnsm" -na;
connectAttr "jointVisReverse.msg" "AllSet.dnsm" -na;
connectAttr "jointLayer.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsOffset_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenter_M.iog" "AllSet.dsm" -na;
connectAttr "Center_MShape.iog" "AllSet.dsm" -na;
connectAttr "Center_M.iog" "AllSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "AllSet.dsm" -na;
connectAttr "CenterOffset_M.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Fingers_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Knee_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Ankle_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "MiddleToe1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_L.iog" "AllSet.dsm" -na;
connectAttr "effector6.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKLiftToeLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleToe_L.iog" "AllSet.dsm" -na;
connectAttr "effector5.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_LStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L.iog" "AllSet.dsm" -na;
connectAttr "effector4.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalHead_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "GlobalHead_M.iog" "AllSet.dsm" -na;
connectAttr "GlobalOffsetHead_M.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Fingers_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Head_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Jaw_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Knee_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Ankle_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "MiddleToe1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_R.iog" "AllSet.dsm" -na;
connectAttr "effector3.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKLiftToeLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleToe_R.iog" "AllSet.dsm" -na;
connectAttr "effector2.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_RStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R.iog" "AllSet.dsm" -na;
connectAttr "effector1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKXFingers_L.iog" "AllSet.dsm" -na;
connectAttr "FKFingers_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKFingers_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraFingers_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetFingers_L.iog" "AllSet.dsm" -na;
connectAttr "FKXFingers_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "IKXHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKHip_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbowShield_L.iog" "AllSet.dsm" -na;
connectAttr "FKElbowShield_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbowShield_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbowShield_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbowShield_L.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbowShield_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKXKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKXKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetKnee_L.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKXAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHeel_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe2_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe2_End_L.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_L.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L.iog" "AllSet.dsm" -na;
connectAttr "Fingers_L.iog" "AllSet.dsm" -na;
connectAttr "Fingers_End_L.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "Hip_L.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_L.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_End_L.iog" "AllSet.dsm" -na;
connectAttr "Knee_L.iog" "AllSet.dsm" -na;
connectAttr "Ankle_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe2_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKXPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKXFingers_R.iog" "AllSet.dsm" -na;
connectAttr "FKFingers_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKFingers_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraFingers_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetFingers_R.iog" "AllSet.dsm" -na;
connectAttr "FKXFingers_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKHead_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalStaticHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKXJaw_M.iog" "AllSet.dsm" -na;
connectAttr "FKJaw_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKJaw_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraJaw_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetJaw_M.iog" "AllSet.dsm" -na;
connectAttr "FKXJaw_End_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHead_End_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "IKXHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKHip_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbowShield_R.iog" "AllSet.dsm" -na;
connectAttr "FKElbowShield_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbowShield_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbowShield_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbowShield_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbowShield_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKXKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetKnee_R.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKXAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHeel_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_R.iog" "AllSet.dsm" -na;
connectAttr "Elbow_R.iog" "AllSet.dsm" -na;
connectAttr "Fingers_R.iog" "AllSet.dsm" -na;
connectAttr "Fingers_End_R.iog" "AllSet.dsm" -na;
connectAttr "Head_M.iog" "AllSet.dsm" -na;
connectAttr "Jaw_M.iog" "AllSet.dsm" -na;
connectAttr "Jaw_End_M.iog" "AllSet.dsm" -na;
connectAttr "Head_End_M.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "Hip_R.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_R.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_End_R.iog" "AllSet.dsm" -na;
connectAttr "Knee_R.iog" "AllSet.dsm" -na;
connectAttr "Ankle_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKMessure.iog" "AllSet.dsm" -na;
connectAttr "IKCrv.iog" "AllSet.dsm" -na;
connectAttr "IKStatic.iog" "AllSet.dsm" -na;
connectAttr "IKHandle.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraint.iog" "AllSet.dsm" -na;
connectAttr "GlobalSystem.iog" "AllSet.dsm" -na;
connectAttr "RootSystem.iog" "AllSet.dsm" -na;
connectAttr "FKIKSystem.iog" "AllSet.dsm" -na;
connectAttr "IKSystem.iog" "AllSet.dsm" -na;
connectAttr "FKSystem.iog" "AllSet.dsm" -na;
connectAttr "GameSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MotionSystem.iog" "AllSet.dsm" -na;
connectAttr "BaseSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MainShape.iog" "AllSet.dsm" -na;
connectAttr "Main.iog" "AllSet.dsm" -na;
connectAttr "forwardArrowShape.iog" "AllSet.dsm" -na;
connectAttr "GameSet.msg" "Sets.dnsm" -na;
connectAttr "ControlSet.msg" "Sets.dnsm" -na;
connectAttr "AllSet.msg" "Sets.dnsm" -na;
connectAttr "IKLeg_R.swivel" "unitConversion1.i";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vx";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vy";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegUnitConversion_R.i";
connectAttr "FKIKBlendLegUnitConversion_R.o" "FKIKBlendLegReverse_R.ix";
connectAttr "FKIKLeg_R.autoVis" "FKIKBlendLegCondition_R.ft";
connectAttr "FKIKLeg_R.IKVis" "FKIKBlendLegCondition_R.ctr";
connectAttr "FKIKLeg_R.FKVis" "FKIKBlendLegCondition_R.ctg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegCondition_R.cfr";
connectAttr "FKIKBlendLegsetRange_R.ox" "FKIKBlendLegCondition_R.cfg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegsetRange_R.vx";
connectAttr "IKLeg_R.toe" "IKLiftToeLegUnitConversion_R.i";
connectAttr "IKLeg_R.rollAngle" "Leg_RAngleReverse.i1x";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vx";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vy";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vz";
connectAttr "Leg_RAngleReverse.ox" "IKRollAngleLeg_R.nx";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.my";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.mz";
connectAttr "IKRollAngleLeg_R.ox" "unitConversion2.i";
connectAttr "IKRollAngleLeg_R.oy" "unitConversion3.i";
connectAttr "IKRollAngleLeg_R.oz" "unitConversion4.i";
connectAttr "unitConversion5.o" "IKBallToFKBallMiddleToe1blendTwoAttr_R.i[1]";
connectAttr "FKIKBlendLegUnitConversion_R.o" "IKBallToFKBallMiddleToe1blendTwoAttr_R.ab"
		;
connectAttr "IKXMiddleToe1_R.rx" "unitConversion5.i";
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_R.o" "unitConversion6.i";
connectAttr "FKAnkle_R.s" "ScaleBlendAnkle_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendAnkle_R.b";
connectAttr "FKKnee_R.s" "ScaleBlendKnee_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendKnee_R.b";
connectAttr "FKHip_R.s" "ScaleBlendHip_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendHip_R.b";
connectAttr "FKHead_M.Global" "GlobalHead_unitConversion_M.i";
connectAttr "GlobalHead_unitConversion_M.o" "GlobalHead_reverse_M.ix";
connectAttr "IKLeg_L.swivel" "unitConversion7.i";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vx";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vy";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegUnitConversion_L.i";
connectAttr "FKIKBlendLegUnitConversion_L.o" "FKIKBlendLegReverse_L.ix";
connectAttr "FKIKLeg_L.autoVis" "FKIKBlendLegCondition_L.ft";
connectAttr "FKIKLeg_L.IKVis" "FKIKBlendLegCondition_L.ctr";
connectAttr "FKIKLeg_L.FKVis" "FKIKBlendLegCondition_L.ctg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegCondition_L.cfr";
connectAttr "FKIKBlendLegsetRange_L.ox" "FKIKBlendLegCondition_L.cfg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegsetRange_L.vx";
connectAttr "IKLeg_L.toe" "IKLiftToeLegUnitConversion_L.i";
connectAttr "IKLeg_L.rollAngle" "Leg_LAngleReverse.i1x";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vx";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vy";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vz";
connectAttr "Leg_LAngleReverse.ox" "IKRollAngleLeg_L.nx";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.my";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.mz";
connectAttr "IKRollAngleLeg_L.ox" "unitConversion8.i";
connectAttr "IKRollAngleLeg_L.oy" "unitConversion9.i";
connectAttr "IKRollAngleLeg_L.oz" "unitConversion10.i";
connectAttr "unitConversion11.o" "IKBallToFKBallMiddleToe1blendTwoAttr_L.i[1]";
connectAttr "FKIKBlendLegUnitConversion_L.o" "IKBallToFKBallMiddleToe1blendTwoAttr_L.ab"
		;
connectAttr "IKXMiddleToe1_L.rx" "unitConversion11.i";
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_L.o" "unitConversion12.i";
connectAttr "FKAnkle_L.s" "ScaleBlendAnkle_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendAnkle_L.b";
connectAttr "FKKnee_L.s" "ScaleBlendKnee_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendKnee_L.b";
connectAttr "FKHip_L.s" "ScaleBlendHip_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendHip_L.b";
connectAttr "Center_M.CenterBtwFeet" "CenterBtwFeet_M.vx";
connectAttr "Center_M.CenterBtwFeet" "PelvisCenterBtwFeetUnitConversion_M.i";
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwFeetReverse_M.ix"
		;
connectAttr "irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "BPRig_irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ikSCsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
// End of Colossus2__rig.ma
