//Maya ASCII 2013 scene
//Name: Colossus__rig.ma
//Last modified: Mon, Apr 21, 2014 11:07:13 AM
//Codeset: UTF-8
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.29954416370040526 7.9177817755548876 27.566804960368415 ;
	setAttr ".r" -type "double3" -2.1383527317759734 2160.1999999998852 7.0662167806012337e-17 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 27.918298101152821;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 4.1784972405491922 7.7612609843192404 1.6715583172870974 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.5698517566574912 9.2330006461955882 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 17.434325782968777;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 6.4794975166716737 -1.0195760413273467 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 11.1997179230661;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "ctrl_rig";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Main" -p "ctrl_rig";
	addAttr -ci true -sn "height" -ln "height" -at "double";
	addAttr -ci true -sn "fkVis" -ln "fkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "ikVis" -ln "ikVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fkIkVis" -ln "fkIkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimVis" -ln "aimVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "eyeVis" -ln "eyeVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fingerVis" -ln "fingerVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "bendVis" -ln "bendVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "arrowVis" -ln "arrowVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "jointVis" -ln "jointVis" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -l on ".height" 11.396292029313988;
	setAttr -cb on ".fkVis";
	setAttr -cb on ".ikVis";
	setAttr -cb on ".fkIkVis";
	setAttr -cb on ".aimVis";
	setAttr -cb on ".eyeVis";
	setAttr -cb on ".fingerVis";
	setAttr -cb on ".bendVis";
	setAttr -cb on ".arrowVis";
	setAttr -cb on ".jointVis";
createNode nurbsCurve -n "MainShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		-3.1944594689999992e-16 1.7145024419999999e-16 -2.7999949750000002
		-1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		-2.7999949750000002 4.9681991409999996e-32 -8.113684933e-16
		-1.9798954339999999 -1.2123363030000002e-16 1.9798954339999999
		-8.4369313090000001e-16 -1.7145024419999999e-16 2.7999949750000002
		1.9798954339999999 -1.2123363030000002e-16 1.9798954339999999
		2.7999949750000002 -9.2086260590000001e-32 1.503882763e-15
		1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		-3.1944594689999992e-16 1.7145024419999999e-16 -2.7999949750000002
		-1.9798954339999999 1.2123363030000002e-16 -1.9798954339999999
		;
createNode transform -n "BaseSkeleton" -p "Main";
	setAttr -l on ".v" no;
createNode transform -n "BaseSkeletonOffset" -p "BaseSkeleton";
createNode joint -n "Pelvis" -p "BaseSkeletonOffset";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0 7.2655224800109854 0.42850792407989502 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 1;
createNode joint -n "HipTwist" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.9559509754180908 1.0318973064422607 -1.2030465602874756 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 205.1395790329064 89.999999999999758 ;
createNode joint -n "Hip" -p "HipTwist";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.0010066368702428489 2.2593353864342078 0.0065937479968450497 ;
	setAttr ".r" -type "double3" -1.1752566656535359 -35.295006097216394 -83.747378136107159 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 2;
createNode joint -n "Knee" -p "Hip";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.023204386125963872 5.0337125212412897 0.45114729649270346 ;
	setAttr ".r" -type "double3" -63.22298348183373 11.179388613527411 -13.010752049480663 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "Ankle" -p "Knee";
	setAttr ".t" -type "double3" -0.020687185949858088 4.9257099408590461 0.36690001995298799 ;
	setAttr ".r" -type "double3" 53.614568254588967 0.067470303103079679 -0.13352626925837993 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".pa" -type "double3" 3.1147589914174403 -1.2104724556304991 -11.405913270501992 ;
	setAttr ".dl" yes;
	setAttr ".typ" 4;
createNode joint -n "MiddleToe1" -p "Ankle";
	setAttr ".t" -type "double3" -0.041921108855506262 0.090809517989211913 -1.5007351236359552 ;
	setAttr ".r" -type "double3" 106.0380694124019 0.24854723021448008 -179.1355328454631 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.37268767106233647 0.85129237623615406 1.2445404195761689 ;
	setAttr ".pa" -type "double3" -0.00019030234564052423 0.00053514845282692043 25.864574245063647 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Ball";
createNode joint -n "MiddleToe2_End" -p "MiddleToe1";
	setAttr ".t" -type "double3" 7.1054273576010019e-15 0.91899691277650097 3.3306690738754696e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.8347794621262352 -5.3930280317565768e-13 -6.5161614036483816e-13 ;
	setAttr ".dl" yes;
	setAttr ".typ" 5;
createNode joint -n "Heel_End" -p "Ankle";
	setAttr ".t" -type "double3" -0.12698930469579128 0.2484227124967846 0.79340478542146653 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 59.467892085048035 179.95177563522736 1.2503912846228438 ;
	setAttr ".dl" yes;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "Heel";
createNode joint -n "ElbowShield" -p "Hip";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0.061883867177008334 4.9423722817692894 1.105689073656829 ;
	setAttr ".r" -type "double3" 11.735762420490044 -1.6901391111902413 -5.9460919655529132 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "ElbowShield_End" -p "ElbowShield";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -6.0572645117029915e-17 1.5871705715078104 0.10623558443189808 ;
	setAttr ".r" -type "double3" -63.22298348183373 11.179388613527411 -13.010752049480663 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".jo" -type "double3" 63.661942569916398 -5.0110212704251458 3.0045374368972722 ;
createNode joint -n "Head" -p "Pelvis";
	addAttr -ci true -k true -sn "global" -ln "global" -dv 10 -min 0 -max 10 -at "long";
	addAttr -ci true -sn "globalConnect" -ln "globalConnect" -dv 10 -min 0 -max 10 -at "long";
	setAttr ".t" -type "double3" 0 2.2194187641143799 -0.38132631778717041 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 15;
	setAttr ".otp" -type "string" "36";
	setAttr -k on ".global";
createNode joint -n "Head_End" -p "Head";
	setAttr ".t" -type "double3" 3.1589715250000827e-16 1.8033760160193548 1.7486012637846216e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "23";
createNode joint -n "Jaw" -p "Head";
	setAttr ".t" -type "double3" 0 -0.19902873039245603 0.4243211448192597 ;
	setAttr ".r" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "23";
createNode joint -n "Jaw_End" -p "Jaw";
	setAttr ".t" -type "double3" 0 3.6496593076700785 1.7763568394002505e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "23";
createNode transform -n "Head_globalLocator" -p "Head";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "Head_globalLocatorShape" -p "Head_globalLocator";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode joint -n "Shoulder" -p "Pelvis";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -1.5034208297729492 -0.49391078948974609 0.35724256187677383 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -152.0053170522194 4.9505588210855915 -11.904809842065388 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
	setAttr ".otp" -type "string" "Shoulder1";
createNode joint -n "Elbow" -p "Shoulder";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.12905708064100052 3.1327770267862576 -0.12174410559921832 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -129.97952417590875 1.4902849999290784 0.83989225685842428 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "Fingers" -p "Elbow";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -0.066073639752822544 3.2053430917063097 0.23905965585214473 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 36.446714387517382 -2.231045088328806 -1.1437535407680814 ;
	setAttr ".otp" -type "string" "Hand1";
	setAttr ".radi" 0.97203911833656331;
createNode joint -n "Fingers_End" -p "Fingers";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" -4.8849813083506888e-15 1.3895324463926046 9.7699626167013776e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dla" yes;
	setAttr ".otp" -type "string" "Hand1";
	setAttr ".radi" 0.97203911833656331;
createNode transform -n "MotionSystem" -p "Main";
createNode transform -n "FKSystem" -p "MotionSystem";
createNode transform -n "FKParentConstraintToAnkle_R" -p "FKSystem";
	setAttr ".ro" 3;
createNode joint -n "FKOffsetMiddleToe1_R" -p "FKParentConstraintToAnkle_R";
	setAttr ".t" -type "double3" -0.041921108855506262 0.090809517989211885 -1.5007351236359554 ;
	setAttr ".r" -type "double3" 106.03807097905201 0.24854722216303193 -179.13553528646449 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -0.37268767123615903 0.85129236659217522 1.2445404013339536 ;
createNode transform -n "FKExtraMiddleToe1_R" -p "FKOffsetMiddleToe1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000004 ;
createNode transform -n "FKMiddleToe1_R" -p "FKExtraMiddleToe1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 1.1102230246251563e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleToe1_RShape" -p "FKMiddleToe1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.83138740909999997 -0.0099768334289999996 -0.83138740909999997
		-1.1757593500000001 -0.0099768334289999996 -9.4956468399999994e-17
		-0.83138740909999997 -0.0099768334289999996 0.83138740909999997
		-3.6109525879999998e-16 -0.0099768334289999996 1.1757593500000001
		0.83138740909999997 -0.0099768334289999996 0.83138740909999997
		1.1757593500000001 -0.0099768334289999996 -9.4956468399999994e-17
		0.83138740909999997 -0.0099768334289999996 -0.83138740909999997
		6.111130259e-16 -0.0099768334289999996 -1.1757593500000001
		-0.83138740909999997 -0.0099768334289999996 -0.83138740909999997
		-1.1757593500000001 -0.0099768334289999996 -9.4956468399999994e-17
		-0.83138740909999997 -0.0099768334289999996 0.83138740909999997
		;
createNode joint -n "FKXMiddleToe1_R" -p "FKMiddleToe1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleToe2_End_R" -p "FKXMiddleToe1_R";
	setAttr ".t" -type "double3" 7.1054273576010019e-15 0.91899691277650197 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -70.536833723889416 1.7841727691760545 -178.70576745105654 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 74.372311447972109 -1.6936286486567611 178.58933684660781 ;
createNode parentConstraint -n "FKParentConstraintToAnkle_R_parentConstraint1" -p
		 "FKParentConstraintToAnkle_R";
	addAttr -ci true -k true -sn "w0" -ln "Ankle_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 180.00000693585528 1.766947186488951 -3.3286765231911759e-07 ;
	setAttr ".rst" -type "double3" -4.7127432456533462 0.23096786378932599 1.6526076357552413 ;
	setAttr ".rsrr" -type "double3" 180 1.7669612584690473 -1.153297927842441e-13 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToHip_R" -p "FKSystem";
	setAttr ".ro" 2;
createNode joint -n "FKOffsetElbowShield_R" -p "FKParentConstraintToHip_R";
	setAttr ".t" -type "double3" 0.061883867177006557 4.9423722817692894 1.1056890736568334 ;
	setAttr ".r" -type "double3" 11.735762513057432 -1.6901390612995293 -5.946092045167239 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraElbowShield_R" -p "FKOffsetElbowShield_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.6645352591003757e-15 -8.8817841970012523e-16 0 ;
	setAttr ".ro" 2;
createNode transform -n "FKElbowShield_R" -p "FKExtraElbowShield_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbowShield_RShape" -p "FKElbowShield_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.9119495898616188 0.0077942343674092911 -1.309209342833854
		-2.726635852049375 0.011324481263571188 0.022721743516062071
		-1.9440837625527985 0.0082208406593943829 1.3413424197216488
		-0.022701722135585311 0.00030138312807385443 1.8742225772872565
		1.9119907284259556 -0.0077947805151806059 1.3092082470304651
		2.726676990613714 -0.011325027411342505 -0.022722839319449073
		1.9441249011171355 -0.0082213868071656995 -1.3413435155250339
		0.022742860699921513 -0.00030192927584516587 -1.874223673090643
		-1.9119495898616188 0.0077942343674092911 -1.309209342833854
		-2.726635852049375 0.011324481263571188 0.022721743516062071
		-1.9440837625527985 0.0082208406593943829 1.3413424197216488
		;
createNode joint -n "FKXElbowShield_R" -p "FKElbowShield_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXElbowShield_End_R" -p "FKXElbowShield_R";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 1.5871705715078106 0.10623558443189653 ;
	setAttr ".r" -type "double3" 202.00051147617185 1.6455413485447246 0.1440132611567766 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 157.99127861345528 1.5256844242989986 -0.7606032626813698 ;
createNode parentConstraint -n "FKParentConstraintToHip_R_parentConstraint1" -p "FKParentConstraintToHip_R";
	addAttr -ci true -k true -sn "w0" -ln "Hip_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -169.8467347066981 1.1939583362991846 6.0421172473827092 ;
	setAttr ".rst" -type "double3" -4.2152863618523009 8.2937073145402067 -0.78008015068733005 ;
	setAttr ".rsrr" -type "double3" -169.84673193873004 1.1939570157980186 6.0421192743625483 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToAnkle_L" -p "FKSystem";
	setAttr ".ro" 3;
createNode joint -n "FKOffsetMiddleToe1_L" -p "FKParentConstraintToAnkle_L";
	setAttr ".t" -type "double3" 0.04192110885550715 -0.090809517989211691 1.5007351236359554 ;
	setAttr ".r" -type "double3" 106.03807426963054 0.24854722216303193 -179.13553528646449 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -0.37268767123615903 0.85129236659217522 1.2445404013339536 ;
createNode transform -n "FKExtraMiddleToe1_L" -p "FKOffsetMiddleToe1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 -1.1102230246251563e-16 ;
	setAttr ".r" -type "double3" 3.1805546814635155e-14 -1.7430930539427004e-14 -1.1765567513070114e-14 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKMiddleToe1_L" -p "FKExtraMiddleToe1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 1.1102230246251563e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleToe1_LShape" -p "FKMiddleToe1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.83138744702643441 0.0099755597300235088 0.83138706288819442
		1.1757592884006995 0.0099751905882059511 -3.8743662589268974e-07
		0.83138724797503327 0.0099751170199851025 -0.83138775531173503
		-2.0234977871069759e-07 0.0099753821206260973 -1.175759596686115
		-0.83138757022480292 0.0099758305977690753 -0.83138755626052396
		-1.1757594115990673 0.0099761997395861873 -1.0593570343164771e-07
		-0.83138737117340078 0.0099762733078074817 0.83138726193940549
		7.915141164716033e-08 0.0099760082071669309 1.1757591033137853
		0.83138744702643441 0.0099755597300235088 0.83138706288819442
		1.1757592884006995 0.0099751905882059511 -3.8743662589268974e-07
		0.83138724797503327 0.0099751170199851025 -0.83138775531173503
		;
createNode joint -n "FKXMiddleToe1_L" -p "FKMiddleToe1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleToe2_End_L" -p "FKXMiddleToe1_L";
	setAttr ".t" -type "double3" -6.2172489379008766e-15 -0.91899691277650097 -6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" 109.46316627611058 1.7841727691760538 -178.70576745105654 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -105.62768855202796 -1.6936286486567609 178.58933684660781 ;
createNode parentConstraint -n "FKParentConstraintToAnkle_L_parentConstraint1" -p
		 "FKParentConstraintToAnkle_L";
	addAttr -ci true -k true -sn "w0" -ln "Ankle_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.1047733123609654e-05 -1.7669731118703464 -2.4144234027786041e-07 ;
	setAttr ".rst" -type "double3" 4.7127432456533462 0.23096786378932732 1.6526076357552413 ;
	setAttr ".rsrr" -type "double3" 1.0279713500935972e-14 -1.76696125846904 1.8032437527283032e-14 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToHip_L" -p "FKSystem";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode joint -n "FKOffsetElbowShield_L" -p "FKParentConstraintToHip_L";
	setAttr ".t" -type "double3" -0.061883867177006557 -4.9423722817692886 -1.105689073656829 ;
	setAttr ".r" -type "double3" 11.735762513057432 -1.6901390612995293 -5.946092045167239 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraElbowShield_L" -p "FKOffsetElbowShield_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 -4.4408920985006262e-16 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -3.1828841892868542e-15 3.6029721000953912e-16 -1.987846675914698e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999989 ;
createNode transform -n "FKElbowShield_L" -p "FKExtraElbowShield_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 1.3322676295501878e-15 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000009 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbowShield_LShape" -p "FKElbowShield_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.911949333673844 -0.0077942237080477206 1.309210151264502
		2.7266356730209713 -0.011324615844222397 -0.022720887505305321
		1.9440836601623444 -0.0082210589064009021 -1.3413416093895707
		0.022701650968188325 -0.0003015744571635004 -1.8742218791346936
		-1.9119908320752312 0.007794710920131287 -1.3092076618450177
		-2.7266771714223603 0.011325103056305521 0.022723376924786987
		-1.9441251585637316 0.0082215461184840244 1.3413440988090499
		-0.022743149369573779 0.00030206166924617861 1.874224368554176
		1.911949333673844 -0.0077942237080477206 1.309210151264502
		2.7266356730209713 -0.011324615844222397 -0.022720887505305321
		1.9440836601623444 -0.0082210589064009021 -1.3413416093895707
		;
createNode joint -n "FKXElbowShield_L" -p "FKElbowShield_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXElbowShield_End_L" -p "FKXElbowShield_L";
	setAttr ".t" -type "double3" -2.6645352591003757e-15 -1.5871705715078115 -0.10623558443189828 ;
	setAttr ".r" -type "double3" 22.000511476171837 -1.6455413485447237 0.1440132611567764 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -22.008721386544728 1.5256844242989995 -0.76060326268136991 ;
createNode parentConstraint -n "FKParentConstraintToHip_L_parentConstraint1" -p "FKParentConstraintToHip_L";
	addAttr -ci true -k true -sn "w0" -ln "Hip_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 10.153270180966803 -1.1939554633556035 6.0421205985684701 ;
	setAttr ".rst" -type "double3" 4.2152863618522982 8.2937073145402085 -0.78008015068733005 ;
	setAttr ".rsrr" -type "double3" 10.153268061269978 -1.1939570157980344 6.0421192743624603 ;
	setAttr -k on ".w0";
createNode transform -n "PelvisCenterBtwLegsBlended_M" -p "FKSystem";
	setAttr ".ro" 3;
createNode transform -n "CenterOffset_M" -p "PelvisCenterBtwLegsBlended_M";
createNode transform -n "CenterExtra_M" -p "CenterOffset_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Center_M" -p "CenterExtra_M";
	addAttr -ci true -k true -sn "CenterBtwFeet" -ln "CenterBtwFeet" -min 0 -max 10 
		-at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 4;
	setAttr -k on ".CenterBtwFeet";
createNode nurbsCurve -n "Center_MShape" -p "Center_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 46 0 no 3
		47 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46
		47
		-4.7936814906325615 -0.10836748345776516 3.5399155420978771e-05
		-5.0988512511326807 -0.10836748345776516 0.93925342340087214
		-5.8977981630040848 -0.10836748345776516 1.5197243091011421
		-6.8853524763307226 -0.10836748345776516 1.5197243091011421
		-7.6842993899661325 -0.10836748345776516 0.93925342340087214
		-7.9894691469382302 -0.10836748345776516 3.5399155420978771e-05
		-7.6842993899661325 -0.10836748345776516 -0.9391826251113744
		-6.8853524763307226 -0.10836748345776516 -1.5196535108116451
		-5.8977981630040848 -0.10836748345776516 -1.5196535108116451
		-5.0988512511326807 -0.10836748345776516 -0.9391826251113744
		-4.7936814906325615 -0.10836748345776516 3.5399155420978771e-05
		-4.1267516330388979e-09 -0.10836748345776516 3.5399155420978771e-05
		4.7936814818124809 -0.10836748345776516 3.5399155420978771e-05
		5.0988512423126027 -0.10836748345776516 0.93925342340087214
		5.8977981541840059 -0.10836748345776516 1.5197243091011421
		6.885352467510641 -0.10836748345776516 1.5197243091011421
		7.684299382910071 -0.10836748345776516 0.93925342340087214
		7.989469139882166 -0.10836748345776516 3.5399155420978771e-05
		7.684299382910071 -0.10836748345776516 -0.9391826251113744
		6.885352467510641 -0.10836748345776516 -1.5196535108116451
		5.8977981541840059 -0.10836748345776516 -1.5196535108116451
		5.0988512423126027 -0.10836748345776516 -0.9391826251113744
		4.7936814818124809 -0.10836748345776516 3.5399155420978771e-05
		-4.1267516330388979e-09 -0.10836748345776516 3.5399155420978771e-05
		-4.1267516330388979e-09 -0.10836748345776516 -4.7936460869013686
		-0.93921802831336043 -0.10836748345776516 -5.0988158474014931
		-1.5196889141900325 -0.10836748345776516 -5.8977627592728954
		-1.5196889141900325 -0.10836748345776516 -6.8853170725995314
		-0.93921802831336043 -0.10836748345776516 -7.6842639879989569
		-4.1267516330388979e-09 -0.10836748345776516 -7.9894337432070399
		0.93921802019888623 -0.10836748345776516 -7.6842639879989569
		1.5196889058991572 -0.10836748345776516 -6.8853170725995314
		1.5196889058991572 -0.10836748345776516 -5.8977627592728954
		0.93921802019888623 -0.10836748345776516 -5.0988158474014931
		-4.1267516330388979e-09 -0.10836748345776516 -4.7936460869013686
		-4.1267516330388979e-09 -0.10836748345776516 3.5399155420978771e-05
		-4.1267516330388979e-09 -0.10836748345776516 4.7937168855436703
		-0.93921802831336043 -0.10836748345776516 5.0988866460437929
		-1.5196889141900325 -0.10836748345776516 5.8978335579151944
		-1.5196889141900325 -0.10836748345776516 6.8853878712418286
		-0.93921802831336043 -0.10836748345776516 7.684334784877243
		-4.1267516330388979e-09 -0.10836748345776516 7.9895045418493442
		0.93921802019888623 -0.10836748345776516 7.684334784877243
		1.5196889058991572 -0.10836748345776516 6.8853878712418286
		1.5196889058991572 -0.10836748345776516 5.8978335579151944
		0.93921802019888623 -0.10836748345776516 5.0988866460437929
		-4.1267516330388979e-09 -0.10836748345776516 4.7937168855436703
		;
createNode joint -n "FKOffsetPelvis_M" -p "Center_M";
	setAttr ".ro" 3;
createNode transform -n "FKExtraPelvis_M" -p "FKOffsetPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode transform -n "FKPelvis_M" -p "FKExtraPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode nurbsCurve -n "FKPelvis_MShape" -p "FKPelvis_M";
	setAttr -l on -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.63722489380477232 1.805111211200271e-16 -0.63722489380477365
		-0.90117208710046492 2.0010040004968866e-16 0
		-0.63722489380477276 1.0247357846652442e-16 0.63722489380477298
		-2.6113712529619539e-16 -5.5180875597426144e-17 0.90117208710046492
		0.63722489380477254 -1.805111211200271e-16 0.63722489380477298
		0.90117208710046492 -2.0010040004968869e-16 0
		0.63722489380477276 -1.0247357846652447e-16 -0.63722489380477221
		4.8402128592911001e-16 5.5180875597426101e-17 -0.90117208710046492
		-0.63722489380477232 1.805111211200271e-16 -0.63722489380477365
		-0.90117208710046492 2.0010040004968866e-16 0
		-0.63722489380477276 1.0247357846652442e-16 0.63722489380477298
		;
createNode joint -n "FKXPelvis_M" -p "FKPelvis_M";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHipTwist_R" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" -1.9559509754180908 1.0318973064422607 -1.2030465602874756 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 205.13958175869882 90.000002504478161 ;
createNode transform -n "FKExtraHipTwist_R" -p "FKOffsetHipTwist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 4.4408920985006262e-16 ;
	setAttr ".ro" 2;
createNode transform -n "FKHipTwist_R" -p "FKExtraHipTwist_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHipTwist_RShape" -p "FKHipTwist_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.4288694833815545 0.27525397671596413 -1.0886851087969818
		-1.8532278801734601 0.27525397671596413 -0.064193312443124095
		-1.4288694833815545 0.27525397671596408 0.96029848391073358
		-0.40437768702769666 0.27525397671596369 1.3846568807026391
		0.62011410932616162 0.27525397671596358 0.96029848391073358
		1.0444725061180673 0.27525397671596358 -0.064193312443124095
		0.62011410932616162 0.27525397671596363 -1.0886851087969818
		-0.4043776870276955 0.27525397671596391 -1.5130435055888876
		-1.4288694833815545 0.27525397671596413 -1.0886851087969818
		-1.8532278801734601 0.27525397671596413 -0.064193312443124095
		-1.4288694833815545 0.27525397671596408 0.96029848391073358
		;
createNode joint -n "FKXHipTwist_R" -p "FKHipTwist_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHip_R" -p "FKXHipTwist_R";
	setAttr ".t" -type "double3" 0.0010066368702439377 2.2593353864342083 0.0065937479968400936 ;
	setAttr ".r" -type "double3" -1.1752566699406366 -35.295005951135472 -83.747380960870871 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraHip_R" -p "FKOffsetHip_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKHip_R" -p "FKExtraHip_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHip_RShape" -p "FKHip_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.2842300226066536 -0.1623025207015219 -1.2130084749545167
		-1.8139096061356432 -0.1623025207015219 0.065751159008488047
		-1.2842300226066536 -0.16230252070152215 1.344510792971493
		-0.0054703886436489376 -0.1623025207015224 1.8741903765004819
		1.2732892453193565 -0.16230252070152271 1.344510792971493
		1.8029688288483456 -0.16230252070152276 0.065751159008488047
		1.2732892453193565 -0.16230252070152248 -1.2130084749545167
		-0.0054703886436474423 -0.16230252070152221 -1.7426880584835067
		-1.2842300226066536 -0.1623025207015219 -1.2130084749545167
		-1.8139096061356432 -0.1623025207015219 0.065751159008488047
		-1.2842300226066536 -0.16230252070152215 1.344510792971493
		;
createNode joint -n "FKXHip_R" -p "FKHip_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetKnee_R" -p "FKXHip_R";
	setAttr ".t" -type "double3" -0.023204386125966536 5.0337125212412897 0.4511472964927053 ;
	setAttr ".r" -type "double3" -63.222983523873538 11.179388963646533 -13.010752217876419 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraKnee_R" -p "FKOffsetKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 2.2204460492503131e-16 ;
	setAttr ".ro" 2;
createNode transform -n "FKKnee_R" -p "FKExtraKnee_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKKnee_RShape" -p "FKKnee_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.5212409434870688 -0.062070414992941005 -1.5349183504008606
		-2.151581382043255 -0.062070414992940977 -0.013141914620497432
		-1.5212409434870688 -0.062070414992941213 1.5086345211598653
		0.00053549229329410866 -0.062070414992941574 2.1389749597160508
		1.522311928073657 -0.062070414992941851 1.5086345211598653
		2.1526523666298423 -0.062070414992941934 -0.013141914620497432
		1.522311928073657 -0.062070414992941685 -1.5349183504008606
		0.00053549229329588849 -0.062070414992941331 -2.1652587889570465
		-1.5212409434870688 -0.062070414992941005 -1.5349183504008606
		-2.151581382043255 -0.062070414992940977 -0.013141914620497432
		-1.5212409434870688 -0.062070414992941213 1.5086345211598653
		;
createNode joint -n "FKXKnee_R" -p "FKKnee_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetAnkle_R" -p "FKXKnee_R";
	setAttr ".t" -type "double3" -0.020687185949857856 4.9257099408590488 0.36690001995298727 ;
	setAttr ".r" -type "double3" 53.614567097633909 0.067470302828465237 -0.1335262629108486 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraAnkle_R" -p "FKOffsetAnkle_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -2.2204460492503131e-16 ;
	setAttr ".ro" 3;
createNode transform -n "FKAnkle_R" -p "FKExtraAnkle_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKAnkle_RShape" -p "FKAnkle_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.3738108663297854 -0.025228781583849193 -1.4407184382338001
		-1.9482038674721756 -0.025228781583849159 -0.054011064175494269
		-1.3738108663297854 -0.025228781583849377 1.3326963098828106
		0.012896507728518887 -0.025228781583849721 1.9070893110252001
		1.3996038817868246 -0.025228781583849988 1.3326963098828106
		1.9739968829292152 -0.025228781583850036 -0.054011064175494269
		1.3996038817868246 -0.025228781583849814 -1.4407184382338001
		0.012896507728520508 -0.025228781583849481 -2.0151114393761893
		-1.3738108663297854 -0.025228781583849193 -1.4407184382338001
		-1.9482038674721756 -0.025228781583849159 -0.054011064175494269
		-1.3738108663297854 -0.025228781583849377 1.3326963098828106
		;
createNode joint -n "FKXAnkle_R" -p "FKAnkle_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToAnkle_R" -p "FKXAnkle_R";
	setAttr ".r" -type "double3" -180 1.7669612584689427 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode joint -n "FKXHeel_End_R" -p "FKXAnkle_R";
	setAttr ".t" -type "double3" -0.12698930469579128 0.24842271249678471 0.79340478542146708 ;
	setAttr ".r" -type "double3" 59.506463443299211 -1.8147647267137106 178.74898158894109 ;
	setAttr ".jo" -type "double3" -180 1.7669612584689425 2.1645897125257875e-16 ;
createNode joint -n "FKOffsetHead_M" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 0 2.2194187641143799 -0.38132631778717041 ;
	setAttr ".ro" 5;
createNode transform -n "FKGlobalStaticHead_M" -p "FKOffsetHead_M";
	setAttr ".ro" 5;
createNode transform -n "FKGlobalHead_M" -p "FKGlobalStaticHead_M";
	setAttr ".ro" 5;
createNode transform -n "FKExtraHead_M" -p "FKGlobalHead_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 5;
createNode transform -n "FKHead_M" -p "FKExtraHead_M";
	addAttr -ci true -k true -sn "Global" -ln "Global" -dv 10 -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".Global";
createNode nurbsCurve -n "FKHead_MShape" -p "FKHead_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.9987712544657907 3.2346477231121735 -4.2327875993393098
		-2.8266894179281441 3.2346477231121735 -0.53116021168978322
		-1.9987712544657907 3.2346477231121735 3.170467175230979
		-8.1910387478182901e-16 3.2346477231121735 4.7037314438024502
		1.9987712544657907 3.2346477231121735 3.170467175230979
		2.8266894179281441 3.2346477231121735 -0.53116021168978322
		1.9987712544657907 3.2346477231121735 -4.2327875993393098
		1.5182203996901784e-15 3.2346477231121735 -5.7660518679107806
		-1.9987712544657907 3.2346477231121735 -4.2327875993393098
		-2.8266894179281441 3.2346477231121735 -0.53116021168978322
		-1.9987712544657907 3.2346477231121735 3.170467175230979
		;
createNode joint -n "FKXHead_M" -p "FKHead_M";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXHead_End_M" -p "FKXHead_M";
	setAttr ".t" -type "double3" 3.1589715250000827e-16 1.8033760160193548 1.7486012637846216e-15 ;
createNode joint -n "FKOffsetJaw_M" -p "FKXHead_M";
	setAttr ".t" -type "double3" 0 -0.19902873039245603 0.4243211448192597 ;
	setAttr ".r" -type "double3" 90.000002504478161 0 0 ;
createNode transform -n "FKExtraJaw_M" -p "FKOffsetJaw_M";
	setAttr -l on -k off ".v";
createNode transform -n "FKJaw_M" -p "FKExtraJaw_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKJaw_MShape" -p "FKJaw_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.7360137251441845 -0.015106699061387492 -0.8870269496055192
		-3.8693077169378394 -0.015106699061387462 6.7428673028977943e-14
		-2.7360137251441867 -0.015106699061387599 0.88702694960565331
		-1.1212285739326155e-15 -0.015106699061387825 1.2544455423227223
		2.7360137251441854 -0.015106699061387986 0.88702694960565331
		3.8693077169378394 -0.015106699061388016 6.7428673028977943e-14
		2.7360137251441867 -0.015106699061387886 -0.88702694960551765
		2.078212722759238e-15 -0.01510669906138766 -1.2544455423225878
		-2.7360137251441845 -0.015106699061387492 -0.8870269496055192
		-3.8693077169378394 -0.015106699061387462 6.7428673028977943e-14
		-2.7360137251441867 -0.015106699061387599 0.88702694960565331
		;
createNode joint -n "FKXJaw_M" -p "FKJaw_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXJaw_End_M" -p "FKXJaw_M";
	setAttr ".t" -type "double3" 0 3.6496593076700785 3.5527136788005009e-15 ;
	setAttr ".jo" -type "double3" -89.999999999999986 0 0 ;
createNode orientConstraint -n "FKGlobalHead_M_orientConstraint1" -p "FKGlobalHead_M";
	addAttr -ci true -k true -sn "w0" -ln "GlobalHead_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "FKGlobalStaticHead_MW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "FKOffsetShoulder_R" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" -1.5034208297729492 -0.49391078948974609 0.35724256187677383 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -152.00531356350018 4.9505586764656773 -11.904809695015247 ;
createNode transform -n "FKExtraShoulder_R" -p "FKOffsetShoulder_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKShoulder_R" -p "FKExtraShoulder_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulder_RShape" -p "FKShoulder_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.035304235650119 -0.020948132228509705 -1.0090859741611218
		-1.464793850321175 -0.020948132228509664 0.027793678476137375
		-1.0353042356501201 -0.020948132228509823 1.0646733311133951
		0.001575416987137319 -0.02094813222851008 1.4941629457844503
		1.0384550696243948 -0.020948132228510285 1.0646733311133951
		1.4679446842954509 -0.020948132228510323 0.027793678476137375
		1.038455069624395 -0.020948132228510153 -1.0090859741611191
		0.0015754169871385307 -0.020948132228509907 -1.4385755888321761
		-1.035304235650119 -0.020948132228509705 -1.0090859741611218
		-1.464793850321175 -0.020948132228509664 0.027793678476137375
		-1.0353042356501201 -0.020948132228509823 1.0646733311133951
		;
createNode joint -n "FKXShoulder_R" -p "FKShoulder_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow_R" -p "FKXShoulder_R";
	setAttr ".t" -type "double3" -0.12905708064099963 3.1327770267862576 -0.12174410559921656 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -129.97952443371815 1.4902849514206058 0.839892247258895 ;
createNode transform -n "FKExtraElbow_R" -p "FKOffsetElbow_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.7755575615628914e-17 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "FKElbow_R" -p "FKExtraElbow_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.3877787807814457e-17 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow_RShape" -p "FKElbow_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.89801102611234029 8.4782677454259175e-14 -0.89801102611232386
		-1.2699793722886523 8.4810283702294124e-14 1.828096209242016e-14
		-0.89801102611234085 8.4672702821386435e-14 0.8980110261123595
		-3.6800824971395406e-16 8.4450527825648527e-14 1.2699793722886703
		0.89801102611234063 8.4273905814363542e-14 0.8980110261123595
		1.2699793722886523 8.4246299566328581e-14 1.828096209242016e-14
		0.89801102611234085 8.4383880447236295e-14 -0.89801102611232175
		6.8210839824878139e-16 8.4606055442974191e-14 -1.2699793722886339
		-0.89801102611234029 8.4782677454259175e-14 -0.89801102611232386
		-1.2699793722886523 8.4810283702294124e-14 1.828096209242016e-14
		-0.89801102611234085 8.4672702821386435e-14 0.8980110261123595
		;
createNode joint -n "FKXElbow_R" -p "FKElbow_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetFingers_R" -p "FKXElbow_R";
	setAttr ".t" -type "double3" -0.0660736397528221 3.2053430917063084 0.23905965585214517 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 36.446715864077611 -2.231045007004699 -1.1437534896110106 ;
createNode transform -n "FKExtraFingers_R" -p "FKOffsetFingers_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -4.4408920985006262e-16 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKFingers_R" -p "FKExtraFingers_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -3.3306690738754696e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKFingers_RShape" -p "FKFingers_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.81385025218767137 -0.062956147448667774 -0.8138502521876757
		-1.1509580643845705 -0.06295614744866776 -5.9497747389052218e-16
		-0.81385025218767215 -0.062956147448667871 0.81385025218767348
		1.1869648259792469e-15 -0.062956147448668079 1.1509580643845712
		0.81385025218767493 -0.062956147448668232 0.81385025218767348
		1.1509580643845734 -0.062956147448668259 -5.9497747389052218e-16
		0.81385025218767504 -0.062956147448668121 -0.81385025218767393
		2.1386654999351498e-15 -0.062956147448667926 -1.1509580643845725
		-0.81385025218767137 -0.062956147448667774 -0.8138502521876757
		-1.1509580643845705 -0.06295614744866776 -5.9497747389052218e-16
		-0.81385025218767215 -0.062956147448667871 0.81385025218767348
		;
createNode joint -n "FKXFingers_R" -p "FKFingers_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXFingers_End_R" -p "FKXFingers_R";
	setAttr ".t" -type "double3" -5.3290705182007514e-15 1.3895324463926055 1.1546319456101628e-14 ;
	setAttr ".r" -type "double3" 115.11746200516393 -16.272889478817827 2.3654194306647987 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -115.1174620051639 16.272889478817827 -2.3654194306647955 ;
createNode joint -n "FKOffsetHipTwist_L" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 1.9559509754180917 1.031897306442259 -1.2030465602874734 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.0509930133114849e-06 25.139577240952882 90.000002504478161 ;
createNode transform -n "FKExtraHipTwist_L" -p "FKOffsetHipTwist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 2.2204460492503131e-16 0 ;
	setAttr ".r" -type "double3" 1.0495830448829606e-13 1.5294185322743802e-29 1.6697912077683464e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKHipTwist_L" -p "FKExtraHipTwist_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 0.99999999999999933 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHipTwist_LShape" -p "FKHipTwist_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.4288695474397963 -0.27525403425759887 1.0886850101739372
		1.853227863450992 -0.27525412010863182 0.064193180359621405
		1.4288693858783832 -0.27525413879076588 -0.96029858253376865
		0.40437755606406878 -0.27525407936025914 -1.3846568985449701
		-0.62011420682931906 -0.27525397663069828 -0.96029842097236462
		-1.0444725228405147 -0.27525389077966578 0.064193408841950905
		-0.62011404526790681 -0.27525387209753172 1.088685171735341
		0.4043777845464076 -0.27525393152803712 1.5130434877465428
		1.4288695474397963 -0.27525403425759887 1.0886850101739372
		1.853227863450992 -0.27525412010863182 0.064193180359621405
		1.4288693858783832 -0.27525413879076588 -0.96029858253376865
		;
createNode joint -n "FKXHipTwist_L" -p "FKHipTwist_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetHip_L" -p "FKXHipTwist_L";
	setAttr ".t" -type "double3" -0.0010066368702430495 -2.2593353864342078 -0.00659374799684187 ;
	setAttr ".r" -type "double3" -1.1752566699406366 -35.295005951135472 -83.747380960870871 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraHip_L" -p "FKOffsetHip_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.7763568394002505e-15 0 ;
	setAttr ".r" -type "double3" -4.8081041473686766e-15 -1.987846675914698e-16 -6.9574633657014439e-16 ;
	setAttr ".ro" 2;
createNode transform -n "FKHip_L" -p "FKExtraHip_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 1.7763568394002505e-15 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKHip_LShape" -p "FKHip_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.284229928811123 0.16230250543956792 1.2130087586882237
		1.813909612316736 0.16230237440698758 -0.065750833863095881
		1.2842301287643751 0.16230227885539694 -1.344510509237768
		0.0054705362130498258 0.16230227475762149 -1.8741901927433835
		-1.2732891391616254 0.16230236451408153 -1.3445107091910282
		-1.8029688226672391 0.16230249554665921 -0.065751116639707785
		-1.2732893391148781 0.16230259109825074 1.2130085587349635
		0.0054702534364459154 0.16230259519602708 1.7426882422405805
		1.284229928811123 0.16230250543956792 1.2130087586882237
		1.813909612316736 0.16230237440698758 -0.065750833863095881
		1.2842301287643751 0.16230227885539694 -1.344510509237768
		;
createNode joint -n "FKXHip_L" -p "FKHip_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetKnee_L" -p "FKXHip_L";
	setAttr ".t" -type "double3" 0.023204386125966536 -5.0337125212412932 -0.45114729649270302 ;
	setAttr ".r" -type "double3" -63.222983523873538 11.179388963646533 -13.010752217876419 ;
	setAttr ".ro" 2;
createNode transform -n "FKExtraKnee_L" -p "FKOffsetKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 0 1.987846675914698e-16 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "FKKnee_L" -p "FKExtraKnee_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 4.4408920985006262e-16 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKKnee_LShape" -p "FKKnee_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.5212408161121314 0.062069758991747825 1.5349185246583177
		2.1515812682146285 0.062069578309410733 0.013142094489030807
		1.5212408432047573 0.06206951403855987 -1.5086343469023984
		-0.00053558696453226418 0.062069603828188491 -2.1389747990049064
		-1.5223120283559544 0.062069795080749834 -1.508634373995047
		-2.1526524804584497 0.062069975763087815 0.013142056174240135
		-1.5223120554485805 0.062070040033938234 1.5349184975656691
		-0.00053562527929340575 0.062069950244309613 2.1652589496681776
		1.5212408161121314 0.062069758991747825 1.5349185246583177
		2.1515812682146285 0.062069578309410733 0.013142094489030807
		1.5212408432047573 0.06206951403855987 -1.5086343469023984
		;
createNode joint -n "FKXKnee_L" -p "FKKnee_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetAnkle_L" -p "FKXKnee_L";
	setAttr ".t" -type "double3" 0.02068718594985608 -4.925709940859047 -0.36690001995298638 ;
	setAttr ".r" -type "double3" 53.614567097633909 0.067470302828465237 -0.1335262629108486 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraAnkle_L" -p "FKOffsetAnkle_L";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 0 7.9513867036587899e-16 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKAnkle_L" -p "FKExtraAnkle_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKAnkle_LShape" -p "FKAnkle_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.373810182434994 0.025228885688511588 1.440719419883755
		1.9482032942447951 0.025228746969312627 0.054012091665404549
		1.3738104037698229 0.02522866283661826 -1.3326953282328369
		-0.012896924448531522 0.025228682574219924 -1.9070884400426411
		-1.3996043443467745 0.02522879462009817 -1.3326955495676751
		-1.9739974561565767 0.025228933339297127 0.054011778650674643
		-1.399604565681603 0.025229017471991497 1.4407191985489167
		-0.012897237463249668 0.02522899773438983 2.0151123103587207
		1.373810182434994 0.025228885688511588 1.440719419883755
		1.9482032942447951 0.025228746969312627 0.054012091665404549
		1.3738104037698229 0.02522866283661826 -1.3326953282328369
		;
createNode joint -n "FKXAnkle_L" -p "FKAnkle_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToAnkle_L" -p "FKXAnkle_L";
	setAttr ".r" -type "double3" 0 1.7669612584689447 0 ;
	setAttr ".ro" 3;
createNode joint -n "FKXHeel_End_L" -p "FKXAnkle_L";
	setAttr ".t" -type "double3" 0.12698930469579217 -0.24842271249678485 -0.79340478542146708 ;
	setAttr ".r" -type "double3" 59.506463443299197 178.18523527328628 1.2510184110588851 ;
	setAttr ".jo" -type "double3" 0 1.7669612584689438 0 ;
createNode joint -n "FKOffsetShoulder_L" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 1.5034208297729497 -0.49391078948974609 0.35724256187677383 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 27.99468558272617 -4.9505586764656755 11.904809695015247 ;
createNode transform -n "FKExtraShoulder_L" -p "FKOffsetShoulder_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 6.4853497801717023e-15 7.9513867036587919e-16 1.2921003393445538e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
createNode transform -n "FKShoulder_L" -p "FKExtraShoulder_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKShoulder_LShape" -p "FKShoulder_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.0353042356501181 0.020948117191956328 1.0090859744732728
		1.4647938503211746 0.020948132642668149 -0.027793678163987146
		1.0353042356501201 0.020948148093379079 -1.0646733308012446
		-0.00157541698713759 0.020948154493273918 -1.4941629454722991
		-1.0384550696243957 0.020948148093379079 -1.0646733308012446
		-1.4679446842954509 0.020948132642668149 -0.027793678163986701
		-1.0384550696243957 0.020948117191957216 1.0090859744732703
		-0.0015754169871389223 0.02094811079206238 1.4385755891443277
		1.0353042356501181 0.020948117191956328 1.0090859744732728
		1.4647938503211746 0.020948132642668149 -0.027793678163987146
		1.0353042356501201 0.020948148093379079 -1.0646733308012446
		;
createNode joint -n "FKXShoulder_L" -p "FKShoulder_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow_L" -p "FKXShoulder_L";
	setAttr ".t" -type "double3" 0.12905708064100008 -3.1327770267862585 0.12174410559921788 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -129.97952443371815 1.4902849514206058 0.839892247258895 ;
createNode transform -n "FKExtraElbow_L" -p "FKOffsetElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -1.6653345369377348e-16 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -6.8580710319057077e-15 -7.951386703658789e-16 -4.7708320221952752e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKElbow_L" -p "FKExtraElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 2.7755575615628914e-17 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow_LShape" -p "FKElbow_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.89801102715921921 2.3175716332146656e-08 0.89801105484711075
		1.2699793734761489 3.639629130025579e-08 2.8793014639205694e-08
		0.89801102744045547 4.9926603670757963e-08 -0.89801099737757317
		1.386362136202024e-09 5.5840779542015717e-08 -1.2699793436945024
		-0.89801102478422523 5.067437512662476e-08 -0.89801099765880965
		-1.2699793711011556 3.7453799714426417e-08 2.8395285678328719e-08
		-0.8980110250654626 2.392348762148e-08 0.8980110545658726
		9.886313989682094e-10 1.8009311583688792e-08 1.2699794008828025
		0.89801102715921921 2.3175716332146656e-08 0.89801105484711075
		1.2699793734761489 3.639629130025579e-08 2.8793014639205694e-08
		0.89801102744045547 4.9926603670757963e-08 -0.89801099737757317
		;
createNode joint -n "FKXElbow_L" -p "FKElbow_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetFingers_L" -p "FKXElbow_L";
	setAttr ".t" -type "double3" 0.066073639752822988 -3.2053430917063097 -0.23905965585214384 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 36.446715864077611 -2.231045007004699 -1.1437534896110106 ;
createNode transform -n "FKExtraFingers_L" -p "FKOffsetFingers_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 3.3306690738754696e-16 0 ;
	setAttr ".r" -type "double3" -1.2722218725854064e-14 6.361109362927032e-15 -7.951386703658783e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999967 ;
createNode transform -n "FKFingers_L" -p "FKExtraFingers_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKFingers_LShape" -p "FKFingers_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.81385025019253376 0.062956155826496052 0.81385021412980318
		1.1509580627787797 0.062956167627913229 -3.789660052433419e-08
		0.81385025097122732 0.062956180025811936 -0.813850290245547
		-1.0551728379937233e-09 0.0629561857576727 -1.1509581028317917
		-0.81385025340411932 0.062956181465847583 -0.81385029102424067
		-1.1509580659903644 0.062956169664430406 -3.899783873606566e-08
		-0.8138502541828122 0.062956157266531698 0.81385021335110885
		-2.1564119379036129e-09 0.062956151534671378 1.1509580259373529
		0.81385025019253376 0.062956155826496052 0.81385021412980318
		1.1509580627787797 0.062956167627913229 -3.789660052433419e-08
		0.81385025097122732 0.062956180025811936 -0.813850290245547
		;
createNode joint -n "FKXFingers_L" -p "FKFingers_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXFingers_End_L" -p "FKXFingers_L";
	setAttr ".t" -type "double3" 4.4408920985006262e-15 -1.3895324463926055 -1.1546319456101628e-14 ;
	setAttr ".r" -type "double3" -64.882537994836113 -16.272889478817834 2.3654194306647955 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 64.882537994836127 16.272889478817834 -2.3654194306647964 ;
createNode parentConstraint -n "PelvisCenterBtwLegsBlended_M_parentConstraint1" -p
		 "PelvisCenterBtwLegsBlended_M";
	addAttr -ci true -k true -sn "w0" -ln "PelvisCenter_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PelvisCenterBtwLegsOffset_MW1" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0 7.2655224800109854 0.42850792407989502 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKSystem" -p "MotionSystem";
createNode transform -n "IKParentConstraint" -p "IKSystem";
createNode transform -n "IKParentConstraintHip_R" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "IKOffsetHip_R" -p "IKParentConstraintHip_R";
	setAttr ".t" -type "double3" 0.0010066368702448258 2.2593353864342074 0.0065937479968409818 ;
	setAttr ".r" -type "double3" -1.1752566656536634 -35.295006097216358 -83.747378136107173 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode joint -n "IKXHip_R" -p "IKOffsetHip_R";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -3.5129885838503184e-08 -1.0770510807529869e-06 5.4459684445004505e-07 ;
	setAttr ".ro" 2;
createNode joint -n "IKXKnee_R" -p "IKXHip_R";
	setAttr ".t" -type "double3" -0.023204386125967424 5.0337125212412897 0.45114729649270569 ;
	setAttr ".r" -type "double3" -63.22298608263295 11.179388966274715 -13.010752519199174 ;
	setAttr ".ro" 2;
	setAttr ".pa" -type "double3" -63.222983523873538 11.179388963646533 -13.010752217876419 ;
createNode joint -n "IKXAnkle_R" -p "IKXKnee_R";
	setAttr ".t" -type "double3" -0.020687185949857856 4.925709940859047 0.36690001995298704 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" 53.614567097633909 0.067470302828465237 -0.1335262629108486 ;
createNode joint -n "IKXMiddleToe1_R" -p "IKXAnkle_R";
	setAttr ".t" -type "double3" -0.041921108855506262 0.090809517989211885 -1.5007351236359552 ;
	setAttr ".r" -type "double3" 106.03807097905201 0.24855023855528227 -179.13552136092389 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -0.37268767123615903 0.85129236659217522 1.2445404013339536 ;
	setAttr ".pa" -type "double3" 106.03807214742264 0.24854722216303193 -179.13553528646449 ;
createNode joint -n "IKXMiddleToe2_End_R" -p "IKXMiddleToe1_R";
	setAttr ".t" -type "double3" 6.2172489379008766e-15 0.91899691277650142 0 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.8347794005392162 -5.3930280317565758e-13 -6.5161614036483786e-13 ;
createNode ikEffector -n "effector2" -p "IKXMiddleToe1_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "IKFKAlignedLeg_R" -p "IKXAnkle_R";
	setAttr ".t" -type "double3" 7.1050676364592391e-07 2.543418714529011e-08 -4.1397558137035162e-07 ;
	setAttr ".r" -type "double3" -2.9755621406223014e-06 0 -5.1958786794913239e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000004 ;
createNode orientConstraint -n "IKXAnkle_R_orientConstraint1" -p "IKXAnkle_R";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -126.39599732256823 1.1158137095274556 1.2889016349046145 ;
	setAttr ".o" -type "double3" 0 1.766961052075855 0 ;
	setAttr ".rsrr" -type "double3" 53.614567097584221 0.067470281495827539 -0.13352624719148534 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector3" -p "IKXAnkle_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector1" -p "IKXKnee_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_R" -p "IKXKnee_R";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" -126.37090066489557 1.1155319192321176 1.2891436652395976 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000009 ;
createNode annotationShape -n "PoleAnnotationLeg_RShape" -p "PoleAnnotationLeg_R";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintHip_R_parentConstraint1" -p "IKParentConstraintHip_R";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 154.8604182413012 1.526666247102488e-13 89.999997732757279 ;
	setAttr ".rst" -type "double3" -1.9559509754180908 8.2974197864532471 -0.77453863620758068 ;
	setAttr ".rsrr" -type "double3" 154.8604209670936 0 90.000000000000242 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintHip_L" -p "IKParentConstraint";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "IKOffsetHip_L" -p "IKParentConstraintHip_L";
	setAttr ".t" -type "double3" -0.0010066368702448258 -2.2593353864342065 -0.0065937479968414259 ;
	setAttr ".r" -type "double3" -1.1752566656535517 -35.29500609721638 -83.747378136107216 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999989 0.99999999999999989 ;
createNode joint -n "IKXHip_L" -p "IKOffsetHip_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -8.8817841970012523e-16 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -9.9362285072546375e-08 -3.0463605747438928e-06 1.5403523680082964e-06 ;
	setAttr ".ro" 2;
createNode joint -n "IKXKnee_L" -p "IKXHip_L";
	setAttr ".t" -type "double3" 0.023204386125971865 -5.0337125212412897 -0.4511472964927043 ;
	setAttr ".r" -type "double3" -63.222981052673809 11.1793889720955 -13.010751675081854 ;
	setAttr ".ro" 2;
	setAttr ".pa" -type "double3" -63.222983523873538 11.179388963646533 -13.010752217876419 ;
createNode joint -n "IKXAnkle_L" -p "IKXKnee_L";
	setAttr ".t" -type "double3" 0.020687185949857856 -4.925709940859047 -0.36690001995298566 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" 53.614567097633909 0.067470302828465237 -0.1335262629108486 ;
createNode joint -n "IKXMiddleToe1_L" -p "IKXAnkle_L";
	setAttr ".t" -type "double3" 0.04192110885550715 -0.090809517989211691 1.5007351236359558 ;
	setAttr ".r" -type "double3" 106.03807426963054 0.24854469306104665 -179.13554689878279 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -0.37268767123615903 0.85129236659217522 1.2445404013339536 ;
	setAttr ".pa" -type "double3" 106.03807214742264 0.24854722216303193 -179.13553528646449 ;
createNode joint -n "IKXMiddleToe2_End_L" -p "IKXMiddleToe1_L";
	setAttr ".t" -type "double3" -7.1054273576010019e-15 -0.91899691277650053 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.8347794005392162 -5.3930280317565758e-13 -6.5161614036483786e-13 ;
createNode ikEffector -n "effector5" -p "IKXMiddleToe1_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "IKFKAlignedLeg_L" -p "IKXAnkle_L";
	setAttr ".t" -type "double3" -1.4039108009455958e-07 -7.905189119972178e-08 -4.6006343623616885e-07 ;
	setAttr ".r" -type "double3" 1.2339322096049914e-06 5.0662754948675815e-06 -2.4733728563512062e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000009 1.0000000000000002 1.0000000000000004 ;
createNode orientConstraint -n "IKXAnkle_L_orientConstraint1" -p "IKXAnkle_L";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 53.603992944451882 1.1158140672460353 1.2888968359781061 ;
	setAttr ".o" -type "double3" 180 -1.7669610520757697 0 ;
	setAttr ".rsrr" -type "double3" 53.614567097584192 0.067470281495748061 -0.13352624719142703 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector6" -p "IKXAnkle_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector4" -p "IKXKnee_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_L" -p "IKXKnee_L";
	setAttr ".t" -type "double3" 0 0 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 53.629099335104378 1.1155319192319897 1.2891436652395856 ;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 1.0000000000000004 ;
createNode annotationShape -n "PoleAnnotationLeg_LShape" -p "PoleAnnotationLeg_L";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintHip_L_parentConstraint1" -p "IKParentConstraintHip_L";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -25.139577240952999 4.4042152622383557e-06 90.000000396222816 ;
	setAttr ".rst" -type "double3" 1.9559509754180908 8.2974197864532471 -0.77453863620758068 ;
	setAttr ".rsrr" -type "double3" -25.139579032906394 -1.7566776614494369e-15 90.000000000000242 ;
	setAttr -k on ".w0";
createNode transform -n "IKHandle" -p "IKSystem";
createNode transform -n "IKParentConstraintLeg_R" -p "IKHandle";
	setAttr ".t" -type "double3" -4.7127432569829528 0.23096786880712772 1.6526076403146557 ;
	setAttr ".ro" 3;
createNode transform -n "IKExtraLeg_R" -p "IKParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_R" -p "IKExtraLeg_R";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "toe" -ln "toe" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".toe";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
createNode nurbsCurve -n "IKLeg_RShape" -p "IKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-2.3619209591378851 -0.20097692929101851 2.6905080769999996
		2.3619209591378851 -0.20097692929101851 2.6905080769999996
		2.3619209591378851 0.99640672790898144 2.6905080769999996
		1.5293311700000001 0.99640672790898144 -1.4785317810000005
		1.5293311700000001 -0.20097692929101851 -1.4785317810000005
		-1.5293311700000001 -0.20097692929101851 -1.4785317810000005
		-1.5293311700000001 0.99640672790898144 -1.4785317810000005
		-2.3619209591378851 0.99640672790898144 2.6905080769999996
		-2.3619209591378851 -0.20097692929101851 2.6905080769999996
		-1.5293311700000001 -0.20097692929101851 -1.4785317810000005
		-1.5293311700000001 0.99640672790898144 -1.4785317810000005
		1.5293311700000001 0.99640672790898144 -1.4785317810000005
		1.523013768 -0.20097692929101851 -1.4768483840000004
		2.3521642733775514 -0.20097692929101851 2.6888246789999997
		2.3619209591378851 0.99640672790898144 2.6905080769999996
		-2.3619209591378851 0.99640672790898144 2.6905080769999996
		;
createNode transform -n "IKFootRollLeg_R" -p "IKLeg_R";
	setAttr ".r" -type "double3" 0 0.64805701576220909 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
createNode transform -n "IKRollLegHeel_R" -p "IKFootRollLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.14245815004146767 -0.24842271751458384 -0.7907737453209327 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999956 1 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_R" -p "IKRollLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_R" -p "IKExtraLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_RShape" -p "IKLegHeel_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		-1.002780287e-16 -0.45161209270000002 -1.3086588599999999e-16
		-5.1353462759999998e-17 -0.31933797320000001 0.31933797320000001
		2.7653265189999997e-17 -1.3607953719999999e-16 0.45161209270000002
		9.0461085430000003e-17 0.31933797320000001 0.31933797320000001
		1.002780287e-16 0.45161209270000002 2.4256173589999999e-16
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKRollLegToe_R" -p "IKLegHeel_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.14245815004147477 -0.090519920461809705 3.1768160492939064 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 9.9392333795734924e-17 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999989 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegToe_R" -p "IKRollLegToe_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -2.7755575615628914e-17 1.7763568394002505e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegToe_R" -p "IKExtraLegToe_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.9392333795734924e-17 0 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegToe_RShape" -p "IKLegToe_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		-1.002780287e-16 -0.45161209270000002 -1.3086588599999999e-16
		-5.1353462759999998e-17 -0.31933797320000001 0.31933797320000001
		2.7653265189999997e-17 -1.3607953719999999e-16 0.45161209270000002
		9.0461085430000003e-17 0.31933797320000001 0.31933797320000001
		1.002780287e-16 0.45161209270000002 2.4256173589999999e-16
		5.1353462759999998e-17 0.31933797320000001 -0.31933797320000001
		-2.7653265189999997e-17 -5.1523539830000001e-17 -0.45161209270000002
		-9.0461085430000003e-17 -0.31933797320000001 -0.31933797320000001
		;
createNode transform -n "IKLiftToeLegToe_R" -p "IKLegToe_R";
	setAttr ".t" -type "double3" -0.012607783132706274 0.24813315230728381 -0.88477472610528596 ;
	setAttr ".r" -type "double3" 0 -9.9392333795734924e-17 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode ikHandle -n "IKXLegHandleToe_R" -p "IKLiftToeLegToe_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0.012607723420131832 -0.24813313728603184 0.88477472679970548 ;
	setAttr ".r" -type "double3" 74.357120907418661 -1.166809317454472 178.74932655832609 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
	setAttr ".hs" 1;
	setAttr ".pv" -type "double3" -0.011310487498786459 0 0.99993603439047041 ;
	setAttr ".roc" yes;
createNode transform -n "IKRollLegBall_R" -p "IKLegToe_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.012607766382525476 0.24813311496938176 -0.88477473244616212 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 9.9392333795734924e-17 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_R" -p "IKRollLegBall_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_R" -p "IKExtraLegBall_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_RShape" -p "IKLegBall_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-9.9600445170000006e-17 0.6017504897 -0.6017504897
		-2.4847825250000002e-16 -1.550498299e-16 -0.85100370380000001
		-3.6683134469999999e-16 -0.6017504897 -0.6017504897
		-3.8533008560000004e-16 -0.85100370380000001 -3.850084044e-16
		-2.9313816359999997e-16 -0.6017504897 0.6017504897
		-1.4426035630000001e-16 -1.9846354150000001e-16 0.85100370380000001
		-2.5907264070000001e-17 0.6017504897 0.6017504897
		-7.4085231850000001e-18 0.85100370380000001 2.027459622e-16
		-9.9600445170000006e-17 0.6017504897 -0.6017504897
		-2.4847825250000002e-16 -1.550498299e-16 -0.85100370380000001
		-3.6683134469999999e-16 -0.6017504897 -0.6017504897
		;
createNode transform -n "IKFootPivotBallReverseLeg_R" -p "IKLegBall_R";
	setAttr ".r" -type "double3" 0 -0.64805701576220887 0 ;
createNode ikHandle -n "IKXLegHandle_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -0.0043731081810278738 0.090809523007011478 -1.5013141420155911 ;
	setAttr ".ro" 3;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_R_poleVectorConstraint1" -p "IKXLegHandle_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.2850558895439237 -6.4563781283511119 -6.797591635879435 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_R" -p "IKFootPivotBallReverseLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -1.667739102373389e-08 3.7337902047962508e-08 6.5299237128613186e-09 ;
	setAttr ".r" -type "double3" 179.99999876606776 1.7669610520758554 1.4817657568029597e-24 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1 ;
	setAttr ".hs" 1;
	setAttr ".pv" -type "double3" -1.7347234759768071e-18 0 1 ;
	setAttr ".roc" yes;
createNode transform -n "PoleAimLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000004 ;
createNode aimConstraint -n "PoleAimLeg_R_aimConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -sn "w0" -ln "IKLeg_RW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" 1.0192430869620359 -16.759523343412283 -93.530573818529376 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_R_pointConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "IKXHip_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -4.2152863618522973 8.293707314540212 -0.78008015068732972 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "PoleExtraLeg_R" -p "PoleParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_R" -p "PoleExtraLeg_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_RShape" -p "PoleLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		-1.6265306409999996e-32 0.32989960689999998 -7.3252427879999994e-17
		2.1975728359999999e-16 1.0987864179999999e-16 0.49484941030000001
		1.6265306409999996e-32 -0.32989960689999998 7.3252427879999994e-17
		8.1326532040000003e-33 -0.16494980340000001 3.6626213939999997e-17
		-2.1975728359999999e-16 -0.16494980340000001 -0.49484941030000001
		-2.1975728359999999e-16 0.16494980340000001 -0.49484941030000001
		-8.1326532040000003e-33 0.16494980340000001 -3.6626213939999997e-17
		;
createNode transform -n "PoleAnnotateTargetLeg_R" -p "PoleLeg_R";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_RShape" -p "PoleAnnotateTargetLeg_R";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_R_parentConstraint1" -p "PoleParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_RStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" -8.8817841970012523e-16 2.2204460492503131e-16 
		0 ;
	setAttr ".tg[1].tot" -type "double3" 4.2860516332513807 -1.0341265169533171 -8.3728001867784521 ;
	setAttr ".tg[1].tor" -type "double3" 16.789600337012697 5.7093259062015257e-16 93.380429074416966 ;
	setAttr ".lr" -type "double3" 8.8960857103148253e-08 -1.0345458567703082e-08 -3.4288192589161301e-08 ;
	setAttr ".rst" -type "double3" -5.500342251396221 1.8373291861891008 -7.5776717865667651 ;
	setAttr ".rsrr" -type "double3" -4.1380371544220237e-33 -1.5902773407317584e-15 
		2.981770013872047e-16 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintLeg_L" -p "IKHandle";
	setAttr ".t" -type "double3" 4.7127432569829493 0.23096786880712103 1.6526076403146526 ;
	setAttr ".ro" 3;
createNode transform -n "IKExtraLeg_L" -p "IKParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_L" -p "IKExtraLeg_L";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "toe" -ln "toe" -at "double";
	addAttr -ci true -k true -sn "roll" -ln "roll" -min -5 -max 10 -at "double";
	addAttr -ci true -k true -sn "rollAngle" -ln "rollAngle" -dv 25 -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".toe";
	setAttr -k on ".roll";
	setAttr -k on ".rollAngle";
createNode nurbsCurve -n "IKLeg_LShape" -p "IKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		2.3619209591378887 -0.20097692929101185 2.6905080770000023
		-2.3619209591378816 -0.20097692929101185 2.6905080770000023
		-2.3619209591378816 0.99640672790898799 2.6905080770000023
		-1.5293311699999963 0.99640672790898799 -1.4785317809999974
		-1.5293311699999963 -0.20097692929101185 -1.4785317809999974
		1.5293311700000034 -0.20097692929101185 -1.4785317809999974
		1.5293311700000034 0.99640672790898799 -1.4785317809999974
		2.3619209591378887 0.99640672790898799 2.6905080770000023
		2.3619209591378887 -0.20097692929101185 2.6905080770000023
		1.5293311700000034 -0.20097692929101185 -1.4785317809999974
		1.5293311700000034 0.99640672790898799 -1.4785317809999974
		-1.5293311699999963 0.99640672790898799 -1.4785317809999974
		-1.5230137679999969 -0.20097692929101185 -1.4768483839999973
		-2.3521642733775479 -0.20097692929101185 2.6888246790000032
		-2.3619209591378816 0.99640672790898799 2.6905080770000023
		2.3619209591378887 0.99640672790898799 2.6905080770000023
		;
createNode transform -n "IKFootRollLeg_L" -p "IKLeg_L";
	setAttr ".r" -type "double3" 0 -0.64805701576164676 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "IKRollLegHeel_L" -p "IKFootRollLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.14245815004147744 -0.24842271751457792 -0.79077374532092892 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegHeel_L" -p "IKRollLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 6.9388939039072284e-18 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegHeel_L" -p "IKExtraLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegHeel_LShape" -p "IKLegHeel_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		4.4408920985006262e-15 0.31933797320000068 -0.31933797319999935
		7.1054273576010019e-15 6.106226635438361e-16 -0.45161209269999936
		5.3290705182007514e-15 -0.31933797319999935 -0.3193379731999994
		1.7763568394002505e-15 -0.45161209269999936 5.5511151231257827e-16
		-8.8817841970012523e-16 -0.31933797319999935 0.31933797320000079
		-3.5527136788005009e-15 5.2735593669694936e-16 0.45161209270000074
		-1.7763568394002505e-15 0.31933797320000068 0.31933797320000079
		1.7763568394002505e-15 0.45161209270000074 8.8817841970012523e-16
		4.4408920985006262e-15 0.31933797320000068 -0.31933797319999935
		7.1054273576010019e-15 6.106226635438361e-16 -0.45161209269999936
		5.3290705182007514e-15 -0.31933797319999935 -0.3193379731999994
		;
createNode transform -n "IKRollLegToe_L" -p "IKLegHeel_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -0.14245815004149875 -0.090519920461808207 3.1768160492939019 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" 0 -9.9392333795734887e-17 0 ;
	setAttr ".s" -type "double3" 1.0000000000000002 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegToe_L" -p "IKRollLegToe_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 0.99999999999999967 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegToe_L" -p "IKExtraLegToe_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegToe_LShape" -p "IKLegToe_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0 0.31933797319999929 -0.31933797319999346
		1.7763568394002505e-15 -9.1593399531575415e-16 -0.45161209269999336
		8.8817841970012523e-16 -0.31933797320000101 -0.31933797319999346
		-2.6645352591003757e-15 -0.45161209270000113 7.1054273576010019e-15
		-6.2172489379008766e-15 -0.31933797320000101 0.31933797320000767
		-7.9936057773011255e-15 -9.9920072216264089e-16 0.45161209270000757
		-7.1054273576010019e-15 0.31933797319999929 0.31933797320000767
		-2.6645352591003757e-15 0.45161209269999936 7.1054273576010019e-15
		0 0.31933797319999929 -0.31933797319999346
		1.7763568394002505e-15 -9.1593399531575415e-16 -0.45161209269999336
		8.8817841970012523e-16 -0.31933797320000101 -0.31933797319999346
		;
createNode transform -n "IKLiftToeLegToe_L" -p "IKLegToe_L";
	setAttr ".t" -type "double3" 0.012607783132713379 0.24813315230727653 -0.88477472610528052 ;
	setAttr ".s" -type "double3" 1 1.0000000000000004 1 ;
createNode ikHandle -n "IKXLegHandleToe_L" -p "IKLiftToeLegToe_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" -0.012607723420138052 -0.24813313728603081 0.88477472679970415 ;
	setAttr ".r" -type "double3" 74.357120907418604 178.83319068254505 1.2506734416739222 ;
	setAttr ".s" -type "double3" 1.0000000000000009 1.0000000000000002 1.0000000000000004 ;
	setAttr ".hs" 1;
	setAttr ".pv" -type "double3" 0.011310487498776644 0 0.99993603439047041 ;
	setAttr ".roc" yes;
createNode transform -n "IKRollLegBall_L" -p "IKLegToe_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0.012607766382531692 0.24813311496938067 -0.88477473244615723 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".s" -type "double3" 1 1.0000000000000004 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKExtraLegBall_L" -p "IKRollLegBall_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLegBall_L" -p "IKExtraLegBall_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mnrl" -type "double3" -45 0 0 ;
	setAttr ".mxrl" -type "double3" 45 0 0 ;
	setAttr ".mrye" yes;
	setAttr ".mrze" yes;
	setAttr ".xrye" yes;
	setAttr ".xrze" yes;
	setAttr ".smd" 1;
createNode nurbsCurve -n "IKLegBall_LShape" -p "IKLegBall_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.3290705182007514e-15 0.60175048970000022 -0.60175048969999834
		7.9936057773011255e-15 1.3877787807814457e-16 -0.85100370379999868
		5.3290705182007514e-15 -0.60175048969999967 -0.60175048969999834
		-8.8817841970012523e-16 -0.85100370379999968 8.8817841970012523e-16
		-6.2172489379008766e-15 -0.60175048969999967 0.60175048970000145
		-8.8817841970012523e-15 1.1102230246251563e-16 0.85100370380000179
		-6.2172489379008766e-15 0.60175048970000022 0.60175048970000145
		-8.8817841970012523e-16 0.85100370380000034 1.3322676295501878e-15
		5.3290705182007514e-15 0.60175048970000022 -0.60175048969999834
		7.9936057773011255e-15 1.3877787807814457e-16 -0.85100370379999868
		5.3290705182007514e-15 -0.60175048969999967 -0.60175048969999834
		;
createNode transform -n "IKFootPivotBallReverseLeg_L" -p "IKLegBall_L";
	setAttr ".r" -type "double3" 0 0.64805701576164665 0 ;
createNode ikHandle -n "IKXLegHandle_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0.0043731081810278738 0.090809523007005594 -1.5013141420155918 ;
	setAttr ".ro" 3;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_L_poleVectorConstraint1" -p "IKXLegHandle_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.2850558895439104 -6.4563781283511119 -6.7975916358794422 ;
	setAttr -k on ".w0";
createNode ikHandle -n "IKXLegHandleBall_L" -p "IKFootPivotBallReverseLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 1.667739013555547e-08 3.7337895802957981e-08 6.5299228246828989e-09 ;
	setAttr ".r" -type "double3" -1.2339322274074166e-06 -1.7669610520757704 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr ".hs" 1;
	setAttr ".pv" -type "double3" 1.7347234759768071e-18 0 1 ;
	setAttr ".roc" yes;
createNode transform -n "PoleAimLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode aimConstraint -n "PoleAimLeg_L_aimConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -sn "w0" -ln "IKLeg_LW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -1.0192430869620377 -16.759523343412258 -86.46942618147061 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_L_pointConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "IKXHip_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 4.2152863618522964 8.2937073145402067 -0.78008015068732861 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 0.99999999999999967 1 0.99999999999999967 ;
createNode transform -n "PoleExtraLeg_L" -p "PoleParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_L" -p "PoleExtraLeg_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 8.8817841970012523e-16 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_LShape" -p "PoleLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		4.7468695640873193e-09 0.16494985579528265 7.8920741053423171e-08
		4.7468695640873193e-09 0.32989965929528253 7.8920741053423171e-08
		4.7468695640873193e-09 5.2395282779116314e-08 0.49484948922074162
		4.7468695640873193e-09 -0.3298995545047172 7.8920741053423171e-08
		4.7468695640873193e-09 -0.16494975100471732 7.8920741053423171e-08
		4.7468695640873193e-09 -0.16494975100471732 -0.49484933137925857
		4.7468695640873193e-09 0.16494985579528265 -0.49484933137925857
		4.7468695640873193e-09 0.16494985579528265 7.8920741053423171e-08
		;
createNode transform -n "PoleAnnotateTargetLeg_L" -p "PoleLeg_L";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_LShape" -p "PoleAnnotateTargetLeg_L";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_L_parentConstraint1" -p "PoleParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_LStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" 0 -2.2204460492503131e-16 0 ;
	setAttr ".tg[1].tot" -type "double3" 4.2860516332513807 1.0341265169533027 -8.3728001867784609 ;
	setAttr ".tg[1].tor" -type "double3" 16.789600337012676 -2.0934194989405603e-15 
		86.619570925583062 ;
	setAttr ".lr" -type "double3" -1.1470222398044074e-06 -6.7937986082879798e-09 -2.2516783315029588e-08 ;
	setAttr ".rst" -type "double3" 5.5003422513962068 1.8373291861890952 -7.5776717865667731 ;
	setAttr ".rsrr" -type "double3" 2.5931699501044686e-31 3.1805546814635168e-15 9.3428793767990803e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKStatic" -p "IKSystem";
createNode transform -n "PoleParentConstraintLeg_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -5.500342251396221 1.8373291861891008 -7.5776717865667651 ;
createNode transform -n "PoleParentConstraintLeg_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 5.5003422513962077 1.8373291861890952 -7.5776717865667731 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "IKCrv" -p "IKSystem";
	setAttr ".it" no;
createNode transform -n "IKMessure" -p "IKSystem";
createNode transform -n "FKIKSystem" -p "MotionSystem";
createNode transform -n "FKIKParentConstraintLeg_R" -p "FKIKSystem";
createNode transform -n "FKIKLeg_R" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Hip";
	setAttr -l on ".middleJoint" -type "string" "Knee";
	setAttr -l on ".endJoint" -type "string" "Ankle";
createNode nurbsCurve -n "FKIKLeg_RShape" -p "FKIKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		-0.3080633183 -0.07701582958 -6.8403797810000007e-17
		-0.29706105700000002 -0.07701582958 -6.5960805029999992e-17
		-0.28605879560000003 -0.066013568209999998 -6.3517812250000002e-17
		-0.28605879560000003 0.066013568209999998 -6.3517812250000002e-17
		-0.29706105700000002 0.07701582958 -6.5960805029999992e-17
		-0.3080633183 0.07701582958 -6.8403797810000007e-17
		-0.3080633183 0.088018090950000002 -6.8403797810000007e-17
		-0.11002261369999999 0.088018090950000002 -2.4429927789999999e-17
		-0.11002261369999999 0.07701582958 -2.4429927789999999e-17
		-0.1210248751 0.07701582958 -2.6872920570000002e-17
		-0.13202713639999999 0.066013568209999998 -2.9315913349999996e-17
		-0.13202713639999999 0.01100226137 -2.9315913349999996e-17
		-0.066013568209999998 0.066013568209999998 -1.465795667e-17
		-0.07701582958 0.07701582958 -1.710094945e-17
		-0.088018090950000002 0.07701582958 -1.954394223e-17
		-0.088018090950000002 0.088018090950000002 -1.954394223e-17
		-0.022004522740000001 0.088018090950000002 -4.8859855579999998e-18
		-0.022004522740000001 0.07701582958 -4.8859855579999998e-18
		-0.03300678411 0.07701582958 -7.3289783369999998e-18
		-0.11002261369999999 0.01100226137 -2.4429927789999999e-17
		-0.022004522740000001 -0.07701582958 -4.8859855579999998e-18
		-0.01100226137 -0.07701582958 -2.4429927789999999e-18
		0.044009045480000002 0.088018090950000002 9.7719711159999997e-18
		0.209042966 0.088018090950000002 4.6416862800000002e-17
		0.209042966 0.07701582958 4.6416862800000002e-17
		0.19804070460000001 0.07701582958 4.3973870020000011e-17
		0.1870384433 0.066013568209999998 4.1530877239999996e-17
		0.1870384433 0.01100226137 4.1530877239999996e-17
		0.25305201150000001 0.066013568209999998 5.6188833920000001e-17
		0.24204975009999999 0.07701582958 5.3745841140000004e-17
		0.23104748880000001 0.07701582958 5.1302848360000001e-17
		0.23104748880000001 0.088018090950000002 5.1302848360000001e-17
		0.29706105700000002 0.088018090950000002 6.5960805029999992e-17
		0.29706105700000002 0.07701582958 6.5960805029999992e-17
		0.28605879560000003 0.07701582958 6.3517812250000002e-17
		0.209042966 0.01100226137 4.6416862800000002e-17
		0.29706105700000002 -0.07701582958 6.5960805029999992e-17
		0.3080633183 -0.07701582958 6.8403797810000007e-17
		0.3080633183 -0.088018090950000002 6.8403797810000007e-17
		0.24204975009999999 -0.088018090950000002 5.3745841140000004e-17
		0.24204975009999999 -0.07701582958 5.3745841140000004e-17
		0.25305201150000001 -0.07701582958 5.6188833920000001e-17
		0.1870384433 -0.01100226137 4.1530877239999996e-17
		0.1870384433 -0.066013568209999998 4.1530877239999996e-17
		0.19804070460000001 -0.07701582958 4.3973870020000011e-17
		0.209042966 -0.07701582958 4.6416862800000002e-17
		0.209042966 -0.088018090950000002 4.6416862800000002e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.1540316592 -0.07701582958 3.4201898910000001e-17
		0.16503392049999999 -0.066013568209999998 3.6644891679999997e-17
		0.16503392049999999 0.066013568209999998 3.6644891679999997e-17
		0.1540316592 0.07701582958 3.4201898910000001e-17
		0.1210248751 0.07701582958 2.6872920570000002e-17
		0.11002261369999999 0.066013568209999998 2.4429927789999999e-17
		0.11002261369999999 -0.066013568209999998 2.4429927789999999e-17
		0.1210248751 -0.07701582958 2.6872920570000002e-17
		0.13202713639999999 -0.07701582958 2.9315913349999996e-17
		0.13202713639999999 -0.088018090950000002 2.9315913349999996e-17
		0.066013568209999998 -0.088018090950000002 1.465795667e-17
		0.066013568209999998 -0.07701582958 1.465795667e-17
		0.07701582958 -0.07701582958 1.710094945e-17
		0.088018090950000002 -0.066013568209999998 1.954394223e-17
		0.088018090950000002 0.066013568209999998 1.954394223e-17
		0.07701582958 0.07701582958 1.710094945e-17
		0.055011306849999997 0.07701582958 1.2214963889999999e-17
		0 -0.07701582958 0
		0 -0.088018090950000002 0
		-0.07701582958 -0.088018090950000002 -1.710094945e-17
		-0.07701582958 -0.07701582958 -1.710094945e-17
		-0.066013568209999998 -0.07701582958 -1.465795667e-17
		-0.13202713639999999 -0.01100226137 -2.9315913349999996e-17
		-0.13202713639999999 -0.066013568209999998 -2.9315913349999996e-17
		-0.1210248751 -0.07701582958 -2.6872920570000002e-17
		-0.11002261369999999 -0.07701582958 -2.4429927789999999e-17
		-0.11002261369999999 -0.088018090950000002 -2.4429927789999999e-17
		-0.1760361819 -0.088018090950000002 -3.9087884459999999e-17
		-0.1760361819 -0.07701582958 -3.9087884459999999e-17
		-0.16503392049999999 -0.07701582958 -3.6644891679999997e-17
		-0.1540316592 -0.066013568209999998 -3.4201898910000001e-17
		-0.1540316592 0.066013568209999998 -3.4201898910000001e-17
		-0.16503392049999999 0.07701582958 -3.6644891679999997e-17
		-0.1870384433 0.07701582958 -4.1530877239999996e-17
		-0.1870384433 0.044009045480000002 -4.1530877239999996e-17
		-0.19804070460000001 0.044009045480000002 -4.3973870020000011e-17
		-0.19804070460000001 0.066013568209999998 -4.3973870020000011e-17
		-0.209042966 0.07701582958 -4.6416862800000002e-17
		-0.25305201150000001 0.07701582958 -5.6188833920000001e-17
		-0.2640542729 0.066013568209999998 -5.8631826699999991e-17
		-0.2640542729 0.01100226137 -5.8631826699999991e-17
		-0.23104748880000001 0.01100226137 -5.1302848360000001e-17
		-0.22004522739999999 0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 0.03300678411 -4.8859855579999998e-17
		-0.209042966 0.03300678411 -4.6416862800000002e-17
		-0.209042966 -0.022004522740000001 -4.6416862800000002e-17
		-0.22004522739999999 -0.022004522740000001 -4.8859855579999998e-17
		-0.22004522739999999 -0.01100226137 -4.8859855579999998e-17
		-0.23104748880000001 0 -5.1302848360000001e-17
		-0.2640542729 0 -5.8631826699999991e-17
		-0.2640542729 -0.066013568209999998 -5.8631826699999991e-17
		-0.25305201150000001 -0.07701582958 -5.6188833920000001e-17
		-0.24204975009999999 -0.07701582958 -5.3745841140000004e-17
		-0.24204975009999999 -0.088018090950000002 -5.3745841140000004e-17
		-0.3080633183 -0.088018090950000002 -6.8403797810000007e-17
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_R_parentConstraint1" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 2.1298220687390907 1.2546325697082048 0.46024214276189435 ;
	setAttr ".tg[0].tor" -type "double3" -154.86042096709366 1.3115279850922298e-13 
		-90.000000000000284 ;
	setAttr ".lr" -type "double3" -2.7257924134550872e-06 9.6318144925612825e-07 2.0524795952408073e-06 ;
	setAttr ".rst" -type "double3" -3.2105835451263065 6.1738216257522645 -0.28638320365612685 ;
	setAttr ".rsrr" -type "double3" -1.2722218725854124e-14 1.1449996853268662e-13 -5.7249984266343308e-14 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintLeg_L" -p "FKIKSystem";
createNode transform -n "FKIKLeg_L" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Hip";
	setAttr -l on ".middleJoint" -type "string" "Knee";
	setAttr -l on ".endJoint" -type "string" "Ankle";
createNode nurbsCurve -n "FKIKLeg_LShape" -p "FKIKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		0.30806317861596444 -0.08801811629328693 3.2018847384573945e-07
		0.30806317901009361 -0.077015854923287108 3.1932095062581212e-07
		0.29706091771009335 -0.077015854529157934 3.1844290543370235e-07
		0.28605865670422315 -0.066013592765028939 3.166973369661541e-07
		0.28605866143376923 0.066013543654970697 3.0628705854907068e-07
		0.29706092322789779 0.077015804630841345 3.0629758057676426e-07
		0.3080631845278976 0.077015804236713059 3.0717562582438518e-07
		0.30806318492202633 0.088018065606712881 3.0630810265996899e-07
		0.11002248032202731 0.088018072701030903 2.9050328842483708e-07
		0.1100224799278986 0.077015811331030193 2.9137081164476442e-07
		0.12102474132789842 0.077015810936901907 2.9224885689238533e-07
		0.13202700223376995 0.066013549172772912 2.9399442535993359e-07
		0.13202700026312542 0.011002242332772916 2.9833204129303681e-07
		0.066013434043769692 0.066013551537546178 2.8872615392971923e-07
		0.077015695807898688 0.077015812513416826 2.8873667601292397e-07
		0.088017957177898509 0.07701581211928854 2.8961472120503373e-07
		0.088017957572027683 0.088018073489288362 2.887471979851064e-07
		0.022004389362027421 0.08801807585406074 2.8347892661040319e-07
		0.022004388967898691 0.077015814484060918 2.8434644983033053e-07
		0.033006650337898513 0.077015814089931744 2.8522449507795145e-07
		0.11002247756312532 0.011002243121030377 2.9657595085330613e-07
		0.0220043834500947 -0.077015844675938375 2.9649177463175747e-07
		0.011002122080094434 -0.077015844281810075 2.9561372938413655e-07
		-0.044009178857971953 0.088018078218834006 2.7821065523569999e-07
		-0.2090430993779715 0.088018084130765395 2.6503997679894198e-07
		-0.20904309977210023 0.077015822760764685 2.6590749996335816e-07
		-0.1980408383721004 0.077015822366636399 2.6678554521097908e-07
		-0.18703857746622887 0.066013560602507404 2.6853111362301618e-07
		-0.18703857943687341 0.011002253762507408 2.7286872966714171e-07
		-0.25305214566622913 0.06601356296728067 2.6326284224831298e-07
		-0.24204988387210016 0.077015823943151318 2.6327336427600656e-07
		-0.23104762257210032 0.077015823549023046 2.6415140952362748e-07
		-0.23104762217797159 0.088018084919022854 2.6328388630370014e-07
		-0.29706119037797096 0.088018087283795232 2.5801561492899694e-07
		-0.29706119077209969 0.07701582591379541 2.5888313809341312e-07
		-0.28605892937209987 0.077015825519667125 2.5976118334103404e-07
		-0.20904310213687349 0.011002254550764867 2.7111263922741102e-07
		-0.29706119628990413 -0.077015833246203869 2.7102846295035121e-07
		-0.30806345758990394 -0.077015832852075583 2.7015041770273029e-07
		-0.30806345798403267 -0.088018094222075405 2.7101794092265763e-07
		-0.2420498897840333 -0.088018096586847783 2.7628621229736083e-07
		-0.24204988938990457 -0.077015835216847975 2.7541868913294465e-07
		-0.2530521507899044 -0.077015834822719675 2.7454064388532373e-07
		-0.18703858022513131 -0.011002268977492236 2.7460377605148523e-07
		-0.1870385821957754 -0.066013575817492232 2.7894139209561075e-07
		-0.19804084388990439 -0.077015836793363782 2.7893087006791717e-07
		-0.20904310528990463 -0.077015836399234594 2.7805282482029625e-07
		-0.20904310568403339 -0.088018097769234416 2.7892034798471244e-07
		-0.13202727608403375 -0.088018100528135065 2.8506666460703656e-07
		-0.13202727568990502 -0.077015839158135257 2.8419914144262037e-07
		-0.15403179848990467 -0.077015838369877798 2.8244305094737859e-07
		-0.16503405939577531 -0.066013576605748803 2.8069748253534144e-07
		-0.16503405466622922 0.066013559814250833 2.7028720411825802e-07
		-0.15403179297210023 0.077015820790122369 2.702977261459516e-07
		-0.12102500887210076 0.07701581960773575 2.7293186177779205e-07
		-0.11002274786622968 0.066013557843606741 2.7467743030085146e-07
		-0.11002275259577576 -0.066013578576392895 2.8508770866242372e-07
		-0.12102501438990476 -0.077015839552264431 2.8507718669024129e-07
		-0.13202727568990502 -0.077015839158135257 2.8419914144262037e-07
		-0.13202727608403375 -0.088018100528135065 2.8506666460703656e-07
		-0.066013707894033935 -0.088018102892908345 2.9033493598173976e-07
		-0.066013707499905205 -0.077015841522908524 2.8946741276181243e-07
		-0.077015968869905027 -0.07701584112877935 2.8858936756970266e-07
		-0.088018229845775675 -0.066013579364650354 2.8684379910215441e-07
		-0.088018225116229587 0.066013557055349281 2.7643352068507099e-07
		-0.077015963352100592 0.077015818031219929 2.7644404271276457e-07
		-0.055011440622100949 0.07701581724296247 2.7820013320800641e-07
		-1.3928990494349591e-07 -0.077015843887680902 2.9473568419202678e-07
		-1.3968403367314863e-07 -0.088018105257680723 2.9560320735644297e-07
		0.077015689895965522 -0.088018108016583163 3.0174952397876709e-07
		0.077015690290094252 -0.077015846646582453 3.008820008143509e-07
		0.06601342892009443 -0.077015846252454181 3.0000395556672999e-07
		0.13202699947486751 -0.011002280407226728 3.0006708773289148e-07
		0.13202699750422386 -0.066013587247226724 3.0440470372150585e-07
		0.12102473581009444 -0.07701584822309826 3.0439418174932342e-07
		0.11002247441009461 -0.077015847828969086 3.035161365017025e-07
		0.11002247401596545 -0.088018109198968908 3.0438365972162984e-07
		0.17603604221596525 -0.088018111563741286 3.0965193109633304e-07
		0.17603604261009398 -0.077015850193741478 3.0878440787640571e-07
		0.16503378121009371 -0.077015849799613179 3.0790636262878479e-07
		0.15403152030422307 -0.066013588035484183 3.0616079421674769e-07
		0.15403152503376916 0.066013548384515452 2.9575051574415312e-07
		0.16503378672789815 0.0770158093603861 2.957610377718467e-07
		0.18703830952789824 0.077015808572129529 2.9751712826708859e-07
		0.18703830834551116 0.044009024472130065 3.0011969792687054e-07
		0.19804056964551145 0.044009024078000891 3.0099774311898031e-07
		0.19804057043376933 0.066013546808000534 2.9926269667912564e-07
		0.20904283222789832 0.07701580778387207 2.9927321870681922e-07
		0.25305187772789806 0.077015806207357151 3.0278539964179174e-07
		0.26405413873376915 0.066013544443228156 3.0453096810933999e-07
		0.26405413676312461 0.01100223760322816 3.0886858409795437e-07
		0.23104735266312473 0.011002238785614791 3.0623444841060277e-07
		0.22004509165725361 0.022004500549743788 3.0448887999856566e-07
		0.22004509205138231 0.03300676191974361 3.0362135677863833e-07
		0.20904283065138252 0.033006762313871896 3.0274331158652856e-07
		0.2090428286807384 -0.022004544536128101 3.0708092751963179e-07
		0.22004509008073825 -0.022004544930256387 3.079589727672527e-07
		0.22004509047486745 -0.011002283560256563 3.0709144954732537e-07
		0.231047352268996 -2.2584385028778797e-08 3.071019716305301e-07
		0.26405413636899588 -2.3766771661826169e-08 3.097361073178817e-07
		0.26405413400422306 -0.06601359197677148 3.1494124652642341e-07
		0.25305187221009406 -0.077015852952643002 3.1493072449872983e-07
		0.2420496108100938 -0.077015852558513842 3.1405267925110891e-07
		0.24204961041596507 -0.088018113928513664 3.149202024155251e-07
		0.30806317861596444 -0.08801811629328693 3.2018847384573945e-07
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_L_parentConstraint1" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "HipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -2.1298220687390943 -1.2546325697082039 -0.46024214276189435 ;
	setAttr ".tg[0].tor" -type "double3" 25.13957903290639 1.0412922365280964e-13 -90.000000000000213 ;
	setAttr ".lr" -type "double3" 1.7919534219594726e-06 4.5725404685536196e-06 3.5869063624349378e-07 ;
	setAttr ".rst" -type "double3" 3.2105835451263038 6.1738216257522609 -0.28638320365612563 ;
	setAttr ".rsrr" -type "double3" 4.7708320221952381e-15 -9.2236085762441977e-14 4.6913181551586868e-14 ;
	setAttr -k on ".w0";
createNode transform -n "RootSystem" -p "MotionSystem";
createNode transform -n "PelvisCenter_M" -p "RootSystem";
	setAttr ".t" -type "double3" 0 7.2655224800109854 0.42850792407989502 ;
	setAttr ".ro" 3;
createNode transform -n "PelvisCenterBtwLegs_M" -p "RootSystem";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 7.2655224800109854 1.6526076403146539 ;
	setAttr ".ro" 3;
createNode orientConstraint -n "PelvisCenterBtwLegs_M_orientConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "PelvisCenterBtwLegs_M_pointConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -1.7763568394002505e-15 0.23096786880712439 1.6526076403146539 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PelvisCenterBtwLegsOffset_M" -p "PelvisCenterBtwLegs_M";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 0 -1.2240997162347591 ;
	setAttr ".ro" 3;
createNode transform -n "GlobalSystem" -p "MotionSystem";
createNode transform -n "GlobalOffsetHead_M" -p "GlobalSystem";
	setAttr ".t" -type "double3" 0 9.4849412441253662 0.047181606292724609 ;
createNode transform -n "GlobalHead_M" -p "GlobalOffsetHead_M";
	setAttr ".ro" 5;
createNode transform -n "GameSkeleton" -p "Main";
createNode joint -n "Pelvis_M" -p "GameSkeleton";
	setAttr ".ro" 3;
createNode joint -n "HipTwist_R" -p "Pelvis_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 205.1395790329064 89.999999999999702 ;
createNode joint -n "Hip_R" -p "HipTwist_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 35.053931728105297 -4.5637958222710893 -85.563283529155967 ;
createNode joint -n "Knee_R" -p "Hip_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -63.76933786411913 -0.474076486599188 -5.8215609805559136 ;
createNode joint -n "Ankle_R" -p "Knee_R";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 53.614411016936472 0.067470119883900365 -0.13352636183799624 ;
createNode joint -n "MiddleToe1_R" -p "Ankle_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 74.332702039399834 179.95182586565662 1.2504139207912697 ;
createNode joint -n "MiddleToe2_End_R" -p "MiddleToe1_R";
	setAttr ".t" -type "double3" 3.5527136788005009e-15 0.91899691277650053 9.9920072216264089e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.8347794621262592 0 0 ;
createNode parentConstraint -n "MiddleToe1_R_parentConstraint1" -p "MiddleToe1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleToe1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.5658655328640833e-06 3.671443298300615e-07 -2.3301030070476523e-06 ;
	setAttr ".rst" -type "double3" -0.041921108855510703 0.090809517989209665 -1.5007351236359554 ;
	setAttr ".rsrr" -type "double3" -2.734372080563963e-06 6.427158965668499e-08 -2.4149259908385004e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Ankle_R_parentConstraint1" -p "Ankle_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXAnkle_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXAnkle_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 1.2293438563680906e-05 1.504115007850025e-05 2.2972983400923341e-06 ;
	setAttr ".rst" -type "double3" -0.020686841895814823 4.9257101146466802 0.36689990046173637 ;
	setAttr ".rsrr" -type "double3" -2.6832243680433438e-06 3.525375554093962e-07 -2.5959461504703078e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Knee_R_parentConstraint1" -p "Knee_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXKnee_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXKnee_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -2.5698092353766278e-06 -4.3068834543423973e-07 -1.5479750498779337e-07 ;
	setAttr ".rst" -type "double3" -0.023204151644971915 5.0337125823674143 0.45114715793262006 ;
	setAttr ".rsrr" -type "double3" -1.560613581466651e-06 2.3855851847954289e-06 -1.1964695772880132e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ElbowShield_R" -p "Hip_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 11.84580473072679 -0.47407648659919366 -5.8215609805559376 ;
createNode joint -n "ElbowShield_End_R" -p "ElbowShield_R";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 1.5871705715078137 0.10623558443189828 ;
	setAttr ".ro" 2;
createNode parentConstraint -n "ElbowShield_R_parentConstraint1" -p "ElbowShield_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbowShield_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 8.7009086209358701e-08 5.8174303681141408e-08 -8.9762048284533675e-08 ;
	setAttr ".rst" -type "double3" 0.061883867177003893 4.9423722817692894 1.105689073656833 ;
	setAttr ".rsrr" -type "double3" 8.7009064945611279e-08 5.8174283703282327e-08 -8.976204210978496e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Hip_R_parentConstraint1" -p "Hip_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHip_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXHip_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -3.4986101496098606e-14 1.0654858182902783e-13 -8.7465253740246747e-14 ;
	setAttr ".rst" -type "double3" 0.0010066817290077523 2.2593353863530679 0.0065937689504749208 ;
	setAttr ".rsrr" -type "double3" -1.4765066016611713e-06 -6.2499002841159386e-08 
		-2.615291114349836e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "HipTwist_R_parentConstraint1" -p "HipTwist_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXHipTwist_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.0639648514587052e-06 2.725792393753111e-06 -2.2672430427380352e-06 ;
	setAttr ".rst" -type "double3" -1.9559509754180908 1.031897306442259 -1.203046560287476 ;
	setAttr ".rsrr" -type "double3" 1.0639648530489828e-06 2.7257923603572866e-06 -2.2672430872658017e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Head_M" -p "Pelvis_M";
	setAttr ".ro" 5;
createNode joint -n "Head_End_M" -p "Head_M";
	setAttr ".t" -type "double3" 3.1589715250000827e-16 1.8033760160193548 1.7486012637846216e-15 ;
createNode joint -n "Jaw_M" -p "Head_M";
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
createNode joint -n "Jaw_End_M" -p "Jaw_M";
	setAttr ".t" -type "double3" 0 3.6496593076700785 1.0658141036401504e-14 ;
	setAttr ".jo" -type "double3" -89.999999999999986 0 0 ;
createNode parentConstraint -n "Jaw_M_parentConstraint1" -p "Jaw_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXJaw_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.5044781545431972e-06 0 0 ;
	setAttr ".rst" -type "double3" 0 -0.19902873039245603 0.4243211448192597 ;
	setAttr ".rsrr" -type "double3" 2.5044781545431972e-06 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Head_M_parentConstraint1" -p "Head_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXHead_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 2.2194187641143799 -0.38132631778717041 ;
	setAttr -k on ".w0";
createNode joint -n "Shoulder_R" -p "Pelvis_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -152.00531705221945 4.9505588210855853 -11.904809842065395 ;
createNode joint -n "Elbow_R" -p "Shoulder_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -129.97952417590869 1.4902849999290719 0.83989225685842239 ;
createNode joint -n "Fingers_R" -p "Elbow_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 36.446714387517368 -2.2310450883288171 -1.1437535407680881 ;
createNode joint -n "Fingers_End_R" -p "Fingers_R";
	setAttr ".t" -type "double3" -3.9968028886505628e-15 1.3895324463926029 7.9936057773011255e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "Fingers_R_parentConstraint1" -p "Fingers_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXFingers_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.2296816747021347e-06 3.0326076601247097e-07 2.6830878236962908e-08 ;
	setAttr ".rst" -type "double3" -0.066073635893799221 3.205342927373545 0.23905971982674681 ;
	setAttr ".rsrr" -type "double3" 4.6977841438951867e-06 1.9166307910052564e-07 -1.7954642711360258e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Elbow_R_parentConstraint1" -p "Elbow_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.5156647724589822e-07 1.5173395874509047e-07 1.4082184256084428e-07 ;
	setAttr ".rst" -type "double3" -0.12905706998152813 3.1327770346165278 -0.12174391540666241 ;
	setAttr ".rsrr" -type "double3" 3.2229138101404635e-06 1.1520612804162744e-07 4.3699255809559341e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Shoulder_R_parentConstraint1" -p "Shoulder_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 3.4760294309174583e-06 5.893176654319481e-08 -1.9724268876121702e-07 ;
	setAttr ".rst" -type "double3" -1.5034208297729501 -0.49391078948974698 0.35724256187677306 ;
	setAttr ".rsrr" -type "double3" 3.4760294050506044e-06 5.8931765748056104e-08 -1.9724269194177168e-07 ;
	setAttr -k on ".w0";
createNode joint -n "HipTwist_L" -p "Pelvis_M";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.7566776614494369e-15 25.139579032906383 90.000000000000242 ;
createNode joint -n "Hip_L" -p "HipTwist_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 35.053931728105304 -4.5637958222710759 -85.563283529155996 ;
createNode joint -n "Knee_L" -p "Hip_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" -63.769337864119102 -0.47407648659918761 -5.8215609805559234 ;
createNode joint -n "Ankle_L" -p "Knee_L";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 53.614411016936465 0.067470119883899934 -0.13352636183799624 ;
createNode joint -n "MiddleToe1_L" -p "Ankle_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 74.332702039399834 179.95182586565664 1.2504139207912697 ;
createNode joint -n "MiddleToe2_End_L" -p "MiddleToe1_L";
	setAttr ".t" -type "double3" -7.9936057773011255e-15 -0.91899691277649742 -1.5543122344752192e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.8347794621262201 0 0 ;
createNode parentConstraint -n "MiddleToe1_L_parentConstraint1" -p "MiddleToe1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleToe1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -4.8565214670544846e-06 -1.3618235361071804e-07 -2.4709215338745954e-06 ;
	setAttr ".rst" -type "double3" 0.041921108855512479 -0.090809517989210858 1.5007351236359554 ;
	setAttr ".rsrr" -type "double3" -2.7343720614806385e-06 6.4271509148894125e-08 -2.414925987061594e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Ankle_L_parentConstraint1" -p "Ankle_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXAnkle_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXAnkle_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.5510781390566308e-05 -1.3177841193279979e-05 -1.5209021184969277e-06 ;
	setAttr ".rst" -type "double3" 0.020687127404826811 -4.9257097793950901 -0.36690018162666616 ;
	setAttr ".rsrr" -type "double3" -3.8128356257089167e-07 2.6388079308685368e-06 -1.2346932780620737e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Knee_L_parentConstraint1" -p "Knee_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXKnee_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXKnee_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 2.3304295491580573e-06 7.0428104277769295e-07 6.9451642006086666e-07 ;
	setAttr ".rst" -type "double3" 0.023204220503563633 -5.0337125221359527 -0.45114748067564125 ;
	setAttr ".rsrr" -type "double3" 7.4504440830611094e-07 2.6405985910421634e-06 1.4488814215022618e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ElbowShield_L" -p "Hip_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 11.845804730726796 -0.47407648659919249 -5.8215609805559385 ;
createNode joint -n "ElbowShield_End_L" -p "ElbowShield_L";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 -1.5871705715078119 -0.1062355844318974 ;
	setAttr ".ro" 2;
createNode parentConstraint -n "ElbowShield_L_parentConstraint1" -p "ElbowShield_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbowShield_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 8.7009095595722185e-08 5.8174277441565291e-08 -8.9762039177711115e-08 ;
	setAttr ".rst" -type "double3" -0.061883867177003893 -4.9423722817692868 -1.1056890736568279 ;
	setAttr ".rsrr" -type "double3" 8.7009079649464634e-08 5.8174277441565297e-08 -8.9762039115590896e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Hip_L_parentConstraint1" -p "Hip_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHip_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXHip_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.6300472500677699e-14 -3.0463604861627165e-06 1.5403523042995683e-06 ;
	setAttr ".rst" -type "double3" -0.0010065920652220228 -2.2593353862247012 -0.0065938266225149533 ;
	setAttr ".rsrr" -type "double3" 1.0615617163707751e-06 2.1772646025816088e-06 -1.6557907198488818e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "HipTwist_L_parentConstraint1" -p "HipTwist_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXHipTwist_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 3.9870285645229463e-06 -1.7919533208102199e-06 2.2672426641864098e-06 ;
	setAttr ".rst" -type "double3" 1.9559509754180913 1.031897306442259 -1.2030465602874734 ;
	setAttr ".rsrr" -type "double3" 3.9870285692937797e-06 -1.7919533239907746e-06 2.2672426653791186e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Shoulder_L" -p "Pelvis_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 27.994682947780603 -4.9505588210855898 11.904809842065388 ;
createNode joint -n "Elbow_L" -p "Shoulder_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -129.97952417590878 1.4902849999290839 0.83989225685841695 ;
createNode joint -n "Fingers_L" -p "Elbow_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 36.446714387517325 -2.2310450883288246 -1.1437535407680888 ;
createNode joint -n "Fingers_End_L" -p "Fingers_L";
	setAttr ".t" -type "double3" 4.8849813083506888e-15 -1.3895324463926029 -1.3322676295501878e-14 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "Fingers_L_parentConstraint1" -p "Fingers_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXFingers_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.2296818018622018e-06 3.0326076593792673e-07 2.6830872571599544e-08 ;
	setAttr ".rst" -type "double3" 0.066073636003408875 -3.2053429678318448 -0.23905970068921745 ;
	setAttr ".rsrr" -type "double3" 3.8459575911040327e-06 2.1907342155495379e-07 -1.2885655364462931e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Elbow_L_parentConstraint1" -p "Elbow_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.5156640093859868e-07 1.5173395695602856e-07 1.4082184010088389e-07 ;
	setAttr ".rst" -type "double3" 0.12905706998152813 -3.1327770328024003 0.1217439620886811 ;
	setAttr ".rsrr" -type "double3" 2.3695207128753261e-06 1.2417799290089713e-07 6.7554257775154727e-08 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Shoulder_L_parentConstraint1" -p "Shoulder_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.6222557198123676e-06 5.8931777952000903e-08 -1.9724268673186406e-07 ;
	setAttr ".rst" -type "double3" 1.5034208297729501 -0.49391078948974609 0.35724256187677428 ;
	setAttr ".rsrr" -type "double3" 2.6222557263722612e-06 5.8931779542278288e-08 -1.9724268384948641e-07 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "Pelvis_M_pointConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 7.2655224800109854 0.42850792407989502 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "Pelvis_M_orientConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode nurbsCurve -n "forwardArrowShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-0.22320054087126528 4.9560475916813373e-17 1.8381116176312879
		-0.44640108174253057 9.9120951833626745e-17 1.8381116176312879
		-1.4868142775044016e-16 2.9736285550088052e-16 2.5077132402450837
		0.44640108174253057 -9.9120951833626745e-17 1.8381116176312879
		0.22320054087126528 -4.9560475916813373e-17 1.8381116176312879
		0.22320054087126545 -3.469233314176939e-16 1.1685099950174922
		-0.22320054087126515 -2.4780237958406709e-16 1.1685099950174922
		-0.22320054087126528 4.9560475916813373e-17 1.8381116176312879
		;
createNode transform -n "items" -p "ctrl_rig";
	setAttr -l on ".it" no;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 12 ".lnk";
	setAttr -s 12 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr ".cm" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".cs" 3;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode shadingEngine -n "irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode shadingEngine -n "BPRig_irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "BPRig_materialInfo3";
createNode ikSCsolver -n "ikSCsolver";
createNode ikRPsolver -n "ikRPsolver";
createNode displayLayer -n "jointLayer";
createNode reverse -n "jointVisReverse";
createNode objectSet -n "ControlSet";
	setAttr ".ihi" 0;
	setAttr -s 65 ".dsm";
createNode objectSet -n "GameSet";
	setAttr ".ihi" 0;
	setAttr -s 21 ".dsm";
createNode objectSet -n "AllSet";
	setAttr ".ihi" 0;
	setAttr -s 314 ".dsm";
	setAttr -s 46 ".dnsm";
createNode objectSet -n "Sets";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dnsm";
createNode unitConversion -n "unitConversion1";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_R";
createNode condition -n "FKIKBlendLegCondition_R";
createNode setRange -n "FKIKBlendLegsetRange_R";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode unitConversion -n "IKLiftToeLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode multiplyDivide -n "Leg_RAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_R";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion4";
	setAttr ".cf" 0.017453292519943295;
createNode blendTwoAttr -n "IKBallToFKBallMiddleToe1blendTwoAttr_R";
	setAttr -s 2 ".i[0:1]"  106.03807214742264 106.03807097905201;
createNode unitConversion -n "unitConversion5";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion6";
	setAttr ".cf" 0.017453292519943295;
createNode blendColors -n "ScaleBlendAnkle_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendKnee_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendHip_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode unitConversion -n "GlobalHead_unitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "GlobalHead_reverse_M";
createNode unitConversion -n "unitConversion7";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_L";
createNode condition -n "FKIKBlendLegCondition_L";
createNode setRange -n "FKIKBlendLegsetRange_L";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode unitConversion -n "IKLiftToeLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode multiplyDivide -n "Leg_LAngleReverse";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode setRange -n "IKRollAngleLeg_L";
	setAttr ".on" -type "float3" -5 0 5 ;
	setAttr ".om" -type "float3" 0 5 10 ;
createNode unitConversion -n "unitConversion8";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion9";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion10";
	setAttr ".cf" 0.017453292519943295;
createNode blendTwoAttr -n "IKBallToFKBallMiddleToe1blendTwoAttr_L";
	setAttr -s 2 ".i[0:1]"  106.03807214742264 106.03807426963054;
createNode unitConversion -n "unitConversion11";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion12";
	setAttr ".cf" 0.017453292519943295;
createNode blendColors -n "ScaleBlendAnkle_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendKnee_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendHip_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode setRange -n "CenterBtwFeet_M";
	setAttr ".v" -type "float3" 0 1 0 ;
	setAttr ".n" -type "float3" 2 0 0 ;
	setAttr ".om" -type "float3" 0.1 0 0 ;
createNode unitConversion -n "PelvisCenterBtwFeetUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "PelvisCenterBtwFeetReverse_M";
createNode dagPose -n "buildPose";
	addAttr -ci true -sn "udAttr" -ln "udAttr" -dt "string";
	setAttr ".udAttr" -type "string" (
		"setAttr FKIKLeg_R.FKIKBlend 10;setAttr FKIKLeg_R.FKVis 0;setAttr FKIKLeg_R.IKVis 1;setAttr IKLegToe_R.rotateX 0;setAttr IKLegToe_R.rotateY -9.93923338e-17;setAttr IKLegToe_R.rotateZ 0;setAttr IKExtraLegToe_R.rotateX 0;setAttr IKExtraLegToe_R.rotateY -0;setAttr IKExtraLegToe_R.rotateZ 0;setAttr IKLegHeel_R.rotateX 0;setAttr IKLegHeel_R.rotateY -0;setAttr IKLegHeel_R.rotateZ 0;setAttr IKExtraLegHeel_R.rotateX 0;setAttr IKExtraLegHeel_R.rotateY -0;setAttr IKExtraLegHeel_R.rotateZ 0;setAttr IKExtraLegBall_R.rotateX 0;setAttr IKExtraLegBall_R.rotateY -0;setAttr IKExtraLegBall_R.rotateZ 0;setAttr IKLegBall_R.rotateX 0;setAttr PoleLeg_L.translateX -8.881784197e-16;setAttr PoleLeg_L.translateY 0;setAttr PoleLeg_L.translateZ 8.881784197e-16;setAttr PoleLeg_L.follow 10;setAttr PoleExtraLeg_L.translateX 0;setAttr PoleExtraLeg_L.translateY 0;setAttr PoleExtraLeg_L.translateZ 0;setAttr IKExtraLeg_L.translateX 0;setAttr IKExtraLeg_L.translateY 0;setAttr IKExtraLeg_L.translateZ 0;setAttr IKExtraLeg_L.rotateX -0;setAttr IKExtraLeg_L.rotateY -0;setAttr IKExtraLeg_L.rotateZ 0;setAttr IKLeg_L.translateX 0;setAttr IKLeg_L.translateY 0;setAttr IKLeg_L.translateZ 0;setAttr IKLeg_L.rotateX -0;setAttr IKLeg_L.rotateY -0;setAttr IKLeg_L.rotateZ 0;setAttr IKLeg_L.swivel 0;setAttr IKLeg_L.toe 0;setAttr IKLeg_L.roll 0;setAttr IKLeg_L.rollAngle 25;setAttr FKIKLeg_L.FKIKBlend 10;setAttr FKIKLeg_L.FKVis 0;setAttr FKIKLeg_L.IKVis 1;setAttr IKExtraLegHeel_L.rotateX 0;setAttr IKExtraLegHeel_L.rotateY -0;setAttr IKExtraLegHeel_L.rotateZ 0;setAttr IKLegHeel_L.rotateX 0;setAttr IKLegHeel_L.rotateY -0;setAttr IKLegHeel_L.rotateZ 0;setAttr IKExtraLegToe_L.rotateX 0;setAttr IKExtraLegToe_L.rotateY -0;setAttr IKExtraLegToe_L.rotateZ 0;setAttr IKLegToe_L.rotateX 0;setAttr IKLegToe_L.rotateY -0;setAttr IKLegToe_L.rotateZ 0;setAttr FKExtraMiddleToe1_R.translateX 0;setAttr FKExtraMiddleToe1_R.translateY -4.440892099e-16;setAttr FKExtraMiddleToe1_R.translateZ 0;setAttr FKExtraMiddleToe1_R.rotateX -0;setAttr FKExtraMiddleToe1_R.rotateY 0;setAttr FKExtraMiddleToe1_R.rotateZ -0;setAttr FKExtraMiddleToe1_R.scaleX 1;setAttr FKExtraMiddleToe1_R.scaleY 1;setAttr FKExtraMiddleToe1_R.scaleZ 1;setAttr FKMiddleToe1_R.translateX 0;setAttr FKMiddleToe1_R.translateY -4.440892099e-16;setAttr FKMiddleToe1_R.translateZ 1.110223025e-16;setAttr FKMiddleToe1_R.rotateX -0;setAttr FKMiddleToe1_R.rotateY 0;setAttr FKMiddleToe1_R.rotateZ -0;setAttr FKMiddleToe1_R.scaleX 1;setAttr FKMiddleToe1_R.scaleY 1;setAttr FKMiddleToe1_R.scaleZ 1;setAttr FKExtraAnkle_R.translateX 0;setAttr FKExtraAnkle_R.translateY 0;setAttr FKExtraAnkle_R.translateZ -2.220446049e-16;setAttr FKExtraAnkle_R.rotateX -0;setAttr FKExtraAnkle_R.rotateY -0;setAttr FKExtraAnkle_R.rotateZ 0;setAttr FKExtraAnkle_R.scaleX 1;setAttr FKExtraAnkle_R.scaleY 1;setAttr FKExtraAnkle_R.scaleZ 1;setAttr IKExtraLeg_R.translateX 0;setAttr IKExtraLeg_R.translateY 0;setAttr IKExtraLeg_R.translateZ 0;setAttr IKExtraLeg_R.rotateX -0;setAttr IKExtraLeg_R.rotateY -0;setAttr IKExtraLeg_R.rotateZ 0;setAttr IKLeg_R.translateX 0;setAttr IKLeg_R.translateY 0;setAttr IKLeg_R.translateZ 0;setAttr IKLeg_R.rotateX -0;setAttr IKLeg_R.rotateY -0;setAttr IKLeg_R.rotateZ 0;setAttr IKLeg_R.swivel 0;setAttr IKLeg_R.toe 0;setAttr IKLeg_R.roll 0;setAttr IKLeg_R.rollAngle 25;setAttr PoleLeg_R.translateX 0;setAttr PoleLeg_R.translateY 0;setAttr PoleLeg_R.translateZ 0;setAttr PoleLeg_R.follow 10;setAttr PoleExtraLeg_R.translateX 0;setAttr PoleExtraLeg_R.translateY 0;setAttr PoleExtraLeg_R.translateZ 0;setAttr FKHip_R.translateX 8.881784197e-16;setAttr FKHip_R.translateY -8.881784197e-16;setAttr FKHip_R.translateZ 0;setAttr FKHip_R.rotateX -0;setAttr FKHip_R.rotateY 0;setAttr FKHip_R.rotateZ 0;setAttr FKHip_R.scaleX 1;setAttr FKHip_R.scaleY 1;setAttr FKHip_R.scaleZ 1;setAttr FKExtraHip_R.translateX 8.881784197e-16;setAttr FKExtraHip_R.translateY 0;setAttr FKExtraHip_R.translateZ 0;setAttr FKExtraHip_R.rotateX -0;setAttr FKExtraHip_R.rotateY 0;setAttr FKExtraHip_R.rotateZ 0;setAttr FKExtraHip_R.scaleX 1;setAttr FKExtraHip_R.scaleY 1;setAttr FKExtraHip_R.scaleZ 1;setAttr FKElbowShield_R.translateX -8.881784197e-16;setAttr FKElbowShield_R.translateY 0;setAttr FKElbowShield_R.translateZ 0;setAttr FKElbowShield_R.rotateX -0;setAttr FKElbowShield_R.rotateY 0;setAttr FKElbowShield_R.rotateZ 0;setAttr FKElbowShield_R.scaleX 1;setAttr FKElbowShield_R.scaleY 1;setAttr FKElbowShield_R.scaleZ 1;setAttr FKExtraElbowShield_R.translateX -2.664535259e-15;setAttr FKExtraElbowShield_R.translateY -8.881784197e-16;setAttr FKExtraElbowShield_R.translateZ 0;setAttr FKExtraElbowShield_R.rotateX -0;setAttr FKExtraElbowShield_R.rotateY 0;setAttr FKExtraElbowShield_R.rotateZ 0;setAttr FKExtraElbowShield_R.scaleX 1;setAttr FKExtraElbowShield_R.scaleY 1;setAttr FKExtraElbowShield_R.scaleZ 1;setAttr FKKnee_R.translateX 8.881784197e-16;setAttr FKKnee_R.translateY 0;setAttr FKKnee_R.translateZ 2.220446049e-16;setAttr FKKnee_R.rotateX -0;setAttr FKKnee_R.rotateY 0;setAttr FKKnee_R.rotateZ 0;setAttr FKKnee_R.scaleX 1;setAttr FKKnee_R.scaleY 1;setAttr FKKnee_R.scaleZ 1;setAttr FKExtraKnee_R.translateX 0;setAttr FKExtraKnee_R.translateY -8.881784197e-16;setAttr FKExtraKnee_R.translateZ 2.220446049e-16;setAttr FKExtraKnee_R.rotateX -0;setAttr FKExtraKnee_R.rotateY 0;setAttr FKExtraKnee_R.rotateZ 0;setAttr FKExtraKnee_R.scaleX 1;setAttr FKExtraKnee_R.scaleY 1;setAttr FKExtraKnee_R.scaleZ 1;setAttr FKAnkle_R.translateX 0;setAttr FKAnkle_R.translateY 0;setAttr FKAnkle_R.translateZ -2.220446049e-16;setAttr FKAnkle_R.rotateX -0;setAttr FKAnkle_R.rotateY -0;setAttr FKAnkle_R.rotateZ 0;setAttr FKAnkle_R.scaleX 1;setAttr FKAnkle_R.scaleY 1;setAttr FKAnkle_R.scaleZ 1;setAttr FKExtraHead_M.translateX 0;setAttr FKExtraHead_M.translateY 0;setAttr FKExtraHead_M.translateZ 0;setAttr FKExtraHead_M.rotateX -0;setAttr FKExtraHead_M.rotateY 0;setAttr FKExtraHead_M.rotateZ -0;setAttr FKExtraHead_M.scaleX 1;setAttr FKExtraHead_M.scaleY 1;setAttr FKExtraHead_M.scaleZ 1;setAttr FKJaw_M.translateX 0;setAttr FKJaw_M.translateY 0;setAttr FKJaw_M.translateZ 0;setAttr FKJaw_M.rotateX 0;setAttr FKJaw_M.rotateY -0;setAttr FKJaw_M.rotateZ 0;setAttr FKJaw_M.scaleX 1;setAttr FKJaw_M.scaleY 1;setAttr FKJaw_M.scaleZ 1;setAttr FKExtraJaw_M.translateX 0;setAttr FKExtraJaw_M.translateY 0;setAttr FKExtraJaw_M.translateZ 0;setAttr FKExtraJaw_M.rotateX 0;setAttr FKExtraJaw_M.rotateY -0;setAttr FKExtraJaw_M.rotateZ 0;setAttr FKExtraJaw_M.scaleX 1;setAttr FKExtraJaw_M.scaleY 1;setAttr FKExtraJaw_M.scaleZ 1;setAttr FKExtraHipTwist_R.translateX 8.881784197e-16;setAttr FKExtraHipTwist_R.translateY 0;setAttr FKExtraHipTwist_R.translateZ 4.440892099e-16;setAttr FKExtraHipTwist_R.rotateX -0;setAttr FKExtraHipTwist_R.rotateY 0;setAttr FKExtraHipTwist_R.rotateZ 0;setAttr FKExtraHipTwist_R.scaleX 1;setAttr FKExtraHipTwist_R.scaleY 1;setAttr FKExtraHipTwist_R.scaleZ 1;setAttr FKHipTwist_R.translateX 0;setAttr FKHipTwist_R.translateY 0;setAttr FKHipTwist_R.translateZ 4.440892099e-16;setAttr FKHipTwist_R.rotateX -0;setAttr FKHipTwist_R.rotateY 0;setAttr FKHipTwist_R.rotateZ 0;setAttr FKHipTwist_R.scaleX 1;setAttr FKHipTwist_R.scaleY 1;setAttr FKHipTwist_R.scaleZ 1;setAttr FKShoulder_R.translateX 0;setAttr FKShoulder_R.translateY 8.881784197e-16;setAttr FKShoulder_R.translateZ 0;setAttr FKShoulder_R.rotateX -0;setAttr FKShoulder_R.rotateY 0;setAttr FKShoulder_R.rotateZ -0;setAttr FKShoulder_R.scaleX 1;setAttr FKShoulder_R.scaleY 1;setAttr FKShoulder_R.scaleZ 1;setAttr FKHead_M.translateX 0;setAttr FKHead_M.translateY 0;setAttr FKHead_M.translateZ 0;setAttr FKHead_M.rotateX -0;setAttr FKHead_M.rotateY 0;setAttr FKHead_M.rotateZ -0;setAttr FKHead_M.scaleX 1;setAttr FKHead_M.scaleY 1;setAttr FKHead_M.scaleZ 1;setAttr FKHead_M.Global 10;setAttr FKExtraFingers_R.translateX 4.440892099e-16;setAttr FKExtraFingers_R.translateY -4.440892099e-16;setAttr FKExtraFingers_R.translateZ 0;setAttr FKExtraFingers_R.rotateX -0;setAttr FKExtraFingers_R.rotateY 0;setAttr FKExtraFingers_R.rotateZ -0;setAttr FKExtraFingers_R.scaleX 1;setAttr FKExtraFingers_R.scaleY 1;setAttr FKExtraFingers_R.scaleZ 1;setAttr FKFingers_R.translateX -4.440892099e-16;setAttr FKFingers_R.translateY -3.330669074e-16;setAttr FKFingers_R.translateZ 0;setAttr FKFingers_R.rotateX -0;setAttr FKFingers_R.rotateY 0;setAttr FKFingers_R.rotateZ -0;setAttr FKFingers_R.scaleX 1;setAttr FKFingers_R.scaleY 1;setAttr FKFingers_R.scaleZ 1;setAttr FKExtraElbow_R.translateX 0;setAttr FKExtraElbow_R.translateY 2.775557562e-17;setAttr FKExtraElbow_R.translateZ 0;setAttr FKExtraElbow_R.rotateX -0;setAttr FKExtraElbow_R.rotateY 0;setAttr FKExtraElbow_R.rotateZ -0;setAttr FKExtraElbow_R.scaleX 1;setAttr FKExtraElbow_R.scaleY 1;setAttr FKExtraElbow_R.scaleZ 1;setAttr FKElbow_R.translateX 0;setAttr FKElbow_R.translateY 1.387778781e-17;setAttr FKElbow_R.translateZ -4.440892099e-16;setAttr FKElbow_R.rotateX -0;setAttr FKElbow_R.rotateY 0;setAttr FKElbow_R.rotateZ -0;setAttr FKElbow_R.scaleX 1;setAttr FKElbow_R.scaleY 1;setAttr FKElbow_R.scaleZ 1;setAttr FKExtraShoulder_R.translateX -4.440892099e-16;setAttr FKExtraShoulder_R.translateY 0;setAttr FKExtraShoulder_R.translateZ 4.440892099e-16;setAttr FKExtraShoulder_R.rotateX -0;setAttr FKExtraShoulder_R.rotateY 0;setAttr FKExtraShoulder_R.rotateZ -0;setAttr FKExtraShoulder_R.scaleX 1;setAttr FKExtraShoulder_R.scaleY 1;setAttr FKExtraShoulder_R.scaleZ 1;setAttr FKKnee_L.translateX 8.881784197e-16;setAttr FKKnee_L.translateY 4.440892099e-16;setAttr FKKnee_L.translateZ 2.220446049e-16;setAttr FKKnee_L.rotateX -0;setAttr FKKnee_L.rotateY 0;setAttr FKKnee_L.rotateZ 0;setAttr FKKnee_L.scaleX 1;setAttr FKKnee_L.scaleY 1;setAttr FKKnee_L.scaleZ 1;setAttr FKExtraMiddleToe1_L.translateX 0;setAttr FKExtraMiddleToe1_L.translateY 4.440892099e-16;setAttr FKExtraMiddleToe1_L.translateZ -1.110223025e-16;setAttr FKExtraMiddleToe1_L.rotateX 3.180554681e-14;setAttr FKExtraMiddleToe1_L.rotateY -1.743093054e-14;setAttr FKExtraMiddleToe1_L.rotateZ -1.176556751e-14;setAttr FKExtraMiddleToe1_L.scaleX 1;setAttr FKExtraMiddleToe1_L.scaleY 1;setAttr FKExtraMiddleToe1_L.scaleZ 1;setAttr FKMiddleToe1_L.translateX 0;setAttr FKMiddleToe1_L.translateY -4.440892099e-16;setAttr FKMiddleToe1_L.translateZ 1.110223025e-16;setAttr FKMiddleToe1_L.rotateX -0;setAttr FKMiddleToe1_L.rotateY 0;setAttr FKMiddleToe1_L.rotateZ -0;setAttr FKMiddleToe1_L.scaleX 1;setAttr FKMiddleToe1_L.scaleY 1;setAttr FKMiddleToe1_L.scaleZ 1;setAttr FKExtraAnkle_L.translateX 0;setAttr FKExtraAnkle_L.translateY 0;setAttr FKExtraAnkle_L.translateZ 0;setAttr FKExtraAnkle_L.rotateX -0;setAttr FKExtraAnkle_L.rotateY 7.951386704e-16;setAttr FKExtraAnkle_L.rotateZ 0;setAttr FKExtraAnkle_L.scaleX 1;setAttr FKExtraAnkle_L.scaleY 1;setAttr FKExtraAnkle_L.scaleZ 1;setAttr FKAnkle_L.translateX -8.881784197e-16;setAttr FKAnkle_L.translateY 0;setAttr FKAnkle_L.translateZ 4.440892099e-16;setAttr FKAnkle_L.rotateX -0;setAttr FKAnkle_L.rotateY -0;setAttr FKAnkle_L.rotateZ 0;setAttr FKAnkle_L.scaleX 1;setAttr FKAnkle_L.scaleY 1;setAttr FKAnkle_L.scaleZ 1;setAttr FKExtraKnee_L.translateX 0;setAttr FKExtraKnee_L.translateY 0;setAttr FKExtraKnee_L.translateZ 0;setAttr FKExtraKnee_L.rotateX -0;setAttr FKExtraKnee_L.rotateY 1.987846676e-16;setAttr FKExtraKnee_L.rotateZ -0;setAttr FKExtraKnee_L.scaleX 1;setAttr FKExtraKnee_L.scaleY 1;setAttr FKExtraKnee_L.scaleZ 1;setAttr FKExtraElbowShield_L.translateX 1.776356839e-15;setAttr FKExtraElbowShield_L.translateY -4.440892099e-16;setAttr FKExtraElbowShield_L.translateZ 8.881784197e-16;setAttr FKExtraElbowShield_L.rotateX -3.182884189e-15;setAttr FKExtraElbowShield_L.rotateY 3.6029721e-16;setAttr FKExtraElbowShield_L.rotateZ -1.987846676e-16;setAttr FKExtraElbowShield_L.scaleX 1;setAttr FKExtraElbowShield_L.scaleY 1;setAttr FKExtraElbowShield_L.scaleZ 1;setAttr FKElbowShield_L.translateX 1.776356839e-15;setAttr FKElbowShield_L.translateY 1.33226763e-15;setAttr FKElbowShield_L.translateZ 0;setAttr FKElbowShield_L.rotateX -0;setAttr FKElbowShield_L.rotateY 0;setAttr FKElbowShield_L.rotateZ 0;setAttr FKElbowShield_L.scaleX 1;setAttr FKElbowShield_L.scaleY 1;setAttr FKElbowShield_L.scaleZ 1;setAttr FKExtraHip_L.translateX 0;setAttr FKExtraHip_L.translateY 1.776356839e-15;setAttr FKExtraHip_L.translateZ 0;setAttr FKExtraHip_L.rotateX -4.808104147e-15;setAttr FKExtraHip_L.rotateY -1.987846676e-16;setAttr FKExtraHip_L.rotateZ -6.957463366e-16;setAttr FKExtraHip_L.scaleX 1;setAttr FKExtraHip_L.scaleY 1;setAttr FKExtraHip_L.scaleZ 1;setAttr FKHip_L.translateX 0;setAttr FKHip_L.translateY 1.776356839e-15;setAttr FKHip_L.translateZ 4.440892099e-16;setAttr FKHip_L.rotateX -0;setAttr FKHip_L.rotateY 0;setAttr FKHip_L.rotateZ 0;setAttr FKHip_L.scaleX 1;setAttr FKHip_L.scaleY 1;setAttr FKHip_L.scaleZ 1;setAttr FKExtraHipTwist_L.translateX 8.881784197e-16;setAttr FKExtraHipTwist_L.translateY 2.220446049e-16;setAttr FKExtraHipTwist_L.translateZ 0;setAttr FKExtraHipTwist_L.rotateX 1.049583045e-13;setAttr FKExtraHipTwist_L.rotateY 1.529418532e-29;setAttr FKExtraHipTwist_L.rotateZ 1.669791208e-14;setAttr FKExtraHipTwist_L.scaleX 1;setAttr FKExtraHipTwist_L.scaleY 1;setAttr FKExtraHipTwist_L.scaleZ 1;setAttr FKHipTwist_L.translateX -8.881784197e-16;setAttr FKHipTwist_L.translateY 0;setAttr FKHipTwist_L.translateZ 0;setAttr FKHipTwist_L.rotateX -0;setAttr FKHipTwist_L.rotateY 0;setAttr FKHipTwist_L.rotateZ 0;setAttr FKHipTwist_L.scaleX 1;setAttr FKHipTwist_L.scaleY 1;setAttr FKHipTwist_L.scaleZ 1;setAttr FKExtraFingers_L.translateX 0;setAttr FKExtraFingers_L.translateY 3.330669074e-16;setAttr FKExtraFingers_L.translateZ 0;setAttr FKExtraFingers_L.rotateX -1.272221873e-14;setAttr FKExtraFingers_L.rotateY 6.361109363e-15;setAttr FKExtraFingers_L.rotateZ -7.951386704e-16;setAttr FKExtraFingers_L.scaleX 1;setAttr FKExtraFingers_L.scaleY 1;setAttr FKExtraFingers_L.scaleZ 1;setAttr FKFingers_L.translateX 0;setAttr FKFingers_L.translateY 0;setAttr FKFingers_L.translateZ 0;setAttr FKFingers_L.rotateX -0;setAttr FKFingers_L.rotateY 0;setAttr FKFingers_L.rotateZ -0;setAttr FKFingers_L.scaleX 1;setAttr FKFingers_L.scaleY 1;setAttr FKFingers_L.scaleZ 1;setAttr FKExtraElbow_L.translateX -4.440892099e-16;setAttr FKExtraElbow_L.translateY -1.665334537e-16;setAttr FKExtraElbow_L.translateZ -8.881784197e-16;setAttr FKExtraElbow_L.rotateX -6.858071032e-15;setAttr FKExtraElbow_L.rotateY -7.951386704e-16;setAttr FKExtraElbow_L.rotateZ -4.770832022e-15;setAttr FKExtraElbow_L.scaleX 1;setAttr FKExtraElbow_L.scaleY 1;setAttr FKExtraElbow_L.scaleZ 1;setAttr FKElbow_L.translateX -4.440892099e-16;setAttr FKElbow_L.translateY 2.775557562e-17;setAttr FKElbow_L.translateZ 0;setAttr FKElbow_L.rotateX -0;setAttr FKElbow_L.rotateY 0;setAttr FKElbow_L.rotateZ -0;setAttr FKElbow_L.scaleX 1;setAttr FKElbow_L.scaleY 1;setAttr FKElbow_L.scaleZ 1;setAttr FKExtraShoulder_L.translateX 4.440892099e-16;setAttr FKExtraShoulder_L.translateY 0;setAttr FKExtraShoulder_L.translateZ -4.440892099e-16;setAttr FKExtraShoulder_L.rotateX 6.48534978e-15;setAttr FKExtraShoulder_L.rotateY 7.951386704e-16;setAttr FKExtraShoulder_L.rotateZ 1.292100339e-15;setAttr FKExtraShoulder_L.scaleX 1;setAttr FKExtraShoulder_L.scaleY 1;setAttr FKExtraShoulder_L.scaleZ 1;setAttr FKShoulder_L.translateX 0;setAttr FKShoulder_L.translateY 0;setAttr FKShoulder_L.translateZ 8.881784197e-16;setAttr FKShoulder_L.rotateX -0;setAttr FKShoulder_L.rotateY 0;setAttr FKShoulder_L.rotateZ -0;setAttr FKShoulder_L.scaleX 1;setAttr FKShoulder_L.scaleY 1;setAttr FKShoulder_L.scaleZ 1;setAttr Main.visibility 1;setAttr Main.translateX 0;setAttr Main.translateY 0;setAttr Main.translateZ 0;setAttr Main.rotateX 0;setAttr Main.rotateY 0;setAttr Main.rotateZ 0;setAttr Main.scaleX 1;setAttr Main.scaleY 1;setAttr Main.scaleZ 1;setAttr IKExtraLegBall_L.rotateX 0;setAttr IKExtraLegBall_L.rotateY -0;setAttr IKExtraLegBall_L.rotateZ 0;setAttr IKLegBall_L.rotateX 0;setAttr CenterExtra_M.translateX 0;setAttr CenterExtra_M.translateY 0;setAttr CenterExtra_M.translateZ 0;setAttr CenterExtra_M.rotateX 0;setAttr CenterExtra_M.rotateY -0;setAttr CenterExtra_M.rotateZ 0;setAttr Center_M.translateX 0;setAttr Center_M.translateY 0;setAttr Center_M.translateZ 0;setAttr Center_M.rotateX 0;setAttr Center_M.rotateY -0;setAttr Center_M.rotateZ 0;setAttr Center_M.CenterBtwFeet 0;");
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -s 2 ".sol";
connectAttr "jointLayer.di" "Pelvis.do";
connectAttr "Pelvis.s" "HipTwist.is";
connectAttr "jointLayer.di" "HipTwist.do";
connectAttr "jointLayer.di" "Hip.do";
connectAttr "Hip.s" "Knee.is";
connectAttr "jointLayer.di" "Knee.do";
connectAttr "Knee.s" "Ankle.is";
connectAttr "jointLayer.di" "Ankle.do";
connectAttr "Ankle.s" "MiddleToe1.is";
connectAttr "jointLayer.di" "MiddleToe1.do";
connectAttr "MiddleToe1.s" "MiddleToe2_End.is";
connectAttr "jointLayer.di" "MiddleToe2_End.do";
connectAttr "Ankle.s" "Heel_End.is";
connectAttr "jointLayer.di" "Heel_End.do";
connectAttr "Hip.s" "ElbowShield.is";
connectAttr "jointLayer.di" "ElbowShield.do";
connectAttr "jointLayer.di" "ElbowShield_End.do";
connectAttr "Pelvis.s" "Head.is";
connectAttr "jointLayer.di" "Head.do";
connectAttr "Head.global" "Head.globalConnect";
connectAttr "Head.s" "Head_End.is";
connectAttr "jointLayer.di" "Head_End.do";
connectAttr "Head.s" "Jaw.is";
connectAttr "jointLayer.di" "Jaw.do";
connectAttr "Jaw.s" "Jaw_End.is";
connectAttr "jointLayer.di" "Jaw_End.do";
connectAttr "Pelvis.s" "Shoulder.is";
connectAttr "jointLayer.di" "Shoulder.do";
connectAttr "Shoulder.s" "Elbow.is";
connectAttr "jointLayer.di" "Elbow.do";
connectAttr "Elbow.s" "Fingers.is";
connectAttr "jointLayer.di" "Fingers.do";
connectAttr "jointLayer.di" "Fingers_End.do";
connectAttr "Main.fkVis" "FKSystem.v";
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.ctx" "FKParentConstraintToAnkle_R.tx"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.cty" "FKParentConstraintToAnkle_R.ty"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.ctz" "FKParentConstraintToAnkle_R.tz"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.crx" "FKParentConstraintToAnkle_R.rx"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.cry" "FKParentConstraintToAnkle_R.ry"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.crz" "FKParentConstraintToAnkle_R.rz"
		;
connectAttr "unitConversion6.o" "FKOffsetMiddleToe1_R.rx";
connectAttr "jointLayer.di" "FKOffsetMiddleToe1_R.do";
connectAttr "jointLayer.di" "FKXMiddleToe1_R.do";
connectAttr "FKMiddleToe1_R.s" "FKXMiddleToe2_End_R.is";
connectAttr "jointLayer.di" "FKXMiddleToe2_End_R.do";
connectAttr "FKParentConstraintToAnkle_R.ro" "FKParentConstraintToAnkle_R_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToAnkle_R.pim" "FKParentConstraintToAnkle_R_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToAnkle_R.rp" "FKParentConstraintToAnkle_R_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToAnkle_R.rpt" "FKParentConstraintToAnkle_R_parentConstraint1.crt"
		;
connectAttr "Ankle_R.t" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tt"
		;
connectAttr "Ankle_R.rp" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Ankle_R.rpt" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Ankle_R.r" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tr"
		;
connectAttr "Ankle_R.ro" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Ankle_R.s" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].ts"
		;
connectAttr "Ankle_R.pm" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Ankle_R.jo" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.w0" "FKParentConstraintToAnkle_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.ctx" "FKParentConstraintToHip_R.tx"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.cty" "FKParentConstraintToHip_R.ty"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.ctz" "FKParentConstraintToHip_R.tz"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.crx" "FKParentConstraintToHip_R.rx"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.cry" "FKParentConstraintToHip_R.ry"
		;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.crz" "FKParentConstraintToHip_R.rz"
		;
connectAttr "jointLayer.di" "FKOffsetElbowShield_R.do";
connectAttr "jointLayer.di" "FKXElbowShield_R.do";
connectAttr "FKElbowShield_R.s" "FKXElbowShield_End_R.is";
connectAttr "jointLayer.di" "FKXElbowShield_End_R.do";
connectAttr "FKParentConstraintToHip_R.ro" "FKParentConstraintToHip_R_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToHip_R.pim" "FKParentConstraintToHip_R_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToHip_R.rp" "FKParentConstraintToHip_R_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToHip_R.rpt" "FKParentConstraintToHip_R_parentConstraint1.crt"
		;
connectAttr "Hip_R.t" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tt";
connectAttr "Hip_R.rp" "FKParentConstraintToHip_R_parentConstraint1.tg[0].trp";
connectAttr "Hip_R.rpt" "FKParentConstraintToHip_R_parentConstraint1.tg[0].trt";
connectAttr "Hip_R.r" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tr";
connectAttr "Hip_R.ro" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tro";
connectAttr "Hip_R.s" "FKParentConstraintToHip_R_parentConstraint1.tg[0].ts";
connectAttr "Hip_R.pm" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tpm";
connectAttr "Hip_R.jo" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tjo";
connectAttr "FKParentConstraintToHip_R_parentConstraint1.w0" "FKParentConstraintToHip_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.ctx" "FKParentConstraintToAnkle_L.tx"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.cty" "FKParentConstraintToAnkle_L.ty"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.ctz" "FKParentConstraintToAnkle_L.tz"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.crx" "FKParentConstraintToAnkle_L.rx"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.cry" "FKParentConstraintToAnkle_L.ry"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.crz" "FKParentConstraintToAnkle_L.rz"
		;
connectAttr "unitConversion12.o" "FKOffsetMiddleToe1_L.rx";
connectAttr "jointLayer.di" "FKOffsetMiddleToe1_L.do";
connectAttr "jointLayer.di" "FKXMiddleToe1_L.do";
connectAttr "FKMiddleToe1_L.s" "FKXMiddleToe2_End_L.is";
connectAttr "jointLayer.di" "FKXMiddleToe2_End_L.do";
connectAttr "FKParentConstraintToAnkle_L.ro" "FKParentConstraintToAnkle_L_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToAnkle_L.pim" "FKParentConstraintToAnkle_L_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToAnkle_L.rp" "FKParentConstraintToAnkle_L_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToAnkle_L.rpt" "FKParentConstraintToAnkle_L_parentConstraint1.crt"
		;
connectAttr "Ankle_L.t" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tt"
		;
connectAttr "Ankle_L.rp" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Ankle_L.rpt" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Ankle_L.r" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tr"
		;
connectAttr "Ankle_L.ro" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Ankle_L.s" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].ts"
		;
connectAttr "Ankle_L.pm" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Ankle_L.jo" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.w0" "FKParentConstraintToAnkle_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.ctx" "FKParentConstraintToHip_L.tx"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.cty" "FKParentConstraintToHip_L.ty"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.ctz" "FKParentConstraintToHip_L.tz"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.crx" "FKParentConstraintToHip_L.rx"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.cry" "FKParentConstraintToHip_L.ry"
		;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.crz" "FKParentConstraintToHip_L.rz"
		;
connectAttr "jointLayer.di" "FKOffsetElbowShield_L.do";
connectAttr "jointLayer.di" "FKXElbowShield_L.do";
connectAttr "FKElbowShield_L.s" "FKXElbowShield_End_L.is";
connectAttr "jointLayer.di" "FKXElbowShield_End_L.do";
connectAttr "FKParentConstraintToHip_L.ro" "FKParentConstraintToHip_L_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToHip_L.pim" "FKParentConstraintToHip_L_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToHip_L.rp" "FKParentConstraintToHip_L_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToHip_L.rpt" "FKParentConstraintToHip_L_parentConstraint1.crt"
		;
connectAttr "Hip_L.t" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tt";
connectAttr "Hip_L.rp" "FKParentConstraintToHip_L_parentConstraint1.tg[0].trp";
connectAttr "Hip_L.rpt" "FKParentConstraintToHip_L_parentConstraint1.tg[0].trt";
connectAttr "Hip_L.r" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tr";
connectAttr "Hip_L.ro" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tro";
connectAttr "Hip_L.s" "FKParentConstraintToHip_L_parentConstraint1.tg[0].ts";
connectAttr "Hip_L.pm" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tpm";
connectAttr "Hip_L.jo" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tjo";
connectAttr "FKParentConstraintToHip_L_parentConstraint1.w0" "FKParentConstraintToHip_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctx" "PelvisCenterBtwLegsBlended_M.tx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cty" "PelvisCenterBtwLegsBlended_M.ty"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctz" "PelvisCenterBtwLegsBlended_M.tz"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crx" "PelvisCenterBtwLegsBlended_M.rx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cry" "PelvisCenterBtwLegsBlended_M.ry"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crz" "PelvisCenterBtwLegsBlended_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetPelvis_M.do";
connectAttr "jointLayer.di" "FKXPelvis_M.do";
connectAttr "FKXPelvis_M.s" "FKOffsetHipTwist_R.is";
connectAttr "jointLayer.di" "FKOffsetHipTwist_R.do";
connectAttr "FKPelvis_M.s" "FKXHipTwist_R.is";
connectAttr "jointLayer.di" "FKXHipTwist_R.do";
connectAttr "FKXHipTwist_R.s" "FKOffsetHip_R.is";
connectAttr "FKIKBlendLegCondition_R.ocg" "FKOffsetHip_R.v" -l on;
connectAttr "jointLayer.di" "FKOffsetHip_R.do";
connectAttr "jointLayer.di" "FKXHip_R.do";
connectAttr "FKXHip_R.s" "FKOffsetKnee_R.is";
connectAttr "jointLayer.di" "FKOffsetKnee_R.do";
connectAttr "FKHip_R.s" "FKXKnee_R.is";
connectAttr "jointLayer.di" "FKXKnee_R.do";
connectAttr "FKXKnee_R.s" "FKOffsetAnkle_R.is";
connectAttr "jointLayer.di" "FKOffsetAnkle_R.do";
connectAttr "FKKnee_R.s" "FKXAnkle_R.is";
connectAttr "jointLayer.di" "FKXAnkle_R.do";
connectAttr "FKAnkle_R.s" "FKXHeel_End_R.is";
connectAttr "jointLayer.di" "FKXHeel_End_R.do";
connectAttr "FKXPelvis_M.s" "FKOffsetHead_M.is";
connectAttr "jointLayer.di" "FKOffsetHead_M.do";
connectAttr "FKGlobalHead_M_orientConstraint1.crx" "FKGlobalHead_M.rx";
connectAttr "FKGlobalHead_M_orientConstraint1.cry" "FKGlobalHead_M.ry";
connectAttr "FKGlobalHead_M_orientConstraint1.crz" "FKGlobalHead_M.rz";
connectAttr "FKPelvis_M.s" "FKXHead_M.is";
connectAttr "jointLayer.di" "FKXHead_M.do";
connectAttr "FKHead_M.s" "FKXHead_End_M.is";
connectAttr "jointLayer.di" "FKXHead_End_M.do";
connectAttr "FKXHead_M.s" "FKOffsetJaw_M.is";
connectAttr "jointLayer.di" "FKOffsetJaw_M.do";
connectAttr "FKHead_M.s" "FKXJaw_M.is";
connectAttr "jointLayer.di" "FKXJaw_M.do";
connectAttr "FKJaw_M.s" "FKXJaw_End_M.is";
connectAttr "jointLayer.di" "FKXJaw_End_M.do";
connectAttr "FKGlobalHead_M.ro" "FKGlobalHead_M_orientConstraint1.cro";
connectAttr "FKGlobalHead_M.pim" "FKGlobalHead_M_orientConstraint1.cpim";
connectAttr "GlobalHead_M.r" "FKGlobalHead_M_orientConstraint1.tg[0].tr";
connectAttr "GlobalHead_M.ro" "FKGlobalHead_M_orientConstraint1.tg[0].tro";
connectAttr "GlobalHead_M.pm" "FKGlobalHead_M_orientConstraint1.tg[0].tpm";
connectAttr "FKGlobalHead_M_orientConstraint1.w0" "FKGlobalHead_M_orientConstraint1.tg[0].tw"
		;
connectAttr "FKGlobalStaticHead_M.r" "FKGlobalHead_M_orientConstraint1.tg[1].tr"
		;
connectAttr "FKGlobalStaticHead_M.ro" "FKGlobalHead_M_orientConstraint1.tg[1].tro"
		;
connectAttr "FKGlobalStaticHead_M.pm" "FKGlobalHead_M_orientConstraint1.tg[1].tpm"
		;
connectAttr "FKGlobalHead_M_orientConstraint1.w1" "FKGlobalHead_M_orientConstraint1.tg[1].tw"
		;
connectAttr "GlobalHead_unitConversion_M.o" "FKGlobalHead_M_orientConstraint1.w0"
		;
connectAttr "GlobalHead_reverse_M.ox" "FKGlobalHead_M_orientConstraint1.w1";
connectAttr "FKXPelvis_M.s" "FKOffsetShoulder_R.is";
connectAttr "jointLayer.di" "FKOffsetShoulder_R.do";
connectAttr "FKPelvis_M.s" "FKXShoulder_R.is";
connectAttr "jointLayer.di" "FKXShoulder_R.do";
connectAttr "FKXShoulder_R.s" "FKOffsetElbow_R.is";
connectAttr "jointLayer.di" "FKOffsetElbow_R.do";
connectAttr "FKShoulder_R.s" "FKXElbow_R.is";
connectAttr "jointLayer.di" "FKXElbow_R.do";
connectAttr "FKXElbow_R.s" "FKOffsetFingers_R.is";
connectAttr "jointLayer.di" "FKOffsetFingers_R.do";
connectAttr "FKElbow_R.s" "FKXFingers_R.is";
connectAttr "jointLayer.di" "FKXFingers_R.do";
connectAttr "FKFingers_R.s" "FKXFingers_End_R.is";
connectAttr "jointLayer.di" "FKXFingers_End_R.do";
connectAttr "FKXPelvis_M.s" "FKOffsetHipTwist_L.is";
connectAttr "jointLayer.di" "FKOffsetHipTwist_L.do";
connectAttr "FKPelvis_M.s" "FKXHipTwist_L.is";
connectAttr "jointLayer.di" "FKXHipTwist_L.do";
connectAttr "FKXHipTwist_L.s" "FKOffsetHip_L.is";
connectAttr "FKIKBlendLegCondition_L.ocg" "FKOffsetHip_L.v" -l on;
connectAttr "jointLayer.di" "FKOffsetHip_L.do";
connectAttr "jointLayer.di" "FKXHip_L.do";
connectAttr "FKXHip_L.s" "FKOffsetKnee_L.is";
connectAttr "jointLayer.di" "FKOffsetKnee_L.do";
connectAttr "FKHip_L.s" "FKXKnee_L.is";
connectAttr "jointLayer.di" "FKXKnee_L.do";
connectAttr "FKXKnee_L.s" "FKOffsetAnkle_L.is";
connectAttr "jointLayer.di" "FKOffsetAnkle_L.do";
connectAttr "FKKnee_L.s" "FKXAnkle_L.is";
connectAttr "jointLayer.di" "FKXAnkle_L.do";
connectAttr "FKAnkle_L.s" "FKXHeel_End_L.is";
connectAttr "jointLayer.di" "FKXHeel_End_L.do";
connectAttr "FKXPelvis_M.s" "FKOffsetShoulder_L.is";
connectAttr "jointLayer.di" "FKOffsetShoulder_L.do";
connectAttr "FKPelvis_M.s" "FKXShoulder_L.is";
connectAttr "jointLayer.di" "FKXShoulder_L.do";
connectAttr "FKXShoulder_L.s" "FKOffsetElbow_L.is";
connectAttr "jointLayer.di" "FKOffsetElbow_L.do";
connectAttr "FKShoulder_L.s" "FKXElbow_L.is";
connectAttr "jointLayer.di" "FKXElbow_L.do";
connectAttr "FKXElbow_L.s" "FKOffsetFingers_L.is";
connectAttr "jointLayer.di" "FKOffsetFingers_L.do";
connectAttr "FKElbow_L.s" "FKXFingers_L.is";
connectAttr "jointLayer.di" "FKXFingers_L.do";
connectAttr "FKFingers_L.s" "FKXFingers_End_L.is";
connectAttr "jointLayer.di" "FKXFingers_End_L.do";
connectAttr "PelvisCenterBtwLegsBlended_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.pim" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crt"
		;
connectAttr "PelvisCenter_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tt"
		;
connectAttr "PelvisCenter_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trp"
		;
connectAttr "PelvisCenter_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trt"
		;
connectAttr "PelvisCenter_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tr"
		;
connectAttr "PelvisCenter_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tro"
		;
connectAttr "PelvisCenter_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].ts"
		;
connectAttr "PelvisCenter_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trp"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tr"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tro"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].ts"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tw"
		;
connectAttr "PelvisCenterBtwFeetReverse_M.ox" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0"
		;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1"
		;
connectAttr "Main.ikVis" "IKSystem.v";
connectAttr "IKParentConstraintHip_R_parentConstraint1.ctx" "IKParentConstraintHip_R.tx"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.cty" "IKParentConstraintHip_R.ty"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.ctz" "IKParentConstraintHip_R.tz"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.crx" "IKParentConstraintHip_R.rx"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.cry" "IKParentConstraintHip_R.ry"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.crz" "IKParentConstraintHip_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintHip_R.v";
connectAttr "jointLayer.di" "IKXHip_R.do";
connectAttr "IKXHip_R.s" "IKXKnee_R.is";
connectAttr "jointLayer.di" "IKXKnee_R.do";
connectAttr "IKXKnee_R.s" "IKXAnkle_R.is";
connectAttr "IKXAnkle_R_orientConstraint1.crx" "IKXAnkle_R.rx";
connectAttr "IKXAnkle_R_orientConstraint1.cry" "IKXAnkle_R.ry";
connectAttr "IKXAnkle_R_orientConstraint1.crz" "IKXAnkle_R.rz";
connectAttr "jointLayer.di" "IKXAnkle_R.do";
connectAttr "IKXAnkle_R.s" "IKXMiddleToe1_R.is";
connectAttr "jointLayer.di" "IKXMiddleToe1_R.do";
connectAttr "IKXMiddleToe1_R.s" "IKXMiddleToe2_End_R.is";
connectAttr "jointLayer.di" "IKXMiddleToe2_End_R.do";
connectAttr "IKXMiddleToe2_End_R.tx" "effector2.tx";
connectAttr "IKXMiddleToe2_End_R.ty" "effector2.ty";
connectAttr "IKXMiddleToe2_End_R.tz" "effector2.tz";
connectAttr "IKXAnkle_R.ro" "IKXAnkle_R_orientConstraint1.cro";
connectAttr "IKXAnkle_R.pim" "IKXAnkle_R_orientConstraint1.cpim";
connectAttr "IKXAnkle_R.jo" "IKXAnkle_R_orientConstraint1.cjo";
connectAttr "IKLeg_R.r" "IKXAnkle_R_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "IKXAnkle_R_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "IKXAnkle_R_orientConstraint1.tg[0].tpm";
connectAttr "IKXAnkle_R_orientConstraint1.w0" "IKXAnkle_R_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXMiddleToe1_R.tx" "effector3.tx";
connectAttr "IKXMiddleToe1_R.ty" "effector3.ty";
connectAttr "IKXMiddleToe1_R.tz" "effector3.tz";
connectAttr "IKXAnkle_R.tx" "effector1.tx";
connectAttr "IKXAnkle_R.ty" "effector1.ty";
connectAttr "IKXAnkle_R.tz" "effector1.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_R.v";
connectAttr "PoleAnnotateTargetLeg_RShape.wm" "PoleAnnotationLeg_RShape.dom" -na
		;
connectAttr "IKParentConstraintHip_R.ro" "IKParentConstraintHip_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintHip_R.pim" "IKParentConstraintHip_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintHip_R.rp" "IKParentConstraintHip_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintHip_R.rpt" "IKParentConstraintHip_R_parentConstraint1.crt"
		;
connectAttr "HipTwist_R.t" "IKParentConstraintHip_R_parentConstraint1.tg[0].tt";
connectAttr "HipTwist_R.rp" "IKParentConstraintHip_R_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_R.rpt" "IKParentConstraintHip_R_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_R.r" "IKParentConstraintHip_R_parentConstraint1.tg[0].tr";
connectAttr "HipTwist_R.ro" "IKParentConstraintHip_R_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_R.s" "IKParentConstraintHip_R_parentConstraint1.tg[0].ts";
connectAttr "HipTwist_R.pm" "IKParentConstraintHip_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_R.jo" "IKParentConstraintHip_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintHip_R_parentConstraint1.w0" "IKParentConstraintHip_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.ctx" "IKParentConstraintHip_L.tx"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.cty" "IKParentConstraintHip_L.ty"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.ctz" "IKParentConstraintHip_L.tz"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.crx" "IKParentConstraintHip_L.rx"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.cry" "IKParentConstraintHip_L.ry"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.crz" "IKParentConstraintHip_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintHip_L.v";
connectAttr "jointLayer.di" "IKXHip_L.do";
connectAttr "IKXHip_L.s" "IKXKnee_L.is";
connectAttr "jointLayer.di" "IKXKnee_L.do";
connectAttr "IKXKnee_L.s" "IKXAnkle_L.is";
connectAttr "IKXAnkle_L_orientConstraint1.crx" "IKXAnkle_L.rx";
connectAttr "IKXAnkle_L_orientConstraint1.cry" "IKXAnkle_L.ry";
connectAttr "IKXAnkle_L_orientConstraint1.crz" "IKXAnkle_L.rz";
connectAttr "jointLayer.di" "IKXAnkle_L.do";
connectAttr "IKXAnkle_L.s" "IKXMiddleToe1_L.is";
connectAttr "jointLayer.di" "IKXMiddleToe1_L.do";
connectAttr "IKXMiddleToe1_L.s" "IKXMiddleToe2_End_L.is";
connectAttr "jointLayer.di" "IKXMiddleToe2_End_L.do";
connectAttr "IKXMiddleToe2_End_L.tx" "effector5.tx";
connectAttr "IKXMiddleToe2_End_L.ty" "effector5.ty";
connectAttr "IKXMiddleToe2_End_L.tz" "effector5.tz";
connectAttr "IKXAnkle_L.ro" "IKXAnkle_L_orientConstraint1.cro";
connectAttr "IKXAnkle_L.pim" "IKXAnkle_L_orientConstraint1.cpim";
connectAttr "IKXAnkle_L.jo" "IKXAnkle_L_orientConstraint1.cjo";
connectAttr "IKLeg_L.r" "IKXAnkle_L_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_L.ro" "IKXAnkle_L_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_L.pm" "IKXAnkle_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXAnkle_L_orientConstraint1.w0" "IKXAnkle_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXMiddleToe1_L.tx" "effector6.tx";
connectAttr "IKXMiddleToe1_L.ty" "effector6.ty";
connectAttr "IKXMiddleToe1_L.tz" "effector6.tz";
connectAttr "IKXAnkle_L.tx" "effector4.tx";
connectAttr "IKXAnkle_L.ty" "effector4.ty";
connectAttr "IKXAnkle_L.tz" "effector4.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_L.v";
connectAttr "PoleAnnotateTargetLeg_LShape.wm" "PoleAnnotationLeg_LShape.dom" -na
		;
connectAttr "IKParentConstraintHip_L.ro" "IKParentConstraintHip_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintHip_L.pim" "IKParentConstraintHip_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintHip_L.rp" "IKParentConstraintHip_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintHip_L.rpt" "IKParentConstraintHip_L_parentConstraint1.crt"
		;
connectAttr "HipTwist_L.t" "IKParentConstraintHip_L_parentConstraint1.tg[0].tt";
connectAttr "HipTwist_L.rp" "IKParentConstraintHip_L_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_L.rpt" "IKParentConstraintHip_L_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_L.r" "IKParentConstraintHip_L_parentConstraint1.tg[0].tr";
connectAttr "HipTwist_L.ro" "IKParentConstraintHip_L_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_L.s" "IKParentConstraintHip_L_parentConstraint1.tg[0].ts";
connectAttr "HipTwist_L.pm" "IKParentConstraintHip_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_L.jo" "IKParentConstraintHip_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintHip_L_parentConstraint1.w0" "IKParentConstraintHip_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintLeg_R.v";
connectAttr "unitConversion2.o" "IKRollLegHeel_R.rx";
connectAttr "unitConversion4.o" "IKRollLegToe_R.rx";
connectAttr "IKLiftToeLegUnitConversion_R.o" "IKLiftToeLegToe_R.rx";
connectAttr "IKXMiddleToe1_R.msg" "IKXLegHandleToe_R.hsj";
connectAttr "effector2.hp" "IKXLegHandleToe_R.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleToe_R.hsv";
connectAttr "unitConversion3.o" "IKRollLegBall_R.rx";
connectAttr "IKXHip_R.msg" "IKXLegHandle_R.hsj";
connectAttr "effector1.hp" "IKXLegHandle_R.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_R.hsv";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctx" "IKXLegHandle_R.pvx";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.cty" "IKXLegHandle_R.pvy";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctz" "IKXLegHandle_R.pvz";
connectAttr "IKXLegHandle_R.pim" "IKXLegHandle_R_poleVectorConstraint1.cpim";
connectAttr "IKXHip_R.pm" "IKXLegHandle_R_poleVectorConstraint1.ps";
connectAttr "IKXHip_R.t" "IKXLegHandle_R_poleVectorConstraint1.crp";
connectAttr "PoleLeg_R.t" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_R.rp" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_R.rpt" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_R.pm" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.w0" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXAnkle_R.msg" "IKXLegHandleBall_R.hsj";
connectAttr "effector3.hp" "IKXLegHandleBall_R.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_R.hsv";
connectAttr "PoleAimLeg_R_aimConstraint1.crx" "PoleAimLeg_R.rx";
connectAttr "PoleAimLeg_R_aimConstraint1.cry" "PoleAimLeg_R.ry";
connectAttr "PoleAimLeg_R_aimConstraint1.crz" "PoleAimLeg_R.rz";
connectAttr "PoleAimLeg_R_pointConstraint1.ctx" "PoleAimLeg_R.tx";
connectAttr "PoleAimLeg_R_pointConstraint1.cty" "PoleAimLeg_R.ty";
connectAttr "PoleAimLeg_R_pointConstraint1.ctz" "PoleAimLeg_R.tz";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_aimConstraint1.cpim";
connectAttr "PoleAimLeg_R.t" "PoleAimLeg_R_aimConstraint1.ct";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_aimConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.crt";
connectAttr "PoleAimLeg_R.ro" "PoleAimLeg_R_aimConstraint1.cro";
connectAttr "IKLeg_R.t" "PoleAimLeg_R_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PoleAimLeg_R_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PoleAimLeg_R_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_aimConstraint1.w0" "PoleAimLeg_R_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_R.wm" "PoleAimLeg_R_aimConstraint1.wum";
connectAttr "unitConversion1.o" "PoleAimLeg_R_aimConstraint1.ox";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_pointConstraint1.cpim";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_pointConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_pointConstraint1.crt";
connectAttr "IKXHip_R.t" "PoleAimLeg_R_pointConstraint1.tg[0].tt";
connectAttr "IKXHip_R.rp" "PoleAimLeg_R_pointConstraint1.tg[0].trp";
connectAttr "IKXHip_R.rpt" "PoleAimLeg_R_pointConstraint1.tg[0].trt";
connectAttr "IKXHip_R.pm" "PoleAimLeg_R_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_pointConstraint1.w0" "PoleAimLeg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctx" "PoleParentConstraintLeg_R.tx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cty" "PoleParentConstraintLeg_R.ty"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctz" "PoleParentConstraintLeg_R.tz"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crx" "PoleParentConstraintLeg_R.rx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cry" "PoleParentConstraintLeg_R.ry"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crz" "PoleParentConstraintLeg_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "PoleParentConstraintLeg_R.v";
connectAttr "PoleParentConstraintLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_R.pim" "PoleParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_RStatic.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_RStatic.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_RStatic.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w0" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_R.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_R.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_R.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_R.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w1" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_RSetRangeFollow.oy" "PoleParentConstraintLeg_R_parentConstraint1.w0"
		;
connectAttr "PoleLeg_RSetRangeFollow.ox" "PoleParentConstraintLeg_R_parentConstraint1.w1"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintLeg_L.v";
connectAttr "unitConversion8.o" "IKRollLegHeel_L.rx";
connectAttr "unitConversion10.o" "IKRollLegToe_L.rx";
connectAttr "IKLiftToeLegUnitConversion_L.o" "IKLiftToeLegToe_L.rx";
connectAttr "IKXMiddleToe1_L.msg" "IKXLegHandleToe_L.hsj";
connectAttr "effector5.hp" "IKXLegHandleToe_L.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleToe_L.hsv";
connectAttr "unitConversion9.o" "IKRollLegBall_L.rx";
connectAttr "IKXHip_L.msg" "IKXLegHandle_L.hsj";
connectAttr "effector4.hp" "IKXLegHandle_L.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_L.hsv";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctx" "IKXLegHandle_L.pvx";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.cty" "IKXLegHandle_L.pvy";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctz" "IKXLegHandle_L.pvz";
connectAttr "IKXLegHandle_L.pim" "IKXLegHandle_L_poleVectorConstraint1.cpim";
connectAttr "IKXHip_L.pm" "IKXLegHandle_L_poleVectorConstraint1.ps";
connectAttr "IKXHip_L.t" "IKXLegHandle_L_poleVectorConstraint1.crp";
connectAttr "PoleLeg_L.t" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_L.rp" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_L.rpt" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_L.pm" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.w0" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "IKXAnkle_L.msg" "IKXLegHandleBall_L.hsj";
connectAttr "effector6.hp" "IKXLegHandleBall_L.hee";
connectAttr "ikSCsolver.msg" "IKXLegHandleBall_L.hsv";
connectAttr "PoleAimLeg_L_aimConstraint1.crx" "PoleAimLeg_L.rx";
connectAttr "PoleAimLeg_L_aimConstraint1.cry" "PoleAimLeg_L.ry";
connectAttr "PoleAimLeg_L_aimConstraint1.crz" "PoleAimLeg_L.rz";
connectAttr "PoleAimLeg_L_pointConstraint1.ctx" "PoleAimLeg_L.tx";
connectAttr "PoleAimLeg_L_pointConstraint1.cty" "PoleAimLeg_L.ty";
connectAttr "PoleAimLeg_L_pointConstraint1.ctz" "PoleAimLeg_L.tz";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_aimConstraint1.cpim";
connectAttr "PoleAimLeg_L.t" "PoleAimLeg_L_aimConstraint1.ct";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_aimConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.crt";
connectAttr "PoleAimLeg_L.ro" "PoleAimLeg_L_aimConstraint1.cro";
connectAttr "IKLeg_L.t" "PoleAimLeg_L_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_L.rp" "PoleAimLeg_L_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_L.pm" "PoleAimLeg_L_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_aimConstraint1.w0" "PoleAimLeg_L_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.wm" "PoleAimLeg_L_aimConstraint1.wum";
connectAttr "unitConversion7.o" "PoleAimLeg_L_aimConstraint1.ox";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_pointConstraint1.cpim";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_pointConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_pointConstraint1.crt";
connectAttr "IKXHip_L.t" "PoleAimLeg_L_pointConstraint1.tg[0].tt";
connectAttr "IKXHip_L.rp" "PoleAimLeg_L_pointConstraint1.tg[0].trp";
connectAttr "IKXHip_L.rpt" "PoleAimLeg_L_pointConstraint1.tg[0].trt";
connectAttr "IKXHip_L.pm" "PoleAimLeg_L_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_pointConstraint1.w0" "PoleAimLeg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctx" "PoleParentConstraintLeg_L.tx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cty" "PoleParentConstraintLeg_L.ty"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctz" "PoleParentConstraintLeg_L.tz"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crx" "PoleParentConstraintLeg_L.rx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cry" "PoleParentConstraintLeg_L.ry"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crz" "PoleParentConstraintLeg_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "PoleParentConstraintLeg_L.v";
connectAttr "PoleParentConstraintLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_L.pim" "PoleParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_LStatic.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_LStatic.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_LStatic.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w0" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_L.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_L.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_L.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_L.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w1" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_LSetRangeFollow.oy" "PoleParentConstraintLeg_L_parentConstraint1.w0"
		;
connectAttr "PoleLeg_LSetRangeFollow.ox" "PoleParentConstraintLeg_L_parentConstraint1.w1"
		;
connectAttr "Main.fkIkVis" "FKIKSystem.v";
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctx" "FKIKParentConstraintLeg_R.tx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cty" "FKIKParentConstraintLeg_R.ty"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctz" "FKIKParentConstraintLeg_R.tz"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crx" "FKIKParentConstraintLeg_R.rx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cry" "FKIKParentConstraintLeg_R.ry"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crz" "FKIKParentConstraintLeg_R.rz"
		;
connectAttr "FKIKParentConstraintLeg_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_R.pim" "FKIKParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "HipTwist_R.t" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "HipTwist_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_R.r" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "HipTwist_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_R.s" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "HipTwist_R.pm" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_R.jo" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.w0" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctx" "FKIKParentConstraintLeg_L.tx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cty" "FKIKParentConstraintLeg_L.ty"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctz" "FKIKParentConstraintLeg_L.tz"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crx" "FKIKParentConstraintLeg_L.rx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cry" "FKIKParentConstraintLeg_L.ry"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crz" "FKIKParentConstraintLeg_L.rz"
		;
connectAttr "FKIKParentConstraintLeg_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_L.pim" "FKIKParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "HipTwist_L.t" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "HipTwist_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "HipTwist_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "HipTwist_L.r" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "HipTwist_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "HipTwist_L.s" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "HipTwist_L.pm" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "HipTwist_L.jo" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.w0" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.cry" "PelvisCenterBtwLegs_M.ry"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctx" "PelvisCenterBtwLegs_M.tx"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctz" "PelvisCenterBtwLegs_M.tz"
		;
connectAttr "PelvisCenterBtwLegs_M.ro" "PelvisCenterBtwLegs_M_orientConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_orientConstraint1.cpim"
		;
connectAttr "IKLeg_R.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w0" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tr";
connectAttr "IKLeg_L.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tro";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w1" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tw"
		;
connectAttr "CenterBtwFeet_M.ox" "PelvisCenterBtwLegs_M_pointConstraint1.nds";
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_pointConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegs_M.rp" "PelvisCenterBtwLegs_M_pointConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegs_M.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.crt"
		;
connectAttr "IKLeg_R.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w0" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tt";
connectAttr "IKLeg_L.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trp";
connectAttr "IKLeg_L.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trt";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w1" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tw"
		;
connectAttr "FKPelvis_M.s" "Pelvis_M.s";
connectAttr "Pelvis_M_pointConstraint1.ctx" "Pelvis_M.tx";
connectAttr "Pelvis_M_pointConstraint1.cty" "Pelvis_M.ty";
connectAttr "Pelvis_M_pointConstraint1.ctz" "Pelvis_M.tz";
connectAttr "Pelvis_M_orientConstraint1.crx" "Pelvis_M.rx";
connectAttr "Pelvis_M_orientConstraint1.cry" "Pelvis_M.ry";
connectAttr "Pelvis_M_orientConstraint1.crz" "Pelvis_M.rz";
connectAttr "jointLayer.di" "Pelvis_M.do";
connectAttr "HipTwist_R_parentConstraint1.ctx" "HipTwist_R.tx";
connectAttr "HipTwist_R_parentConstraint1.cty" "HipTwist_R.ty";
connectAttr "HipTwist_R_parentConstraint1.ctz" "HipTwist_R.tz";
connectAttr "HipTwist_R_parentConstraint1.crx" "HipTwist_R.rx";
connectAttr "HipTwist_R_parentConstraint1.cry" "HipTwist_R.ry";
connectAttr "HipTwist_R_parentConstraint1.crz" "HipTwist_R.rz";
connectAttr "FKHipTwist_R.s" "HipTwist_R.s";
connectAttr "Pelvis_M.s" "HipTwist_R.is";
connectAttr "jointLayer.di" "HipTwist_R.do";
connectAttr "Hip_R_parentConstraint1.ctx" "Hip_R.tx";
connectAttr "Hip_R_parentConstraint1.cty" "Hip_R.ty";
connectAttr "Hip_R_parentConstraint1.ctz" "Hip_R.tz";
connectAttr "Hip_R_parentConstraint1.crx" "Hip_R.rx";
connectAttr "Hip_R_parentConstraint1.cry" "Hip_R.ry";
connectAttr "Hip_R_parentConstraint1.crz" "Hip_R.rz";
connectAttr "ScaleBlendHip_R.op" "Hip_R.s";
connectAttr "jointLayer.di" "Hip_R.do";
connectAttr "ScaleBlendKnee_R.op" "Knee_R.s";
connectAttr "Hip_R.s" "Knee_R.is";
connectAttr "Knee_R_parentConstraint1.ctx" "Knee_R.tx";
connectAttr "Knee_R_parentConstraint1.cty" "Knee_R.ty";
connectAttr "Knee_R_parentConstraint1.ctz" "Knee_R.tz";
connectAttr "Knee_R_parentConstraint1.crx" "Knee_R.rx";
connectAttr "Knee_R_parentConstraint1.cry" "Knee_R.ry";
connectAttr "Knee_R_parentConstraint1.crz" "Knee_R.rz";
connectAttr "jointLayer.di" "Knee_R.do";
connectAttr "Ankle_R_parentConstraint1.ctx" "Ankle_R.tx";
connectAttr "Ankle_R_parentConstraint1.cty" "Ankle_R.ty";
connectAttr "Ankle_R_parentConstraint1.ctz" "Ankle_R.tz";
connectAttr "Ankle_R_parentConstraint1.crx" "Ankle_R.rx";
connectAttr "Ankle_R_parentConstraint1.cry" "Ankle_R.ry";
connectAttr "Ankle_R_parentConstraint1.crz" "Ankle_R.rz";
connectAttr "ScaleBlendAnkle_R.op" "Ankle_R.s";
connectAttr "Knee_R.s" "Ankle_R.is";
connectAttr "jointLayer.di" "Ankle_R.do";
connectAttr "FKMiddleToe1_R.s" "MiddleToe1_R.s";
connectAttr "Ankle_R.s" "MiddleToe1_R.is";
connectAttr "MiddleToe1_R_parentConstraint1.ctx" "MiddleToe1_R.tx";
connectAttr "MiddleToe1_R_parentConstraint1.cty" "MiddleToe1_R.ty";
connectAttr "MiddleToe1_R_parentConstraint1.ctz" "MiddleToe1_R.tz";
connectAttr "MiddleToe1_R_parentConstraint1.crx" "MiddleToe1_R.rx";
connectAttr "MiddleToe1_R_parentConstraint1.cry" "MiddleToe1_R.ry";
connectAttr "MiddleToe1_R_parentConstraint1.crz" "MiddleToe1_R.rz";
connectAttr "jointLayer.di" "MiddleToe1_R.do";
connectAttr "MiddleToe1_R.s" "MiddleToe2_End_R.is";
connectAttr "jointLayer.di" "MiddleToe2_End_R.do";
connectAttr "MiddleToe1_R.ro" "MiddleToe1_R_parentConstraint1.cro";
connectAttr "MiddleToe1_R.pim" "MiddleToe1_R_parentConstraint1.cpim";
connectAttr "MiddleToe1_R.rp" "MiddleToe1_R_parentConstraint1.crp";
connectAttr "MiddleToe1_R.rpt" "MiddleToe1_R_parentConstraint1.crt";
connectAttr "MiddleToe1_R.jo" "MiddleToe1_R_parentConstraint1.cjo";
connectAttr "FKXMiddleToe1_R.t" "MiddleToe1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleToe1_R.rp" "MiddleToe1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXMiddleToe1_R.rpt" "MiddleToe1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXMiddleToe1_R.r" "MiddleToe1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleToe1_R.ro" "MiddleToe1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXMiddleToe1_R.s" "MiddleToe1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleToe1_R.pm" "MiddleToe1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXMiddleToe1_R.jo" "MiddleToe1_R_parentConstraint1.tg[0].tjo";
connectAttr "MiddleToe1_R_parentConstraint1.w0" "MiddleToe1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Ankle_R.ro" "Ankle_R_parentConstraint1.cro";
connectAttr "Ankle_R.pim" "Ankle_R_parentConstraint1.cpim";
connectAttr "Ankle_R.rp" "Ankle_R_parentConstraint1.crp";
connectAttr "Ankle_R.rpt" "Ankle_R_parentConstraint1.crt";
connectAttr "Ankle_R.jo" "Ankle_R_parentConstraint1.cjo";
connectAttr "FKXAnkle_R.t" "Ankle_R_parentConstraint1.tg[0].tt";
connectAttr "FKXAnkle_R.rp" "Ankle_R_parentConstraint1.tg[0].trp";
connectAttr "FKXAnkle_R.rpt" "Ankle_R_parentConstraint1.tg[0].trt";
connectAttr "FKXAnkle_R.r" "Ankle_R_parentConstraint1.tg[0].tr";
connectAttr "FKXAnkle_R.ro" "Ankle_R_parentConstraint1.tg[0].tro";
connectAttr "FKXAnkle_R.s" "Ankle_R_parentConstraint1.tg[0].ts";
connectAttr "FKXAnkle_R.pm" "Ankle_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXAnkle_R.jo" "Ankle_R_parentConstraint1.tg[0].tjo";
connectAttr "Ankle_R_parentConstraint1.w0" "Ankle_R_parentConstraint1.tg[0].tw";
connectAttr "IKXAnkle_R.t" "Ankle_R_parentConstraint1.tg[1].tt";
connectAttr "IKXAnkle_R.rp" "Ankle_R_parentConstraint1.tg[1].trp";
connectAttr "IKXAnkle_R.rpt" "Ankle_R_parentConstraint1.tg[1].trt";
connectAttr "IKXAnkle_R.r" "Ankle_R_parentConstraint1.tg[1].tr";
connectAttr "IKXAnkle_R.ro" "Ankle_R_parentConstraint1.tg[1].tro";
connectAttr "IKXAnkle_R.s" "Ankle_R_parentConstraint1.tg[1].ts";
connectAttr "IKXAnkle_R.pm" "Ankle_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXAnkle_R.jo" "Ankle_R_parentConstraint1.tg[1].tjo";
connectAttr "Ankle_R_parentConstraint1.w1" "Ankle_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Ankle_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Ankle_R_parentConstraint1.w1";
connectAttr "Knee_R.ro" "Knee_R_parentConstraint1.cro";
connectAttr "Knee_R.pim" "Knee_R_parentConstraint1.cpim";
connectAttr "Knee_R.rp" "Knee_R_parentConstraint1.crp";
connectAttr "Knee_R.rpt" "Knee_R_parentConstraint1.crt";
connectAttr "Knee_R.jo" "Knee_R_parentConstraint1.cjo";
connectAttr "FKXKnee_R.t" "Knee_R_parentConstraint1.tg[0].tt";
connectAttr "FKXKnee_R.rp" "Knee_R_parentConstraint1.tg[0].trp";
connectAttr "FKXKnee_R.rpt" "Knee_R_parentConstraint1.tg[0].trt";
connectAttr "FKXKnee_R.r" "Knee_R_parentConstraint1.tg[0].tr";
connectAttr "FKXKnee_R.ro" "Knee_R_parentConstraint1.tg[0].tro";
connectAttr "FKXKnee_R.s" "Knee_R_parentConstraint1.tg[0].ts";
connectAttr "FKXKnee_R.pm" "Knee_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXKnee_R.jo" "Knee_R_parentConstraint1.tg[0].tjo";
connectAttr "Knee_R_parentConstraint1.w0" "Knee_R_parentConstraint1.tg[0].tw";
connectAttr "IKXKnee_R.t" "Knee_R_parentConstraint1.tg[1].tt";
connectAttr "IKXKnee_R.rp" "Knee_R_parentConstraint1.tg[1].trp";
connectAttr "IKXKnee_R.rpt" "Knee_R_parentConstraint1.tg[1].trt";
connectAttr "IKXKnee_R.r" "Knee_R_parentConstraint1.tg[1].tr";
connectAttr "IKXKnee_R.ro" "Knee_R_parentConstraint1.tg[1].tro";
connectAttr "IKXKnee_R.s" "Knee_R_parentConstraint1.tg[1].ts";
connectAttr "IKXKnee_R.pm" "Knee_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXKnee_R.jo" "Knee_R_parentConstraint1.tg[1].tjo";
connectAttr "Knee_R_parentConstraint1.w1" "Knee_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Knee_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Knee_R_parentConstraint1.w1";
connectAttr "FKElbowShield_R.s" "ElbowShield_R.s";
connectAttr "Hip_R.s" "ElbowShield_R.is";
connectAttr "ElbowShield_R_parentConstraint1.ctx" "ElbowShield_R.tx";
connectAttr "ElbowShield_R_parentConstraint1.cty" "ElbowShield_R.ty";
connectAttr "ElbowShield_R_parentConstraint1.ctz" "ElbowShield_R.tz";
connectAttr "ElbowShield_R_parentConstraint1.crx" "ElbowShield_R.rx";
connectAttr "ElbowShield_R_parentConstraint1.cry" "ElbowShield_R.ry";
connectAttr "ElbowShield_R_parentConstraint1.crz" "ElbowShield_R.rz";
connectAttr "jointLayer.di" "ElbowShield_R.do";
connectAttr "jointLayer.di" "ElbowShield_End_R.do";
connectAttr "ElbowShield_R.ro" "ElbowShield_R_parentConstraint1.cro";
connectAttr "ElbowShield_R.pim" "ElbowShield_R_parentConstraint1.cpim";
connectAttr "ElbowShield_R.rp" "ElbowShield_R_parentConstraint1.crp";
connectAttr "ElbowShield_R.rpt" "ElbowShield_R_parentConstraint1.crt";
connectAttr "ElbowShield_R.jo" "ElbowShield_R_parentConstraint1.cjo";
connectAttr "FKXElbowShield_R.t" "ElbowShield_R_parentConstraint1.tg[0].tt";
connectAttr "FKXElbowShield_R.rp" "ElbowShield_R_parentConstraint1.tg[0].trp";
connectAttr "FKXElbowShield_R.rpt" "ElbowShield_R_parentConstraint1.tg[0].trt";
connectAttr "FKXElbowShield_R.r" "ElbowShield_R_parentConstraint1.tg[0].tr";
connectAttr "FKXElbowShield_R.ro" "ElbowShield_R_parentConstraint1.tg[0].tro";
connectAttr "FKXElbowShield_R.s" "ElbowShield_R_parentConstraint1.tg[0].ts";
connectAttr "FKXElbowShield_R.pm" "ElbowShield_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbowShield_R.jo" "ElbowShield_R_parentConstraint1.tg[0].tjo";
connectAttr "ElbowShield_R_parentConstraint1.w0" "ElbowShield_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Hip_R.ro" "Hip_R_parentConstraint1.cro";
connectAttr "Hip_R.pim" "Hip_R_parentConstraint1.cpim";
connectAttr "Hip_R.rp" "Hip_R_parentConstraint1.crp";
connectAttr "Hip_R.rpt" "Hip_R_parentConstraint1.crt";
connectAttr "Hip_R.jo" "Hip_R_parentConstraint1.cjo";
connectAttr "FKXHip_R.t" "Hip_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHip_R.rp" "Hip_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHip_R.rpt" "Hip_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHip_R.r" "Hip_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHip_R.ro" "Hip_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHip_R.s" "Hip_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHip_R.pm" "Hip_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHip_R.jo" "Hip_R_parentConstraint1.tg[0].tjo";
connectAttr "Hip_R_parentConstraint1.w0" "Hip_R_parentConstraint1.tg[0].tw";
connectAttr "IKXHip_R.t" "Hip_R_parentConstraint1.tg[1].tt";
connectAttr "IKXHip_R.rp" "Hip_R_parentConstraint1.tg[1].trp";
connectAttr "IKXHip_R.rpt" "Hip_R_parentConstraint1.tg[1].trt";
connectAttr "IKXHip_R.r" "Hip_R_parentConstraint1.tg[1].tr";
connectAttr "IKXHip_R.ro" "Hip_R_parentConstraint1.tg[1].tro";
connectAttr "IKXHip_R.s" "Hip_R_parentConstraint1.tg[1].ts";
connectAttr "IKXHip_R.pm" "Hip_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXHip_R.jo" "Hip_R_parentConstraint1.tg[1].tjo";
connectAttr "Hip_R_parentConstraint1.w1" "Hip_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Hip_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Hip_R_parentConstraint1.w1";
connectAttr "HipTwist_R.ro" "HipTwist_R_parentConstraint1.cro";
connectAttr "HipTwist_R.pim" "HipTwist_R_parentConstraint1.cpim";
connectAttr "HipTwist_R.rp" "HipTwist_R_parentConstraint1.crp";
connectAttr "HipTwist_R.rpt" "HipTwist_R_parentConstraint1.crt";
connectAttr "HipTwist_R.jo" "HipTwist_R_parentConstraint1.cjo";
connectAttr "FKXHipTwist_R.t" "HipTwist_R_parentConstraint1.tg[0].tt";
connectAttr "FKXHipTwist_R.rp" "HipTwist_R_parentConstraint1.tg[0].trp";
connectAttr "FKXHipTwist_R.rpt" "HipTwist_R_parentConstraint1.tg[0].trt";
connectAttr "FKXHipTwist_R.r" "HipTwist_R_parentConstraint1.tg[0].tr";
connectAttr "FKXHipTwist_R.ro" "HipTwist_R_parentConstraint1.tg[0].tro";
connectAttr "FKXHipTwist_R.s" "HipTwist_R_parentConstraint1.tg[0].ts";
connectAttr "FKXHipTwist_R.pm" "HipTwist_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXHipTwist_R.jo" "HipTwist_R_parentConstraint1.tg[0].tjo";
connectAttr "HipTwist_R_parentConstraint1.w0" "HipTwist_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKHead_M.s" "Head_M.s";
connectAttr "Pelvis_M.s" "Head_M.is";
connectAttr "Head_M_parentConstraint1.ctx" "Head_M.tx";
connectAttr "Head_M_parentConstraint1.cty" "Head_M.ty";
connectAttr "Head_M_parentConstraint1.ctz" "Head_M.tz";
connectAttr "Head_M_parentConstraint1.crx" "Head_M.rx";
connectAttr "Head_M_parentConstraint1.cry" "Head_M.ry";
connectAttr "Head_M_parentConstraint1.crz" "Head_M.rz";
connectAttr "jointLayer.di" "Head_M.do";
connectAttr "Head_M.s" "Head_End_M.is";
connectAttr "jointLayer.di" "Head_End_M.do";
connectAttr "FKJaw_M.s" "Jaw_M.s";
connectAttr "Head_M.s" "Jaw_M.is";
connectAttr "Jaw_M_parentConstraint1.ctx" "Jaw_M.tx";
connectAttr "Jaw_M_parentConstraint1.cty" "Jaw_M.ty";
connectAttr "Jaw_M_parentConstraint1.ctz" "Jaw_M.tz";
connectAttr "Jaw_M_parentConstraint1.crx" "Jaw_M.rx";
connectAttr "Jaw_M_parentConstraint1.cry" "Jaw_M.ry";
connectAttr "Jaw_M_parentConstraint1.crz" "Jaw_M.rz";
connectAttr "jointLayer.di" "Jaw_M.do";
connectAttr "Jaw_M.s" "Jaw_End_M.is";
connectAttr "jointLayer.di" "Jaw_End_M.do";
connectAttr "Jaw_M.ro" "Jaw_M_parentConstraint1.cro";
connectAttr "Jaw_M.pim" "Jaw_M_parentConstraint1.cpim";
connectAttr "Jaw_M.rp" "Jaw_M_parentConstraint1.crp";
connectAttr "Jaw_M.rpt" "Jaw_M_parentConstraint1.crt";
connectAttr "Jaw_M.jo" "Jaw_M_parentConstraint1.cjo";
connectAttr "FKXJaw_M.t" "Jaw_M_parentConstraint1.tg[0].tt";
connectAttr "FKXJaw_M.rp" "Jaw_M_parentConstraint1.tg[0].trp";
connectAttr "FKXJaw_M.rpt" "Jaw_M_parentConstraint1.tg[0].trt";
connectAttr "FKXJaw_M.r" "Jaw_M_parentConstraint1.tg[0].tr";
connectAttr "FKXJaw_M.ro" "Jaw_M_parentConstraint1.tg[0].tro";
connectAttr "FKXJaw_M.s" "Jaw_M_parentConstraint1.tg[0].ts";
connectAttr "FKXJaw_M.pm" "Jaw_M_parentConstraint1.tg[0].tpm";
connectAttr "FKXJaw_M.jo" "Jaw_M_parentConstraint1.tg[0].tjo";
connectAttr "Jaw_M_parentConstraint1.w0" "Jaw_M_parentConstraint1.tg[0].tw";
connectAttr "Head_M.ro" "Head_M_parentConstraint1.cro";
connectAttr "Head_M.pim" "Head_M_parentConstraint1.cpim";
connectAttr "Head_M.rp" "Head_M_parentConstraint1.crp";
connectAttr "Head_M.rpt" "Head_M_parentConstraint1.crt";
connectAttr "Head_M.jo" "Head_M_parentConstraint1.cjo";
connectAttr "FKXHead_M.t" "Head_M_parentConstraint1.tg[0].tt";
connectAttr "FKXHead_M.rp" "Head_M_parentConstraint1.tg[0].trp";
connectAttr "FKXHead_M.rpt" "Head_M_parentConstraint1.tg[0].trt";
connectAttr "FKXHead_M.r" "Head_M_parentConstraint1.tg[0].tr";
connectAttr "FKXHead_M.ro" "Head_M_parentConstraint1.tg[0].tro";
connectAttr "FKXHead_M.s" "Head_M_parentConstraint1.tg[0].ts";
connectAttr "FKXHead_M.pm" "Head_M_parentConstraint1.tg[0].tpm";
connectAttr "FKXHead_M.jo" "Head_M_parentConstraint1.tg[0].tjo";
connectAttr "Head_M_parentConstraint1.w0" "Head_M_parentConstraint1.tg[0].tw";
connectAttr "FKShoulder_R.s" "Shoulder_R.s";
connectAttr "Pelvis_M.s" "Shoulder_R.is";
connectAttr "Shoulder_R_parentConstraint1.ctx" "Shoulder_R.tx";
connectAttr "Shoulder_R_parentConstraint1.cty" "Shoulder_R.ty";
connectAttr "Shoulder_R_parentConstraint1.ctz" "Shoulder_R.tz";
connectAttr "Shoulder_R_parentConstraint1.crx" "Shoulder_R.rx";
connectAttr "Shoulder_R_parentConstraint1.cry" "Shoulder_R.ry";
connectAttr "Shoulder_R_parentConstraint1.crz" "Shoulder_R.rz";
connectAttr "jointLayer.di" "Shoulder_R.do";
connectAttr "FKElbow_R.s" "Elbow_R.s";
connectAttr "Shoulder_R.s" "Elbow_R.is";
connectAttr "Elbow_R_parentConstraint1.ctx" "Elbow_R.tx";
connectAttr "Elbow_R_parentConstraint1.cty" "Elbow_R.ty";
connectAttr "Elbow_R_parentConstraint1.ctz" "Elbow_R.tz";
connectAttr "Elbow_R_parentConstraint1.crx" "Elbow_R.rx";
connectAttr "Elbow_R_parentConstraint1.cry" "Elbow_R.ry";
connectAttr "Elbow_R_parentConstraint1.crz" "Elbow_R.rz";
connectAttr "jointLayer.di" "Elbow_R.do";
connectAttr "FKFingers_R.s" "Fingers_R.s";
connectAttr "Elbow_R.s" "Fingers_R.is";
connectAttr "Fingers_R_parentConstraint1.ctx" "Fingers_R.tx";
connectAttr "Fingers_R_parentConstraint1.cty" "Fingers_R.ty";
connectAttr "Fingers_R_parentConstraint1.ctz" "Fingers_R.tz";
connectAttr "Fingers_R_parentConstraint1.crx" "Fingers_R.rx";
connectAttr "Fingers_R_parentConstraint1.cry" "Fingers_R.ry";
connectAttr "Fingers_R_parentConstraint1.crz" "Fingers_R.rz";
connectAttr "jointLayer.di" "Fingers_R.do";
connectAttr "jointLayer.di" "Fingers_End_R.do";
connectAttr "Fingers_R.ro" "Fingers_R_parentConstraint1.cro";
connectAttr "Fingers_R.pim" "Fingers_R_parentConstraint1.cpim";
connectAttr "Fingers_R.rp" "Fingers_R_parentConstraint1.crp";
connectAttr "Fingers_R.rpt" "Fingers_R_parentConstraint1.crt";
connectAttr "Fingers_R.jo" "Fingers_R_parentConstraint1.cjo";
connectAttr "FKXFingers_R.t" "Fingers_R_parentConstraint1.tg[0].tt";
connectAttr "FKXFingers_R.rp" "Fingers_R_parentConstraint1.tg[0].trp";
connectAttr "FKXFingers_R.rpt" "Fingers_R_parentConstraint1.tg[0].trt";
connectAttr "FKXFingers_R.r" "Fingers_R_parentConstraint1.tg[0].tr";
connectAttr "FKXFingers_R.ro" "Fingers_R_parentConstraint1.tg[0].tro";
connectAttr "FKXFingers_R.s" "Fingers_R_parentConstraint1.tg[0].ts";
connectAttr "FKXFingers_R.pm" "Fingers_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXFingers_R.jo" "Fingers_R_parentConstraint1.tg[0].tjo";
connectAttr "Fingers_R_parentConstraint1.w0" "Fingers_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Elbow_R.ro" "Elbow_R_parentConstraint1.cro";
connectAttr "Elbow_R.pim" "Elbow_R_parentConstraint1.cpim";
connectAttr "Elbow_R.rp" "Elbow_R_parentConstraint1.crp";
connectAttr "Elbow_R.rpt" "Elbow_R_parentConstraint1.crt";
connectAttr "Elbow_R.jo" "Elbow_R_parentConstraint1.cjo";
connectAttr "FKXElbow_R.t" "Elbow_R_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow_R.rp" "Elbow_R_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow_R.rpt" "Elbow_R_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow_R.r" "Elbow_R_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow_R.ro" "Elbow_R_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow_R.s" "Elbow_R_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow_R.pm" "Elbow_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow_R.jo" "Elbow_R_parentConstraint1.tg[0].tjo";
connectAttr "Elbow_R_parentConstraint1.w0" "Elbow_R_parentConstraint1.tg[0].tw";
connectAttr "Shoulder_R.ro" "Shoulder_R_parentConstraint1.cro";
connectAttr "Shoulder_R.pim" "Shoulder_R_parentConstraint1.cpim";
connectAttr "Shoulder_R.rp" "Shoulder_R_parentConstraint1.crp";
connectAttr "Shoulder_R.rpt" "Shoulder_R_parentConstraint1.crt";
connectAttr "Shoulder_R.jo" "Shoulder_R_parentConstraint1.cjo";
connectAttr "FKXShoulder_R.t" "Shoulder_R_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder_R.rp" "Shoulder_R_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder_R.rpt" "Shoulder_R_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder_R.r" "Shoulder_R_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder_R.ro" "Shoulder_R_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder_R.s" "Shoulder_R_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder_R.pm" "Shoulder_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder_R.jo" "Shoulder_R_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder_R_parentConstraint1.w0" "Shoulder_R_parentConstraint1.tg[0].tw"
		;
connectAttr "HipTwist_L_parentConstraint1.ctx" "HipTwist_L.tx";
connectAttr "HipTwist_L_parentConstraint1.cty" "HipTwist_L.ty";
connectAttr "HipTwist_L_parentConstraint1.ctz" "HipTwist_L.tz";
connectAttr "HipTwist_L_parentConstraint1.crx" "HipTwist_L.rx";
connectAttr "HipTwist_L_parentConstraint1.cry" "HipTwist_L.ry";
connectAttr "HipTwist_L_parentConstraint1.crz" "HipTwist_L.rz";
connectAttr "FKHipTwist_L.s" "HipTwist_L.s";
connectAttr "Pelvis_M.s" "HipTwist_L.is";
connectAttr "jointLayer.di" "HipTwist_L.do";
connectAttr "Hip_L_parentConstraint1.ctx" "Hip_L.tx";
connectAttr "Hip_L_parentConstraint1.cty" "Hip_L.ty";
connectAttr "Hip_L_parentConstraint1.ctz" "Hip_L.tz";
connectAttr "Hip_L_parentConstraint1.crx" "Hip_L.rx";
connectAttr "Hip_L_parentConstraint1.cry" "Hip_L.ry";
connectAttr "Hip_L_parentConstraint1.crz" "Hip_L.rz";
connectAttr "ScaleBlendHip_L.op" "Hip_L.s";
connectAttr "jointLayer.di" "Hip_L.do";
connectAttr "ScaleBlendKnee_L.op" "Knee_L.s";
connectAttr "Hip_L.s" "Knee_L.is";
connectAttr "Knee_L_parentConstraint1.ctx" "Knee_L.tx";
connectAttr "Knee_L_parentConstraint1.cty" "Knee_L.ty";
connectAttr "Knee_L_parentConstraint1.ctz" "Knee_L.tz";
connectAttr "Knee_L_parentConstraint1.crx" "Knee_L.rx";
connectAttr "Knee_L_parentConstraint1.cry" "Knee_L.ry";
connectAttr "Knee_L_parentConstraint1.crz" "Knee_L.rz";
connectAttr "jointLayer.di" "Knee_L.do";
connectAttr "Ankle_L_parentConstraint1.ctx" "Ankle_L.tx";
connectAttr "Ankle_L_parentConstraint1.cty" "Ankle_L.ty";
connectAttr "Ankle_L_parentConstraint1.ctz" "Ankle_L.tz";
connectAttr "Ankle_L_parentConstraint1.crx" "Ankle_L.rx";
connectAttr "Ankle_L_parentConstraint1.cry" "Ankle_L.ry";
connectAttr "Ankle_L_parentConstraint1.crz" "Ankle_L.rz";
connectAttr "ScaleBlendAnkle_L.op" "Ankle_L.s";
connectAttr "Knee_L.s" "Ankle_L.is";
connectAttr "jointLayer.di" "Ankle_L.do";
connectAttr "FKMiddleToe1_L.s" "MiddleToe1_L.s";
connectAttr "Ankle_L.s" "MiddleToe1_L.is";
connectAttr "MiddleToe1_L_parentConstraint1.ctx" "MiddleToe1_L.tx";
connectAttr "MiddleToe1_L_parentConstraint1.cty" "MiddleToe1_L.ty";
connectAttr "MiddleToe1_L_parentConstraint1.ctz" "MiddleToe1_L.tz";
connectAttr "MiddleToe1_L_parentConstraint1.crx" "MiddleToe1_L.rx";
connectAttr "MiddleToe1_L_parentConstraint1.cry" "MiddleToe1_L.ry";
connectAttr "MiddleToe1_L_parentConstraint1.crz" "MiddleToe1_L.rz";
connectAttr "jointLayer.di" "MiddleToe1_L.do";
connectAttr "MiddleToe1_L.s" "MiddleToe2_End_L.is";
connectAttr "jointLayer.di" "MiddleToe2_End_L.do";
connectAttr "MiddleToe1_L.ro" "MiddleToe1_L_parentConstraint1.cro";
connectAttr "MiddleToe1_L.pim" "MiddleToe1_L_parentConstraint1.cpim";
connectAttr "MiddleToe1_L.rp" "MiddleToe1_L_parentConstraint1.crp";
connectAttr "MiddleToe1_L.rpt" "MiddleToe1_L_parentConstraint1.crt";
connectAttr "MiddleToe1_L.jo" "MiddleToe1_L_parentConstraint1.cjo";
connectAttr "FKXMiddleToe1_L.t" "MiddleToe1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleToe1_L.rp" "MiddleToe1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXMiddleToe1_L.rpt" "MiddleToe1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXMiddleToe1_L.r" "MiddleToe1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleToe1_L.ro" "MiddleToe1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXMiddleToe1_L.s" "MiddleToe1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleToe1_L.pm" "MiddleToe1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXMiddleToe1_L.jo" "MiddleToe1_L_parentConstraint1.tg[0].tjo";
connectAttr "MiddleToe1_L_parentConstraint1.w0" "MiddleToe1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Ankle_L.ro" "Ankle_L_parentConstraint1.cro";
connectAttr "Ankle_L.pim" "Ankle_L_parentConstraint1.cpim";
connectAttr "Ankle_L.rp" "Ankle_L_parentConstraint1.crp";
connectAttr "Ankle_L.rpt" "Ankle_L_parentConstraint1.crt";
connectAttr "Ankle_L.jo" "Ankle_L_parentConstraint1.cjo";
connectAttr "FKXAnkle_L.t" "Ankle_L_parentConstraint1.tg[0].tt";
connectAttr "FKXAnkle_L.rp" "Ankle_L_parentConstraint1.tg[0].trp";
connectAttr "FKXAnkle_L.rpt" "Ankle_L_parentConstraint1.tg[0].trt";
connectAttr "FKXAnkle_L.r" "Ankle_L_parentConstraint1.tg[0].tr";
connectAttr "FKXAnkle_L.ro" "Ankle_L_parentConstraint1.tg[0].tro";
connectAttr "FKXAnkle_L.s" "Ankle_L_parentConstraint1.tg[0].ts";
connectAttr "FKXAnkle_L.pm" "Ankle_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXAnkle_L.jo" "Ankle_L_parentConstraint1.tg[0].tjo";
connectAttr "Ankle_L_parentConstraint1.w0" "Ankle_L_parentConstraint1.tg[0].tw";
connectAttr "IKXAnkle_L.t" "Ankle_L_parentConstraint1.tg[1].tt";
connectAttr "IKXAnkle_L.rp" "Ankle_L_parentConstraint1.tg[1].trp";
connectAttr "IKXAnkle_L.rpt" "Ankle_L_parentConstraint1.tg[1].trt";
connectAttr "IKXAnkle_L.r" "Ankle_L_parentConstraint1.tg[1].tr";
connectAttr "IKXAnkle_L.ro" "Ankle_L_parentConstraint1.tg[1].tro";
connectAttr "IKXAnkle_L.s" "Ankle_L_parentConstraint1.tg[1].ts";
connectAttr "IKXAnkle_L.pm" "Ankle_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXAnkle_L.jo" "Ankle_L_parentConstraint1.tg[1].tjo";
connectAttr "Ankle_L_parentConstraint1.w1" "Ankle_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Ankle_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Ankle_L_parentConstraint1.w1";
connectAttr "Knee_L.ro" "Knee_L_parentConstraint1.cro";
connectAttr "Knee_L.pim" "Knee_L_parentConstraint1.cpim";
connectAttr "Knee_L.rp" "Knee_L_parentConstraint1.crp";
connectAttr "Knee_L.rpt" "Knee_L_parentConstraint1.crt";
connectAttr "Knee_L.jo" "Knee_L_parentConstraint1.cjo";
connectAttr "FKXKnee_L.t" "Knee_L_parentConstraint1.tg[0].tt";
connectAttr "FKXKnee_L.rp" "Knee_L_parentConstraint1.tg[0].trp";
connectAttr "FKXKnee_L.rpt" "Knee_L_parentConstraint1.tg[0].trt";
connectAttr "FKXKnee_L.r" "Knee_L_parentConstraint1.tg[0].tr";
connectAttr "FKXKnee_L.ro" "Knee_L_parentConstraint1.tg[0].tro";
connectAttr "FKXKnee_L.s" "Knee_L_parentConstraint1.tg[0].ts";
connectAttr "FKXKnee_L.pm" "Knee_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXKnee_L.jo" "Knee_L_parentConstraint1.tg[0].tjo";
connectAttr "Knee_L_parentConstraint1.w0" "Knee_L_parentConstraint1.tg[0].tw";
connectAttr "IKXKnee_L.t" "Knee_L_parentConstraint1.tg[1].tt";
connectAttr "IKXKnee_L.rp" "Knee_L_parentConstraint1.tg[1].trp";
connectAttr "IKXKnee_L.rpt" "Knee_L_parentConstraint1.tg[1].trt";
connectAttr "IKXKnee_L.r" "Knee_L_parentConstraint1.tg[1].tr";
connectAttr "IKXKnee_L.ro" "Knee_L_parentConstraint1.tg[1].tro";
connectAttr "IKXKnee_L.s" "Knee_L_parentConstraint1.tg[1].ts";
connectAttr "IKXKnee_L.pm" "Knee_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXKnee_L.jo" "Knee_L_parentConstraint1.tg[1].tjo";
connectAttr "Knee_L_parentConstraint1.w1" "Knee_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Knee_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Knee_L_parentConstraint1.w1";
connectAttr "FKElbowShield_L.s" "ElbowShield_L.s";
connectAttr "Hip_L.s" "ElbowShield_L.is";
connectAttr "ElbowShield_L_parentConstraint1.ctx" "ElbowShield_L.tx";
connectAttr "ElbowShield_L_parentConstraint1.cty" "ElbowShield_L.ty";
connectAttr "ElbowShield_L_parentConstraint1.ctz" "ElbowShield_L.tz";
connectAttr "ElbowShield_L_parentConstraint1.crx" "ElbowShield_L.rx";
connectAttr "ElbowShield_L_parentConstraint1.cry" "ElbowShield_L.ry";
connectAttr "ElbowShield_L_parentConstraint1.crz" "ElbowShield_L.rz";
connectAttr "jointLayer.di" "ElbowShield_L.do";
connectAttr "jointLayer.di" "ElbowShield_End_L.do";
connectAttr "ElbowShield_L.ro" "ElbowShield_L_parentConstraint1.cro";
connectAttr "ElbowShield_L.pim" "ElbowShield_L_parentConstraint1.cpim";
connectAttr "ElbowShield_L.rp" "ElbowShield_L_parentConstraint1.crp";
connectAttr "ElbowShield_L.rpt" "ElbowShield_L_parentConstraint1.crt";
connectAttr "ElbowShield_L.jo" "ElbowShield_L_parentConstraint1.cjo";
connectAttr "FKXElbowShield_L.t" "ElbowShield_L_parentConstraint1.tg[0].tt";
connectAttr "FKXElbowShield_L.rp" "ElbowShield_L_parentConstraint1.tg[0].trp";
connectAttr "FKXElbowShield_L.rpt" "ElbowShield_L_parentConstraint1.tg[0].trt";
connectAttr "FKXElbowShield_L.r" "ElbowShield_L_parentConstraint1.tg[0].tr";
connectAttr "FKXElbowShield_L.ro" "ElbowShield_L_parentConstraint1.tg[0].tro";
connectAttr "FKXElbowShield_L.s" "ElbowShield_L_parentConstraint1.tg[0].ts";
connectAttr "FKXElbowShield_L.pm" "ElbowShield_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbowShield_L.jo" "ElbowShield_L_parentConstraint1.tg[0].tjo";
connectAttr "ElbowShield_L_parentConstraint1.w0" "ElbowShield_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Hip_L.ro" "Hip_L_parentConstraint1.cro";
connectAttr "Hip_L.pim" "Hip_L_parentConstraint1.cpim";
connectAttr "Hip_L.rp" "Hip_L_parentConstraint1.crp";
connectAttr "Hip_L.rpt" "Hip_L_parentConstraint1.crt";
connectAttr "Hip_L.jo" "Hip_L_parentConstraint1.cjo";
connectAttr "FKXHip_L.t" "Hip_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHip_L.rp" "Hip_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHip_L.rpt" "Hip_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHip_L.r" "Hip_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHip_L.ro" "Hip_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHip_L.s" "Hip_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHip_L.pm" "Hip_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHip_L.jo" "Hip_L_parentConstraint1.tg[0].tjo";
connectAttr "Hip_L_parentConstraint1.w0" "Hip_L_parentConstraint1.tg[0].tw";
connectAttr "IKXHip_L.t" "Hip_L_parentConstraint1.tg[1].tt";
connectAttr "IKXHip_L.rp" "Hip_L_parentConstraint1.tg[1].trp";
connectAttr "IKXHip_L.rpt" "Hip_L_parentConstraint1.tg[1].trt";
connectAttr "IKXHip_L.r" "Hip_L_parentConstraint1.tg[1].tr";
connectAttr "IKXHip_L.ro" "Hip_L_parentConstraint1.tg[1].tro";
connectAttr "IKXHip_L.s" "Hip_L_parentConstraint1.tg[1].ts";
connectAttr "IKXHip_L.pm" "Hip_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXHip_L.jo" "Hip_L_parentConstraint1.tg[1].tjo";
connectAttr "Hip_L_parentConstraint1.w1" "Hip_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Hip_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Hip_L_parentConstraint1.w1";
connectAttr "HipTwist_L.ro" "HipTwist_L_parentConstraint1.cro";
connectAttr "HipTwist_L.pim" "HipTwist_L_parentConstraint1.cpim";
connectAttr "HipTwist_L.rp" "HipTwist_L_parentConstraint1.crp";
connectAttr "HipTwist_L.rpt" "HipTwist_L_parentConstraint1.crt";
connectAttr "HipTwist_L.jo" "HipTwist_L_parentConstraint1.cjo";
connectAttr "FKXHipTwist_L.t" "HipTwist_L_parentConstraint1.tg[0].tt";
connectAttr "FKXHipTwist_L.rp" "HipTwist_L_parentConstraint1.tg[0].trp";
connectAttr "FKXHipTwist_L.rpt" "HipTwist_L_parentConstraint1.tg[0].trt";
connectAttr "FKXHipTwist_L.r" "HipTwist_L_parentConstraint1.tg[0].tr";
connectAttr "FKXHipTwist_L.ro" "HipTwist_L_parentConstraint1.tg[0].tro";
connectAttr "FKXHipTwist_L.s" "HipTwist_L_parentConstraint1.tg[0].ts";
connectAttr "FKXHipTwist_L.pm" "HipTwist_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXHipTwist_L.jo" "HipTwist_L_parentConstraint1.tg[0].tjo";
connectAttr "HipTwist_L_parentConstraint1.w0" "HipTwist_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKShoulder_L.s" "Shoulder_L.s";
connectAttr "Pelvis_M.s" "Shoulder_L.is";
connectAttr "Shoulder_L_parentConstraint1.ctx" "Shoulder_L.tx";
connectAttr "Shoulder_L_parentConstraint1.cty" "Shoulder_L.ty";
connectAttr "Shoulder_L_parentConstraint1.ctz" "Shoulder_L.tz";
connectAttr "Shoulder_L_parentConstraint1.crx" "Shoulder_L.rx";
connectAttr "Shoulder_L_parentConstraint1.cry" "Shoulder_L.ry";
connectAttr "Shoulder_L_parentConstraint1.crz" "Shoulder_L.rz";
connectAttr "jointLayer.di" "Shoulder_L.do";
connectAttr "FKElbow_L.s" "Elbow_L.s";
connectAttr "Shoulder_L.s" "Elbow_L.is";
connectAttr "Elbow_L_parentConstraint1.ctx" "Elbow_L.tx";
connectAttr "Elbow_L_parentConstraint1.cty" "Elbow_L.ty";
connectAttr "Elbow_L_parentConstraint1.ctz" "Elbow_L.tz";
connectAttr "Elbow_L_parentConstraint1.crx" "Elbow_L.rx";
connectAttr "Elbow_L_parentConstraint1.cry" "Elbow_L.ry";
connectAttr "Elbow_L_parentConstraint1.crz" "Elbow_L.rz";
connectAttr "jointLayer.di" "Elbow_L.do";
connectAttr "FKFingers_L.s" "Fingers_L.s";
connectAttr "Elbow_L.s" "Fingers_L.is";
connectAttr "Fingers_L_parentConstraint1.ctx" "Fingers_L.tx";
connectAttr "Fingers_L_parentConstraint1.cty" "Fingers_L.ty";
connectAttr "Fingers_L_parentConstraint1.ctz" "Fingers_L.tz";
connectAttr "Fingers_L_parentConstraint1.crx" "Fingers_L.rx";
connectAttr "Fingers_L_parentConstraint1.cry" "Fingers_L.ry";
connectAttr "Fingers_L_parentConstraint1.crz" "Fingers_L.rz";
connectAttr "jointLayer.di" "Fingers_L.do";
connectAttr "jointLayer.di" "Fingers_End_L.do";
connectAttr "Fingers_L.ro" "Fingers_L_parentConstraint1.cro";
connectAttr "Fingers_L.pim" "Fingers_L_parentConstraint1.cpim";
connectAttr "Fingers_L.rp" "Fingers_L_parentConstraint1.crp";
connectAttr "Fingers_L.rpt" "Fingers_L_parentConstraint1.crt";
connectAttr "Fingers_L.jo" "Fingers_L_parentConstraint1.cjo";
connectAttr "FKXFingers_L.t" "Fingers_L_parentConstraint1.tg[0].tt";
connectAttr "FKXFingers_L.rp" "Fingers_L_parentConstraint1.tg[0].trp";
connectAttr "FKXFingers_L.rpt" "Fingers_L_parentConstraint1.tg[0].trt";
connectAttr "FKXFingers_L.r" "Fingers_L_parentConstraint1.tg[0].tr";
connectAttr "FKXFingers_L.ro" "Fingers_L_parentConstraint1.tg[0].tro";
connectAttr "FKXFingers_L.s" "Fingers_L_parentConstraint1.tg[0].ts";
connectAttr "FKXFingers_L.pm" "Fingers_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXFingers_L.jo" "Fingers_L_parentConstraint1.tg[0].tjo";
connectAttr "Fingers_L_parentConstraint1.w0" "Fingers_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Elbow_L.ro" "Elbow_L_parentConstraint1.cro";
connectAttr "Elbow_L.pim" "Elbow_L_parentConstraint1.cpim";
connectAttr "Elbow_L.rp" "Elbow_L_parentConstraint1.crp";
connectAttr "Elbow_L.rpt" "Elbow_L_parentConstraint1.crt";
connectAttr "Elbow_L.jo" "Elbow_L_parentConstraint1.cjo";
connectAttr "FKXElbow_L.t" "Elbow_L_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow_L.rp" "Elbow_L_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow_L.rpt" "Elbow_L_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow_L.r" "Elbow_L_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow_L.ro" "Elbow_L_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow_L.s" "Elbow_L_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow_L.pm" "Elbow_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow_L.jo" "Elbow_L_parentConstraint1.tg[0].tjo";
connectAttr "Elbow_L_parentConstraint1.w0" "Elbow_L_parentConstraint1.tg[0].tw";
connectAttr "Shoulder_L.ro" "Shoulder_L_parentConstraint1.cro";
connectAttr "Shoulder_L.pim" "Shoulder_L_parentConstraint1.cpim";
connectAttr "Shoulder_L.rp" "Shoulder_L_parentConstraint1.crp";
connectAttr "Shoulder_L.rpt" "Shoulder_L_parentConstraint1.crt";
connectAttr "Shoulder_L.jo" "Shoulder_L_parentConstraint1.cjo";
connectAttr "FKXShoulder_L.t" "Shoulder_L_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder_L.rp" "Shoulder_L_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder_L.rpt" "Shoulder_L_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder_L.r" "Shoulder_L_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder_L.ro" "Shoulder_L_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder_L.s" "Shoulder_L_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder_L.pm" "Shoulder_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder_L.jo" "Shoulder_L_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder_L_parentConstraint1.w0" "Shoulder_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Pelvis_M.pim" "Pelvis_M_pointConstraint1.cpim";
connectAttr "Pelvis_M.rp" "Pelvis_M_pointConstraint1.crp";
connectAttr "Pelvis_M.rpt" "Pelvis_M_pointConstraint1.crt";
connectAttr "FKXPelvis_M.t" "Pelvis_M_pointConstraint1.tg[0].tt";
connectAttr "FKXPelvis_M.rp" "Pelvis_M_pointConstraint1.tg[0].trp";
connectAttr "FKXPelvis_M.rpt" "Pelvis_M_pointConstraint1.tg[0].trt";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_pointConstraint1.tg[0].tpm";
connectAttr "Pelvis_M_pointConstraint1.w0" "Pelvis_M_pointConstraint1.tg[0].tw";
connectAttr "Pelvis_M.ro" "Pelvis_M_orientConstraint1.cro";
connectAttr "Pelvis_M.pim" "Pelvis_M_orientConstraint1.cpim";
connectAttr "Pelvis_M.jo" "Pelvis_M_orientConstraint1.cjo";
connectAttr "FKXPelvis_M.r" "Pelvis_M_orientConstraint1.tg[0].tr";
connectAttr "FKXPelvis_M.ro" "Pelvis_M_orientConstraint1.tg[0].tro";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXPelvis_M.jo" "Pelvis_M_orientConstraint1.tg[0].tjo";
connectAttr "Pelvis_M_orientConstraint1.w0" "Pelvis_M_orientConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "irisPuppetSG.msg" "materialInfo3.sg";
connectAttr "BPRig_irisPuppetSG.msg" "BPRig_materialInfo3.sg";
connectAttr "jointVisReverse.ox" "jointLayer.lod";
connectAttr "Main.jointVis" "jointVisReverse.ix";
connectAttr "Main.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleToe1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKAnkle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraAnkle_R.iog" "ControlSet.dsm" -na;
connectAttr "FKKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraKnee_R.iog" "ControlSet.dsm" -na;
connectAttr "FKElbowShield_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbowShield_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHip_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHip_R.iog" "ControlSet.dsm" -na;
connectAttr "FKHipTwist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHipTwist_R.iog" "ControlSet.dsm" -na;
connectAttr "FKJaw_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraJaw_M.iog" "ControlSet.dsm" -na;
connectAttr "FKHead_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHead_M.iog" "ControlSet.dsm" -na;
connectAttr "FKFingers_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraFingers_R.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow_R.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder_R.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleToe1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKAnkle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraAnkle_L.iog" "ControlSet.dsm" -na;
connectAttr "FKKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraKnee_L.iog" "ControlSet.dsm" -na;
connectAttr "FKElbowShield_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbowShield_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHip_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHip_L.iog" "ControlSet.dsm" -na;
connectAttr "FKHipTwist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraHipTwist_L.iog" "ControlSet.dsm" -na;
connectAttr "FKFingers_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraFingers_L.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegToe_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegToe_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegToe_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegToe_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "ControlSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "ControlSet.dsm" -na;
connectAttr "Center_M.iog" "ControlSet.dsm" -na;
connectAttr "MiddleToe1_R.iog" "GameSet.dsm" -na;
connectAttr "Ankle_R.iog" "GameSet.dsm" -na;
connectAttr "Knee_R.iog" "GameSet.dsm" -na;
connectAttr "ElbowShield_R.iog" "GameSet.dsm" -na;
connectAttr "Hip_R.iog" "GameSet.dsm" -na;
connectAttr "HipTwist_R.iog" "GameSet.dsm" -na;
connectAttr "Jaw_M.iog" "GameSet.dsm" -na;
connectAttr "Head_M.iog" "GameSet.dsm" -na;
connectAttr "Fingers_R.iog" "GameSet.dsm" -na;
connectAttr "Elbow_R.iog" "GameSet.dsm" -na;
connectAttr "Shoulder_R.iog" "GameSet.dsm" -na;
connectAttr "Pelvis_M.iog" "GameSet.dsm" -na;
connectAttr "MiddleToe1_L.iog" "GameSet.dsm" -na;
connectAttr "Ankle_L.iog" "GameSet.dsm" -na;
connectAttr "Knee_L.iog" "GameSet.dsm" -na;
connectAttr "ElbowShield_L.iog" "GameSet.dsm" -na;
connectAttr "Hip_L.iog" "GameSet.dsm" -na;
connectAttr "HipTwist_L.iog" "GameSet.dsm" -na;
connectAttr "Fingers_L.iog" "GameSet.dsm" -na;
connectAttr "Elbow_L.iog" "GameSet.dsm" -na;
connectAttr "Shoulder_L.iog" "GameSet.dsm" -na;
connectAttr "buildPose.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "CenterBtwFeet_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendHip_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendKnee_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAnkle_L.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion12.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion11.msg" "AllSet.dnsm" -na;
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_L.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion10.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion9.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion8.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "Leg_LAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "IKLiftToeLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion7.msg" "AllSet.dnsm" -na;
connectAttr "GlobalHead_reverse_M.msg" "AllSet.dnsm" -na;
connectAttr "GlobalHead_unitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendHip_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendKnee_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAnkle_R.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion6.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion5.msg" "AllSet.dnsm" -na;
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_R.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion4.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion3.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion2.msg" "AllSet.dnsm" -na;
connectAttr "IKRollAngleLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "Leg_RAngleReverse.msg" "AllSet.dnsm" -na;
connectAttr "IKLiftToeLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion1.msg" "AllSet.dnsm" -na;
connectAttr "GameSet.msg" "AllSet.dnsm" -na;
connectAttr "ControlSet.msg" "AllSet.dnsm" -na;
connectAttr "jointVisReverse.msg" "AllSet.dnsm" -na;
connectAttr "jointLayer.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsOffset_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenter_M.iog" "AllSet.dsm" -na;
connectAttr "Center_MShape.iog" "AllSet.dsm" -na;
connectAttr "Center_M.iog" "AllSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "AllSet.dsm" -na;
connectAttr "CenterOffset_M.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Fingers_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToHip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Knee_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Ankle_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_L_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "MiddleToe1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_L.iog" "AllSet.dsm" -na;
connectAttr "effector6.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_L.iog" "AllSet.dsm" -na;
connectAttr "IKLiftToeLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleToe_L.iog" "AllSet.dsm" -na;
connectAttr "effector5.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegToe_L.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_L.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_LStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L.iog" "AllSet.dsm" -na;
connectAttr "effector4.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalHead_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "GlobalHead_M.iog" "AllSet.dsm" -na;
connectAttr "GlobalOffsetHead_M.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Fingers_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Head_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Jaw_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Hip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToHip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Knee_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Ankle_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_R_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "MiddleToe1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFootPivotBallReverseLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleBall_R.iog" "AllSet.dsm" -na;
connectAttr "effector3.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegBall_R.iog" "AllSet.dsm" -na;
connectAttr "IKLiftToeLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandleToe_R.iog" "AllSet.dsm" -na;
connectAttr "effector2.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegToe_R.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKRollLegHeel_R.iog" "AllSet.dsm" -na;
connectAttr "IKFootRollLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_RStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R.iog" "AllSet.dsm" -na;
connectAttr "effector1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKXFingers_L.iog" "AllSet.dsm" -na;
connectAttr "FKFingers_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKFingers_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraFingers_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetFingers_L.iog" "AllSet.dsm" -na;
connectAttr "FKXFingers_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "IKXHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetHip_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKHip_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbowShield_L.iog" "AllSet.dsm" -na;
connectAttr "FKElbowShield_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbowShield_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbowShield_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbowShield_L.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToHip_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbowShield_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKXKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKXKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraKnee_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetKnee_L.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKXAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "FKXHeel_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_L.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe2_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe2_End_L.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_L.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L.iog" "AllSet.dsm" -na;
connectAttr "Fingers_L.iog" "AllSet.dsm" -na;
connectAttr "Fingers_End_L.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_L.iog" "AllSet.dsm" -na;
connectAttr "Hip_L.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_L.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_End_L.iog" "AllSet.dsm" -na;
connectAttr "Knee_L.iog" "AllSet.dsm" -na;
connectAttr "Ankle_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe1_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe2_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKXPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKXFingers_R.iog" "AllSet.dsm" -na;
connectAttr "FKFingers_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKFingers_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraFingers_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetFingers_R.iog" "AllSet.dsm" -na;
connectAttr "FKXFingers_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKHead_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalStaticHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHead_M.iog" "AllSet.dsm" -na;
connectAttr "FKXJaw_M.iog" "AllSet.dsm" -na;
connectAttr "FKJaw_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKJaw_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraJaw_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetJaw_M.iog" "AllSet.dsm" -na;
connectAttr "FKXJaw_End_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHead_End_M.iog" "AllSet.dsm" -na;
connectAttr "FKXHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "IKXHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetHip_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKHip_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbowShield_R.iog" "AllSet.dsm" -na;
connectAttr "FKElbowShield_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbowShield_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbowShield_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbowShield_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToHip_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbowShield_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKXKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraKnee_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetKnee_R.iog" "AllSet.dsm" -na;
connectAttr "IKXAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKXAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "FKXHeel_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToAnkle_R.iog" "AllSet.dsm" -na;
connectAttr "IKXMiddleToe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleToe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_R.iog" "AllSet.dsm" -na;
connectAttr "Elbow_R.iog" "AllSet.dsm" -na;
connectAttr "Fingers_R.iog" "AllSet.dsm" -na;
connectAttr "Fingers_End_R.iog" "AllSet.dsm" -na;
connectAttr "Head_M.iog" "AllSet.dsm" -na;
connectAttr "Jaw_M.iog" "AllSet.dsm" -na;
connectAttr "Jaw_End_M.iog" "AllSet.dsm" -na;
connectAttr "Head_End_M.iog" "AllSet.dsm" -na;
connectAttr "HipTwist_R.iog" "AllSet.dsm" -na;
connectAttr "Hip_R.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_R.iog" "AllSet.dsm" -na;
connectAttr "ElbowShield_End_R.iog" "AllSet.dsm" -na;
connectAttr "Knee_R.iog" "AllSet.dsm" -na;
connectAttr "Ankle_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe1_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleToe2_End_R.iog" "AllSet.dsm" -na;
connectAttr "IKMessure.iog" "AllSet.dsm" -na;
connectAttr "IKCrv.iog" "AllSet.dsm" -na;
connectAttr "IKStatic.iog" "AllSet.dsm" -na;
connectAttr "IKHandle.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraint.iog" "AllSet.dsm" -na;
connectAttr "GlobalSystem.iog" "AllSet.dsm" -na;
connectAttr "RootSystem.iog" "AllSet.dsm" -na;
connectAttr "FKIKSystem.iog" "AllSet.dsm" -na;
connectAttr "IKSystem.iog" "AllSet.dsm" -na;
connectAttr "FKSystem.iog" "AllSet.dsm" -na;
connectAttr "GameSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MotionSystem.iog" "AllSet.dsm" -na;
connectAttr "BaseSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MainShape.iog" "AllSet.dsm" -na;
connectAttr "Main.iog" "AllSet.dsm" -na;
connectAttr "forwardArrowShape.iog" "AllSet.dsm" -na;
connectAttr "GameSet.msg" "Sets.dnsm" -na;
connectAttr "ControlSet.msg" "Sets.dnsm" -na;
connectAttr "AllSet.msg" "Sets.dnsm" -na;
connectAttr "IKLeg_R.swivel" "unitConversion1.i";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vx";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vy";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegUnitConversion_R.i";
connectAttr "FKIKBlendLegUnitConversion_R.o" "FKIKBlendLegReverse_R.ix";
connectAttr "FKIKLeg_R.autoVis" "FKIKBlendLegCondition_R.ft";
connectAttr "FKIKLeg_R.IKVis" "FKIKBlendLegCondition_R.ctr";
connectAttr "FKIKLeg_R.FKVis" "FKIKBlendLegCondition_R.ctg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegCondition_R.cfr";
connectAttr "FKIKBlendLegsetRange_R.ox" "FKIKBlendLegCondition_R.cfg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegsetRange_R.vx";
connectAttr "IKLeg_R.toe" "IKLiftToeLegUnitConversion_R.i";
connectAttr "IKLeg_R.rollAngle" "Leg_RAngleReverse.i1x";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vx";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vy";
connectAttr "IKLeg_R.roll" "IKRollAngleLeg_R.vz";
connectAttr "Leg_RAngleReverse.ox" "IKRollAngleLeg_R.nx";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.my";
connectAttr "IKLeg_R.rollAngle" "IKRollAngleLeg_R.mz";
connectAttr "IKRollAngleLeg_R.ox" "unitConversion2.i";
connectAttr "IKRollAngleLeg_R.oy" "unitConversion3.i";
connectAttr "IKRollAngleLeg_R.oz" "unitConversion4.i";
connectAttr "unitConversion5.o" "IKBallToFKBallMiddleToe1blendTwoAttr_R.i[1]";
connectAttr "FKIKBlendLegUnitConversion_R.o" "IKBallToFKBallMiddleToe1blendTwoAttr_R.ab"
		;
connectAttr "IKXMiddleToe1_R.rx" "unitConversion5.i";
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_R.o" "unitConversion6.i";
connectAttr "FKAnkle_R.s" "ScaleBlendAnkle_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendAnkle_R.b";
connectAttr "FKKnee_R.s" "ScaleBlendKnee_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendKnee_R.b";
connectAttr "FKHip_R.s" "ScaleBlendHip_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendHip_R.b";
connectAttr "FKHead_M.Global" "GlobalHead_unitConversion_M.i";
connectAttr "GlobalHead_unitConversion_M.o" "GlobalHead_reverse_M.ix";
connectAttr "IKLeg_L.swivel" "unitConversion7.i";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vx";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vy";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegUnitConversion_L.i";
connectAttr "FKIKBlendLegUnitConversion_L.o" "FKIKBlendLegReverse_L.ix";
connectAttr "FKIKLeg_L.autoVis" "FKIKBlendLegCondition_L.ft";
connectAttr "FKIKLeg_L.IKVis" "FKIKBlendLegCondition_L.ctr";
connectAttr "FKIKLeg_L.FKVis" "FKIKBlendLegCondition_L.ctg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegCondition_L.cfr";
connectAttr "FKIKBlendLegsetRange_L.ox" "FKIKBlendLegCondition_L.cfg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegsetRange_L.vx";
connectAttr "IKLeg_L.toe" "IKLiftToeLegUnitConversion_L.i";
connectAttr "IKLeg_L.rollAngle" "Leg_LAngleReverse.i1x";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vx";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vy";
connectAttr "IKLeg_L.roll" "IKRollAngleLeg_L.vz";
connectAttr "Leg_LAngleReverse.ox" "IKRollAngleLeg_L.nx";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.my";
connectAttr "IKLeg_L.rollAngle" "IKRollAngleLeg_L.mz";
connectAttr "IKRollAngleLeg_L.ox" "unitConversion8.i";
connectAttr "IKRollAngleLeg_L.oy" "unitConversion9.i";
connectAttr "IKRollAngleLeg_L.oz" "unitConversion10.i";
connectAttr "unitConversion11.o" "IKBallToFKBallMiddleToe1blendTwoAttr_L.i[1]";
connectAttr "FKIKBlendLegUnitConversion_L.o" "IKBallToFKBallMiddleToe1blendTwoAttr_L.ab"
		;
connectAttr "IKXMiddleToe1_L.rx" "unitConversion11.i";
connectAttr "IKBallToFKBallMiddleToe1blendTwoAttr_L.o" "unitConversion12.i";
connectAttr "FKAnkle_L.s" "ScaleBlendAnkle_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendAnkle_L.b";
connectAttr "FKKnee_L.s" "ScaleBlendKnee_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendKnee_L.b";
connectAttr "FKHip_L.s" "ScaleBlendHip_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendHip_L.b";
connectAttr "Center_M.CenterBtwFeet" "CenterBtwFeet_M.vx";
connectAttr "Center_M.CenterBtwFeet" "PelvisCenterBtwFeetUnitConversion_M.i";
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwFeetReverse_M.ix"
		;
connectAttr "irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "BPRig_irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ikSCsolver.msg" ":ikSystem.sol" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
// End of Colossus__rig.ma
