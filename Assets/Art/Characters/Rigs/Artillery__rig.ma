//Maya ASCII 2013 scene
//Name: Artillery__rig.ma
//Last modified: Mon, Apr 28, 2014 01:17:39 PM
//Codeset: UTF-8
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -19.183689587434774 6.3276155868842663 12.288946412703677 ;
	setAttr ".r" -type "double3" -13.538352729754594 -54.199999999999719 0 ;
	setAttr ".rp" -type "double3" -3.5527136788005009e-15 8.8817841970012523e-16 -1.7763568394002505e-15 ;
	setAttr ".rpt" -type "double3" 8.0561384561717429e-17 -2.679750588218649e-16 2.9873205679369792e-17 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 23.212904739752251;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -6.5641223007949918 2.6857760914601534 -0.53952481517920825 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.72088652048480428 100.1 -2.297825784045314 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 21.196691130875198;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "ctrl_rig";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Main" -p "ctrl_rig";
	addAttr -ci true -sn "height" -ln "height" -at "double";
	addAttr -ci true -sn "fkVis" -ln "fkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "ikVis" -ln "ikVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fkIkVis" -ln "fkIkVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimVis" -ln "aimVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "eyeVis" -ln "eyeVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fingerVis" -ln "fingerVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "bendVis" -ln "bendVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "arrowVis" -ln "arrowVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "jointVis" -ln "jointVis" -dv 1 -min 0 -max 1 -at "bool";
	setAttr -l on ".height" 10.340256214141849;
	setAttr -cb on ".fkVis";
	setAttr -cb on ".ikVis";
	setAttr -cb on ".fkIkVis";
	setAttr -cb on ".aimVis";
	setAttr -cb on ".eyeVis";
	setAttr -cb on ".fingerVis";
	setAttr -cb on ".bendVis";
	setAttr -cb on ".arrowVis";
	setAttr -cb on ".jointVis";
createNode nurbsCurve -n "MainShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		5.9928279660000001 -0.24952640810000001 -5.938955226
		-1.3561676610000001e-15 -0.24952640810000001 -8.4212658460000007
		-5.9928279660000001 -0.24952640810000001 -5.938955226
		-8.4751385849999998 -0.24952640810000001 0.053872739330000002
		-5.9928279660000001 -0.24952640810000001 6.0467007050000001
		-2.942980343e-15 -0.24952640810000001 8.5290113250000008
		5.9928279660000001 -0.24952640810000001 6.0467007050000001
		8.4751385849999998 -0.24952640810000001 0.053872739330000002
		5.9928279660000001 -0.24952640810000001 -5.938955226
		-1.3561676610000001e-15 -0.24952640810000001 -8.4212658460000007
		-5.9928279660000001 -0.24952640810000001 -5.938955226
		;
createNode transform -n "BaseSkeleton" -p "Main";
	setAttr -l on ".v" no;
createNode transform -n "BaseSkeletonOffset" -p "BaseSkeleton";
createNode joint -n "Pelvis" -p "BaseSkeletonOffset";
	addAttr -ci true -k true -sn "freeOrient" -ln "freeOrient" -dv 1 -min 1 -max 1 
		-at "long";
	setAttr ".t" -type "double3" 0 2.3360524237536708 0 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 1;
createNode joint -n "Body" -p "Pelvis";
	setAttr ".t" -type "double3" 7.1525573730468729e-07 2.7414545952892979 -1.7648212909698484 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".otp" -type "string" "Mid";
createNode joint -n "Weapon" -p "Body";
	setAttr ".t" -type "double3" -7.152557373046875e-07 0.94345192130518818 -1.7817480349361012 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "38";
createNode joint -n "Weapon_End" -p "Weapon";
	setAttr ".t" -type "double3" -6.2302109906456059e-17 0.78480521338340203 -4.4408920985006262e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".otp" -type "string" "Chest";
createNode joint -n "Shoulder" -p "Body";
	addAttr -ci true -k true -sn "global" -ln "global" -dv 10 -min 0 -max 10 -at "long";
	addAttr -ci true -sn "globalConnect" -ln "globalConnect" -dv 10 -min 0 -max 10 -at "long";
	setAttr ".t" -type "double3" -5.6343156692574441 0.20712480712431525 -0.68573384593890574 ;
	setAttr ".r" -type "double3" -104.46853508350887 -68.91410736675563 55.145881240606435 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -4.1293130717023521e-07 0 0 ;
	setAttr -k on ".global";
createNode transform -n "Shoulder_globalLocator" -p "Shoulder";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".r" -type "double3" -1.9878466759146984e-15 1.5902773407317584e-15 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "Shoulder_globalLocatorShape" -p "Shoulder_globalLocator";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
createNode joint -n "Elbow" -p "Shoulder";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 3.0206925677459955 1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.46684350781504635 -1.3269620535625186 109.38457411993305 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "22";
createNode joint -n "MiddleFinger1" -p "Elbow";
	setAttr ".t" -type "double3" 0.2976567277881037 2.6683615851300639 -0.023725028499105072 ;
	setAttr ".r" -type "double3" 119.14462697885564 58.101273521025348 -119.8100527067289 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -2.490303168013669e-17 3.8068719241856415 -4.0949047407001542 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "21";
createNode joint -n "MiddleFinger2" -p "MiddleFinger1";
	setAttr ".t" -type "double3" 0 0.69033865096549851 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -12.640650171656279 -2.0329840949392182 5.6096029658917468 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0 0 2.5199999009299203 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "20";
createNode joint -n "MiddleFinger3_End" -p "MiddleFinger2";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0.13248348336405691 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.6712936928302726 -2.2571645175857493e-05 0.00070428586596252068 ;
	setAttr ".pa" -type "double3" 0 0 3.6712939054552742 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "19";
createNode joint -n "IndexFinger1" -p "Elbow";
	setAttr ".t" -type "double3" 0.048771924557457533 2.7288818338408771 0.28520562099039459 ;
	setAttr ".r" -type "double3" 35.442060902999408 -67.525450463813002 8.2444186438951323 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" 0.065532877363568762 20.527688987272207 -2.5422327562497964 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "8";
createNode joint -n "IndexFinger2" -p "IndexFinger1";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0.59426302015667032 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -3.9153768875785775 4.4420638825440086 2.7018762175592053 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "7";
createNode joint -n "IndexFinger3_End" -p "IndexFinger2";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0.13732743200941844 -4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.7599462155480898 0.0012471107435520262 -0.024789858678763422 ;
	setAttr ".pa" -type "double3" 0 0 5.7600000490223469 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "6";
createNode joint -n "ThumbFinger1" -p "Elbow";
	setAttr ".t" -type "double3" -0.11739851835680171 2.7357413095711296 -0.14142536441553677 ;
	setAttr ".r" -type "double3" 41.262693281878839 -72.731377870867576 54.456809846176924 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".pa" -type "double3" -34.462082586865911 -8.7285733235282201 -1.7903981777634761 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "4";
createNode joint -n "ThumbFinger2" -p "ThumbFinger1";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0.6384728589777493 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 0.014345738093218962 0.10149567043938315 -4.0834181390513358 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "3";
createNode joint -n "ThumbFinger3_End" -p "ThumbFinger2";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0.12953755254568577 -4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.6697912077683459e-13 -4.3732626870123352e-15 -3.1805546814635168e-15 ;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "2";
createNode joint -n "Track" -p "Pelvis";
	setAttr ".t" -type "double3" -1.9369647648037842 0 0 ;
	setAttr ".r" -type "double3" 51.580167854041854 0.0012779026285295334 179.99838883409495 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 2;
createNode joint -n "TrackBend" -p "Track";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0.77347563476454995 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" -104.50683097258131 -0.00086172398720417182 -0.00066713583840531578 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "TrackBottom" -p "TrackBend";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0.75952924543354583 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 52.926663114004469 -0.0013444510459025495 0.0027008789339559997 ;
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".dl" yes;
	setAttr ".typ" 4;
createNode joint -n "Wheel1" -p "TrackBottom";
	setAttr ".t" -type "double3" 1.8839142986457333 0.5176425576256205 4.9243162660155706 ;
	setAttr ".r" -type "double3" -1.6223190103418797e-10 0.0020108491719727671 -90.000000000000043 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.2590588351589583e-14 -0.0020645926262348024 1.4033132660856815e-10 ;
createNode joint -n "Wheel1_End" -p "Wheel1";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 1.4116922076074299 0 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -9.5416523516743185e-14 -1.7695238050068128e-10 -90.00000000014029 ;
createNode joint -n "Wheel2" -p "TrackBottom";
	setAttr ".t" -type "double3" 1.8372944138814673 0.2600182890937055 3.2643206664076949 ;
	setAttr ".r" -type "double3" -1.6234004455237336e-10 0.0020108491720809102 -90.000000000000043 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.2590588351589583e-14 -0.0020645926262348024 1.4033132660856815e-10 ;
createNode joint -n "Wheel2_End" -p "Wheel2";
	setAttr ".t" -type "double3" 0 1.4116922076074307 -4.4408920985006262e-16 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -9.5416911768311627e-14 -1.7684423737074717e-10 -90.00000000014029 ;
createNode joint -n "Wheel3" -p "TrackBottom";
	setAttr ".t" -type "double3" 1.7610100589386404 4.3141046290884333e-12 1.0466412134814318 ;
	setAttr ".r" -type "double3" -1.6231300847870137e-10 0.0020108491720538736 -90.000000000000043 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.2590588351589583e-14 -0.0020645926262348024 1.4033132660856815e-10 ;
createNode joint -n "Wheel3_End" -p "Wheel3";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 1.4116922076074307 0 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -9.5416911768245426e-14 -1.7687127363854482e-10 -90.00000000014029 ;
createNode joint -n "Wheel4" -p "TrackBottom";
	setAttr ".t" -type "double3" 1.7610124743337159 4.312106227644108e-12 -1.5284062198937476 ;
	setAttr ".r" -type "double3" 1.8963193623200714e-11 0.0020108491720268362 -90.000000000000028 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.2590588351589583e-14 -0.0020645926262348024 1.4033132660856815e-10 ;
createNode joint -n "Wheel4_End" -p "Wheel4";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 1.4116922076074312 0 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -9.5416329391223736e-14 -1.768983110710964e-10 -90.00000000014029 ;
createNode joint -n "Wheel5" -p "TrackBottom";
	setAttr ".t" -type "double3" 1.8373009803280893 0.26001828909369995 -3.7361537865616383 ;
	setAttr ".r" -type "double3" -1.6230399541880722e-10 0.0020108491720448609 -90.000000000000043 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.2590588351589583e-14 -0.0020645926262348024 1.4033132660856815e-10 ;
createNode joint -n "Wheel5_End" -p "Wheel5";
	setAttr ".t" -type "double3" 0 1.4116922076074316 0 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -9.5416523516919713e-14 -1.7688028650431332e-10 -90.00000000014029 ;
createNode joint -n "Wheel6" -p "TrackBottom";
	setAttr ".t" -type "double3" 1.8839239978084041 0.51764255762561207 -5.4159399481217267 ;
	setAttr ".r" -type "double3" -1.6237609213293434e-10 0.0020108491721169573 -90.000000000000028 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.2590588351589583e-14 -0.0020645926262348024 1.4033132660856815e-10 ;
createNode joint -n "Wheel6_End" -p "Wheel6";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 1.4116922076074307 8.8817841970012523e-16 ;
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -9.5416329391444403e-14 -1.7680819017843755e-10 -90.00000000014029 ;
createNode transform -n "MotionSystem" -p "Main";
createNode transform -n "FKSystem" -p "MotionSystem";
createNode transform -n "FKParentConstraintToTrackBottom_R" -p "FKSystem";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000011 1 1 ;
createNode joint -n "FKOffsetWheel1_R" -p "FKParentConstraintToTrackBottom_R";
	setAttr ".t" -type "double3" 1.8839142986457338 0.51764255762562261 4.9243162660155688 ;
	setAttr ".r" -type "double3" -1.6223190685795757e-10 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel1_R" -p "FKOffsetWheel1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
createNode transform -n "FKWheel1_R" -p "FKExtraWheel1_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel1_RShape" -p "FKWheel1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.520426718 4.3070183569999996e-16 -1.520426718
		-2.1502080860000001 4.7744210500000004e-16 0
		-1.520426718 2.4450326429999997e-16 1.520426718
		-6.2307650939999999e-16 -1.3166227249999999e-16 2.1502080860000001
		1.520426718 -4.3070183569999996e-16 1.520426718
		2.1502080860000001 -4.7744210500000004e-16 0
		1.520426718 -2.4450326429999997e-16 -1.520426718
		1.1548809579999999e-15 1.3166227249999999e-16 -2.1502080860000001
		-1.520426718 4.3070183569999996e-16 -1.520426718
		-2.1502080860000001 4.7744210500000004e-16 0
		-1.520426718 2.4450326429999997e-16 1.520426718
		;
createNode joint -n "FKXWheel1_R" -p "FKWheel1_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel1_End_R" -p "FKXWheel1_R";
	setAttr ".t" -type "double3" 3.3306690738754696e-16 1.4116922076074263 4.3609560407276149e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 7.0167061655942429e-15 5.4597839610252525e-20 -90 ;
createNode joint -n "FKOffsetWheel2_R" -p "FKParentConstraintToTrackBottom_R";
	setAttr ".t" -type "double3" 1.8372944138814673 0.26001828909370683 3.2643206664076945 ;
	setAttr ".r" -type "double3" -1.6234004571712733e-10 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel2_R" -p "FKOffsetWheel2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -8.8817841970012523e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999956 1 ;
createNode transform -n "FKWheel2_R" -p "FKExtraWheel2_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel2_RShape" -p "FKWheel2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.520426718 1.644599071e-15 -1.520426718
		-2.1502080860000001 1.6913393400000001e-15 -1.213897235e-15
		-1.520426718 1.458400499e-15 1.520426718
		-6.2307650939999999e-16 1.082234962e-15 2.1502080860000001
		1.520426718 7.8319539929999996e-16 1.520426718
		2.1502080860000001 7.3645512999999993e-16 -1.213897235e-15
		1.520426718 9.6939397059999995e-16 -1.520426718
		1.1548809579999999e-15 1.3455595069999998e-15 -2.1502080860000001
		-1.520426718 1.644599071e-15 -1.520426718
		-2.1502080860000001 1.6913393400000001e-15 -1.213897235e-15
		-1.520426718 1.458400499e-15 1.520426718
		;
createNode joint -n "FKXWheel2_R" -p "FKWheel2_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel2_End_R" -p "FKXWheel2_R";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.4116922076074281 4.3569592378389643e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 7.0167061655942429e-15 4.8531412986977678e-20 -90 ;
createNode joint -n "FKOffsetWheel3_R" -p "FKParentConstraintToTrackBottom_R";
	setAttr ".t" -type "double3" 1.7610100589386399 4.3149928075081334e-12 1.0466412134814311 ;
	setAttr ".r" -type "double3" -1.6231301100233489e-10 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel3_R" -p "FKOffsetWheel3_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 -4.4408920985006262e-16 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1 ;
createNode transform -n "FKWheel3_R" -p "FKExtraWheel3_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel3_RShape" -p "FKWheel3_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.520426718 4.3070183569999996e-16 -1.520426718
		-2.1502080860000001 4.7744210500000004e-16 0
		-1.520426718 2.4450326429999997e-16 1.520426718
		-1.2300251269999999e-15 -1.3166227249999999e-16 2.1502080860000001
		1.520426718 -4.3070183569999996e-16 1.520426718
		2.1502080860000001 -4.7744210500000004e-16 0
		1.520426718 -2.4450326429999997e-16 -1.520426718
		5.4793234019999999e-16 1.3166227249999999e-16 -2.1502080860000001
		-1.520426718 4.3070183569999996e-16 -1.520426718
		-2.1502080860000001 4.7744210500000004e-16 0
		-1.520426718 2.4450326429999997e-16 1.520426718
		;
createNode joint -n "FKXWheel3_R" -p "FKWheel3_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel3_End_R" -p "FKXWheel3_R";
	setAttr ".t" -type "double3" 6.6613381477509392e-16 1.4116922076074285 4.3578474162586645e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 7.0167091988075536e-15 3.0332133117153172e-20 -90 ;
createNode joint -n "FKOffsetWheel4_R" -p "FKParentConstraintToTrackBottom_R";
	setAttr ".t" -type "double3" 1.7610124743337146 4.3114400938293329e-12 -1.528406219893748 ;
	setAttr ".r" -type "double3" 1.8963193817326368e-11 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel4_R" -p "FKOffsetWheel4_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 0 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999911 1 ;
createNode transform -n "FKWheel4_R" -p "FKExtraWheel4_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel4_RShape" -p "FKWheel4_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.520426718 -7.8319539929999996e-16 -1.520426718
		-2.1502080860000001 -7.3645512999999993e-16 6.0694861749999998e-16
		-1.520426718 -9.6939397059999995e-16 1.520426718
		-1.2300251269999999e-15 -1.3455595069999998e-15 2.1502080860000001
		1.520426718 -1.644599071e-15 1.520426718
		2.1502080860000001 -1.6913393400000001e-15 6.0694861749999998e-16
		1.520426718 -1.458400499e-15 -1.520426718
		5.4793234019999999e-16 -1.082234962e-15 -2.1502080860000001
		-1.520426718 -7.8319539929999996e-16 -1.520426718
		-2.1502080860000001 -7.3645512999999993e-16 6.0694861749999998e-16
		-1.520426718 -9.6939397059999995e-16 1.520426718
		;
createNode joint -n "FKXWheel4_R" -p "FKWheel4_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel4_End_R" -p "FKXWheel4_R";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 1.4116922076074294 4.3587355946783646e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 7.0167061655942429e-15 2.4265706493878337e-20 -90 ;
createNode joint -n "FKOffsetWheel5_R" -p "FKParentConstraintToTrackBottom_R";
	setAttr ".t" -type "double3" 1.8373009803280866 0.2600182890936984 -3.7361537865616379 ;
	setAttr ".r" -type "double3" -1.6230399114804293e-10 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel5_R" -p "FKOffsetWheel5_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999889 1 ;
createNode transform -n "FKWheel5_R" -p "FKExtraWheel5_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel5_RShape" -p "FKWheel5_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.520426718 1.644599071e-15 -1.520426718
		-2.1502080860000001 1.6913393400000001e-15 0
		-1.520426718 1.458400499e-15 1.520426718
		-6.2307650939999999e-16 1.082234962e-15 2.1502080860000001
		1.520426718 7.8319539929999996e-16 1.520426718
		2.1502080860000001 7.3645512999999993e-16 0
		1.520426718 9.6939397059999995e-16 -1.520426718
		1.1548809579999999e-15 1.3455595069999998e-15 -2.1502080860000001
		-1.520426718 1.644599071e-15 -1.520426718
		-2.1502080860000001 1.6913393400000001e-15 0
		-1.520426718 1.458400499e-15 1.520426718
		;
createNode joint -n "FKXWheel5_R" -p "FKWheel5_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel5_End_R" -p "FKXWheel5_R";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.4116922076074307 4.3582915054685145e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 7.0167061655942429e-15 6.0664266240538392e-21 -90 ;
createNode joint -n "FKOffsetWheel6_R" -p "FKParentConstraintToTrackBottom_R";
	setAttr ".t" -type "double3" 1.883923997808401 0.51764255762561007 -5.4159399481217267 ;
	setAttr ".r" -type "double3" -1.6237608786217e-10 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel6_R" -p "FKOffsetWheel6_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 -4.4408920985006262e-16 8.8817841970012523e-16 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999867 1 ;
createNode transform -n "FKWheel6_R" -p "FKExtraWheel6_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel6_RShape" -p "FKWheel6_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.520426718 -0.1039196607 -1.520426718
		-2.1502080860000001 -0.1039196607 -1.1503330949999999e-17
		-1.520426718 -0.1039196607 1.520426718
		-6.0687922440000004e-16 -0.1039196607 2.1502080860000001
		1.520426718 -0.1039196607 1.520426718
		2.1502080860000001 -0.1039196607 -1.1503330949999999e-17
		1.520426718 -0.1039196607 -1.520426718
		1.171078243e-15 -0.1039196607 -2.1502080860000001
		-1.520426718 -0.1039196607 -1.520426718
		-2.1502080860000001 -0.1039196607 -1.1503330949999999e-17
		-1.520426718 -0.1039196607 1.520426718
		;
createNode joint -n "FKXWheel6_R" -p "FKWheel6_R";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel6_End_R" -p "FKXWheel6_R";
	setAttr ".t" -type "double3" 1.1102230246251565e-16 1.4116922076074312 4.3565151486291143e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 7.0167061655942429e-15 -6.0664266224958183e-21 -90 ;
createNode parentConstraint -n "FKParentConstraintToTrackBottom_R_parentConstraint1" 
		-p "FKParentConstraintToTrackBottom_R";
	addAttr -ci true -k true -sn "w0" -ln "TrackBottom_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 179.99999992598796 -180.00005374334847 -360.00000000000188 ;
	setAttr ".rst" -type "double3" -1.937 1.3975278139114389 -1.8873791418627661e-15 ;
	setAttr ".rsrr" -type "double3" -5.0888874903416268e-14 -5.3743277289625299e-05 
		-179.99999999999997 ;
	setAttr -k on ".w0";
createNode transform -n "FKParentConstraintToTrackBottom_L" -p "FKSystem";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000011 1 1.0000000000000011 ;
createNode joint -n "FKOffsetWheel1_L" -p "FKParentConstraintToTrackBottom_L";
	setAttr ".t" -type "double3" -1.8839142986457382 -0.51764255762562206 -4.9243162660155662 ;
	setAttr ".r" -type "double3" -1.6223190685795757e-10 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel1_L" -p "FKOffsetWheel1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 0 0 ;
	setAttr ".r" -type "double3" -7.0167092985348736e-15 7.016709298534876e-15 -1.2722218725854067e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999978 0.99999999999999978 ;
createNode transform -n "FKWheel1_L" -p "FKExtraWheel1_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel1_LShape" -p "FKWheel1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.5204267250000001 6.4854788210000002e-12 1.5204267220000001
		2.1502080960000001 9.68958247e-12 5.2005137969999998e-09
		1.5204267309999999 1.426680996e-11 -1.5204267140000001
		1.380128545e-08 1.753663881e-11 -2.150208085
		-1.5204267060000001 1.7582824090000001e-11 -1.5204267199999999
		-2.1502080760000002 1.437872044e-11 -3.1901716819999999e-09
		-1.5204267119999999 9.8010488610000003e-12 1.520426716
		5.4105974150000001e-09 6.5316640980000001e-12 2.1502080870000002
		1.5204267250000001 6.4854788210000002e-12 1.5204267220000001
		2.1502080960000001 9.68958247e-12 5.2005137969999998e-09
		1.5204267309999999 1.426680996e-11 -1.5204267140000001
		;
createNode joint -n "FKXWheel1_L" -p "FKWheel1_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel1_End_L" -p "FKXWheel1_L";
	setAttr ".t" -type "double3" -7.7715611723760958e-16 -1.4116922076074272 -4.354738791789714e-12 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 180 -7.0167141545309611e-15 -90.000000000000014 ;
createNode joint -n "FKOffsetWheel2_L" -p "FKParentConstraintToTrackBottom_L";
	setAttr ".t" -type "double3" -1.8372944138814704 -0.26001828909370639 -3.2643206664076909 ;
	setAttr ".r" -type "double3" -1.6234004571712733e-10 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel2_L" -p "FKOffsetWheel2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 0 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -7.0167092985348736e-15 7.016709298534876e-15 -1.2722218725854067e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999956 0.99999999999999956 ;
createNode transform -n "FKWheel2_L" -p "FKExtraWheel2_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel2_LShape" -p "FKWheel2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.5204267220000001 2.5166535520000002e-12 1.5204267220000001
		2.1502080920000002 5.7212012899999998e-12 4.697974454e-09
		1.5204267279999999 1.0298872869999999e-11 -1.5204267149999999
		1.0562463129999999e-08 1.3568257629999999e-11 -2.150208085
		-1.5204267090000001 1.3614442909999999e-11 -1.520426721
		-2.1502080800000001 1.0409895170000001e-11 -3.6927114699999999e-09
		-1.5204267149999999 5.8322235930000002e-12 1.520426716
		2.171774982e-09 2.56283883e-12 2.1502080860000001
		1.5204267220000001 2.5166535520000002e-12 1.5204267220000001
		2.1502080920000002 5.7212012899999998e-12 4.697974454e-09
		1.5204267279999999 1.0298872869999999e-11 -1.5204267149999999
		;
createNode joint -n "FKXWheel2_L" -p "FKWheel2_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel2_End_L" -p "FKXWheel2_L";
	setAttr ".t" -type "double3" -6.6613381477509392e-16 -1.4116922076074281 -4.3534065241601638e-12 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 180 -7.0167141545309611e-15 -90.000000000000014 ;
createNode joint -n "FKOffsetWheel3_L" -p "FKParentConstraintToTrackBottom_L";
	setAttr ".t" -type "double3" -1.7610100589386408 -4.3149928075081334e-12 -1.0466412134814289 ;
	setAttr ".r" -type "double3" -1.6231301100233489e-10 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel3_L" -p "FKOffsetWheel3_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" -7.0167092985348736e-15 7.016709298534876e-15 -1.2722218725854067e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999933 0.99999999999999933 ;
createNode transform -n "FKWheel3_L" -p "FKExtraWheel3_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel3_LShape" -p "FKWheel3_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.520426718 -2.8710367419999998e-12 1.520426721
		2.1502080879999999 3.3306690739999996e-13 4.1908378989999996e-09
		1.5204267229999999 4.9107384830000002e-12 -1.5204267149999999
		6.2355578479999998e-09 8.1801232450000013e-12 -2.1502080860000001
		-1.520426713 8.2263085229999995e-12 -1.520426721
		-2.150208084 5.0222048739999998e-12 -4.1998482470000004e-09
		-1.520426719 4.4453329909999999e-13 1.5204267149999999
		-2.1551302959999997e-09 -2.824851464e-12 2.1502080860000001
		1.520426718 -2.8710367419999998e-12 1.520426721
		2.1502080879999999 3.3306690739999996e-13 4.1908378989999996e-09
		1.5204267229999999 4.9107384830000002e-12 -1.5204267149999999
		;
createNode joint -n "FKXWheel3_L" -p "FKWheel3_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel3_End_L" -p "FKXWheel3_L";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -1.4116922076074285 -4.3567371932340393e-12 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 180 -7.0167141545309595e-15 -90.000000000000014 ;
createNode joint -n "FKOffsetWheel4_L" -p "FKParentConstraintToTrackBottom_L";
	setAttr ".t" -type "double3" -1.761012474333713 -4.311884183039183e-12 1.528406219893748 ;
	setAttr ".r" -type "double3" 1.8963193817326368e-11 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel4_L" -p "FKOffsetWheel4_L";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" -7.0167092985348736e-15 7.016709298534876e-15 -1.2722218725854067e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999911 0.99999999999999911 ;
createNode transform -n "FKWheel4_L" -p "FKExtraWheel4_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel4_LShape" -p "FKWheel4_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.520426713 -9.4586560810000008e-12 1.520426721
		2.1502080829999999 -6.2545524319999994e-12 4.1908359010000001e-09
		1.520426718 -1.6764367670000001e-12 -1.5204267149999999
		1.211296619e-09 1.592947996e-12 -2.1502080860000001
		-1.520426718 1.639133274e-12 -1.520426721
		-2.1502080889999999 -1.565414465e-12 -4.1998502450000007e-09
		-1.520426724 -6.1435301290000005e-12 1.5204267149999999
		-7.1793915259999998e-09 -9.4124708029999993e-12 2.1502080860000001
		1.520426713 -9.4586560810000008e-12 1.520426721
		2.1502080829999999 -6.2545524319999994e-12 4.1908359010000001e-09
		1.520426718 -1.6764367670000001e-12 -1.5204267149999999
		;
createNode joint -n "FKXWheel4_L" -p "FKWheel4_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel4_End_L" -p "FKXWheel4_L";
	setAttr ".t" -type "double3" -6.6613381477509392e-16 -1.4116922076074285 -4.3589576392832896e-12 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 180 -7.0167141545309595e-15 -90.000000000000014 ;
createNode joint -n "FKOffsetWheel5_L" -p "FKParentConstraintToTrackBottom_L";
	setAttr ".t" -type "double3" -1.8373009803280835 -0.26001828909369884 3.7361537865616357 ;
	setAttr ".r" -type "double3" -1.6230399114804293e-10 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel5_L" -p "FKOffsetWheel5_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 -4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" -7.0167092985348736e-15 7.016709298534876e-15 -1.2722218725854067e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999889 0.99999999999999889 ;
createNode transform -n "FKWheel5_L" -p "FKExtraWheel5_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel5_LShape" -p "FKWheel5_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.520426708 -1.539612882e-11 1.5204267220000001
		2.150208079 -1.2191581080000001e-11 4.6979709010000001e-09
		1.5204267140000001 -7.6139095029999998e-12 -1.5204267149999999
		-3.0963978050000002e-09 -4.3445247399999996e-12 -2.150208085
		-1.5204267229999999 -4.2983394620000006e-12 -1.520426721
		-2.1502080929999998 -7.5028871999999993e-12 -3.6927154670000003e-09
		-1.520426729 -1.2080558779999999e-11 1.520426716
		-1.1487085729999999e-08 -1.5349943539999999e-11 2.1502080860000001
		1.520426708 -1.539612882e-11 1.5204267220000001
		2.150208079 -1.2191581080000001e-11 4.6979709010000001e-09
		1.5204267140000001 -7.6139095029999998e-12 -1.5204267149999999
		;
createNode joint -n "FKXWheel5_L" -p "FKWheel5_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel5_End_L" -p "FKXWheel5_L";
	setAttr ".t" -type "double3" -1.5543122344752192e-15 -1.4116922076074303 -4.3600678623079148e-12 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 180 -7.0167141545309595e-15 -90.000000000000014 ;
createNode joint -n "FKOffsetWheel6_L" -p "FKParentConstraintToTrackBottom_L";
	setAttr ".t" -type "double3" -1.8839239978083961 -0.51764255762561073 5.4159399481217214 ;
	setAttr ".r" -type "double3" -1.6237608786217e-10 0.0020108491296890766 -90.000002504478161 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 1.2590588642678922e-14 -0.0020645926799466273 1.4033132791581981e-10 ;
createNode transform -n "FKExtraWheel6_L" -p "FKOffsetWheel6_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 -4.4408920985006262e-16 -1.7763568394002505e-15 ;
	setAttr ".r" -type "double3" -7.0167092985348736e-15 7.016709298534876e-15 -1.2722218725854067e-14 ;
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1 0.99999999999999867 0.99999999999999867 ;
createNode transform -n "FKWheel6_L" -p "FKExtraWheel6_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 2;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWheel6_LShape" -p "FKWheel6_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.520426705 0.1039196607 1.5204267220000001
		2.1502080750000001 0.1039196607 5.2007758100000001e-09
		1.520426711 0.1039196607 -1.5204267140000001
		-6.3738221370000006e-09 0.1039196607 -2.150208085
		-1.520426726 0.1039196607 -1.5204267199999999
		-2.1502080970000002 0.1039196607 -3.1899105579999999e-09
		-1.520426732 0.1039196607 1.520426716
		-1.476451028e-08 0.1039196607 2.1502080870000002
		1.520426705 0.1039196607 1.5204267220000001
		2.1502080750000001 0.1039196607 5.2007758100000001e-09
		1.520426711 0.1039196607 -1.5204267140000001
		;
createNode joint -n "FKXWheel6_L" -p "FKWheel6_L";
	setAttr ".ro" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWheel6_End_L" -p "FKXWheel6_L";
	setAttr ".t" -type "double3" -6.6613381477509392e-16 -1.4116922076074307 -4.3565151486291143e-12 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 180 -7.0167141545309595e-15 -90.000000000000014 ;
createNode parentConstraint -n "FKParentConstraintToTrackBottom_L_parentConstraint1" 
		-p "FKParentConstraintToTrackBottom_L";
	addAttr -ci true -k true -sn "w0" -ln "TrackBottom_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -4.4510510584004817e-14 180.00005374334845 -3.6791015479302323e-11 ;
	setAttr ".rst" -type "double3" 1.9370000000000007 1.3975278139114393 -1.9984014443252818e-15 ;
	setAttr ".rsrr" -type "double3" -3.8166656844774009e-14 -179.99994625672275 1.4226218095157087e-15 ;
	setAttr -k on ".w0";
createNode transform -n "PelvisCenterBtwLegsBlended_M" -p "FKSystem";
	setAttr ".ro" 3;
createNode transform -n "CenterOffset_M" -p "PelvisCenterBtwLegsBlended_M";
createNode transform -n "CenterExtra_M" -p "CenterOffset_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "Center_M" -p "CenterExtra_M";
	addAttr -ci true -k true -sn "CenterBtwFeet" -ln "CenterBtwFeet" -min 0 -max 10 
		-at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".smd" 4;
	setAttr -k on ".CenterBtwFeet";
createNode nurbsCurve -n "Center_MShape" -p "Center_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 46 0 no 3
		47 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46
		47
		-3.5135543770000002 0 0
		-3.7372301760000002 1.528565836e-16 0.68840485309999999
		-4.3228225729999998 2.4732750970000002e-16 1.113864081
		-5.0466557659999998 2.4732750970000002e-16 1.113864081
		-5.6322481619999998 1.528565836e-16 0.68840485309999999
		-5.8559239610000002 0 0
		-5.6322481619999998 -1.528565836e-16 -0.68840485309999999
		-5.0466557659999998 -2.4732750970000002e-16 -1.113864081
		-4.3228225729999998 -2.4732750970000002e-16 -1.113864081
		-3.7372301760000002 -1.528565836e-16 -0.68840485309999999
		-3.5135543770000002 0 0
		0 0 0
		3.5135543770000002 0 0
		3.7372301760000002 1.528565836e-16 0.68840485309999999
		4.3228225729999998 2.4732750970000002e-16 1.113864081
		5.0466557659999998 2.4732750970000002e-16 1.113864081
		5.6322481619999998 1.528565836e-16 0.68840485309999999
		5.8559239610000002 0 0
		5.6322481619999998 -1.528565836e-16 -0.68840485309999999
		5.0466557659999998 -2.4732750970000002e-16 -1.113864081
		4.3228225729999998 -2.4732750970000002e-16 -1.113864081
		3.7372301760000002 -1.528565836e-16 -0.68840485309999999
		3.5135543770000002 0 0
		0 0 0
		0 -7.801657935e-16 -3.5135543770000002
		-0.68840485309999999 -8.298317981e-16 -3.7372301760000002
		-1.113864081 -9.5985943030000014e-16 -4.3228225729999998
		-1.113864081 -1.1205826860000002e-15 -5.0466557659999998
		-0.68840485309999999 -1.2506103180000001e-15 -5.6322481619999998
		0 -1.300276322e-15 -5.8559239610000002
		0.68840485309999999 -1.2506103180000001e-15 -5.6322481619999998
		1.113864081 -1.1205826860000002e-15 -5.0466557659999998
		1.113864081 -9.5985943030000014e-16 -4.3228225729999998
		0.68840485309999999 -8.298317981e-16 -3.7372301760000002
		0 -7.801657935e-16 -3.5135543770000002
		0 0 0
		0 7.801657935e-16 3.5135543770000002
		-0.68840485309999999 8.298317981e-16 3.7372301760000002
		-1.113864081 9.5985943030000014e-16 4.3228225729999998
		-1.113864081 1.1205826860000002e-15 5.0466557659999998
		-0.68840485309999999 1.2506103180000001e-15 5.6322481619999998
		0 1.300276322e-15 5.8559239610000002
		0.68840485309999999 1.2506103180000001e-15 5.6322481619999998
		1.113864081 1.1205826860000002e-15 5.0466557659999998
		1.113864081 9.5985943030000014e-16 4.3228225729999998
		0.68840485309999999 8.298317981e-16 3.7372301760000002
		0 7.801657935e-16 3.5135543770000002
		;
createNode joint -n "FKOffsetPelvis_M" -p "Center_M";
	setAttr ".ro" 3;
createNode transform -n "FKExtraPelvis_M" -p "FKOffsetPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode transform -n "FKPelvis_M" -p "FKExtraPelvis_M";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
createNode nurbsCurve -n "FKPelvis_MShape" -p "FKPelvis_M";
	setAttr -l on -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.76941004858139805 2.1795612792395666e-16 -0.76941004858139972
		-1.0881101257299559 2.416089829826342e-16 0
		-0.7694100485813985 1.237305726012548e-16 0.76941004858139883
		-3.1530709207055706e-16 -6.662752912975065e-17 1.0881101257299559
		0.76941004858139828 -2.1795612792395666e-16 0.76941004858139883
		1.0881101257299559 -2.4160898298263425e-16 0
		0.7694100485813985 -1.2373057260125487e-16 -0.76941004858139794
		5.8442607114348448e-16 6.6627529129750588e-17 -1.0881101257299559
		-0.76941004858139805 2.1795612792395666e-16 -0.76941004858139972
		-1.0881101257299559 2.416089829826342e-16 0
		-0.7694100485813985 1.237305726012548e-16 0.76941004858139883
		;
createNode joint -n "FKXPelvis_M" -p "FKPelvis_M";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetBody_M" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 7.1525573730468729e-07 2.7414545952892979 -1.7648212909698484 ;
createNode transform -n "FKExtraBody_M" -p "FKOffsetBody_M";
	setAttr -l on -k off ".v";
createNode transform -n "FKBody_M" -p "FKExtraBody_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKBody_MShape" -p "FKBody_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.6788301579999998 -2.4720090489999999e-14 -6.5406479800000001
		-3.788438298 -2.4703931109999999e-14 -0.7205095426
		-2.6788301579999998 -2.4784464370000002e-14 5.0996288950000004
		8.6264948229999994e-07 -2.4914514959999999e-14 7.5104091710000001
		2.678831883 -2.5017901020000003e-14 5.0996288950000004
		3.7884400230000002 -2.5034060390000001e-14 -0.7205095426
		2.678831883 -2.495352714e-14 -6.5406479800000001
		8.6264948540000003e-07 -2.5711654969999999e-14 -6.7376369650000001
		-2.6788301579999998 -2.4720090489999999e-14 -6.5406479800000001
		-3.788438298 -2.4703931109999999e-14 -0.7205095426
		-2.6788301579999998 -2.4784464370000002e-14 5.0996288950000004
		;
createNode joint -n "FKXBody_M" -p "FKBody_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetWeapon_M" -p "FKXBody_M";
	setAttr ".t" -type "double3" -7.152557373046875e-07 0.94345192130518818 -1.7817480349361012 ;
createNode transform -n "FKExtraWeapon_M" -p "FKOffsetWeapon_M";
	setAttr -l on -k off ".v";
createNode transform -n "FKWeapon_M" -p "FKExtraWeapon_M";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKWeapon_MShape" -p "FKWeapon_M";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-5.3782120979999997 -7.7190948620000006e-14 -2.021923455
		-7.6059404900000001 -7.7182204650000004e-14 0
		-5.3782120979999997 -7.7225781869999994e-14 2.021923455
		-2.1643168220000001e-15 -7.7296153339999998e-14 2.8594315720000001
		5.3782120979999997 -7.7352096409999999e-14 2.021923455
		7.6059404900000001 -7.7360840380000001e-14 0
		5.3782120979999997 -7.7317263159999999e-14 -2.021923455
		4.1248599219999996e-15 -7.7246891690000008e-14 -2.8594315720000001
		-5.3782120979999997 -7.7190948620000006e-14 -2.021923455
		-7.6059404900000001 -7.7182204650000004e-14 0
		-5.3782120979999997 -7.7225781869999994e-14 2.021923455
		;
createNode joint -n "FKXWeapon_M" -p "FKWeapon_M";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXWeapon_End_M" -p "FKXWeapon_M";
	setAttr ".t" -type "double3" -6.2302109906456059e-17 0.78480521338340203 -4.4408920985006262e-16 ;
createNode joint -n "FKOffsetShoulder_R" -p "FKXBody_M";
	setAttr ".t" -type "double3" -5.6343156692574441 0.20712480712431525 -0.68573384593890574 ;
	setAttr ".r" -type "double3" -104.46853565728875 -68.914108876192429 55.145881849172426 ;
	setAttr ".ro" 5;
createNode transform -n "FKGlobalStaticShoulder_R" -p "FKOffsetShoulder_R";
	setAttr ".t" -type "double3" 0 0 -1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "FKGlobalShoulder_R" -p "FKGlobalStaticShoulder_R";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -4.4408920985006262e-16 1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
createNode transform -n "FKExtraShoulder_R" -p "FKGlobalShoulder_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 4.4408920985006262e-16 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000004 ;
createNode transform -n "FKShoulder_R" -p "FKExtraShoulder_R";
	addAttr -ci true -k true -sn "Global" -ln "Global" -dv 10 -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 0 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999956 0.99999999999999933 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".Global";
createNode nurbsCurve -n "FKShoulder_RShape" -p "FKShoulder_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.72245188790000003 2.0465396359999998e-16 -0.72245188790000003
		-1.021701258 2.268632522e-16 0
		-0.72245188790000003 1.1617912439999999e-16 0.72245188790000003
		-2.9606346360000001e-16 -6.2561158770000004e-17 1.021701258
		0.72245188790000003 -2.0465396359999998e-16 0.72245188790000003
		1.021701258 -2.268632522e-16 0
		0.72245188790000003 -1.1617912439999999e-16 -0.72245188790000003
		5.487577387e-16 6.2561158770000004e-17 -1.021701258
		-0.72245188790000003 2.0465396359999998e-16 -0.72245188790000003
		-1.021701258 2.268632522e-16 0
		-0.72245188790000003 1.1617912439999999e-16 0.72245188790000003
		;
createNode joint -n "FKXShoulder_R" -p "FKShoulder_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow_R" -p "FKXShoulder_R";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 3.0206925677459959 2.6645352591003757e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -0.46684348313122442 -1.3269620015957899 109.3845711425946 ;
createNode transform -n "FKExtraElbow_R" -p "FKOffsetElbow_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 3.0531133177191805e-16 8.8817841970012523e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKElbow_R" -p "FKExtraElbow_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.6367796834847468e-16 8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow_RShape" -p "FKElbow_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.65669205630000005 1.8602571939999998e-16 -0.65669205630000005
		-0.92870281239999997 2.062134491e-16 0
		-0.65669205630000005 1.05604137e-16 0.65669205630000005
		-2.6911484069999998e-16 -5.6866646329999999e-17 0.92870281239999997
		0.65669205630000005 -1.8602571939999998e-16 0.65669205630000005
		0.92870281239999997 -2.062134491e-16 0
		0.65669205630000005 -1.05604137e-16 -0.65669205630000005
		4.9880809220000001e-16 5.6866646329999999e-17 -0.92870281239999997
		-0.65669205630000005 1.8602571939999998e-16 -0.65669205630000005
		-0.92870281239999997 2.062134491e-16 0
		-0.65669205630000005 1.05604137e-16 0.65669205630000005
		;
createNode joint -n "FKXElbow_R" -p "FKElbow_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger1_R" -p "FKXElbow_R";
	setAttr ".t" -type "double3" 0.29765672778810726 2.6683615851300635 -0.023725028499104184 ;
	setAttr ".r" -type "double3" 119.14463142878685 58.101270552091094 -119.81005577848617 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger1_R" -p "FKOffsetMiddleFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 3.3306690738754696e-16 2.2204460492503131e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKMiddleFinger1_R" -p "FKExtraMiddleFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 2.2204460492503131e-16 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger1_RShape" -p "FKMiddleFinger1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.4548693721 2.866151853e-16 -0.4548693721
		-0.64328243500000004 3.0059857349999997e-16 -1.5776117929999999e-16
		-0.4548693721 2.309097533e-16 0.4548693721
		1.075682279e-15 1.183714906e-16 0.64328243500000004
		0.4548693721 2.8907173300000005e-17 0.4548693721
		0.64328243500000004 1.4923785180000001e-17 -1.5776117929999999e-16
		0.4548693721 8.461260534999999e-17 -0.4548693721
		1.607597685e-15 1.9715086809999999e-16 -0.64328243500000004
		-0.4548693721 2.866151853e-16 -0.4548693721
		-0.64328243500000004 3.0059857349999997e-16 -1.5776117929999999e-16
		-0.4548693721 2.309097533e-16 0.4548693721
		;
createNode joint -n "FKXMiddleFinger1_R" -p "FKMiddleFinger1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger2_R" -p "FKXMiddleFinger1_R";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0.69033865096549729 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -12.640649879962815 -2.032984035226316 5.609602775706561 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger2_R" -p "FKOffsetMiddleFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 0 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKMiddleFinger2_R" -p "FKExtraMiddleFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999956 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger2_RShape" -p "FKMiddleFinger2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.30524250139999998 -0.0050864548479999999 -0.30524250139999998
		-0.43167808520000001 -0.0050864548479999999 2.5326679040000002e-16
		-0.30524250139999998 -0.0050864548479999999 0.30524250139999998
		7.1747792979999995e-16 -0.0050864548479999999 0.43167808520000001
		0.30524250139999998 -0.0050864548479999999 0.30524250139999998
		0.43167808520000001 -0.0050864548479999999 2.5326679040000002e-16
		0.30524250139999998 -0.0050864548479999999 -0.30524250139999998
		1.0744225810000001e-15 -0.0050864548479999999 -0.43167808520000001
		-0.30524250139999998 -0.0050864548479999999 -0.30524250139999998
		-0.43167808520000001 -0.0050864548479999999 2.5326679040000002e-16
		-0.30524250139999998 -0.0050864548479999999 0.30524250139999998
		;
createNode joint -n "FKXMiddleFinger2_R" -p "FKMiddleFinger2_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleFinger3_End_R" -p "FKXMiddleFinger2_R";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0.13248348336405757 8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 109.42346594337717 -5.1390366278561803 15.55002141287328 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -105.8664716271503 6.1127567356735826 -15.200491040675908 ;
createNode joint -n "FKOffsetIndexFinger1_R" -p "FKXElbow_R";
	setAttr ".t" -type "double3" 0.048771924557461446 2.7288818338408771 0.28520562099039548 ;
	setAttr ".r" -type "double3" 35.44205992396585 -67.525449455658801 8.2444182574779727 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger1_R" -p "FKOffsetIndexFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 0 ;
	setAttr ".ro" 5;
createNode transform -n "FKIndexFinger1_R" -p "FKExtraIndexFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 8.8817841970012523e-16 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999989 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger1_RShape" -p "FKIndexFinger1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.42267533260000001 4.3525653550000004e-16 -0.42267533260000001
		-0.59775318769999997 4.4825022910000003e-16 -3.1552235859999999e-16
		-0.42267533260000001 3.8349373619999999e-16 0.42267533260000001
		-1.732139192e-16 2.789205322e-16 0.59775318769999997
		0.42267533260000001 1.9578818179999999e-16 0.42267533260000001
		0.59775318769999997 1.827944882e-16 -3.1552235859999999e-16
		0.42267533260000001 2.4755098109999999e-16 -0.42267533260000001
		3.2105440320000002e-16 3.5212418510000003e-16 -0.59775318769999997
		-0.42267533260000001 4.3525653550000004e-16 -0.42267533260000001
		-0.59775318769999997 4.4825022910000003e-16 -3.1552235859999999e-16
		-0.42267533260000001 3.8349373619999999e-16 0.42267533260000001
		;
createNode joint -n "FKXIndexFinger1_R" -p "FKIndexFinger1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetIndexFinger2_R" -p "FKXIndexFinger1_R";
	setAttr ".t" -type "double3" 2.6645352591003757e-15 0.59426302015666899 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" -3.9153769134058369 4.4420638993629211 2.7018762000333005 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger2_R" -p "FKOffsetIndexFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
createNode transform -n "FKIndexFinger2_R" -p "FKExtraIndexFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 8.8817841970012523e-16 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger2_RShape" -p "FKIndexFinger2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.30709165710000003 -3.342917331e-16 -0.30709165710000003
		-0.43429318630000002 -3.2485126059999998e-16 -4.2128371959999998e-16
		-0.30709165710000003 -3.718996146e-16 0.30709165710000003
		-1.2584730019999999e-16 -4.4787650759999998e-16 0.43429318630000002
		0.30709165710000003 -5.0827570599999995e-16 0.30709165710000003
		0.43429318630000002 -5.1771617849999998e-16 -4.2128371959999998e-16
		0.30709165710000003 -4.7066782449999995e-16 -0.30709165710000003
		2.3325971750000002e-16 -3.9469093150000002e-16 -0.43429318630000002
		-0.30709165710000003 -3.342917331e-16 -0.30709165710000003
		-0.43429318630000002 -3.2485126059999998e-16 -4.2128371959999998e-16
		-0.30709165710000003 -3.718996146e-16 0.30709165710000003
		;
createNode joint -n "FKXIndexFinger2_R" -p "FKIndexFinger2_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXIndexFinger3_End_R" -p "FKXIndexFinger2_R";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 0.13732743200941755 -1.3322676295501878e-15 ;
	setAttr ".r" -type "double3" -106.24797391080133 0.92233975634209753 153.09889937882559 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 101.11030647136279 1.682860326726243 -153.15788105038223 ;
createNode joint -n "FKOffsetThumbFinger1_R" -p "FKXElbow_R";
	setAttr ".t" -type "double3" -0.11739851835679826 2.7357413095711296 -0.14142536441553677 ;
	setAttr ".r" -type "double3" 41.262692493137358 -72.731378659907506 54.456811384566521 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraThumbFinger1_R" -p "FKOffsetThumbFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 6.6613381477509392e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999967 ;
createNode transform -n "FKThumbFinger1_R" -p "FKExtraThumbFinger1_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 -2.2204460492503131e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1.0000000000000004 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger1_RShape" -p "FKThumbFinger1_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.43755604100000001 -0.0055267534030000003 -0.43748974870000001
		-0.61877018039999998 -0.0055267534030000003 -1.1526949630000001e-07
		-0.43755604100000001 -0.0055267534030000003 0.43748951809999997
		-6.6407557700000008e-05 -0.0055267534030000003 0.61870365760000001
		0.43742322579999998 -0.0055267534030000003 0.43748951809999997
		0.61863736530000002 -0.0055267534030000003 -1.1526949630000001e-07
		0.43742322579999998 -0.0055267534030000003 -0.43748974870000001
		-6.6407557700000008e-05 -0.0055267534030000003 -0.61870388809999999
		-0.43755604100000001 -0.0055267534030000003 -0.43748974870000001
		-0.61877018039999998 -0.0055267534030000003 -1.1526949630000001e-07
		-0.43755604100000001 -0.0055267534030000003 0.43748951809999997
		;
createNode joint -n "FKXThumbFinger1_R" -p "FKThumbFinger1_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetThumbFinger2_R" -p "FKXThumbFinger1_R";
	setAttr ".t" -type "double3" -5.3290705182007514e-15 0.63847285897774975 -8.8817841970012523e-16 ;
	setAttr ".r" -type "double3" 0.014345738464447358 0.10149567058135728 -4.0834183500083681 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraThumbFinger2_R" -p "FKOffsetThumbFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -1.1102230246251565e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKThumbFinger2_R" -p "FKExtraThumbFinger2_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 1.1102230246251565e-16 -8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999967 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger2_RShape" -p "FKThumbFinger2_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.30411790550000001 1.9147052099999998e-16 -0.30411790550000001
		-0.4300876663 2.008195759e-16 -4.2128371959999998e-16
		-0.30411790550000001 1.5422681910000001e-16 0.30411790550000001
		-1.246286458e-16 7.8985655690000002e-17 0.4300876663
		0.30411790550000001 1.9171338769999998e-17 0.30411790550000001
		0.4300876663 9.822283923000001e-18 -4.2128371959999998e-16
		0.30411790550000001 5.6415040710000002e-17 -0.30411790550000001
		2.3100092469999998e-16 1.316562041e-16 -0.4300876663
		-0.30411790550000001 1.9147052099999998e-16 -0.30411790550000001
		-0.4300876663 2.008195759e-16 -4.2128371959999998e-16
		-0.30411790550000001 1.5422681910000001e-16 0.30411790550000001
		;
createNode joint -n "FKXThumbFinger2_R" -p "FKThumbFinger2_R";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXThumbFinger3_End_R" -p "FKXThumbFinger2_R";
	setAttr ".t" -type "double3" -2.6645352591003757e-15 0.12953755254568589 6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" 79.552587634689885 170.75388166562388 7.0610427956746742 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 100.44741236531011 -9.2461183343760975 172.93895720432533 ;
createNode orientConstraint -n "FKGlobalShoulder_R_orientConstraint1" -p "FKGlobalShoulder_R";
	addAttr -ci true -k true -sn "w0" -ln "GlobalShoulder_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "FKGlobalStaticShoulder_RW1" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "FKOffsetShoulder_L" -p "FKXBody_M";
	setAttr ".t" -type "double3" 5.6343142387459695 0.20712480712431613 -0.68573384593890574 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 76.057464552233071 -68.095823061854645 -56.55527240679114 ;
createNode transform -n "FKGlobalStaticShoulder_L" -p "FKOffsetShoulder_L";
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" 2.4848083448933718e-15 4.4975031042570047e-15 2.2263882770244617e-14 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 1 1 ;
createNode transform -n "FKGlobalShoulder_L" -p "FKGlobalStaticShoulder_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 4.4408920985006262e-16 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
createNode transform -n "FKExtraShoulder_L" -p "FKGlobalShoulder_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 0 ;
	setAttr ".r" -type "double3" 2.4848083448933718e-15 4.4975031042570047e-15 2.2263882770244617e-14 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999989 0.99999999999999967 ;
createNode transform -n "FKShoulder_L" -p "FKExtraShoulder_L";
	addAttr -ci true -k true -sn "Global" -ln "Global" -dv 10 -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000004 1 1.0000000000000004 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".Global";
createNode nurbsCurve -n "FKShoulder_LShape" -p "FKShoulder_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.72245188790000003 -8.881784197e-16 0.72245188790000003
		1.021701258 -1.776356839e-15 0
		0.72245188790000003 -2.6645352589999999e-15 -0.72245188790000003
		8.881784197e-16 -2.220446049e-15 -1.021701258
		-0.72245188790000003 -1.776356839e-15 -0.72245188790000003
		-1.021701258 -8.881784197e-16 -1.776356839e-15
		-0.72245188790000003 0 0.72245188790000003
		0 4.4408920989999998e-16 1.021701258
		0.72245188790000003 -8.881784197e-16 0.72245188790000003
		1.021701258 -1.776356839e-15 0
		0.72245188790000003 -2.6645352589999999e-15 -0.72245188790000003
		;
createNode joint -n "FKXShoulder_L" -p "FKShoulder_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetElbow_L" -p "FKXShoulder_L";
	setAttr ".t" -type "double3" -1.3322676295501878e-15 -3.0206925677459955 -3.5527136788005009e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -0.46684348313122442 -1.3269620015957899 109.3845711425946 ;
createNode transform -n "FKExtraElbow_L" -p "FKOffsetElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 8.8278125961003172e-31 -3.1805546814635168e-15 3.1805546814635168e-14 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
createNode transform -n "FKElbow_L" -p "FKExtraElbow_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -4.5796699765787707e-16 0 8.8817841970012523e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000004 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKElbow_LShape" -p "FKElbow_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.65669205630000005 0 0.65669205630000005
		0.92870281239999997 0 8.881784197e-16
		0.65669205630000005 4.4408920989999998e-16 -0.65669205630000005
		-3.2057689840000003e-15 8.881784197e-16 -0.92870281239999997
		-0.65669205630000005 8.881784197e-16 -0.65669205630000005
		-0.92870281239999997 8.881784197e-16 2.6645352589999999e-15
		-0.65669205630000005 4.4408920989999998e-16 0.65669205630000005
		-1.7486012639999999e-15 4.4408920989999998e-16 0.92870281239999997
		0.65669205630000005 0 0.65669205630000005
		0.92870281239999997 0 8.881784197e-16
		0.65669205630000005 4.4408920989999998e-16 -0.65669205630000005
		;
createNode joint -n "FKXElbow_L" -p "FKElbow_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger1_L" -p "FKXElbow_L";
	setAttr ".t" -type "double3" -0.29765672778810631 -2.6683615851300635 0.023725028499104184 ;
	setAttr ".r" -type "double3" 119.14463142878685 58.101270552091094 -119.81005577848617 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger1_L" -p "FKOffsetMiddleFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 2.2204460492503131e-16 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -1.272221872585407e-14 -5.7380781874652084e-31 5.1684013573782151e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKMiddleFinger1_L" -p "FKExtraMiddleFinger1_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999933 0.99999999999999944 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger1_LShape" -p "FKMiddleFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.4548693721 -5.5511151229999996e-15 0.4548693721
		0.64328243500000004 -5.3290705179999997e-15 -3.3306690739999996e-15
		0.4548693721 -5.1070259130000006e-15 -0.4548693721
		-2.6645352589999999e-15 -4.8849813080000007e-15 -0.64328243500000004
		-0.4548693721 -4.8849813080000007e-15 -0.4548693721
		-0.64328243500000004 -4.8849813080000007e-15 -2.4424906540000003e-15
		-0.4548693721 -5.1070259130000006e-15 0.4548693721
		-8.881784197e-16 -5.5511151229999996e-15 0.64328243500000004
		0.4548693721 -5.5511151229999996e-15 0.4548693721
		0.64328243500000004 -5.3290705179999997e-15 -3.3306690739999996e-15
		0.4548693721 -5.1070259130000006e-15 -0.4548693721
		;
createNode joint -n "FKXMiddleFinger1_L" -p "FKMiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetMiddleFinger2_L" -p "FKXMiddleFinger1_L";
	setAttr ".t" -type "double3" 0 -0.69033865096549962 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" -12.640649879962815 -2.032984035226316 5.609602775706561 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraMiddleFinger2_L" -p "FKOffsetMiddleFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" -6.7586786981099719e-15 -1.590277340731758e-15 -2.3854160110976372e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode transform -n "FKMiddleFinger2_L" -p "FKExtraMiddleFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999967 0.99999999999999967 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKMiddleFinger2_LShape" -p "FKMiddleFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.30524250139999998 0.0050864548479999999 0.30524250139999998
		0.43167808520000001 0.0050864548479999999 -3.1086244690000001e-15
		0.30524250139999998 0.0050864548479999999 -0.30524250139999998
		-6.2172489380000002e-15 0.0050864548479999999 -0.43167808520000001
		-0.30524250139999998 0.0050864548479999999 -0.30524250139999998
		-0.43167808520000001 0.0050864548479999999 -2.220446049e-15
		-0.30524250139999998 0.0050864548479999999 0.30524250139999998
		-5.3290705179999997e-15 0.0050864548479999999 0.43167808520000001
		0.30524250139999998 0.0050864548479999999 0.30524250139999998
		0.43167808520000001 0.0050864548479999999 -3.1086244690000001e-15
		0.30524250139999998 0.0050864548479999999 -0.30524250139999998
		;
createNode joint -n "FKXMiddleFinger2_L" -p "FKMiddleFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXMiddleFinger3_End_L" -p "FKXMiddleFinger2_L";
	setAttr ".t" -type "double3" 3.5527136788005009e-15 -0.13248348336405713 -1.1102230246251565e-15 ;
	setAttr ".r" -type "double3" -70.576534056622847 -5.1390366278561812 15.55002141287328 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 74.13352837284971 6.1127567356735852 -15.200491040675905 ;
createNode joint -n "FKOffsetIndexFinger1_L" -p "FKXElbow_L";
	setAttr ".t" -type "double3" -0.048771924557460072 -2.7288818338408776 -0.28520562099039548 ;
	setAttr ".r" -type "double3" 35.44205992396585 -67.525449455658801 8.2444182574779727 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger1_L" -p "FKOffsetIndexFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 0 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -1.2722218725854067e-14 -3.1805546814635168e-15 -3.5311250384401269e-31 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1.0000000000000002 1 ;
createNode transform -n "FKIndexFinger1_L" -p "FKExtraIndexFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -8.8817841970012523e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999944 0.99999999999999989 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger1_LShape" -p "FKIndexFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.42267533260000001 -1.3322676300000001e-15 0.42267533260000001
		0.59775318769999997 -2.220446049e-15 2.220446049e-15
		0.42267533260000001 -1.3322676300000001e-15 -0.42267533260000001
		3.5527136789999999e-15 -1.3322676300000001e-15 -0.59775318769999997
		-0.42267533260000001 -1.3322676300000001e-15 -0.42267533260000001
		-0.59775318769999997 -4.4408920989999998e-16 2.6645352589999999e-15
		-0.42267533260000001 -4.4408920989999998e-16 0.42267533260000001
		3.5527136789999999e-15 -8.881784197e-16 0.59775318769999997
		0.42267533260000001 -1.3322676300000001e-15 0.42267533260000001
		0.59775318769999997 -2.220446049e-15 2.220446049e-15
		0.42267533260000001 -1.3322676300000001e-15 -0.42267533260000001
		;
createNode joint -n "FKXIndexFinger1_L" -p "FKIndexFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetIndexFinger2_L" -p "FKXIndexFinger1_L";
	setAttr ".t" -type "double3" -2.6645352591003757e-15 -0.59426302015666987 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -3.9153769134058369 4.4420638993629211 2.7018762000333005 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraIndexFinger2_L" -p "FKOffsetIndexFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -4.4408920985006262e-16 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 6.3611093629270351e-15 1.113194138512231e-14 -9.5416640443905519e-15 ;
	setAttr ".ro" 5;
createNode transform -n "FKIndexFinger2_L" -p "FKExtraIndexFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999989 1.0000000000000002 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKIndexFinger2_LShape" -p "FKIndexFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.30709165710000003 8.881784197e-16 0.30709165710000003
		0.43429318630000002 4.4408920989999998e-16 4.8849813080000007e-15
		0.30709165710000003 8.881784197e-16 -0.30709165710000003
		8.881784197e-16 8.881784197e-16 -0.43429318630000002
		-0.30709165710000003 8.881784197e-16 -0.30709165710000003
		-0.43429318630000002 8.881784197e-16 5.7731597280000003e-15
		-0.30709165710000003 8.881784197e-16 0.30709165710000003
		1.776356839e-15 8.881784197e-16 0.43429318630000002
		0.30709165710000003 8.881784197e-16 0.30709165710000003
		0.43429318630000002 4.4408920989999998e-16 4.8849813080000007e-15
		0.30709165710000003 8.881784197e-16 -0.30709165710000003
		;
createNode joint -n "FKXIndexFinger2_L" -p "FKIndexFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXIndexFinger3_End_L" -p "FKXIndexFinger2_L";
	setAttr ".t" -type "double3" 4.4408920985006262e-15 -0.13732743200941844 1.3322676295501878e-15 ;
	setAttr ".r" -type "double3" 73.752026089198736 0.9223397563420942 153.09889937882559 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -78.889693528637224 1.6828603267262465 -153.15788105038223 ;
createNode joint -n "FKOffsetThumbFinger1_L" -p "FKXElbow_L";
	setAttr ".t" -type "double3" 0.11739851835679918 -2.7357413095711296 0.14142536441553499 ;
	setAttr ".r" -type "double3" 41.262692493137358 -72.731378659907506 54.456811384566521 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraThumbFinger1_L" -p "FKOffsetThumbFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -2.2204460492503131e-16 -4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -1.9083328088781101e-14 -1.1833899742554687e-14 -2.2922356981641384e-15 ;
	setAttr ".ro" 5;
createNode transform -n "FKThumbFinger1_L" -p "FKExtraThumbFinger1_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger1_LShape" -p "FKThumbFinger1_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.43755604100000001 0.0055267534030000003 0.43748974870000001
		0.61877018039999998 0.0055267534030000003 1.1526949909999999e-07
		0.43755604100000001 0.0055267534030000003 -0.43748951809999997
		6.6407557700000008e-05 0.0055267534030000003 -0.61870365760000001
		-0.43742322579999998 0.0055267534030000003 -0.43748951809999997
		-0.61863736530000002 0.0055267534030000003 1.1526949989999999e-07
		-0.43742322579999998 0.0055267534030000003 0.43748974870000001
		6.6407557700000008e-05 0.0055267534030000003 0.61870388809999999
		0.43755604100000001 0.0055267534030000003 0.43748974870000001
		0.61877018039999998 0.0055267534030000003 1.1526949909999999e-07
		0.43755604100000001 0.0055267534030000003 -0.43748951809999997
		;
createNode joint -n "FKXThumbFinger1_L" -p "FKThumbFinger1_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetThumbFinger2_L" -p "FKXThumbFinger1_L";
	setAttr ".t" -type "double3" 2.6645352591003757e-15 -0.6384728589777493 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 0.014345738464447358 0.10149567058135728 -4.0834183500083681 ;
	setAttr ".ro" 5;
createNode transform -n "FKExtraThumbFinger2_L" -p "FKOffsetThumbFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -1.1102230246251565e-16 6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" -1.2722218725854067e-14 8.7465253740246703e-15 -5.5659706925611536e-15 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "FKThumbFinger2_L" -p "FKExtraThumbFinger2_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -1.1102230246251565e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKThumbFinger2_LShape" -p "FKThumbFinger2_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.30411790550000001 -3.3306690739999996e-15 0.30411790550000001
		0.4300876663 -3.1086244690000001e-15 4.2188474940000004e-15
		0.30411790550000001 -2.8865798640000002e-15 -0.30411790550000001
		-8.881784197e-16 -2.6645352589999999e-15 -0.4300876663
		-0.30411790550000001 -2.6645352589999999e-15 -0.30411790550000001
		-0.4300876663 -2.6645352589999999e-15 4.8849813080000007e-15
		-0.30411790550000001 -2.8865798640000002e-15 0.30411790550000001
		0 -3.1086244690000001e-15 0.4300876663
		0.30411790550000001 -3.3306690739999996e-15 0.30411790550000001
		0.4300876663 -3.1086244690000001e-15 4.2188474940000004e-15
		0.30411790550000001 -2.8865798640000002e-15 -0.30411790550000001
		;
createNode joint -n "FKXThumbFinger2_L" -p "FKThumbFinger2_L";
	setAttr ".ro" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKXThumbFinger3_End_L" -p "FKXThumbFinger2_L";
	setAttr ".t" -type "double3" 8.8817841970012523e-16 -0.12953755254568555 6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" 79.552587634689885 9.246118334376126 -172.93895720432533 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -79.552587634689885 -9.2461183343760975 172.93895720432533 ;
createNode orientConstraint -n "FKGlobalShoulder_L_orientConstraint1" -p "FKGlobalShoulder_L";
	addAttr -ci true -k true -sn "w0" -ln "GlobalShoulder_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "FKGlobalStaticShoulder_LW1" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "FKOffsetTrack_R" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" -1.9369647648037842 0 0 ;
	setAttr ".r" -type "double3" 51.580167932588935 0.001277902588734433 179.9983930843122 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraTrack_R" -p "FKOffsetTrack_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -4.4408920985006262e-16 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
createNode transform -n "FKTrack_R" -p "FKExtraTrack_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 -4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrack_RShape" -p "FKTrack_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.2897019359999999 3.653428241e-16 -1.2897019359999999
		-1.8239139689999999 4.0499025659999999e-16 0
		-1.2897019359999999 2.0739988939999999e-16 1.2897019359999999
		-5.2852463739999996e-16 -1.1168252019999999e-16 1.8239139689999999
		1.2897019359999999 -3.653428241e-16 1.2897019359999999
		1.8239139689999999 -4.0499025659999999e-16 0
		1.2897019359999999 -2.0739988939999999e-16 -1.2897019359999999
		9.7962775059999983e-16 1.1168252019999999e-16 -1.8239139689999999
		-1.2897019359999999 3.653428241e-16 -1.2897019359999999
		-1.8239139689999999 4.0499025659999999e-16 0
		-1.2897019359999999 2.0739988939999999e-16 1.2897019359999999
		;
createNode joint -n "FKXTrack_R" -p "FKTrack_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetTrackBend_R" -p "FKXTrack_R";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0.77347563476454995 6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" -104.506832527965 -0.00086172399645064867 -0.0006671358298534362 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraTrackBend_R" -p "FKOffsetTrackBend_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0 -2.2204460492503131e-16 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "FKTrackBend_R" -p "FKExtraTrackBend_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 -2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1.0000000000000004 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrackBend_RShape" -p "FKTrackBend_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.2794834216 7.9171209820000003e-17 -0.2794834216
		-0.3952492453 8.7762962519999995e-17 0
		-0.2794834216 4.4944362050000004e-17 0.2794834216
		-1.1453334289999998e-16 -2.4202036159999997e-17 0.3952492453
		0.2794834216 -7.9171209820000003e-17 0.2794834216
		0.3952492453 -8.7762962519999995e-17 0
		0.2794834216 -4.4944362050000004e-17 -0.2794834216
		2.1228914069999999e-16 2.4202036159999997e-17 -0.3952492453
		-0.2794834216 7.9171209820000003e-17 -0.2794834216
		-0.3952492453 8.7762962519999995e-17 0
		-0.2794834216 4.4944362050000004e-17 0.2794834216
		;
createNode joint -n "FKXTrackBend_R" -p "FKTrackBend_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetTrackBottom_R" -p "FKXTrackBend_R";
	setAttr ".t" -type "double3" 0 0.75952924543354516 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" 52.926664595376074 -0.0013444510761591044 0.0027008789540613658 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraTrackBottom_R" -p "FKOffsetTrackBottom_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 0 ;
	setAttr ".ro" 3;
createNode transform -n "FKTrackBottom_R" -p "FKExtraTrackBottom_R";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrackBottom_RShape" -p "FKTrackBottom_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.168530775 3.3101782800000001e-16 -1.168530775
		-1.65255207 3.6694027159999999e-16 0
		-1.168530775 1.8791408060000002e-16 1.168530775
		-4.7886824639999998e-16 -1.0118963020000001e-16 1.65255207
		1.168530775 -3.3101782800000001e-16 1.168530775
		1.65255207 -3.6694027159999999e-16 0
		1.168530775 -1.8791408060000002e-16 -1.168530775
		8.875889406000001e-16 1.0118963020000001e-16 -1.65255207
		-1.168530775 3.3101782800000001e-16 -1.168530775
		-1.65255207 3.6694027159999999e-16 0
		-1.168530775 1.8791408060000002e-16 1.168530775
		;
createNode joint -n "FKXTrackBottom_R" -p "FKTrackBottom_R";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToTrackBottom_R" -p "FKXTrackBottom_R";
	setAttr ".r" -type "double3" -180 179.99994625672272 7.0167092985317876e-15 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999978 1 1 ;
createNode joint -n "FKOffsetTrack_L" -p "FKXPelvis_M";
	setAttr ".t" -type "double3" 1.936964764803784 0 -2.4084365415639325e-16 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 51.580167968428924 179.99872209741696 0.0016069156816309244 ;
createNode transform -n "FKExtraTrack_L" -p "FKOffsetTrack_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 0 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -6.498620643734155e-31 -7.3480927088273561e-15 -1.0134426722603982e-14 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
createNode transform -n "FKTrack_L" -p "FKExtraTrack_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 2.2204460492503131e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrack_LShape" -p "FKTrack_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.2897019359999999 -2.7178436609999999e-08 1.2897019359999999
		1.8239139689999999 -4.4408920989999998e-16 -9.6878061130000003e-13
		1.2897019359999999 2.7178436389999999e-08 -1.2897019359999999
		-9.6722629910000002e-13 3.8436113629999999e-08 -1.8239139689999999
		-1.2897019359999999 2.7178437720000002e-08 -1.2897019359999999
		-1.8239139689999999 8.881784197e-16 9.6878061130000003e-13
		-1.2897019359999999 -2.7178435499999998e-08 1.2897019359999999
		9.6855856669999991e-13 -3.8436112740000001e-08 1.8239139689999999
		1.2897019359999999 -2.7178436609999999e-08 1.2897019359999999
		1.8239139689999999 -4.4408920989999998e-16 -9.6878061130000003e-13
		1.2897019359999999 2.7178436389999999e-08 -1.2897019359999999
		;
createNode joint -n "FKXTrack_L" -p "FKTrack_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetTrackBend_L" -p "FKXTrack_L";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 -0.77347563476454972 0 ;
	setAttr ".r" -type "double3" -104.506832527965 -0.00086172399645064867 -0.0006671358298534362 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraTrackBend_L" -p "FKOffsetTrackBend_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 6.3611093629270327e-15 -8.2295687628956831e-15 -8.0837803982851423e-15 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1.0000000000000002 ;
createNode transform -n "FKTrackBend_L" -p "FKExtraTrackBend_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 2.2204460492503131e-16 4.4408920985006262e-16 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 0.99999999999999956 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrackBend_LShape" -p "FKTrackBend_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.2794834216 9.8904018309999999e-09 0.27948342570000001
		0.3952492453 1.5780061039999999e-08 4.0832666140000003e-09
		0.2794834216 2.166974733e-08 -0.2794834175
		7.3541173149999998e-13 2.410936295e-08 -0.39524924119999999
		-0.2794834216 2.1669813939999999e-08 -0.2794834175
		-0.3952492453 1.5780154520000001e-08 4.0827743410000004e-09
		-0.2794834216 9.8904679999999998e-09 0.27948342570000001
		2.4269475319999996e-13 7.4508530459999997e-09 0.3952492494
		0.2794834216 9.8904018309999999e-09 0.27948342570000001
		0.3952492453 1.5780061039999999e-08 4.0832666140000003e-09
		0.2794834216 2.166974733e-08 -0.2794834175
		;
createNode joint -n "FKXTrackBend_L" -p "FKTrackBend_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "FKOffsetTrackBottom_L" -p "FKXTrackBend_L";
	setAttr ".t" -type "double3" -6.6613381477509392e-16 -0.75952924543354583 -6.6613381477509392e-16 ;
	setAttr ".r" -type "double3" 52.926664595376074 -0.0013444510761591044 0.0027008789540613658 ;
	setAttr ".ro" 3;
createNode transform -n "FKExtraTrackBottom_L" -p "FKOffsetTrackBottom_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 0 ;
	setAttr ".r" -type "double3" 0 2.5444437451708128e-14 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "FKTrackBottom_L" -p "FKExtraTrackBottom_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "FKTrackBottom_LShape" -p "FKTrackBottom_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.168530775 -2.4624221990000002e-08 1.1685307549999999
		1.65255207 1.021849272e-12 -1.9776510260000001e-08
		1.168530775 2.4625667510000001e-08 -1.1685307949999999
		1.997957355e-12 3.4824931380000001e-08 -1.6525520899999999
		-1.168530775 2.462422288e-08 -1.1685307949999999
		-1.65255207 -1.020961093e-12 -1.9779343949999998e-08
		-1.168530775 -2.4625666620000001e-08 1.1685307549999999
		-8.3666407140000003e-13 -3.4824930720000002e-08 1.6525520499999999
		1.168530775 -2.4624221990000002e-08 1.1685307549999999
		1.65255207 1.021849272e-12 -1.9776510260000001e-08
		1.168530775 2.4625667510000001e-08 -1.1685307949999999
		;
createNode joint -n "FKXTrackBottom_L" -p "FKTrackBottom_L";
	setAttr ".ro" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "AlignIKToTrackBottom_L" -p "FKXTrackBottom_L";
	setAttr ".r" -type "double3" 0 179.99994625672272 0 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode parentConstraint -n "PelvisCenterBtwLegsBlended_M_parentConstraint1" -p
		 "PelvisCenterBtwLegsBlended_M";
	addAttr -ci true -k true -sn "w0" -ln "PelvisCenter_MW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PelvisCenterBtwLegsOffset_MW1" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0 2.3360524237536708 0 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKSystem" -p "MotionSystem";
createNode transform -n "IKParentConstraint" -p "IKSystem";
createNode transform -n "IKParentConstraintTrack_R" -p "IKParentConstraint";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "IKOffsetTrack_R" -p "IKParentConstraintTrack_R";
	setAttr ".t" -type "double3" -1.9369647648037842 0 0 ;
	setAttr ".r" -type "double3" 51.580167854041854 0.0012779026285295334 179.99838883409492 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
createNode joint -n "IKXTrack_R" -p "IKOffsetTrack_R";
	setAttr ".r" -type "double3" 8.0482297263145586e-12 5.3055117650198854e-07 -6.6891321416262475e-07 ;
	setAttr ".ro" 3;
createNode joint -n "IKXTrackBend_R" -p "IKXTrack_R";
	setAttr ".t" -type "double3" 0 0.77347564697265625 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" -104.50683254054562 -0.00086172399645064867 -0.00066713582970931505 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" -104.506832527965 -0.00086172399645064867 -0.0006671358298534362 ;
createNode joint -n "IKXTrackBottom_R" -p "IKXTrackBend_R";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0.7595292329788208 0 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" 52.926664595376074 -0.0013444510761591044 0.0027008789540613658 ;
createNode transform -n "IKFKAlignedLeg_R" -p "IKXTrackBottom_R";
	setAttr ".t" -type "double3" -6.9620052167707058e-08 2.6105784201035931e-12 1.2850091877870265e-09 ;
	setAttr ".r" -type "double3" 0 0 4.2502190959809657e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999944 0.99999999999999989 ;
createNode orientConstraint -n "IKXTrackBottom_R_orientConstraint1" -p "IKXTrackBottom_R";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -52.92666468348385 -0.0013768495357181264 -179.99734200094838 ;
	setAttr ".o" -type "double3" 179.99999992598799 -180.00005374333594 0 ;
	setAttr ".rsrr" -type "double3" 232.92666459537608 -180.00134445107793 -180.00270087895532 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector1" -p "IKXTrackBend_R";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_R" -p "IKXTrackBend_R";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 0 0 ;
	setAttr ".r" -type "double3" -52.92666460703002 0.0013768495338045399 -179.99734200094738 ;
	setAttr ".s" -type "double3" 0.99999999999999978 0.99999999999999956 0.99999999999999967 ;
createNode annotationShape -n "PoleAnnotationLeg_RShape" -p "PoleAnnotationLeg_R";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintTrack_R_parentConstraint1" -p "IKParentConstraintTrack_R";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 2.3360524237536708 0 ;
	setAttr -k on ".w0";
createNode transform -n "IKParentConstraintTrack_L" -p "IKParentConstraint";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode transform -n "IKOffsetTrack_L" -p "IKParentConstraintTrack_L";
	setAttr ".t" -type "double3" 1.9369647648037838 0 0 ;
	setAttr ".r" -type "double3" 51.580167854041875 179.99872209737146 -0.001611165905036512 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
createNode joint -n "IKXTrack_L" -p "IKOffsetTrack_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 0 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" 8.0482298592596325e-12 5.3055118522262102e-07 -6.6891322515750575e-07 ;
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" -3.1805546814635155e-14 6.9482132783862751e-13 4.5085682664178585e-15 ;
createNode joint -n "IKXTrackBend_L" -p "IKXTrack_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -0.77347564697265625 4.4408920985006262e-16 ;
	setAttr ".r" -type "double3" -104.50683254054567 -0.00086172399645064867 -0.00066713582970931429 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" -104.506832527965 -0.00086172399645064867 -0.0006671358298534362 ;
createNode joint -n "IKXTrackBottom_L" -p "IKXTrackBend_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -0.7595292329788208 2.2204460492503131e-16 ;
	setAttr ".ro" 3;
	setAttr ".pa" -type "double3" 52.926664595376074 -0.0013444510761591044 0.0027008789540613658 ;
createNode transform -n "IKFKAlignedLeg_L" -p "IKXTrackBottom_L";
	setAttr ".t" -type "double3" 6.9620159859340447e-08 -2.6116886431282182e-12 -1.2850092987083203e-09 ;
	setAttr ".r" -type "double3" 0 0 4.2502256211696116e-06 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999944 0.99999999999999989 0.99999999999999911 ;
createNode orientConstraint -n "IKXTrackBottom_L_orientConstraint1" -p "IKXTrackBottom_L";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -52.926664683483907 179.99862315046431 -0.0026579990515881225 ;
	setAttr ".o" -type "double3" 0 180.00005374333594 0 ;
	setAttr ".rsrr" -type "double3" 52.92666466938816 359.9986555489221 0.0027008789553296612 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector2" -p "IKXTrackBend_L";
	setAttr -l on ".v" no;
	setAttr ".hd" yes;
createNode transform -n "PoleAnnotationLeg_L" -p "IKXTrackBend_L";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 2.2204460492503131e-16 0 ;
	setAttr ".r" -type "double3" 127.07333539296994 0.0013768495337688161 -179.99734200094738 ;
	setAttr ".s" -type "double3" 0.99999999999999944 0.99999999999999989 0.99999999999999933 ;
createNode annotationShape -n "PoleAnnotationLeg_LShape" -p "PoleAnnotationLeg_L";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "IKParentConstraintTrack_L_parentConstraint1" -p "IKParentConstraintTrack_L";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 2.3360524237536708 0 ;
	setAttr -k on ".w0";
createNode transform -n "IKHandle" -p "IKSystem";
createNode transform -n "IKParentConstraintLeg_R" -p "IKHandle";
	setAttr ".t" -type "double3" -1.9369999999996519 1.3975278303622969 -1.2429657747503597e-08 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1 ;
createNode transform -n "IKExtraLeg_R" -p "IKParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999978 1 1 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_R" -p "IKExtraLeg_R";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".stretchy";
	setAttr -k on ".antiPop";
	setAttr -k on ".Length1";
	setAttr -k on ".Length2";
createNode nurbsCurve -n "IKLeg_RShape" -p "IKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		-0.2521636458 -1.397527814 6.5012537620000002
		-3.7027441809999999 -1.397527814 6.5099715710000003
		-3.7027441809999999 1.7843058430000001 4.7527129820000003
		-3.7027441809999999 1.7843058430000001 -5.609471396
		-3.7027441809999999 -1.397527814 -7.445280726
		-0.2521636458 -1.397527814 -7.445280726
		-0.2521636458 1.7843058430000001 -5.609471396
		-0.2521636458 1.7843058430000001 4.7462397540000003
		-0.2521636458 -1.397527814 6.5012537620000002
		-0.2521636458 -1.397527814 -7.445280726
		-0.2521636458 1.7843058430000001 -5.609471396
		-3.7027441809999999 1.7843058430000001 -5.609471396
		-3.7027441809999999 -1.397527814 -7.445280726
		-3.7027441809999999 -1.397527814 6.5113494359999997
		-3.7027441809999999 1.7843058430000001 4.7527129820000003
		-0.2521636458 1.7843058430000001 4.7462397540000003
		;
createNode ikHandle -n "IKXLegHandle_R" -p "IKLeg_R";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 -6.6613381477509392e-16 ;
	setAttr ".ro" 3;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_R_poleVectorConstraint1" -p "IKXLegHandle_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 9.9017139814083777e-06 -0.48065246607776557 0.93852458727102128 ;
	setAttr -k on ".w0";
createNode transform -n "IKmessureConstrainToLeg_R" -p "IKLeg_R";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 -6.6613381477509392e-16 ;
createNode transform -n "PoleAimLeg_R" -p "IKHandle";
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 1 ;
createNode aimConstraint -n "PoleAimLeg_R_aimConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -sn "w0" -ln "IKLeg_RW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -7.5884390430335782e-07 7.5881541542637537e-07 -90.002151065647936 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_R_pointConstraint1" -p "PoleAimLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "IKXTrack_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.9369647648037842 2.3360524237536708 0 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_R" -p "IKHandle";
createNode transform -n "PoleExtraLeg_R" -p "PoleParentConstraintLeg_R";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_R" -p "PoleExtraLeg_R";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_RShape" -p "PoleLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-6.6565954689999989e-33 0.13501179590000001 -2.9978640870000002e-17
		-1.3313190939999998e-32 0.27002359170000001 -5.9957281749999998e-17
		1.7987184519999999e-16 8.993592262e-17 0.40503538760000002
		1.3313190939999998e-32 -0.27002359170000001 5.9957281749999998e-17
		6.6565954689999989e-33 -0.13501179590000001 2.9978640870000002e-17
		-1.7987184519999999e-16 -0.13501179590000001 -0.40503538760000002
		-1.7987184519999999e-16 0.13501179590000001 -0.40503538760000002
		-6.6565954689999989e-33 0.13501179590000001 -2.9978640870000002e-17
		;
createNode transform -n "PoleAnnotateTargetLeg_R" -p "PoleLeg_R";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_RShape" -p "PoleAnnotateTargetLeg_R";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_R_parentConstraint1" -p "PoleParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_RStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tot" -type "double3" -2.2204460492503131e-16 0 0 ;
	setAttr ".tg[1].tot" -type "double3" 0.48065246536728656 2.7946935052147737e-05 
		0.93852458727102128 ;
	setAttr ".tg[1].tor" -type "double3" 0 0 90.002151065647922 ;
	setAttr ".lr" -type "double3" 0 0 -1.9083328088781101e-14 ;
	setAttr ".rst" -type "double3" -1.9369548630898028 1.855399957675905 0.93852458727102128 ;
	setAttr ".rsrr" -type "double3" 0 0 -9.5416640443905503e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKParentConstraintLeg_L" -p "IKHandle";
	setAttr ".t" -type "double3" 1.9369999999996523 1.3975278303622976 -1.2429658413637412e-08 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode transform -n "IKExtraLeg_L" -p "IKParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-16 1.6543612251060553e-24 ;
	setAttr ".ro" 3;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "IKLeg_L" -p "IKExtraLeg_L";
	addAttr -ci true -k true -sn "swivel" -ln "swivel" -at "double";
	addAttr -ci true -k true -sn "stretchy" -ln "stretchy" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "antiPop" -ln "antiPop" -min 0 -max 10 -at "double";
	addAttr -ci true -k true -sn "Length1" -ln "Length1" -dv 1 -at "double";
	addAttr -ci true -k true -sn "Length2" -ln "Length2" -dv 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ro" 3;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".swivel";
	setAttr -k on ".stretchy";
	setAttr -k on ".antiPop";
	setAttr -k on ".Length1";
	setAttr -k on ".Length2";
createNode nurbsCurve -n "IKLeg_LShape" -p "IKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.2521636458 -1.397527814 6.5012537620000002
		3.7027441809999999 -1.397527814 6.5099715710000003
		3.7027441809999999 1.7843058430000001 4.7527129820000003
		3.7027441809999999 1.7843058430000001 -5.609471396
		3.7027441809999999 -1.397527814 -7.445280726
		0.2521636458 -1.397527814 -7.445280726
		0.2521636458 1.7843058430000001 -5.609471396
		0.2521636458 1.7843058430000001 4.7462397540000003
		0.2521636458 -1.397527814 6.5012537620000002
		0.2521636458 -1.397527814 -7.445280726
		0.2521636458 1.7843058430000001 -5.609471396
		3.7027441809999999 1.7843058430000001 -5.609471396
		3.7027441809999999 -1.397527814 -7.445280726
		3.7027441809999999 -1.397527814 6.5113494359999997
		3.7027441809999999 1.7843058430000001 4.7527129820000003
		0.2521636458 1.7843058430000001 4.7462397540000003
		;
createNode ikHandle -n "IKXLegHandle_L" -p "IKLeg_L";
	setAttr -l on ".v" no;
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 -1.6543612251060553e-24 ;
	setAttr ".ro" 3;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "IKXLegHandle_L_poleVectorConstraint1" -p "IKXLegHandle_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleLeg_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -9.9017139816304223e-06 -0.48065246607776557 0.93852458727102028 ;
	setAttr -k on ".w0";
createNode transform -n "IKmessureConstrainToLeg_L" -p "IKLeg_L";
	setAttr ".t" -type "double3" 0 -2.2204460492503131e-16 -1.6543612251060553e-24 ;
createNode transform -n "PoleAimLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1.0000000000000004 1 1.0000000000000007 ;
createNode aimConstraint -n "PoleAimLeg_L_aimConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -sn "w0" -ln "IKLeg_LW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".wu" -type "double3" 1 0 0 ;
	setAttr ".wut" 2;
	setAttr ".rsrr" -type "double3" -7.5878695986130827e-07 7.5881544766995845e-07 -89.997848934352064 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "PoleAimLeg_L_pointConstraint1" -p "PoleAimLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "IKXTrack_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.9369647648037847 2.3360524237536708 -1.3797283313866929e-16 ;
	setAttr -k on ".w0";
createNode transform -n "PoleParentConstraintLeg_L" -p "IKHandle";
	setAttr ".s" -type "double3" 1.0000000000000002 1 1.0000000000000002 ;
createNode transform -n "PoleExtraLeg_L" -p "PoleParentConstraintLeg_L";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "PoleLeg_L" -p "PoleExtraLeg_L";
	addAttr -ci true -k true -sn "follow" -ln "follow" -min 0 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".follow" 10;
createNode nurbsCurve -n "PoleLeg_LShape" -p "PoleLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 15;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		1.110223025e-15 0.13501179590000001 5.5511151229999998e-16
		1.110223025e-15 0.27002359170000001 4.4408920989999998e-16
		8.881784197e-16 1.110223025e-15 0.40503538760000002
		1.110223025e-15 -0.27002359170000001 6.6613381479999994e-16
		1.110223025e-15 -0.13501179590000001 5.5511151229999998e-16
		1.3322676300000001e-15 -0.13501179590000001 -0.40503538760000002
		1.3322676300000001e-15 0.13501179590000001 -0.40503538760000002
		1.110223025e-15 0.13501179590000001 5.5511151229999998e-16
		;
createNode transform -n "PoleAnnotateTargetLeg_L" -p "PoleLeg_L";
	setAttr -l on -k off ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "PoleAnnotateTargetLeg_LShape" -p "PoleAnnotateTargetLeg_L";
	setAttr -k off ".v";
createNode parentConstraint -n "PoleParentConstraintLeg_L_parentConstraint1" -p "PoleParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "PoleParentConstraintLeg_LStaticW0" -dv 1 
		-min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "PoleAimLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[1].tot" -type "double3" 0.4806524653672859 -2.7946935052369781e-05 
		0.93852458727101962 ;
	setAttr ".tg[1].tor" -type "double3" 0 0 89.997848934352078 ;
	setAttr ".lr" -type "double3" 0 0 6.3611093629270351e-15 ;
	setAttr ".rst" -type "double3" 1.936954863089803 1.8553999576759055 0.93852458727102017 ;
	setAttr ".rsrr" -type "double3" 0 0 3.1805546814635176e-15 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "IKStatic" -p "IKSystem";
createNode transform -n "PoleParentConstraintLeg_RStatic" -p "IKStatic";
	setAttr ".t" -type "double3" -1.9369548630898028 1.8553999576759053 0.93852458727102128 ;
createNode transform -n "PoleParentConstraintLeg_LStatic" -p "IKStatic";
	setAttr ".t" -type "double3" 1.936954863089803 1.8553999576759053 0.93852458727102017 ;
createNode transform -n "IKCrv" -p "IKSystem";
	setAttr ".it" no;
createNode transform -n "IKMessure" -p "IKSystem";
createNode transform -n "IKmessureLoc1Leg_R" -p "IKMessure";
	setAttr -l on ".v" no;
createNode locator -n "IKmessureLoc1Leg_RShape" -p "IKmessureLoc1Leg_R";
	setAttr -k off ".v";
createNode pointConstraint -n "IKmessureLoc1Leg_R_pointConstraint1" -p "IKmessureLoc1Leg_R";
	addAttr -ci true -k true -sn "w0" -ln "IKXTrack_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.9369647648037842 2.3360524237536708 0 ;
	setAttr -k on ".w0";
createNode transform -n "IKmessureLoc2Leg_R" -p "IKmessureLoc1Leg_R";
	setAttr -l on ".v" no;
createNode locator -n "IKmessureLoc2Leg_RShape" -p "IKmessureLoc2Leg_R";
	setAttr -k off ".v";
createNode pointConstraint -n "IKmessureLoc2Leg_R_pointConstraint1" -p "IKmessureLoc2Leg_R";
	addAttr -ci true -k true -sn "w0" -ln "IKmessureConstrainToLeg_RW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -3.523519586767776e-05 -0.93852459339137351 -1.2429658413637412e-08 ;
	setAttr -k on ".w0";
createNode transform -n "IKdistanceLeg_R" -p "IKMessure";
	setAttr -l on ".v" no;
createNode distanceDimShape -n "IKdistanceLeg_RShape" -p "IKdistanceLeg_R";
	addAttr -ci true -sn "antiPop" -ln "antiPop" -at "double";
	setAttr -k off ".v";
createNode transform -n "IKmessureLoc1Leg_L" -p "IKMessure";
	setAttr -l on ".v" no;
createNode locator -n "IKmessureLoc1Leg_LShape" -p "IKmessureLoc1Leg_L";
	setAttr -k off ".v";
createNode pointConstraint -n "IKmessureLoc1Leg_L_pointConstraint1" -p "IKmessureLoc1Leg_L";
	addAttr -ci true -k true -sn "w0" -ln "IKXTrack_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.9369647648037847 2.3360524237536708 -1.3797283313866929e-16 ;
	setAttr -k on ".w0";
createNode transform -n "IKmessureLoc2Leg_L" -p "IKmessureLoc1Leg_L";
	setAttr -l on ".v" no;
createNode locator -n "IKmessureLoc2Leg_LShape" -p "IKmessureLoc2Leg_L";
	setAttr -k off ".v";
createNode pointConstraint -n "IKmessureLoc2Leg_L_pointConstraint1" -p "IKmessureLoc2Leg_L";
	addAttr -ci true -k true -sn "w0" -ln "IKmessureConstrainToLeg_LW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 3.523519586767776e-05 -0.93852459339137329 -1.2429658275664579e-08 ;
	setAttr -k on ".w0";
createNode transform -n "IKdistanceLeg_L" -p "IKMessure";
	setAttr -l on ".v" no;
createNode distanceDimShape -n "IKdistanceLeg_LShape" -p "IKdistanceLeg_L";
	addAttr -ci true -sn "antiPop" -ln "antiPop" -at "double";
	setAttr -k off ".v";
createNode transform -n "FKIKSystem" -p "MotionSystem";
createNode transform -n "FKIKParentConstraintLeg_R" -p "FKIKSystem";
createNode transform -n "FKIKLeg_R" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Track";
	setAttr -l on ".middleJoint" -type "string" "TrackBend";
	setAttr -l on ".endJoint" -type "string" "TrackBottom";
createNode nurbsCurve -n "FKIKLeg_RShape" -p "FKIKLeg_R";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		-0.25215053900000001 -0.072043011150000003 -5.5988666819999994e-17
		-0.25215053900000001 -0.063037634760000003 -5.5988666819999994e-17
		-0.24314516259999999 -0.063037634760000003 -5.3989071580000002e-17
		-0.23413978620000001 -0.054032258360000002 -5.1989476330000002e-17
		-0.23413978620000001 0.054032258360000002 -5.1989476330000002e-17
		-0.24314516259999999 0.063037634760000003 -5.3989071580000002e-17
		-0.25215053900000001 0.063037634760000003 -5.5988666819999994e-17
		-0.25215053900000001 0.072043011150000003 -5.5988666819999994e-17
		-0.090053763940000003 0.072043011150000003 -1.9995952440000001e-17
		-0.090053763940000003 0.063037634760000003 -1.9995952440000001e-17
		-0.099059140330000003 0.063037634760000003 -2.1995547680000002e-17
		-0.1080645167 0.054032258360000002 -2.3995142920000003e-17
		-0.1080645167 0.009005376394 -2.3995142920000003e-17
		-0.054032258360000002 0.054032258360000002 -1.1997571460000001e-17
		-0.063037634760000003 0.063037634760000003 -1.3997166700000001e-17
		-0.072043011150000003 0.063037634760000003 -1.599676195e-17
		-0.072043011150000003 0.072043011150000003 -1.599676195e-17
		-0.018010752790000001 0.072043011150000003 -3.9991904870000001e-18
		-0.018010752790000001 0.063037634760000003 -3.9991904870000001e-18
		-0.027016129180000001 0.063037634760000003 -5.9987857309999997e-18
		-0.090053763940000003 0.009005376394 -1.9995952440000001e-17
		-0.018010752790000001 -0.063037634760000003 -3.9991904870000001e-18
		-0.009005376394 -0.063037634760000003 -1.999595244e-18
		0.036021505580000002 0.072043011150000003 7.9983809740000002e-18
		0.17110215149999999 0.072043011150000003 3.7992309629999995e-17
		0.17110215149999999 0.063037634760000003 3.7992309629999995e-17
		0.16209677510000001 0.063037634760000003 3.5992714380000002e-17
		0.15309139869999999 0.054032258360000002 3.3993119139999998e-17
		0.15309139869999999 0.009005376394 3.3993119139999998e-17
		0.2071236571 0.054032258360000002 4.5990690599999995e-17
		0.19811828070000001 0.063037634760000003 4.3991095360000003e-17
		0.1891129043 0.063037634760000003 4.1991500110000004e-17
		0.1891129043 0.072043011150000003 4.1991500110000004e-17
		0.24314516259999999 0.072043011150000003 5.3989071580000002e-17
		0.24314516259999999 0.063037634760000003 5.3989071580000002e-17
		0.23413978620000001 0.063037634760000003 5.1989476330000002e-17
		0.17110215149999999 0.009005376394 3.7992309629999995e-17
		0.24314516259999999 -0.063037634760000003 5.3989071580000002e-17
		0.25215053900000001 -0.063037634760000003 5.5988666819999994e-17
		0.25215053900000001 -0.072043011150000003 5.5988666819999994e-17
		0.19811828070000001 -0.072043011150000003 4.3991095360000003e-17
		0.19811828070000001 -0.063037634760000003 4.3991095360000003e-17
		0.2071236571 -0.063037634760000003 4.5990690599999995e-17
		0.15309139869999999 -0.009005376394 3.3993119139999998e-17
		0.15309139869999999 -0.054032258360000002 3.3993119139999998e-17
		0.16209677510000001 -0.063037634760000003 3.5992714380000002e-17
		0.17110215149999999 -0.063037634760000003 3.7992309629999995e-17
		0.17110215149999999 -0.072043011150000003 3.7992309629999995e-17
		0.1080645167 -0.072043011150000003 2.3995142920000003e-17
		0.1080645167 -0.063037634760000003 2.3995142920000003e-17
		0.1260752695 -0.063037634760000003 2.7994333409999997e-17
		0.13508064589999999 -0.054032258360000002 2.9993928650000001e-17
		0.13508064589999999 0.054032258360000002 2.9993928650000001e-17
		0.1260752695 0.063037634760000003 2.7994333409999997e-17
		0.099059140330000003 0.063037634760000003 2.1995547680000002e-17
		0.090053763940000003 0.054032258360000002 1.9995952440000001e-17
		0.090053763940000003 -0.054032258360000002 1.9995952440000001e-17
		0.099059140330000003 -0.063037634760000003 2.1995547680000002e-17
		0.1080645167 -0.063037634760000003 2.3995142920000003e-17
		0.1080645167 -0.072043011150000003 2.3995142920000003e-17
		0.054032258360000002 -0.072043011150000003 1.1997571460000001e-17
		0.054032258360000002 -0.063037634760000003 1.1997571460000001e-17
		0.063037634760000003 -0.063037634760000003 1.3997166700000001e-17
		0.072043011150000003 -0.054032258360000002 1.599676195e-17
		0.072043011150000003 0.054032258360000002 1.599676195e-17
		0.063037634760000003 0.063037634760000003 1.3997166700000001e-17
		0.045026881970000002 0.063037634760000003 9.997976217999999e-18
		0 -0.063037634760000003 0
		0 -0.072043011150000003 0
		-0.063037634760000003 -0.072043011150000003 -1.3997166700000001e-17
		-0.063037634760000003 -0.063037634760000003 -1.3997166700000001e-17
		-0.054032258360000002 -0.063037634760000003 -1.1997571460000001e-17
		-0.1080645167 -0.009005376394 -2.3995142920000003e-17
		-0.1080645167 -0.054032258360000002 -2.3995142920000003e-17
		-0.099059140330000003 -0.063037634760000003 -2.1995547680000002e-17
		-0.090053763940000003 -0.063037634760000003 -1.9995952440000001e-17
		-0.090053763940000003 -0.072043011150000003 -1.9995952440000001e-17
		-0.14408602230000001 -0.072043011150000003 -3.19935239e-17
		-0.14408602230000001 -0.063037634760000003 -3.19935239e-17
		-0.13508064589999999 -0.063037634760000003 -2.9993928650000001e-17
		-0.1260752695 -0.054032258360000002 -2.7994333409999997e-17
		-0.1260752695 0.054032258360000002 -2.7994333409999997e-17
		-0.13508064589999999 0.063037634760000003 -2.9993928650000001e-17
		-0.15309139869999999 0.063037634760000003 -3.3993119139999998e-17
		-0.15309139869999999 0.036021505580000002 -3.3993119139999998e-17
		-0.16209677510000001 0.036021505580000002 -3.5992714380000002e-17
		-0.16209677510000001 0.054032258360000002 -3.5992714380000002e-17
		-0.17110215149999999 0.063037634760000003 -3.7992309629999995e-17
		-0.2071236571 0.063037634760000003 -4.5990690599999995e-17
		-0.21612903350000001 0.054032258360000002 -4.799028585e-17
		-0.21612903350000001 0.009005376394 -4.799028585e-17
		-0.1891129043 0.009005376394 -4.1991500110000004e-17
		-0.18010752790000001 0.018010752790000001 -3.999190487e-17
		-0.18010752790000001 0.027016129180000001 -3.999190487e-17
		-0.17110215149999999 0.027016129180000001 -3.7992309629999995e-17
		-0.17110215149999999 -0.018010752790000001 -3.7992309629999995e-17
		-0.18010752790000001 -0.018010752790000001 -3.999190487e-17
		-0.18010752790000001 -0.009005376394 -3.999190487e-17
		-0.1891129043 0 -4.1991500110000004e-17
		-0.21612903350000001 0 -4.799028585e-17
		-0.21612903350000001 -0.054032258360000002 -4.799028585e-17
		-0.2071236571 -0.063037634760000003 -4.5990690599999995e-17
		-0.19811828070000001 -0.063037634760000003 -4.3991095360000003e-17
		-0.19811828070000001 -0.072043011150000003 -4.3991095360000003e-17
		-0.25215053900000001 -0.072043011150000003 -5.5988666819999994e-17
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_R_parentConstraint1" -p "FKIKParentConstraintLeg_R";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -2.9709991938297762 -0.23459232774033412 3.0787611408632261e-05 ;
	setAttr ".rst" -type "double3" -2.9709991938297762 2.1014600960133367 3.0787611408632261e-05 ;
	setAttr -k on ".w0";
createNode transform -n "FKIKParentConstraintLeg_L" -p "FKIKSystem";
createNode transform -n "FKIKLeg_L" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "FKIKBlend" -ln "FKIKBlend" -min 0 -max 10 -at "double";
	addAttr -ci true -sn "autoVis" -ln "autoVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "FKVis" -ln "FKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -k true -sn "IKVis" -ln "IKVis" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "startJoint" -ln "startJoint" -dt "string";
	addAttr -ci true -sn "middleJoint" -ln "middleJoint" -dt "string";
	addAttr -ci true -sn "endJoint" -ln "endJoint" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".FKIKBlend" 10;
	setAttr -cb on ".autoVis";
	setAttr -k on ".FKVis" no;
	setAttr -k on ".IKVis";
	setAttr -l on ".startJoint" -type "string" "Track";
	setAttr -l on ".middleJoint" -type "string" "TrackBend";
	setAttr -l on ".endJoint" -type "string" "TrackBottom";
createNode nurbsCurve -n "FKIKLeg_LShape" -p "FKIKLeg_L";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 104 0 no 3
		105 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81
		 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104
		105
		0.25215053900000001 -0.072043011150000003 -5.1046948789999999e-16
		0.25215053900000001 -0.063037634760000003 -5.1046948789999999e-16
		0.24314516259999999 -0.063037634760000003 -5.0847049010000002e-16
		0.23413978620000001 -0.054032258360000002 -5.0647149230000005e-16
		0.23413978620000001 0.054032258360000002 -5.0647149230000005e-16
		0.24314516259999999 0.063037634760000003 -5.0847049010000002e-16
		0.25215053900000001 0.063037634760000003 -5.1046948789999999e-16
		0.25215053900000001 0.072043011150000003 -5.1046948789999999e-16
		0.090053763940000003 0.072043011150000003 -4.7448075199999997e-16
		0.090053763940000003 0.063037634760000003 -4.7448075199999997e-16
		0.099059140330000003 0.063037634760000003 -4.7647974980000004e-16
		0.1080645167 0.054032258360000002 -4.7847874749999997e-16
		0.1080645167 0.009005376394 -4.7847874749999997e-16
		0.054032258360000002 0.054032258360000002 -4.6648476099999999e-16
		0.063037634760000003 0.063037634760000003 -4.6848375870000001e-16
		0.072043011150000003 0.063037634760000003 -4.7048275650000008e-16
		0.072043011150000003 0.072043011150000003 -4.7048275650000008e-16
		0.018010752790000001 0.072043011150000003 -4.5848199369999997e-16
		0.018010752790000001 0.063037634760000003 -4.5848199369999997e-16
		0.027016129180000001 0.063037634760000003 -4.6048099139999999e-16
		0.090053763940000003 0.009005376394 -4.7448075199999997e-16
		0.018010752790000001 -0.063037634760000003 -4.5848199369999997e-16
		0.009005376394 -0.063037634760000003 -4.564829959e-16
		-0.036021505580000002 0.072043011150000003 -4.4648800719999999e-16
		-0.17110215149999999 0.072043011150000003 -4.1648948829999995e-16
		-0.17110215149999999 0.063037634760000003 -4.1648948829999995e-16
		-0.16209677510000001 0.063037634760000003 -4.1848848609999997e-16
		-0.15309139869999999 0.054032258360000002 -4.2049426009999998e-16
		-0.15309139869999999 0.009005376394 -4.2049426009999998e-16
		-0.2071236571 0.054032258360000002 -4.0849349730000001e-16
		-0.19811828070000001 0.063037634760000003 -4.1049249499999999e-16
		-0.1891129043 0.063037634760000003 -4.1249149280000001e-16
		-0.1891129043 0.072043011150000003 -4.1249149280000001e-16
		-0.24314516259999999 0.072043011150000003 -4.0049750629999998e-16
		-0.24314516259999999 0.063037634760000003 -4.0049750629999998e-16
		-0.23413978620000001 0.063037634760000003 -4.0249650399999995e-16
		-0.17110215149999999 0.009005376394 -4.1648948829999995e-16
		-0.24314516259999999 -0.063037634760000003 -4.0049750629999998e-16
		-0.25215053900000001 -0.063037634760000003 -3.9849850850000001e-16
		-0.25215053900000001 -0.072043011150000003 -3.9849850850000001e-16
		-0.19811828070000001 -0.072043011150000003 -4.1049249499999999e-16
		-0.19811828070000001 -0.063037634760000003 -4.1049249499999999e-16
		-0.2071236571 -0.063037634760000003 -4.0849349730000001e-16
		-0.15309139869999999 -0.009005376394 -4.2049426009999998e-16
		-0.15309139869999999 -0.054032258360000002 -4.2049426009999998e-16
		-0.16209677510000001 -0.063037634760000003 -4.1848848609999997e-16
		-0.17110215149999999 -0.063037634760000003 -4.1648948829999995e-16
		-0.17110215149999999 -0.072043011150000003 -4.1648948829999995e-16
		-0.1080645167 -0.072043011150000003 -4.3048924880000003e-16
		-0.1080645167 -0.063037634760000003 -4.3048924880000003e-16
		-0.1260752695 -0.063037634760000003 -4.2649125330000004e-16
		-0.13508064589999999 -0.054032258360000002 -4.2449225559999997e-16
		-0.13508064589999999 0.054032258360000002 -4.2449225559999997e-16
		-0.1260752695 0.063037634760000003 -4.2649125330000004e-16
		-0.099059140330000003 0.063037634760000003 -4.324882466e-16
		-0.090053763940000003 0.054032258360000002 -4.3448724439999998e-16
		-0.090053763940000003 -0.054032258360000002 -4.3448724439999998e-16
		-0.099059140330000003 -0.063037634760000003 -4.324882466e-16
		-0.1080645167 -0.063037634760000003 -4.3048924880000003e-16
		-0.1080645167 -0.072043011150000003 -4.3048924880000003e-16
		-0.054032258360000002 -0.072043011150000003 -4.4248323540000001e-16
		-0.054032258360000002 -0.063037634760000003 -4.4248323540000001e-16
		-0.063037634760000003 -0.063037634760000003 -4.4048423760000004e-16
		-0.072043011150000003 -0.054032258360000002 -4.3848523989999997e-16
		-0.072043011150000003 0.054032258360000002 -4.3848523989999997e-16
		-0.063037634760000003 0.063037634760000003 -4.4048423760000004e-16
		-0.045026881970000002 0.063037634760000003 -4.4448900940000002e-16
		8.881784197e-16 -0.063037634760000003 -4.5448399819999998e-16
		8.881784197e-16 -0.072043011150000003 -4.5448399819999998e-16
		0.063037634760000003 -0.072043011150000003 -4.6848375870000001e-16
		0.063037634760000003 -0.063037634760000003 -4.6848375870000001e-16
		0.054032258360000002 -0.063037634760000003 -4.6648476099999999e-16
		0.1080645167 -0.009005376394 -4.7847874749999997e-16
		0.1080645167 -0.054032258360000002 -4.7847874749999997e-16
		0.099059140330000003 -0.063037634760000003 -4.7647974980000004e-16
		0.090053763940000003 -0.063037634760000003 -4.7448075199999997e-16
		0.090053763940000003 -0.072043011150000003 -4.7448075199999997e-16
		0.14408602230000001 -0.072043011150000003 -4.8647473850000005e-16
		0.14408602230000001 -0.063037634760000003 -4.8647473850000005e-16
		0.13508064589999999 -0.063037634760000003 -4.8447574080000003e-16
		0.1260752695 -0.054032258360000002 -4.8247674299999996e-16
		0.1260752695 0.054032258360000002 -4.8247674299999996e-16
		0.13508064589999999 0.063037634760000003 -4.8447574080000003e-16
		0.15309139869999999 0.063037634760000003 -4.8847373630000002e-16
		0.15309139869999999 0.036021505580000002 -4.8847373630000002e-16
		0.16209677510000001 0.036021505580000002 -4.9047951029999998e-16
		0.16209677510000001 0.054032258360000002 -4.9047951029999998e-16
		0.17110215149999999 0.063037634760000003 -4.9247850809999995e-16
		0.2071236571 0.063037634760000003 -5.0047449910000004e-16
		0.21612903350000001 0.054032258360000002 -5.0247349679999996e-16
		0.21612903350000001 0.009005376394 -5.0247349679999996e-16
		0.1891129043 0.009005376394 -4.9647650360000004e-16
		0.18010752790000001 0.018010752790000001 -4.9447750580000007e-16
		0.18010752790000001 0.027016129180000001 -4.9447750580000007e-16
		0.17110215149999999 0.027016129180000001 -4.9247850809999995e-16
		0.17110215149999999 -0.018010752790000001 -4.9247850809999995e-16
		0.18010752790000001 -0.018010752790000001 -4.9447750580000007e-16
		0.18010752790000001 -0.009005376394 -4.9447750580000007e-16
		0.1891129043 1.3322676300000001e-15 -4.9647650360000004e-16
		0.21612903350000001 1.3322676300000001e-15 -5.0247349679999996e-16
		0.21612903350000001 -0.054032258360000002 -5.0247349679999996e-16
		0.2071236571 -0.063037634760000003 -5.0047449910000004e-16
		0.19811828070000001 -0.063037634760000003 -4.9847550129999996e-16
		0.19811828070000001 -0.072043011150000003 -4.9847550129999996e-16
		0.25215053900000001 -0.072043011150000003 -5.1046948789999999e-16
		;
createNode parentConstraint -n "FKIKParentConstraintLeg_L_parentConstraint1" -p "FKIKParentConstraintLeg_L";
	addAttr -ci true -k true -sn "w0" -ln "Pelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 2.9709991938297762 -0.23459232774033367 3.0787611408362248e-05 ;
	setAttr ".rst" -type "double3" 2.9709991938297762 2.1014600960133372 3.0787611408362248e-05 ;
	setAttr -k on ".w0";
createNode transform -n "RootSystem" -p "MotionSystem";
createNode transform -n "PelvisCenter_M" -p "RootSystem";
	setAttr ".t" -type "double3" 0 2.3360524237536708 0 ;
	setAttr ".ro" 3;
createNode transform -n "PelvisCenterBtwLegs_M" -p "RootSystem";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 2.3360524237536708 -1.2429658080570505e-08 ;
	setAttr ".ro" 3;
createNode orientConstraint -n "PelvisCenterBtwLegs_M_orientConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "PelvisCenterBtwLegs_M_pointConstraint1" -p "PelvisCenterBtwLegs_M";
	addAttr -ci true -k true -sn "w0" -ln "IKLeg_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKLeg_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 2.2204460492503131e-16 1.3975278303622973 -1.2429658080570505e-08 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode transform -n "PelvisCenterBtwLegsOffset_M" -p "PelvisCenterBtwLegs_M";
	setAttr ".t" -type "double3" -2.2204460492503131e-16 0 1.2429658080570505e-08 ;
	setAttr ".ro" 3;
createNode transform -n "GlobalSystem" -p "MotionSystem";
createNode transform -n "GlobalOffsetShoulder_R" -p "GlobalSystem";
	setAttr ".t" -type "double3" -5.6343149540017086 5.2846318261672849 -2.450555136908755 ;
	setAttr ".r" -type "double3" -103.94253643557197 68.095821784597533 56.555271342127035 ;
	setAttr ".s" -type "double3" 0.99999999999999967 1 1.0000000000000004 ;
createNode transform -n "GlobalShoulder_R" -p "GlobalOffsetShoulder_R";
	setAttr ".t" -type "double3" 0 8.8817841970012523e-16 8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 1.0000000000000002 1 0.99999999999999989 ;
createNode transform -n "GlobalOffsetShoulder_L" -p "GlobalSystem";
	setAttr ".t" -type "double3" 5.634314954001705 5.284631826167284 -2.4505551369087537 ;
	setAttr ".r" -type "double3" 76.057463564428133 -68.09582178459749 -56.555271342127092 ;
	setAttr ".s" -type "double3" 0.99999999999999989 0.99999999999999978 1 ;
createNode transform -n "GlobalShoulder_L" -p "GlobalOffsetShoulder_L";
	setAttr ".t" -type "double3" 0 4.4408920985006262e-16 -8.8817841970012523e-16 ;
	setAttr ".ro" 5;
	setAttr ".s" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
createNode transform -n "GameSkeleton" -p "Main";
createNode joint -n "Pelvis_M" -p "GameSkeleton";
	setAttr ".ro" 3;
createNode joint -n "Body_M" -p "Pelvis_M";
createNode joint -n "Weapon_M" -p "Body_M";
createNode joint -n "Weapon_End_M" -p "Weapon_M";
	setAttr ".t" -type "double3" -6.2302109906456059e-17 0.78480521338340203 -4.4408920985006262e-16 ;
createNode parentConstraint -n "Weapon_M_parentConstraint1" -p "Weapon_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXWeapon_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -7.152557373046875e-07 0.94345192130518818 -1.7817480349361012 ;
	setAttr -k on ".w0";
createNode joint -n "Shoulder_R" -p "Body_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -103.94253507894264 68.095821784597533 56.555271342127057 ;
createNode joint -n "Elbow_R" -p "Shoulder_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -0.46684350781470363 -1.3269620535626434 109.38457411993316 ;
createNode joint -n "MiddleFinger1_R" -p "Elbow_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -163.66408744627753 74.44429790515477 168.39309078324212 ;
createNode joint -n "MiddleFinger2_R" -p "MiddleFinger1_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -12.778939641786762 -0.74819476325612777 5.9185852654567581 ;
createNode joint -n "MiddleFinger3_End_R" -p "MiddleFinger2_R";
	setAttr ".t" -type "double3" -8.8817841970012523e-16 0.13248348336405757 6.6613381477509392e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 3.6712936928303077 -2.2571645185354311e-05 0.0007042858659583851 ;
createNode parentConstraint -n "MiddleFinger2_R_parentConstraint1" -p "MiddleFinger2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.9593811057842251e-07 3.1144992298341974e-08 -2.0147232093576792e-07 ;
	setAttr ".rst" -type "double3" 5.9326620949207154e-08 0.690338747096227 1.2759122514083288e-07 ;
	setAttr ".rsrr" -type "double3" 4.0908565884391135e-06 4.4525591714724713e-06 1.0158492373939388e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "MiddleFinger1_R_parentConstraint1" -p "MiddleFinger1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.599544529297429e-06 3.4711872206728272e-06 -9.6329534117435938e-10 ;
	setAttr ".rst" -type "double3" 0.29765683003766397 2.6683616330818305 -0.023725118501068287 ;
	setAttr ".rsrr" -type "double3" 3.2992633682792236e-06 4.9477368103585969e-06 2.5776988683088238e-07 ;
	setAttr -k on ".w0";
createNode joint -n "IndexFinger1_R" -p "Elbow_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 56.240531654987706 -55.914025741901426 -47.541987269430322 ;
createNode joint -n "IndexFinger2_R" -p "IndexFinger1_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.7138995847900955 4.6117381418751968 2.4003652305853307 ;
createNode joint -n "IndexFinger3_End_R" -p "IndexFinger2_R";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 0.13732743200942066 1.7763568394002505e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 5.7599462155481103 0.0012471107435412636 -0.02478985867875385 ;
createNode parentConstraint -n "IndexFinger2_R_parentConstraint1" -p "IndexFinger2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -7.1134396805665921e-07 -2.294089220049907e-08 1.0010618134304826e-06 ;
	setAttr ".rst" -type "double3" -4.802511277546273e-08 0.59426302549315402 -1.682896559884739e-07 ;
	setAttr ".rsrr" -type "double3" -2.3971316471075245e-06 1.7511410681145737e-06 -2.9187529638117052e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IndexFinger1_R_parentConstraint1" -p "IndexFinger1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.0262273682435211e-07 1.3503733791627526e-06 1.0729817137275085e-06 ;
	setAttr ".rst" -type "double3" 0.04877203722553318 2.7288818908721995 0.28520553760322898 ;
	setAttr ".rsrr" -type "double3" -2.4649949376574232e-06 1.6100198345608194e-06 -1.9206340529137386e-07 ;
	setAttr -k on ".w0";
createNode joint -n "ThumbFinger1_R" -p "Elbow_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -41.96633851289063 -72.535901527326232 54.899725293291311 ;
createNode joint -n "ThumbFinger2_R" -p "ThumbFinger1_R";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0.0070819533803781242 0.10225955746863878 -4.0833991129701239 ;
createNode joint -n "ThumbFinger3_End_R" -p "ThumbFinger2_R";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 0.12953755254568627 -1.1102230246251565e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "ThumbFinger2_R_parentConstraint1" -p "ThumbFinger2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -8.3120574389057929e-07 1.1712470307375859e-07 -2.1245501498004844e-07 ;
	setAttr ".rst" -type "double3" -6.2799669464652652e-08 0.6384729079487379 -1.5013320564527532e-07 ;
	setAttr ".rsrr" -type "double3" -2.2755470607782819e-06 1.4888205721712516e-06 1.5740131702670902e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ThumbFinger1_R_parentConstraint1" -p "ThumbFinger1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -8.211279937257792e-07 1.7587446004265179e-07 2.8747207774697362e-06 ;
	setAttr ".rst" -type "double3" -0.1173984172111065 2.7357413702191646 -0.14142544325664463 ;
	setAttr ".rsrr" -type "double3" -2.1609592199500509e-06 1.6464900837692793e-06 1.7892126709144457e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Elbow_R_parentConstraint1" -p "Elbow_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.7153146862791342e-07 2.9685658010064102e-07 -1.8154180387292964e-06 ;
	setAttr ".rst" -type "double3" -6.0308997351654625e-08 3.0206925677459942 -7.1522896050169038e-08 ;
	setAttr ".rsrr" -type "double3" -2.2137718998988824e-07 1.5766239291739375e-06 -1.8154180308041839e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Shoulder_R_parentConstraint1" -p "Shoulder_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.3566293708103942e-06 -6.932303659710117e-07 1.1439267478096656e-06 ;
	setAttr ".rst" -type "double3" -5.6343156692574459 0.20712480712431613 -0.68573384593890663 ;
	setAttr ".rsrr" -type "double3" -1.3566293708103942e-06 -6.932303659710117e-07 1.1439267478096656e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Shoulder_L" -p "Body_M";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 76.057464921057459 -68.09582178459749 -56.555271342127078 ;
createNode joint -n "Elbow_L" -p "Shoulder_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -0.46684350781471246 -1.3269620535626745 109.38457411993305 ;
createNode joint -n "MiddleFinger1_L" -p "Elbow_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -163.66408744627722 74.44429790515477 168.39309078324249 ;
createNode joint -n "MiddleFinger2_L" -p "MiddleFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -12.778939641786762 -0.74819476325612722 5.9185852654567661 ;
createNode joint -n "MiddleFinger3_End_L" -p "MiddleFinger2_L";
	setAttr ".t" -type "double3" 0 -0.13248348336405602 -2.6645352591003757e-15 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 3.671293692830333 -2.2571645188456969e-05 0.00070428586595589892 ;
createNode parentConstraint -n "MiddleFinger2_L_parentConstraint1" -p "MiddleFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.9593814394939855e-07 3.1144988521433477e-08 -2.0147222738273374e-07 ;
	setAttr ".rst" -type "double3" -5.9326621837385574e-08 -0.69033874709622922 -1.2759122047789617e-07 ;
	setAttr ".rsrr" -type "double3" 4.090856450818002e-06 4.4525591681925268e-06 1.0158493087824882e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "MiddleFinger1_L_parentConstraint1" -p "MiddleFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXMiddleFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.5995445102140973e-06 3.4711872815009368e-06 -9.6318084120609778e-10 ;
	setAttr ".rst" -type "double3" -0.29765683003765159 -2.6683616330818336 0.023725118501065623 ;
	setAttr ".rsrr" -type "double3" 3.2992632283348147e-06 4.9477368262613728e-06 2.5776996634475506e-07 ;
	setAttr -k on ".w0";
createNode joint -n "IndexFinger1_L" -p "Elbow_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 56.24053165498772 -55.914025741901447 -47.541987269430301 ;
createNode joint -n "IndexFinger2_L" -p "IndexFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -3.7138995847900786 4.6117381418752021 2.4003652305853262 ;
createNode joint -n "IndexFinger3_End_L" -p "IndexFinger2_L";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 -0.1373274320094211 4.4408920985006262e-16 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 5.7599462155480543 0.0012471107435257864 -0.024789858678762829 ;
createNode parentConstraint -n "IndexFinger2_L_parentConstraint1" -p "IndexFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -7.1134400921750954e-07 -2.2940881267342331e-08 1.0010617515090589e-06 ;
	setAttr ".rst" -type "double3" 4.802511277546273e-08 -0.59426302549315624 1.6828965376802785e-07 ;
	setAttr ".rsrr" -type "double3" -2.397131555716274e-06 1.7511411042436862e-06 -2.9187526840222924e-07 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "IndexFinger1_L_parentConstraint1" -p "IndexFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXIndexFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.0262276624448257e-07 1.3503734141488541e-06 1.0729816485261382e-06 ;
	setAttr ".rst" -type "double3" -0.048772037225521106 -2.7288818908722021 -0.28520553760323075 ;
	setAttr ".rsrr" -type "double3" -2.4649948486018919e-06 1.6100198890278179e-06 -1.9206339097887786e-07 ;
	setAttr -k on ".w0";
createNode joint -n "ThumbFinger1_L" -p "Elbow_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" -41.966338512890566 -72.535901527326203 54.89972529329129 ;
createNode joint -n "ThumbFinger2_L" -p "ThumbFinger1_L";
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0.0070819533804097412 0.10225955746863698 -4.0833991129701213 ;
createNode joint -n "ThumbFinger3_End_L" -p "ThumbFinger2_L";
	setAttr ".t" -type "double3" 6.2172489379008766e-15 -0.12953755254568666 3.9968028886505635e-15 ;
	setAttr ".ro" 5;
createNode parentConstraint -n "ThumbFinger2_L_parentConstraint1" -p "ThumbFinger2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -8.3120581207372023e-07 1.1712471357998461e-07 -2.1245501822209984e-07 ;
	setAttr ".rst" -type "double3" 6.2799669464652652e-08 -0.63847290794874101 1.5013320209256165e-07 ;
	setAttr ".rsrr" -type "double3" -2.275546984263267e-06 1.4888204942642606e-06 1.5740131727256013e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ThumbFinger1_L_parentConstraint1" -p "ThumbFinger1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXThumbFinger1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -8.2112793011468609e-07 1.758744860772332e-07 2.8747207235990912e-06 ;
	setAttr ".rst" -type "double3" 0.11739841721111871 -2.7357413702191682 0.14142544325664286 ;
	setAttr ".rsrr" -type "double3" -2.1609591038598038e-06 1.6464900122192244e-06 1.7892126901220109e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Elbow_L_parentConstraint1" -p "Elbow_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXElbow_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.7153141614876067e-07 2.9685663695305415e-07 -1.8154178736898793e-06 ;
	setAttr ".rst" -type "double3" 6.0308998683922255e-08 -3.0206925677459937 7.1522897826525877e-08 ;
	setAttr ".rsrr" -type "double3" -2.2137717249684038e-07 1.5766239088979009e-06 -1.8154178465882586e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Shoulder_L_parentConstraint1" -p "Shoulder_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXShoulder_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.3566293275747286e-06 -6.9323034109808e-07 1.1439267869702455e-06 ;
	setAttr ".rst" -type "double3" 5.6343142387459677 0.20712480712431525 -0.6857338459389053 ;
	setAttr ".rsrr" -type "double3" -1.3566293275747286e-06 -6.9323034109808e-07 1.1439267869702455e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Body_M_parentConstraint1" -p "Body_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXBody_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 7.1525573730468729e-07 2.7414545952892979 -1.7648212909698484 ;
	setAttr -k on ".w0";
createNode joint -n "Track_R" -p "Pelvis_M";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 51.580167889976693 -0.0012779026280242873 179.99838883409456 ;
createNode joint -n "TrackBend_R" -p "Track_R";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" -104.50683096254765 -0.00086172398712344256 -0.00066713583844575913 ;
createNode joint -n "TrackBottom_R" -p "TrackBend_R";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 52.926663050628029 -0.0013444510444005004 0.0027008789346947238 ;
createNode joint -n "Wheel1_R" -p "TrackBottom_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277296642025e-05 0 -90.000000000000014 ;
createNode joint -n "Wheel1_End_R" -p "Wheel1_R";
	setAttr ".t" -type "double3" 6.6613381477509392e-16 1.4116922076074325 4.3645087544064154e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000014 ;
createNode parentConstraint -n "Wheel1_R_parentConstraint1" -p "Wheel1_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel1_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7297908426491595e-10 -2.3492052798761158e-12 -2.5044781609037557e-06 ;
	setAttr ".rst" -type "double3" 1.8839142986457358 0.51764255762561839 4.9243162660155679 ;
	setAttr ".rsrr" -type "double3" 2.7296149475495141e-10 -2.3492052976910101e-12 -2.5044781799870835e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel2_R" -p "TrackBottom_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277296642025e-05 0 -90.000000000000014 ;
createNode joint -n "Wheel2_End_R" -p "Wheel2_R";
	setAttr ".t" -type "double3" 1.3322676295501878e-15 1.4116922076074334 4.3574033270488144e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000014 ;
createNode parentConstraint -n "Wheel2_R_parentConstraint1" -p "Wheel2_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel2_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7297908426491595e-10 -2.3492052798761158e-12 -2.5044781609037557e-06 ;
	setAttr ".rst" -type "double3" 1.8372944138814689 0.26001828909370506 3.2643206664076931 ;
	setAttr ".rsrr" -type "double3" 2.7296851146303949e-10 -2.3492052735785979e-12 -2.5044781545426462e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel3_R" -p "TrackBottom_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277296642025e-05 0 -90.000000000000014 ;
createNode joint -n "Wheel3_End_R" -p "Wheel3_R";
	setAttr ".t" -type "double3" 2.4424906541753444e-15 1.4116922076074321 4.3567371932340393e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000014 ;
createNode parentConstraint -n "Wheel3_R_parentConstraint1" -p "Wheel3_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel3_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7297908426491595e-10 -2.3492052798761158e-12 -2.5044781609037557e-06 ;
	setAttr ".rst" -type "double3" 1.7610100589386415 4.3143266736933583e-12 1.0466412134814314 ;
	setAttr ".rsrr" -type "double3" 2.7296851146303949e-10 -2.3492052735785979e-12 -2.5044781545426462e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel4_R" -p "TrackBottom_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277296642025e-05 0 -90.000000000000014 ;
createNode joint -n "Wheel4_End_R" -p "Wheel4_R";
	setAttr ".t" -type "double3" 1.9984014443252818e-15 1.4116922076074316 4.3582915054685145e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000014 ;
createNode parentConstraint -n "Wheel4_R_parentConstraint1" -p "Wheel4_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel4_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7297908426491595e-10 -2.3492052798761158e-12 -2.5044781609037557e-06 ;
	setAttr ".rst" -type "double3" 1.7610124743337165 4.3127723614588831e-12 -1.5284062198937474 ;
	setAttr ".rsrr" -type "double3" 2.7296851146303949e-10 -2.3492052735785979e-12 -2.5044781545426462e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel5_R" -p "TrackBottom_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277296642025e-05 0 -90.000000000000014 ;
createNode joint -n "Wheel5_End_R" -p "Wheel5_R";
	setAttr ".t" -type "double3" 1.7763568394002505e-15 1.4116922076074343 4.3591796838882146e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000014 ;
createNode parentConstraint -n "Wheel5_R_parentConstraint1" -p "Wheel5_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel5_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7297908426491595e-10 -2.3492052798761158e-12 -2.5044781609037557e-06 ;
	setAttr ".rst" -type "double3" 1.8373009803280895 0.26001828909370128 -3.7361537865616365 ;
	setAttr ".rsrr" -type "double3" 2.7296851146303949e-10 -2.3492052735785979e-12 -2.5044781545426462e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel6_R" -p "TrackBottom_R";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277296642025e-05 0 -90.000000000000014 ;
createNode joint -n "Wheel6_End_R" -p "Wheel6_R";
	setAttr ".t" -type "double3" 3.3306690738754696e-16 1.4116922076074312 4.354738791789714e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000014 ;
createNode parentConstraint -n "Wheel6_R_parentConstraint1" -p "Wheel6_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel6_RW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7297908426491595e-10 -2.3492052798761158e-12 -2.5044781609037557e-06 ;
	setAttr ".rst" -type "double3" 1.8839239978084035 0.51764255762561373 -5.4159399481217241 ;
	setAttr ".rsrr" -type "double3" 2.7296851146303949e-10 -2.3492052735785979e-12 -2.5044781545426462e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "TrackBottom_R_parentConstraint1" -p "TrackBottom_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrackBottom_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrackBottom_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 180.00000149395228 -180.00000000000273 -180.00000000003865 ;
	setAttr ".rst" -type "double3" -3.4809864102314236e-08 0.75952924492005935 -2.0231059938424778e-08 ;
	setAttr ".rsrr" -type "double3" 0.00067919339325884218 2.1148301471106388 179.99998940602859 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "TrackBend_R_parentConstraint1" -p "TrackBend_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrackBend_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrackBend_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.5679643056921873e-06 -6.0630697608437435e-12 -1.1142820333384468e-11 ;
	setAttr ".rst" -type "double3" -1.7827257448033151e-08 0.77347563476454928 5.3018123225001546e-10 ;
	setAttr ".rsrr" -type "double3" -1.5161096535686879e-06 -1.6955423138626952e-06 
		1.2810989605857455e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Track_R_parentConstraint1" -p "Track_R";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrack_RW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrack_RW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -6.3611093629270367e-15 -1.9935539700818749e-14 -1.5811340225251585e-14 ;
	setAttr ".rst" -type "double3" -1.9369647648037842 0 -3.1194989163442764e-16 ;
	setAttr ".rsrr" -type "double3" 3.9273501824718256e-08 1.6649890804305722e-06 1.3205672966215287e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "Track_L" -p "Pelvis_M";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 51.580167889976678 179.99872209737197 0.0016111659054251952 ;
createNode joint -n "TrackBend_L" -p "Track_L";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" -104.50683096254762 -0.00086172398714931043 -0.00066713583849075362 ;
createNode joint -n "TrackBottom_L" -p "TrackBend_L";
	setAttr ".ro" 3;
	setAttr ".jo" -type "double3" 52.926663050628029 -0.0013444510444331631 0.0027008789346489761 ;
createNode joint -n "Wheel1_L" -p "TrackBottom_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277303658724e-05 0 -89.999999999999972 ;
createNode joint -n "Wheel1_End_L" -p "Wheel1_L";
	setAttr ".t" -type "double3" -7.7715611723760958e-16 -1.4116922076074334 -4.3582915054685145e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000028 ;
createNode parentConstraint -n "Wheel1_L_parentConstraint1" -p "Wheel1_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel1_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7296152186884673e-10 -2.3491708099686684e-12 -2.5044414127749661e-06 ;
	setAttr ".rst" -type "double3" -1.8839142986457387 -0.51764255762561906 -4.9243162660155759 ;
	setAttr ".rsrr" -type "double3" 2.729649789919164e-10 -2.3492053159664704e-12 -2.5044781927093024e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel2_L" -p "TrackBottom_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277303658724e-05 0 -89.999999999999972 ;
createNode joint -n "Wheel2_End_L" -p "Wheel2_L";
	setAttr ".t" -type "double3" -2.2204460492503131e-15 -1.4116922076074276 -4.354294702579864e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000028 ;
createNode parentConstraint -n "Wheel2_L_parentConstraint1" -p "Wheel2_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel2_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7296152186884673e-10 -2.3491708099686684e-12 -2.5044414127749661e-06 ;
	setAttr ".rst" -type "double3" -1.8372944138814726 -0.2600182890937035 -3.2643206664076967 ;
	setAttr ".rsrr" -type "double3" 2.729649789919164e-10 -2.3492053159664704e-12 -2.5044781927093024e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel3_L" -p "TrackBottom_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277303658724e-05 0 -89.999999999999972 ;
createNode joint -n "Wheel3_End_L" -p "Wheel3_L";
	setAttr ".t" -type "double3" -1.9984014443252818e-15 -1.4116922076074307 -4.3569592378389643e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000028 ;
createNode parentConstraint -n "Wheel3_L_parentConstraint1" -p "Wheel3_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel3_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7296152186884673e-10 -2.3491708099686684e-12 -2.5044414127749661e-06 ;
	setAttr ".rst" -type "double3" -1.7610100589386422 -4.3136605398785832e-12 -1.0466412134814307 ;
	setAttr ".rsrr" -type "double3" 2.729649789919164e-10 -2.3492053159664704e-12 -2.5044781927093024e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel4_L" -p "TrackBottom_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277303658724e-05 0 -89.999999999999972 ;
createNode joint -n "Wheel4_End_L" -p "Wheel4_L";
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -1.4116922076074303 -4.3591796838882146e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000028 ;
createNode parentConstraint -n "Wheel4_L_parentConstraint1" -p "Wheel4_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel4_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7296152186884673e-10 -2.3491708099686684e-12 -2.5044414127749661e-06 ;
	setAttr ".rst" -type "double3" -1.7610124743337154 -4.3127723614588831e-12 1.52840621989375 ;
	setAttr ".rsrr" -type "double3" 2.729649789919164e-10 -2.3492053159664704e-12 -2.5044781927093024e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel5_L" -p "TrackBottom_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277303658724e-05 0 -89.999999999999972 ;
createNode joint -n "Wheel5_End_L" -p "Wheel5_L";
	setAttr ".t" -type "double3" -2.2204460492503131e-15 -1.4116922076074299 -4.3560710594192642e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000028 ;
createNode parentConstraint -n "Wheel5_L_parentConstraint1" -p "Wheel5_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel5_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7296152186884673e-10 -2.3491708099686684e-12 -2.5044414127749661e-06 ;
	setAttr ".rst" -type "double3" -1.8373009803280866 -0.2600182890937015 3.736153786561641 ;
	setAttr ".rsrr" -type "double3" 2.729649789919164e-10 -2.3492053159664704e-12 -2.5044781927093024e-06 ;
	setAttr -k on ".w0";
createNode joint -n "Wheel6_L" -p "TrackBottom_L";
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 5.3743277303658724e-05 0 -89.999999999999972 ;
createNode joint -n "Wheel6_End_L" -p "Wheel6_L";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 -1.4116922076074325 -4.3520742565306136e-12 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000028 ;
createNode parentConstraint -n "Wheel6_L_parentConstraint1" -p "Wheel6_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXWheel6_LW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.7296152186884673e-10 -2.3491708099686684e-12 -2.5044414127749661e-06 ;
	setAttr ".rst" -type "double3" -1.8839239978083997 -0.51764255762561417 5.4159399481217267 ;
	setAttr ".rsrr" -type "double3" 2.729649789919164e-10 -2.3492053159664704e-12 -2.5044781927093024e-06 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "TrackBottom_L_parentConstraint1" -p "TrackBottom_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrackBottom_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrackBottom_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 1.5679643698511337e-06 359.99999999999733 3.8629176450105182e-11 ;
	setAttr ".rst" -type "double3" 3.4809917170974813e-08 -0.75952924492005913 2.0231061270692408e-08 ;
	setAttr ".rsrr" -type "double3" 0.0008187529494296695 0.11960486881604576 180.00000117533315 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "TrackBend_L_parentConstraint1" -p "TrackBend_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrackBend_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrackBend_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" -1.5679643820253115e-06 -6.1131102093969011e-12 -1.1129048579072413e-11 ;
	setAttr ".rst" -type "double3" 1.7827257225988546e-08 -0.77347563476454884 -5.3018123225001546e-10 ;
	setAttr ".rsrr" -type "double3" -1.516109698120522e-06 -1.695543404280211e-06 1.2811030149451192e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode parentConstraint -n "Track_L_parentConstraint1" -p "Track_L";
	addAttr -ci true -k true -sn "w0" -ln "FKXTrack_LW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "IKXTrack_LW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 0 9.7062825972397362e-20 ;
	setAttr ".rst" -type "double3" 1.9369647648037844 0 -3.9637987398809185e-16 ;
	setAttr ".rsrr" -type "double3" 3.9273508185779326e-08 1.6649932656834882e-06 1.3205673018952454e-06 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode pointConstraint -n "Pelvis_M_pointConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 2.3360524237536708 0 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "Pelvis_M_orientConstraint1" -p "Pelvis_M";
	addAttr -ci true -k true -sn "w0" -ln "FKXPelvis_MW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode nurbsCurve -n "forwardArrowShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 7 0 no 3
		8 0 1 2 3 4 5 6 7
		8
		-0.20251769380841786 4.4967961312018578e-17 1.667783260345459
		-0.40503538761683572 8.9935922624037156e-17 1.667783260345459
		-1.3490388393605578e-16 2.698077678721117e-16 2.2753363417707124
		0.40503538761683572 -8.9935922624037156e-17 1.667783260345459
		0.20251769380841786 -4.4967961312018578e-17 1.667783260345459
		0.20251769380841803 -3.1477572918413026e-16 1.0602301789202055
		-0.20251769380841775 -2.2483980656009309e-16 1.0602301789202055
		-0.20251769380841786 4.4967961312018578e-17 1.667783260345459
		;
createNode transform -n "items" -p "ctrl_rig";
	setAttr -l on ".it" no;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 16 ".lnk";
	setAttr -s 16 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
	setAttr -s 3 ".opt";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -sn "imrVerbosity" -ln "imrVerbosity" -dv 3 -at "short";
	addAttr -ci true -sn "imrThreads" -ln "imrThreads" -dv 1 -at "short";
	addAttr -ci true -sn "imrThreadsAdjust" -ln "imrThreadsAdjust" -dv 1 -min 0 -max 
		1 -at "bool";
	addAttr -ci true -sn "imrTaskOrder" -ln "imrTaskOrder" -dv 1 -at "short";
	addAttr -ci true -sn "imrTaskSize" -ln "imrTaskSize" -at "short";
	addAttr -ci true -sn "imrTaskAdjust" -ln "imrTaskAdjust" -dv 1 -at "short";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
	setAttr ".imrThreads" 4;
	setAttr ".imrThreadsAdjust" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode shadingEngine -n "irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode shadingEngine -n "BPRig_irisPuppetSG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "BPRig_materialInfo3";
createNode ikRPsolver -n "ikRPsolver";
createNode displayLayer -n "jointLayer";
createNode reverse -n "jointVisReverse";
createNode objectSet -n "ControlSet";
	setAttr ".ihi" 0;
	setAttr -s 85 ".dsm";
createNode objectSet -n "GameSet";
	setAttr ".ihi" 0;
	setAttr -s 37 ".dsm";
createNode objectSet -n "AllSet";
	setAttr ".ihi" 0;
	setAttr -s 423 ".dsm";
	setAttr -s 56 ".dnsm";
createNode objectSet -n "Sets";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dnsm";
createNode unitConversion -n "unitConversion1";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_RSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_R";
createNode condition -n "FKIKBlendLegCondition_R";
createNode setRange -n "FKIKBlendLegsetRange_R";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode blendColors -n "ScaleBlendTrackBottom_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendTrackBend_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendTrack_R";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode setRange -n "IKSetRangeStretchLeg_R";
	setAttr ".m" -type "float3" 0 1 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode setRange -n "IKSetRangeAntiPopLeg_R";
	setAttr ".m" -type "float3" 0 1 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "IKmessureDivLeg_R";
	setAttr ".op" 2;
	setAttr ".i2" -type "float3" 1 1.5330049 1 ;
createNode blendTwoAttr -n "IKmessureBlendAntiPopLeg_R";
	setAttr -s 2 ".i";
	setAttr -s 2 ".i";
createNode animCurveUU -n "IKdistanceLeg_RShape_antiPop";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0.15330049395561218 1.5330048801980953 
		0.38325121998786926 1.5330048801980953 1.5330048799514771 1.5330048801980953 1.8396058082580566 
		1.8396058562377142;
	setAttr -s 4 ".kit[3]"  2;
	setAttr -s 4 ".kot[3]"  2;
	setAttr -s 4 ".ktl[1:3]" no no yes;
	setAttr -s 4 ".kix[0:3]"  1 0.22995069622993469 0.58132672309875488 
		0.70710664987564087;
	setAttr -s 4 ".kiy[0:3]"  0 0 0.81367021799087524 0.7071068286895752;
	setAttr -s 4 ".kox[0:3]"  1 0.61197483539581299 0.70710664987564087 
		1;
	setAttr -s 4 ".koy[0:3]"  0 -0.79087716341018677 0.7071068286895752 
		0;
	setAttr ".pst" 1;
createNode animCurveUU -n "IKdistanceLeg_RShape_normal";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0.15330049395561218 1.5330048801980953 
		0.38325121998786926 1.5330048801980953 1.5330048799514771 1.5330048801980953 1.8396058082580566 
		1.8396058562377142;
	setAttr -s 4 ".kit[3]"  2;
	setAttr -s 4 ".kot[3]"  2;
	setAttr -s 4 ".kix[0:3]"  1 0.22995069622993469 1 0.70710664987564087;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0.7071068286895752;
	setAttr -s 4 ".kox[0:3]"  1 1.1497536897659302 0.70710664987564087 
		1;
	setAttr -s 4 ".koy[0:3]"  0 0 0.7071068286895752 0;
	setAttr ".pst" 1;
createNode clamp -n "IKdistanceClampLeg_R";
	setAttr ".mx" -type "float3" 1.5330049 0 0 ;
createNode blendTwoAttr -n "IKmessureBlendStretchLeg_R";
	setAttr -s 2 ".i";
	setAttr -s 2 ".i";
createNode multiplyDivide -n "IKXTrackBend_R_IKmessureDiv_R";
createNode multiplyDivide -n "IKXTrackBend_R_IKLength_R";
	setAttr ".i2" -type "float3" 1 0.77347565 1 ;
createNode multiplyDivide -n "IKXTrackBottom_R_IKmessureDiv_R";
createNode multiplyDivide -n "IKXTrackBottom_R_IKLength_R";
	setAttr ".i2" -type "float3" 1 0.75952923 1 ;
createNode plusMinusAverage -n "ScaleBlendAddYTrackBend_R";
	setAttr -s 3 ".i1[2]"  -1;
	setAttr -s 2 ".i1";
createNode unitConversion -n "GlobalShoulder_unitConversion_R";
	setAttr ".cf" 0.1;
createNode reverse -n "GlobalShoulder_reverse_R";
createNode unitConversion -n "unitConversion2";
	setAttr ".cf" 0.1;
createNode setRange -n "PoleLeg_LSetRangeFollow";
	setAttr ".n" -type "float3" 0 1 0 ;
	setAttr ".m" -type "float3" 1 0 0 ;
	setAttr ".om" -type "float3" 10 10 0 ;
createNode unitConversion -n "FKIKBlendLegUnitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "FKIKBlendLegReverse_L";
createNode condition -n "FKIKBlendLegCondition_L";
createNode setRange -n "FKIKBlendLegsetRange_L";
	setAttr ".n" -type "float3" 10 0 0 ;
	setAttr ".om" -type "float3" 10 0 0 ;
createNode blendColors -n "ScaleBlendTrackBottom_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendTrackBend_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode blendColors -n "ScaleBlendTrack_L";
	setAttr ".c1" -type "float3" 1 1 1 ;
createNode setRange -n "IKSetRangeStretchLeg_L";
	setAttr ".m" -type "float3" 0 1 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode setRange -n "IKSetRangeAntiPopLeg_L";
	setAttr ".m" -type "float3" 0 1 0 ;
	setAttr ".om" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "IKmessureDivLeg_L";
	setAttr ".op" 2;
	setAttr ".i2" -type "float3" 1 1.5330049 1 ;
createNode blendTwoAttr -n "IKmessureBlendAntiPopLeg_L";
	setAttr -s 2 ".i";
	setAttr -s 2 ".i";
createNode animCurveUU -n "IKdistanceLeg_LShape_antiPop";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0.15330049395561218 1.5330048801980956 
		0.38325121998786926 1.5330048801980956 1.5330048799514771 1.5330048801980956 1.8396058082580566 
		1.8396058562377147;
	setAttr -s 4 ".kit[3]"  2;
	setAttr -s 4 ".kot[3]"  2;
	setAttr -s 4 ".ktl[1:3]" no no yes;
	setAttr -s 4 ".kix[0:3]"  1 0.22995069622993469 0.58132672309875488 
		0.70710664987564087;
	setAttr -s 4 ".kiy[0:3]"  0 0 0.81367021799087524 0.7071068286895752;
	setAttr -s 4 ".kox[0:3]"  1 0.61197483539581299 0.70710664987564087 
		1;
	setAttr -s 4 ".koy[0:3]"  0 -0.79087716341018677 0.7071068286895752 
		0;
	setAttr ".pst" 1;
createNode animCurveUU -n "IKdistanceLeg_LShape_normal";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0.15330049395561218 1.5330048801980956 
		0.38325121998786926 1.5330048801980956 1.5330048799514771 1.5330048801980956 1.8396058082580566 
		1.8396058562377147;
	setAttr -s 4 ".kit[3]"  2;
	setAttr -s 4 ".kot[3]"  2;
	setAttr -s 4 ".kix[0:3]"  1 0.22995069622993469 1 0.70710664987564087;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0.7071068286895752;
	setAttr -s 4 ".kox[0:3]"  1 1.1497536897659302 0.70710664987564087 
		1;
	setAttr -s 4 ".koy[0:3]"  0 0 0.7071068286895752 0;
	setAttr ".pst" 1;
createNode clamp -n "IKdistanceClampLeg_L";
	setAttr ".mx" -type "float3" 1.5330049 0 0 ;
createNode blendTwoAttr -n "IKmessureBlendStretchLeg_L";
	setAttr -s 2 ".i";
	setAttr -s 2 ".i";
createNode multiplyDivide -n "IKXTrackBend_L_IKmessureDiv_L";
createNode multiplyDivide -n "IKXTrackBend_L_IKLength_L";
	setAttr ".i2" -type "float3" 1 -0.77347565 1 ;
createNode multiplyDivide -n "IKXTrackBottom_L_IKmessureDiv_L";
createNode multiplyDivide -n "IKXTrackBottom_L_IKLength_L";
	setAttr ".i2" -type "float3" 1 -0.75952923 1 ;
createNode plusMinusAverage -n "ScaleBlendAddYTrackBend_L";
	setAttr -s 3 ".i1[2]"  -1;
	setAttr -s 2 ".i1";
createNode unitConversion -n "GlobalShoulder_unitConversion_L";
	setAttr ".cf" 0.1;
createNode reverse -n "GlobalShoulder_reverse_L";
createNode setRange -n "CenterBtwFeet_M";
	setAttr ".v" -type "float3" 0 1 0 ;
	setAttr ".n" -type "float3" 2 0 0 ;
	setAttr ".om" -type "float3" 0.1 0 0 ;
createNode unitConversion -n "PelvisCenterBtwFeetUnitConversion_M";
	setAttr ".cf" 0.1;
createNode reverse -n "PelvisCenterBtwFeetReverse_M";
createNode dagPose -n "buildPose";
	addAttr -ci true -sn "udAttr" -ln "udAttr" -dt "string";
	setAttr ".udAttr" -type "string" (
		"setAttr PoleExtraLeg_L.translateX 0;setAttr PoleExtraLeg_L.translateY 0;setAttr PoleExtraLeg_L.translateZ 0;setAttr PoleLeg_L.translateX 0;setAttr PoleLeg_L.translateY 0;setAttr PoleLeg_L.translateZ 0;setAttr PoleLeg_L.follow 10;setAttr FKIKLeg_L.FKIKBlend 10;setAttr FKIKLeg_L.FKVis 0;setAttr FKIKLeg_L.IKVis 1;setAttr CenterExtra_M.translateX 0;setAttr CenterExtra_M.translateY 0;setAttr CenterExtra_M.translateZ 0;setAttr CenterExtra_M.rotateX 0;setAttr CenterExtra_M.rotateY -0;setAttr CenterExtra_M.rotateZ 0;setAttr Center_M.translateX 0;setAttr Center_M.translateY 0;setAttr Center_M.translateZ 0;setAttr Center_M.rotateX 0;setAttr Center_M.rotateY -0;setAttr Center_M.rotateZ 0;setAttr Center_M.CenterBtwFeet 0;setAttr FKWheel5_L.translateX 0;setAttr FKWheel5_L.translateY 0;setAttr FKWheel5_L.translateZ 0;setAttr FKWheel5_L.rotateX -0;setAttr FKWheel5_L.rotateY 0;setAttr FKWheel5_L.rotateZ 0;setAttr FKWheel5_L.scaleX 1;setAttr FKWheel5_L.scaleY 1;setAttr FKWheel5_L.scaleZ 1;setAttr FKExtraWheel6_L.translateX -1.110223025e-16;setAttr FKExtraWheel6_L.translateY -4.440892099e-16;setAttr FKExtraWheel6_L.translateZ -1.776356839e-15;setAttr FKExtraWheel6_L.rotateX -7.016709299e-15;setAttr FKExtraWheel6_L.rotateY 7.016709299e-15;setAttr FKExtraWheel6_L.rotateZ -1.272221873e-14;setAttr FKExtraWheel6_L.scaleX 1;setAttr FKExtraWheel6_L.scaleY 1;setAttr FKExtraWheel6_L.scaleZ 1;setAttr FKWheel6_L.translateX 0;setAttr FKWheel6_L.translateY 0;setAttr FKWheel6_L.translateZ 0;setAttr FKWheel6_L.rotateX -0;setAttr FKWheel6_L.rotateY 0;setAttr FKWheel6_L.rotateZ 0;setAttr FKWheel6_L.scaleX 1;setAttr FKWheel6_L.scaleY 1;setAttr FKWheel6_L.scaleZ 1;setAttr FKExtraTrackBottom_L.translateX 0;setAttr FKExtraTrackBottom_L.translateY -2.220446049e-16;setAttr FKExtraTrackBottom_L.translateZ 0;setAttr FKExtraTrackBottom_L.rotateX -0;setAttr FKExtraTrackBottom_L.rotateY 2.544443745e-14;setAttr FKExtraTrackBottom_L.rotateZ 0;setAttr FKExtraTrackBottom_L.scaleX 1;setAttr FKExtraTrackBottom_L.scaleY 1;setAttr FKExtraTrackBottom_L.scaleZ 1;setAttr FKTrackBottom_L.translateX 0;setAttr FKTrackBottom_L.translateY 0;setAttr FKTrackBottom_L.translateZ 0;setAttr FKTrackBottom_L.rotateX -0;setAttr FKTrackBottom_L.rotateY -0;setAttr FKTrackBottom_L.rotateZ 0;setAttr FKTrackBottom_L.scaleX 1;setAttr FKTrackBottom_L.scaleY 1;setAttr FKTrackBottom_L.scaleZ 1;setAttr FKExtraTrackBend_L.translateX 0;setAttr FKExtraTrackBend_L.translateY 2.220446049e-16;setAttr FKExtraTrackBend_L.translateZ 2.220446049e-16;setAttr FKExtraTrackBend_L.rotateX 6.361109363e-15;setAttr FKExtraTrackBend_L.rotateY -8.229568763e-15;setAttr FKExtraTrackBend_L.rotateZ -8.083780398e-15;setAttr FKExtraTrackBend_L.scaleX 1;setAttr FKExtraTrackBend_L.scaleY 1;setAttr FKExtraTrackBend_L.scaleZ 1;setAttr FKTrackBend_L.translateX 2.220446049e-16;setAttr FKTrackBend_L.translateY 2.220446049e-16;setAttr FKTrackBend_L.translateZ 4.440892099e-16;setAttr FKTrackBend_L.rotateX -0;setAttr FKTrackBend_L.rotateY -0;setAttr FKTrackBend_L.rotateZ 0;setAttr FKTrackBend_L.scaleX 1;setAttr FKTrackBend_L.scaleY 1;setAttr FKTrackBend_L.scaleZ 1;setAttr FKExtraTrack_L.translateX -2.220446049e-16;setAttr FKExtraTrack_L.translateY 0;setAttr FKExtraTrack_L.translateZ 4.440892099e-16;setAttr FKExtraTrack_L.rotateX -6.498620644e-31;setAttr FKExtraTrack_L.rotateY -7.348092709e-15;setAttr FKExtraTrack_L.rotateZ -1.013442672e-14;setAttr FKExtraTrack_L.scaleX 1;setAttr FKExtraTrack_L.scaleY 1;setAttr FKExtraTrack_L.scaleZ 1;setAttr FKTrack_L.translateX 0;setAttr FKTrack_L.translateY 0;setAttr FKTrack_L.translateZ 2.220446049e-16;setAttr FKTrack_L.rotateX -0;setAttr FKTrack_L.rotateY -0;setAttr FKTrack_L.rotateZ 0;setAttr FKTrack_L.scaleX 1;setAttr FKTrack_L.scaleY 1;setAttr FKTrack_L.scaleZ 1;setAttr IKExtraLeg_R.translateX 0;setAttr IKExtraLeg_R.translateY 0;setAttr IKExtraLeg_R.translateZ 0;setAttr IKExtraLeg_R.rotateX -0;setAttr IKExtraLeg_R.rotateY -0;setAttr IKExtraLeg_R.rotateZ 0;setAttr IKLeg_R.translateX 0;setAttr IKLeg_R.translateY 0;setAttr IKLeg_R.translateZ 0;setAttr IKLeg_R.rotateX -0;setAttr IKLeg_R.rotateY -0;setAttr IKLeg_R.rotateZ 0;setAttr IKLeg_R.swivel 0;setAttr IKLeg_R.stretchy 0;setAttr IKLeg_R.antiPop 0;setAttr IKLeg_R.Length1 1;setAttr IKLeg_R.Length2 1;setAttr PoleExtraLeg_R.translateX 0;setAttr PoleExtraLeg_R.translateY 0;setAttr PoleExtraLeg_R.translateZ 0;setAttr PoleLeg_R.translateX 0;setAttr PoleLeg_R.translateY 0;setAttr PoleLeg_R.translateZ 0;setAttr PoleLeg_R.follow 10;setAttr FKIKLeg_R.FKIKBlend 10;setAttr FKIKLeg_R.FKVis 0;setAttr FKIKLeg_R.IKVis 1;setAttr IKExtraLeg_L.translateX 0;setAttr IKExtraLeg_L.translateY 2.220446049e-16;setAttr IKExtraLeg_L.translateZ 1.654361225e-24;setAttr IKExtraLeg_L.rotateX -0;setAttr IKExtraLeg_L.rotateY -0;setAttr IKExtraLeg_L.rotateZ 0;setAttr IKLeg_L.translateX 0;setAttr IKLeg_L.translateY 0;setAttr IKLeg_L.translateZ 0;setAttr IKLeg_L.rotateX -0;setAttr IKLeg_L.rotateY -0;setAttr IKLeg_L.rotateZ 0;setAttr IKLeg_L.swivel 0;setAttr IKLeg_L.stretchy 0;setAttr IKLeg_L.antiPop 0;setAttr IKLeg_L.Length1 1;setAttr IKLeg_L.Length2 1;setAttr Main.visibility 1;setAttr Main.translateX 0;setAttr Main.translateY 0;setAttr Main.translateZ 0;setAttr Main.rotateX 0;setAttr Main.rotateY 0;setAttr Main.rotateZ 0;setAttr Main.scaleX 1;setAttr Main.scaleY 1;setAttr Main.scaleZ 1;setAttr FKExtraWeapon_M.translateX 0;setAttr FKExtraWeapon_M.translateY 0;setAttr FKExtraWeapon_M.translateZ 0;setAttr FKExtraWeapon_M.rotateX 0;setAttr FKExtraWeapon_M.rotateY -0;setAttr FKExtraWeapon_M.rotateZ 0;setAttr FKExtraWeapon_M.scaleX 1;setAttr FKExtraWeapon_M.scaleY 1;setAttr FKExtraWeapon_M.scaleZ 1;setAttr FKWeapon_M.translateX 0;setAttr FKWeapon_M.translateY 0;setAttr FKWeapon_M.translateZ 0;setAttr FKWeapon_M.rotateX 0;setAttr FKWeapon_M.rotateY -0;setAttr FKWeapon_M.rotateZ 0;setAttr FKWeapon_M.scaleX 1;setAttr FKWeapon_M.scaleY 1;setAttr FKWeapon_M.scaleZ 1;setAttr FKExtraMiddleFinger2_R.translateX 0;setAttr FKExtraMiddleFinger2_R.translateY -2.220446049e-16;setAttr FKExtraMiddleFinger2_R.translateZ 0;setAttr FKExtraMiddleFinger2_R.rotateX -0;setAttr FKExtraMiddleFinger2_R.rotateY 0;setAttr FKExtraMiddleFinger2_R.rotateZ -0;setAttr FKExtraMiddleFinger2_R.scaleX 1;setAttr FKExtraMiddleFinger2_R.scaleY 1;setAttr FKExtraMiddleFinger2_R.scaleZ 1;setAttr FKMiddleFinger2_R.translateX 0;setAttr FKMiddleFinger2_R.translateY -2.220446049e-16;setAttr FKMiddleFinger2_R.translateZ 2.220446049e-16;setAttr FKMiddleFinger2_R.rotateX -0;setAttr FKMiddleFinger2_R.rotateY 0;setAttr FKMiddleFinger2_R.rotateZ -0;setAttr FKMiddleFinger2_R.scaleX 1;setAttr FKMiddleFinger2_R.scaleY 1;setAttr FKMiddleFinger2_R.scaleZ 1;setAttr FKExtraMiddleFinger1_R.translateX 8.881784197e-16;setAttr FKExtraMiddleFinger1_R.translateY 3.330669074e-16;setAttr FKExtraMiddleFinger1_R.translateZ 2.220446049e-16;setAttr FKExtraMiddleFinger1_R.rotateX -0;setAttr FKExtraMiddleFinger1_R.rotateY 0;setAttr FKExtraMiddleFinger1_R.rotateZ -0;setAttr FKExtraMiddleFinger1_R.scaleX 1;setAttr FKExtraMiddleFinger1_R.scaleY 1;setAttr FKExtraMiddleFinger1_R.scaleZ 1;setAttr FKMiddleFinger1_R.translateX 8.881784197e-16;setAttr FKMiddleFinger1_R.translateY 2.220446049e-16;setAttr FKMiddleFinger1_R.translateZ -4.440892099e-16;setAttr FKMiddleFinger1_R.rotateX -0;setAttr FKMiddleFinger1_R.rotateY 0;setAttr FKMiddleFinger1_R.rotateZ -0;setAttr FKMiddleFinger1_R.scaleX 1;setAttr FKMiddleFinger1_R.scaleY 1;setAttr FKMiddleFinger1_R.scaleZ 1;setAttr FKExtraIndexFinger2_R.translateX 0;setAttr FKExtraIndexFinger2_R.translateY 4.440892099e-16;setAttr FKExtraIndexFinger2_R.translateZ 4.440892099e-16;setAttr FKExtraIndexFinger2_R.rotateX -0;setAttr FKExtraIndexFinger2_R.rotateY 0;setAttr FKExtraIndexFinger2_R.rotateZ -0;setAttr FKExtraIndexFinger2_R.scaleX 1;setAttr FKExtraIndexFinger2_R.scaleY 1;setAttr FKExtraIndexFinger2_R.scaleZ 1;setAttr FKIndexFinger2_R.translateX 8.881784197e-16;setAttr FKIndexFinger2_R.translateY 8.881784197e-16;setAttr FKIndexFinger2_R.translateZ 4.440892099e-16;setAttr FKIndexFinger2_R.rotateX -0;setAttr FKIndexFinger2_R.rotateY 0;setAttr FKIndexFinger2_R.rotateZ -0;setAttr FKIndexFinger2_R.scaleX 1;setAttr FKIndexFinger2_R.scaleY 1;setAttr FKIndexFinger2_R.scaleZ 1;setAttr FKExtraIndexFinger1_R.translateX 0;setAttr FKExtraIndexFinger1_R.translateY 8.881784197e-16;setAttr FKExtraIndexFinger1_R.translateZ 0;setAttr FKExtraIndexFinger1_R.rotateX -0;setAttr FKExtraIndexFinger1_R.rotateY 0;setAttr FKExtraIndexFinger1_R.rotateZ -0;setAttr FKExtraIndexFinger1_R.scaleX 1;setAttr FKExtraIndexFinger1_R.scaleY 1;setAttr FKExtraIndexFinger1_R.scaleZ 1;setAttr FKIndexFinger1_R.translateX -8.881784197e-16;setAttr FKIndexFinger1_R.translateY 8.881784197e-16;setAttr FKIndexFinger1_R.translateZ 4.440892099e-16;setAttr FKIndexFinger1_R.rotateX -0;setAttr FKIndexFinger1_R.rotateY 0;setAttr FKIndexFinger1_R.rotateZ -0;setAttr FKIndexFinger1_R.scaleX 1;setAttr FKIndexFinger1_R.scaleY 1;setAttr FKIndexFinger1_R.scaleZ 1;setAttr FKExtraThumbFinger2_R.translateX 0;setAttr FKExtraThumbFinger2_R.translateY -1.110223025e-16;setAttr FKExtraThumbFinger2_R.translateZ -8.881784197e-16;setAttr FKExtraThumbFinger2_R.rotateX -0;setAttr FKExtraThumbFinger2_R.rotateY 0;setAttr FKExtraThumbFinger2_R.rotateZ -0;setAttr FKExtraThumbFinger2_R.scaleX 1;setAttr FKExtraThumbFinger2_R.scaleY 1;setAttr FKExtraThumbFinger2_R.scaleZ 1;setAttr FKThumbFinger2_R.translateX 8.881784197e-16;setAttr FKThumbFinger2_R.translateY 1.110223025e-16;setAttr FKThumbFinger2_R.translateZ -8.881784197e-16;setAttr FKThumbFinger2_R.rotateX -0;setAttr FKThumbFinger2_R.rotateY 0;setAttr FKThumbFinger2_R.rotateZ -0;setAttr FKThumbFinger2_R.scaleX 1;setAttr FKThumbFinger2_R.scaleY 1;setAttr FKThumbFinger2_R.scaleZ 1;setAttr FKExtraThumbFinger1_R.translateX 8.881784197e-16;setAttr FKExtraThumbFinger1_R.translateY 0;setAttr FKExtraThumbFinger1_R.translateZ 6.661338148e-16;setAttr FKExtraThumbFinger1_R.rotateX -0;setAttr FKExtraThumbFinger1_R.rotateY 0;setAttr FKExtraThumbFinger1_R.rotateZ -0;setAttr FKExtraThumbFinger1_R.scaleX 1;setAttr FKExtraThumbFinger1_R.scaleY 1;setAttr FKExtraThumbFinger1_R.scaleZ 1;setAttr FKThumbFinger1_R.translateX 1.776356839e-15;setAttr FKThumbFinger1_R.translateY -2.220446049e-16;setAttr FKThumbFinger1_R.translateZ 0;setAttr FKThumbFinger1_R.rotateX -0;setAttr FKThumbFinger1_R.rotateY 0;setAttr FKThumbFinger1_R.rotateZ -0;setAttr FKThumbFinger1_R.scaleX 1;setAttr FKThumbFinger1_R.scaleY 1;setAttr FKThumbFinger1_R.scaleZ 1;setAttr FKExtraElbow_R.translateX 3.053113318e-16;setAttr FKExtraElbow_R.translateY 8.881784197e-16;setAttr FKExtraElbow_R.translateZ -8.881784197e-16;setAttr FKExtraElbow_R.rotateX -0;setAttr FKExtraElbow_R.rotateY 0;setAttr FKExtraElbow_R.rotateZ -0;setAttr FKExtraElbow_R.scaleX 1;setAttr FKExtraElbow_R.scaleY 1;setAttr FKExtraElbow_R.scaleZ 1;setAttr FKElbow_R.translateX 2.636779683e-16;setAttr FKElbow_R.translateY 8.881784197e-16;setAttr FKElbow_R.translateZ 0;setAttr FKElbow_R.rotateX -0;setAttr FKElbow_R.rotateY 0;setAttr FKElbow_R.rotateZ -0;setAttr FKElbow_R.scaleX 1;setAttr FKElbow_R.scaleY 1;setAttr FKElbow_R.scaleZ 1;setAttr FKExtraShoulder_R.translateX -4.440892099e-16;setAttr FKExtraShoulder_R.translateY 4.440892099e-16;setAttr FKExtraShoulder_R.translateZ 8.881784197e-16;setAttr FKExtraShoulder_R.rotateX -0;setAttr FKExtraShoulder_R.rotateY 0;setAttr FKExtraShoulder_R.rotateZ -0;setAttr FKExtraShoulder_R.scaleX 1;setAttr FKExtraShoulder_R.scaleY 1;setAttr FKExtraShoulder_R.scaleZ 1;setAttr FKShoulder_R.translateX 4.440892099e-16;setAttr FKShoulder_R.translateY 0;setAttr FKShoulder_R.translateZ 8.881784197e-16;setAttr FKShoulder_R.rotateX -0;setAttr FKShoulder_R.rotateY 0;setAttr FKShoulder_R.rotateZ -0;setAttr FKShoulder_R.scaleX 1;setAttr FKShoulder_R.scaleY 1;setAttr FKShoulder_R.scaleZ 1;setAttr FKShoulder_R.Global 10;setAttr FKExtraBody_M.translateX 0;setAttr FKExtraBody_M.translateY 0;setAttr FKExtraBody_M.translateZ 0;setAttr FKExtraBody_M.rotateX 0;setAttr FKExtraBody_M.rotateY -0;setAttr FKExtraBody_M.rotateZ 0;setAttr FKExtraBody_M.scaleX 1;setAttr FKExtraBody_M.scaleY 1;setAttr FKExtraBody_M.scaleZ 1;setAttr FKBody_M.translateX 0;setAttr FKBody_M.translateY 0;setAttr FKBody_M.translateZ 0;setAttr FKBody_M.rotateX 0;setAttr FKBody_M.rotateY -0;setAttr FKBody_M.rotateZ 0;setAttr FKBody_M.scaleX 1;setAttr FKBody_M.scaleY 1;setAttr FKBody_M.scaleZ 1;setAttr FKExtraWheel1_R.translateX 2.220446049e-16;setAttr FKExtraWheel1_R.translateY 0;setAttr FKExtraWheel1_R.translateZ 0;setAttr FKExtraWheel1_R.rotateX -0;setAttr FKExtraWheel1_R.rotateY 0;setAttr FKExtraWheel1_R.rotateZ 0;setAttr FKExtraWheel1_R.scaleX 1;setAttr FKExtraWheel1_R.scaleY 1;setAttr FKExtraWheel1_R.scaleZ 1;setAttr FKWheel1_R.translateX 0;setAttr FKWheel1_R.translateY 0;setAttr FKWheel1_R.translateZ 0;setAttr FKWheel1_R.rotateX -0;setAttr FKWheel1_R.rotateY 0;setAttr FKWheel1_R.rotateZ 0;setAttr FKWheel1_R.scaleX 1;setAttr FKWheel1_R.scaleY 1;setAttr FKWheel1_R.scaleZ 1;setAttr FKExtraWheel2_R.translateX 0;setAttr FKExtraWheel2_R.translateY 0;setAttr FKExtraWheel2_R.translateZ -8.881784197e-16;setAttr FKExtraWheel2_R.rotateX -0;setAttr FKExtraWheel2_R.rotateY 0;setAttr FKExtraWheel2_R.rotateZ 0;setAttr FKExtraWheel2_R.scaleX 1;setAttr FKExtraWheel2_R.scaleY 1;setAttr FKExtraWheel2_R.scaleZ 1;setAttr FKWheel2_R.translateX 0;setAttr FKWheel2_R.translateY 0;setAttr FKWheel2_R.translateZ 0;setAttr FKWheel2_R.rotateX -0;setAttr FKWheel2_R.rotateY 0;setAttr FKWheel2_R.rotateZ 0;setAttr FKWheel2_R.scaleX 1;setAttr FKWheel2_R.scaleY 1;setAttr FKWheel2_R.scaleZ 1;setAttr FKExtraWheel3_R.translateX 2.220446049e-16;setAttr FKExtraWheel3_R.translateY -4.440892099e-16;setAttr FKExtraWheel3_R.translateZ 0;setAttr FKExtraWheel3_R.rotateX -0;setAttr FKExtraWheel3_R.rotateY 0;setAttr FKExtraWheel3_R.rotateZ 0;setAttr FKExtraWheel3_R.scaleX 1;setAttr FKExtraWheel3_R.scaleY 1;setAttr FKExtraWheel3_R.scaleZ 1;setAttr FKWheel3_R.translateX 0;setAttr FKWheel3_R.translateY 0;setAttr FKWheel3_R.translateZ 0;setAttr FKWheel3_R.rotateX -0;setAttr FKWheel3_R.rotateY 0;setAttr FKWheel3_R.rotateZ 0;setAttr FKWheel3_R.scaleX 1;setAttr FKWheel3_R.scaleY 1;setAttr FKWheel3_R.scaleZ 1;setAttr FKExtraWheel4_R.translateX 0;setAttr FKExtraWheel4_R.translateY -4.440892099e-16;setAttr FKExtraWheel4_R.translateZ 0;setAttr FKExtraWheel4_R.rotateX -0;setAttr FKExtraWheel4_R.rotateY 0;setAttr FKExtraWheel4_R.rotateZ 0;setAttr FKExtraWheel4_R.scaleX 1;setAttr FKExtraWheel4_R.scaleY 1;setAttr FKExtraWheel4_R.scaleZ 1;setAttr FKWheel4_R.translateX 0;setAttr FKWheel4_R.translateY 0;setAttr FKWheel4_R.translateZ 0;setAttr FKWheel4_R.rotateX -0;setAttr FKWheel4_R.rotateY 0;setAttr FKWheel4_R.rotateZ 0;setAttr FKWheel4_R.scaleX 1;setAttr FKWheel4_R.scaleY 1;setAttr FKWheel4_R.scaleZ 1;setAttr FKExtraWheel5_R.translateX 0;setAttr FKExtraWheel5_R.translateY 0;setAttr FKExtraWheel5_R.translateZ 0;setAttr FKExtraWheel5_R.rotateX -0;setAttr FKExtraWheel5_R.rotateY 0;setAttr FKExtraWheel5_R.rotateZ 0;setAttr FKExtraWheel5_R.scaleX 1;setAttr FKExtraWheel5_R.scaleY 1;setAttr FKExtraWheel5_R.scaleZ 1;setAttr FKWheel5_R.translateX 0;setAttr FKWheel5_R.translateY 0;setAttr FKWheel5_R.translateZ 0;setAttr FKWheel5_R.rotateX -0;setAttr FKWheel5_R.rotateY 0;setAttr FKWheel5_R.rotateZ 0;setAttr FKWheel5_R.scaleX 1;setAttr FKWheel5_R.scaleY 1;setAttr FKWheel5_R.scaleZ 1;setAttr FKExtraWheel6_R.translateX 2.220446049e-16;setAttr FKExtraWheel6_R.translateY -4.440892099e-16;setAttr FKExtraWheel6_R.translateZ 8.881784197e-16;setAttr FKExtraWheel6_R.rotateX -0;setAttr FKExtraWheel6_R.rotateY 0;setAttr FKExtraWheel6_R.rotateZ 0;setAttr FKExtraWheel6_R.scaleX 1;setAttr FKExtraWheel6_R.scaleY 1;setAttr FKExtraWheel6_R.scaleZ 1;setAttr FKWheel6_R.translateX 0;setAttr FKWheel6_R.translateY 0;setAttr FKWheel6_R.translateZ 0;setAttr FKWheel6_R.rotateX -0;setAttr FKWheel6_R.rotateY 0;setAttr FKWheel6_R.rotateZ 0;setAttr FKWheel6_R.scaleX 1;setAttr FKWheel6_R.scaleY 1;setAttr FKWheel6_R.scaleZ 1;setAttr FKExtraTrackBottom_R.translateX 0;setAttr FKExtraTrackBottom_R.translateY 2.220446049e-16;setAttr FKExtraTrackBottom_R.translateZ 0;setAttr FKExtraTrackBottom_R.rotateX -0;setAttr FKExtraTrackBottom_R.rotateY -0;setAttr FKExtraTrackBottom_R.rotateZ 0;setAttr FKExtraTrackBottom_R.scaleX 1;setAttr FKExtraTrackBottom_R.scaleY 1;setAttr FKExtraTrackBottom_R.scaleZ 1;setAttr FKTrackBottom_R.translateX 0;setAttr FKTrackBottom_R.translateY 2.220446049e-16;setAttr FKTrackBottom_R.translateZ 0;setAttr FKTrackBottom_R.rotateX -0;setAttr FKTrackBottom_R.rotateY -0;setAttr FKTrackBottom_R.rotateZ 0;setAttr FKTrackBottom_R.scaleX 1;setAttr FKTrackBottom_R.scaleY 1;setAttr FKTrackBottom_R.scaleZ 1;setAttr FKExtraTrackBend_R.translateX 2.220446049e-16;setAttr FKExtraTrackBend_R.translateY 0;setAttr FKExtraTrackBend_R.translateZ -2.220446049e-16;setAttr FKExtraTrackBend_R.rotateX -0;setAttr FKExtraTrackBend_R.rotateY -0;setAttr FKExtraTrackBend_R.rotateZ 0;setAttr FKExtraTrackBend_R.scaleX 1;setAttr FKExtraTrackBend_R.scaleY 1;setAttr FKExtraTrackBend_R.scaleZ 1;setAttr FKTrackBend_R.translateX 0;setAttr FKTrackBend_R.translateY -2.220446049e-16;setAttr FKTrackBend_R.translateZ -2.220446049e-16;setAttr FKTrackBend_R.rotateX -0;setAttr FKTrackBend_R.rotateY -0;setAttr FKTrackBend_R.rotateZ 0;setAttr FKTrackBend_R.scaleX 1;setAttr FKTrackBend_R.scaleY 1;setAttr FKTrackBend_R.scaleZ 1;setAttr FKExtraTrack_R.translateX 0;setAttr FKExtraTrack_R.translateY -4.440892099e-16;setAttr FKExtraTrack_R.translateZ 0;setAttr FKExtraTrack_R.rotateX -0;setAttr FKExtraTrack_R.rotateY -0;setAttr FKExtraTrack_R.rotateZ 0;setAttr FKExtraTrack_R.scaleX 1;setAttr FKExtraTrack_R.scaleY 1;setAttr FKExtraTrack_R.scaleZ 1;setAttr FKTrack_R.translateX 0;setAttr FKTrack_R.translateY 0;setAttr FKTrack_R.translateZ -4.440892099e-16;setAttr FKTrack_R.rotateX -0;setAttr FKTrack_R.rotateY -0;setAttr FKTrack_R.rotateZ 0;setAttr FKTrack_R.scaleX 1;setAttr FKTrack_R.scaleY 1;setAttr FKTrack_R.scaleZ 1;setAttr FKExtraMiddleFinger2_L.translateX 0;setAttr FKExtraMiddleFinger2_L.translateY 2.220446049e-16;setAttr FKExtraMiddleFinger2_L.translateZ 2.220446049e-16;setAttr FKExtraMiddleFinger2_L.rotateX -6.758678698e-15;setAttr FKExtraMiddleFinger2_L.rotateY -1.590277341e-15;setAttr FKExtraMiddleFinger2_L.rotateZ -2.385416011e-15;setAttr FKExtraMiddleFinger2_L.scaleX 1;setAttr FKExtraMiddleFinger2_L.scaleY 1;setAttr FKExtraMiddleFinger2_L.scaleZ 1;setAttr FKMiddleFinger2_L.translateX 0;setAttr FKMiddleFinger2_L.translateY -2.220446049e-16;setAttr FKMiddleFinger2_L.translateZ 2.220446049e-16;setAttr FKMiddleFinger2_L.rotateX -0;setAttr FKMiddleFinger2_L.rotateY 0;setAttr FKMiddleFinger2_L.rotateZ -0;setAttr FKMiddleFinger2_L.scaleX 1;setAttr FKMiddleFinger2_L.scaleY 1;setAttr FKMiddleFinger2_L.scaleZ 1;setAttr FKExtraMiddleFinger1_L.translateX -8.881784197e-16;setAttr FKExtraMiddleFinger1_L.translateY 2.220446049e-16;setAttr FKExtraMiddleFinger1_L.translateZ 4.440892099e-16;setAttr FKExtraMiddleFinger1_L.rotateX -1.272221873e-14;setAttr FKExtraMiddleFinger1_L.rotateY -5.738078187e-31;setAttr FKExtraMiddleFinger1_L.rotateZ 5.168401357e-15;setAttr FKExtraMiddleFinger1_L.scaleX 1;setAttr FKExtraMiddleFinger1_L.scaleY 1;setAttr FKExtraMiddleFinger1_L.scaleZ 1;setAttr FKMiddleFinger1_L.translateX 0;setAttr FKMiddleFinger1_L.translateY 0;setAttr FKMiddleFinger1_L.translateZ 0;setAttr FKMiddleFinger1_L.rotateX -0;setAttr FKMiddleFinger1_L.rotateY 0;setAttr FKMiddleFinger1_L.rotateZ -0;setAttr FKMiddleFinger1_L.scaleX 1;setAttr FKMiddleFinger1_L.scaleY 1;setAttr FKMiddleFinger1_L.scaleZ 1;setAttr FKExtraIndexFinger2_L.translateX -8.881784197e-16;setAttr FKExtraIndexFinger2_L.translateY -4.440892099e-16;setAttr FKExtraIndexFinger2_L.translateZ -4.440892099e-16;setAttr FKExtraIndexFinger2_L.rotateX 6.361109363e-15;setAttr FKExtraIndexFinger2_L.rotateY 1.113194139e-14;setAttr FKExtraIndexFinger2_L.rotateZ -9.541664044e-15;setAttr FKExtraIndexFinger2_L.scaleX 1;setAttr FKExtraIndexFinger2_L.scaleY 1;setAttr FKExtraIndexFinger2_L.scaleZ 1;setAttr FKIndexFinger2_L.translateX -8.881784197e-16;setAttr FKIndexFinger2_L.translateY 0;setAttr FKIndexFinger2_L.translateZ 0;setAttr FKIndexFinger2_L.rotateX -0;setAttr FKIndexFinger2_L.rotateY 0;setAttr FKIndexFinger2_L.rotateZ -0;setAttr FKIndexFinger2_L.scaleX 1;setAttr FKIndexFinger2_L.scaleY 1;setAttr FKIndexFinger2_L.scaleZ 1;setAttr FKExtraIndexFinger1_L.translateX 8.881784197e-16;setAttr FKExtraIndexFinger1_L.translateY 0;setAttr FKExtraIndexFinger1_L.translateZ -4.440892099e-16;setAttr FKExtraIndexFinger1_L.rotateX -1.272221873e-14;setAttr FKExtraIndexFinger1_L.rotateY -3.180554681e-15;setAttr FKExtraIndexFinger1_L.rotateZ -3.531125038e-31;setAttr FKExtraIndexFinger1_L.scaleX 1;setAttr FKExtraIndexFinger1_L.scaleY 1;setAttr FKExtraIndexFinger1_L.scaleZ 1;setAttr FKIndexFinger1_L.translateX 0;setAttr FKIndexFinger1_L.translateY -8.881784197e-16;setAttr FKIndexFinger1_L.translateZ 0;setAttr FKIndexFinger1_L.rotateX -0;setAttr FKIndexFinger1_L.rotateY 0;setAttr FKIndexFinger1_L.rotateZ -0;setAttr FKIndexFinger1_L.scaleX 1;setAttr FKIndexFinger1_L.scaleY 1;setAttr FKIndexFinger1_L.scaleZ 1;setAttr FKExtraThumbFinger2_L.translateX 0;setAttr FKExtraThumbFinger2_L.translateY -1.110223025e-16;setAttr FKExtraThumbFinger2_L.translateZ 6.661338148e-16;setAttr FKExtraThumbFinger2_L.rotateX -1.272221873e-14;setAttr FKExtraThumbFinger2_L.rotateY 8.746525374e-15;setAttr FKExtraThumbFinger2_L.rotateZ -5.565970693e-15;setAttr FKExtraThumbFinger2_L.scaleX 1;setAttr FKExtraThumbFinger2_L.scaleY 1;setAttr FKExtraThumbFinger2_L.scaleZ 1;setAttr FKThumbFinger2_L.translateX 8.881784197e-16;setAttr FKThumbFinger2_L.translateY -1.110223025e-16;setAttr FKThumbFinger2_L.translateZ 0;setAttr FKThumbFinger2_L.rotateX -0;setAttr FKThumbFinger2_L.rotateY 0;setAttr FKThumbFinger2_L.rotateZ -0;setAttr FKThumbFinger2_L.scaleX 1;setAttr FKThumbFinger2_L.scaleY 1;setAttr FKThumbFinger2_L.scaleZ 1;setAttr FKExtraThumbFinger1_L.translateX -8.881784197e-16;setAttr FKExtraThumbFinger1_L.translateY -2.220446049e-16;setAttr FKExtraThumbFinger1_L.translateZ -4.440892099e-16;setAttr FKExtraThumbFinger1_L.rotateX -1.908332809e-14;setAttr FKExtraThumbFinger1_L.rotateY -1.183389974e-14;setAttr FKExtraThumbFinger1_L.rotateZ -2.292235698e-15;setAttr FKExtraThumbFinger1_L.scaleX 1;setAttr FKExtraThumbFinger1_L.scaleY 1;setAttr FKExtraThumbFinger1_L.scaleZ 1;setAttr FKThumbFinger1_L.translateX 0;setAttr FKThumbFinger1_L.translateY -2.220446049e-16;setAttr FKThumbFinger1_L.translateZ -2.220446049e-16;setAttr FKThumbFinger1_L.rotateX -0;setAttr FKThumbFinger1_L.rotateY 0;setAttr FKThumbFinger1_L.rotateZ -0;setAttr FKThumbFinger1_L.scaleX 1;setAttr FKThumbFinger1_L.scaleY 1;setAttr FKThumbFinger1_L.scaleZ 1;setAttr FKExtraElbow_L.translateX 0;setAttr FKExtraElbow_L.translateY 0;setAttr FKExtraElbow_L.translateZ 0;setAttr FKExtraElbow_L.rotateX 8.827812596e-31;setAttr FKExtraElbow_L.rotateY -3.180554681e-15;setAttr FKExtraElbow_L.rotateZ 3.180554681e-14;setAttr FKExtraElbow_L.scaleX 1;setAttr FKExtraElbow_L.scaleY 1;setAttr FKExtraElbow_L.scaleZ 1;setAttr FKElbow_L.translateX -4.579669977e-16;setAttr FKElbow_L.translateY 0;setAttr FKElbow_L.translateZ 8.881784197e-16;setAttr FKElbow_L.rotateX -0;setAttr FKElbow_L.rotateY 0;setAttr FKElbow_L.rotateZ -0;setAttr FKElbow_L.scaleX 1;setAttr FKElbow_L.scaleY 1;setAttr FKElbow_L.scaleZ 1;setAttr FKExtraShoulder_L.translateX 0;setAttr FKExtraShoulder_L.translateY 4.440892099e-16;setAttr FKExtraShoulder_L.translateZ 0;setAttr FKExtraShoulder_L.rotateX 2.484808345e-15;setAttr FKExtraShoulder_L.rotateY 4.497503104e-15;setAttr FKExtraShoulder_L.rotateZ 2.226388277e-14;setAttr FKExtraShoulder_L.scaleX 1;setAttr FKExtraShoulder_L.scaleY 1;setAttr FKExtraShoulder_L.scaleZ 1;setAttr FKShoulder_L.translateX 0;setAttr FKShoulder_L.translateY 0;setAttr FKShoulder_L.translateZ 8.881784197e-16;setAttr FKShoulder_L.rotateX -0;setAttr FKShoulder_L.rotateY 0;setAttr FKShoulder_L.rotateZ -0;setAttr FKShoulder_L.scaleX 1;setAttr FKShoulder_L.scaleY 1;setAttr FKShoulder_L.scaleZ 1;setAttr FKShoulder_L.Global 10;setAttr FKExtraWheel1_L.translateX -2.220446049e-16;setAttr FKExtraWheel1_L.translateY 0;setAttr FKExtraWheel1_L.translateZ 0;setAttr FKExtraWheel1_L.rotateX -7.016709299e-15;setAttr FKExtraWheel1_L.rotateY 7.016709299e-15;setAttr FKExtraWheel1_L.rotateZ -1.272221873e-14;setAttr FKExtraWheel1_L.scaleX 1;setAttr FKExtraWheel1_L.scaleY 1;setAttr FKExtraWheel1_L.scaleZ 1;setAttr FKWheel1_L.translateX 0;setAttr FKWheel1_L.translateY 0;setAttr FKWheel1_L.translateZ 0;setAttr FKWheel1_L.rotateX -0;setAttr FKWheel1_L.rotateY 0;setAttr FKWheel1_L.rotateZ 0;setAttr FKWheel1_L.scaleX 1;setAttr FKWheel1_L.scaleY 1;setAttr FKWheel1_L.scaleZ 1;setAttr FKExtraWheel2_L.translateX -2.220446049e-16;setAttr FKExtraWheel2_L.translateY 0;setAttr FKExtraWheel2_L.translateZ -4.440892099e-16;setAttr FKExtraWheel2_L.rotateX -7.016709299e-15;setAttr FKExtraWheel2_L.rotateY 7.016709299e-15;setAttr FKExtraWheel2_L.rotateZ -1.272221873e-14;setAttr FKExtraWheel2_L.scaleX 1;setAttr FKExtraWheel2_L.scaleY 1;setAttr FKExtraWheel2_L.scaleZ 1;setAttr FKWheel2_L.translateX 0;setAttr FKWheel2_L.translateY 0;setAttr FKWheel2_L.translateZ 0;setAttr FKWheel2_L.rotateX -0;setAttr FKWheel2_L.rotateY 0;setAttr FKWheel2_L.rotateZ 0;setAttr FKWheel2_L.scaleX 1;setAttr FKWheel2_L.scaleY 1;setAttr FKWheel2_L.scaleZ 1;setAttr FKExtraWheel3_L.translateX -4.440892099e-16;setAttr FKExtraWheel3_L.translateY 4.440892099e-16;setAttr FKExtraWheel3_L.translateZ 0;setAttr FKExtraWheel3_L.rotateX -7.016709299e-15;setAttr FKExtraWheel3_L.rotateY 7.016709299e-15;setAttr FKExtraWheel3_L.rotateZ -1.272221873e-14;setAttr FKExtraWheel3_L.scaleX 1;setAttr FKExtraWheel3_L.scaleY 1;setAttr FKExtraWheel3_L.scaleZ 1;setAttr FKWheel3_L.translateX 0;setAttr FKWheel3_L.translateY 0;setAttr FKWheel3_L.translateZ 0;setAttr FKWheel3_L.rotateX -0;setAttr FKWheel3_L.rotateY 0;setAttr FKWheel3_L.rotateZ 0;setAttr FKWheel3_L.scaleX 1;setAttr FKWheel3_L.scaleY 1;setAttr FKWheel3_L.scaleZ 1;setAttr FKExtraWheel4_L.translateX 0;setAttr FKExtraWheel4_L.translateY 0;setAttr FKExtraWheel4_L.translateZ 0;setAttr FKExtraWheel4_L.rotateX -7.016709299e-15;setAttr FKExtraWheel4_L.rotateY 7.016709299e-15;setAttr FKExtraWheel4_L.rotateZ -1.272221873e-14;setAttr FKExtraWheel4_L.scaleX 1;setAttr FKExtraWheel4_L.scaleY 1;setAttr FKExtraWheel4_L.scaleZ 1;setAttr FKWheel4_L.translateX 0;setAttr FKWheel4_L.translateY 0;setAttr FKWheel4_L.translateZ 0;setAttr FKWheel4_L.rotateX -0;setAttr FKWheel4_L.rotateY 0;setAttr FKWheel4_L.rotateZ 0;setAttr FKWheel4_L.scaleX 1;setAttr FKWheel4_L.scaleY 1;setAttr FKWheel4_L.scaleZ 1;setAttr FKExtraWheel5_L.translateX 2.220446049e-16;setAttr FKExtraWheel5_L.translateY -4.440892099e-16;setAttr FKExtraWheel5_L.translateZ 0;setAttr FKExtraWheel5_L.rotateX -7.016709299e-15;setAttr FKExtraWheel5_L.rotateY 7.016709299e-15;setAttr FKExtraWheel5_L.rotateZ -1.272221873e-14;setAttr FKExtraWheel5_L.scaleX 1;setAttr FKExtraWheel5_L.scaleY 1;setAttr FKExtraWheel5_L.scaleZ 1;");
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "jointLayer.di" "Pelvis.do";
connectAttr "Pelvis.s" "Body.is";
connectAttr "jointLayer.di" "Body.do";
connectAttr "Body.s" "Weapon.is";
connectAttr "jointLayer.di" "Weapon.do";
connectAttr "Weapon.s" "Weapon_End.is";
connectAttr "jointLayer.di" "Weapon_End.do";
connectAttr "Body.s" "Shoulder.is";
connectAttr "jointLayer.di" "Shoulder.do";
connectAttr "Shoulder.global" "Shoulder.globalConnect";
connectAttr "Shoulder.s" "Elbow.is";
connectAttr "jointLayer.di" "Elbow.do";
connectAttr "Elbow.s" "MiddleFinger1.is";
connectAttr "jointLayer.di" "MiddleFinger1.do";
connectAttr "MiddleFinger1.s" "MiddleFinger2.is";
connectAttr "jointLayer.di" "MiddleFinger2.do";
connectAttr "MiddleFinger2.s" "MiddleFinger3_End.is";
connectAttr "jointLayer.di" "MiddleFinger3_End.do";
connectAttr "Elbow.s" "IndexFinger1.is";
connectAttr "jointLayer.di" "IndexFinger1.do";
connectAttr "IndexFinger1.s" "IndexFinger2.is";
connectAttr "jointLayer.di" "IndexFinger2.do";
connectAttr "IndexFinger2.s" "IndexFinger3_End.is";
connectAttr "jointLayer.di" "IndexFinger3_End.do";
connectAttr "Elbow.s" "ThumbFinger1.is";
connectAttr "jointLayer.di" "ThumbFinger1.do";
connectAttr "ThumbFinger1.s" "ThumbFinger2.is";
connectAttr "jointLayer.di" "ThumbFinger2.do";
connectAttr "ThumbFinger2.s" "ThumbFinger3_End.is";
connectAttr "jointLayer.di" "ThumbFinger3_End.do";
connectAttr "Pelvis.s" "Track.is";
connectAttr "jointLayer.di" "Track.do";
connectAttr "Track.s" "TrackBend.is";
connectAttr "jointLayer.di" "TrackBend.do";
connectAttr "TrackBend.s" "TrackBottom.is";
connectAttr "jointLayer.di" "TrackBottom.do";
connectAttr "jointLayer.di" "Wheel1.do";
connectAttr "TrackBottom.s" "Wheel1.is";
connectAttr "Wheel1.s" "Wheel1_End.is";
connectAttr "jointLayer.di" "Wheel1_End.do";
connectAttr "jointLayer.di" "Wheel2.do";
connectAttr "TrackBottom.s" "Wheel2.is";
connectAttr "Wheel2.s" "Wheel2_End.is";
connectAttr "jointLayer.di" "Wheel2_End.do";
connectAttr "jointLayer.di" "Wheel3.do";
connectAttr "TrackBottom.s" "Wheel3.is";
connectAttr "Wheel3.s" "Wheel3_End.is";
connectAttr "jointLayer.di" "Wheel3_End.do";
connectAttr "jointLayer.di" "Wheel4.do";
connectAttr "TrackBottom.s" "Wheel4.is";
connectAttr "Wheel4.s" "Wheel4_End.is";
connectAttr "jointLayer.di" "Wheel4_End.do";
connectAttr "jointLayer.di" "Wheel5.do";
connectAttr "TrackBottom.s" "Wheel5.is";
connectAttr "Wheel5.s" "Wheel5_End.is";
connectAttr "jointLayer.di" "Wheel5_End.do";
connectAttr "jointLayer.di" "Wheel6.do";
connectAttr "TrackBottom.s" "Wheel6.is";
connectAttr "Wheel6.s" "Wheel6_End.is";
connectAttr "jointLayer.di" "Wheel6_End.do";
connectAttr "Main.fkVis" "FKSystem.v";
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.ctx" "FKParentConstraintToTrackBottom_R.tx"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.cty" "FKParentConstraintToTrackBottom_R.ty"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.ctz" "FKParentConstraintToTrackBottom_R.tz"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.crx" "FKParentConstraintToTrackBottom_R.rx"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.cry" "FKParentConstraintToTrackBottom_R.ry"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.crz" "FKParentConstraintToTrackBottom_R.rz"
		;
connectAttr "jointLayer.di" "FKOffsetWheel1_R.do";
connectAttr "jointLayer.di" "FKXWheel1_R.do";
connectAttr "FKWheel1_R.s" "FKXWheel1_End_R.is";
connectAttr "jointLayer.di" "FKXWheel1_End_R.do";
connectAttr "jointLayer.di" "FKOffsetWheel2_R.do";
connectAttr "jointLayer.di" "FKXWheel2_R.do";
connectAttr "FKWheel2_R.s" "FKXWheel2_End_R.is";
connectAttr "jointLayer.di" "FKXWheel2_End_R.do";
connectAttr "jointLayer.di" "FKOffsetWheel3_R.do";
connectAttr "jointLayer.di" "FKXWheel3_R.do";
connectAttr "FKWheel3_R.s" "FKXWheel3_End_R.is";
connectAttr "jointLayer.di" "FKXWheel3_End_R.do";
connectAttr "jointLayer.di" "FKOffsetWheel4_R.do";
connectAttr "jointLayer.di" "FKXWheel4_R.do";
connectAttr "FKWheel4_R.s" "FKXWheel4_End_R.is";
connectAttr "jointLayer.di" "FKXWheel4_End_R.do";
connectAttr "jointLayer.di" "FKOffsetWheel5_R.do";
connectAttr "jointLayer.di" "FKXWheel5_R.do";
connectAttr "FKWheel5_R.s" "FKXWheel5_End_R.is";
connectAttr "jointLayer.di" "FKXWheel5_End_R.do";
connectAttr "jointLayer.di" "FKOffsetWheel6_R.do";
connectAttr "jointLayer.di" "FKXWheel6_R.do";
connectAttr "FKWheel6_R.s" "FKXWheel6_End_R.is";
connectAttr "jointLayer.di" "FKXWheel6_End_R.do";
connectAttr "FKParentConstraintToTrackBottom_R.ro" "FKParentConstraintToTrackBottom_R_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToTrackBottom_R.pim" "FKParentConstraintToTrackBottom_R_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToTrackBottom_R.rp" "FKParentConstraintToTrackBottom_R_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToTrackBottom_R.rpt" "FKParentConstraintToTrackBottom_R_parentConstraint1.crt"
		;
connectAttr "TrackBottom_R.t" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tt"
		;
connectAttr "TrackBottom_R.rp" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].trp"
		;
connectAttr "TrackBottom_R.rpt" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].trt"
		;
connectAttr "TrackBottom_R.r" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tr"
		;
connectAttr "TrackBottom_R.ro" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tro"
		;
connectAttr "TrackBottom_R.s" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].ts"
		;
connectAttr "TrackBottom_R.pm" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "TrackBottom_R.jo" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.w0" "FKParentConstraintToTrackBottom_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.ctx" "FKParentConstraintToTrackBottom_L.tx"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.cty" "FKParentConstraintToTrackBottom_L.ty"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.ctz" "FKParentConstraintToTrackBottom_L.tz"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.crx" "FKParentConstraintToTrackBottom_L.rx"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.cry" "FKParentConstraintToTrackBottom_L.ry"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.crz" "FKParentConstraintToTrackBottom_L.rz"
		;
connectAttr "jointLayer.di" "FKOffsetWheel1_L.do";
connectAttr "jointLayer.di" "FKXWheel1_L.do";
connectAttr "FKWheel1_L.s" "FKXWheel1_End_L.is";
connectAttr "jointLayer.di" "FKXWheel1_End_L.do";
connectAttr "jointLayer.di" "FKOffsetWheel2_L.do";
connectAttr "jointLayer.di" "FKXWheel2_L.do";
connectAttr "FKWheel2_L.s" "FKXWheel2_End_L.is";
connectAttr "jointLayer.di" "FKXWheel2_End_L.do";
connectAttr "jointLayer.di" "FKOffsetWheel3_L.do";
connectAttr "jointLayer.di" "FKXWheel3_L.do";
connectAttr "FKWheel3_L.s" "FKXWheel3_End_L.is";
connectAttr "jointLayer.di" "FKXWheel3_End_L.do";
connectAttr "jointLayer.di" "FKOffsetWheel4_L.do";
connectAttr "jointLayer.di" "FKXWheel4_L.do";
connectAttr "FKWheel4_L.s" "FKXWheel4_End_L.is";
connectAttr "jointLayer.di" "FKXWheel4_End_L.do";
connectAttr "jointLayer.di" "FKOffsetWheel5_L.do";
connectAttr "jointLayer.di" "FKXWheel5_L.do";
connectAttr "FKWheel5_L.s" "FKXWheel5_End_L.is";
connectAttr "jointLayer.di" "FKXWheel5_End_L.do";
connectAttr "jointLayer.di" "FKOffsetWheel6_L.do";
connectAttr "jointLayer.di" "FKXWheel6_L.do";
connectAttr "FKWheel6_L.s" "FKXWheel6_End_L.is";
connectAttr "jointLayer.di" "FKXWheel6_End_L.do";
connectAttr "FKParentConstraintToTrackBottom_L.ro" "FKParentConstraintToTrackBottom_L_parentConstraint1.cro"
		;
connectAttr "FKParentConstraintToTrackBottom_L.pim" "FKParentConstraintToTrackBottom_L_parentConstraint1.cpim"
		;
connectAttr "FKParentConstraintToTrackBottom_L.rp" "FKParentConstraintToTrackBottom_L_parentConstraint1.crp"
		;
connectAttr "FKParentConstraintToTrackBottom_L.rpt" "FKParentConstraintToTrackBottom_L_parentConstraint1.crt"
		;
connectAttr "TrackBottom_L.t" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tt"
		;
connectAttr "TrackBottom_L.rp" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].trp"
		;
connectAttr "TrackBottom_L.rpt" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].trt"
		;
connectAttr "TrackBottom_L.r" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tr"
		;
connectAttr "TrackBottom_L.ro" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tro"
		;
connectAttr "TrackBottom_L.s" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].ts"
		;
connectAttr "TrackBottom_L.pm" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "TrackBottom_L.jo" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.w0" "FKParentConstraintToTrackBottom_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctx" "PelvisCenterBtwLegsBlended_M.tx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cty" "PelvisCenterBtwLegsBlended_M.ty"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.ctz" "PelvisCenterBtwLegsBlended_M.tz"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crx" "PelvisCenterBtwLegsBlended_M.rx"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.cry" "PelvisCenterBtwLegsBlended_M.ry"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.crz" "PelvisCenterBtwLegsBlended_M.rz"
		;
connectAttr "jointLayer.di" "FKOffsetPelvis_M.do";
connectAttr "jointLayer.di" "FKXPelvis_M.do";
connectAttr "FKXPelvis_M.s" "FKOffsetBody_M.is";
connectAttr "jointLayer.di" "FKOffsetBody_M.do";
connectAttr "FKPelvis_M.s" "FKXBody_M.is";
connectAttr "jointLayer.di" "FKXBody_M.do";
connectAttr "FKXBody_M.s" "FKOffsetWeapon_M.is";
connectAttr "jointLayer.di" "FKOffsetWeapon_M.do";
connectAttr "FKBody_M.s" "FKXWeapon_M.is";
connectAttr "jointLayer.di" "FKXWeapon_M.do";
connectAttr "FKWeapon_M.s" "FKXWeapon_End_M.is";
connectAttr "jointLayer.di" "FKXWeapon_End_M.do";
connectAttr "FKXBody_M.s" "FKOffsetShoulder_R.is";
connectAttr "jointLayer.di" "FKOffsetShoulder_R.do";
connectAttr "FKGlobalShoulder_R_orientConstraint1.crx" "FKGlobalShoulder_R.rx";
connectAttr "FKGlobalShoulder_R_orientConstraint1.cry" "FKGlobalShoulder_R.ry";
connectAttr "FKGlobalShoulder_R_orientConstraint1.crz" "FKGlobalShoulder_R.rz";
connectAttr "FKBody_M.s" "FKXShoulder_R.is";
connectAttr "jointLayer.di" "FKXShoulder_R.do";
connectAttr "FKXShoulder_R.s" "FKOffsetElbow_R.is";
connectAttr "jointLayer.di" "FKOffsetElbow_R.do";
connectAttr "FKShoulder_R.s" "FKXElbow_R.is";
connectAttr "jointLayer.di" "FKXElbow_R.do";
connectAttr "FKXElbow_R.s" "FKOffsetMiddleFinger1_R.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger1_R.do";
connectAttr "FKElbow_R.s" "FKXMiddleFinger1_R.is";
connectAttr "jointLayer.di" "FKXMiddleFinger1_R.do";
connectAttr "FKXMiddleFinger1_R.s" "FKOffsetMiddleFinger2_R.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger2_R.do";
connectAttr "FKMiddleFinger1_R.s" "FKXMiddleFinger2_R.is";
connectAttr "jointLayer.di" "FKXMiddleFinger2_R.do";
connectAttr "FKMiddleFinger2_R.s" "FKXMiddleFinger3_End_R.is";
connectAttr "jointLayer.di" "FKXMiddleFinger3_End_R.do";
connectAttr "FKXElbow_R.s" "FKOffsetIndexFinger1_R.is";
connectAttr "jointLayer.di" "FKOffsetIndexFinger1_R.do";
connectAttr "FKElbow_R.s" "FKXIndexFinger1_R.is";
connectAttr "jointLayer.di" "FKXIndexFinger1_R.do";
connectAttr "FKXIndexFinger1_R.s" "FKOffsetIndexFinger2_R.is";
connectAttr "jointLayer.di" "FKOffsetIndexFinger2_R.do";
connectAttr "FKIndexFinger1_R.s" "FKXIndexFinger2_R.is";
connectAttr "jointLayer.di" "FKXIndexFinger2_R.do";
connectAttr "FKIndexFinger2_R.s" "FKXIndexFinger3_End_R.is";
connectAttr "jointLayer.di" "FKXIndexFinger3_End_R.do";
connectAttr "FKXElbow_R.s" "FKOffsetThumbFinger1_R.is";
connectAttr "jointLayer.di" "FKOffsetThumbFinger1_R.do";
connectAttr "FKElbow_R.s" "FKXThumbFinger1_R.is";
connectAttr "jointLayer.di" "FKXThumbFinger1_R.do";
connectAttr "FKXThumbFinger1_R.s" "FKOffsetThumbFinger2_R.is";
connectAttr "jointLayer.di" "FKOffsetThumbFinger2_R.do";
connectAttr "FKThumbFinger1_R.s" "FKXThumbFinger2_R.is";
connectAttr "jointLayer.di" "FKXThumbFinger2_R.do";
connectAttr "FKThumbFinger2_R.s" "FKXThumbFinger3_End_R.is";
connectAttr "jointLayer.di" "FKXThumbFinger3_End_R.do";
connectAttr "FKGlobalShoulder_R.ro" "FKGlobalShoulder_R_orientConstraint1.cro";
connectAttr "FKGlobalShoulder_R.pim" "FKGlobalShoulder_R_orientConstraint1.cpim"
		;
connectAttr "GlobalShoulder_R.r" "FKGlobalShoulder_R_orientConstraint1.tg[0].tr"
		;
connectAttr "GlobalShoulder_R.ro" "FKGlobalShoulder_R_orientConstraint1.tg[0].tro"
		;
connectAttr "GlobalShoulder_R.pm" "FKGlobalShoulder_R_orientConstraint1.tg[0].tpm"
		;
connectAttr "FKGlobalShoulder_R_orientConstraint1.w0" "FKGlobalShoulder_R_orientConstraint1.tg[0].tw"
		;
connectAttr "FKGlobalStaticShoulder_R.r" "FKGlobalShoulder_R_orientConstraint1.tg[1].tr"
		;
connectAttr "FKGlobalStaticShoulder_R.ro" "FKGlobalShoulder_R_orientConstraint1.tg[1].tro"
		;
connectAttr "FKGlobalStaticShoulder_R.pm" "FKGlobalShoulder_R_orientConstraint1.tg[1].tpm"
		;
connectAttr "FKGlobalShoulder_R_orientConstraint1.w1" "FKGlobalShoulder_R_orientConstraint1.tg[1].tw"
		;
connectAttr "GlobalShoulder_unitConversion_R.o" "FKGlobalShoulder_R_orientConstraint1.w0"
		;
connectAttr "GlobalShoulder_reverse_R.ox" "FKGlobalShoulder_R_orientConstraint1.w1"
		;
connectAttr "FKXBody_M.s" "FKOffsetShoulder_L.is";
connectAttr "jointLayer.di" "FKOffsetShoulder_L.do";
connectAttr "FKGlobalShoulder_L_orientConstraint1.crx" "FKGlobalShoulder_L.rx";
connectAttr "FKGlobalShoulder_L_orientConstraint1.cry" "FKGlobalShoulder_L.ry";
connectAttr "FKGlobalShoulder_L_orientConstraint1.crz" "FKGlobalShoulder_L.rz";
connectAttr "FKBody_M.s" "FKXShoulder_L.is";
connectAttr "jointLayer.di" "FKXShoulder_L.do";
connectAttr "FKXShoulder_L.s" "FKOffsetElbow_L.is";
connectAttr "jointLayer.di" "FKOffsetElbow_L.do";
connectAttr "FKShoulder_L.s" "FKXElbow_L.is";
connectAttr "jointLayer.di" "FKXElbow_L.do";
connectAttr "FKXElbow_L.s" "FKOffsetMiddleFinger1_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger1_L.do";
connectAttr "FKElbow_L.s" "FKXMiddleFinger1_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger1_L.do";
connectAttr "FKXMiddleFinger1_L.s" "FKOffsetMiddleFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetMiddleFinger2_L.do";
connectAttr "FKMiddleFinger1_L.s" "FKXMiddleFinger2_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger2_L.do";
connectAttr "FKMiddleFinger2_L.s" "FKXMiddleFinger3_End_L.is";
connectAttr "jointLayer.di" "FKXMiddleFinger3_End_L.do";
connectAttr "FKXElbow_L.s" "FKOffsetIndexFinger1_L.is";
connectAttr "jointLayer.di" "FKOffsetIndexFinger1_L.do";
connectAttr "FKElbow_L.s" "FKXIndexFinger1_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger1_L.do";
connectAttr "FKXIndexFinger1_L.s" "FKOffsetIndexFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetIndexFinger2_L.do";
connectAttr "FKIndexFinger1_L.s" "FKXIndexFinger2_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger2_L.do";
connectAttr "FKIndexFinger2_L.s" "FKXIndexFinger3_End_L.is";
connectAttr "jointLayer.di" "FKXIndexFinger3_End_L.do";
connectAttr "FKXElbow_L.s" "FKOffsetThumbFinger1_L.is";
connectAttr "jointLayer.di" "FKOffsetThumbFinger1_L.do";
connectAttr "FKElbow_L.s" "FKXThumbFinger1_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger1_L.do";
connectAttr "FKXThumbFinger1_L.s" "FKOffsetThumbFinger2_L.is";
connectAttr "jointLayer.di" "FKOffsetThumbFinger2_L.do";
connectAttr "FKThumbFinger1_L.s" "FKXThumbFinger2_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger2_L.do";
connectAttr "FKThumbFinger2_L.s" "FKXThumbFinger3_End_L.is";
connectAttr "jointLayer.di" "FKXThumbFinger3_End_L.do";
connectAttr "FKGlobalShoulder_L.ro" "FKGlobalShoulder_L_orientConstraint1.cro";
connectAttr "FKGlobalShoulder_L.pim" "FKGlobalShoulder_L_orientConstraint1.cpim"
		;
connectAttr "GlobalShoulder_L.r" "FKGlobalShoulder_L_orientConstraint1.tg[0].tr"
		;
connectAttr "GlobalShoulder_L.ro" "FKGlobalShoulder_L_orientConstraint1.tg[0].tro"
		;
connectAttr "GlobalShoulder_L.pm" "FKGlobalShoulder_L_orientConstraint1.tg[0].tpm"
		;
connectAttr "FKGlobalShoulder_L_orientConstraint1.w0" "FKGlobalShoulder_L_orientConstraint1.tg[0].tw"
		;
connectAttr "FKGlobalStaticShoulder_L.r" "FKGlobalShoulder_L_orientConstraint1.tg[1].tr"
		;
connectAttr "FKGlobalStaticShoulder_L.ro" "FKGlobalShoulder_L_orientConstraint1.tg[1].tro"
		;
connectAttr "FKGlobalStaticShoulder_L.pm" "FKGlobalShoulder_L_orientConstraint1.tg[1].tpm"
		;
connectAttr "FKGlobalShoulder_L_orientConstraint1.w1" "FKGlobalShoulder_L_orientConstraint1.tg[1].tw"
		;
connectAttr "GlobalShoulder_unitConversion_L.o" "FKGlobalShoulder_L_orientConstraint1.w0"
		;
connectAttr "GlobalShoulder_reverse_L.ox" "FKGlobalShoulder_L_orientConstraint1.w1"
		;
connectAttr "FKXPelvis_M.s" "FKOffsetTrack_R.is";
connectAttr "FKIKBlendLegCondition_R.ocg" "FKOffsetTrack_R.v" -l on;
connectAttr "jointLayer.di" "FKOffsetTrack_R.do";
connectAttr "jointLayer.di" "FKXTrack_R.do";
connectAttr "FKXTrack_R.s" "FKOffsetTrackBend_R.is";
connectAttr "jointLayer.di" "FKOffsetTrackBend_R.do";
connectAttr "FKTrack_R.s" "FKXTrackBend_R.is";
connectAttr "jointLayer.di" "FKXTrackBend_R.do";
connectAttr "FKXTrackBend_R.s" "FKOffsetTrackBottom_R.is";
connectAttr "jointLayer.di" "FKOffsetTrackBottom_R.do";
connectAttr "FKTrackBend_R.s" "FKXTrackBottom_R.is";
connectAttr "jointLayer.di" "FKXTrackBottom_R.do";
connectAttr "FKXPelvis_M.s" "FKOffsetTrack_L.is";
connectAttr "FKIKBlendLegCondition_L.ocg" "FKOffsetTrack_L.v" -l on;
connectAttr "jointLayer.di" "FKOffsetTrack_L.do";
connectAttr "jointLayer.di" "FKXTrack_L.do";
connectAttr "FKXTrack_L.s" "FKOffsetTrackBend_L.is";
connectAttr "jointLayer.di" "FKOffsetTrackBend_L.do";
connectAttr "FKTrack_L.s" "FKXTrackBend_L.is";
connectAttr "jointLayer.di" "FKXTrackBend_L.do";
connectAttr "FKXTrackBend_L.s" "FKOffsetTrackBottom_L.is";
connectAttr "jointLayer.di" "FKOffsetTrackBottom_L.do";
connectAttr "FKTrackBend_L.s" "FKXTrackBottom_L.is";
connectAttr "jointLayer.di" "FKXTrackBottom_L.do";
connectAttr "PelvisCenterBtwLegsBlended_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.pim" "PelvisCenterBtwLegsBlended_M_parentConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegsBlended_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.crt"
		;
connectAttr "PelvisCenter_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tt"
		;
connectAttr "PelvisCenter_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trp"
		;
connectAttr "PelvisCenter_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].trt"
		;
connectAttr "PelvisCenter_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tr"
		;
connectAttr "PelvisCenter_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tro"
		;
connectAttr "PelvisCenter_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].ts"
		;
connectAttr "PelvisCenter_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.t" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rp" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trp"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.rpt" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].trt"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.r" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tr"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.ro" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tro"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.s" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].ts"
		;
connectAttr "PelvisCenterBtwLegsOffset_M.pm" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tpm"
		;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1" "PelvisCenterBtwLegsBlended_M_parentConstraint1.tg[1].tw"
		;
connectAttr "PelvisCenterBtwFeetReverse_M.ox" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w0"
		;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwLegsBlended_M_parentConstraint1.w1"
		;
connectAttr "Main.ikVis" "IKSystem.v";
connectAttr "IKParentConstraintTrack_R_parentConstraint1.ctx" "IKParentConstraintTrack_R.tx"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.cty" "IKParentConstraintTrack_R.ty"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.ctz" "IKParentConstraintTrack_R.tz"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.crx" "IKParentConstraintTrack_R.rx"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.cry" "IKParentConstraintTrack_R.ry"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.crz" "IKParentConstraintTrack_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintTrack_R.v";
connectAttr "jointLayer.di" "IKXTrack_R.do";
connectAttr "IKXTrack_R.s" "IKXTrackBend_R.is";
connectAttr "IKXTrackBend_R_IKmessureDiv_R.oy" "IKXTrackBend_R.ty";
connectAttr "jointLayer.di" "IKXTrackBend_R.do";
connectAttr "IKXTrackBend_R.s" "IKXTrackBottom_R.is";
connectAttr "IKXTrackBottom_R_IKmessureDiv_R.oy" "IKXTrackBottom_R.ty";
connectAttr "IKXTrackBottom_R_orientConstraint1.crx" "IKXTrackBottom_R.rx";
connectAttr "IKXTrackBottom_R_orientConstraint1.cry" "IKXTrackBottom_R.ry";
connectAttr "IKXTrackBottom_R_orientConstraint1.crz" "IKXTrackBottom_R.rz";
connectAttr "jointLayer.di" "IKXTrackBottom_R.do";
connectAttr "IKXTrackBottom_R.ro" "IKXTrackBottom_R_orientConstraint1.cro";
connectAttr "IKXTrackBottom_R.pim" "IKXTrackBottom_R_orientConstraint1.cpim";
connectAttr "IKXTrackBottom_R.jo" "IKXTrackBottom_R_orientConstraint1.cjo";
connectAttr "IKLeg_R.r" "IKXTrackBottom_R_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "IKXTrackBottom_R_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "IKXTrackBottom_R_orientConstraint1.tg[0].tpm";
connectAttr "IKXTrackBottom_R_orientConstraint1.w0" "IKXTrackBottom_R_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBottom_R.tx" "effector1.tx";
connectAttr "IKXTrackBottom_R.ty" "effector1.ty";
connectAttr "IKXTrackBottom_R.tz" "effector1.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_R.v";
connectAttr "PoleAnnotateTargetLeg_RShape.wm" "PoleAnnotationLeg_RShape.dom" -na
		;
connectAttr "IKParentConstraintTrack_R.ro" "IKParentConstraintTrack_R_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintTrack_R.pim" "IKParentConstraintTrack_R_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintTrack_R.rp" "IKParentConstraintTrack_R_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintTrack_R.rpt" "IKParentConstraintTrack_R_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tt";
connectAttr "Pelvis_M.rp" "IKParentConstraintTrack_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "IKParentConstraintTrack_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tr";
connectAttr "Pelvis_M.ro" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "IKParentConstraintTrack_R_parentConstraint1.tg[0].ts";
connectAttr "Pelvis_M.pm" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.w0" "IKParentConstraintTrack_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.ctx" "IKParentConstraintTrack_L.tx"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.cty" "IKParentConstraintTrack_L.ty"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.ctz" "IKParentConstraintTrack_L.tz"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.crx" "IKParentConstraintTrack_L.rx"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.cry" "IKParentConstraintTrack_L.ry"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.crz" "IKParentConstraintTrack_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintTrack_L.v";
connectAttr "jointLayer.di" "IKXTrack_L.do";
connectAttr "IKXTrack_L.s" "IKXTrackBend_L.is";
connectAttr "IKXTrackBend_L_IKmessureDiv_L.oy" "IKXTrackBend_L.ty";
connectAttr "jointLayer.di" "IKXTrackBend_L.do";
connectAttr "IKXTrackBend_L.s" "IKXTrackBottom_L.is";
connectAttr "IKXTrackBottom_L_IKmessureDiv_L.oy" "IKXTrackBottom_L.ty";
connectAttr "IKXTrackBottom_L_orientConstraint1.crx" "IKXTrackBottom_L.rx";
connectAttr "IKXTrackBottom_L_orientConstraint1.cry" "IKXTrackBottom_L.ry";
connectAttr "IKXTrackBottom_L_orientConstraint1.crz" "IKXTrackBottom_L.rz";
connectAttr "jointLayer.di" "IKXTrackBottom_L.do";
connectAttr "IKXTrackBottom_L.ro" "IKXTrackBottom_L_orientConstraint1.cro";
connectAttr "IKXTrackBottom_L.pim" "IKXTrackBottom_L_orientConstraint1.cpim";
connectAttr "IKXTrackBottom_L.jo" "IKXTrackBottom_L_orientConstraint1.cjo";
connectAttr "IKLeg_L.r" "IKXTrackBottom_L_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_L.ro" "IKXTrackBottom_L_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_L.pm" "IKXTrackBottom_L_orientConstraint1.tg[0].tpm";
connectAttr "IKXTrackBottom_L_orientConstraint1.w0" "IKXTrackBottom_L_orientConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBottom_L.tx" "effector2.tx";
connectAttr "IKXTrackBottom_L.ty" "effector2.ty";
connectAttr "IKXTrackBottom_L.tz" "effector2.tz";
connectAttr "Main.arrowVis" "PoleAnnotationLeg_L.v";
connectAttr "PoleAnnotateTargetLeg_LShape.wm" "PoleAnnotationLeg_LShape.dom" -na
		;
connectAttr "IKParentConstraintTrack_L.ro" "IKParentConstraintTrack_L_parentConstraint1.cro"
		;
connectAttr "IKParentConstraintTrack_L.pim" "IKParentConstraintTrack_L_parentConstraint1.cpim"
		;
connectAttr "IKParentConstraintTrack_L.rp" "IKParentConstraintTrack_L_parentConstraint1.crp"
		;
connectAttr "IKParentConstraintTrack_L.rpt" "IKParentConstraintTrack_L_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tt";
connectAttr "Pelvis_M.rp" "IKParentConstraintTrack_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "IKParentConstraintTrack_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tr";
connectAttr "Pelvis_M.ro" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "IKParentConstraintTrack_L_parentConstraint1.tg[0].ts";
connectAttr "Pelvis_M.pm" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.w0" "IKParentConstraintTrack_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "IKParentConstraintLeg_R.v";
connectAttr "IKXTrack_R.msg" "IKXLegHandle_R.hsj";
connectAttr "effector1.hp" "IKXLegHandle_R.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_R.hsv";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctx" "IKXLegHandle_R.pvx";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.cty" "IKXLegHandle_R.pvy";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.ctz" "IKXLegHandle_R.pvz";
connectAttr "IKXLegHandle_R.pim" "IKXLegHandle_R_poleVectorConstraint1.cpim";
connectAttr "IKXTrack_R.pm" "IKXLegHandle_R_poleVectorConstraint1.ps";
connectAttr "IKXTrack_R.t" "IKXLegHandle_R_poleVectorConstraint1.crp";
connectAttr "PoleLeg_R.t" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_R.rp" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_R.rpt" "IKXLegHandle_R_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_R.pm" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_R_poleVectorConstraint1.w0" "IKXLegHandle_R_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_R_aimConstraint1.crx" "PoleAimLeg_R.rx";
connectAttr "PoleAimLeg_R_aimConstraint1.cry" "PoleAimLeg_R.ry";
connectAttr "PoleAimLeg_R_aimConstraint1.crz" "PoleAimLeg_R.rz";
connectAttr "PoleAimLeg_R_pointConstraint1.ctx" "PoleAimLeg_R.tx";
connectAttr "PoleAimLeg_R_pointConstraint1.cty" "PoleAimLeg_R.ty";
connectAttr "PoleAimLeg_R_pointConstraint1.ctz" "PoleAimLeg_R.tz";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_aimConstraint1.cpim";
connectAttr "PoleAimLeg_R.t" "PoleAimLeg_R_aimConstraint1.ct";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_aimConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.crt";
connectAttr "PoleAimLeg_R.ro" "PoleAimLeg_R_aimConstraint1.cro";
connectAttr "IKLeg_R.t" "PoleAimLeg_R_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PoleAimLeg_R_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PoleAimLeg_R_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PoleAimLeg_R_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_aimConstraint1.w0" "PoleAimLeg_R_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_R.wm" "PoleAimLeg_R_aimConstraint1.wum";
connectAttr "unitConversion1.o" "PoleAimLeg_R_aimConstraint1.ox";
connectAttr "PoleAimLeg_R.pim" "PoleAimLeg_R_pointConstraint1.cpim";
connectAttr "PoleAimLeg_R.rp" "PoleAimLeg_R_pointConstraint1.crp";
connectAttr "PoleAimLeg_R.rpt" "PoleAimLeg_R_pointConstraint1.crt";
connectAttr "IKXTrack_R.t" "PoleAimLeg_R_pointConstraint1.tg[0].tt";
connectAttr "IKXTrack_R.rp" "PoleAimLeg_R_pointConstraint1.tg[0].trp";
connectAttr "IKXTrack_R.rpt" "PoleAimLeg_R_pointConstraint1.tg[0].trt";
connectAttr "IKXTrack_R.pm" "PoleAimLeg_R_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_R_pointConstraint1.w0" "PoleAimLeg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctx" "PoleParentConstraintLeg_R.tx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cty" "PoleParentConstraintLeg_R.ty"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.ctz" "PoleParentConstraintLeg_R.tz"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crx" "PoleParentConstraintLeg_R.rx"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.cry" "PoleParentConstraintLeg_R.ry"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.crz" "PoleParentConstraintLeg_R.rz"
		;
connectAttr "FKIKBlendLegCondition_R.ocr" "PoleParentConstraintLeg_R.v";
connectAttr "PoleParentConstraintLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_R.pim" "PoleParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_RStatic.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_RStatic.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_RStatic.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_RStatic.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_RStatic.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w0" "PoleParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_R.t" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_R.rp" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_R.rpt" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_R.r" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_R.ro" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_R.s" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_R.pm" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.w1" "PoleParentConstraintLeg_R_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_RSetRangeFollow.oy" "PoleParentConstraintLeg_R_parentConstraint1.w0"
		;
connectAttr "PoleLeg_RSetRangeFollow.ox" "PoleParentConstraintLeg_R_parentConstraint1.w1"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "IKParentConstraintLeg_L.v";
connectAttr "IKXTrack_L.msg" "IKXLegHandle_L.hsj";
connectAttr "effector2.hp" "IKXLegHandle_L.hee";
connectAttr "ikRPsolver.msg" "IKXLegHandle_L.hsv";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctx" "IKXLegHandle_L.pvx";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.cty" "IKXLegHandle_L.pvy";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.ctz" "IKXLegHandle_L.pvz";
connectAttr "IKXLegHandle_L.pim" "IKXLegHandle_L_poleVectorConstraint1.cpim";
connectAttr "IKXTrack_L.pm" "IKXLegHandle_L_poleVectorConstraint1.ps";
connectAttr "IKXTrack_L.t" "IKXLegHandle_L_poleVectorConstraint1.crp";
connectAttr "PoleLeg_L.t" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tt";
connectAttr "PoleLeg_L.rp" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trp";
connectAttr "PoleLeg_L.rpt" "IKXLegHandle_L_poleVectorConstraint1.tg[0].trt";
connectAttr "PoleLeg_L.pm" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tpm";
connectAttr "IKXLegHandle_L_poleVectorConstraint1.w0" "IKXLegHandle_L_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_L_aimConstraint1.crx" "PoleAimLeg_L.rx";
connectAttr "PoleAimLeg_L_aimConstraint1.cry" "PoleAimLeg_L.ry";
connectAttr "PoleAimLeg_L_aimConstraint1.crz" "PoleAimLeg_L.rz";
connectAttr "PoleAimLeg_L_pointConstraint1.ctx" "PoleAimLeg_L.tx";
connectAttr "PoleAimLeg_L_pointConstraint1.cty" "PoleAimLeg_L.ty";
connectAttr "PoleAimLeg_L_pointConstraint1.ctz" "PoleAimLeg_L.tz";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_aimConstraint1.cpim";
connectAttr "PoleAimLeg_L.t" "PoleAimLeg_L_aimConstraint1.ct";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_aimConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.crt";
connectAttr "PoleAimLeg_L.ro" "PoleAimLeg_L_aimConstraint1.cro";
connectAttr "IKLeg_L.t" "PoleAimLeg_L_aimConstraint1.tg[0].tt";
connectAttr "IKLeg_L.rp" "PoleAimLeg_L_aimConstraint1.tg[0].trp";
connectAttr "IKLeg_L.rpt" "PoleAimLeg_L_aimConstraint1.tg[0].trt";
connectAttr "IKLeg_L.pm" "PoleAimLeg_L_aimConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_aimConstraint1.w0" "PoleAimLeg_L_aimConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.wm" "PoleAimLeg_L_aimConstraint1.wum";
connectAttr "unitConversion2.o" "PoleAimLeg_L_aimConstraint1.ox";
connectAttr "PoleAimLeg_L.pim" "PoleAimLeg_L_pointConstraint1.cpim";
connectAttr "PoleAimLeg_L.rp" "PoleAimLeg_L_pointConstraint1.crp";
connectAttr "PoleAimLeg_L.rpt" "PoleAimLeg_L_pointConstraint1.crt";
connectAttr "IKXTrack_L.t" "PoleAimLeg_L_pointConstraint1.tg[0].tt";
connectAttr "IKXTrack_L.rp" "PoleAimLeg_L_pointConstraint1.tg[0].trp";
connectAttr "IKXTrack_L.rpt" "PoleAimLeg_L_pointConstraint1.tg[0].trt";
connectAttr "IKXTrack_L.pm" "PoleAimLeg_L_pointConstraint1.tg[0].tpm";
connectAttr "PoleAimLeg_L_pointConstraint1.w0" "PoleAimLeg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctx" "PoleParentConstraintLeg_L.tx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cty" "PoleParentConstraintLeg_L.ty"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.ctz" "PoleParentConstraintLeg_L.tz"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crx" "PoleParentConstraintLeg_L.rx"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.cry" "PoleParentConstraintLeg_L.ry"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.crz" "PoleParentConstraintLeg_L.rz"
		;
connectAttr "FKIKBlendLegCondition_L.ocr" "PoleParentConstraintLeg_L.v";
connectAttr "PoleParentConstraintLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "PoleParentConstraintLeg_L.pim" "PoleParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "PoleParentConstraintLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "PoleParentConstraintLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "PoleParentConstraintLeg_LStatic.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "PoleParentConstraintLeg_LStatic.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tr"
		;
connectAttr "PoleParentConstraintLeg_LStatic.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "PoleParentConstraintLeg_LStatic.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].ts"
		;
connectAttr "PoleParentConstraintLeg_LStatic.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w0" "PoleParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PoleAimLeg_L.t" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tt"
		;
connectAttr "PoleAimLeg_L.rp" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trp"
		;
connectAttr "PoleAimLeg_L.rpt" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].trt"
		;
connectAttr "PoleAimLeg_L.r" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tr"
		;
connectAttr "PoleAimLeg_L.ro" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tro"
		;
connectAttr "PoleAimLeg_L.s" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].ts"
		;
connectAttr "PoleAimLeg_L.pm" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tpm"
		;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.w1" "PoleParentConstraintLeg_L_parentConstraint1.tg[1].tw"
		;
connectAttr "PoleLeg_LSetRangeFollow.oy" "PoleParentConstraintLeg_L_parentConstraint1.w0"
		;
connectAttr "PoleLeg_LSetRangeFollow.ox" "PoleParentConstraintLeg_L_parentConstraint1.w1"
		;
connectAttr "IKmessureLoc1Leg_R_pointConstraint1.ctx" "IKmessureLoc1Leg_R.tx";
connectAttr "IKmessureLoc1Leg_R_pointConstraint1.cty" "IKmessureLoc1Leg_R.ty";
connectAttr "IKmessureLoc1Leg_R_pointConstraint1.ctz" "IKmessureLoc1Leg_R.tz";
connectAttr "IKmessureLoc1Leg_R.pim" "IKmessureLoc1Leg_R_pointConstraint1.cpim";
connectAttr "IKmessureLoc1Leg_R.rp" "IKmessureLoc1Leg_R_pointConstraint1.crp";
connectAttr "IKmessureLoc1Leg_R.rpt" "IKmessureLoc1Leg_R_pointConstraint1.crt";
connectAttr "IKXTrack_R.t" "IKmessureLoc1Leg_R_pointConstraint1.tg[0].tt";
connectAttr "IKXTrack_R.rp" "IKmessureLoc1Leg_R_pointConstraint1.tg[0].trp";
connectAttr "IKXTrack_R.rpt" "IKmessureLoc1Leg_R_pointConstraint1.tg[0].trt";
connectAttr "IKXTrack_R.pm" "IKmessureLoc1Leg_R_pointConstraint1.tg[0].tpm";
connectAttr "IKmessureLoc1Leg_R_pointConstraint1.w0" "IKmessureLoc1Leg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "IKmessureLoc2Leg_R_pointConstraint1.ctx" "IKmessureLoc2Leg_R.tx";
connectAttr "IKmessureLoc2Leg_R_pointConstraint1.cty" "IKmessureLoc2Leg_R.ty";
connectAttr "IKmessureLoc2Leg_R_pointConstraint1.ctz" "IKmessureLoc2Leg_R.tz";
connectAttr "IKmessureLoc2Leg_R.pim" "IKmessureLoc2Leg_R_pointConstraint1.cpim";
connectAttr "IKmessureLoc2Leg_R.rp" "IKmessureLoc2Leg_R_pointConstraint1.crp";
connectAttr "IKmessureLoc2Leg_R.rpt" "IKmessureLoc2Leg_R_pointConstraint1.crt";
connectAttr "IKmessureConstrainToLeg_R.t" "IKmessureLoc2Leg_R_pointConstraint1.tg[0].tt"
		;
connectAttr "IKmessureConstrainToLeg_R.rp" "IKmessureLoc2Leg_R_pointConstraint1.tg[0].trp"
		;
connectAttr "IKmessureConstrainToLeg_R.rpt" "IKmessureLoc2Leg_R_pointConstraint1.tg[0].trt"
		;
connectAttr "IKmessureConstrainToLeg_R.pm" "IKmessureLoc2Leg_R_pointConstraint1.tg[0].tpm"
		;
connectAttr "IKmessureLoc2Leg_R_pointConstraint1.w0" "IKmessureLoc2Leg_R_pointConstraint1.tg[0].tw"
		;
connectAttr "IKmessureLoc2Leg_R.t" "IKdistanceLeg_RShape.ep";
connectAttr "IKdistanceLeg_RShape_antiPop.o" "IKdistanceLeg_RShape.antiPop";
connectAttr "IKmessureLoc1Leg_L_pointConstraint1.ctx" "IKmessureLoc1Leg_L.tx";
connectAttr "IKmessureLoc1Leg_L_pointConstraint1.cty" "IKmessureLoc1Leg_L.ty";
connectAttr "IKmessureLoc1Leg_L_pointConstraint1.ctz" "IKmessureLoc1Leg_L.tz";
connectAttr "IKmessureLoc1Leg_L.pim" "IKmessureLoc1Leg_L_pointConstraint1.cpim";
connectAttr "IKmessureLoc1Leg_L.rp" "IKmessureLoc1Leg_L_pointConstraint1.crp";
connectAttr "IKmessureLoc1Leg_L.rpt" "IKmessureLoc1Leg_L_pointConstraint1.crt";
connectAttr "IKXTrack_L.t" "IKmessureLoc1Leg_L_pointConstraint1.tg[0].tt";
connectAttr "IKXTrack_L.rp" "IKmessureLoc1Leg_L_pointConstraint1.tg[0].trp";
connectAttr "IKXTrack_L.rpt" "IKmessureLoc1Leg_L_pointConstraint1.tg[0].trt";
connectAttr "IKXTrack_L.pm" "IKmessureLoc1Leg_L_pointConstraint1.tg[0].tpm";
connectAttr "IKmessureLoc1Leg_L_pointConstraint1.w0" "IKmessureLoc1Leg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "IKmessureLoc2Leg_L_pointConstraint1.ctx" "IKmessureLoc2Leg_L.tx";
connectAttr "IKmessureLoc2Leg_L_pointConstraint1.cty" "IKmessureLoc2Leg_L.ty";
connectAttr "IKmessureLoc2Leg_L_pointConstraint1.ctz" "IKmessureLoc2Leg_L.tz";
connectAttr "IKmessureLoc2Leg_L.pim" "IKmessureLoc2Leg_L_pointConstraint1.cpim";
connectAttr "IKmessureLoc2Leg_L.rp" "IKmessureLoc2Leg_L_pointConstraint1.crp";
connectAttr "IKmessureLoc2Leg_L.rpt" "IKmessureLoc2Leg_L_pointConstraint1.crt";
connectAttr "IKmessureConstrainToLeg_L.t" "IKmessureLoc2Leg_L_pointConstraint1.tg[0].tt"
		;
connectAttr "IKmessureConstrainToLeg_L.rp" "IKmessureLoc2Leg_L_pointConstraint1.tg[0].trp"
		;
connectAttr "IKmessureConstrainToLeg_L.rpt" "IKmessureLoc2Leg_L_pointConstraint1.tg[0].trt"
		;
connectAttr "IKmessureConstrainToLeg_L.pm" "IKmessureLoc2Leg_L_pointConstraint1.tg[0].tpm"
		;
connectAttr "IKmessureLoc2Leg_L_pointConstraint1.w0" "IKmessureLoc2Leg_L_pointConstraint1.tg[0].tw"
		;
connectAttr "IKmessureLoc2Leg_L.t" "IKdistanceLeg_LShape.ep";
connectAttr "IKdistanceLeg_LShape_antiPop.o" "IKdistanceLeg_LShape.antiPop";
connectAttr "Main.fkIkVis" "FKIKSystem.v";
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctx" "FKIKParentConstraintLeg_R.tx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cty" "FKIKParentConstraintLeg_R.ty"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.ctz" "FKIKParentConstraintLeg_R.tz"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crx" "FKIKParentConstraintLeg_R.rx"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.cry" "FKIKParentConstraintLeg_R.ry"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.crz" "FKIKParentConstraintLeg_R.rz"
		;
connectAttr "FKIKParentConstraintLeg_R.ro" "FKIKParentConstraintLeg_R_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_R.pim" "FKIKParentConstraintLeg_R_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_R.rp" "FKIKParentConstraintLeg_R_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_R.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tt";
connectAttr "Pelvis_M.rp" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tr";
connectAttr "Pelvis_M.ro" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].ts";
connectAttr "Pelvis_M.pm" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.w0" "FKIKParentConstraintLeg_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctx" "FKIKParentConstraintLeg_L.tx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cty" "FKIKParentConstraintLeg_L.ty"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.ctz" "FKIKParentConstraintLeg_L.tz"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crx" "FKIKParentConstraintLeg_L.rx"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.cry" "FKIKParentConstraintLeg_L.ry"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.crz" "FKIKParentConstraintLeg_L.rz"
		;
connectAttr "FKIKParentConstraintLeg_L.ro" "FKIKParentConstraintLeg_L_parentConstraint1.cro"
		;
connectAttr "FKIKParentConstraintLeg_L.pim" "FKIKParentConstraintLeg_L_parentConstraint1.cpim"
		;
connectAttr "FKIKParentConstraintLeg_L.rp" "FKIKParentConstraintLeg_L_parentConstraint1.crp"
		;
connectAttr "FKIKParentConstraintLeg_L.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.crt"
		;
connectAttr "Pelvis_M.t" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tt";
connectAttr "Pelvis_M.rp" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trp"
		;
connectAttr "Pelvis_M.rpt" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].trt"
		;
connectAttr "Pelvis_M.r" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tr";
connectAttr "Pelvis_M.ro" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tro"
		;
connectAttr "Pelvis_M.s" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].ts";
connectAttr "Pelvis_M.pm" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "Pelvis_M.jo" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.w0" "FKIKParentConstraintLeg_L_parentConstraint1.tg[0].tw"
		;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.cry" "PelvisCenterBtwLegs_M.ry"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctx" "PelvisCenterBtwLegs_M.tx"
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.ctz" "PelvisCenterBtwLegs_M.tz"
		;
connectAttr "PelvisCenterBtwLegs_M.ro" "PelvisCenterBtwLegs_M_orientConstraint1.cro"
		;
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_orientConstraint1.cpim"
		;
connectAttr "IKLeg_R.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tr";
connectAttr "IKLeg_R.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tro";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w0" "PelvisCenterBtwLegs_M_orientConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.r" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tr";
connectAttr "IKLeg_L.ro" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tro";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.w1" "PelvisCenterBtwLegs_M_orientConstraint1.tg[1].tw"
		;
connectAttr "CenterBtwFeet_M.ox" "PelvisCenterBtwLegs_M_pointConstraint1.nds";
connectAttr "PelvisCenterBtwLegs_M.pim" "PelvisCenterBtwLegs_M_pointConstraint1.cpim"
		;
connectAttr "PelvisCenterBtwLegs_M.rp" "PelvisCenterBtwLegs_M_pointConstraint1.crp"
		;
connectAttr "PelvisCenterBtwLegs_M.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.crt"
		;
connectAttr "IKLeg_R.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tt";
connectAttr "IKLeg_R.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trp";
connectAttr "IKLeg_R.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].trt";
connectAttr "IKLeg_R.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w0" "PelvisCenterBtwLegs_M_pointConstraint1.tg[0].tw"
		;
connectAttr "IKLeg_L.t" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tt";
connectAttr "IKLeg_L.rp" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trp";
connectAttr "IKLeg_L.rpt" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].trt";
connectAttr "IKLeg_L.pm" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tpm";
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.w1" "PelvisCenterBtwLegs_M_pointConstraint1.tg[1].tw"
		;
connectAttr "FKPelvis_M.s" "Pelvis_M.s";
connectAttr "Pelvis_M_pointConstraint1.ctx" "Pelvis_M.tx";
connectAttr "Pelvis_M_pointConstraint1.cty" "Pelvis_M.ty";
connectAttr "Pelvis_M_pointConstraint1.ctz" "Pelvis_M.tz";
connectAttr "Pelvis_M_orientConstraint1.crx" "Pelvis_M.rx";
connectAttr "Pelvis_M_orientConstraint1.cry" "Pelvis_M.ry";
connectAttr "Pelvis_M_orientConstraint1.crz" "Pelvis_M.rz";
connectAttr "jointLayer.di" "Pelvis_M.do";
connectAttr "FKBody_M.s" "Body_M.s";
connectAttr "Pelvis_M.s" "Body_M.is";
connectAttr "Body_M_parentConstraint1.ctx" "Body_M.tx";
connectAttr "Body_M_parentConstraint1.cty" "Body_M.ty";
connectAttr "Body_M_parentConstraint1.ctz" "Body_M.tz";
connectAttr "Body_M_parentConstraint1.crx" "Body_M.rx";
connectAttr "Body_M_parentConstraint1.cry" "Body_M.ry";
connectAttr "Body_M_parentConstraint1.crz" "Body_M.rz";
connectAttr "jointLayer.di" "Body_M.do";
connectAttr "FKWeapon_M.s" "Weapon_M.s";
connectAttr "Body_M.s" "Weapon_M.is";
connectAttr "Weapon_M_parentConstraint1.ctx" "Weapon_M.tx";
connectAttr "Weapon_M_parentConstraint1.cty" "Weapon_M.ty";
connectAttr "Weapon_M_parentConstraint1.ctz" "Weapon_M.tz";
connectAttr "Weapon_M_parentConstraint1.crx" "Weapon_M.rx";
connectAttr "Weapon_M_parentConstraint1.cry" "Weapon_M.ry";
connectAttr "Weapon_M_parentConstraint1.crz" "Weapon_M.rz";
connectAttr "jointLayer.di" "Weapon_M.do";
connectAttr "Weapon_M.s" "Weapon_End_M.is";
connectAttr "jointLayer.di" "Weapon_End_M.do";
connectAttr "Weapon_M.ro" "Weapon_M_parentConstraint1.cro";
connectAttr "Weapon_M.pim" "Weapon_M_parentConstraint1.cpim";
connectAttr "Weapon_M.rp" "Weapon_M_parentConstraint1.crp";
connectAttr "Weapon_M.rpt" "Weapon_M_parentConstraint1.crt";
connectAttr "Weapon_M.jo" "Weapon_M_parentConstraint1.cjo";
connectAttr "FKXWeapon_M.t" "Weapon_M_parentConstraint1.tg[0].tt";
connectAttr "FKXWeapon_M.rp" "Weapon_M_parentConstraint1.tg[0].trp";
connectAttr "FKXWeapon_M.rpt" "Weapon_M_parentConstraint1.tg[0].trt";
connectAttr "FKXWeapon_M.r" "Weapon_M_parentConstraint1.tg[0].tr";
connectAttr "FKXWeapon_M.ro" "Weapon_M_parentConstraint1.tg[0].tro";
connectAttr "FKXWeapon_M.s" "Weapon_M_parentConstraint1.tg[0].ts";
connectAttr "FKXWeapon_M.pm" "Weapon_M_parentConstraint1.tg[0].tpm";
connectAttr "FKXWeapon_M.jo" "Weapon_M_parentConstraint1.tg[0].tjo";
connectAttr "Weapon_M_parentConstraint1.w0" "Weapon_M_parentConstraint1.tg[0].tw"
		;
connectAttr "FKShoulder_R.s" "Shoulder_R.s";
connectAttr "Body_M.s" "Shoulder_R.is";
connectAttr "Shoulder_R_parentConstraint1.ctx" "Shoulder_R.tx";
connectAttr "Shoulder_R_parentConstraint1.cty" "Shoulder_R.ty";
connectAttr "Shoulder_R_parentConstraint1.ctz" "Shoulder_R.tz";
connectAttr "Shoulder_R_parentConstraint1.crx" "Shoulder_R.rx";
connectAttr "Shoulder_R_parentConstraint1.cry" "Shoulder_R.ry";
connectAttr "Shoulder_R_parentConstraint1.crz" "Shoulder_R.rz";
connectAttr "jointLayer.di" "Shoulder_R.do";
connectAttr "FKElbow_R.s" "Elbow_R.s";
connectAttr "Shoulder_R.s" "Elbow_R.is";
connectAttr "Elbow_R_parentConstraint1.ctx" "Elbow_R.tx";
connectAttr "Elbow_R_parentConstraint1.cty" "Elbow_R.ty";
connectAttr "Elbow_R_parentConstraint1.ctz" "Elbow_R.tz";
connectAttr "Elbow_R_parentConstraint1.crx" "Elbow_R.rx";
connectAttr "Elbow_R_parentConstraint1.cry" "Elbow_R.ry";
connectAttr "Elbow_R_parentConstraint1.crz" "Elbow_R.rz";
connectAttr "jointLayer.di" "Elbow_R.do";
connectAttr "FKMiddleFinger1_R.s" "MiddleFinger1_R.s";
connectAttr "Elbow_R.s" "MiddleFinger1_R.is";
connectAttr "MiddleFinger1_R_parentConstraint1.ctx" "MiddleFinger1_R.tx";
connectAttr "MiddleFinger1_R_parentConstraint1.cty" "MiddleFinger1_R.ty";
connectAttr "MiddleFinger1_R_parentConstraint1.ctz" "MiddleFinger1_R.tz";
connectAttr "MiddleFinger1_R_parentConstraint1.crx" "MiddleFinger1_R.rx";
connectAttr "MiddleFinger1_R_parentConstraint1.cry" "MiddleFinger1_R.ry";
connectAttr "MiddleFinger1_R_parentConstraint1.crz" "MiddleFinger1_R.rz";
connectAttr "jointLayer.di" "MiddleFinger1_R.do";
connectAttr "FKMiddleFinger2_R.s" "MiddleFinger2_R.s";
connectAttr "MiddleFinger1_R.s" "MiddleFinger2_R.is";
connectAttr "MiddleFinger2_R_parentConstraint1.ctx" "MiddleFinger2_R.tx";
connectAttr "MiddleFinger2_R_parentConstraint1.cty" "MiddleFinger2_R.ty";
connectAttr "MiddleFinger2_R_parentConstraint1.ctz" "MiddleFinger2_R.tz";
connectAttr "MiddleFinger2_R_parentConstraint1.crx" "MiddleFinger2_R.rx";
connectAttr "MiddleFinger2_R_parentConstraint1.cry" "MiddleFinger2_R.ry";
connectAttr "MiddleFinger2_R_parentConstraint1.crz" "MiddleFinger2_R.rz";
connectAttr "jointLayer.di" "MiddleFinger2_R.do";
connectAttr "MiddleFinger2_R.s" "MiddleFinger3_End_R.is";
connectAttr "jointLayer.di" "MiddleFinger3_End_R.do";
connectAttr "MiddleFinger2_R.ro" "MiddleFinger2_R_parentConstraint1.cro";
connectAttr "MiddleFinger2_R.pim" "MiddleFinger2_R_parentConstraint1.cpim";
connectAttr "MiddleFinger2_R.rp" "MiddleFinger2_R_parentConstraint1.crp";
connectAttr "MiddleFinger2_R.rpt" "MiddleFinger2_R_parentConstraint1.crt";
connectAttr "MiddleFinger2_R.jo" "MiddleFinger2_R_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger2_R.t" "MiddleFinger2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger2_R.rp" "MiddleFinger2_R_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger2_R.rpt" "MiddleFinger2_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger2_R.r" "MiddleFinger2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger2_R.ro" "MiddleFinger2_R_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger2_R.s" "MiddleFinger2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger2_R.pm" "MiddleFinger2_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger2_R.jo" "MiddleFinger2_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger2_R_parentConstraint1.w0" "MiddleFinger2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "MiddleFinger1_R.ro" "MiddleFinger1_R_parentConstraint1.cro";
connectAttr "MiddleFinger1_R.pim" "MiddleFinger1_R_parentConstraint1.cpim";
connectAttr "MiddleFinger1_R.rp" "MiddleFinger1_R_parentConstraint1.crp";
connectAttr "MiddleFinger1_R.rpt" "MiddleFinger1_R_parentConstraint1.crt";
connectAttr "MiddleFinger1_R.jo" "MiddleFinger1_R_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger1_R.t" "MiddleFinger1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger1_R.rp" "MiddleFinger1_R_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger1_R.rpt" "MiddleFinger1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger1_R.r" "MiddleFinger1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger1_R.ro" "MiddleFinger1_R_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger1_R.s" "MiddleFinger1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger1_R.pm" "MiddleFinger1_R_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger1_R.jo" "MiddleFinger1_R_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger1_R_parentConstraint1.w0" "MiddleFinger1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIndexFinger1_R.s" "IndexFinger1_R.s";
connectAttr "Elbow_R.s" "IndexFinger1_R.is";
connectAttr "IndexFinger1_R_parentConstraint1.ctx" "IndexFinger1_R.tx";
connectAttr "IndexFinger1_R_parentConstraint1.cty" "IndexFinger1_R.ty";
connectAttr "IndexFinger1_R_parentConstraint1.ctz" "IndexFinger1_R.tz";
connectAttr "IndexFinger1_R_parentConstraint1.crx" "IndexFinger1_R.rx";
connectAttr "IndexFinger1_R_parentConstraint1.cry" "IndexFinger1_R.ry";
connectAttr "IndexFinger1_R_parentConstraint1.crz" "IndexFinger1_R.rz";
connectAttr "jointLayer.di" "IndexFinger1_R.do";
connectAttr "FKIndexFinger2_R.s" "IndexFinger2_R.s";
connectAttr "IndexFinger1_R.s" "IndexFinger2_R.is";
connectAttr "IndexFinger2_R_parentConstraint1.ctx" "IndexFinger2_R.tx";
connectAttr "IndexFinger2_R_parentConstraint1.cty" "IndexFinger2_R.ty";
connectAttr "IndexFinger2_R_parentConstraint1.ctz" "IndexFinger2_R.tz";
connectAttr "IndexFinger2_R_parentConstraint1.crx" "IndexFinger2_R.rx";
connectAttr "IndexFinger2_R_parentConstraint1.cry" "IndexFinger2_R.ry";
connectAttr "IndexFinger2_R_parentConstraint1.crz" "IndexFinger2_R.rz";
connectAttr "jointLayer.di" "IndexFinger2_R.do";
connectAttr "IndexFinger2_R.s" "IndexFinger3_End_R.is";
connectAttr "jointLayer.di" "IndexFinger3_End_R.do";
connectAttr "IndexFinger2_R.ro" "IndexFinger2_R_parentConstraint1.cro";
connectAttr "IndexFinger2_R.pim" "IndexFinger2_R_parentConstraint1.cpim";
connectAttr "IndexFinger2_R.rp" "IndexFinger2_R_parentConstraint1.crp";
connectAttr "IndexFinger2_R.rpt" "IndexFinger2_R_parentConstraint1.crt";
connectAttr "IndexFinger2_R.jo" "IndexFinger2_R_parentConstraint1.cjo";
connectAttr "FKXIndexFinger2_R.t" "IndexFinger2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger2_R.rp" "IndexFinger2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger2_R.rpt" "IndexFinger2_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger2_R.r" "IndexFinger2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger2_R.ro" "IndexFinger2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger2_R.s" "IndexFinger2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger2_R.pm" "IndexFinger2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger2_R.jo" "IndexFinger2_R_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger2_R_parentConstraint1.w0" "IndexFinger2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IndexFinger1_R.ro" "IndexFinger1_R_parentConstraint1.cro";
connectAttr "IndexFinger1_R.pim" "IndexFinger1_R_parentConstraint1.cpim";
connectAttr "IndexFinger1_R.rp" "IndexFinger1_R_parentConstraint1.crp";
connectAttr "IndexFinger1_R.rpt" "IndexFinger1_R_parentConstraint1.crt";
connectAttr "IndexFinger1_R.jo" "IndexFinger1_R_parentConstraint1.cjo";
connectAttr "FKXIndexFinger1_R.t" "IndexFinger1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger1_R.rp" "IndexFinger1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger1_R.rpt" "IndexFinger1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger1_R.r" "IndexFinger1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger1_R.ro" "IndexFinger1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger1_R.s" "IndexFinger1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger1_R.pm" "IndexFinger1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger1_R.jo" "IndexFinger1_R_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger1_R_parentConstraint1.w0" "IndexFinger1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKThumbFinger1_R.s" "ThumbFinger1_R.s";
connectAttr "Elbow_R.s" "ThumbFinger1_R.is";
connectAttr "ThumbFinger1_R_parentConstraint1.ctx" "ThumbFinger1_R.tx";
connectAttr "ThumbFinger1_R_parentConstraint1.cty" "ThumbFinger1_R.ty";
connectAttr "ThumbFinger1_R_parentConstraint1.ctz" "ThumbFinger1_R.tz";
connectAttr "ThumbFinger1_R_parentConstraint1.crx" "ThumbFinger1_R.rx";
connectAttr "ThumbFinger1_R_parentConstraint1.cry" "ThumbFinger1_R.ry";
connectAttr "ThumbFinger1_R_parentConstraint1.crz" "ThumbFinger1_R.rz";
connectAttr "jointLayer.di" "ThumbFinger1_R.do";
connectAttr "FKThumbFinger2_R.s" "ThumbFinger2_R.s";
connectAttr "ThumbFinger1_R.s" "ThumbFinger2_R.is";
connectAttr "ThumbFinger2_R_parentConstraint1.ctx" "ThumbFinger2_R.tx";
connectAttr "ThumbFinger2_R_parentConstraint1.cty" "ThumbFinger2_R.ty";
connectAttr "ThumbFinger2_R_parentConstraint1.ctz" "ThumbFinger2_R.tz";
connectAttr "ThumbFinger2_R_parentConstraint1.crx" "ThumbFinger2_R.rx";
connectAttr "ThumbFinger2_R_parentConstraint1.cry" "ThumbFinger2_R.ry";
connectAttr "ThumbFinger2_R_parentConstraint1.crz" "ThumbFinger2_R.rz";
connectAttr "jointLayer.di" "ThumbFinger2_R.do";
connectAttr "ThumbFinger2_R.s" "ThumbFinger3_End_R.is";
connectAttr "jointLayer.di" "ThumbFinger3_End_R.do";
connectAttr "ThumbFinger2_R.ro" "ThumbFinger2_R_parentConstraint1.cro";
connectAttr "ThumbFinger2_R.pim" "ThumbFinger2_R_parentConstraint1.cpim";
connectAttr "ThumbFinger2_R.rp" "ThumbFinger2_R_parentConstraint1.crp";
connectAttr "ThumbFinger2_R.rpt" "ThumbFinger2_R_parentConstraint1.crt";
connectAttr "ThumbFinger2_R.jo" "ThumbFinger2_R_parentConstraint1.cjo";
connectAttr "FKXThumbFinger2_R.t" "ThumbFinger2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger2_R.rp" "ThumbFinger2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger2_R.rpt" "ThumbFinger2_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger2_R.r" "ThumbFinger2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger2_R.ro" "ThumbFinger2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger2_R.s" "ThumbFinger2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger2_R.pm" "ThumbFinger2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger2_R.jo" "ThumbFinger2_R_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger2_R_parentConstraint1.w0" "ThumbFinger2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "ThumbFinger1_R.ro" "ThumbFinger1_R_parentConstraint1.cro";
connectAttr "ThumbFinger1_R.pim" "ThumbFinger1_R_parentConstraint1.cpim";
connectAttr "ThumbFinger1_R.rp" "ThumbFinger1_R_parentConstraint1.crp";
connectAttr "ThumbFinger1_R.rpt" "ThumbFinger1_R_parentConstraint1.crt";
connectAttr "ThumbFinger1_R.jo" "ThumbFinger1_R_parentConstraint1.cjo";
connectAttr "FKXThumbFinger1_R.t" "ThumbFinger1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger1_R.rp" "ThumbFinger1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger1_R.rpt" "ThumbFinger1_R_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger1_R.r" "ThumbFinger1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger1_R.ro" "ThumbFinger1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger1_R.s" "ThumbFinger1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger1_R.pm" "ThumbFinger1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger1_R.jo" "ThumbFinger1_R_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger1_R_parentConstraint1.w0" "ThumbFinger1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "Elbow_R.ro" "Elbow_R_parentConstraint1.cro";
connectAttr "Elbow_R.pim" "Elbow_R_parentConstraint1.cpim";
connectAttr "Elbow_R.rp" "Elbow_R_parentConstraint1.crp";
connectAttr "Elbow_R.rpt" "Elbow_R_parentConstraint1.crt";
connectAttr "Elbow_R.jo" "Elbow_R_parentConstraint1.cjo";
connectAttr "FKXElbow_R.t" "Elbow_R_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow_R.rp" "Elbow_R_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow_R.rpt" "Elbow_R_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow_R.r" "Elbow_R_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow_R.ro" "Elbow_R_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow_R.s" "Elbow_R_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow_R.pm" "Elbow_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow_R.jo" "Elbow_R_parentConstraint1.tg[0].tjo";
connectAttr "Elbow_R_parentConstraint1.w0" "Elbow_R_parentConstraint1.tg[0].tw";
connectAttr "Shoulder_R.ro" "Shoulder_R_parentConstraint1.cro";
connectAttr "Shoulder_R.pim" "Shoulder_R_parentConstraint1.cpim";
connectAttr "Shoulder_R.rp" "Shoulder_R_parentConstraint1.crp";
connectAttr "Shoulder_R.rpt" "Shoulder_R_parentConstraint1.crt";
connectAttr "Shoulder_R.jo" "Shoulder_R_parentConstraint1.cjo";
connectAttr "FKXShoulder_R.t" "Shoulder_R_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder_R.rp" "Shoulder_R_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder_R.rpt" "Shoulder_R_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder_R.r" "Shoulder_R_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder_R.ro" "Shoulder_R_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder_R.s" "Shoulder_R_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder_R.pm" "Shoulder_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder_R.jo" "Shoulder_R_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder_R_parentConstraint1.w0" "Shoulder_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKShoulder_L.s" "Shoulder_L.s";
connectAttr "Body_M.s" "Shoulder_L.is";
connectAttr "Shoulder_L_parentConstraint1.ctx" "Shoulder_L.tx";
connectAttr "Shoulder_L_parentConstraint1.cty" "Shoulder_L.ty";
connectAttr "Shoulder_L_parentConstraint1.ctz" "Shoulder_L.tz";
connectAttr "Shoulder_L_parentConstraint1.crx" "Shoulder_L.rx";
connectAttr "Shoulder_L_parentConstraint1.cry" "Shoulder_L.ry";
connectAttr "Shoulder_L_parentConstraint1.crz" "Shoulder_L.rz";
connectAttr "jointLayer.di" "Shoulder_L.do";
connectAttr "FKElbow_L.s" "Elbow_L.s";
connectAttr "Shoulder_L.s" "Elbow_L.is";
connectAttr "Elbow_L_parentConstraint1.ctx" "Elbow_L.tx";
connectAttr "Elbow_L_parentConstraint1.cty" "Elbow_L.ty";
connectAttr "Elbow_L_parentConstraint1.ctz" "Elbow_L.tz";
connectAttr "Elbow_L_parentConstraint1.crx" "Elbow_L.rx";
connectAttr "Elbow_L_parentConstraint1.cry" "Elbow_L.ry";
connectAttr "Elbow_L_parentConstraint1.crz" "Elbow_L.rz";
connectAttr "jointLayer.di" "Elbow_L.do";
connectAttr "FKMiddleFinger1_L.s" "MiddleFinger1_L.s";
connectAttr "Elbow_L.s" "MiddleFinger1_L.is";
connectAttr "MiddleFinger1_L_parentConstraint1.ctx" "MiddleFinger1_L.tx";
connectAttr "MiddleFinger1_L_parentConstraint1.cty" "MiddleFinger1_L.ty";
connectAttr "MiddleFinger1_L_parentConstraint1.ctz" "MiddleFinger1_L.tz";
connectAttr "MiddleFinger1_L_parentConstraint1.crx" "MiddleFinger1_L.rx";
connectAttr "MiddleFinger1_L_parentConstraint1.cry" "MiddleFinger1_L.ry";
connectAttr "MiddleFinger1_L_parentConstraint1.crz" "MiddleFinger1_L.rz";
connectAttr "jointLayer.di" "MiddleFinger1_L.do";
connectAttr "FKMiddleFinger2_L.s" "MiddleFinger2_L.s";
connectAttr "MiddleFinger1_L.s" "MiddleFinger2_L.is";
connectAttr "MiddleFinger2_L_parentConstraint1.ctx" "MiddleFinger2_L.tx";
connectAttr "MiddleFinger2_L_parentConstraint1.cty" "MiddleFinger2_L.ty";
connectAttr "MiddleFinger2_L_parentConstraint1.ctz" "MiddleFinger2_L.tz";
connectAttr "MiddleFinger2_L_parentConstraint1.crx" "MiddleFinger2_L.rx";
connectAttr "MiddleFinger2_L_parentConstraint1.cry" "MiddleFinger2_L.ry";
connectAttr "MiddleFinger2_L_parentConstraint1.crz" "MiddleFinger2_L.rz";
connectAttr "jointLayer.di" "MiddleFinger2_L.do";
connectAttr "MiddleFinger2_L.s" "MiddleFinger3_End_L.is";
connectAttr "jointLayer.di" "MiddleFinger3_End_L.do";
connectAttr "MiddleFinger2_L.ro" "MiddleFinger2_L_parentConstraint1.cro";
connectAttr "MiddleFinger2_L.pim" "MiddleFinger2_L_parentConstraint1.cpim";
connectAttr "MiddleFinger2_L.rp" "MiddleFinger2_L_parentConstraint1.crp";
connectAttr "MiddleFinger2_L.rpt" "MiddleFinger2_L_parentConstraint1.crt";
connectAttr "MiddleFinger2_L.jo" "MiddleFinger2_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger2_L.t" "MiddleFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger2_L.rp" "MiddleFinger2_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger2_L.rpt" "MiddleFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger2_L.r" "MiddleFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger2_L.ro" "MiddleFinger2_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger2_L.s" "MiddleFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger2_L.pm" "MiddleFinger2_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger2_L.jo" "MiddleFinger2_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger2_L_parentConstraint1.w0" "MiddleFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "MiddleFinger1_L.ro" "MiddleFinger1_L_parentConstraint1.cro";
connectAttr "MiddleFinger1_L.pim" "MiddleFinger1_L_parentConstraint1.cpim";
connectAttr "MiddleFinger1_L.rp" "MiddleFinger1_L_parentConstraint1.crp";
connectAttr "MiddleFinger1_L.rpt" "MiddleFinger1_L_parentConstraint1.crt";
connectAttr "MiddleFinger1_L.jo" "MiddleFinger1_L_parentConstraint1.cjo";
connectAttr "FKXMiddleFinger1_L.t" "MiddleFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXMiddleFinger1_L.rp" "MiddleFinger1_L_parentConstraint1.tg[0].trp"
		;
connectAttr "FKXMiddleFinger1_L.rpt" "MiddleFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXMiddleFinger1_L.r" "MiddleFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXMiddleFinger1_L.ro" "MiddleFinger1_L_parentConstraint1.tg[0].tro"
		;
connectAttr "FKXMiddleFinger1_L.s" "MiddleFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXMiddleFinger1_L.pm" "MiddleFinger1_L_parentConstraint1.tg[0].tpm"
		;
connectAttr "FKXMiddleFinger1_L.jo" "MiddleFinger1_L_parentConstraint1.tg[0].tjo"
		;
connectAttr "MiddleFinger1_L_parentConstraint1.w0" "MiddleFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKIndexFinger1_L.s" "IndexFinger1_L.s";
connectAttr "Elbow_L.s" "IndexFinger1_L.is";
connectAttr "IndexFinger1_L_parentConstraint1.ctx" "IndexFinger1_L.tx";
connectAttr "IndexFinger1_L_parentConstraint1.cty" "IndexFinger1_L.ty";
connectAttr "IndexFinger1_L_parentConstraint1.ctz" "IndexFinger1_L.tz";
connectAttr "IndexFinger1_L_parentConstraint1.crx" "IndexFinger1_L.rx";
connectAttr "IndexFinger1_L_parentConstraint1.cry" "IndexFinger1_L.ry";
connectAttr "IndexFinger1_L_parentConstraint1.crz" "IndexFinger1_L.rz";
connectAttr "jointLayer.di" "IndexFinger1_L.do";
connectAttr "FKIndexFinger2_L.s" "IndexFinger2_L.s";
connectAttr "IndexFinger1_L.s" "IndexFinger2_L.is";
connectAttr "IndexFinger2_L_parentConstraint1.ctx" "IndexFinger2_L.tx";
connectAttr "IndexFinger2_L_parentConstraint1.cty" "IndexFinger2_L.ty";
connectAttr "IndexFinger2_L_parentConstraint1.ctz" "IndexFinger2_L.tz";
connectAttr "IndexFinger2_L_parentConstraint1.crx" "IndexFinger2_L.rx";
connectAttr "IndexFinger2_L_parentConstraint1.cry" "IndexFinger2_L.ry";
connectAttr "IndexFinger2_L_parentConstraint1.crz" "IndexFinger2_L.rz";
connectAttr "jointLayer.di" "IndexFinger2_L.do";
connectAttr "IndexFinger2_L.s" "IndexFinger3_End_L.is";
connectAttr "jointLayer.di" "IndexFinger3_End_L.do";
connectAttr "IndexFinger2_L.ro" "IndexFinger2_L_parentConstraint1.cro";
connectAttr "IndexFinger2_L.pim" "IndexFinger2_L_parentConstraint1.cpim";
connectAttr "IndexFinger2_L.rp" "IndexFinger2_L_parentConstraint1.crp";
connectAttr "IndexFinger2_L.rpt" "IndexFinger2_L_parentConstraint1.crt";
connectAttr "IndexFinger2_L.jo" "IndexFinger2_L_parentConstraint1.cjo";
connectAttr "FKXIndexFinger2_L.t" "IndexFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger2_L.rp" "IndexFinger2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger2_L.rpt" "IndexFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger2_L.r" "IndexFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger2_L.ro" "IndexFinger2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger2_L.s" "IndexFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger2_L.pm" "IndexFinger2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger2_L.jo" "IndexFinger2_L_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger2_L_parentConstraint1.w0" "IndexFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IndexFinger1_L.ro" "IndexFinger1_L_parentConstraint1.cro";
connectAttr "IndexFinger1_L.pim" "IndexFinger1_L_parentConstraint1.cpim";
connectAttr "IndexFinger1_L.rp" "IndexFinger1_L_parentConstraint1.crp";
connectAttr "IndexFinger1_L.rpt" "IndexFinger1_L_parentConstraint1.crt";
connectAttr "IndexFinger1_L.jo" "IndexFinger1_L_parentConstraint1.cjo";
connectAttr "FKXIndexFinger1_L.t" "IndexFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXIndexFinger1_L.rp" "IndexFinger1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXIndexFinger1_L.rpt" "IndexFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXIndexFinger1_L.r" "IndexFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXIndexFinger1_L.ro" "IndexFinger1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXIndexFinger1_L.s" "IndexFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXIndexFinger1_L.pm" "IndexFinger1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXIndexFinger1_L.jo" "IndexFinger1_L_parentConstraint1.tg[0].tjo";
connectAttr "IndexFinger1_L_parentConstraint1.w0" "IndexFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKThumbFinger1_L.s" "ThumbFinger1_L.s";
connectAttr "Elbow_L.s" "ThumbFinger1_L.is";
connectAttr "ThumbFinger1_L_parentConstraint1.ctx" "ThumbFinger1_L.tx";
connectAttr "ThumbFinger1_L_parentConstraint1.cty" "ThumbFinger1_L.ty";
connectAttr "ThumbFinger1_L_parentConstraint1.ctz" "ThumbFinger1_L.tz";
connectAttr "ThumbFinger1_L_parentConstraint1.crx" "ThumbFinger1_L.rx";
connectAttr "ThumbFinger1_L_parentConstraint1.cry" "ThumbFinger1_L.ry";
connectAttr "ThumbFinger1_L_parentConstraint1.crz" "ThumbFinger1_L.rz";
connectAttr "jointLayer.di" "ThumbFinger1_L.do";
connectAttr "FKThumbFinger2_L.s" "ThumbFinger2_L.s";
connectAttr "ThumbFinger1_L.s" "ThumbFinger2_L.is";
connectAttr "ThumbFinger2_L_parentConstraint1.ctx" "ThumbFinger2_L.tx";
connectAttr "ThumbFinger2_L_parentConstraint1.cty" "ThumbFinger2_L.ty";
connectAttr "ThumbFinger2_L_parentConstraint1.ctz" "ThumbFinger2_L.tz";
connectAttr "ThumbFinger2_L_parentConstraint1.crx" "ThumbFinger2_L.rx";
connectAttr "ThumbFinger2_L_parentConstraint1.cry" "ThumbFinger2_L.ry";
connectAttr "ThumbFinger2_L_parentConstraint1.crz" "ThumbFinger2_L.rz";
connectAttr "jointLayer.di" "ThumbFinger2_L.do";
connectAttr "ThumbFinger2_L.s" "ThumbFinger3_End_L.is";
connectAttr "jointLayer.di" "ThumbFinger3_End_L.do";
connectAttr "ThumbFinger2_L.ro" "ThumbFinger2_L_parentConstraint1.cro";
connectAttr "ThumbFinger2_L.pim" "ThumbFinger2_L_parentConstraint1.cpim";
connectAttr "ThumbFinger2_L.rp" "ThumbFinger2_L_parentConstraint1.crp";
connectAttr "ThumbFinger2_L.rpt" "ThumbFinger2_L_parentConstraint1.crt";
connectAttr "ThumbFinger2_L.jo" "ThumbFinger2_L_parentConstraint1.cjo";
connectAttr "FKXThumbFinger2_L.t" "ThumbFinger2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger2_L.rp" "ThumbFinger2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger2_L.rpt" "ThumbFinger2_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger2_L.r" "ThumbFinger2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger2_L.ro" "ThumbFinger2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger2_L.s" "ThumbFinger2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger2_L.pm" "ThumbFinger2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger2_L.jo" "ThumbFinger2_L_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger2_L_parentConstraint1.w0" "ThumbFinger2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "ThumbFinger1_L.ro" "ThumbFinger1_L_parentConstraint1.cro";
connectAttr "ThumbFinger1_L.pim" "ThumbFinger1_L_parentConstraint1.cpim";
connectAttr "ThumbFinger1_L.rp" "ThumbFinger1_L_parentConstraint1.crp";
connectAttr "ThumbFinger1_L.rpt" "ThumbFinger1_L_parentConstraint1.crt";
connectAttr "ThumbFinger1_L.jo" "ThumbFinger1_L_parentConstraint1.cjo";
connectAttr "FKXThumbFinger1_L.t" "ThumbFinger1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXThumbFinger1_L.rp" "ThumbFinger1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXThumbFinger1_L.rpt" "ThumbFinger1_L_parentConstraint1.tg[0].trt"
		;
connectAttr "FKXThumbFinger1_L.r" "ThumbFinger1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXThumbFinger1_L.ro" "ThumbFinger1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXThumbFinger1_L.s" "ThumbFinger1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXThumbFinger1_L.pm" "ThumbFinger1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXThumbFinger1_L.jo" "ThumbFinger1_L_parentConstraint1.tg[0].tjo";
connectAttr "ThumbFinger1_L_parentConstraint1.w0" "ThumbFinger1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Elbow_L.ro" "Elbow_L_parentConstraint1.cro";
connectAttr "Elbow_L.pim" "Elbow_L_parentConstraint1.cpim";
connectAttr "Elbow_L.rp" "Elbow_L_parentConstraint1.crp";
connectAttr "Elbow_L.rpt" "Elbow_L_parentConstraint1.crt";
connectAttr "Elbow_L.jo" "Elbow_L_parentConstraint1.cjo";
connectAttr "FKXElbow_L.t" "Elbow_L_parentConstraint1.tg[0].tt";
connectAttr "FKXElbow_L.rp" "Elbow_L_parentConstraint1.tg[0].trp";
connectAttr "FKXElbow_L.rpt" "Elbow_L_parentConstraint1.tg[0].trt";
connectAttr "FKXElbow_L.r" "Elbow_L_parentConstraint1.tg[0].tr";
connectAttr "FKXElbow_L.ro" "Elbow_L_parentConstraint1.tg[0].tro";
connectAttr "FKXElbow_L.s" "Elbow_L_parentConstraint1.tg[0].ts";
connectAttr "FKXElbow_L.pm" "Elbow_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXElbow_L.jo" "Elbow_L_parentConstraint1.tg[0].tjo";
connectAttr "Elbow_L_parentConstraint1.w0" "Elbow_L_parentConstraint1.tg[0].tw";
connectAttr "Shoulder_L.ro" "Shoulder_L_parentConstraint1.cro";
connectAttr "Shoulder_L.pim" "Shoulder_L_parentConstraint1.cpim";
connectAttr "Shoulder_L.rp" "Shoulder_L_parentConstraint1.crp";
connectAttr "Shoulder_L.rpt" "Shoulder_L_parentConstraint1.crt";
connectAttr "Shoulder_L.jo" "Shoulder_L_parentConstraint1.cjo";
connectAttr "FKXShoulder_L.t" "Shoulder_L_parentConstraint1.tg[0].tt";
connectAttr "FKXShoulder_L.rp" "Shoulder_L_parentConstraint1.tg[0].trp";
connectAttr "FKXShoulder_L.rpt" "Shoulder_L_parentConstraint1.tg[0].trt";
connectAttr "FKXShoulder_L.r" "Shoulder_L_parentConstraint1.tg[0].tr";
connectAttr "FKXShoulder_L.ro" "Shoulder_L_parentConstraint1.tg[0].tro";
connectAttr "FKXShoulder_L.s" "Shoulder_L_parentConstraint1.tg[0].ts";
connectAttr "FKXShoulder_L.pm" "Shoulder_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXShoulder_L.jo" "Shoulder_L_parentConstraint1.tg[0].tjo";
connectAttr "Shoulder_L_parentConstraint1.w0" "Shoulder_L_parentConstraint1.tg[0].tw"
		;
connectAttr "Body_M.ro" "Body_M_parentConstraint1.cro";
connectAttr "Body_M.pim" "Body_M_parentConstraint1.cpim";
connectAttr "Body_M.rp" "Body_M_parentConstraint1.crp";
connectAttr "Body_M.rpt" "Body_M_parentConstraint1.crt";
connectAttr "Body_M.jo" "Body_M_parentConstraint1.cjo";
connectAttr "FKXBody_M.t" "Body_M_parentConstraint1.tg[0].tt";
connectAttr "FKXBody_M.rp" "Body_M_parentConstraint1.tg[0].trp";
connectAttr "FKXBody_M.rpt" "Body_M_parentConstraint1.tg[0].trt";
connectAttr "FKXBody_M.r" "Body_M_parentConstraint1.tg[0].tr";
connectAttr "FKXBody_M.ro" "Body_M_parentConstraint1.tg[0].tro";
connectAttr "FKXBody_M.s" "Body_M_parentConstraint1.tg[0].ts";
connectAttr "FKXBody_M.pm" "Body_M_parentConstraint1.tg[0].tpm";
connectAttr "FKXBody_M.jo" "Body_M_parentConstraint1.tg[0].tjo";
connectAttr "Body_M_parentConstraint1.w0" "Body_M_parentConstraint1.tg[0].tw";
connectAttr "ScaleBlendTrack_R.op" "Track_R.s";
connectAttr "Pelvis_M.s" "Track_R.is";
connectAttr "Track_R_parentConstraint1.ctx" "Track_R.tx";
connectAttr "Track_R_parentConstraint1.cty" "Track_R.ty";
connectAttr "Track_R_parentConstraint1.ctz" "Track_R.tz";
connectAttr "Track_R_parentConstraint1.crx" "Track_R.rx";
connectAttr "Track_R_parentConstraint1.cry" "Track_R.ry";
connectAttr "Track_R_parentConstraint1.crz" "Track_R.rz";
connectAttr "jointLayer.di" "Track_R.do";
connectAttr "ScaleBlendTrackBend_R.op" "TrackBend_R.s";
connectAttr "Track_R.s" "TrackBend_R.is";
connectAttr "TrackBend_R_parentConstraint1.ctx" "TrackBend_R.tx";
connectAttr "TrackBend_R_parentConstraint1.cty" "TrackBend_R.ty";
connectAttr "TrackBend_R_parentConstraint1.ctz" "TrackBend_R.tz";
connectAttr "TrackBend_R_parentConstraint1.crx" "TrackBend_R.rx";
connectAttr "TrackBend_R_parentConstraint1.cry" "TrackBend_R.ry";
connectAttr "TrackBend_R_parentConstraint1.crz" "TrackBend_R.rz";
connectAttr "jointLayer.di" "TrackBend_R.do";
connectAttr "ScaleBlendTrackBottom_R.op" "TrackBottom_R.s";
connectAttr "TrackBend_R.s" "TrackBottom_R.is";
connectAttr "TrackBottom_R_parentConstraint1.ctx" "TrackBottom_R.tx";
connectAttr "TrackBottom_R_parentConstraint1.cty" "TrackBottom_R.ty";
connectAttr "TrackBottom_R_parentConstraint1.ctz" "TrackBottom_R.tz";
connectAttr "TrackBottom_R_parentConstraint1.crx" "TrackBottom_R.rx";
connectAttr "TrackBottom_R_parentConstraint1.cry" "TrackBottom_R.ry";
connectAttr "TrackBottom_R_parentConstraint1.crz" "TrackBottom_R.rz";
connectAttr "jointLayer.di" "TrackBottom_R.do";
connectAttr "FKWheel1_R.s" "Wheel1_R.s";
connectAttr "TrackBottom_R.s" "Wheel1_R.is";
connectAttr "Wheel1_R_parentConstraint1.ctx" "Wheel1_R.tx";
connectAttr "Wheel1_R_parentConstraint1.cty" "Wheel1_R.ty";
connectAttr "Wheel1_R_parentConstraint1.ctz" "Wheel1_R.tz";
connectAttr "Wheel1_R_parentConstraint1.crx" "Wheel1_R.rx";
connectAttr "Wheel1_R_parentConstraint1.cry" "Wheel1_R.ry";
connectAttr "Wheel1_R_parentConstraint1.crz" "Wheel1_R.rz";
connectAttr "jointLayer.di" "Wheel1_R.do";
connectAttr "Wheel1_R.s" "Wheel1_End_R.is";
connectAttr "jointLayer.di" "Wheel1_End_R.do";
connectAttr "Wheel1_R.ro" "Wheel1_R_parentConstraint1.cro";
connectAttr "Wheel1_R.pim" "Wheel1_R_parentConstraint1.cpim";
connectAttr "Wheel1_R.rp" "Wheel1_R_parentConstraint1.crp";
connectAttr "Wheel1_R.rpt" "Wheel1_R_parentConstraint1.crt";
connectAttr "Wheel1_R.jo" "Wheel1_R_parentConstraint1.cjo";
connectAttr "FKXWheel1_R.t" "Wheel1_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel1_R.rp" "Wheel1_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel1_R.rpt" "Wheel1_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel1_R.r" "Wheel1_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel1_R.ro" "Wheel1_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel1_R.s" "Wheel1_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel1_R.pm" "Wheel1_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel1_R.jo" "Wheel1_R_parentConstraint1.tg[0].tjo";
connectAttr "Wheel1_R_parentConstraint1.w0" "Wheel1_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel2_R.s" "Wheel2_R.s";
connectAttr "TrackBottom_R.s" "Wheel2_R.is";
connectAttr "Wheel2_R_parentConstraint1.ctx" "Wheel2_R.tx";
connectAttr "Wheel2_R_parentConstraint1.cty" "Wheel2_R.ty";
connectAttr "Wheel2_R_parentConstraint1.ctz" "Wheel2_R.tz";
connectAttr "Wheel2_R_parentConstraint1.crx" "Wheel2_R.rx";
connectAttr "Wheel2_R_parentConstraint1.cry" "Wheel2_R.ry";
connectAttr "Wheel2_R_parentConstraint1.crz" "Wheel2_R.rz";
connectAttr "jointLayer.di" "Wheel2_R.do";
connectAttr "Wheel2_R.s" "Wheel2_End_R.is";
connectAttr "jointLayer.di" "Wheel2_End_R.do";
connectAttr "Wheel2_R.ro" "Wheel2_R_parentConstraint1.cro";
connectAttr "Wheel2_R.pim" "Wheel2_R_parentConstraint1.cpim";
connectAttr "Wheel2_R.rp" "Wheel2_R_parentConstraint1.crp";
connectAttr "Wheel2_R.rpt" "Wheel2_R_parentConstraint1.crt";
connectAttr "Wheel2_R.jo" "Wheel2_R_parentConstraint1.cjo";
connectAttr "FKXWheel2_R.t" "Wheel2_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel2_R.rp" "Wheel2_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel2_R.rpt" "Wheel2_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel2_R.r" "Wheel2_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel2_R.ro" "Wheel2_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel2_R.s" "Wheel2_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel2_R.pm" "Wheel2_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel2_R.jo" "Wheel2_R_parentConstraint1.tg[0].tjo";
connectAttr "Wheel2_R_parentConstraint1.w0" "Wheel2_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel3_R.s" "Wheel3_R.s";
connectAttr "TrackBottom_R.s" "Wheel3_R.is";
connectAttr "Wheel3_R_parentConstraint1.ctx" "Wheel3_R.tx";
connectAttr "Wheel3_R_parentConstraint1.cty" "Wheel3_R.ty";
connectAttr "Wheel3_R_parentConstraint1.ctz" "Wheel3_R.tz";
connectAttr "Wheel3_R_parentConstraint1.crx" "Wheel3_R.rx";
connectAttr "Wheel3_R_parentConstraint1.cry" "Wheel3_R.ry";
connectAttr "Wheel3_R_parentConstraint1.crz" "Wheel3_R.rz";
connectAttr "jointLayer.di" "Wheel3_R.do";
connectAttr "Wheel3_R.s" "Wheel3_End_R.is";
connectAttr "jointLayer.di" "Wheel3_End_R.do";
connectAttr "Wheel3_R.ro" "Wheel3_R_parentConstraint1.cro";
connectAttr "Wheel3_R.pim" "Wheel3_R_parentConstraint1.cpim";
connectAttr "Wheel3_R.rp" "Wheel3_R_parentConstraint1.crp";
connectAttr "Wheel3_R.rpt" "Wheel3_R_parentConstraint1.crt";
connectAttr "Wheel3_R.jo" "Wheel3_R_parentConstraint1.cjo";
connectAttr "FKXWheel3_R.t" "Wheel3_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel3_R.rp" "Wheel3_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel3_R.rpt" "Wheel3_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel3_R.r" "Wheel3_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel3_R.ro" "Wheel3_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel3_R.s" "Wheel3_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel3_R.pm" "Wheel3_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel3_R.jo" "Wheel3_R_parentConstraint1.tg[0].tjo";
connectAttr "Wheel3_R_parentConstraint1.w0" "Wheel3_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel4_R.s" "Wheel4_R.s";
connectAttr "TrackBottom_R.s" "Wheel4_R.is";
connectAttr "Wheel4_R_parentConstraint1.ctx" "Wheel4_R.tx";
connectAttr "Wheel4_R_parentConstraint1.cty" "Wheel4_R.ty";
connectAttr "Wheel4_R_parentConstraint1.ctz" "Wheel4_R.tz";
connectAttr "Wheel4_R_parentConstraint1.crx" "Wheel4_R.rx";
connectAttr "Wheel4_R_parentConstraint1.cry" "Wheel4_R.ry";
connectAttr "Wheel4_R_parentConstraint1.crz" "Wheel4_R.rz";
connectAttr "jointLayer.di" "Wheel4_R.do";
connectAttr "Wheel4_R.s" "Wheel4_End_R.is";
connectAttr "jointLayer.di" "Wheel4_End_R.do";
connectAttr "Wheel4_R.ro" "Wheel4_R_parentConstraint1.cro";
connectAttr "Wheel4_R.pim" "Wheel4_R_parentConstraint1.cpim";
connectAttr "Wheel4_R.rp" "Wheel4_R_parentConstraint1.crp";
connectAttr "Wheel4_R.rpt" "Wheel4_R_parentConstraint1.crt";
connectAttr "Wheel4_R.jo" "Wheel4_R_parentConstraint1.cjo";
connectAttr "FKXWheel4_R.t" "Wheel4_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel4_R.rp" "Wheel4_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel4_R.rpt" "Wheel4_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel4_R.r" "Wheel4_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel4_R.ro" "Wheel4_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel4_R.s" "Wheel4_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel4_R.pm" "Wheel4_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel4_R.jo" "Wheel4_R_parentConstraint1.tg[0].tjo";
connectAttr "Wheel4_R_parentConstraint1.w0" "Wheel4_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel5_R.s" "Wheel5_R.s";
connectAttr "TrackBottom_R.s" "Wheel5_R.is";
connectAttr "Wheel5_R_parentConstraint1.ctx" "Wheel5_R.tx";
connectAttr "Wheel5_R_parentConstraint1.cty" "Wheel5_R.ty";
connectAttr "Wheel5_R_parentConstraint1.ctz" "Wheel5_R.tz";
connectAttr "Wheel5_R_parentConstraint1.crx" "Wheel5_R.rx";
connectAttr "Wheel5_R_parentConstraint1.cry" "Wheel5_R.ry";
connectAttr "Wheel5_R_parentConstraint1.crz" "Wheel5_R.rz";
connectAttr "jointLayer.di" "Wheel5_R.do";
connectAttr "Wheel5_R.s" "Wheel5_End_R.is";
connectAttr "jointLayer.di" "Wheel5_End_R.do";
connectAttr "Wheel5_R.ro" "Wheel5_R_parentConstraint1.cro";
connectAttr "Wheel5_R.pim" "Wheel5_R_parentConstraint1.cpim";
connectAttr "Wheel5_R.rp" "Wheel5_R_parentConstraint1.crp";
connectAttr "Wheel5_R.rpt" "Wheel5_R_parentConstraint1.crt";
connectAttr "Wheel5_R.jo" "Wheel5_R_parentConstraint1.cjo";
connectAttr "FKXWheel5_R.t" "Wheel5_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel5_R.rp" "Wheel5_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel5_R.rpt" "Wheel5_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel5_R.r" "Wheel5_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel5_R.ro" "Wheel5_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel5_R.s" "Wheel5_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel5_R.pm" "Wheel5_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel5_R.jo" "Wheel5_R_parentConstraint1.tg[0].tjo";
connectAttr "Wheel5_R_parentConstraint1.w0" "Wheel5_R_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel6_R.s" "Wheel6_R.s";
connectAttr "TrackBottom_R.s" "Wheel6_R.is";
connectAttr "Wheel6_R_parentConstraint1.ctx" "Wheel6_R.tx";
connectAttr "Wheel6_R_parentConstraint1.cty" "Wheel6_R.ty";
connectAttr "Wheel6_R_parentConstraint1.ctz" "Wheel6_R.tz";
connectAttr "Wheel6_R_parentConstraint1.crx" "Wheel6_R.rx";
connectAttr "Wheel6_R_parentConstraint1.cry" "Wheel6_R.ry";
connectAttr "Wheel6_R_parentConstraint1.crz" "Wheel6_R.rz";
connectAttr "jointLayer.di" "Wheel6_R.do";
connectAttr "Wheel6_R.s" "Wheel6_End_R.is";
connectAttr "jointLayer.di" "Wheel6_End_R.do";
connectAttr "Wheel6_R.ro" "Wheel6_R_parentConstraint1.cro";
connectAttr "Wheel6_R.pim" "Wheel6_R_parentConstraint1.cpim";
connectAttr "Wheel6_R.rp" "Wheel6_R_parentConstraint1.crp";
connectAttr "Wheel6_R.rpt" "Wheel6_R_parentConstraint1.crt";
connectAttr "Wheel6_R.jo" "Wheel6_R_parentConstraint1.cjo";
connectAttr "FKXWheel6_R.t" "Wheel6_R_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel6_R.rp" "Wheel6_R_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel6_R.rpt" "Wheel6_R_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel6_R.r" "Wheel6_R_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel6_R.ro" "Wheel6_R_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel6_R.s" "Wheel6_R_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel6_R.pm" "Wheel6_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel6_R.jo" "Wheel6_R_parentConstraint1.tg[0].tjo";
connectAttr "Wheel6_R_parentConstraint1.w0" "Wheel6_R_parentConstraint1.tg[0].tw"
		;
connectAttr "TrackBottom_R.ro" "TrackBottom_R_parentConstraint1.cro";
connectAttr "TrackBottom_R.pim" "TrackBottom_R_parentConstraint1.cpim";
connectAttr "TrackBottom_R.rp" "TrackBottom_R_parentConstraint1.crp";
connectAttr "TrackBottom_R.rpt" "TrackBottom_R_parentConstraint1.crt";
connectAttr "TrackBottom_R.jo" "TrackBottom_R_parentConstraint1.cjo";
connectAttr "FKXTrackBottom_R.t" "TrackBottom_R_parentConstraint1.tg[0].tt";
connectAttr "FKXTrackBottom_R.rp" "TrackBottom_R_parentConstraint1.tg[0].trp";
connectAttr "FKXTrackBottom_R.rpt" "TrackBottom_R_parentConstraint1.tg[0].trt";
connectAttr "FKXTrackBottom_R.r" "TrackBottom_R_parentConstraint1.tg[0].tr";
connectAttr "FKXTrackBottom_R.ro" "TrackBottom_R_parentConstraint1.tg[0].tro";
connectAttr "FKXTrackBottom_R.s" "TrackBottom_R_parentConstraint1.tg[0].ts";
connectAttr "FKXTrackBottom_R.pm" "TrackBottom_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrackBottom_R.jo" "TrackBottom_R_parentConstraint1.tg[0].tjo";
connectAttr "TrackBottom_R_parentConstraint1.w0" "TrackBottom_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBottom_R.t" "TrackBottom_R_parentConstraint1.tg[1].tt";
connectAttr "IKXTrackBottom_R.rp" "TrackBottom_R_parentConstraint1.tg[1].trp";
connectAttr "IKXTrackBottom_R.rpt" "TrackBottom_R_parentConstraint1.tg[1].trt";
connectAttr "IKXTrackBottom_R.r" "TrackBottom_R_parentConstraint1.tg[1].tr";
connectAttr "IKXTrackBottom_R.ro" "TrackBottom_R_parentConstraint1.tg[1].tro";
connectAttr "IKXTrackBottom_R.s" "TrackBottom_R_parentConstraint1.tg[1].ts";
connectAttr "IKXTrackBottom_R.pm" "TrackBottom_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrackBottom_R.jo" "TrackBottom_R_parentConstraint1.tg[1].tjo";
connectAttr "TrackBottom_R_parentConstraint1.w1" "TrackBottom_R_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendLegReverse_R.ox" "TrackBottom_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "TrackBottom_R_parentConstraint1.w1"
		;
connectAttr "TrackBend_R.ro" "TrackBend_R_parentConstraint1.cro";
connectAttr "TrackBend_R.pim" "TrackBend_R_parentConstraint1.cpim";
connectAttr "TrackBend_R.rp" "TrackBend_R_parentConstraint1.crp";
connectAttr "TrackBend_R.rpt" "TrackBend_R_parentConstraint1.crt";
connectAttr "TrackBend_R.jo" "TrackBend_R_parentConstraint1.cjo";
connectAttr "FKXTrackBend_R.t" "TrackBend_R_parentConstraint1.tg[0].tt";
connectAttr "FKXTrackBend_R.rp" "TrackBend_R_parentConstraint1.tg[0].trp";
connectAttr "FKXTrackBend_R.rpt" "TrackBend_R_parentConstraint1.tg[0].trt";
connectAttr "FKXTrackBend_R.r" "TrackBend_R_parentConstraint1.tg[0].tr";
connectAttr "FKXTrackBend_R.ro" "TrackBend_R_parentConstraint1.tg[0].tro";
connectAttr "FKXTrackBend_R.s" "TrackBend_R_parentConstraint1.tg[0].ts";
connectAttr "FKXTrackBend_R.pm" "TrackBend_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrackBend_R.jo" "TrackBend_R_parentConstraint1.tg[0].tjo";
connectAttr "TrackBend_R_parentConstraint1.w0" "TrackBend_R_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBend_R.t" "TrackBend_R_parentConstraint1.tg[1].tt";
connectAttr "IKXTrackBend_R.rp" "TrackBend_R_parentConstraint1.tg[1].trp";
connectAttr "IKXTrackBend_R.rpt" "TrackBend_R_parentConstraint1.tg[1].trt";
connectAttr "IKXTrackBend_R.r" "TrackBend_R_parentConstraint1.tg[1].tr";
connectAttr "IKXTrackBend_R.ro" "TrackBend_R_parentConstraint1.tg[1].tro";
connectAttr "IKXTrackBend_R.s" "TrackBend_R_parentConstraint1.tg[1].ts";
connectAttr "IKXTrackBend_R.pm" "TrackBend_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrackBend_R.jo" "TrackBend_R_parentConstraint1.tg[1].tjo";
connectAttr "TrackBend_R_parentConstraint1.w1" "TrackBend_R_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendLegReverse_R.ox" "TrackBend_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "TrackBend_R_parentConstraint1.w1";
connectAttr "Track_R.ro" "Track_R_parentConstraint1.cro";
connectAttr "Track_R.pim" "Track_R_parentConstraint1.cpim";
connectAttr "Track_R.rp" "Track_R_parentConstraint1.crp";
connectAttr "Track_R.rpt" "Track_R_parentConstraint1.crt";
connectAttr "Track_R.jo" "Track_R_parentConstraint1.cjo";
connectAttr "FKXTrack_R.t" "Track_R_parentConstraint1.tg[0].tt";
connectAttr "FKXTrack_R.rp" "Track_R_parentConstraint1.tg[0].trp";
connectAttr "FKXTrack_R.rpt" "Track_R_parentConstraint1.tg[0].trt";
connectAttr "FKXTrack_R.r" "Track_R_parentConstraint1.tg[0].tr";
connectAttr "FKXTrack_R.ro" "Track_R_parentConstraint1.tg[0].tro";
connectAttr "FKXTrack_R.s" "Track_R_parentConstraint1.tg[0].ts";
connectAttr "FKXTrack_R.pm" "Track_R_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrack_R.jo" "Track_R_parentConstraint1.tg[0].tjo";
connectAttr "Track_R_parentConstraint1.w0" "Track_R_parentConstraint1.tg[0].tw";
connectAttr "IKXTrack_R.t" "Track_R_parentConstraint1.tg[1].tt";
connectAttr "IKXTrack_R.rp" "Track_R_parentConstraint1.tg[1].trp";
connectAttr "IKXTrack_R.rpt" "Track_R_parentConstraint1.tg[1].trt";
connectAttr "IKXTrack_R.r" "Track_R_parentConstraint1.tg[1].tr";
connectAttr "IKXTrack_R.ro" "Track_R_parentConstraint1.tg[1].tro";
connectAttr "IKXTrack_R.s" "Track_R_parentConstraint1.tg[1].ts";
connectAttr "IKXTrack_R.pm" "Track_R_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrack_R.jo" "Track_R_parentConstraint1.tg[1].tjo";
connectAttr "Track_R_parentConstraint1.w1" "Track_R_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_R.ox" "Track_R_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_R.o" "Track_R_parentConstraint1.w1";
connectAttr "ScaleBlendTrack_L.op" "Track_L.s";
connectAttr "Pelvis_M.s" "Track_L.is";
connectAttr "Track_L_parentConstraint1.ctx" "Track_L.tx";
connectAttr "Track_L_parentConstraint1.cty" "Track_L.ty";
connectAttr "Track_L_parentConstraint1.ctz" "Track_L.tz";
connectAttr "Track_L_parentConstraint1.crx" "Track_L.rx";
connectAttr "Track_L_parentConstraint1.cry" "Track_L.ry";
connectAttr "Track_L_parentConstraint1.crz" "Track_L.rz";
connectAttr "jointLayer.di" "Track_L.do";
connectAttr "ScaleBlendTrackBend_L.op" "TrackBend_L.s";
connectAttr "Track_L.s" "TrackBend_L.is";
connectAttr "TrackBend_L_parentConstraint1.ctx" "TrackBend_L.tx";
connectAttr "TrackBend_L_parentConstraint1.cty" "TrackBend_L.ty";
connectAttr "TrackBend_L_parentConstraint1.ctz" "TrackBend_L.tz";
connectAttr "TrackBend_L_parentConstraint1.crx" "TrackBend_L.rx";
connectAttr "TrackBend_L_parentConstraint1.cry" "TrackBend_L.ry";
connectAttr "TrackBend_L_parentConstraint1.crz" "TrackBend_L.rz";
connectAttr "jointLayer.di" "TrackBend_L.do";
connectAttr "ScaleBlendTrackBottom_L.op" "TrackBottom_L.s";
connectAttr "TrackBend_L.s" "TrackBottom_L.is";
connectAttr "TrackBottom_L_parentConstraint1.ctx" "TrackBottom_L.tx";
connectAttr "TrackBottom_L_parentConstraint1.cty" "TrackBottom_L.ty";
connectAttr "TrackBottom_L_parentConstraint1.ctz" "TrackBottom_L.tz";
connectAttr "TrackBottom_L_parentConstraint1.crx" "TrackBottom_L.rx";
connectAttr "TrackBottom_L_parentConstraint1.cry" "TrackBottom_L.ry";
connectAttr "TrackBottom_L_parentConstraint1.crz" "TrackBottom_L.rz";
connectAttr "jointLayer.di" "TrackBottom_L.do";
connectAttr "FKWheel1_L.s" "Wheel1_L.s";
connectAttr "TrackBottom_L.s" "Wheel1_L.is";
connectAttr "Wheel1_L_parentConstraint1.ctx" "Wheel1_L.tx";
connectAttr "Wheel1_L_parentConstraint1.cty" "Wheel1_L.ty";
connectAttr "Wheel1_L_parentConstraint1.ctz" "Wheel1_L.tz";
connectAttr "Wheel1_L_parentConstraint1.crx" "Wheel1_L.rx";
connectAttr "Wheel1_L_parentConstraint1.cry" "Wheel1_L.ry";
connectAttr "Wheel1_L_parentConstraint1.crz" "Wheel1_L.rz";
connectAttr "jointLayer.di" "Wheel1_L.do";
connectAttr "Wheel1_L.s" "Wheel1_End_L.is";
connectAttr "jointLayer.di" "Wheel1_End_L.do";
connectAttr "Wheel1_L.ro" "Wheel1_L_parentConstraint1.cro";
connectAttr "Wheel1_L.pim" "Wheel1_L_parentConstraint1.cpim";
connectAttr "Wheel1_L.rp" "Wheel1_L_parentConstraint1.crp";
connectAttr "Wheel1_L.rpt" "Wheel1_L_parentConstraint1.crt";
connectAttr "Wheel1_L.jo" "Wheel1_L_parentConstraint1.cjo";
connectAttr "FKXWheel1_L.t" "Wheel1_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel1_L.rp" "Wheel1_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel1_L.rpt" "Wheel1_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel1_L.r" "Wheel1_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel1_L.ro" "Wheel1_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel1_L.s" "Wheel1_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel1_L.pm" "Wheel1_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel1_L.jo" "Wheel1_L_parentConstraint1.tg[0].tjo";
connectAttr "Wheel1_L_parentConstraint1.w0" "Wheel1_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel2_L.s" "Wheel2_L.s";
connectAttr "TrackBottom_L.s" "Wheel2_L.is";
connectAttr "Wheel2_L_parentConstraint1.ctx" "Wheel2_L.tx";
connectAttr "Wheel2_L_parentConstraint1.cty" "Wheel2_L.ty";
connectAttr "Wheel2_L_parentConstraint1.ctz" "Wheel2_L.tz";
connectAttr "Wheel2_L_parentConstraint1.crx" "Wheel2_L.rx";
connectAttr "Wheel2_L_parentConstraint1.cry" "Wheel2_L.ry";
connectAttr "Wheel2_L_parentConstraint1.crz" "Wheel2_L.rz";
connectAttr "jointLayer.di" "Wheel2_L.do";
connectAttr "Wheel2_L.s" "Wheel2_End_L.is";
connectAttr "jointLayer.di" "Wheel2_End_L.do";
connectAttr "Wheel2_L.ro" "Wheel2_L_parentConstraint1.cro";
connectAttr "Wheel2_L.pim" "Wheel2_L_parentConstraint1.cpim";
connectAttr "Wheel2_L.rp" "Wheel2_L_parentConstraint1.crp";
connectAttr "Wheel2_L.rpt" "Wheel2_L_parentConstraint1.crt";
connectAttr "Wheel2_L.jo" "Wheel2_L_parentConstraint1.cjo";
connectAttr "FKXWheel2_L.t" "Wheel2_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel2_L.rp" "Wheel2_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel2_L.rpt" "Wheel2_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel2_L.r" "Wheel2_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel2_L.ro" "Wheel2_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel2_L.s" "Wheel2_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel2_L.pm" "Wheel2_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel2_L.jo" "Wheel2_L_parentConstraint1.tg[0].tjo";
connectAttr "Wheel2_L_parentConstraint1.w0" "Wheel2_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel3_L.s" "Wheel3_L.s";
connectAttr "TrackBottom_L.s" "Wheel3_L.is";
connectAttr "Wheel3_L_parentConstraint1.ctx" "Wheel3_L.tx";
connectAttr "Wheel3_L_parentConstraint1.cty" "Wheel3_L.ty";
connectAttr "Wheel3_L_parentConstraint1.ctz" "Wheel3_L.tz";
connectAttr "Wheel3_L_parentConstraint1.crx" "Wheel3_L.rx";
connectAttr "Wheel3_L_parentConstraint1.cry" "Wheel3_L.ry";
connectAttr "Wheel3_L_parentConstraint1.crz" "Wheel3_L.rz";
connectAttr "jointLayer.di" "Wheel3_L.do";
connectAttr "Wheel3_L.s" "Wheel3_End_L.is";
connectAttr "jointLayer.di" "Wheel3_End_L.do";
connectAttr "Wheel3_L.ro" "Wheel3_L_parentConstraint1.cro";
connectAttr "Wheel3_L.pim" "Wheel3_L_parentConstraint1.cpim";
connectAttr "Wheel3_L.rp" "Wheel3_L_parentConstraint1.crp";
connectAttr "Wheel3_L.rpt" "Wheel3_L_parentConstraint1.crt";
connectAttr "Wheel3_L.jo" "Wheel3_L_parentConstraint1.cjo";
connectAttr "FKXWheel3_L.t" "Wheel3_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel3_L.rp" "Wheel3_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel3_L.rpt" "Wheel3_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel3_L.r" "Wheel3_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel3_L.ro" "Wheel3_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel3_L.s" "Wheel3_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel3_L.pm" "Wheel3_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel3_L.jo" "Wheel3_L_parentConstraint1.tg[0].tjo";
connectAttr "Wheel3_L_parentConstraint1.w0" "Wheel3_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel4_L.s" "Wheel4_L.s";
connectAttr "TrackBottom_L.s" "Wheel4_L.is";
connectAttr "Wheel4_L_parentConstraint1.ctx" "Wheel4_L.tx";
connectAttr "Wheel4_L_parentConstraint1.cty" "Wheel4_L.ty";
connectAttr "Wheel4_L_parentConstraint1.ctz" "Wheel4_L.tz";
connectAttr "Wheel4_L_parentConstraint1.crx" "Wheel4_L.rx";
connectAttr "Wheel4_L_parentConstraint1.cry" "Wheel4_L.ry";
connectAttr "Wheel4_L_parentConstraint1.crz" "Wheel4_L.rz";
connectAttr "jointLayer.di" "Wheel4_L.do";
connectAttr "Wheel4_L.s" "Wheel4_End_L.is";
connectAttr "jointLayer.di" "Wheel4_End_L.do";
connectAttr "Wheel4_L.ro" "Wheel4_L_parentConstraint1.cro";
connectAttr "Wheel4_L.pim" "Wheel4_L_parentConstraint1.cpim";
connectAttr "Wheel4_L.rp" "Wheel4_L_parentConstraint1.crp";
connectAttr "Wheel4_L.rpt" "Wheel4_L_parentConstraint1.crt";
connectAttr "Wheel4_L.jo" "Wheel4_L_parentConstraint1.cjo";
connectAttr "FKXWheel4_L.t" "Wheel4_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel4_L.rp" "Wheel4_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel4_L.rpt" "Wheel4_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel4_L.r" "Wheel4_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel4_L.ro" "Wheel4_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel4_L.s" "Wheel4_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel4_L.pm" "Wheel4_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel4_L.jo" "Wheel4_L_parentConstraint1.tg[0].tjo";
connectAttr "Wheel4_L_parentConstraint1.w0" "Wheel4_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel5_L.s" "Wheel5_L.s";
connectAttr "TrackBottom_L.s" "Wheel5_L.is";
connectAttr "Wheel5_L_parentConstraint1.ctx" "Wheel5_L.tx";
connectAttr "Wheel5_L_parentConstraint1.cty" "Wheel5_L.ty";
connectAttr "Wheel5_L_parentConstraint1.ctz" "Wheel5_L.tz";
connectAttr "Wheel5_L_parentConstraint1.crx" "Wheel5_L.rx";
connectAttr "Wheel5_L_parentConstraint1.cry" "Wheel5_L.ry";
connectAttr "Wheel5_L_parentConstraint1.crz" "Wheel5_L.rz";
connectAttr "jointLayer.di" "Wheel5_L.do";
connectAttr "Wheel5_L.s" "Wheel5_End_L.is";
connectAttr "jointLayer.di" "Wheel5_End_L.do";
connectAttr "Wheel5_L.ro" "Wheel5_L_parentConstraint1.cro";
connectAttr "Wheel5_L.pim" "Wheel5_L_parentConstraint1.cpim";
connectAttr "Wheel5_L.rp" "Wheel5_L_parentConstraint1.crp";
connectAttr "Wheel5_L.rpt" "Wheel5_L_parentConstraint1.crt";
connectAttr "Wheel5_L.jo" "Wheel5_L_parentConstraint1.cjo";
connectAttr "FKXWheel5_L.t" "Wheel5_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel5_L.rp" "Wheel5_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel5_L.rpt" "Wheel5_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel5_L.r" "Wheel5_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel5_L.ro" "Wheel5_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel5_L.s" "Wheel5_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel5_L.pm" "Wheel5_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel5_L.jo" "Wheel5_L_parentConstraint1.tg[0].tjo";
connectAttr "Wheel5_L_parentConstraint1.w0" "Wheel5_L_parentConstraint1.tg[0].tw"
		;
connectAttr "FKWheel6_L.s" "Wheel6_L.s";
connectAttr "TrackBottom_L.s" "Wheel6_L.is";
connectAttr "Wheel6_L_parentConstraint1.ctx" "Wheel6_L.tx";
connectAttr "Wheel6_L_parentConstraint1.cty" "Wheel6_L.ty";
connectAttr "Wheel6_L_parentConstraint1.ctz" "Wheel6_L.tz";
connectAttr "Wheel6_L_parentConstraint1.crx" "Wheel6_L.rx";
connectAttr "Wheel6_L_parentConstraint1.cry" "Wheel6_L.ry";
connectAttr "Wheel6_L_parentConstraint1.crz" "Wheel6_L.rz";
connectAttr "jointLayer.di" "Wheel6_L.do";
connectAttr "Wheel6_L.s" "Wheel6_End_L.is";
connectAttr "jointLayer.di" "Wheel6_End_L.do";
connectAttr "Wheel6_L.ro" "Wheel6_L_parentConstraint1.cro";
connectAttr "Wheel6_L.pim" "Wheel6_L_parentConstraint1.cpim";
connectAttr "Wheel6_L.rp" "Wheel6_L_parentConstraint1.crp";
connectAttr "Wheel6_L.rpt" "Wheel6_L_parentConstraint1.crt";
connectAttr "Wheel6_L.jo" "Wheel6_L_parentConstraint1.cjo";
connectAttr "FKXWheel6_L.t" "Wheel6_L_parentConstraint1.tg[0].tt";
connectAttr "FKXWheel6_L.rp" "Wheel6_L_parentConstraint1.tg[0].trp";
connectAttr "FKXWheel6_L.rpt" "Wheel6_L_parentConstraint1.tg[0].trt";
connectAttr "FKXWheel6_L.r" "Wheel6_L_parentConstraint1.tg[0].tr";
connectAttr "FKXWheel6_L.ro" "Wheel6_L_parentConstraint1.tg[0].tro";
connectAttr "FKXWheel6_L.s" "Wheel6_L_parentConstraint1.tg[0].ts";
connectAttr "FKXWheel6_L.pm" "Wheel6_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXWheel6_L.jo" "Wheel6_L_parentConstraint1.tg[0].tjo";
connectAttr "Wheel6_L_parentConstraint1.w0" "Wheel6_L_parentConstraint1.tg[0].tw"
		;
connectAttr "TrackBottom_L.ro" "TrackBottom_L_parentConstraint1.cro";
connectAttr "TrackBottom_L.pim" "TrackBottom_L_parentConstraint1.cpim";
connectAttr "TrackBottom_L.rp" "TrackBottom_L_parentConstraint1.crp";
connectAttr "TrackBottom_L.rpt" "TrackBottom_L_parentConstraint1.crt";
connectAttr "TrackBottom_L.jo" "TrackBottom_L_parentConstraint1.cjo";
connectAttr "FKXTrackBottom_L.t" "TrackBottom_L_parentConstraint1.tg[0].tt";
connectAttr "FKXTrackBottom_L.rp" "TrackBottom_L_parentConstraint1.tg[0].trp";
connectAttr "FKXTrackBottom_L.rpt" "TrackBottom_L_parentConstraint1.tg[0].trt";
connectAttr "FKXTrackBottom_L.r" "TrackBottom_L_parentConstraint1.tg[0].tr";
connectAttr "FKXTrackBottom_L.ro" "TrackBottom_L_parentConstraint1.tg[0].tro";
connectAttr "FKXTrackBottom_L.s" "TrackBottom_L_parentConstraint1.tg[0].ts";
connectAttr "FKXTrackBottom_L.pm" "TrackBottom_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrackBottom_L.jo" "TrackBottom_L_parentConstraint1.tg[0].tjo";
connectAttr "TrackBottom_L_parentConstraint1.w0" "TrackBottom_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBottom_L.t" "TrackBottom_L_parentConstraint1.tg[1].tt";
connectAttr "IKXTrackBottom_L.rp" "TrackBottom_L_parentConstraint1.tg[1].trp";
connectAttr "IKXTrackBottom_L.rpt" "TrackBottom_L_parentConstraint1.tg[1].trt";
connectAttr "IKXTrackBottom_L.r" "TrackBottom_L_parentConstraint1.tg[1].tr";
connectAttr "IKXTrackBottom_L.ro" "TrackBottom_L_parentConstraint1.tg[1].tro";
connectAttr "IKXTrackBottom_L.s" "TrackBottom_L_parentConstraint1.tg[1].ts";
connectAttr "IKXTrackBottom_L.pm" "TrackBottom_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrackBottom_L.jo" "TrackBottom_L_parentConstraint1.tg[1].tjo";
connectAttr "TrackBottom_L_parentConstraint1.w1" "TrackBottom_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendLegReverse_L.ox" "TrackBottom_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "TrackBottom_L_parentConstraint1.w1"
		;
connectAttr "TrackBend_L.ro" "TrackBend_L_parentConstraint1.cro";
connectAttr "TrackBend_L.pim" "TrackBend_L_parentConstraint1.cpim";
connectAttr "TrackBend_L.rp" "TrackBend_L_parentConstraint1.crp";
connectAttr "TrackBend_L.rpt" "TrackBend_L_parentConstraint1.crt";
connectAttr "TrackBend_L.jo" "TrackBend_L_parentConstraint1.cjo";
connectAttr "FKXTrackBend_L.t" "TrackBend_L_parentConstraint1.tg[0].tt";
connectAttr "FKXTrackBend_L.rp" "TrackBend_L_parentConstraint1.tg[0].trp";
connectAttr "FKXTrackBend_L.rpt" "TrackBend_L_parentConstraint1.tg[0].trt";
connectAttr "FKXTrackBend_L.r" "TrackBend_L_parentConstraint1.tg[0].tr";
connectAttr "FKXTrackBend_L.ro" "TrackBend_L_parentConstraint1.tg[0].tro";
connectAttr "FKXTrackBend_L.s" "TrackBend_L_parentConstraint1.tg[0].ts";
connectAttr "FKXTrackBend_L.pm" "TrackBend_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrackBend_L.jo" "TrackBend_L_parentConstraint1.tg[0].tjo";
connectAttr "TrackBend_L_parentConstraint1.w0" "TrackBend_L_parentConstraint1.tg[0].tw"
		;
connectAttr "IKXTrackBend_L.t" "TrackBend_L_parentConstraint1.tg[1].tt";
connectAttr "IKXTrackBend_L.rp" "TrackBend_L_parentConstraint1.tg[1].trp";
connectAttr "IKXTrackBend_L.rpt" "TrackBend_L_parentConstraint1.tg[1].trt";
connectAttr "IKXTrackBend_L.r" "TrackBend_L_parentConstraint1.tg[1].tr";
connectAttr "IKXTrackBend_L.ro" "TrackBend_L_parentConstraint1.tg[1].tro";
connectAttr "IKXTrackBend_L.s" "TrackBend_L_parentConstraint1.tg[1].ts";
connectAttr "IKXTrackBend_L.pm" "TrackBend_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrackBend_L.jo" "TrackBend_L_parentConstraint1.tg[1].tjo";
connectAttr "TrackBend_L_parentConstraint1.w1" "TrackBend_L_parentConstraint1.tg[1].tw"
		;
connectAttr "FKIKBlendLegReverse_L.ox" "TrackBend_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "TrackBend_L_parentConstraint1.w1";
connectAttr "Track_L.ro" "Track_L_parentConstraint1.cro";
connectAttr "Track_L.pim" "Track_L_parentConstraint1.cpim";
connectAttr "Track_L.rp" "Track_L_parentConstraint1.crp";
connectAttr "Track_L.rpt" "Track_L_parentConstraint1.crt";
connectAttr "Track_L.jo" "Track_L_parentConstraint1.cjo";
connectAttr "FKXTrack_L.t" "Track_L_parentConstraint1.tg[0].tt";
connectAttr "FKXTrack_L.rp" "Track_L_parentConstraint1.tg[0].trp";
connectAttr "FKXTrack_L.rpt" "Track_L_parentConstraint1.tg[0].trt";
connectAttr "FKXTrack_L.r" "Track_L_parentConstraint1.tg[0].tr";
connectAttr "FKXTrack_L.ro" "Track_L_parentConstraint1.tg[0].tro";
connectAttr "FKXTrack_L.s" "Track_L_parentConstraint1.tg[0].ts";
connectAttr "FKXTrack_L.pm" "Track_L_parentConstraint1.tg[0].tpm";
connectAttr "FKXTrack_L.jo" "Track_L_parentConstraint1.tg[0].tjo";
connectAttr "Track_L_parentConstraint1.w0" "Track_L_parentConstraint1.tg[0].tw";
connectAttr "IKXTrack_L.t" "Track_L_parentConstraint1.tg[1].tt";
connectAttr "IKXTrack_L.rp" "Track_L_parentConstraint1.tg[1].trp";
connectAttr "IKXTrack_L.rpt" "Track_L_parentConstraint1.tg[1].trt";
connectAttr "IKXTrack_L.r" "Track_L_parentConstraint1.tg[1].tr";
connectAttr "IKXTrack_L.ro" "Track_L_parentConstraint1.tg[1].tro";
connectAttr "IKXTrack_L.s" "Track_L_parentConstraint1.tg[1].ts";
connectAttr "IKXTrack_L.pm" "Track_L_parentConstraint1.tg[1].tpm";
connectAttr "IKXTrack_L.jo" "Track_L_parentConstraint1.tg[1].tjo";
connectAttr "Track_L_parentConstraint1.w1" "Track_L_parentConstraint1.tg[1].tw";
connectAttr "FKIKBlendLegReverse_L.ox" "Track_L_parentConstraint1.w0";
connectAttr "FKIKBlendLegUnitConversion_L.o" "Track_L_parentConstraint1.w1";
connectAttr "Pelvis_M.pim" "Pelvis_M_pointConstraint1.cpim";
connectAttr "Pelvis_M.rp" "Pelvis_M_pointConstraint1.crp";
connectAttr "Pelvis_M.rpt" "Pelvis_M_pointConstraint1.crt";
connectAttr "FKXPelvis_M.t" "Pelvis_M_pointConstraint1.tg[0].tt";
connectAttr "FKXPelvis_M.rp" "Pelvis_M_pointConstraint1.tg[0].trp";
connectAttr "FKXPelvis_M.rpt" "Pelvis_M_pointConstraint1.tg[0].trt";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_pointConstraint1.tg[0].tpm";
connectAttr "Pelvis_M_pointConstraint1.w0" "Pelvis_M_pointConstraint1.tg[0].tw";
connectAttr "Pelvis_M.ro" "Pelvis_M_orientConstraint1.cro";
connectAttr "Pelvis_M.pim" "Pelvis_M_orientConstraint1.cpim";
connectAttr "Pelvis_M.jo" "Pelvis_M_orientConstraint1.cjo";
connectAttr "FKXPelvis_M.r" "Pelvis_M_orientConstraint1.tg[0].tr";
connectAttr "FKXPelvis_M.ro" "Pelvis_M_orientConstraint1.tg[0].tro";
connectAttr "FKXPelvis_M.pm" "Pelvis_M_orientConstraint1.tg[0].tpm";
connectAttr "FKXPelvis_M.jo" "Pelvis_M_orientConstraint1.tg[0].tjo";
connectAttr "Pelvis_M_orientConstraint1.w0" "Pelvis_M_orientConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "irisPuppetSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "BPRig_irisPuppetSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "irisPuppetSG.msg" "materialInfo3.sg";
connectAttr "BPRig_irisPuppetSG.msg" "BPRig_materialInfo3.sg";
connectAttr "jointVisReverse.ox" "jointLayer.lod";
connectAttr "Main.jointVis" "jointVisReverse.ix";
connectAttr "Main.iog" "ControlSet.dsm" -na;
connectAttr "FKWeapon_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWeapon_M.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow_R.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder_R.iog" "ControlSet.dsm" -na;
connectAttr "FKBody_M.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraBody_M.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel1_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel2_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel3_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel3_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel4_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel4_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel5_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel5_R.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel6_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel6_R.iog" "ControlSet.dsm" -na;
connectAttr "FKTrackBottom_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrackBottom_R.iog" "ControlSet.dsm" -na;
connectAttr "FKTrackBend_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrackBend_R.iog" "ControlSet.dsm" -na;
connectAttr "FKTrack_R.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrack_R.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKMiddleFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIndexFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKThumbFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "ControlSet.dsm" -na;
connectAttr "FKShoulder_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraShoulder_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel1_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel2_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel3_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel4_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel4_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel5_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel5_L.iog" "ControlSet.dsm" -na;
connectAttr "FKWheel6_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraWheel6_L.iog" "ControlSet.dsm" -na;
connectAttr "FKTrackBottom_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrackBottom_L.iog" "ControlSet.dsm" -na;
connectAttr "FKTrackBend_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrackBend_L.iog" "ControlSet.dsm" -na;
connectAttr "FKTrack_L.iog" "ControlSet.dsm" -na;
connectAttr "FKExtraTrack_L.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "ControlSet.dsm" -na;
connectAttr "IKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "ControlSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "ControlSet.dsm" -na;
connectAttr "Center_M.iog" "ControlSet.dsm" -na;
connectAttr "Weapon_M.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger2_R.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger1_R.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger2_R.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger1_R.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger2_R.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger1_R.iog" "GameSet.dsm" -na;
connectAttr "Elbow_R.iog" "GameSet.dsm" -na;
connectAttr "Shoulder_R.iog" "GameSet.dsm" -na;
connectAttr "Body_M.iog" "GameSet.dsm" -na;
connectAttr "Wheel1_R.iog" "GameSet.dsm" -na;
connectAttr "Wheel2_R.iog" "GameSet.dsm" -na;
connectAttr "Wheel3_R.iog" "GameSet.dsm" -na;
connectAttr "Wheel4_R.iog" "GameSet.dsm" -na;
connectAttr "Wheel5_R.iog" "GameSet.dsm" -na;
connectAttr "Wheel6_R.iog" "GameSet.dsm" -na;
connectAttr "TrackBottom_R.iog" "GameSet.dsm" -na;
connectAttr "TrackBend_R.iog" "GameSet.dsm" -na;
connectAttr "Track_R.iog" "GameSet.dsm" -na;
connectAttr "Pelvis_M.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "MiddleFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "IndexFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger2_L.iog" "GameSet.dsm" -na;
connectAttr "ThumbFinger1_L.iog" "GameSet.dsm" -na;
connectAttr "Elbow_L.iog" "GameSet.dsm" -na;
connectAttr "Shoulder_L.iog" "GameSet.dsm" -na;
connectAttr "Wheel1_L.iog" "GameSet.dsm" -na;
connectAttr "Wheel2_L.iog" "GameSet.dsm" -na;
connectAttr "Wheel3_L.iog" "GameSet.dsm" -na;
connectAttr "Wheel4_L.iog" "GameSet.dsm" -na;
connectAttr "Wheel5_L.iog" "GameSet.dsm" -na;
connectAttr "Wheel6_L.iog" "GameSet.dsm" -na;
connectAttr "TrackBottom_L.iog" "GameSet.dsm" -na;
connectAttr "TrackBend_L.iog" "GameSet.dsm" -na;
connectAttr "Track_L.iog" "GameSet.dsm" -na;
connectAttr "buildPose.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetReverse_M.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwFeetUnitConversion_M.msg" "AllSet.dnsm" -na;
connectAttr "CenterBtwFeet_M.msg" "AllSet.dnsm" -na;
connectAttr "GlobalShoulder_reverse_L.msg" "AllSet.dnsm" -na;
connectAttr "GlobalShoulder_unitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAddYTrackBend_L.msg" "AllSet.dnsm" -na;
connectAttr "IKXTrackBottom_L_IKLength_L.msg" "AllSet.dnsm" -na;
connectAttr "IKXTrackBottom_L_IKmessureDiv_L.msg" "AllSet.dnsm" -na;
connectAttr "IKXTrackBend_L_IKLength_L.msg" "AllSet.dnsm" -na;
connectAttr "IKXTrackBend_L_IKmessureDiv_L.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureBlendStretchLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceClampLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceLeg_LShape_normal.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceLeg_LShape_antiPop.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureBlendAntiPopLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureDivLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "IKSetRangeAntiPopLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "IKSetRangeStretchLeg_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrack_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrackBend_L.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrackBottom_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_L.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_L.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_LSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion2.msg" "AllSet.dnsm" -na;
connectAttr "GlobalShoulder_reverse_R.msg" "AllSet.dnsm" -na;
connectAttr "GlobalShoulder_unitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendAddYTrackBend_R.msg" "AllSet.dnsm" -na;
connectAttr "IKXTrackBottom_R_IKLength_R.msg" "AllSet.dnsm" -na;
connectAttr "IKXTrackBottom_R_IKmessureDiv_R.msg" "AllSet.dnsm" -na;
connectAttr "IKXTrackBend_R_IKLength_R.msg" "AllSet.dnsm" -na;
connectAttr "IKXTrackBend_R_IKmessureDiv_R.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureBlendStretchLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceClampLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceLeg_RShape_normal.msg" "AllSet.dnsm" -na;
connectAttr "IKdistanceLeg_RShape_antiPop.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureBlendAntiPopLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "IKmessureDivLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "IKSetRangeAntiPopLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "IKSetRangeStretchLeg_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrack_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrackBend_R.msg" "AllSet.dnsm" -na;
connectAttr "ScaleBlendTrackBottom_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegsetRange_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegCondition_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegReverse_R.msg" "AllSet.dnsm" -na;
connectAttr "FKIKBlendLegUnitConversion_R.msg" "AllSet.dnsm" -na;
connectAttr "PoleLeg_RSetRangeFollow.msg" "AllSet.dnsm" -na;
connectAttr "unitConversion1.msg" "AllSet.dnsm" -na;
connectAttr "GameSet.msg" "AllSet.dnsm" -na;
connectAttr "ControlSet.msg" "AllSet.dnsm" -na;
connectAttr "jointVisReverse.msg" "AllSet.dnsm" -na;
connectAttr "jointLayer.msg" "AllSet.dnsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M_parentConstraint1.iog" "AllSet.dsm" -na
		;
connectAttr "PelvisCenterBtwLegs_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsBlended_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegsOffset_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenterBtwLegs_M.iog" "AllSet.dsm" -na;
connectAttr "PelvisCenter_M.iog" "AllSet.dsm" -na;
connectAttr "Center_MShape.iog" "AllSet.dsm" -na;
connectAttr "Center_M.iog" "AllSet.dsm" -na;
connectAttr "CenterExtra_M.iog" "AllSet.dsm" -na;
connectAttr "CenterOffset_M.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalShoulder_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "GlobalShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "GlobalOffsetShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "IKdistanceLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKdistanceLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKmessureConstrainToLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_L.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_L.iog" "AllSet.dsm" -na;
connectAttr "Track_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TrackBend_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TrackBottom_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel6_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel5_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToTrackBottom_L_parentConstraint1.iog" "AllSet.dsm"
		 -na;
connectAttr "Wheel1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger1_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_LStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBottom_L_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_LShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_L.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_L.iog" "AllSet.dsm" -na;
connectAttr "effector2.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalShoulder_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "GlobalShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "GlobalOffsetShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "IKdistanceLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKdistanceLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKmessureConstrainToLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc2Leg_R.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKmessureLoc1Leg_R.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Track_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TrackBend_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "TrackBottom_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel6_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel5_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToTrackBottom_R_parentConstraint1.iog" "AllSet.dsm"
		 -na;
connectAttr "Wheel1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Body_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Elbow_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger1_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "Weapon_M_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKIKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_RStatic.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotationLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleAnnotateTargetLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R_poleVectorConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "PoleLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_pointConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R_aimConstraint1.iog" "AllSet.dsm" -na;
connectAttr "PoleAimLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBottom_R_orientConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKFKAlignedLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_RShape.iog" "AllSet.dsm" -na;
connectAttr "IKLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKExtraLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintLeg_R.iog" "AllSet.dsm" -na;
connectAttr "IKXLegHandle_R.iog" "AllSet.dsm" -na;
connectAttr "effector1.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintTrack_L_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "IKXTrack_L.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetTrack_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintTrack_L.iog" "AllSet.dsm" -na;
connectAttr "FKXTrack_L.iog" "AllSet.dsm" -na;
connectAttr "FKTrack_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrack_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrack_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrack_L.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "FKXTrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBend_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKXTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBottom_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel6_L.iog" "AllSet.dsm" -na;
connectAttr "FKWheel6_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel6_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel6_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel6_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel6_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel5_L.iog" "AllSet.dsm" -na;
connectAttr "FKWheel5_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel5_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel5_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel5_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel5_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel4_L.iog" "AllSet.dsm" -na;
connectAttr "FKWheel4_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel4_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel4_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel4_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel4_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel3_L.iog" "AllSet.dsm" -na;
connectAttr "FKWheel3_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel3_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel3_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel3_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel3_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel2_L.iog" "AllSet.dsm" -na;
connectAttr "FKWheel2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel2_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel1_L.iog" "AllSet.dsm" -na;
connectAttr "FKWheel1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel1_L.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToTrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel1_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalStaticShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder_L.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_LShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "Track_L.iog" "AllSet.dsm" -na;
connectAttr "TrackBend_L.iog" "AllSet.dsm" -na;
connectAttr "TrackBottom_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel6_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel6_End_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel5_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel5_End_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_End_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_End_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_End_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel1_L.iog" "AllSet.dsm" -na;
connectAttr "Wheel1_End_L.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_L.iog" "AllSet.dsm" -na;
connectAttr "Elbow_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger1_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_L.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger3_End_L.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintTrack_R_parentConstraint1.iog" "AllSet.dsm" -na;
connectAttr "FKXPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetPelvis_M.iog" "AllSet.dsm" -na;
connectAttr "IKXTrack_R.iog" "AllSet.dsm" -na;
connectAttr "IKOffsetTrack_R.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraintTrack_R.iog" "AllSet.dsm" -na;
connectAttr "FKXTrack_R.iog" "AllSet.dsm" -na;
connectAttr "FKTrack_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrack_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrack_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrack_R.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "FKXTrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBend_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "IKXTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "AlignIKToTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKXTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBottom_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel6_R.iog" "AllSet.dsm" -na;
connectAttr "FKWheel6_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel6_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel6_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel6_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel6_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel5_R.iog" "AllSet.dsm" -na;
connectAttr "FKWheel5_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel5_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel5_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel5_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel5_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel4_R.iog" "AllSet.dsm" -na;
connectAttr "FKWheel4_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel4_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel4_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel4_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel4_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKWheel3_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel3_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel3_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKWheel2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel2_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKWheel1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKWheel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWheel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWheel1_R.iog" "AllSet.dsm" -na;
connectAttr "FKParentConstraintToTrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWheel1_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXBody_M.iog" "AllSet.dsm" -na;
connectAttr "FKBody_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKBody_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraBody_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetBody_M.iog" "AllSet.dsm" -na;
connectAttr "FKXShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKGlobalStaticShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetShoulder_R.iog" "AllSet.dsm" -na;
connectAttr "FKXElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetElbow_R.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKThumbFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraThumbFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetThumbFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXThumbFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKIndexFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraIndexFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetIndexFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXIndexFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_RShape.iog" "AllSet.dsm" -na;
connectAttr "FKMiddleFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKExtraMiddleFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetMiddleFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "FKXMiddleFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "FKXWeapon_M.iog" "AllSet.dsm" -na;
connectAttr "FKWeapon_MShape.iog" "AllSet.dsm" -na;
connectAttr "FKWeapon_M.iog" "AllSet.dsm" -na;
connectAttr "FKExtraWeapon_M.iog" "AllSet.dsm" -na;
connectAttr "FKOffsetWeapon_M.iog" "AllSet.dsm" -na;
connectAttr "FKXWeapon_End_M.iog" "AllSet.dsm" -na;
connectAttr "Pelvis_M.iog" "AllSet.dsm" -na;
connectAttr "Track_R.iog" "AllSet.dsm" -na;
connectAttr "TrackBend_R.iog" "AllSet.dsm" -na;
connectAttr "TrackBottom_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel6_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel6_End_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel5_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel5_End_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel4_End_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel3_End_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel2_End_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel1_R.iog" "AllSet.dsm" -na;
connectAttr "Wheel1_End_R.iog" "AllSet.dsm" -na;
connectAttr "Body_M.iog" "AllSet.dsm" -na;
connectAttr "Shoulder_R.iog" "AllSet.dsm" -na;
connectAttr "Elbow_R.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "ThumbFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "IndexFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger1_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger2_R.iog" "AllSet.dsm" -na;
connectAttr "MiddleFinger3_End_R.iog" "AllSet.dsm" -na;
connectAttr "Weapon_M.iog" "AllSet.dsm" -na;
connectAttr "Weapon_End_M.iog" "AllSet.dsm" -na;
connectAttr "IKMessure.iog" "AllSet.dsm" -na;
connectAttr "IKCrv.iog" "AllSet.dsm" -na;
connectAttr "IKStatic.iog" "AllSet.dsm" -na;
connectAttr "IKHandle.iog" "AllSet.dsm" -na;
connectAttr "IKParentConstraint.iog" "AllSet.dsm" -na;
connectAttr "GlobalSystem.iog" "AllSet.dsm" -na;
connectAttr "RootSystem.iog" "AllSet.dsm" -na;
connectAttr "FKIKSystem.iog" "AllSet.dsm" -na;
connectAttr "IKSystem.iog" "AllSet.dsm" -na;
connectAttr "FKSystem.iog" "AllSet.dsm" -na;
connectAttr "GameSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MotionSystem.iog" "AllSet.dsm" -na;
connectAttr "BaseSkeleton.iog" "AllSet.dsm" -na;
connectAttr "MainShape.iog" "AllSet.dsm" -na;
connectAttr "Main.iog" "AllSet.dsm" -na;
connectAttr "forwardArrowShape.iog" "AllSet.dsm" -na;
connectAttr "GameSet.msg" "Sets.dnsm" -na;
connectAttr "ControlSet.msg" "Sets.dnsm" -na;
connectAttr "AllSet.msg" "Sets.dnsm" -na;
connectAttr "IKLeg_R.swivel" "unitConversion1.i";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vx";
connectAttr "PoleLeg_R.follow" "PoleLeg_RSetRangeFollow.vy";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegUnitConversion_R.i";
connectAttr "FKIKBlendLegUnitConversion_R.o" "FKIKBlendLegReverse_R.ix";
connectAttr "FKIKLeg_R.autoVis" "FKIKBlendLegCondition_R.ft";
connectAttr "FKIKLeg_R.IKVis" "FKIKBlendLegCondition_R.ctr";
connectAttr "FKIKLeg_R.FKVis" "FKIKBlendLegCondition_R.ctg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegCondition_R.cfr";
connectAttr "FKIKBlendLegsetRange_R.ox" "FKIKBlendLegCondition_R.cfg";
connectAttr "FKIKLeg_R.FKIKBlend" "FKIKBlendLegsetRange_R.vx";
connectAttr "FKTrackBottom_R.s" "ScaleBlendTrackBottom_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendTrackBottom_R.b";
connectAttr "FKTrackBend_R.s" "ScaleBlendTrackBend_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendTrackBend_R.b";
connectAttr "ScaleBlendAddYTrackBend_R.o1" "ScaleBlendTrackBend_R.c1g";
connectAttr "FKTrack_R.s" "ScaleBlendTrack_R.c2";
connectAttr "FKIKBlendLegUnitConversion_R.o" "ScaleBlendTrack_R.b";
connectAttr "IKLeg_R.stretchy" "IKSetRangeStretchLeg_R.vy";
connectAttr "IKLeg_R.antiPop" "IKSetRangeAntiPopLeg_R.vy";
connectAttr "IKmessureBlendStretchLeg_R.o" "IKmessureDivLeg_R.i1y";
connectAttr "IKSetRangeAntiPopLeg_R.oy" "IKmessureBlendAntiPopLeg_R.ab";
connectAttr "IKdistanceLeg_RShape_normal.o" "IKmessureBlendAntiPopLeg_R.i[0]";
connectAttr "IKdistanceLeg_RShape_antiPop.o" "IKmessureBlendAntiPopLeg_R.i[1]";
connectAttr "IKdistanceLeg_RShape.dist" "IKdistanceLeg_RShape_antiPop.i";
connectAttr "IKdistanceLeg_RShape.dist" "IKdistanceLeg_RShape_normal.i";
connectAttr "IKmessureBlendAntiPopLeg_R.o" "IKdistanceClampLeg_R.ipr";
connectAttr "IKSetRangeStretchLeg_R.oy" "IKmessureBlendStretchLeg_R.ab";
connectAttr "IKdistanceClampLeg_R.opr" "IKmessureBlendStretchLeg_R.i[0]";
connectAttr "IKmessureBlendAntiPopLeg_R.o" "IKmessureBlendStretchLeg_R.i[1]";
connectAttr "IKmessureDivLeg_R.oy" "IKXTrackBend_R_IKmessureDiv_R.i1y";
connectAttr "IKXTrackBend_R_IKLength_R.oy" "IKXTrackBend_R_IKmessureDiv_R.i2y";
connectAttr "IKLeg_R.Length1" "IKXTrackBend_R_IKLength_R.i1y";
connectAttr "IKmessureDivLeg_R.oy" "IKXTrackBottom_R_IKmessureDiv_R.i1y";
connectAttr "IKXTrackBottom_R_IKLength_R.oy" "IKXTrackBottom_R_IKmessureDiv_R.i2y"
		;
connectAttr "IKLeg_R.Length2" "IKXTrackBottom_R_IKLength_R.i1y";
connectAttr "IKLeg_R.Length2" "ScaleBlendAddYTrackBend_R.i1[0]";
connectAttr "IKmessureDivLeg_R.oy" "ScaleBlendAddYTrackBend_R.i1[1]";
connectAttr "FKShoulder_R.Global" "GlobalShoulder_unitConversion_R.i";
connectAttr "GlobalShoulder_unitConversion_R.o" "GlobalShoulder_reverse_R.ix";
connectAttr "IKLeg_L.swivel" "unitConversion2.i";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vx";
connectAttr "PoleLeg_L.follow" "PoleLeg_LSetRangeFollow.vy";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegUnitConversion_L.i";
connectAttr "FKIKBlendLegUnitConversion_L.o" "FKIKBlendLegReverse_L.ix";
connectAttr "FKIKLeg_L.autoVis" "FKIKBlendLegCondition_L.ft";
connectAttr "FKIKLeg_L.IKVis" "FKIKBlendLegCondition_L.ctr";
connectAttr "FKIKLeg_L.FKVis" "FKIKBlendLegCondition_L.ctg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegCondition_L.cfr";
connectAttr "FKIKBlendLegsetRange_L.ox" "FKIKBlendLegCondition_L.cfg";
connectAttr "FKIKLeg_L.FKIKBlend" "FKIKBlendLegsetRange_L.vx";
connectAttr "FKTrackBottom_L.s" "ScaleBlendTrackBottom_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendTrackBottom_L.b";
connectAttr "FKTrackBend_L.s" "ScaleBlendTrackBend_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendTrackBend_L.b";
connectAttr "ScaleBlendAddYTrackBend_L.o1" "ScaleBlendTrackBend_L.c1g";
connectAttr "FKTrack_L.s" "ScaleBlendTrack_L.c2";
connectAttr "FKIKBlendLegUnitConversion_L.o" "ScaleBlendTrack_L.b";
connectAttr "IKLeg_L.stretchy" "IKSetRangeStretchLeg_L.vy";
connectAttr "IKLeg_L.antiPop" "IKSetRangeAntiPopLeg_L.vy";
connectAttr "IKmessureBlendStretchLeg_L.o" "IKmessureDivLeg_L.i1y";
connectAttr "IKSetRangeAntiPopLeg_L.oy" "IKmessureBlendAntiPopLeg_L.ab";
connectAttr "IKdistanceLeg_LShape_normal.o" "IKmessureBlendAntiPopLeg_L.i[0]";
connectAttr "IKdistanceLeg_LShape_antiPop.o" "IKmessureBlendAntiPopLeg_L.i[1]";
connectAttr "IKdistanceLeg_LShape.dist" "IKdistanceLeg_LShape_antiPop.i";
connectAttr "IKdistanceLeg_LShape.dist" "IKdistanceLeg_LShape_normal.i";
connectAttr "IKmessureBlendAntiPopLeg_L.o" "IKdistanceClampLeg_L.ipr";
connectAttr "IKSetRangeStretchLeg_L.oy" "IKmessureBlendStretchLeg_L.ab";
connectAttr "IKdistanceClampLeg_L.opr" "IKmessureBlendStretchLeg_L.i[0]";
connectAttr "IKmessureBlendAntiPopLeg_L.o" "IKmessureBlendStretchLeg_L.i[1]";
connectAttr "IKmessureDivLeg_L.oy" "IKXTrackBend_L_IKmessureDiv_L.i1y";
connectAttr "IKXTrackBend_L_IKLength_L.oy" "IKXTrackBend_L_IKmessureDiv_L.i2y";
connectAttr "IKLeg_L.Length1" "IKXTrackBend_L_IKLength_L.i1y";
connectAttr "IKmessureDivLeg_L.oy" "IKXTrackBottom_L_IKmessureDiv_L.i1y";
connectAttr "IKXTrackBottom_L_IKLength_L.oy" "IKXTrackBottom_L_IKmessureDiv_L.i2y"
		;
connectAttr "IKLeg_L.Length2" "IKXTrackBottom_L_IKLength_L.i1y";
connectAttr "IKLeg_L.Length2" "ScaleBlendAddYTrackBend_L.i1[0]";
connectAttr "IKmessureDivLeg_L.oy" "ScaleBlendAddYTrackBend_L.i1[1]";
connectAttr "FKShoulder_L.Global" "GlobalShoulder_unitConversion_L.i";
connectAttr "GlobalShoulder_unitConversion_L.o" "GlobalShoulder_reverse_L.ix";
connectAttr "Center_M.CenterBtwFeet" "CenterBtwFeet_M.vx";
connectAttr "Center_M.CenterBtwFeet" "PelvisCenterBtwFeetUnitConversion_M.i";
connectAttr "PelvisCenterBtwFeetUnitConversion_M.o" "PelvisCenterBtwFeetReverse_M.ix"
		;
connectAttr "irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "BPRig_irisPuppetSG.pa" ":renderPartition.st" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
// End of Artillery__rig.ma
