//Maya ASCII 2013 scene
//Name: Colossus__mc-rigw@Col_pose.ma
//Last modified: Thu, Jun 12, 2014 02:38:28 PM
//Codeset: UTF-8
file -rdi 1 -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Colossus/Colossus__mc-rigw.ma";
file -rdi 1 -rpr "everlife" -rfn "everlifeRN" "/Users/jmiller/Art/everlife//Assets/Art/LightingResources/everlife_LightRig_Characters.ma";
file -rdi 1 -ns "Turret06" -dr 1 -rfn "Turret06RN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Colossus/Animations/Colossus2__mc-rigw@Col2_idle.ma";
file -rdi 1 -ns "Turret07" -rfn "Turret06RN1" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Colossus/Animations/Colossus3__mc-rigw@Col3_idle.ma";
file -rdi 2 -ns "rigw" -rfn "Turret07:rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Colossus/Colossus3__mc-rigw.ma";
file -rdi 3 -ns "rig" -rfn "Turret07:rigw:rigRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Rigs/Colossus3__rig.ma";
file -r -ns "rigw" -dr 1 -rfn "rigwRN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Colossus/Colossus__mc-rigw.ma";
file -r -rpr "everlife" -dr 1 -rfn "everlifeRN" "/Users/jmiller/Art/everlife//Assets/Art/LightingResources/everlife_LightRig_Characters.ma";
file -r -ns "Turret06" -dr 1 -rfn "Turret06RN" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Colossus/Animations/Colossus2__mc-rigw@Col2_idle.ma";
file -r -ns "Turret07" -dr 1 -rfn "Turret06RN1" "/Users/jmiller/Art/everlife//Assets/Art/Characters/Colossus/Animations/Colossus3__mc-rigw@Col3_idle.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.3";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 21.624411839516686 28.607940471941745 49.690217614420867 ;
	setAttr ".r" -type "double3" -14.738352729604538 25.800000000001432 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ovr" 1.3;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 59.642606252218521;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".dr" yes;
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" 0 0 500 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".ow" 72;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 66 ".lnk";
	setAttr -s 66 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "rigwRN";
	setAttr -s 60 ".phl";
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigwRN"
		"rigwRN" 0
		"rigw:rigRN" 92
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main" "scale" " -type \"double3\" 2.5 2.5 2.5"
		
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translate" " -type \"double3\" 0 0 1.815165"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotate" " -type \"double3\" 0.0662947 0.695437 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M" 
		"rotateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M|rigw:rig:FKXHead_M|rigw:rig:FKOffsetJaw_M|rigw:rig:FKExtraJaw_M|rigw:rig:FKJaw_M" 
		"rotate" " -type \"double3\" 3.140003 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M|rigw:rig:FKXHead_M|rigw:rig:FKOffsetJaw_M|rigw:rig:FKExtraJaw_M|rigw:rig:FKJaw_M" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotate" " -type \"double3\" 3.784796 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetFingers_R|rigw:rig:FKExtraFingers_R|rigw:rig:FKFingers_R" 
		"rotate" " -type \"double3\" 10.813704 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetFingers_R|rigw:rig:FKExtraFingers_R|rigw:rig:FKFingers_R" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotate" " -type \"double3\" 3.784796 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 10.813704 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetFingers_L|rigw:rig:FKExtraFingers_L|rigw:rig:FKFingers_L" 
		"rotate" " -type \"double3\" 14.5985 0 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetFingers_L|rigw:rig:FKExtraFingers_L|rigw:rig:FKFingers_L" 
		"rotateX" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" 0 0.296913 2.079476"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 -4.272014 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0 0.296913 -1.805259"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 3.447002 0"
		2 "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.CenterBtwFeet" 
		"rigwRN.placeHolderList[1]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateX" 
		"rigwRN.placeHolderList[2]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateY" 
		"rigwRN.placeHolderList[3]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.translateZ" 
		"rigwRN.placeHolderList[4]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateX" 
		"rigwRN.placeHolderList[5]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateY" 
		"rigwRN.placeHolderList[6]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M.rotateZ" 
		"rigwRN.placeHolderList[7]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.Global" 
		"rigwRN.placeHolderList[8]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateX" 
		"rigwRN.placeHolderList[9]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateY" 
		"rigwRN.placeHolderList[10]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M.rotateZ" 
		"rigwRN.placeHolderList[11]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M|rigw:rig:FKXHead_M|rigw:rig:FKOffsetJaw_M|rigw:rig:FKExtraJaw_M|rigw:rig:FKJaw_M.rotateX" 
		"rigwRN.placeHolderList[12]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M|rigw:rig:FKXHead_M|rigw:rig:FKOffsetJaw_M|rigw:rig:FKExtraJaw_M|rigw:rig:FKJaw_M.rotateY" 
		"rigwRN.placeHolderList[13]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetHead_M|rigw:rig:FKGlobalStaticHead_M|rigw:rig:FKGlobalHead_M|rigw:rig:FKExtraHead_M|rigw:rig:FKHead_M|rigw:rig:FKXHead_M|rigw:rig:FKOffsetJaw_M|rigw:rig:FKExtraJaw_M|rigw:rig:FKJaw_M.rotateZ" 
		"rigwRN.placeHolderList[14]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateX" 
		"rigwRN.placeHolderList[15]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateY" 
		"rigwRN.placeHolderList[16]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R.rotateZ" 
		"rigwRN.placeHolderList[17]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateX" 
		"rigwRN.placeHolderList[18]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateY" 
		"rigwRN.placeHolderList[19]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R.rotateZ" 
		"rigwRN.placeHolderList[20]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetFingers_R|rigw:rig:FKExtraFingers_R|rigw:rig:FKFingers_R.rotateX" 
		"rigwRN.placeHolderList[21]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetFingers_R|rigw:rig:FKExtraFingers_R|rigw:rig:FKFingers_R.rotateY" 
		"rigwRN.placeHolderList[22]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_R|rigw:rig:FKExtraShoulder_R|rigw:rig:FKShoulder_R|rigw:rig:FKXShoulder_R|rigw:rig:FKOffsetElbow_R|rigw:rig:FKExtraElbow_R|rigw:rig:FKElbow_R|rigw:rig:FKXElbow_R|rigw:rig:FKOffsetFingers_R|rigw:rig:FKExtraFingers_R|rigw:rig:FKFingers_R.rotateZ" 
		"rigwRN.placeHolderList[23]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateX" 
		"rigwRN.placeHolderList[24]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateY" 
		"rigwRN.placeHolderList[25]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L.rotateZ" 
		"rigwRN.placeHolderList[26]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateX" 
		"rigwRN.placeHolderList[27]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateY" 
		"rigwRN.placeHolderList[28]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L.rotateZ" 
		"rigwRN.placeHolderList[29]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetFingers_L|rigw:rig:FKExtraFingers_L|rigw:rig:FKFingers_L.rotateX" 
		"rigwRN.placeHolderList[30]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetFingers_L|rigw:rig:FKExtraFingers_L|rigw:rig:FKFingers_L.rotateY" 
		"rigwRN.placeHolderList[31]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:FKSystem|rigw:rig:PelvisCenterBtwLegsBlended_M|rigw:rig:CenterOffset_M|rigw:rig:CenterExtra_M|rigw:rig:Center_M|rigw:rig:FKOffsetPelvis_M|rigw:rig:FKExtraPelvis_M|rigw:rig:FKPelvis_M|rigw:rig:FKXPelvis_M|rigw:rig:FKOffsetShoulder_L|rigw:rig:FKExtraShoulder_L|rigw:rig:FKShoulder_L|rigw:rig:FKXShoulder_L|rigw:rig:FKOffsetElbow_L|rigw:rig:FKExtraElbow_L|rigw:rig:FKElbow_L|rigw:rig:FKXElbow_L|rigw:rig:FKOffsetFingers_L|rigw:rig:FKExtraFingers_L|rigw:rig:FKFingers_L.rotateZ" 
		"rigwRN.placeHolderList[32]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateX" 
		"rigwRN.placeHolderList[33]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateY" 
		"rigwRN.placeHolderList[34]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rotateZ" 
		"rigwRN.placeHolderList[35]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateX" 
		"rigwRN.placeHolderList[36]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateY" 
		"rigwRN.placeHolderList[37]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.translateZ" 
		"rigwRN.placeHolderList[38]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.swivel" 
		"rigwRN.placeHolderList[39]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.toe" 
		"rigwRN.placeHolderList[40]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.rollAngle" 
		"rigwRN.placeHolderList[41]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.roll" 
		"rigwRN.placeHolderList[42]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.stretchy" 
		"rigwRN.placeHolderList[43]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.antiPop" 
		"rigwRN.placeHolderList[44]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length1" 
		"rigwRN.placeHolderList[45]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_R|rigw:rig:IKExtraLeg_R|rigw:rig:IKLeg_R.Length2" 
		"rigwRN.placeHolderList[46]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateX" 
		"rigwRN.placeHolderList[47]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateY" 
		"rigwRN.placeHolderList[48]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rotateZ" 
		"rigwRN.placeHolderList[49]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateX" 
		"rigwRN.placeHolderList[50]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateY" 
		"rigwRN.placeHolderList[51]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.translateZ" 
		"rigwRN.placeHolderList[52]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.swivel" 
		"rigwRN.placeHolderList[53]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.toe" 
		"rigwRN.placeHolderList[54]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.rollAngle" 
		"rigwRN.placeHolderList[55]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.roll" 
		"rigwRN.placeHolderList[56]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.stretchy" 
		"rigwRN.placeHolderList[57]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.antiPop" 
		"rigwRN.placeHolderList[58]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length1" 
		"rigwRN.placeHolderList[59]" ""
		5 4 "rigwRN" "|rigw:rig:ctrl_rig|rigw:rig:Main|rigw:rig:MotionSystem|rigw:rig:IKSystem|rigw:rig:IKHandle|rigw:rig:IKParentConstraintLeg_L|rigw:rig:IKExtraLeg_L|rigw:rig:IKLeg_L.Length2" 
		"rigwRN.placeHolderList[60]" ""
		"rigw:rigRN" 0;
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".fil" 2;
	setAttr ".filw" 0.83333331346511841;
	setAttr ".filh" 0.83333331346511841;
	setAttr ".maxr" 2;
	setAttr ".gi" yes;
	setAttr ".fg" yes;
	setAttr ".cm" yes;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".cs" 3;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 6 -ast 1 -aet 7 ";
	setAttr ".st" 6;
createNode animCurveTL -n "rigw:rig:Center_M_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 -0.9813240112778886 7 0;
createNode animCurveTL -n "rigw:rig:Center_M_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1.8151654014160987 7 1.8151654014160987;
createNode animCurveTA -n "rigw:rig:Center_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 4 3.6298017254293344 7 0;
createNode animCurveTA -n "rigw:rig:Center_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTU -n "rigw:rig:Center_M_CenterBtwFeet";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 7 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.29691296552239654;
createNode animCurveTL -n "rigw:rig:IKLeg_L_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -1.805259253503319;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.29691296552239654;
createNode animCurveTL -n "rigw:rig:IKLeg_R_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2.0794755266739071;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 3.4470015893070181;
createNode animCurveTA -n "rigw:rig:IKLeg_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_swivel";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_toe";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_roll";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_rollAngle";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 25;
createNode animCurveTU -n "rigw:rig:IKLeg_L_stretchy";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_antiPop";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:rig:IKLeg_L_Length2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -4.2720144804035973;
createNode animCurveTA -n "rigw:rig:IKLeg_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_swivel";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_toe";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_roll";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_rollAngle";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 25;
createNode animCurveTU -n "rigw:rig:IKLeg_R_stretchy";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_antiPop";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "rigw:rig:IKLeg_R_Length2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 0.25570825357423205 2 0 5 0.25570825357423205
		 8 0;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 2.6823993538699611 2 0 5 2.6823993538699611
		 8 0;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "rigw:rig:FKHead_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTU -n "rigw:rig:FKHead_M_Global";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 14.598500386562923 2 0 5 14.598500386562923
		 8 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -3 0 0 14.598500386562923 3 0 6 14.598500386562923
		 9 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 3 0 9 0;
createNode animCurveTA -n "rigw:rig:FKElbow_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 3 0 9 0;
createNode animCurveTA -n "rigw:rig:FKFingers_L_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -2 0 1 14.598500386562923 4 0 7 14.598500386562923
		 10 0;
createNode animCurveTA -n "rigw:rig:FKFingers_L_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -2 0 4 0 10 0;
createNode animCurveTA -n "rigw:rig:FKFingers_L_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -2 0 4 0 10 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 4 14.598500386562923 7 0 10 14.598500386562923
		 13 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 13 0;
createNode animCurveTA -n "rigw:rig:FKShoulder_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  1 0 7 0 13 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 14.598500386562923 2 0 5 14.598500386562923
		 8 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKElbow_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKFingers_R_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -3 0 0 14.598500386562923 3 0 6 14.598500386562923
		 9 0;
createNode animCurveTA -n "rigw:rig:FKFingers_R_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 3 0 9 0;
createNode animCurveTA -n "rigw:rig:FKFingers_R_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -3 0 3 0 9 0;
createNode animCurveTA -n "rigw:rig:FKJaw_M_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  -4 0 -1 12.11143933630782 2 0 5 12.11143933630782
		 8 0;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "rigw:rig:FKJaw_M_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode animCurveTA -n "rigw:rig:FKJaw_M_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  -4 0 2 0 8 0;
createNode reference -n "everlifeRN";
	setAttr ".ed" -type "dataReferenceEdits" 
		"everlifeRN"
		"everlifeRN" 0;
lockNode -l 1 ;
createNode reference -n "Turret06RN";
	setAttr ".ed" -type "dataReferenceEdits" 
		"Turret06RN"
		"Turret06:rigw:rigRN" 56
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M" 
		"translate" " -type \"double3\" 0 0 1.815165"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetHead_M|Turret06:rigw:rig:FKGlobalStaticHead_M|Turret06:rigw:rig:FKGlobalHead_M|Turret06:rigw:rig:FKExtraHead_M|Turret06:rigw:rig:FKHead_M" 
		"rotate" " -type \"double3\" 0.0662947 0.695437 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetHead_M|Turret06:rigw:rig:FKGlobalStaticHead_M|Turret06:rigw:rig:FKGlobalHead_M|Turret06:rigw:rig:FKExtraHead_M|Turret06:rigw:rig:FKHead_M" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetHead_M|Turret06:rigw:rig:FKGlobalStaticHead_M|Turret06:rigw:rig:FKGlobalHead_M|Turret06:rigw:rig:FKExtraHead_M|Turret06:rigw:rig:FKHead_M" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetHead_M|Turret06:rigw:rig:FKGlobalStaticHead_M|Turret06:rigw:rig:FKGlobalHead_M|Turret06:rigw:rig:FKExtraHead_M|Turret06:rigw:rig:FKHead_M" 
		"rotateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetHead_M|Turret06:rigw:rig:FKGlobalStaticHead_M|Turret06:rigw:rig:FKGlobalHead_M|Turret06:rigw:rig:FKExtraHead_M|Turret06:rigw:rig:FKHead_M|Turret06:rigw:rig:FKXHead_M|Turret06:rigw:rig:FKOffsetJaw_M|Turret06:rigw:rig:FKExtraJaw_M|Turret06:rigw:rig:FKJaw_M" 
		"rotate" " -type \"double3\" 3.140003 0 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetHead_M|Turret06:rigw:rig:FKGlobalStaticHead_M|Turret06:rigw:rig:FKGlobalHead_M|Turret06:rigw:rig:FKExtraHead_M|Turret06:rigw:rig:FKHead_M|Turret06:rigw:rig:FKXHead_M|Turret06:rigw:rig:FKOffsetJaw_M|Turret06:rigw:rig:FKExtraJaw_M|Turret06:rigw:rig:FKJaw_M" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetHead_M|Turret06:rigw:rig:FKGlobalStaticHead_M|Turret06:rigw:rig:FKGlobalHead_M|Turret06:rigw:rig:FKExtraHead_M|Turret06:rigw:rig:FKHead_M|Turret06:rigw:rig:FKXHead_M|Turret06:rigw:rig:FKOffsetJaw_M|Turret06:rigw:rig:FKExtraJaw_M|Turret06:rigw:rig:FKJaw_M" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetHead_M|Turret06:rigw:rig:FKGlobalStaticHead_M|Turret06:rigw:rig:FKGlobalHead_M|Turret06:rigw:rig:FKExtraHead_M|Turret06:rigw:rig:FKHead_M|Turret06:rigw:rig:FKXHead_M|Turret06:rigw:rig:FKOffsetJaw_M|Turret06:rigw:rig:FKExtraJaw_M|Turret06:rigw:rig:FKJaw_M" 
		"rotateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R|Turret06:rigw:rig:FKXShoulder_R|Turret06:rigw:rig:FKOffsetElbow_R|Turret06:rigw:rig:FKExtraElbow_R|Turret06:rigw:rig:FKElbow_R" 
		"rotate" " -type \"double3\" 3.784796 0 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R|Turret06:rigw:rig:FKXShoulder_R|Turret06:rigw:rig:FKOffsetElbow_R|Turret06:rigw:rig:FKExtraElbow_R|Turret06:rigw:rig:FKElbow_R" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R|Turret06:rigw:rig:FKXShoulder_R|Turret06:rigw:rig:FKOffsetElbow_R|Turret06:rigw:rig:FKExtraElbow_R|Turret06:rigw:rig:FKElbow_R" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R|Turret06:rigw:rig:FKXShoulder_R|Turret06:rigw:rig:FKOffsetElbow_R|Turret06:rigw:rig:FKExtraElbow_R|Turret06:rigw:rig:FKElbow_R" 
		"rotateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R|Turret06:rigw:rig:FKXShoulder_R|Turret06:rigw:rig:FKOffsetElbow_R|Turret06:rigw:rig:FKExtraElbow_R|Turret06:rigw:rig:FKElbow_R|Turret06:rigw:rig:FKXElbow_R|Turret06:rigw:rig:FKOffsetFingers_R|Turret06:rigw:rig:FKExtraFingers_R|Turret06:rigw:rig:FKFingers_R" 
		"rotate" " -type \"double3\" 10.813704 0 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R|Turret06:rigw:rig:FKXShoulder_R|Turret06:rigw:rig:FKOffsetElbow_R|Turret06:rigw:rig:FKExtraElbow_R|Turret06:rigw:rig:FKElbow_R|Turret06:rigw:rig:FKXElbow_R|Turret06:rigw:rig:FKOffsetFingers_R|Turret06:rigw:rig:FKExtraFingers_R|Turret06:rigw:rig:FKFingers_R" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R|Turret06:rigw:rig:FKXShoulder_R|Turret06:rigw:rig:FKOffsetElbow_R|Turret06:rigw:rig:FKExtraElbow_R|Turret06:rigw:rig:FKElbow_R|Turret06:rigw:rig:FKXElbow_R|Turret06:rigw:rig:FKOffsetFingers_R|Turret06:rigw:rig:FKExtraFingers_R|Turret06:rigw:rig:FKFingers_R" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_R|Turret06:rigw:rig:FKExtraShoulder_R|Turret06:rigw:rig:FKShoulder_R|Turret06:rigw:rig:FKXShoulder_R|Turret06:rigw:rig:FKOffsetElbow_R|Turret06:rigw:rig:FKExtraElbow_R|Turret06:rigw:rig:FKElbow_R|Turret06:rigw:rig:FKXElbow_R|Turret06:rigw:rig:FKOffsetFingers_R|Turret06:rigw:rig:FKExtraFingers_R|Turret06:rigw:rig:FKFingers_R" 
		"rotateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L" 
		"rotate" " -type \"double3\" 3.784796 0 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L" 
		"rotateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L|Turret06:rigw:rig:FKXShoulder_L|Turret06:rigw:rig:FKOffsetElbow_L|Turret06:rigw:rig:FKExtraElbow_L|Turret06:rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 10.813704 0 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L|Turret06:rigw:rig:FKXShoulder_L|Turret06:rigw:rig:FKOffsetElbow_L|Turret06:rigw:rig:FKExtraElbow_L|Turret06:rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L|Turret06:rigw:rig:FKXShoulder_L|Turret06:rigw:rig:FKOffsetElbow_L|Turret06:rigw:rig:FKExtraElbow_L|Turret06:rigw:rig:FKElbow_L" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L|Turret06:rigw:rig:FKXShoulder_L|Turret06:rigw:rig:FKOffsetElbow_L|Turret06:rigw:rig:FKExtraElbow_L|Turret06:rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L|Turret06:rigw:rig:FKXShoulder_L|Turret06:rigw:rig:FKOffsetElbow_L|Turret06:rigw:rig:FKExtraElbow_L|Turret06:rigw:rig:FKElbow_L|Turret06:rigw:rig:FKXElbow_L|Turret06:rigw:rig:FKOffsetFingers_L|Turret06:rigw:rig:FKExtraFingers_L|Turret06:rigw:rig:FKFingers_L" 
		"rotate" " -type \"double3\" 14.5985 0 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L|Turret06:rigw:rig:FKXShoulder_L|Turret06:rigw:rig:FKOffsetElbow_L|Turret06:rigw:rig:FKExtraElbow_L|Turret06:rigw:rig:FKElbow_L|Turret06:rigw:rig:FKXElbow_L|Turret06:rigw:rig:FKOffsetFingers_L|Turret06:rigw:rig:FKExtraFingers_L|Turret06:rigw:rig:FKFingers_L" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L|Turret06:rigw:rig:FKXShoulder_L|Turret06:rigw:rig:FKOffsetElbow_L|Turret06:rigw:rig:FKExtraElbow_L|Turret06:rigw:rig:FKElbow_L|Turret06:rigw:rig:FKXElbow_L|Turret06:rigw:rig:FKOffsetFingers_L|Turret06:rigw:rig:FKExtraFingers_L|Turret06:rigw:rig:FKFingers_L" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:FKSystem|Turret06:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret06:rigw:rig:CenterOffset_M|Turret06:rigw:rig:CenterExtra_M|Turret06:rigw:rig:Center_M|Turret06:rigw:rig:FKOffsetPelvis_M|Turret06:rigw:rig:FKExtraPelvis_M|Turret06:rigw:rig:FKPelvis_M|Turret06:rigw:rig:FKXPelvis_M|Turret06:rigw:rig:FKOffsetShoulder_L|Turret06:rigw:rig:FKExtraShoulder_L|Turret06:rigw:rig:FKShoulder_L|Turret06:rigw:rig:FKXShoulder_L|Turret06:rigw:rig:FKOffsetElbow_L|Turret06:rigw:rig:FKExtraElbow_L|Turret06:rigw:rig:FKElbow_L|Turret06:rigw:rig:FKXElbow_L|Turret06:rigw:rig:FKOffsetFingers_L|Turret06:rigw:rig:FKExtraFingers_L|Turret06:rigw:rig:FKFingers_L" 
		"rotateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_R|Turret06:rigw:rig:IKExtraLeg_R|Turret06:rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" 0 0.296913 2.079476"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_R|Turret06:rigw:rig:IKExtraLeg_R|Turret06:rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_R|Turret06:rigw:rig:IKExtraLeg_R|Turret06:rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_R|Turret06:rigw:rig:IKExtraLeg_R|Turret06:rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_R|Turret06:rigw:rig:IKExtraLeg_R|Turret06:rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 -4.272014 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_R|Turret06:rigw:rig:IKExtraLeg_R|Turret06:rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_R|Turret06:rigw:rig:IKExtraLeg_R|Turret06:rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_R|Turret06:rigw:rig:IKExtraLeg_R|Turret06:rigw:rig:IKLeg_R" 
		"rotateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_L|Turret06:rigw:rig:IKExtraLeg_L|Turret06:rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0 0.296913 -1.805259"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_L|Turret06:rigw:rig:IKExtraLeg_L|Turret06:rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_L|Turret06:rigw:rig:IKExtraLeg_L|Turret06:rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_L|Turret06:rigw:rig:IKExtraLeg_L|Turret06:rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_L|Turret06:rigw:rig:IKExtraLeg_L|Turret06:rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 3.447002 0"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_L|Turret06:rigw:rig:IKExtraLeg_L|Turret06:rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_L|Turret06:rigw:rig:IKExtraLeg_L|Turret06:rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		2 "|Turret06:rigw:rig:ctrl_rig|Turret06:rigw:rig:Main|Turret06:rigw:rig:MotionSystem|Turret06:rigw:rig:IKSystem|Turret06:rigw:rig:IKHandle|Turret06:rigw:rig:IKParentConstraintLeg_L|Turret06:rigw:rig:IKExtraLeg_L|Turret06:rigw:rig:IKLeg_L" 
		"rotateZ" " -av"
		"Turret06RN" 1
		2 "|Turret06:OrthoCamera|Turret06:orthCam|Turret06:orthCamShape" "renderable" 
		" 1"
		"Turret06:rigwRN" 0;
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "Turret06RN1";
	setAttr ".ed" -type "dataReferenceEdits" 
		"Turret06RN1"
		"Turret07:rigw:rigRN" 0
		"Turret06RN1" 0
		"Turret07:rigwRN" 0
		"Turret06RN1" 1
		2 "|Turret07:OrthoCamera|Turret07:orthCam|Turret07:orthCamShape" "renderable" 
		" 1"
		"Turret07:rigw:rigRN" 56
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M" 
		"translate" " -type \"double3\" 0 0 1.815165"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M" 
		"translateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M" 
		"translateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M" 
		"translateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M" 
		"rotateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetHead_M|Turret07:rigw:rig:FKGlobalStaticHead_M|Turret07:rigw:rig:FKGlobalHead_M|Turret07:rigw:rig:FKExtraHead_M|Turret07:rigw:rig:FKHead_M" 
		"rotate" " -type \"double3\" 0.0662947 0.695437 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetHead_M|Turret07:rigw:rig:FKGlobalStaticHead_M|Turret07:rigw:rig:FKGlobalHead_M|Turret07:rigw:rig:FKExtraHead_M|Turret07:rigw:rig:FKHead_M" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetHead_M|Turret07:rigw:rig:FKGlobalStaticHead_M|Turret07:rigw:rig:FKGlobalHead_M|Turret07:rigw:rig:FKExtraHead_M|Turret07:rigw:rig:FKHead_M" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetHead_M|Turret07:rigw:rig:FKGlobalStaticHead_M|Turret07:rigw:rig:FKGlobalHead_M|Turret07:rigw:rig:FKExtraHead_M|Turret07:rigw:rig:FKHead_M" 
		"rotateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetHead_M|Turret07:rigw:rig:FKGlobalStaticHead_M|Turret07:rigw:rig:FKGlobalHead_M|Turret07:rigw:rig:FKExtraHead_M|Turret07:rigw:rig:FKHead_M|Turret07:rigw:rig:FKXHead_M|Turret07:rigw:rig:FKOffsetJaw_M|Turret07:rigw:rig:FKExtraJaw_M|Turret07:rigw:rig:FKJaw_M" 
		"rotate" " -type \"double3\" 3.140003 0 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetHead_M|Turret07:rigw:rig:FKGlobalStaticHead_M|Turret07:rigw:rig:FKGlobalHead_M|Turret07:rigw:rig:FKExtraHead_M|Turret07:rigw:rig:FKHead_M|Turret07:rigw:rig:FKXHead_M|Turret07:rigw:rig:FKOffsetJaw_M|Turret07:rigw:rig:FKExtraJaw_M|Turret07:rigw:rig:FKJaw_M" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetHead_M|Turret07:rigw:rig:FKGlobalStaticHead_M|Turret07:rigw:rig:FKGlobalHead_M|Turret07:rigw:rig:FKExtraHead_M|Turret07:rigw:rig:FKHead_M|Turret07:rigw:rig:FKXHead_M|Turret07:rigw:rig:FKOffsetJaw_M|Turret07:rigw:rig:FKExtraJaw_M|Turret07:rigw:rig:FKJaw_M" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetHead_M|Turret07:rigw:rig:FKGlobalStaticHead_M|Turret07:rigw:rig:FKGlobalHead_M|Turret07:rigw:rig:FKExtraHead_M|Turret07:rigw:rig:FKHead_M|Turret07:rigw:rig:FKXHead_M|Turret07:rigw:rig:FKOffsetJaw_M|Turret07:rigw:rig:FKExtraJaw_M|Turret07:rigw:rig:FKJaw_M" 
		"rotateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R" 
		"rotateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R|Turret07:rigw:rig:FKXShoulder_R|Turret07:rigw:rig:FKOffsetElbow_R|Turret07:rigw:rig:FKExtraElbow_R|Turret07:rigw:rig:FKElbow_R" 
		"rotate" " -type \"double3\" 3.784796 0 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R|Turret07:rigw:rig:FKXShoulder_R|Turret07:rigw:rig:FKOffsetElbow_R|Turret07:rigw:rig:FKExtraElbow_R|Turret07:rigw:rig:FKElbow_R" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R|Turret07:rigw:rig:FKXShoulder_R|Turret07:rigw:rig:FKOffsetElbow_R|Turret07:rigw:rig:FKExtraElbow_R|Turret07:rigw:rig:FKElbow_R" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R|Turret07:rigw:rig:FKXShoulder_R|Turret07:rigw:rig:FKOffsetElbow_R|Turret07:rigw:rig:FKExtraElbow_R|Turret07:rigw:rig:FKElbow_R" 
		"rotateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R|Turret07:rigw:rig:FKXShoulder_R|Turret07:rigw:rig:FKOffsetElbow_R|Turret07:rigw:rig:FKExtraElbow_R|Turret07:rigw:rig:FKElbow_R|Turret07:rigw:rig:FKXElbow_R|Turret07:rigw:rig:FKOffsetFingers_R|Turret07:rigw:rig:FKExtraFingers_R|Turret07:rigw:rig:FKFingers_R" 
		"rotate" " -type \"double3\" 10.813704 0 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R|Turret07:rigw:rig:FKXShoulder_R|Turret07:rigw:rig:FKOffsetElbow_R|Turret07:rigw:rig:FKExtraElbow_R|Turret07:rigw:rig:FKElbow_R|Turret07:rigw:rig:FKXElbow_R|Turret07:rigw:rig:FKOffsetFingers_R|Turret07:rigw:rig:FKExtraFingers_R|Turret07:rigw:rig:FKFingers_R" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R|Turret07:rigw:rig:FKXShoulder_R|Turret07:rigw:rig:FKOffsetElbow_R|Turret07:rigw:rig:FKExtraElbow_R|Turret07:rigw:rig:FKElbow_R|Turret07:rigw:rig:FKXElbow_R|Turret07:rigw:rig:FKOffsetFingers_R|Turret07:rigw:rig:FKExtraFingers_R|Turret07:rigw:rig:FKFingers_R" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_R|Turret07:rigw:rig:FKExtraShoulder_R|Turret07:rigw:rig:FKShoulder_R|Turret07:rigw:rig:FKXShoulder_R|Turret07:rigw:rig:FKOffsetElbow_R|Turret07:rigw:rig:FKExtraElbow_R|Turret07:rigw:rig:FKElbow_R|Turret07:rigw:rig:FKXElbow_R|Turret07:rigw:rig:FKOffsetFingers_R|Turret07:rigw:rig:FKExtraFingers_R|Turret07:rigw:rig:FKFingers_R" 
		"rotateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L" 
		"rotate" " -type \"double3\" 3.784796 0 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L" 
		"rotateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L|Turret07:rigw:rig:FKXShoulder_L|Turret07:rigw:rig:FKOffsetElbow_L|Turret07:rigw:rig:FKExtraElbow_L|Turret07:rigw:rig:FKElbow_L" 
		"rotate" " -type \"double3\" 10.813704 0 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L|Turret07:rigw:rig:FKXShoulder_L|Turret07:rigw:rig:FKOffsetElbow_L|Turret07:rigw:rig:FKExtraElbow_L|Turret07:rigw:rig:FKElbow_L" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L|Turret07:rigw:rig:FKXShoulder_L|Turret07:rigw:rig:FKOffsetElbow_L|Turret07:rigw:rig:FKExtraElbow_L|Turret07:rigw:rig:FKElbow_L" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L|Turret07:rigw:rig:FKXShoulder_L|Turret07:rigw:rig:FKOffsetElbow_L|Turret07:rigw:rig:FKExtraElbow_L|Turret07:rigw:rig:FKElbow_L" 
		"rotateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L|Turret07:rigw:rig:FKXShoulder_L|Turret07:rigw:rig:FKOffsetElbow_L|Turret07:rigw:rig:FKExtraElbow_L|Turret07:rigw:rig:FKElbow_L|Turret07:rigw:rig:FKXElbow_L|Turret07:rigw:rig:FKOffsetFingers_L|Turret07:rigw:rig:FKExtraFingers_L|Turret07:rigw:rig:FKFingers_L" 
		"rotate" " -type \"double3\" 14.5985 0 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L|Turret07:rigw:rig:FKXShoulder_L|Turret07:rigw:rig:FKOffsetElbow_L|Turret07:rigw:rig:FKExtraElbow_L|Turret07:rigw:rig:FKElbow_L|Turret07:rigw:rig:FKXElbow_L|Turret07:rigw:rig:FKOffsetFingers_L|Turret07:rigw:rig:FKExtraFingers_L|Turret07:rigw:rig:FKFingers_L" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L|Turret07:rigw:rig:FKXShoulder_L|Turret07:rigw:rig:FKOffsetElbow_L|Turret07:rigw:rig:FKExtraElbow_L|Turret07:rigw:rig:FKElbow_L|Turret07:rigw:rig:FKXElbow_L|Turret07:rigw:rig:FKOffsetFingers_L|Turret07:rigw:rig:FKExtraFingers_L|Turret07:rigw:rig:FKFingers_L" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:FKSystem|Turret07:rigw:rig:PelvisCenterBtwLegsBlended_M|Turret07:rigw:rig:CenterOffset_M|Turret07:rigw:rig:CenterExtra_M|Turret07:rigw:rig:Center_M|Turret07:rigw:rig:FKOffsetPelvis_M|Turret07:rigw:rig:FKExtraPelvis_M|Turret07:rigw:rig:FKPelvis_M|Turret07:rigw:rig:FKXPelvis_M|Turret07:rigw:rig:FKOffsetShoulder_L|Turret07:rigw:rig:FKExtraShoulder_L|Turret07:rigw:rig:FKShoulder_L|Turret07:rigw:rig:FKXShoulder_L|Turret07:rigw:rig:FKOffsetElbow_L|Turret07:rigw:rig:FKExtraElbow_L|Turret07:rigw:rig:FKElbow_L|Turret07:rigw:rig:FKXElbow_L|Turret07:rigw:rig:FKOffsetFingers_L|Turret07:rigw:rig:FKExtraFingers_L|Turret07:rigw:rig:FKFingers_L" 
		"rotateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_R|Turret07:rigw:rig:IKExtraLeg_R|Turret07:rigw:rig:IKLeg_R" 
		"translate" " -type \"double3\" 0 0.296913 2.079476"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_R|Turret07:rigw:rig:IKExtraLeg_R|Turret07:rigw:rig:IKLeg_R" 
		"translateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_R|Turret07:rigw:rig:IKExtraLeg_R|Turret07:rigw:rig:IKLeg_R" 
		"translateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_R|Turret07:rigw:rig:IKExtraLeg_R|Turret07:rigw:rig:IKLeg_R" 
		"translateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_R|Turret07:rigw:rig:IKExtraLeg_R|Turret07:rigw:rig:IKLeg_R" 
		"rotate" " -type \"double3\" 0 -4.272014 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_R|Turret07:rigw:rig:IKExtraLeg_R|Turret07:rigw:rig:IKLeg_R" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_R|Turret07:rigw:rig:IKExtraLeg_R|Turret07:rigw:rig:IKLeg_R" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_R|Turret07:rigw:rig:IKExtraLeg_R|Turret07:rigw:rig:IKLeg_R" 
		"rotateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_L|Turret07:rigw:rig:IKExtraLeg_L|Turret07:rigw:rig:IKLeg_L" 
		"translate" " -type \"double3\" 0 0.296913 -1.805259"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_L|Turret07:rigw:rig:IKExtraLeg_L|Turret07:rigw:rig:IKLeg_L" 
		"translateZ" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_L|Turret07:rigw:rig:IKExtraLeg_L|Turret07:rigw:rig:IKLeg_L" 
		"translateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_L|Turret07:rigw:rig:IKExtraLeg_L|Turret07:rigw:rig:IKLeg_L" 
		"translateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_L|Turret07:rigw:rig:IKExtraLeg_L|Turret07:rigw:rig:IKLeg_L" 
		"rotate" " -type \"double3\" 0 3.447002 0"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_L|Turret07:rigw:rig:IKExtraLeg_L|Turret07:rigw:rig:IKLeg_L" 
		"rotateY" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_L|Turret07:rigw:rig:IKExtraLeg_L|Turret07:rigw:rig:IKLeg_L" 
		"rotateX" " -av"
		2 "|Turret07:rigw:rig:ctrl_rig|Turret07:rigw:rig:Main|Turret07:rigw:rig:MotionSystem|Turret07:rigw:rig:IKSystem|Turret07:rigw:rig:IKHandle|Turret07:rigw:rig:IKParentConstraintLeg_L|Turret07:rigw:rig:IKExtraLeg_L|Turret07:rigw:rig:IKLeg_L" 
		"rotateZ" " -av";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode reference -n "sharedReferenceNode";
	setAttr ".ed" -type "dataReferenceEdits" 
		"sharedReferenceNode";
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 11 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -s 35 ".gn";
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 9 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 5 ".tx";
select -ne :lightList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 7 ".l";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 12 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 5 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren" -type "string" "mentalRay";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 32;
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar" 0;
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn" no;
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff" yes;
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultRenderQuality;
	setAttr ".ert" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w" 2048;
	setAttr -av ".h" 2048;
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al" yes;
	setAttr -av ".dar" 1;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -s 7 ".dsm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 2 ".sol";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "rigw:rig:Center_M_CenterBtwFeet.o" "rigwRN.phl[1]";
connectAttr "rigw:rig:Center_M_translateX.o" "rigwRN.phl[2]";
connectAttr "rigw:rig:Center_M_translateY.o" "rigwRN.phl[3]";
connectAttr "rigw:rig:Center_M_translateZ.o" "rigwRN.phl[4]";
connectAttr "rigw:rig:Center_M_rotateX.o" "rigwRN.phl[5]";
connectAttr "rigw:rig:Center_M_rotateY.o" "rigwRN.phl[6]";
connectAttr "rigw:rig:Center_M_rotateZ.o" "rigwRN.phl[7]";
connectAttr "rigw:rig:FKHead_M_Global.o" "rigwRN.phl[8]";
connectAttr "rigw:rig:FKHead_M_rotateX.o" "rigwRN.phl[9]";
connectAttr "rigw:rig:FKHead_M_rotateY.o" "rigwRN.phl[10]";
connectAttr "rigw:rig:FKHead_M_rotateZ.o" "rigwRN.phl[11]";
connectAttr "rigw:rig:FKJaw_M_rotateX.o" "rigwRN.phl[12]";
connectAttr "rigw:rig:FKJaw_M_rotateY.o" "rigwRN.phl[13]";
connectAttr "rigw:rig:FKJaw_M_rotateZ.o" "rigwRN.phl[14]";
connectAttr "rigw:rig:FKShoulder_R_rotateX.o" "rigwRN.phl[15]";
connectAttr "rigw:rig:FKShoulder_R_rotateY.o" "rigwRN.phl[16]";
connectAttr "rigw:rig:FKShoulder_R_rotateZ.o" "rigwRN.phl[17]";
connectAttr "rigw:rig:FKElbow_R_rotateX.o" "rigwRN.phl[18]";
connectAttr "rigw:rig:FKElbow_R_rotateY.o" "rigwRN.phl[19]";
connectAttr "rigw:rig:FKElbow_R_rotateZ.o" "rigwRN.phl[20]";
connectAttr "rigw:rig:FKFingers_R_rotateX.o" "rigwRN.phl[21]";
connectAttr "rigw:rig:FKFingers_R_rotateY.o" "rigwRN.phl[22]";
connectAttr "rigw:rig:FKFingers_R_rotateZ.o" "rigwRN.phl[23]";
connectAttr "rigw:rig:FKShoulder_L_rotateX.o" "rigwRN.phl[24]";
connectAttr "rigw:rig:FKShoulder_L_rotateY.o" "rigwRN.phl[25]";
connectAttr "rigw:rig:FKShoulder_L_rotateZ.o" "rigwRN.phl[26]";
connectAttr "rigw:rig:FKElbow_L_rotateX.o" "rigwRN.phl[27]";
connectAttr "rigw:rig:FKElbow_L_rotateY.o" "rigwRN.phl[28]";
connectAttr "rigw:rig:FKElbow_L_rotateZ.o" "rigwRN.phl[29]";
connectAttr "rigw:rig:FKFingers_L_rotateX.o" "rigwRN.phl[30]";
connectAttr "rigw:rig:FKFingers_L_rotateY.o" "rigwRN.phl[31]";
connectAttr "rigw:rig:FKFingers_L_rotateZ.o" "rigwRN.phl[32]";
connectAttr "rigw:rig:IKLeg_R_rotateX.o" "rigwRN.phl[33]";
connectAttr "rigw:rig:IKLeg_R_rotateY.o" "rigwRN.phl[34]";
connectAttr "rigw:rig:IKLeg_R_rotateZ.o" "rigwRN.phl[35]";
connectAttr "rigw:rig:IKLeg_R_translateX.o" "rigwRN.phl[36]";
connectAttr "rigw:rig:IKLeg_R_translateY.o" "rigwRN.phl[37]";
connectAttr "rigw:rig:IKLeg_R_translateZ.o" "rigwRN.phl[38]";
connectAttr "rigw:rig:IKLeg_R_swivel.o" "rigwRN.phl[39]";
connectAttr "rigw:rig:IKLeg_R_toe.o" "rigwRN.phl[40]";
connectAttr "rigw:rig:IKLeg_R_rollAngle.o" "rigwRN.phl[41]";
connectAttr "rigw:rig:IKLeg_R_roll.o" "rigwRN.phl[42]";
connectAttr "rigw:rig:IKLeg_R_stretchy.o" "rigwRN.phl[43]";
connectAttr "rigw:rig:IKLeg_R_antiPop.o" "rigwRN.phl[44]";
connectAttr "rigw:rig:IKLeg_R_Length1.o" "rigwRN.phl[45]";
connectAttr "rigw:rig:IKLeg_R_Length2.o" "rigwRN.phl[46]";
connectAttr "rigw:rig:IKLeg_L_rotateX.o" "rigwRN.phl[47]";
connectAttr "rigw:rig:IKLeg_L_rotateY.o" "rigwRN.phl[48]";
connectAttr "rigw:rig:IKLeg_L_rotateZ.o" "rigwRN.phl[49]";
connectAttr "rigw:rig:IKLeg_L_translateX.o" "rigwRN.phl[50]";
connectAttr "rigw:rig:IKLeg_L_translateY.o" "rigwRN.phl[51]";
connectAttr "rigw:rig:IKLeg_L_translateZ.o" "rigwRN.phl[52]";
connectAttr "rigw:rig:IKLeg_L_swivel.o" "rigwRN.phl[53]";
connectAttr "rigw:rig:IKLeg_L_toe.o" "rigwRN.phl[54]";
connectAttr "rigw:rig:IKLeg_L_rollAngle.o" "rigwRN.phl[55]";
connectAttr "rigw:rig:IKLeg_L_roll.o" "rigwRN.phl[56]";
connectAttr "rigw:rig:IKLeg_L_stretchy.o" "rigwRN.phl[57]";
connectAttr "rigw:rig:IKLeg_L_antiPop.o" "rigwRN.phl[58]";
connectAttr "rigw:rig:IKLeg_L_Length1.o" "rigwRN.phl[59]";
connectAttr "rigw:rig:IKLeg_L_Length2.o" "rigwRN.phl[60]";
connectAttr "sharedReferenceNode.sr" "rigwRN.sr";
connectAttr "sharedReferenceNode.sr" "Turret06RN.sr";
connectAttr "sharedReferenceNode.sr" "Turret06RN1.sr";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Colossus__mc-rigw@Col_pose.ma
