//Maya ASCII 2013 scene
//Name: Turret05_AcidGunTower03.ma
//Last modified: Wed, Jun 11, 2014 12:09:37 PM
//Codeset: UTF-8
file -rdi 1 -rpr "Turret05" -rfn "Turret05RN" "/Users/jmiller/Art/everlife//Assets/Art/Defense/Turret_05/AcidGunTower_1-5.ma";
file -r -rpr "Turret05" -dr 1 -rfn "Turret05RN" "/Users/jmiller/Art/everlife//Assets/Art/Defense/Turret_05/AcidGunTower_1-5.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.3";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 57.266421701497542 21.561755479459695 21.127904189966948 ;
	setAttr ".r" -type "double3" -9.9383527295988792 -294.99999999999028 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 56.945671325179497;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" 0 0 500 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".ow" 72;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 7 ".lnk";
	setAttr -s 7 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "Turret05RN";
	setAttr -s 59 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Turret05RN"
		"Turret05RN" 0
		"Turret05RN" 126
		2 "|Turret05_Level01" "visibility" " 0"
		2 "|Turret05_Level02" "visibility" " 0"
		2 "|Turret05_Level03" "visibility" " 1"
		2 "|Turret05_Level03|Turret05_AcidGunTower03" "visibility" " 1"
		2 "|Turret05_Level03|Turret05_AcidGunTower03" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167" 
		"rotateX" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168" 
		"rotateX" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints" " -s 48"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[0]" " -type \"double3\" -0.5 -0.5 -0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[0].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[0].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[0].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[1]" " -type \"double3\" 0 -0.5 -0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[1].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[1].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[1].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[2]" " -type \"double3\" 0.5 -0.5 -0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[2].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[2].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[2].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[3]" " -type \"double3\" -0.5 -0.357143 -0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[3].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[3].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[3].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[4]" " -type \"double3\" 0 -0.357143 -0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[4].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[4].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[4].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[5]" " -type \"double3\" 0.5 -0.357143 -0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[5].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[5].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[5].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[6]" " -type \"double3\" -0.5 -0.214286 -0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[8]" " -type \"double3\" 0.5 -0.214286 -0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[24]" " -type \"double3\" -0.5 -0.5 0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[24].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[24].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[24].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[25]" " -type \"double3\" 0 -0.5 0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[25].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[25].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[25].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[26]" " -type \"double3\" 0.5 -0.5 0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[26].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[26].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[26].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[27]" " -type \"double3\" -0.5 -0.357143 0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[27].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[27].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[27].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[28]" " -type \"double3\" 0 -0.357143 0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[28].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[28].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[28].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[29]" " -type \"double3\" 0.5 -0.357143 0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[29].xValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[29].yValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[29].zValue" " -av"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[30]" " -type \"double3\" -0.5 -0.214286 0.5"
		2 "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape" 
		"controlPoints[32]" " -type \"double3\" 0.5 -0.214286 0.5"
		2 "|Turret05_Level04" "visibility" " 0"
		2 "|Turret05_Level05" "visibility" " 0"
		2 "Turret05_Level_03" "visibility" " 1"
		2 "Turret05_Level_02" "visibility" " 0"
		2 "Turret05_Level_01" "visibility" " 0"
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167.rotateX" 
		"Turret05RN.placeHolderList[1]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167.rotateY" 
		"Turret05RN.placeHolderList[2]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167.rotateZ" 
		"Turret05RN.placeHolderList[3]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167.visibility" 
		"Turret05RN.placeHolderList[4]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167.translateX" 
		"Turret05RN.placeHolderList[5]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167.translateY" 
		"Turret05RN.placeHolderList[6]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167.translateZ" 
		"Turret05RN.placeHolderList[7]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167.scaleX" 
		"Turret05RN.placeHolderList[8]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167.scaleY" 
		"Turret05RN.placeHolderList[9]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167.scaleZ" 
		"Turret05RN.placeHolderList[10]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168.rotateX" 
		"Turret05RN.placeHolderList[11]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168.rotateY" 
		"Turret05RN.placeHolderList[12]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168.rotateZ" 
		"Turret05RN.placeHolderList[13]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168.visibility" 
		"Turret05RN.placeHolderList[14]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168.translateX" 
		"Turret05RN.placeHolderList[15]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168.translateY" 
		"Turret05RN.placeHolderList[16]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168.translateZ" 
		"Turret05RN.placeHolderList[17]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168.scaleX" 
		"Turret05RN.placeHolderList[18]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168.scaleY" 
		"Turret05RN.placeHolderList[19]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168.scaleZ" 
		"Turret05RN.placeHolderList[20]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice.rotateX" 
		"Turret05RN.placeHolderList[21]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice.rotateY" 
		"Turret05RN.placeHolderList[22]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice.rotateZ" 
		"Turret05RN.placeHolderList[23]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[0].xValue" 
		"Turret05RN.placeHolderList[24]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[0].yValue" 
		"Turret05RN.placeHolderList[25]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[0].zValue" 
		"Turret05RN.placeHolderList[26]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[1].xValue" 
		"Turret05RN.placeHolderList[27]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[1].yValue" 
		"Turret05RN.placeHolderList[28]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[1].zValue" 
		"Turret05RN.placeHolderList[29]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[2].xValue" 
		"Turret05RN.placeHolderList[30]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[2].yValue" 
		"Turret05RN.placeHolderList[31]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[2].zValue" 
		"Turret05RN.placeHolderList[32]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[3].xValue" 
		"Turret05RN.placeHolderList[33]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[3].yValue" 
		"Turret05RN.placeHolderList[34]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[3].zValue" 
		"Turret05RN.placeHolderList[35]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[4].xValue" 
		"Turret05RN.placeHolderList[36]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[4].yValue" 
		"Turret05RN.placeHolderList[37]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[4].zValue" 
		"Turret05RN.placeHolderList[38]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[5].xValue" 
		"Turret05RN.placeHolderList[39]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[5].yValue" 
		"Turret05RN.placeHolderList[40]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[5].zValue" 
		"Turret05RN.placeHolderList[41]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[24].xValue" 
		"Turret05RN.placeHolderList[42]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[24].yValue" 
		"Turret05RN.placeHolderList[43]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[24].zValue" 
		"Turret05RN.placeHolderList[44]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[25].xValue" 
		"Turret05RN.placeHolderList[45]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[25].yValue" 
		"Turret05RN.placeHolderList[46]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[25].zValue" 
		"Turret05RN.placeHolderList[47]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[26].xValue" 
		"Turret05RN.placeHolderList[48]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[26].yValue" 
		"Turret05RN.placeHolderList[49]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[26].zValue" 
		"Turret05RN.placeHolderList[50]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[27].xValue" 
		"Turret05RN.placeHolderList[51]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[27].yValue" 
		"Turret05RN.placeHolderList[52]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[27].zValue" 
		"Turret05RN.placeHolderList[53]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[28].xValue" 
		"Turret05RN.placeHolderList[54]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[28].yValue" 
		"Turret05RN.placeHolderList[55]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[28].zValue" 
		"Turret05RN.placeHolderList[56]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[29].xValue" 
		"Turret05RN.placeHolderList[57]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[29].yValue" 
		"Turret05RN.placeHolderList[58]" ""
		5 4 "Turret05RN" "|Turret05_Level03|Turret05_AcidGunTower03|Turret05_polySurface166|Turret05_polySurface167|Turret05_polySurface168|Turret05_ffd1Lattice|Turret05_ffd1LatticeShape.controlPoints[29].zValue" 
		"Turret05RN.placeHolderList[59]" "";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
	setAttr -s 19 ".opt";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".filw" 0.83333331346511841;
	setAttr ".filh" 0.83333331346511841;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".gi" yes;
	setAttr ".gia" 32;
	setAttr ".fg" yes;
	setAttr ".fgr" 16;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "40";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 2 -ast 1 -aet 2 ";
	setAttr ".st" 6;
createNode animCurveTU -n "Turret05_polySurface167_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "Turret05_polySurface167_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret05_polySurface167_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret05_polySurface167_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret05_polySurface167_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 -29.082354636718211;
createNode animCurveTA -n "Turret05_polySurface167_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret05_polySurface167_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret05_polySurface167_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret05_polySurface167_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret05_polySurface167_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTA -n "Turret05_polySurface168_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 26.895441535653987;
createNode animCurveTA -n "Turret05_polySurface168_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret05_polySurface168_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret05_polySurface168_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "Turret05_polySurface168_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret05_polySurface168_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret05_polySurface168_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret05_polySurface168_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret05_polySurface168_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret05_polySurface168_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_0__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -0.5;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_0__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.42052752927820358;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_0__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.13490392963756551;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_1__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_1__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.42052752927820358;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_1__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.13490392963756551;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_24__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -0.5;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_24__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.42052752927820358;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_24__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 2 0.86509607036243408;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_25__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_25__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.42052752927820358;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_25__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 2 0.86509607036243408;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_26__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.5;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_26__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.42052752927820358;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_26__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 2 0.86509607036243408;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_27__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.66020173859020215;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_27__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.35714285714285715 2 -0.27767038642106068;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_27__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 2 1.1300986854715216;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_28__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_28__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.35714285714285715 2 -0.27767038642106068;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_28__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 2 0.86509607036243408;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_29__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 2 0.72691927059168171;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_29__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.35714285714285715 2 -0.27767038642106068;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_29__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 2 0.99370361414447805;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_2__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.5;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_2__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.42052752927820358;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_2__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.13490392963756551;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_3__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -0.5;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_3__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.35714285714285715 2 -0.27767038642106068;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_3__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.13490392963756551;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_4__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_4__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.35714285714285715 2 -0.27767038642106068;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_4__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.13490392963756551;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_5__xValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.5;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_5__yValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.35714285714285715 2 -0.27767038642106068;
createNode animCurveTL -n "Turret05_ffd1LatticeShape_controlPoints_5__zValue";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 2 -0.13490392963756551;
createNode animCurveTA -n "Turret05_ffd1Lattice_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret05_ffd1Lattice_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret05_ffd1Lattice_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 7 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -s 8 ".gn";
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 10 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 10 ".tx";
select -ne :lightList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".l";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 19 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 2 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren" -type "string" "mentalRay";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 32;
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar" 0;
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn" no;
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff" yes;
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp" -type "string" "Turret05_AcidGunTower03_idle_a345_2";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultRenderQuality;
	setAttr ".ert" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w" 1024;
	setAttr -av ".h" 1024;
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al" yes;
	setAttr -av ".dar" 1;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -s 2 ".dsm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
connectAttr "Turret05_polySurface167_rotateX.o" "Turret05RN.phl[1]";
connectAttr "Turret05_polySurface167_rotateY.o" "Turret05RN.phl[2]";
connectAttr "Turret05_polySurface167_rotateZ.o" "Turret05RN.phl[3]";
connectAttr "Turret05_polySurface167_visibility.o" "Turret05RN.phl[4]";
connectAttr "Turret05_polySurface167_translateX.o" "Turret05RN.phl[5]";
connectAttr "Turret05_polySurface167_translateY.o" "Turret05RN.phl[6]";
connectAttr "Turret05_polySurface167_translateZ.o" "Turret05RN.phl[7]";
connectAttr "Turret05_polySurface167_scaleX.o" "Turret05RN.phl[8]";
connectAttr "Turret05_polySurface167_scaleY.o" "Turret05RN.phl[9]";
connectAttr "Turret05_polySurface167_scaleZ.o" "Turret05RN.phl[10]";
connectAttr "Turret05_polySurface168_rotateX.o" "Turret05RN.phl[11]";
connectAttr "Turret05_polySurface168_rotateY.o" "Turret05RN.phl[12]";
connectAttr "Turret05_polySurface168_rotateZ.o" "Turret05RN.phl[13]";
connectAttr "Turret05_polySurface168_visibility.o" "Turret05RN.phl[14]";
connectAttr "Turret05_polySurface168_translateX.o" "Turret05RN.phl[15]";
connectAttr "Turret05_polySurface168_translateY.o" "Turret05RN.phl[16]";
connectAttr "Turret05_polySurface168_translateZ.o" "Turret05RN.phl[17]";
connectAttr "Turret05_polySurface168_scaleX.o" "Turret05RN.phl[18]";
connectAttr "Turret05_polySurface168_scaleY.o" "Turret05RN.phl[19]";
connectAttr "Turret05_polySurface168_scaleZ.o" "Turret05RN.phl[20]";
connectAttr "Turret05_ffd1Lattice_rotateX.o" "Turret05RN.phl[21]";
connectAttr "Turret05_ffd1Lattice_rotateY.o" "Turret05RN.phl[22]";
connectAttr "Turret05_ffd1Lattice_rotateZ.o" "Turret05RN.phl[23]";
connectAttr "Turret05_ffd1LatticeShape_controlPoints_0__xValue.o" "Turret05RN.phl[24]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_0__yValue.o" "Turret05RN.phl[25]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_0__zValue.o" "Turret05RN.phl[26]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_1__xValue.o" "Turret05RN.phl[27]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_1__yValue.o" "Turret05RN.phl[28]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_1__zValue.o" "Turret05RN.phl[29]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_2__xValue.o" "Turret05RN.phl[30]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_2__yValue.o" "Turret05RN.phl[31]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_2__zValue.o" "Turret05RN.phl[32]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_3__xValue.o" "Turret05RN.phl[33]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_3__yValue.o" "Turret05RN.phl[34]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_3__zValue.o" "Turret05RN.phl[35]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_4__xValue.o" "Turret05RN.phl[36]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_4__yValue.o" "Turret05RN.phl[37]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_4__zValue.o" "Turret05RN.phl[38]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_5__xValue.o" "Turret05RN.phl[39]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_5__yValue.o" "Turret05RN.phl[40]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_5__zValue.o" "Turret05RN.phl[41]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_24__xValue.o" "Turret05RN.phl[42]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_24__yValue.o" "Turret05RN.phl[43]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_24__zValue.o" "Turret05RN.phl[44]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_25__xValue.o" "Turret05RN.phl[45]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_25__yValue.o" "Turret05RN.phl[46]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_25__zValue.o" "Turret05RN.phl[47]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_26__xValue.o" "Turret05RN.phl[48]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_26__yValue.o" "Turret05RN.phl[49]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_26__zValue.o" "Turret05RN.phl[50]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_27__xValue.o" "Turret05RN.phl[51]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_27__yValue.o" "Turret05RN.phl[52]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_27__zValue.o" "Turret05RN.phl[53]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_28__xValue.o" "Turret05RN.phl[54]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_28__yValue.o" "Turret05RN.phl[55]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_28__zValue.o" "Turret05RN.phl[56]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_29__xValue.o" "Turret05RN.phl[57]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_29__yValue.o" "Turret05RN.phl[58]"
		;
connectAttr "Turret05_ffd1LatticeShape_controlPoints_29__zValue.o" "Turret05RN.phl[59]"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Turret05_AcidGunTower03.ma
