//Maya ASCII 2013 scene
//Name: Turret02_AntiAirCannon02.ma
//Last modified: Tue, Jun 10, 2014 11:00:31 AM
//Codeset: UTF-8
file -rdi 1 -rpr "Turret02" -rfn "Turret02RN" "/Users/jmiller/Art/everlife//Assets/Art/Defense/Turret_02/AntiAirCannon_1-5.ma";
file -r -rpr "Turret02" -dr 1 -rfn "Turret02RN" "/Users/jmiller/Art/everlife//Assets/Art/Defense/Turret_02/AntiAirCannon_1-5.ma";
requires maya "2013";
requires "Mayatomr" "2013.0 - 3.10.1.9 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201207040330-835994";
fileInfo "osv" "Mac OS X 10.9.2";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 23.125311887118045 37.530487869616266 35.925953662507148 ;
	setAttr ".r" -type "double3" -39.938352729602158 27.399999999999601 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 55.144190721948945;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "OrthoCamera";
	setAttr ".r" -type "double3" -45 45 0 ;
createNode transform -n "orthCam" -p "OrthoCamera";
	setAttr ".t" -type "double3" 0 0 500 ;
createNode camera -n "orthCamShape" -p "orthCam";
	setAttr -k off ".v";
	setAttr ".ow" 36;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".dr" yes;
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 8 ".lnk";
	setAttr -s 8 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode reference -n "Turret02RN";
	setAttr -s 110 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Turret02RN"
		"Turret02RN" 0
		"Turret02RN" 130
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115" "translate" 
		" -type \"double3\" 0 0 0"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115" "translateY" 
		" -av"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128" "translate" 
		" -type \"double3\" 0 0 0"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128" "translateY" 
		" -av"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137" "translate" 
		" -type \"double3\" 0 0 0"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137" "translateY" 
		" -av"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182" 
		"translateY" " -av"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183" 
		"translateY" " -av"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22" 
		"visibility" " -av 1"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4" 
		"translateY" " -av"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23" 
		"visibility" " -av 1"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5" 
		"translateY" " -av"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21" 
		"visibility" " -av 1"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3" 
		"translateY" " -av"
		2 "Turret02_level_02" "visibility" " 1"
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115.visibility" 
		"Turret02RN.placeHolderList[1]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115.translateX" 
		"Turret02RN.placeHolderList[2]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115.translateY" 
		"Turret02RN.placeHolderList[3]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115.translateZ" 
		"Turret02RN.placeHolderList[4]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115.rotateX" 
		"Turret02RN.placeHolderList[5]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115.rotateY" 
		"Turret02RN.placeHolderList[6]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115.rotateZ" 
		"Turret02RN.placeHolderList[7]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115.scaleX" 
		"Turret02RN.placeHolderList[8]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115.scaleY" 
		"Turret02RN.placeHolderList[9]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface115.scaleZ" 
		"Turret02RN.placeHolderList[10]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128.visibility" 
		"Turret02RN.placeHolderList[11]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128.translateX" 
		"Turret02RN.placeHolderList[12]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128.translateY" 
		"Turret02RN.placeHolderList[13]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128.translateZ" 
		"Turret02RN.placeHolderList[14]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128.rotateX" 
		"Turret02RN.placeHolderList[15]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128.rotateY" 
		"Turret02RN.placeHolderList[16]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128.rotateZ" 
		"Turret02RN.placeHolderList[17]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128.scaleX" 
		"Turret02RN.placeHolderList[18]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128.scaleY" 
		"Turret02RN.placeHolderList[19]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface128.scaleZ" 
		"Turret02RN.placeHolderList[20]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137.visibility" 
		"Turret02RN.placeHolderList[21]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137.translateX" 
		"Turret02RN.placeHolderList[22]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137.translateY" 
		"Turret02RN.placeHolderList[23]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137.translateZ" 
		"Turret02RN.placeHolderList[24]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137.rotateX" 
		"Turret02RN.placeHolderList[25]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137.rotateY" 
		"Turret02RN.placeHolderList[26]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137.rotateZ" 
		"Turret02RN.placeHolderList[27]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137.scaleX" 
		"Turret02RN.placeHolderList[28]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137.scaleY" 
		"Turret02RN.placeHolderList[29]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_polySurface137.scaleZ" 
		"Turret02RN.placeHolderList[30]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182.visibility" 
		"Turret02RN.placeHolderList[31]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182.translateX" 
		"Turret02RN.placeHolderList[32]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182.translateY" 
		"Turret02RN.placeHolderList[33]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182.translateZ" 
		"Turret02RN.placeHolderList[34]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182.rotateX" 
		"Turret02RN.placeHolderList[35]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182.rotateY" 
		"Turret02RN.placeHolderList[36]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182.rotateZ" 
		"Turret02RN.placeHolderList[37]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182.scaleX" 
		"Turret02RN.placeHolderList[38]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182.scaleY" 
		"Turret02RN.placeHolderList[39]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface182.scaleZ" 
		"Turret02RN.placeHolderList[40]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183.visibility" 
		"Turret02RN.placeHolderList[41]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183.translateX" 
		"Turret02RN.placeHolderList[42]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183.translateY" 
		"Turret02RN.placeHolderList[43]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183.translateZ" 
		"Turret02RN.placeHolderList[44]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183.rotateX" 
		"Turret02RN.placeHolderList[45]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183.rotateY" 
		"Turret02RN.placeHolderList[46]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183.rotateZ" 
		"Turret02RN.placeHolderList[47]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183.scaleX" 
		"Turret02RN.placeHolderList[48]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183.scaleY" 
		"Turret02RN.placeHolderList[49]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform166|Turret02_polySurface117|Turret02_polySurface183.scaleZ" 
		"Turret02RN.placeHolderList[50]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22.visibility" 
		"Turret02RN.placeHolderList[51]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22.translateX" 
		"Turret02RN.placeHolderList[52]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22.translateY" 
		"Turret02RN.placeHolderList[53]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22.translateZ" 
		"Turret02RN.placeHolderList[54]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22.rotateX" 
		"Turret02RN.placeHolderList[55]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22.rotateY" 
		"Turret02RN.placeHolderList[56]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22.rotateZ" 
		"Turret02RN.placeHolderList[57]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22.scaleX" 
		"Turret02RN.placeHolderList[58]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22.scaleY" 
		"Turret02RN.placeHolderList[59]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform167|Turret02_pasted__pasted__pasted__pSphere22.scaleZ" 
		"Turret02RN.placeHolderList[60]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4.translateX" 
		"Turret02RN.placeHolderList[61]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4.translateY" 
		"Turret02RN.placeHolderList[62]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4.translateZ" 
		"Turret02RN.placeHolderList[63]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4.visibility" 
		"Turret02RN.placeHolderList[64]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4.rotateX" 
		"Turret02RN.placeHolderList[65]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4.rotateY" 
		"Turret02RN.placeHolderList[66]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4.rotateZ" 
		"Turret02RN.placeHolderList[67]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4.scaleX" 
		"Turret02RN.placeHolderList[68]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4.scaleY" 
		"Turret02RN.placeHolderList[69]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform168|Turret02_pasted__pasted__pasted__pPipe4.scaleZ" 
		"Turret02RN.placeHolderList[70]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23.visibility" 
		"Turret02RN.placeHolderList[71]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23.translateX" 
		"Turret02RN.placeHolderList[72]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23.translateY" 
		"Turret02RN.placeHolderList[73]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23.translateZ" 
		"Turret02RN.placeHolderList[74]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23.rotateX" 
		"Turret02RN.placeHolderList[75]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23.rotateY" 
		"Turret02RN.placeHolderList[76]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23.rotateZ" 
		"Turret02RN.placeHolderList[77]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23.scaleX" 
		"Turret02RN.placeHolderList[78]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23.scaleY" 
		"Turret02RN.placeHolderList[79]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform169|Turret02_pasted__pasted__pasted__pSphere23.scaleZ" 
		"Turret02RN.placeHolderList[80]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5.translateX" 
		"Turret02RN.placeHolderList[81]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5.translateY" 
		"Turret02RN.placeHolderList[82]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5.translateZ" 
		"Turret02RN.placeHolderList[83]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5.visibility" 
		"Turret02RN.placeHolderList[84]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5.rotateX" 
		"Turret02RN.placeHolderList[85]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5.rotateY" 
		"Turret02RN.placeHolderList[86]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5.rotateZ" 
		"Turret02RN.placeHolderList[87]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5.scaleX" 
		"Turret02RN.placeHolderList[88]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5.scaleY" 
		"Turret02RN.placeHolderList[89]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform170|Turret02_pasted__pasted__pasted__pPipe5.scaleZ" 
		"Turret02RN.placeHolderList[90]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21.visibility" 
		"Turret02RN.placeHolderList[91]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21.translateX" 
		"Turret02RN.placeHolderList[92]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21.translateY" 
		"Turret02RN.placeHolderList[93]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21.translateZ" 
		"Turret02RN.placeHolderList[94]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21.rotateX" 
		"Turret02RN.placeHolderList[95]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21.rotateY" 
		"Turret02RN.placeHolderList[96]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21.rotateZ" 
		"Turret02RN.placeHolderList[97]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21.scaleX" 
		"Turret02RN.placeHolderList[98]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21.scaleY" 
		"Turret02RN.placeHolderList[99]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform171|Turret02_pasted__pasted__pasted__pSphere21.scaleZ" 
		"Turret02RN.placeHolderList[100]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3.translateX" 
		"Turret02RN.placeHolderList[101]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3.translateY" 
		"Turret02RN.placeHolderList[102]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3.translateZ" 
		"Turret02RN.placeHolderList[103]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3.visibility" 
		"Turret02RN.placeHolderList[104]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3.rotateX" 
		"Turret02RN.placeHolderList[105]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3.rotateY" 
		"Turret02RN.placeHolderList[106]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3.rotateZ" 
		"Turret02RN.placeHolderList[107]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3.scaleX" 
		"Turret02RN.placeHolderList[108]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3.scaleY" 
		"Turret02RN.placeHolderList[109]" ""
		5 4 "Turret02RN" "|Turret02_Level02|Turret02_AntiAirCannon02|Turret02_transform172|Turret02_pasted__pasted__pasted__pPipe3.scaleZ" 
		"Turret02RN.placeHolderList[110]" "";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
	setAttr -s 19 ".opt";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".filw" 0.83333331346511841;
	setAttr ".filh" 0.83333331346511841;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".gi" yes;
	setAttr ".gia" 32;
	setAttr ".fg" yes;
	setAttr ".fgr" 16;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "40";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "true";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "512";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".ray" no;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	setAttr ".splck" no;
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".scan" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode mentalrayOptions -s -n "miContourPreset";
createNode mentalrayOptions -s -n "Draft";
	setAttr ".maxr" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".maxr" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".shmth" 3;
	setAttr ".shmap" 3;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 2 -ast 1 -aet 2 ";
	setAttr ".st" 6;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere22_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pSphere22_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pSphere22_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pSphere22_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pSphere22_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pSphere22_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pSphere22_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere22_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere22_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere22_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere23_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pSphere23_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pSphere23_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pSphere23_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pSphere23_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pSphere23_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pSphere23_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere23_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere23_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere23_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere21_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pSphere21_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pSphere21_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pSphere21_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pSphere21_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pSphere21_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pSphere21_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere21_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere21_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pSphere21_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface182_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "Turret02_polySurface182_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_polySurface182_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 -0.03229085073955635;
createNode animCurveTL -n "Turret02_polySurface182_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface182_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface182_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface182_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_polySurface182_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface182_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface182_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface183_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "Turret02_polySurface183_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_polySurface183_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 -0.03229085073955635;
createNode animCurveTL -n "Turret02_polySurface183_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface183_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface183_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface183_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_polySurface183_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface183_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface183_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface137_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "Turret02_polySurface137_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_polySurface137_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 -0.27552736876324602;
createNode animCurveTL -n "Turret02_polySurface137_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface137_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface137_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface137_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_polySurface137_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface137_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface137_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface115_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "Turret02_polySurface115_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_polySurface115_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 -0.27552736876324602;
createNode animCurveTL -n "Turret02_polySurface115_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface115_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface115_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface115_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_polySurface115_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface115_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface115_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface128_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "Turret02_polySurface128_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_polySurface128_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 -0.27552736876324602;
createNode animCurveTL -n "Turret02_polySurface128_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface128_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface128_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_polySurface128_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_polySurface128_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface128_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_polySurface128_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pPipe4_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pPipe4_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 -0.27552736876324602;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pPipe4_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pPipe5_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pPipe5_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 -0.27552736876324602;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pPipe5_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pPipe3_translateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pPipe3_translateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 -0.27552736876324602;
createNode animCurveTL -n "Turret02_pasted__pasted__pasted__pPipe3_translateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe4_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pPipe4_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pPipe4_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pPipe4_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe4_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe4_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe4_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe3_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pPipe3_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pPipe3_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pPipe3_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe3_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe3_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe3_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe5_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pPipe5_rotateX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pPipe5_rotateY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "Turret02_pasted__pasted__pasted__pPipe5_rotateZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe5_scaleX";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe5_scaleY";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "Turret02_pasted__pasted__pasted__pPipe5_scaleZ";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
select -ne :time1;
	setAttr -av -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 8 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -cb on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 8 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :lightList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".l";
select -ne :postProcessList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 6 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 2 ".r";
select -ne :renderGlobalsList1;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren" -type "string" "mentalRay";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 32;
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar" 0;
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn" no;
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff" yes;
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultRenderQuality;
	setAttr ".ert" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w" 512;
	setAttr -av ".h" 512;
	setAttr -av ".pa" 1;
	setAttr -av -k on ".al" yes;
	setAttr -av ".dar" 1;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -s 2 ".dsm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -cb on ".cch";
	setAttr -cb on ".ihi";
	setAttr -cb on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -cb on ".rp";
	setAttr -cb on ".cai";
	setAttr -cb on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -cb on ".ei";
	setAttr -av -cb on ".ex";
	setAttr -av -cb on ".es";
	setAttr -av -cb on ".ef";
	setAttr -av -cb on ".bf";
	setAttr -cb on ".fii";
	setAttr -av -cb on ".sf";
	setAttr -cb on ".gr";
	setAttr -cb on ".li";
	setAttr -cb on ".ls";
	setAttr -av -cb on ".mb";
	setAttr -cb on ".ti";
	setAttr -cb on ".txt";
	setAttr -cb on ".mpr";
	setAttr -cb on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -cb on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -cb on ".as";
	setAttr -cb on ".ds";
	setAttr -cb on ".lm";
	setAttr -av -cb on ".fir";
	setAttr -cb on ".aap";
	setAttr -av -cb on ".gh";
	setAttr -cb on ".sd";
connectAttr "Turret02_polySurface115_visibility.o" "Turret02RN.phl[1]";
connectAttr "Turret02_polySurface115_translateX.o" "Turret02RN.phl[2]";
connectAttr "Turret02_polySurface115_translateY.o" "Turret02RN.phl[3]";
connectAttr "Turret02_polySurface115_translateZ.o" "Turret02RN.phl[4]";
connectAttr "Turret02_polySurface115_rotateX.o" "Turret02RN.phl[5]";
connectAttr "Turret02_polySurface115_rotateY.o" "Turret02RN.phl[6]";
connectAttr "Turret02_polySurface115_rotateZ.o" "Turret02RN.phl[7]";
connectAttr "Turret02_polySurface115_scaleX.o" "Turret02RN.phl[8]";
connectAttr "Turret02_polySurface115_scaleY.o" "Turret02RN.phl[9]";
connectAttr "Turret02_polySurface115_scaleZ.o" "Turret02RN.phl[10]";
connectAttr "Turret02_polySurface128_visibility.o" "Turret02RN.phl[11]";
connectAttr "Turret02_polySurface128_translateX.o" "Turret02RN.phl[12]";
connectAttr "Turret02_polySurface128_translateY.o" "Turret02RN.phl[13]";
connectAttr "Turret02_polySurface128_translateZ.o" "Turret02RN.phl[14]";
connectAttr "Turret02_polySurface128_rotateX.o" "Turret02RN.phl[15]";
connectAttr "Turret02_polySurface128_rotateY.o" "Turret02RN.phl[16]";
connectAttr "Turret02_polySurface128_rotateZ.o" "Turret02RN.phl[17]";
connectAttr "Turret02_polySurface128_scaleX.o" "Turret02RN.phl[18]";
connectAttr "Turret02_polySurface128_scaleY.o" "Turret02RN.phl[19]";
connectAttr "Turret02_polySurface128_scaleZ.o" "Turret02RN.phl[20]";
connectAttr "Turret02_polySurface137_visibility.o" "Turret02RN.phl[21]";
connectAttr "Turret02_polySurface137_translateX.o" "Turret02RN.phl[22]";
connectAttr "Turret02_polySurface137_translateY.o" "Turret02RN.phl[23]";
connectAttr "Turret02_polySurface137_translateZ.o" "Turret02RN.phl[24]";
connectAttr "Turret02_polySurface137_rotateX.o" "Turret02RN.phl[25]";
connectAttr "Turret02_polySurface137_rotateY.o" "Turret02RN.phl[26]";
connectAttr "Turret02_polySurface137_rotateZ.o" "Turret02RN.phl[27]";
connectAttr "Turret02_polySurface137_scaleX.o" "Turret02RN.phl[28]";
connectAttr "Turret02_polySurface137_scaleY.o" "Turret02RN.phl[29]";
connectAttr "Turret02_polySurface137_scaleZ.o" "Turret02RN.phl[30]";
connectAttr "Turret02_polySurface182_visibility.o" "Turret02RN.phl[31]";
connectAttr "Turret02_polySurface182_translateX.o" "Turret02RN.phl[32]";
connectAttr "Turret02_polySurface182_translateY.o" "Turret02RN.phl[33]";
connectAttr "Turret02_polySurface182_translateZ.o" "Turret02RN.phl[34]";
connectAttr "Turret02_polySurface182_rotateX.o" "Turret02RN.phl[35]";
connectAttr "Turret02_polySurface182_rotateY.o" "Turret02RN.phl[36]";
connectAttr "Turret02_polySurface182_rotateZ.o" "Turret02RN.phl[37]";
connectAttr "Turret02_polySurface182_scaleX.o" "Turret02RN.phl[38]";
connectAttr "Turret02_polySurface182_scaleY.o" "Turret02RN.phl[39]";
connectAttr "Turret02_polySurface182_scaleZ.o" "Turret02RN.phl[40]";
connectAttr "Turret02_polySurface183_visibility.o" "Turret02RN.phl[41]";
connectAttr "Turret02_polySurface183_translateX.o" "Turret02RN.phl[42]";
connectAttr "Turret02_polySurface183_translateY.o" "Turret02RN.phl[43]";
connectAttr "Turret02_polySurface183_translateZ.o" "Turret02RN.phl[44]";
connectAttr "Turret02_polySurface183_rotateX.o" "Turret02RN.phl[45]";
connectAttr "Turret02_polySurface183_rotateY.o" "Turret02RN.phl[46]";
connectAttr "Turret02_polySurface183_rotateZ.o" "Turret02RN.phl[47]";
connectAttr "Turret02_polySurface183_scaleX.o" "Turret02RN.phl[48]";
connectAttr "Turret02_polySurface183_scaleY.o" "Turret02RN.phl[49]";
connectAttr "Turret02_polySurface183_scaleZ.o" "Turret02RN.phl[50]";
connectAttr "Turret02_pasted__pasted__pasted__pSphere22_visibility.o" "Turret02RN.phl[51]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere22_translateX.o" "Turret02RN.phl[52]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere22_translateY.o" "Turret02RN.phl[53]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere22_translateZ.o" "Turret02RN.phl[54]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere22_rotateX.o" "Turret02RN.phl[55]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere22_rotateY.o" "Turret02RN.phl[56]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere22_rotateZ.o" "Turret02RN.phl[57]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere22_scaleX.o" "Turret02RN.phl[58]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere22_scaleY.o" "Turret02RN.phl[59]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere22_scaleZ.o" "Turret02RN.phl[60]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe4_translateX.o" "Turret02RN.phl[61]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe4_translateY.o" "Turret02RN.phl[62]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe4_translateZ.o" "Turret02RN.phl[63]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe4_visibility.o" "Turret02RN.phl[64]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe4_rotateX.o" "Turret02RN.phl[65]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe4_rotateY.o" "Turret02RN.phl[66]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe4_rotateZ.o" "Turret02RN.phl[67]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe4_scaleX.o" "Turret02RN.phl[68]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe4_scaleY.o" "Turret02RN.phl[69]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe4_scaleZ.o" "Turret02RN.phl[70]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere23_visibility.o" "Turret02RN.phl[71]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere23_translateX.o" "Turret02RN.phl[72]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere23_translateY.o" "Turret02RN.phl[73]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere23_translateZ.o" "Turret02RN.phl[74]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere23_rotateX.o" "Turret02RN.phl[75]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere23_rotateY.o" "Turret02RN.phl[76]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere23_rotateZ.o" "Turret02RN.phl[77]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere23_scaleX.o" "Turret02RN.phl[78]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere23_scaleY.o" "Turret02RN.phl[79]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere23_scaleZ.o" "Turret02RN.phl[80]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe5_translateX.o" "Turret02RN.phl[81]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe5_translateY.o" "Turret02RN.phl[82]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe5_translateZ.o" "Turret02RN.phl[83]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe5_visibility.o" "Turret02RN.phl[84]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe5_rotateX.o" "Turret02RN.phl[85]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe5_rotateY.o" "Turret02RN.phl[86]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe5_rotateZ.o" "Turret02RN.phl[87]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe5_scaleX.o" "Turret02RN.phl[88]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe5_scaleY.o" "Turret02RN.phl[89]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe5_scaleZ.o" "Turret02RN.phl[90]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere21_visibility.o" "Turret02RN.phl[91]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere21_translateX.o" "Turret02RN.phl[92]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere21_translateY.o" "Turret02RN.phl[93]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere21_translateZ.o" "Turret02RN.phl[94]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere21_rotateX.o" "Turret02RN.phl[95]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere21_rotateY.o" "Turret02RN.phl[96]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere21_rotateZ.o" "Turret02RN.phl[97]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere21_scaleX.o" "Turret02RN.phl[98]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere21_scaleY.o" "Turret02RN.phl[99]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pSphere21_scaleZ.o" "Turret02RN.phl[100]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe3_translateX.o" "Turret02RN.phl[101]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe3_translateY.o" "Turret02RN.phl[102]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe3_translateZ.o" "Turret02RN.phl[103]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe3_visibility.o" "Turret02RN.phl[104]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe3_rotateX.o" "Turret02RN.phl[105]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe3_rotateY.o" "Turret02RN.phl[106]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe3_rotateZ.o" "Turret02RN.phl[107]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe3_scaleX.o" "Turret02RN.phl[108]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe3_scaleY.o" "Turret02RN.phl[109]"
		;
connectAttr "Turret02_pasted__pasted__pasted__pPipe3_scaleZ.o" "Turret02RN.phl[110]"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Turret02_AntiAirCannon02.ma
